﻿using Abp.Data;
using Abp.EntityFramework;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.EntityFramework;
using TechnoForms.EntityFramework.Repositories;
using TechnoForms.Jobs;
using TechnoForms.Jobs.Dto;

namespace TechnoForms.Dto
{
    public class JobRepository : TechnoFormsRepositoryBase<Job, int>, IJobRepository
    {
        private readonly IActiveTransactionProvider _transactionProvider;

        public JobRepository(IDbContextProvider<TechnoFormsDbContext> dbContextProvider, IActiveTransactionProvider transactionProvider)
            : base(dbContextProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public async Task<List<JobList>> GetJobListAsync(JobListInput input)
        {
            EnsureConnectionOpen();
            try
            {
                var Received = "";
                if (input.FilterReceivedId != null)
                {
                    for (int i = 0; i < input.FilterReceivedId.Count(); i++)
                    {
                        if (i == 0)
                            Received = input.FilterReceivedId[i].ToString();
                        else
                            Received = input.FilterReceivedId[i].ToString() + "," + input.FilterReceivedId[i].ToString();
                    }
                }

                var NotReceived = "";
                if (input.FilterNotReceivedId != null)
                {
                    for (int i = 0; i < input.FilterNotReceivedId.Count(); i++)
                    {
                        if (i == 0)
                            NotReceived = input.FilterNotReceivedId[i].ToString();
                        else
                            NotReceived = input.FilterNotReceivedId[i].ToString() + "," + input.FilterNotReceivedId[i].ToString();
                    }
                }

                SqlParameter ProjectNumber = new SqlParameter("@ProjectNumber", input.ProjectNumber);
                SqlParameter FilterFormId = new SqlParameter("@FilterFormId", input.FilterFormId);
                SqlParameter FilterOwnerName = new SqlParameter("@FilterOwnerName", input.FilterOwnerName);
                SqlParameter FilterCity = new SqlParameter("@FilterCityId", input.FilterCity);
                SqlParameter FilterState = new SqlParameter("@FilterState", input.FilterState);
                SqlParameter FilterInstallerId = new SqlParameter("@FilterInstallerId", input.FilterInstallerId);
                SqlParameter FilterSystemtypeId = new SqlParameter("@SystemType_Id", input.FilterSystemtypeId);
                SqlParameter FilterJobStatusId = new SqlParameter("@FilterJobStatusId", input.FilterJobStatusId);
                SqlParameter FilterStartDate = new SqlParameter("@FilterDateypeId", input.FilterStartDate);
                SqlParameter FilterEndDate = new SqlParameter("@FilterStartDate", input.FilterEndDate);
                SqlParameter FilterDateypeId = new SqlParameter("@FilterEndDate", input.FilterDateypeId);
                SqlParameter FilterReceivedId = new SqlParameter("@FilterReceivedId", Received);
                SqlParameter FilterNotReceivedId = new SqlParameter("@FilterNotReceivedId", NotReceived);
                SqlParameter IsVerified = new SqlParameter("@IsVerified", input.IsVerified);
                SqlParameter SkipCount = new SqlParameter("@SkipCount", input.SkipCount);
                SqlParameter MaxResultCount = new SqlParameter("@MaxResultCount", input.MaxResultCount);
                SqlParameter Sorting = new SqlParameter("@Sorting", input.Sorting);
                SqlParameter AscDesc = new SqlParameter("@AscDesc", input.AscDesc);

                using (var command = CreateCommand("jobGridData", CommandType.StoredProcedure, ProjectNumber, FilterFormId, FilterOwnerName, FilterCity, FilterState, FilterInstallerId, FilterSystemtypeId, FilterJobStatusId, FilterStartDate, FilterEndDate, FilterDateypeId, FilterReceivedId, FilterNotReceivedId, IsVerified, SkipCount, MaxResultCount, Sorting, AscDesc))
                {
                    using (var dataReader = await command.ExecuteReaderAsync())
                    {
                        var result = new List<JobList>();

                        while (dataReader.Read())
                        {
                            result.Add(new JobList()
                            {
                                FullName = dataReader["FullName"].ToString()
                                //ExpectedInstallDate = dataReader["ExpectedInstallDate"].ToString(),
                                //ProjectNumber = dataReader["ProjectNumber"].ToString(),
                                //StreetAddress = dataReader["StreetAddress"].ToString(),
                                //StreetCity = dataReader["StreetCity"].ToString(),
                                //StreetState = dataReader["StreetState"].ToString(),
                                //StreetPostCode = dataReader["StreetPostCode"].ToString(),
                                //FirstName = dataReader["FirstName"].ToString(),
                                //LastName = dataReader["LastName"].ToString()
                            });
                        }
                        return result;
                    }
                }
            }
            catch (Exception e)
            {
                throw new UserFriendlyException("Ooppps...!!!", e.Message);
            }
        }

        private void EnsureConnectionOpen()
        {
            var connection = Context.Database.Connection;

            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }
        }

        private DbCommand CreateCommand(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var command = Context.Database.Connection.CreateCommand();

            command.CommandText = commandText;
            command.CommandType = commandType;
            //command.Transaction = GetActiveTransaction();
            command.Parameters.Clear();

            foreach (var parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }

            return command;
        }
    }
}
