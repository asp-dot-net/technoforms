namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class TenantDashboardJobList : DbMigration
    {
        public override void Up()
        {
            CreateStoredProcedure(
        "dbo.TenantDashboardJobList",
        //p => new
        //{
        //    TerritoryID = p.String(maxLength: 20),
        //    TerritoryDescription = p.String(maxLength: 50),
        //    RegionID = p.Int(),
        //},
        body:
            @"select top 5 J.FirstName,J.LastName,CONVERT(varchar,J.ExpectedInstallDate,101) as ExpectedInstallDate,J.ProjectNumber,J.StreetAddress,J.StreetCity,J.StreetState,J.StreetPostCode from Job as J
    where J.SignatureImageOwner is null or J.SignatureImageInst is null or J.NoOfInverter != (Select count(*) from ScanedInverter where Guid = J.Guid and IsDeleted = 0) 
    or J.NoOfPanels != (Select count(*) from Scanedpanel where Guid = J.Guid and IsDeleted = 0) or (Select count(*) from Photo where Guid = J.Guid) <= 0 and IsDeleted = 0 
    order by J.ExpectedInstallDate "
    );
        }

        public override void Down()
        {
            DropStoredProcedure("TenantDashboardJobList");
        }
    }
}
