namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class audio_update_jobId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Audio", "JobId", c => c.Int(nullable: false));
            DropColumn("dbo.Audio", "Guid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Audio", "Guid", c => c.Guid(nullable: false));
            DropColumn("dbo.Audio", "JobId");
        }
    }
}
