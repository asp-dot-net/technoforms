namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class TypeOfConnection : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TypeOfConnection",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TypeOfConnectionName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TypeOfConnection_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.TypeOfConnection", new[] { "IsDeleted" });
            DropTable("dbo.TypeOfConnection",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_TypeOfConnection_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
