namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_UserDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AbpUsers", "Dept_Id", c => c.Int(nullable: false));
            AddColumn("dbo.AbpUsers", "Unit_Id", c => c.Int(nullable: false));
            AddColumn("dbo.AbpUsers", "EmploymentType", c => c.String());
            AddColumn("dbo.AbpUsers", "ContractType", c => c.String());
            AddColumn("dbo.AbpUsers", "WindowsID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AbpUsers", "WindowsID");
            DropColumn("dbo.AbpUsers", "ContractType");
            DropColumn("dbo.AbpUsers", "EmploymentType");
            DropColumn("dbo.AbpUsers", "Unit_Id");
            DropColumn("dbo.AbpUsers", "Dept_Id");
        }
    }
}
