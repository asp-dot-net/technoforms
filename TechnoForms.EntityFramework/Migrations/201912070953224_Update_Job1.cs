namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Job1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "Longitude_PickList", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "Latitude_PickList", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Job", "Latitude_PickList");
            DropColumn("dbo.Job", "Longitude_PickList");
        }
    }
}
