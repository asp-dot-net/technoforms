namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Panel : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Panel", "LicenceOrCertiHolder", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Panel", "ModelNo", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Panel", "FireTested", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Panel", "FireTested", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Panel", "ModelNo", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Panel", "LicenceOrCertiHolder", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
