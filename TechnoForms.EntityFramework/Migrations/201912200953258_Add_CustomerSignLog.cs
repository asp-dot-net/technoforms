namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_CustomerSignLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustomerSignLog",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SenderUserId = c.Int(nullable: false),
                        TokenDate = c.DateTime(nullable: false),
                        TokenGuid = c.Guid(nullable: false),
                        Token = c.String(),
                        SendBy = c.String(),
                        Longitude = c.String(),
                        Latitude = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CustomerSignLog_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
            CreateTable(
                "dbo.Token",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TokenNo = c.String(nullable: false, maxLength: 100),
                        GuId = c.Guid(nullable: false),
                        IsExpired = c.Boolean(nullable: false),
                        IsOwner = c.Boolean(nullable: false),
                        IsSent = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Token_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Token", new[] { "IsDeleted" });
            DropIndex("dbo.CustomerSignLog", new[] { "IsDeleted" });
            DropTable("dbo.Token",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Token_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.CustomerSignLog",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_CustomerSignLog_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
