namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CustomerInstallerUpdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.InstallerEmploye", "FullName", c => c.String(maxLength: 200));
            AlterColumn("dbo.InstallerEmploye", "Phone", c => c.String(maxLength: 10));
            AlterColumn("dbo.InstallerEmploye", "EmailId", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.InstallerEmploye", "EmailId", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.InstallerEmploye", "Phone", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.InstallerEmploye", "FullName", c => c.String(nullable: false, maxLength: 200));
        }
    }
}
