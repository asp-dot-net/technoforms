namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_LatLong_For_Job : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "JobLatitude", c => c.String());
            AddColumn("dbo.Job", "JobLongitude", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Job", "JobLongitude");
            DropColumn("dbo.Job", "JobLatitude");
        }
    }
}
