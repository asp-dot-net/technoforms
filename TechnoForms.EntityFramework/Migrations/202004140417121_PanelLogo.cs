namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PanelLogo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Panel", "PanelLogo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Panel", "PanelLogo");
        }
    }
}
