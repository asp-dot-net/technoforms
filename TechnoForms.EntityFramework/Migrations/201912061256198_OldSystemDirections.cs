namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OldSystemDirections : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.OldSystemDirection",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DirectionName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.OldSystemDirection");
        }
    }
}
