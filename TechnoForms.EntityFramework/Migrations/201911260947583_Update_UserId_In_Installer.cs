namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_UserId_In_Installer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomerInstaller", "UserId", c => c.Int());
            AlterColumn("dbo.Installer", "UserId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Installer", "UserId", c => c.Int(nullable: false));
            DropColumn("dbo.CustomerInstaller", "UserId");
        }
    }
}
