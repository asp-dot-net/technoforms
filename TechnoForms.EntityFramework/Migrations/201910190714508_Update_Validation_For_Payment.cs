namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Validation_For_Payment : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Payment", "date", c => c.String(nullable: false));
            AlterColumn("dbo.Payment", "InstrumentDetail", c => c.String(maxLength: 500));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Payment", "InstrumentDetail", c => c.String());
            AlterColumn("dbo.Payment", "date", c => c.String());
        }
    }
}
