namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Bulkupload_Log : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InverterBulkUpload",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        InverterFileName = c.String(nullable: false, maxLength: 500),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InverterBulkUpload_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
            CreateTable(
                "dbo.JobBulkUpload",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        JobFileName = c.String(nullable: false, maxLength: 500),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_JobBulkUpload_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
            CreateTable(
                "dbo.PanelBulkUpload",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PanelFileName = c.String(nullable: false, maxLength: 500),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PanelBulkUpload_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.PanelBulkUpload", new[] { "IsDeleted" });
            DropIndex("dbo.JobBulkUpload", new[] { "IsDeleted" });
            DropIndex("dbo.InverterBulkUpload", new[] { "IsDeleted" });
            DropTable("dbo.PanelBulkUpload",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PanelBulkUpload_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.JobBulkUpload",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_JobBulkUpload_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
            DropTable("dbo.InverterBulkUpload",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InverterBulkUpload_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
