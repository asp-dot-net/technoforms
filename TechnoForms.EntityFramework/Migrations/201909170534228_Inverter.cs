namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Inverter : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Inverter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ManufacturarName = c.String(nullable: false, maxLength: 50),
                        Series = c.String(nullable: false, maxLength: 50),
                        ModelNumber = c.String(nullable: false, maxLength: 50),
                        ACPower = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Sizes = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ApprovalDate = c.DateTime(nullable: false),
                        ExpiryDate = c.DateTime(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Inverter_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Inverter", new[] { "IsDeleted" });
            DropTable("dbo.Inverter",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Inverter_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
