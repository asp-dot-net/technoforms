namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Fields_Job : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "FormId", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "SignatureImageInst", c => c.String(maxLength: 100));
            AddColumn("dbo.Job", "SignatureImageOwner", c => c.String(maxLength: 100));
            AddColumn("dbo.Job", "RECUploadFileName", c => c.String(maxLength: 100));
            AddColumn("dbo.Job", "RECUploadDate", c => c.DateTime());
            AddColumn("dbo.Job", "BulkUploadID", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "PVDNumber", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "PVDStatus", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "RECCreateDate", c => c.DateTime());
            AddColumn("dbo.Job", "SignatureImageInstDate", c => c.DateTime());
            AddColumn("dbo.Job", "SignatureImageOwnerDate", c => c.DateTime());
            AddColumn("dbo.Job", "SignatureImagePickList", c => c.String(maxLength: 100));
            AddColumn("dbo.Job", "SignatureImaegePickListDate", c => c.DateTime());
            AddColumn("dbo.Job", "IsVerifySTC", c => c.Boolean());
            AddColumn("dbo.Job", "IsVerifyImages", c => c.Boolean());
            AddColumn("dbo.Job", "OwnerPhoto", c => c.String(maxLength: 100));
            AddColumn("dbo.Job", "IsVerifyPanels", c => c.Boolean());
            AddColumn("dbo.Job", "IsVerifyInverters", c => c.Boolean());
            AddColumn("dbo.Job", "InstallerPhoto", c => c.String(maxLength: 100));
            AddColumn("dbo.Job", "IsReapply", c => c.Boolean());
            AddColumn("dbo.Job", "IsRenew", c => c.Boolean());
            AddColumn("dbo.Job", "IsVerifyCS", c => c.Boolean());
            AddColumn("dbo.Job", "IsVerifyIN", c => c.Boolean());
            AddColumn("dbo.Job", "IsVerifyPA", c => c.Boolean());
            AddColumn("dbo.Job", "IsVerifyIS", c => c.Boolean());
            AddColumn("dbo.Job", "IsVerifyPK", c => c.Boolean());
            AddColumn("dbo.Job", "IsVerifyELE", c => c.Boolean());
            AddColumn("dbo.Job", "IsVerifyCUCK", c => c.Boolean());
            AddColumn("dbo.Job", "Longitude_CustSign", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "Latitude_CustSign", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "Longitude_InstSign", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "Latitude_InstSign", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "IsComplaiencePA", c => c.Boolean());
            AddColumn("dbo.Job", "PAVerifiedBy", c => c.Int());
            AddColumn("dbo.Job", "IsComplaienceIN", c => c.Boolean());
            AddColumn("dbo.Job", "INVerifiedBy", c => c.Int());
            AddColumn("dbo.Job", "ISComplaienceIM", c => c.Boolean());
            AddColumn("dbo.Job", "IMVerifiedBy", c => c.Int());
            AddColumn("dbo.Job", "IsComplaienceCS", c => c.Boolean());
            AddColumn("dbo.Job", "CSVerifiedBy", c => c.Int());
            AddColumn("dbo.Job", "IsComplaienceIS", c => c.Boolean());
            AddColumn("dbo.Job", "ISVerifiedBy", c => c.Int());
            AddColumn("dbo.Job", "IsComplaiencePK", c => c.Boolean());
            AddColumn("dbo.Job", "PKVerifiedBy", c => c.Int());
            AddColumn("dbo.Job", "IsComplaienceSTC", c => c.Boolean());
            AddColumn("dbo.Job", "STCVerifiedBy", c => c.Int());
            AddColumn("dbo.Job", "IsComplaienceELE", c => c.Boolean());
            AddColumn("dbo.Job", "ELEVerifiedBy", c => c.Int());
            AddColumn("dbo.Job", "IsComplaienceCUCK", c => c.Boolean());
            AddColumn("dbo.Job", "CUCKVerifiedBy", c => c.Int());
            AddColumn("dbo.Job", "RequestSignatureImageInstDate", c => c.DateTime());
            AddColumn("dbo.Job", "RequestSignatureImageOwnerDate", c => c.DateTime());
            AddColumn("dbo.Job", "RejectReasonPA", c => c.String(maxLength: 1000));
            AddColumn("dbo.Job", "RejectReasonIN", c => c.String(maxLength: 1000));
            AddColumn("dbo.Job", "RejectReasonIM", c => c.String(maxLength: 1000));
            AddColumn("dbo.Job", "RejectReasonCS", c => c.String(maxLength: 1000));
            AddColumn("dbo.Job", "RejectReasonIS", c => c.String(maxLength: 1000));
            AddColumn("dbo.Job", "RejectReasonPK", c => c.String(maxLength: 1000));
            AddColumn("dbo.Job", "RejectReasonSTC", c => c.String(maxLength: 1000));
            AddColumn("dbo.Job", "RejectReasonELE", c => c.String(maxLength: 1000));
            AddColumn("dbo.Job", "RejectReasonCUCK", c => c.String(maxLength: 1000));
            AddColumn("dbo.Job", "RoofFacing", c => c.Int());
            AddColumn("dbo.Job", "ElectricianSignature", c => c.String(maxLength: 100));
            AddColumn("dbo.Job", "ElectricianSignatureDate", c => c.DateTime());
            AddColumn("dbo.Job", "DesignerSignature", c => c.String(maxLength: 100));
            AddColumn("dbo.Job", "DesignerSignatureDate", c => c.DateTime());
            AddColumn("dbo.Job", "Longitude_ElecSign", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "Latitude_ElecSign", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "Longitude_DesiSign", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "Latitude_DesiSign", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "CustomerIP", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "InstallerIP", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Job", "InstallerIP");
            DropColumn("dbo.Job", "CustomerIP");
            DropColumn("dbo.Job", "Latitude_DesiSign");
            DropColumn("dbo.Job", "Longitude_DesiSign");
            DropColumn("dbo.Job", "Latitude_ElecSign");
            DropColumn("dbo.Job", "Longitude_ElecSign");
            DropColumn("dbo.Job", "DesignerSignatureDate");
            DropColumn("dbo.Job", "DesignerSignature");
            DropColumn("dbo.Job", "ElectricianSignatureDate");
            DropColumn("dbo.Job", "ElectricianSignature");
            DropColumn("dbo.Job", "RoofFacing");
            DropColumn("dbo.Job", "RejectReasonCUCK");
            DropColumn("dbo.Job", "RejectReasonELE");
            DropColumn("dbo.Job", "RejectReasonSTC");
            DropColumn("dbo.Job", "RejectReasonPK");
            DropColumn("dbo.Job", "RejectReasonIS");
            DropColumn("dbo.Job", "RejectReasonCS");
            DropColumn("dbo.Job", "RejectReasonIM");
            DropColumn("dbo.Job", "RejectReasonIN");
            DropColumn("dbo.Job", "RejectReasonPA");
            DropColumn("dbo.Job", "RequestSignatureImageOwnerDate");
            DropColumn("dbo.Job", "RequestSignatureImageInstDate");
            DropColumn("dbo.Job", "CUCKVerifiedBy");
            DropColumn("dbo.Job", "IsComplaienceCUCK");
            DropColumn("dbo.Job", "ELEVerifiedBy");
            DropColumn("dbo.Job", "IsComplaienceELE");
            DropColumn("dbo.Job", "STCVerifiedBy");
            DropColumn("dbo.Job", "IsComplaienceSTC");
            DropColumn("dbo.Job", "PKVerifiedBy");
            DropColumn("dbo.Job", "IsComplaiencePK");
            DropColumn("dbo.Job", "ISVerifiedBy");
            DropColumn("dbo.Job", "IsComplaienceIS");
            DropColumn("dbo.Job", "CSVerifiedBy");
            DropColumn("dbo.Job", "IsComplaienceCS");
            DropColumn("dbo.Job", "IMVerifiedBy");
            DropColumn("dbo.Job", "ISComplaienceIM");
            DropColumn("dbo.Job", "INVerifiedBy");
            DropColumn("dbo.Job", "IsComplaienceIN");
            DropColumn("dbo.Job", "PAVerifiedBy");
            DropColumn("dbo.Job", "IsComplaiencePA");
            DropColumn("dbo.Job", "Latitude_InstSign");
            DropColumn("dbo.Job", "Longitude_InstSign");
            DropColumn("dbo.Job", "Latitude_CustSign");
            DropColumn("dbo.Job", "Longitude_CustSign");
            DropColumn("dbo.Job", "IsVerifyCUCK");
            DropColumn("dbo.Job", "IsVerifyELE");
            DropColumn("dbo.Job", "IsVerifyPK");
            DropColumn("dbo.Job", "IsVerifyIS");
            DropColumn("dbo.Job", "IsVerifyPA");
            DropColumn("dbo.Job", "IsVerifyIN");
            DropColumn("dbo.Job", "IsVerifyCS");
            DropColumn("dbo.Job", "IsRenew");
            DropColumn("dbo.Job", "IsReapply");
            DropColumn("dbo.Job", "InstallerPhoto");
            DropColumn("dbo.Job", "IsVerifyInverters");
            DropColumn("dbo.Job", "IsVerifyPanels");
            DropColumn("dbo.Job", "OwnerPhoto");
            DropColumn("dbo.Job", "IsVerifyImages");
            DropColumn("dbo.Job", "IsVerifySTC");
            DropColumn("dbo.Job", "SignatureImaegePickListDate");
            DropColumn("dbo.Job", "SignatureImagePickList");
            DropColumn("dbo.Job", "SignatureImageOwnerDate");
            DropColumn("dbo.Job", "SignatureImageInstDate");
            DropColumn("dbo.Job", "RECCreateDate");
            DropColumn("dbo.Job", "PVDStatus");
            DropColumn("dbo.Job", "PVDNumber");
            DropColumn("dbo.Job", "BulkUploadID");
            DropColumn("dbo.Job", "RECUploadDate");
            DropColumn("dbo.Job", "RECUploadFileName");
            DropColumn("dbo.Job", "SignatureImageOwner");
            DropColumn("dbo.Job", "SignatureImageInst");
            DropColumn("dbo.Job", "FormId");
        }
    }
}
