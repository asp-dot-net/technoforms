namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_photo_ImageName : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Photo", "ImageName", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Photo", "ImageName", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
