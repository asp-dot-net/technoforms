namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Job : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "Guid", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Job", "Guid");
        }
    }
}
