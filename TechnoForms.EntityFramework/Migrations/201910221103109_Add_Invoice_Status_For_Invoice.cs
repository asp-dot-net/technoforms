namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Invoice_Status_For_Invoice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invoice", "Invoice_Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Invoice", "Invoice_Status");
        }
    }
}
