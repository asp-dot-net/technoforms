namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Update_PhotoType : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.PhotoType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhotoTypeName = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PhotoType_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.PhotoType", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.PhotoType", "DeleterUserId", c => c.Long());
            AddColumn("dbo.PhotoType", "DeletionTime", c => c.DateTime());
            AddColumn("dbo.PhotoType", "LastModificationTime", c => c.DateTime());
            AddColumn("dbo.PhotoType", "LastModifierUserId", c => c.Long());
            AddColumn("dbo.PhotoType", "CreationTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.PhotoType", "CreatorUserId", c => c.Long());
            CreateIndex("dbo.PhotoType", "IsDeleted");
        }
        
        public override void Down()
        {
            DropIndex("dbo.PhotoType", new[] { "IsDeleted" });
            DropColumn("dbo.PhotoType", "CreatorUserId");
            DropColumn("dbo.PhotoType", "CreationTime");
            DropColumn("dbo.PhotoType", "LastModifierUserId");
            DropColumn("dbo.PhotoType", "LastModificationTime");
            DropColumn("dbo.PhotoType", "DeletionTime");
            DropColumn("dbo.PhotoType", "DeleterUserId");
            DropColumn("dbo.PhotoType", "IsDeleted");
            AlterTableAnnotations(
                "dbo.PhotoType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhotoTypeName = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_PhotoType_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
