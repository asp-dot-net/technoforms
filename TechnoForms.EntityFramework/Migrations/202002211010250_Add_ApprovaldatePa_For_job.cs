namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ApprovaldatePa_For_job : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "PAVerifiedDate", c => c.DateTime());
            AddColumn("dbo.Job", "INVerifiedDate", c => c.DateTime());
            AddColumn("dbo.Job", "IMVerifiedDate", c => c.DateTime());
            AddColumn("dbo.Job", "CSVerifiedDate", c => c.DateTime());
            AddColumn("dbo.Job", "ISVerifiedDate", c => c.DateTime());
            AddColumn("dbo.Job", "STCVerifiedDate", c => c.DateTime());
            AddColumn("dbo.Job", "ELEVerifiedDate", c => c.DateTime());
            AddColumn("dbo.Job", "CUCKVerifiedDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Job", "CUCKVerifiedDate");
            DropColumn("dbo.Job", "ELEVerifiedDate");
            DropColumn("dbo.Job", "STCVerifiedDate");
            DropColumn("dbo.Job", "ISVerifiedDate");
            DropColumn("dbo.Job", "CSVerifiedDate");
            DropColumn("dbo.Job", "IMVerifiedDate");
            DropColumn("dbo.Job", "INVerifiedDate");
            DropColumn("dbo.Job", "PAVerifiedDate");
        }
    }
}
