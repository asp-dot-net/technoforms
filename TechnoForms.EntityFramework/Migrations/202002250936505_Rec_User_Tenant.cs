namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Rec_User_Tenant : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.RecUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecUserName = c.String(nullable: false, maxLength: 50),
                        RecPasswprd = c.String(nullable: false, maxLength: 50),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_RecUser_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.RecUser", "TenantId", c => c.Int(nullable: false));
            CreateIndex("dbo.RecUser", "TenantId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.RecUser", new[] { "TenantId" });
            DropColumn("dbo.RecUser", "TenantId");
            AlterTableAnnotations(
                "dbo.RecUser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecUserName = c.String(nullable: false, maxLength: 50),
                        RecPasswprd = c.String(nullable: false, maxLength: 50),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_RecUser_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
