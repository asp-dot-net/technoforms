namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Job_STC_Fields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "STCAppliedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Job", "NumberOfCertificate", c => c.String());
            AddColumn("dbo.Job", "TotalSTC", c => c.String());
            AddColumn("dbo.Job", "RegistredSTC", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Job", "RegistredSTC");
            DropColumn("dbo.Job", "TotalSTC");
            DropColumn("dbo.Job", "NumberOfCertificate");
            DropColumn("dbo.Job", "STCAppliedDate");
        }
    }
}
