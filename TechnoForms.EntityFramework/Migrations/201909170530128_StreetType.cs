namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class StreetType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StreetType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        StreetTypeName = c.String(nullable: false, maxLength: 50),
                        StreetTypeCode = c.String(nullable: false, maxLength: 10),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_StreetType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.StreetType", new[] { "IsDeleted" });
            DropTable("dbo.StreetType",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_StreetType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
