namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InvitationStatusUpdate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InvitationStatus", "StatusCode", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InvitationStatus", "StatusCode");
        }
    }
}
