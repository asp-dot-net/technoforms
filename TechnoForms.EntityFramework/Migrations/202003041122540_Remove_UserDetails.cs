namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_UserDetails : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AbpUsers", "Dept_Id");
            DropColumn("dbo.AbpUsers", "Unit_Id");
            DropColumn("dbo.AbpUsers", "EmploymentType");
            DropColumn("dbo.AbpUsers", "ContractType");
            DropColumn("dbo.AbpUsers", "WindowsID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AbpUsers", "WindowsID", c => c.String());
            AddColumn("dbo.AbpUsers", "ContractType", c => c.String());
            AddColumn("dbo.AbpUsers", "EmploymentType", c => c.String());
            AddColumn("dbo.AbpUsers", "Unit_Id", c => c.Int(nullable: false));
            AddColumn("dbo.AbpUsers", "Dept_Id", c => c.Int(nullable: false));
        }
    }
}
