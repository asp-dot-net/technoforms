namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Update_OldSystemDirection : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.OldSystemDirection",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DirectionName = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_OldSystemDirection_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.OldSystemDirection", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.OldSystemDirection", "DeleterUserId", c => c.Long());
            AddColumn("dbo.OldSystemDirection", "DeletionTime", c => c.DateTime());
            AddColumn("dbo.OldSystemDirection", "LastModificationTime", c => c.DateTime());
            AddColumn("dbo.OldSystemDirection", "LastModifierUserId", c => c.Long());
            AddColumn("dbo.OldSystemDirection", "CreationTime", c => c.DateTime(nullable: false));
            AddColumn("dbo.OldSystemDirection", "CreatorUserId", c => c.Long());
            CreateIndex("dbo.OldSystemDirection", "IsDeleted");
        }
        
        public override void Down()
        {
            DropIndex("dbo.OldSystemDirection", new[] { "IsDeleted" });
            DropColumn("dbo.OldSystemDirection", "CreatorUserId");
            DropColumn("dbo.OldSystemDirection", "CreationTime");
            DropColumn("dbo.OldSystemDirection", "LastModifierUserId");
            DropColumn("dbo.OldSystemDirection", "LastModificationTime");
            DropColumn("dbo.OldSystemDirection", "DeletionTime");
            DropColumn("dbo.OldSystemDirection", "DeleterUserId");
            DropColumn("dbo.OldSystemDirection", "IsDeleted");
            AlterTableAnnotations(
                "dbo.OldSystemDirection",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DirectionName = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_OldSystemDirection_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
