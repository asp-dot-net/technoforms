namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class add_TenantId_CustomerSignLog : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.CustomerSignLog",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        JobId = c.Int(nullable: false),
                        JobGuId = c.Guid(nullable: false),
                        SenderUserId = c.Int(nullable: false),
                        TokenDate = c.DateTime(nullable: false),
                        TokenGuid = c.Guid(nullable: false),
                        Token = c.String(),
                        SendBy = c.String(),
                        Longitude = c.String(),
                        Latitude = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_CustomerSignLog_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Token",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        JobId = c.Int(nullable: false),
                        JobGuId = c.Guid(nullable: false),
                        TokenNo = c.String(nullable: false, maxLength: 100),
                        GuId = c.Guid(nullable: false),
                        IsExpired = c.Boolean(nullable: false),
                        IsOwner = c.Boolean(nullable: false),
                        IsSent = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Token_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.CustomerSignLog", "TenantId", c => c.Int(nullable: false));
            AddColumn("dbo.CustomerSignLog", "JobId", c => c.Int(nullable: false));
            AddColumn("dbo.CustomerSignLog", "JobGuId", c => c.Guid(nullable: false));
            AddColumn("dbo.Token", "TenantId", c => c.Int(nullable: false));
            AddColumn("dbo.Token", "JobId", c => c.Int(nullable: false));
            AddColumn("dbo.Token", "JobGuId", c => c.Guid(nullable: false));
            CreateIndex("dbo.CustomerSignLog", "TenantId");
            CreateIndex("dbo.Token", "TenantId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Token", new[] { "TenantId" });
            DropIndex("dbo.CustomerSignLog", new[] { "TenantId" });
            DropColumn("dbo.Token", "JobGuId");
            DropColumn("dbo.Token", "JobId");
            DropColumn("dbo.Token", "TenantId");
            DropColumn("dbo.CustomerSignLog", "JobGuId");
            DropColumn("dbo.CustomerSignLog", "JobId");
            DropColumn("dbo.CustomerSignLog", "TenantId");
            AlterTableAnnotations(
                "dbo.Token",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        JobId = c.Int(nullable: false),
                        JobGuId = c.Guid(nullable: false),
                        TokenNo = c.String(nullable: false, maxLength: 100),
                        GuId = c.Guid(nullable: false),
                        IsExpired = c.Boolean(nullable: false),
                        IsOwner = c.Boolean(nullable: false),
                        IsSent = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Token_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.CustomerSignLog",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        JobId = c.Int(nullable: false),
                        JobGuId = c.Guid(nullable: false),
                        SenderUserId = c.Int(nullable: false),
                        TokenDate = c.DateTime(nullable: false),
                        TokenGuid = c.Guid(nullable: false),
                        Token = c.String(),
                        SendBy = c.String(),
                        Longitude = c.String(),
                        Latitude = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_CustomerSignLog_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
