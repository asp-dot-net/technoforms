namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ScanedPanel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ScanedPanel",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PanelSerialNumber = c.String(nullable: false, maxLength: 50),
                        Guid = c.Guid(nullable: false),
                        IsScan = c.Boolean(nullable: false),
                        PanelBarcodePhoto = c.String(nullable: false, maxLength: 100),
                        Longitude = c.String(nullable: false, maxLength: 50),
                        Latitude = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ScanedPanel_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ScanedPanel", new[] { "IsDeleted" });
            DropTable("dbo.ScanedPanel",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ScanedPanel_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
