namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_InvoiceGenereted_For_Job : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "InvoiceGenereted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Job", "InvoiceGenereted");
        }
    }
}
