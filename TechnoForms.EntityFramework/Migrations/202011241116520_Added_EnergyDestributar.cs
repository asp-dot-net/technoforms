namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Added_EnergyDestributar : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EnergyDestributar",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        NSW = c.Boolean(nullable: false),
                        SA = c.Boolean(nullable: false),
                        QLD = c.Boolean(nullable: false),
                        VIC = c.Boolean(nullable: false),
                        WA = c.Boolean(nullable: false),
                        ACT = c.Boolean(nullable: false),
                        TAS = c.Boolean(nullable: false),
                        NT = c.Boolean(nullable: false),
                        ElectDistABB = c.String(),
                        ElecDistAppReq = c.Boolean(nullable: false),
                        GreenBoatDistributor = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EnergyDestributar_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.EnergyDestributar", new[] { "IsDeleted" });
            DropTable("dbo.EnergyDestributar",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_EnergyDestributar_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
