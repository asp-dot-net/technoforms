namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ResponsibleSupplier_for_Panel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Panel", "ResponsibleSupplier", c => c.String());
            AddColumn("dbo.Panel", "ResponsibleSupplierABN", c => c.String());
            AddColumn("dbo.Panel", "ResponsibleSupplier2", c => c.String());
            AddColumn("dbo.Panel", "ResponsibleSupplierABN2", c => c.String());
            AddColumn("dbo.Panel", "ResponsibleSupplier3", c => c.String());
            AddColumn("dbo.Panel", "ResponsibleSupplierABN3", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Panel", "ResponsibleSupplierABN3");
            DropColumn("dbo.Panel", "ResponsibleSupplier3");
            DropColumn("dbo.Panel", "ResponsibleSupplierABN2");
            DropColumn("dbo.Panel", "ResponsibleSupplier2");
            DropColumn("dbo.Panel", "ResponsibleSupplierABN");
            DropColumn("dbo.Panel", "ResponsibleSupplier");
        }
    }
}
