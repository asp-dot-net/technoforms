namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class PostCode : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostCode",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Postcode = c.String(nullable: false, maxLength: 50),
                        Suburb = c.String(nullable: false, maxLength: 50),
                        POBoxes = c.String(maxLength: 50),
                        Area = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                        State_Id = c.Int(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PostCode_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.State", t => t.State_Id)
                .Index(t => t.IsDeleted)
                .Index(t => t.State_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostCode", "State_Id", "dbo.State");
            DropIndex("dbo.PostCode", new[] { "State_Id" });
            DropIndex("dbo.PostCode", new[] { "IsDeleted" });
            DropTable("dbo.PostCode",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PostCode_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
