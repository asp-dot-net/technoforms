namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Upgraded_To_Abp_v3_8_1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.AppFriendships", "FriendUserName", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.AbpUsers", "UserName", c => c.String(nullable: false, maxLength: 256));
            AlterColumn("dbo.AbpUserAccounts", "UserName", c => c.String(maxLength: 256));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AbpUserAccounts", "UserName", c => c.String(maxLength: 32));
            AlterColumn("dbo.AbpUsers", "UserName", c => c.String(nullable: false, maxLength: 32));
            AlterColumn("dbo.AppFriendships", "FriendUserName", c => c.String(nullable: false, maxLength: 32));
        }
    }
}
