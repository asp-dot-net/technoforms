﻿using System.Linq;
using TechnoForms.Editions;
using TechnoForms.EntityFramework;
using TechnoForms.Jobs;
using TechnoForms.JobStatusName;
using TechnoForms.ModeOfPayments;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;
using TechnoForms.StreetName;
using TechnoForms.PhotoTypes;

namespace TechnoForms.Migrations.Seed.Tenants
{
    public class DefaultTenantBuilder
    {
        private readonly TechnoFormsDbContext _context;

        public DefaultTenantBuilder(TechnoFormsDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateDefaultTenant();
            CreateState();
            CreatePostalAddressType();
            CreatePostalDeliveryType();
            CreateJobStatus();
            CreateHouseType();
            CreateRoofType();
            CreateSolarType();
            CreateSystemType();
            CreateRoofFacingPanel();
            CreateYesNo();
            CreateTypeOfSystem();
            CreateTypeOfConnection();
            CreateMountingType();
            CreateRECYear();
            //CreateUnitType();
            //CreateStreetType();
            //CreateStreetName();
            //CreatePostalType();
            CreateSystemMoutingType();
            CreateModeOfPayment();
            CreatePhotoType();
            CreateOldSystemDairection();
            CreateSTCZoneRating();
        }
        private void CreateDefaultTenant()
        {
            //Default tenant

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName == MultiTenancy.Tenant.DefaultTenantName);
            if (defaultTenant == null)
            {
                defaultTenant = new MultiTenancy.Tenant(MultiTenancy.Tenant.DefaultTenantName, MultiTenancy.Tenant.DefaultTenantName);

                var defaultEdition = _context.Editions.FirstOrDefault(e => e.Name == EditionManager.DefaultEditionName);
                if (defaultEdition != null)
                {
                    defaultTenant.EditionId = defaultEdition.Id;
                }
                defaultTenant.CompanyName = "Default";
                defaultTenant.FullName = "Default";
                defaultTenant.ABN = "1232323";
                defaultTenant.FAX = "323213213";
                defaultTenant.Phone = "23123123213";
                defaultTenant.Mobile = "23123123";
                defaultTenant.RetailerABN = "Default";
                defaultTenant.RetailerName = "Default";
                defaultTenant.STCCharges = 0;
                defaultTenant.Email = "test@test.com";
                
                defaultTenant.StreetAddress = "44 CAMPBELL DRIVE";
                defaultTenant.UnitNo = "Default";
                defaultTenant.UnitType = "Default";
                defaultTenant.StreetNumber = "44";
                defaultTenant.StreetName = "CAMPBELL";
                defaultTenant.StreetType = "DRIVE";
                defaultTenant.Suburb = "MANGO HILL";
                defaultTenant.Suburb_Id = 9503;
                defaultTenant.State = "QLD";
                defaultTenant.PostCode = "4509";


                _context.Tenants.Add(defaultTenant);
                _context.SaveChanges();
            }


        }
        private void CreateState()
        {
            var ACT = _context.State.FirstOrDefault(t => t.StateName == "ACT");
            if (ACT == null)
            {
                _context.State.Add(
              new States.State
              {
                  StateName = "ACT"
              });
            }

            var NSW = _context.State.FirstOrDefault(t => t.StateName == "NSW");
            if (NSW == null)
            {
                _context.State.Add(
              new States.State
              {
                  StateName = "NSW"
              });
            }

            var NT = _context.State.FirstOrDefault(t => t.StateName == "NT");
            if (NT == null)
            {
                _context.State.Add(
              new States.State
              {
                  StateName = "NT"
              });
            }

            var QLD = _context.State.FirstOrDefault(t => t.StateName == "QLD");
            if (QLD == null)
            {
                _context.State.Add(
              new States.State
              {
                  StateName = "QLD"
              });
            }

            var SA = _context.State.FirstOrDefault(t => t.StateName == "SA");
            if (SA == null)
            {
                _context.State.Add(
              new States.State
              {
                  StateName = "SA"
              });
            }

            var TAS = _context.State.FirstOrDefault(t => t.StateName == "TAS");
            if (TAS == null)
            {
                _context.State.Add(
              new States.State
              {
                  StateName = "TAS"
              });
            }

            var VIC = _context.State.FirstOrDefault(t => t.StateName == "VIC");
            if (VIC == null)
            {
                _context.State.Add(
              new States.State
              {
                  StateName = "VIC"
              });
            }

            var WA = _context.State.FirstOrDefault(t => t.StateName == "WA");
            if (WA == null)
            {
                _context.State.Add(
              new States.State
              {
                  StateName = "WA"
              });
            }

        }
        private void CreatePostalAddressType()
        {
            var Physical = _context.PostalAddressType.FirstOrDefault(t => t.PostalAddressTypeName == "Physical");
            if (Physical == null)
            {
                _context.PostalAddressType.Add(
              new PostalAddressTypes.PostalAddressType
              {
                  PostalAddressTypeName = "Physical"
              });
            }

            var Postal = _context.PostalAddressType.FirstOrDefault(t => t.PostalAddressTypeName == "Postal");
            if (Postal == null)
            {
                _context.PostalAddressType.Add(
              new PostalAddressTypes.PostalAddressType
              {
                  PostalAddressTypeName = "Postal"
              });
            }

        }
        private void CreatePostalDeliveryType()
        {
            var CAREPO = _context.PostalDeliveryType.FirstOrDefault(t => t.PostalDeliveryTypeName == "CARE PO");
            if (CAREPO == null)
            {
                _context.PostalDeliveryType.Add(
              new PostalDeliveryTypes.PostalDeliveryType
              {
                  PostalDeliveryTypeName = "CARE PO"
              });
            }

            var CMA = _context.PostalDeliveryType.FirstOrDefault(t => t.PostalDeliveryTypeName == "CMA");
            if (CMA == null)
            {
                _context.PostalDeliveryType.Add(
              new PostalDeliveryTypes.PostalDeliveryType
              {
                  PostalDeliveryTypeName = "CMA"
              });
            }

            var CMB = _context.PostalDeliveryType.FirstOrDefault(t => t.PostalDeliveryTypeName == "CMB");
            if (CMB == null)
            {
                _context.PostalDeliveryType.Add(
              new PostalDeliveryTypes.PostalDeliveryType
              {
                  PostalDeliveryTypeName = "CMB"
              });
            }

            var GPOBOX = _context.PostalDeliveryType.FirstOrDefault(t => t.PostalDeliveryTypeName == "GPO BOX");
            if (GPOBOX == null)
            {
                _context.PostalDeliveryType.Add(
              new PostalDeliveryTypes.PostalDeliveryType
              {
                  PostalDeliveryTypeName = "GPO BOX"
              });
            }

            var LOCKEDBAG = _context.PostalDeliveryType.FirstOrDefault(t => t.PostalDeliveryTypeName == "LOCKED BAG");
            if (LOCKEDBAG == null)
            {
                _context.PostalDeliveryType.Add(
              new PostalDeliveryTypes.PostalDeliveryType
              {
                  PostalDeliveryTypeName = "LOCKED BAG"
              });
            }

            var MS = _context.PostalDeliveryType.FirstOrDefault(t => t.PostalDeliveryTypeName == "MS");
            if (MS == null)
            {
                _context.PostalDeliveryType.Add(
              new PostalDeliveryTypes.PostalDeliveryType
              {
                  PostalDeliveryTypeName = "MS"
              });
            }

            var POBOX = _context.PostalDeliveryType.FirstOrDefault(t => t.PostalDeliveryTypeName == "PO BOX");
            if (POBOX == null)
            {
                _context.PostalDeliveryType.Add(
              new PostalDeliveryTypes.PostalDeliveryType
              {
                  PostalDeliveryTypeName = "PO BOX"
              });
            }

            var PRIVATEBAG = _context.PostalDeliveryType.FirstOrDefault(t => t.PostalDeliveryTypeName == "PRIVATE BAG");
            if (PRIVATEBAG == null)
            {
                _context.PostalDeliveryType.Add(
              new PostalDeliveryTypes.PostalDeliveryType
              {
                  PostalDeliveryTypeName = "PRIVATE BAG"
              });
            }

            var RMB = _context.PostalDeliveryType.FirstOrDefault(t => t.PostalDeliveryTypeName == "RMB");
            if (RMB == null)
            {
                _context.PostalDeliveryType.Add(
              new PostalDeliveryTypes.PostalDeliveryType
              {
                  PostalDeliveryTypeName = "RMB"
              });
            }

            var RMC = _context.PostalDeliveryType.FirstOrDefault(t => t.PostalDeliveryTypeName == "RMC");
            if (RMC == null)
            {
                _context.PostalDeliveryType.Add(
              new PostalDeliveryTypes.PostalDeliveryType
              {
                  PostalDeliveryTypeName = "RMC"
              });
            }

            var RSD = _context.PostalDeliveryType.FirstOrDefault(t => t.PostalDeliveryTypeName == "RSD");
            if (RSD == null)
            {
                _context.PostalDeliveryType.Add(
              new PostalDeliveryTypes.PostalDeliveryType
              {
                  PostalDeliveryTypeName = "RSD"
              });
            }
        }
        private void CreateJobStatus()
        {
            var InProgress = _context.JobStatus.FirstOrDefault(t => t.JobStatusName == "In Progress");
            if (InProgress == null)
            {
                _context.JobStatus.Add(
              new JobStatusName.JobStatus
              {
                  JobStatusName = "In Progress"
              });
            }

            var Completed = _context.JobStatus.FirstOrDefault(t => t.JobStatusName == "Completed");
            if (Completed == null)
            {
                _context.JobStatus.Add(
              new JobStatusName.JobStatus
              {
                  JobStatusName = "Completed"
              });
            }

            var Pending = _context.JobStatus.FirstOrDefault(t => t.JobStatusName == "Pending");
            if (Pending == null)
            {
                _context.JobStatus.Add(
              new JobStatusName.JobStatus
              {
                  JobStatusName = "Pending"
              });
            }
        }
        private void CreateHouseType()
        {
            var DoubleStory = _context.HouseType.FirstOrDefault(t => t.HouseTypeName == "Double Story");
            if (DoubleStory == null)
            {
                _context.HouseType.Add(
              new Jobs.HouseType
              {
                  HouseTypeName = "Double Story"
              });
            }

            var MultiStory = _context.HouseType.FirstOrDefault(t => t.HouseTypeName == "Multi Story");
            if (MultiStory == null)
            {
                _context.HouseType.Add(
              new Jobs.HouseType
              {
                  HouseTypeName = "Multi Story"
              });
            }
            var SingleStory = _context.HouseType.FirstOrDefault(t => t.HouseTypeName == "Single Story");
            if (SingleStory == null)
            {
                _context.HouseType.Add(
              new Jobs.HouseType
              {
                  HouseTypeName = "Single Story"
              });
            }
        }
        private void CreateRoofType()
        {
            var Asbostos = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Asbostos");
            if (Asbostos == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Asbostos"
              });
            }

            var ClipLok = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Clip Lok");
            if (ClipLok == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Clip Lok"
              });
            }
            var Corrugated = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Corrugated");
            if (Corrugated == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Corrugated"
              });
            }
            var Decramastic = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Decramastic");
            if (Decramastic == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Decramastic"
              });
            }
            var FlatClipLock = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Flat ClipLock");
            if (FlatClipLock == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Flat ClipLock"
              });
            }
            var SHED = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "SHEDk");
            if (SHED == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "SHED"
              });
            }
            var TerracottaTiles = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Terracotta Tiles");
            if (TerracottaTiles == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Terracotta Tiles"
              });
            }
            var TileTinFlat = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Tile + Tin Flat");
            if (TileTinFlat == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Tile + Tin Flat"
              });
            }
            var TileRoof = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Tile Roof");
            if (TileRoof == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Tile Roof"
              });
            }
            var TinTile = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Tin + Tile");
            if (TinTile == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Tin + Tile"
              });
            }
            var TinFlat = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Tin Flat");
            if (TinFlat == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Tin Flat"
              });
            }
            var TinPitchedFlat = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Tin Pitched + Flat");
            if (TinPitchedFlat == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Tin Pitched + Flat"
              });
            }
            var TinRoof = _context.RoofType.FirstOrDefault(t => t.RoofTypeName == "Tin Roof");
            if (TinRoof == null)
            {
                _context.RoofType.Add(
              new Jobs.RoofType
              {
                  RoofTypeName = "Tin Roof"
              });
            }
        }
        private void CreateSolarType()
        {
            var Domestic = _context.SolarType.FirstOrDefault(t => t.SolarTypeeName == "Domestic");
            if (Domestic == null)
            {
                _context.SolarType.Add(
              new Jobs.SolarType
              {
                  SolarTypeeName = "Domestic"
              });
            }

            var Commercial = _context.SolarType.FirstOrDefault(t => t.SolarTypeeName == "Commercial");
            if (Commercial == null)
            {
                _context.SolarType.Add(
              new Jobs.SolarType
              {
                  SolarTypeeName = "Commercial"
              });
            }
        }
        private void CreateSystemType()
        {
            var FirstTime = _context.SystemType.FirstOrDefault(t => t.SystemTypeName == "First Time");
            if (FirstTime == null)
            {
                _context.SystemType.Add(
              new Jobs.SystemType
              {
                  SystemTypeName = "First Time"
              });
            }

            var Additional = _context.SystemType.FirstOrDefault(t => t.SystemTypeName == "Additional");
            if (Additional == null)
            {
                _context.SystemType.Add(
              new Jobs.SystemType
              {
                  SystemTypeName = "Additional"
              });
            }

            var Maintainance = _context.SystemType.FirstOrDefault(t => t.SystemTypeName == "Maintainance");
            if (Maintainance == null)
            {
                _context.SystemType.Add(
              new Jobs.SystemType
              {
                  SystemTypeName = "Maintainance"
              });
            }
        }
        private void CreateRoofFacingPanel()
        {
            var North = _context.RoofFacingPanel.FirstOrDefault(t => t.RoofFacingPanelName == "North");
            if (North == null)
            {
                _context.RoofFacingPanel.Add(
              new Jobs.RoofFacingPanel
              {
                  RoofFacingPanelName = "North"
              });
            }

            var NorthEast = _context.RoofFacingPanel.FirstOrDefault(t => t.RoofFacingPanelName == "North-East");
            if (NorthEast == null)
            {
                _context.RoofFacingPanel.Add(
              new Jobs.RoofFacingPanel
              {
                  RoofFacingPanelName = "North-East"
              });
            }

            var East = _context.RoofFacingPanel.FirstOrDefault(t => t.RoofFacingPanelName == "East");
            if (East == null)
            {
                _context.RoofFacingPanel.Add(
              new Jobs.RoofFacingPanel
              {
                  RoofFacingPanelName = "East"
              });
            }

            var SouthEast = _context.RoofFacingPanel.FirstOrDefault(t => t.RoofFacingPanelName == "South-East");
            if (SouthEast == null)
            {
                _context.RoofFacingPanel.Add(
              new Jobs.RoofFacingPanel
              {
                  RoofFacingPanelName = "South-East"
              });
            }

            var South = _context.RoofFacingPanel.FirstOrDefault(t => t.RoofFacingPanelName == "South");
            if (South == null)
            {
                _context.RoofFacingPanel.Add(
              new Jobs.RoofFacingPanel
              {
                  RoofFacingPanelName = "South"
              });
            }

            var SouthWest = _context.RoofFacingPanel.FirstOrDefault(t => t.RoofFacingPanelName == "South-West");
            if (SouthWest == null)
            {
                _context.RoofFacingPanel.Add(
              new Jobs.RoofFacingPanel
              {
                  RoofFacingPanelName = "South-West"
              });
            }

            var West = _context.RoofFacingPanel.FirstOrDefault(t => t.RoofFacingPanelName == "West");
            if (West == null)
            {
                _context.RoofFacingPanel.Add(
              new Jobs.RoofFacingPanel
              {
                  RoofFacingPanelName = "West"
              });
            }

            var NorthWest = _context.RoofFacingPanel.FirstOrDefault(t => t.RoofFacingPanelName == "North-West");
            if (NorthWest == null)
            {
                _context.RoofFacingPanel.Add(
              new Jobs.RoofFacingPanel
              {
                  RoofFacingPanelName = "North-West"
              });
            }
        }
        private void CreateYesNo()
        {
            var Yes = _context.YesNo.FirstOrDefault(t => t.YesNoName == "Yes");
            if (Yes == null)
            {
                _context.YesNo.Add(
              new Jobs.YesNo
              {
                  YesNoName = "Yes"
              });
            }

            var No = _context.YesNo.FirstOrDefault(t => t.YesNoName == "No");
            if (No == null)
            {
                _context.YesNo.Add(
              new Jobs.YesNo
              {
                  YesNoName = "No"
              });
            }
        }
        private void CreateTypeOfSystem()
        {
            var Solar = _context.TypeOfSystem.FirstOrDefault(t => t.TypeOfSystemName == "S.G.U. - Solar (Deemed)");
            if (Solar == null)
            {
                _context.TypeOfSystem.Add(
              new Jobs.TypeOfSystem
              {
                  TypeOfSystemName = "S.G.U. - Solar (Deemed)"
              });
            }

            var Wind = _context.TypeOfSystem.FirstOrDefault(t => t.TypeOfSystemName == "S.G.U. - Wind (Deemed)");
            if (Wind == null)
            {
                _context.TypeOfSystem.Add(
              new Jobs.TypeOfSystem
              {
                  TypeOfSystemName = "S.G.U. - Wind (Deemed)"
              });
            }
        }
        private void CreateTypeOfConnection()
        {
            var batterystorage = _context.TypeOfConnection.FirstOrDefault(t => t.TypeOfConnectionName == "Connected to an electricity grid with battery storage");
            if (batterystorage == null)
            {
                _context.TypeOfConnection.Add(
              new Jobs.TypeOfConnection
              {
                  TypeOfConnectionName = "Connected to an electricity grid with battery storage"
              });
            }

            var withoutbatterystorage = _context.TypeOfConnection.FirstOrDefault(t => t.TypeOfConnectionName == "Connected to an electricity grid without battery storage");
            if (withoutbatterystorage == null)
            {
                _context.TypeOfConnection.Add(
              new Jobs.TypeOfConnection
              {
                  TypeOfConnectionName = "Connected to an electricity grid without battery storage"
              });
            }

            var Standalone = _context.TypeOfConnection.FirstOrDefault(t => t.TypeOfConnectionName == "Stand-alone (not connected to an electricity grid)");
            if (Standalone == null)
            {
                _context.TypeOfConnection.Add(
              new Jobs.TypeOfConnection
              {
                  TypeOfConnectionName = "Stand-alone (not connected to an electricity grid)"
              });
            }
        }
        private void CreateMountingType()
        {
            var Building = _context.MountingType.FirstOrDefault(t => t.MountingTypeName == "Building or structure");
            if (Building == null)
            {
                _context.MountingType.Add(
              new Jobs.MountingType
              {
                  MountingTypeName = "Building or structure"
              });
            }

            var Groundmounted = _context.MountingType.FirstOrDefault(t => t.MountingTypeName == "Ground mounted or free standing");
            if (Groundmounted == null)
            {
                _context.MountingType.Add(
              new Jobs.MountingType
              {
                  MountingTypeName = "Ground mounted or free standing"
              });
            }
        }
        private void CreateRECYear()
        {
            var OneYear = _context.RecYear.FirstOrDefault(t => t.Year == 2030);
            if (OneYear == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2030,
                  REC_Year = "One year",
                  STCRate = (decimal)1.00
              });
            }

            var TwoYears = _context.RecYear.FirstOrDefault(t => t.Year == 2029);
            if (TwoYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2029,
                  REC_Year = "Two years",
                  STCRate = (decimal)2.00
              });
            }

            var ThreeYears = _context.RecYear.FirstOrDefault(t => t.Year == 2028);
            if (ThreeYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2028,
                  REC_Year = "Three years",
                  STCRate = (decimal)3.00
              });
            }

            var FourYears = _context.RecYear.FirstOrDefault(t => t.Year == 2027);
            if (FourYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2027,
                  REC_Year = "Four years",
                  STCRate = (decimal)4.00
              });
            }

            var FiveYears = _context.RecYear.FirstOrDefault(t => t.Year == 2026);
            if (FiveYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2026,
                  REC_Year = "Five years",
                  STCRate = (decimal)5.00
              });
            }

            var SixYears = _context.RecYear.FirstOrDefault(t => t.Year == 2025);
            if (SixYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2025,
                  REC_Year = "Six years",
                  STCRate = (decimal)6.00
              });
            }

            var SevenYears = _context.RecYear.FirstOrDefault(t => t.Year == 2024);
            if (SevenYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2024,
                  REC_Year = "Seven years",
                  STCRate = (decimal)7.00
              });
            }

            var EightYears = _context.RecYear.FirstOrDefault(t => t.Year == 2023);
            if (EightYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2023,
                  REC_Year = "Eight years",
                  STCRate = (decimal)8.00
              });
            }

            var NineYears = _context.RecYear.FirstOrDefault(t => t.Year == 2022);
            if (NineYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2022,
                  REC_Year = "Nine years",
                  STCRate = (decimal)9.00
              });
            }

            var TenYears = _context.RecYear.FirstOrDefault(t => t.Year == 2021);
            if (TenYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2021,
                  REC_Year = "Ten years",
                  STCRate = (decimal)10.00
              });
            }

            var ElevenYears = _context.RecYear.FirstOrDefault(t => t.Year == 2020);
            if (ElevenYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2020,
                  REC_Year = "Eleven years",
                  STCRate = (decimal)11.00
              });
            }

            var TwelveYears = _context.RecYear.FirstOrDefault(t => t.Year == 2019);
            if (TwelveYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2019,
                  REC_Year = "Twelve years",
                  STCRate = (decimal)12.00
              });
            }

            var ThirteenYears = _context.RecYear.FirstOrDefault(t => t.Year == 2018);
            if (ThirteenYears == null)
            {
                _context.RecYear.Add(
              new Jobs.RecYear
              {
                  Year = 2018,
                  REC_Year = "Thirteen years",
                  STCRate = (decimal)13.00
              });
            }
        }
        //private void CreateUnitType()
        //{
        //    var WORKSHOP = _context.UnitType.FirstOrDefault(t => t.UnitTypeName == "WORKSHOP");
        //    if (WORKSHOP == null)
        //    {
        //        _context.UnitType.Add(
        //      new UnitTypes.UnitType
        //      {
        //          UnitTypeName = "WORKSHOP"
        //      });
        //    }
        //    var WAREHOUSE = _context.UnitType.FirstOrDefault(t => t.UnitTypeName == "WAREHOUSE");
        //    if (WAREHOUSE == null)
        //    {
        //        _context.UnitType.Add(
        //      new UnitTypes.UnitType
        //      {
        //          UnitTypeName = "WAREHOUSE"
        //      });
        //    }
        //    var WARD = _context.UnitType.FirstOrDefault(t => t.UnitTypeName == "WARD");
        //    if (WARD == null)
        //    {
        //        _context.UnitType.Add(
        //      new UnitTypes.UnitType
        //      {
        //          UnitTypeName = "WARD"
        //      });
        //    }
        //    var VAULT = _context.UnitType.FirstOrDefault(t => t.UnitTypeName == "VAULT");
        //    if (VAULT == null)
        //    {
        //        _context.UnitType.Add(
        //      new UnitTypes.UnitType
        //      {
        //          UnitTypeName = "VAULT"
        //      });
        //    }
        //    var VILLA = _context.UnitType.FirstOrDefault(t => t.UnitTypeName == "VILLA");
        //    if (VILLA == null)
        //    {
        //        _context.UnitType.Add(
        //      new UnitTypes.UnitType
        //      {
        //          UnitTypeName = "VILLA"
        //      });
        //    }
        //}
        //private void CreateStreetType()
        //{
        //    var STATION = _context.StreetType.FirstOrDefault(t => t.StreetTypeName == "STATION");
        //    if (STATION == null)
        //    {
        //        _context.StreetType.Add(
        //      new StreetTypes.StreetType
        //      {
        //          StreetTypeName = "STATION",
        //          StreetTypeCode = "STAT"
        //      });
        //    }
        //    var WYND = _context.StreetType.FirstOrDefault(t => t.StreetTypeName == "WYND");
        //    if (WYND == null)
        //    {
        //        _context.StreetType.Add(
        //      new StreetTypes.StreetType
        //      {
        //          StreetTypeName = "WYND",
        //          StreetTypeCode = "WYND"
        //      });
        //    }
        //    var WHARF = _context.StreetType.FirstOrDefault(t => t.StreetTypeName == "WHARF");
        //    if (WHARF == null)
        //    {
        //        _context.StreetType.Add(
        //      new StreetTypes.StreetType
        //      {
        //          StreetTypeName = "WHARF",
        //          StreetTypeCode = "WHRF"
        //      });
        //    }
        //    var WAY = _context.StreetType.FirstOrDefault(t => t.StreetTypeName == "WAY");
        //    if (WAY == null)
        //    {
        //        _context.StreetType.Add(
        //      new StreetTypes.StreetType
        //      {
        //          StreetTypeName = "WAY",
        //          StreetTypeCode = "WAY"
        //      });
        //    }
        //    var WATERS = _context.StreetType.FirstOrDefault(t => t.StreetTypeName == "WATERS");
        //    if (WATERS == null)
        //    {
        //        _context.StreetType.Add(
        //      new StreetTypes.StreetType
        //      {
        //          StreetTypeName = "WATERS",
        //          StreetTypeCode = "WTRS"
        //      });
        //    }
        //}
        //private void CreateStreetName()
        //{
        //    var FRANKEN = _context.StreetName.FirstOrDefault(t => t.StreetName == "FRANKEN");
        //    if (FRANKEN == null)
        //    {
        //        _context.StreetName.Add(
        //      new StreetNames
        //      {
        //          StreetName = "FRANKEN"
        //      });
        //    }
        //    var EASTQUAY = _context.StreetName.FirstOrDefault(t => t.StreetName == "EAST QUAY");
        //    if (EASTQUAY == null)
        //    {
        //        _context.StreetName.Add(
        //      new StreetNames
        //      {
        //          StreetName = "EAST QUAY"
        //      });
        //    }
        //    var AURTHUR = _context.StreetName.FirstOrDefault(t => t.StreetName == "AURTHUR");
        //    if (AURTHUR == null)
        //    {
        //        _context.StreetName.Add(
        //      new StreetNames
        //      {
        //          StreetName = "AURTHUR"
        //      });
        //    }
        //    var BENLEXCEN = _context.StreetName.FirstOrDefault(t => t.StreetName == "BEN LEXCEN");
        //    if (BENLEXCEN == null)
        //    {
        //        _context.StreetName.Add(
        //      new StreetNames
        //      {
        //          StreetName = "BEN LEXCEN"
        //      });
        //    }
        //    var VIEWLINE = _context.StreetName.FirstOrDefault(t => t.StreetName == "VIEWLINE");
        //    if (VIEWLINE == null)
        //    {
        //        _context.StreetName.Add(
        //      new StreetNames
        //      {
        //          StreetName = "VIEWLINE"
        //      });
        //    }
        //}
        //private void CreatePostalType()
        //{
        //    var RMC = _context.PostalType.FirstOrDefault(t => t.PostalTypeCode == "RMC");
        //    if (RMC == null)
        //    {
        //        _context.PostalType.Add(
        //      new Postaltypes.PostalType
        //      {
        //          PostalTypeCode = "RMC",
        //          PostalTypeName = "ROADSIDE DELIVERY"
        //      });
        //    }
        //    var RMB = _context.PostalType.FirstOrDefault(t => t.PostalTypeCode == "RMB");
        //    if (RMB == null)
        //    {
        //        _context.PostalType.Add(
        //      new Postaltypes.PostalType
        //      {
        //          PostalTypeCode = "RMB",
        //          PostalTypeName = "ROADSIDE DELIVERY"
        //      });
        //    }
        //    var RSD = _context.PostalType.FirstOrDefault(t => t.PostalTypeCode == "RSD");
        //    if (RSD == null)
        //    {
        //        _context.PostalType.Add(
        //      new Postaltypes.PostalType
        //      {
        //          PostalTypeCode = "RSD",
        //          PostalTypeName = "MAIL SERVICE"
        //      });
        //    }
        //}
        private void CreateSystemMoutingType()
        {
            var Individual = _context.SystemMountingType.FirstOrDefault(t => t.SystemMountingTypeName == "Individual");
            if (Individual == null)
            {
                _context.SystemMountingType.Add(
              new SystemMountingType
              {
                  SystemMountingTypeName = "Individual",
              });
            }

            var Corporatebody = _context.SystemMountingType.FirstOrDefault(t => t.SystemMountingTypeName == "Corporate body");
            if (Corporatebody == null)
            {
                _context.SystemMountingType.Add(
              new SystemMountingType
              {
                  SystemMountingTypeName = "Corporate body",
              });
            }

            var Governmentbody = _context.SystemMountingType.FirstOrDefault(t => t.SystemMountingTypeName == "Government body");
            if (Governmentbody == null)
            {
                _context.SystemMountingType.Add(
              new SystemMountingType
              {
                  SystemMountingTypeName = "Government body",
              });
            }

            var Trustee = _context.SystemMountingType.FirstOrDefault(t => t.SystemMountingTypeName == "Trustee");
            if (Trustee == null)
            {
                _context.SystemMountingType.Add(
              new SystemMountingType
              {
                  SystemMountingTypeName = "Trustee",
              });
            }
        }
        private void CreateModeOfPayment()
        {
            var Cash = _context.ModeOfPayment.FirstOrDefault(t => t.ModeOfPaymentName == "Cash");
            if (Cash == null)
            {
                _context.ModeOfPayment.Add(
              new ModeOfPayment
              {
                  ModeOfPaymentName = "Cash",
              });
            }

            var Cheque = _context.ModeOfPayment.FirstOrDefault(t => t.ModeOfPaymentName == "Cheque");
            if (Cheque == null)
            {
                _context.ModeOfPayment.Add(
              new ModeOfPayment
              {
                  ModeOfPaymentName = "Cheque",
              });
            }
        }
        private void CreatePhotoType()
        {
            var Cheque = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Cheque or EFT Receipt");
            if (Cheque == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Cheque or EFT Receipt",
              });
            }
            var MeterBoxBefore = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "MeterBox Before Installation");
            if (MeterBoxBefore == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "MeterBox Before Installation",
              });
            }
            var MeterboxAfter = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Meterbox After Installation");
            if (MeterboxAfter == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Meterbox After Installation",
              });
            }
            var Front = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Front of House");
            if (Front == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Front of House",
              });
            }
            var Switch = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Switch Board Opened");
            if (Switch == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Switch Board Opened",
              });
            }
            var Inverter = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "The Inverter Installed ");
            if (Inverter == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "The Inverter Installed ",
              });
            }
            var Sketch = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Installation Sketch ");
            if (Sketch == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Installation Sketch ",
              });
            }
            var Roof = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Panel Installed on Roof ");
            if (Roof == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Panel Installed on Roof ",
              });
            }
            var DC = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "DC Isolator installed");
            if (DC == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "DC Isolator installed",
              });
            }
            var Payment = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Payment");
            if (Payment == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Payment",
              });
            }
            var Sys = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Sys Split");
            if (Sys == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Sys Split",
              });
            }
            var Others = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Others");
            if (Others == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Others",
              });
            }
            var Customer = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Customer Signature");
            if (Customer == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Customer Signature",
              });
            }
            var Installer = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Installer Signature");
            if (Installer == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Installer Signature",
              });
            }
            var Electrical = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Electrical Safety");
            if (Electrical == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Electrical Safety",
              });
            }
            var CustomerImage = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Customer Image");
            if (CustomerImage == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Customer Image",
              });
            }
            var InstallerImage = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Installer Image");
            if (InstallerImage == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Installer Image",
              });
            }
            var OldSystem = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "Old System Photo");
            if (OldSystem == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "Old System Photo",
              });
            }
            var Manual = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "CEC Manual");
            if (Manual == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "CEC Manual",
              });
            }
            var CUCK = _context.PhotoType.FirstOrDefault(t => t.PhotoTypeName == "CUCK Manual");
            if (CUCK == null)
            {
                _context.PhotoType.Add(
              new PhotoTypes.PhotoType
              {
                  PhotoTypeName = "CUCK Manual",
              });
            }

        }
        private void CreateOldSystemDairection()
        {
            var North = _context.OldSystemDirection.FirstOrDefault(t => t.DirectionName == "North");
            if (North == null)
            {
                _context.OldSystemDirection.Add(
              new TechnoForms.OldSystemDirections.OldSystemDirection
              {
                  DirectionName = "North",
              });
            }
            var NorthEast = _context.OldSystemDirection.FirstOrDefault(t => t.DirectionName == "North-East");
            if (NorthEast == null)
            {
                _context.OldSystemDirection.Add(
              new TechnoForms.OldSystemDirections.OldSystemDirection
              {
                  DirectionName = "North-East",
              });
            }
            var East = _context.OldSystemDirection.FirstOrDefault(t => t.DirectionName == "East");
            if (East == null)
            {
                _context.OldSystemDirection.Add(
              new TechnoForms.OldSystemDirections.OldSystemDirection
              {
                  DirectionName = "East",
              });
            }
            var SouthEast = _context.OldSystemDirection.FirstOrDefault(t => t.DirectionName == "South-East");
            if (SouthEast == null)
            {
                _context.OldSystemDirection.Add(
              new TechnoForms.OldSystemDirections.OldSystemDirection
              {
                  DirectionName = "South-East",
              });
            }
            var South = _context.OldSystemDirection.FirstOrDefault(t => t.DirectionName == "South");
            if (South == null)
            {
                _context.OldSystemDirection.Add(
              new TechnoForms.OldSystemDirections.OldSystemDirection
              {
                  DirectionName = "South",
              });
            }
            var SouthWest = _context.OldSystemDirection.FirstOrDefault(t => t.DirectionName == "South-West");
            if (SouthWest == null)
            {
                _context.OldSystemDirection.Add(
              new TechnoForms.OldSystemDirections.OldSystemDirection
              {
                  DirectionName = "South-West",
              });
            }
            var West = _context.OldSystemDirection.FirstOrDefault(t => t.DirectionName == "West");
            if (West == null)
            {
                _context.OldSystemDirection.Add(
              new TechnoForms.OldSystemDirections.OldSystemDirection
              {
                  DirectionName = "West",
              });
            }
            var NorthWest = _context.OldSystemDirection.FirstOrDefault(t => t.DirectionName == "North-West");
            if (NorthWest == null)
            {
                _context.OldSystemDirection.Add(
              new TechnoForms.OldSystemDirections.OldSystemDirection
              {
                  DirectionName = "North-West",
              });
            }
        }
        private void CreateSTCZoneRating()
        {
            var one = _context.STCZoneRating.FirstOrDefault(t => t.STCRating == 1.622);
            if (one == null)
            {
                _context.STCZoneRating.Add(
              new TechnoForms.STCZoneRatings.STCZoneRating
              {
                  STCRating = 1.622,
                  upsize_ts = "0x00000000003DCC51"
              });
            }
            var Two = _context.STCZoneRating.FirstOrDefault(t => t.STCRating == 1.536);
            if (Two == null)
            {
                _context.STCZoneRating.Add(
              new TechnoForms.STCZoneRatings.STCZoneRating
              {
                  STCRating = 1.536,
                  upsize_ts = "0x00000000003DCC52"
              });
            }
            var Three = _context.STCZoneRating.FirstOrDefault(t => t.STCRating == 1.382);
            if (Three == null)
            {
                _context.STCZoneRating.Add(
              new TechnoForms.STCZoneRatings.STCZoneRating
              {
                  STCRating = 1.382,
                  upsize_ts = "0x00000000003DCC53"
              });
            }
            var Four = _context.STCZoneRating.FirstOrDefault(t => t.STCRating == 1.185);
            if (Four == null)
            {
                _context.STCZoneRating.Add(
              new TechnoForms.STCZoneRatings.STCZoneRating
              {
                  STCRating = 1.185,
                  upsize_ts = "0x00000000003DCC54"
              });
            }
        }
    }
}
