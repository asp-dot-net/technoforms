﻿using System.Linq;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using TechnoForms.Authorization;
using TechnoForms.Authorization.Roles;
using TechnoForms.Authorization.Users;
using TechnoForms.EntityFramework;

namespace TechnoForms.Migrations.Seed.Tenants
{
    public class TenantRoleAndUserBuilder
    {
        private readonly TechnoFormsDbContext _context;
        private readonly int _tenantId;

        public TenantRoleAndUserBuilder(TechnoFormsDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            CreateRolesAndUsers();
        }

        private void CreateRolesAndUsers()
        {
            //Admin role

            var adminRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Admin);
            if (adminRole == null)
            {
                adminRole = new Role(_tenantId, StaticRoleNames.Tenants.Admin, StaticRoleNames.Tenants.Admin)
                {
                    IsStatic = true
                };
                adminRole.SetNormalizedName();

                _context.Roles.Add(adminRole);
                _context.SaveChanges();
            }
            
            //User role

            var userRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.User);
            if (userRole == null)
            {
                userRole = new Role(_tenantId, StaticRoleNames.Tenants.User, StaticRoleNames.Tenants.User)
                {
                    IsStatic = true, IsDefault = false
                };
                userRole.SetNormalizedName();

                _context.Roles.Add(userRole);
                _context.SaveChanges();
            }

            ////Installer role

            //var installerRole = _context.Roles.FirstOrDefault(r => r.TenantId == _tenantId && r.Name == StaticRoleNames.Tenants.Installer);
            //if (installerRole == null)
            //{
            //    installerRole = new Role(_tenantId, StaticRoleNames.Tenants.Installer, StaticRoleNames.Tenants.Installer)
            //    {
            //        IsStatic = true,
            //        IsDefault = true
            //    };
            //    installerRole.SetNormalizedName();

            //    _context.Roles.Add(installerRole);
            //    _context.SaveChanges();
            //}

            //admin user

            var adminUser = _context.Users.FirstOrDefault(u => u.TenantId == _tenantId && u.UserName == User.AdminUserName);
            if (adminUser == null)
            {
                adminUser = User.CreateTenantAdminUser(_tenantId, "admin@defaulttenant.com", "123qwe");
                adminUser.IsEmailConfirmed = true;
                adminUser.ShouldChangePasswordOnNextLogin = true;
                adminUser.IsActive = true;
                
                _context.Users.Add(adminUser);
                _context.SaveChanges();

                //Assign Admin role to admin user
                _context.UserRoles.Add(new UserRole(_tenantId, adminUser.Id, adminRole.Id));
                _context.SaveChanges();

                //User account of admin user
                if (_tenantId == 1)
                {
                    _context.UserAccounts.Add(new UserAccount
                    {
                        TenantId = _tenantId,
                        UserId = adminUser.Id,
                        UserName = User.AdminUserName,
                        EmailAddress = adminUser.EmailAddress
                    });
                    _context.SaveChanges();
                }
            }
        }
    }
}
