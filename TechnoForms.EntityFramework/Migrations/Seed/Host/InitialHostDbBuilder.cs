﻿using System.Linq;
using TechnoForms.EntityFramework;

namespace TechnoForms.Migrations.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly TechnoFormsDbContext _context;

        public InitialHostDbBuilder(TechnoFormsDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();

            _context.SaveChanges();
            CreateInvitationStatus();
        }

        private void CreateInvitationStatus()
        {
            var Pending = _context.InvitationStatus.FirstOrDefault(t => t.StatusName == "Pending");
            if (Pending == null)
            {
                _context.InvitationStatus.Add(
              new InvitationsStatus.InvitationStatus
              {
                  StatusCode = 0,
                  StatusName = "Pending"
              });
            }
            var Approve = _context.InvitationStatus.FirstOrDefault(t => t.StatusName == "Approve");
            if (Approve == null)
            {
                _context.InvitationStatus.Add(
              new InvitationsStatus.InvitationStatus
              {
                  StatusCode = 1,
                  StatusName = "Approve"
              });
            }
            var Reject = _context.InvitationStatus.FirstOrDefault(t => t.StatusName == "Reject");
            if (Reject == null)
            {
                _context.InvitationStatus.Add(
              new InvitationsStatus.InvitationStatus
              {
                  StatusCode = 2,
                  StatusName = "Reject"
              });
            }
        }
    }
}
