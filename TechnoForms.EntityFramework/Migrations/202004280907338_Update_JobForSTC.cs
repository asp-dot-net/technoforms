namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_JobForSTC : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "ExpectedSTC", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Job", "STCAppliedDate", c => c.DateTime());
            AlterColumn("dbo.Job", "TotalSTC", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Job", "RegistredSTC", c => c.Decimal(precision: 18, scale: 2));
            DropColumn("dbo.Job", "TotalSTS");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Job", "TotalSTS", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Job", "RegistredSTC", c => c.String());
            AlterColumn("dbo.Job", "TotalSTC", c => c.String());
            AlterColumn("dbo.Job", "STCAppliedDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Job", "ExpectedSTC");
        }
    }
}
