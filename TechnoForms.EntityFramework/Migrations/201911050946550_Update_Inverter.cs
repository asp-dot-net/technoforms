namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Inverter : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Inverter", "ManufacturarName", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Inverter", "Series", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Inverter", "ModelNumber", c => c.String(nullable: false, maxLength: 100));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Inverter", "ModelNumber", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Inverter", "Series", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Inverter", "ManufacturarName", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
