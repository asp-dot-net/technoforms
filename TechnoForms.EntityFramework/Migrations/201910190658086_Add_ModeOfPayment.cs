namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ModeOfPayment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ModeOfPayment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ModeOfPaymentName = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ModeOfPayment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.ModeOfPayment", new[] { "IsDeleted" });
            DropTable("dbo.ModeOfPayment",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ModeOfPayment_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
