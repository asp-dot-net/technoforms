namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Regenerated_Energy_TenantId : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.EnergyDestributar",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        NSW = c.Boolean(nullable: false),
                        SA = c.Boolean(nullable: false),
                        QLD = c.Boolean(nullable: false),
                        VIC = c.Boolean(nullable: false),
                        WA = c.Boolean(nullable: false),
                        ACT = c.Boolean(nullable: false),
                        TAS = c.Boolean(nullable: false),
                        NT = c.Boolean(nullable: false),
                        ElectDistABB = c.String(),
                        ElecDistAppReq = c.Boolean(nullable: false),
                        GreenBoatDistributor = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_EnergyDestributar_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.EnergyRetailer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        NSW = c.Boolean(nullable: false),
                        SA = c.Boolean(nullable: false),
                        QLD = c.Boolean(nullable: false),
                        VIC = c.Boolean(nullable: false),
                        WA = c.Boolean(nullable: false),
                        ACT = c.Boolean(nullable: false),
                        TAS = c.Boolean(nullable: false),
                        NT = c.Boolean(nullable: false),
                        ElectricityProviderId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_EnergyRetailer_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.EnergyDestributar", "TenantId", c => c.Int(nullable: false));
            AddColumn("dbo.EnergyRetailer", "TenantId", c => c.Int(nullable: false));
            CreateIndex("dbo.EnergyDestributar", "TenantId");
            CreateIndex("dbo.EnergyRetailer", "TenantId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.EnergyRetailer", new[] { "TenantId" });
            DropIndex("dbo.EnergyDestributar", new[] { "TenantId" });
            DropColumn("dbo.EnergyRetailer", "TenantId");
            DropColumn("dbo.EnergyDestributar", "TenantId");
            AlterTableAnnotations(
                "dbo.EnergyRetailer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        NSW = c.Boolean(nullable: false),
                        SA = c.Boolean(nullable: false),
                        QLD = c.Boolean(nullable: false),
                        VIC = c.Boolean(nullable: false),
                        WA = c.Boolean(nullable: false),
                        ACT = c.Boolean(nullable: false),
                        TAS = c.Boolean(nullable: false),
                        NT = c.Boolean(nullable: false),
                        ElectricityProviderId = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_EnergyRetailer_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.EnergyDestributar",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        NSW = c.Boolean(nullable: false),
                        SA = c.Boolean(nullable: false),
                        QLD = c.Boolean(nullable: false),
                        VIC = c.Boolean(nullable: false),
                        WA = c.Boolean(nullable: false),
                        ACT = c.Boolean(nullable: false),
                        TAS = c.Boolean(nullable: false),
                        NT = c.Boolean(nullable: false),
                        ElectDistABB = c.String(),
                        ElecDistAppReq = c.Boolean(nullable: false),
                        GreenBoatDistributor = c.Int(),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_EnergyDestributar_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
