namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class RoofFacingPanel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RoofFacingPanel",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoofFacingPanelName = c.String(maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RoofFacingPanel_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.RoofFacingPanel", new[] { "IsDeleted" });
            DropTable("dbo.RoofFacingPanel",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_RoofFacingPanel_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
