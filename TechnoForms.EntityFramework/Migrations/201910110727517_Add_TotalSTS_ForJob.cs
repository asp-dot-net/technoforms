namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_TotalSTS_ForJob : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "TotalSTS", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Job", "TotalSTS");
        }
    }
}
