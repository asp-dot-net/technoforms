namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Installer_RequiredFields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Installer", "CompanyName", c => c.String(maxLength: 100));
            AlterColumn("dbo.Installer", "ABN", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Installer", "ABN", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Installer", "CompanyName", c => c.String(nullable: false, maxLength: 100));
        }
    }
}
