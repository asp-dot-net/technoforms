// <auto-generated />
namespace TechnoForms.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Update_Panel : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Update_Panel));
        
        string IMigrationMetadata.Id
        {
            get { return "201911050903230_Update_Panel"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
