namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Add_STCPostCodes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.STCPostCode",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        STCPostCodeFrom = c.Int(nullable: false),
                        STCPostCodeTo = c.Int(nullable: false),
                        STCZoneID = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_STCPostCode_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.STCPostCode", new[] { "IsDeleted" });
            DropTable("dbo.STCPostCode",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_STCPostCode_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
