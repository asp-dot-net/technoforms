namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhotoType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PhotoType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PhotoTypeName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.PhotoType");
        }
    }
}
