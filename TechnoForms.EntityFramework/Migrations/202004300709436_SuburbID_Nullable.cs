namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SuburbID_Nullable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Installer", "Suburb_Id", c => c.Int());
            AddColumn("dbo.InstallerEmploye", "Suburb_Id", c => c.Int());
            AddColumn("dbo.Job", "Suburb_Id", c => c.Int());
            AddColumn("dbo.Job", "InstallationSuburb_Id", c => c.Int());
            AddColumn("dbo.AbpTenants", "Suburb_Id", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AbpTenants", "Suburb_Id");
            DropColumn("dbo.Job", "InstallationSuburb_Id");
            DropColumn("dbo.Job", "Suburb_Id");
            DropColumn("dbo.InstallerEmploye", "Suburb_Id");
            DropColumn("dbo.Installer", "Suburb_Id");
        }
    }
}
