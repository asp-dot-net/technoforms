namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_Job_Comments : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "InstComment", c => c.String(maxLength: 100));
            AddColumn("dbo.Job", "OwnerComment", c => c.String(maxLength: 100));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Job", "OwnerComment");
            DropColumn("dbo.Job", "InstComment");
        }
    }
}
