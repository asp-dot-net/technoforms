namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Installer : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Installer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        CustID = c.Int(nullable: false),
                        FullName = c.String(nullable: false, maxLength: 200),
                        Phone = c.String(nullable: false, maxLength: 10),
                        Mobile = c.String(maxLength: 10),
                        EmailId = c.String(nullable: false, maxLength: 255),
                        CompanyName = c.String(nullable: false, maxLength: 100),
                        ABN = c.String(nullable: false, maxLength: 50),
                        CompanyPhone = c.String(maxLength: 10),
                        Fax = c.String(maxLength: 20),
                        AddressType = c.Int(nullable: false),
                        PostalDelNo = c.String(maxLength: 50),
                        PostalDelType = c.Int(nullable: false),
                        IsGST = c.Boolean(nullable: false),
                        Logo = c.String(nullable: false, maxLength: 500),
                        StreetAddress = c.String(maxLength: 300),
                        UnitNo = c.String(maxLength: 50),
                        UnitType = c.String(maxLength: 50),
                        StreetNumber = c.String(nullable: false, maxLength: 50),
                        StreetName = c.String(nullable: false, maxLength: 100),
                        StreetType = c.String(nullable: false, maxLength: 50),
                        Suburb = c.String(nullable: false, maxLength: 100),
                        State = c.String(nullable: false, maxLength: 50),
                        PostCode = c.String(nullable: false, maxLength: 50),
                        IsInst = c.Boolean(),
                        InstallerExpiry = c.String(maxLength: 100),
                        InsAccreExDate = c.DateTime(),
                        IsElec = c.Boolean(),
                        ElectricalExpiry = c.String(maxLength: 100),
                        EleLicenceExDate = c.DateTime(),
                        IsDesc = c.Boolean(),
                        DesignerExpiry = c.String(maxLength: 100),
                        DesignerAccreExDate = c.DateTime(),
                        AccreditationAccPhoto = c.String(nullable: false, maxLength: 200),
                        LicencePhoto = c.String(nullable: false, maxLength: 200),
                        Guid = c.Guid(nullable: false),
                        InstallerSignature = c.String(maxLength: 50),
                        ElectricianSignature = c.String(maxLength: 50),
                        DesignerSignature = c.String(maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Installer_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Installer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.TenantId)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Installer", new[] { "IsDeleted" });
            DropIndex("dbo.Installer", new[] { "TenantId" });
            DropTable("dbo.Installer",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Installer_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Installer_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
