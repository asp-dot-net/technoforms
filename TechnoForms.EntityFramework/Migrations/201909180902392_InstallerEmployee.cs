namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class InstallerEmployee : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InstallerEmploye",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        FullName = c.String(nullable: false, maxLength: 200),
                        Phone = c.String(nullable: false, maxLength: 10),
                        Mobile = c.String(maxLength: 10),
                        EmailId = c.String(nullable: false, maxLength: 255),
                        Fax = c.String(maxLength: 20),
                        StreetAddress = c.String(maxLength: 300),
                        UnitNo = c.String(maxLength: 50),
                        UnitType = c.String(maxLength: 50),
                        StreetNumber = c.String(nullable: false, maxLength: 50),
                        StreetName = c.String(nullable: false, maxLength: 100),
                        StreetType = c.String(nullable: false, maxLength: 50),
                        Suburb = c.String(nullable: false, maxLength: 100),
                        State = c.String(nullable: false, maxLength: 50),
                        PostCode = c.String(nullable: false, maxLength: 50),
                        InstallerExpiry = c.String(maxLength: 100),
                        InsAccreExDate = c.DateTime(),
                        ElectricalExpiry = c.String(maxLength: 100),
                        EleLicenceExDate = c.DateTime(),
                        DesignerExpiry = c.String(maxLength: 100),
                        DesignerAccreExDate = c.DateTime(),
                        InstallerId = c.Int(nullable: false),
                        CustId = c.Int(nullable: false),
                        AddressType = c.Int(nullable: false),
                        PostalDelNo = c.String(maxLength: 50),
                        PostalDelType = c.Int(nullable: false),
                        IsInst = c.Boolean(),
                        IsElec = c.Boolean(),
                        IsDesc = c.Boolean(),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InstallerEmploye_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InstallerEmploye_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.TenantId)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.InstallerEmploye", new[] { "IsDeleted" });
            DropIndex("dbo.InstallerEmploye", new[] { "TenantId" });
            DropTable("dbo.InstallerEmploye",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_InstallerEmploye_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_InstallerEmploye_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
