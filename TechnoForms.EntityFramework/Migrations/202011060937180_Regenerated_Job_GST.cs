namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Regenerated_Job_GST : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "IsCustomerRegistredGST", c => c.Int());
            AddColumn("dbo.Job", "CustomerABN", c => c.String());
            AddColumn("dbo.Job", "CustomerOrganizationName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Job", "CustomerOrganizationName");
            DropColumn("dbo.Job", "CustomerABN");
            DropColumn("dbo.Job", "IsCustomerRegistredGST");
        }
    }
}
