namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updateInstallerFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Installer", "UserName", c => c.String());
            AddColumn("dbo.Installer", "IsApproved", c => c.Int(nullable: false));
            AddColumn("dbo.Installer", "RejectReason", c => c.String());
            DropColumn("dbo.Installer", "CustID");
            DropColumn("dbo.InstallerEmploye", "CustId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.InstallerEmploye", "CustId", c => c.Int(nullable: false));
            AddColumn("dbo.Installer", "CustID", c => c.Int(nullable: false));
            DropColumn("dbo.Installer", "RejectReason");
            DropColumn("dbo.Installer", "IsApproved");
            DropColumn("dbo.Installer", "UserName");
        }
    }
}
