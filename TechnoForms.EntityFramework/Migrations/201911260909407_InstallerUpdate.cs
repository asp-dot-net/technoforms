namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InstallerUpdate : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Installer", "FullName", c => c.String(maxLength: 200));
            AlterColumn("dbo.Installer", "Phone", c => c.String(maxLength: 10));
            AlterColumn("dbo.Installer", "EmailId", c => c.String(maxLength: 255));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Installer", "EmailId", c => c.String(nullable: false, maxLength: 255));
            AlterColumn("dbo.Installer", "Phone", c => c.String(nullable: false, maxLength: 10));
            AlterColumn("dbo.Installer", "FullName", c => c.String(nullable: false, maxLength: 200));
        }
    }
}
