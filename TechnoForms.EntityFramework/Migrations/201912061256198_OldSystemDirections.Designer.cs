// <auto-generated />
namespace TechnoForms.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class OldSystemDirections : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(OldSystemDirections));
        
        string IMigrationMetadata.Id
        {
            get { return "201912061256198_OldSystemDirections"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
