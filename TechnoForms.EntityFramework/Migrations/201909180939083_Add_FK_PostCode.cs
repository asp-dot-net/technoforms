namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_FK_PostCode : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PostCode", "State_Id", "dbo.State");
            DropIndex("dbo.PostCode", new[] { "State_Id" });
            RenameColumn(table: "dbo.PostCode", name: "State_Id", newName: "StateId");
            AlterColumn("dbo.PostCode", "StateId", c => c.Int(nullable: false));
            CreateIndex("dbo.PostCode", "StateId");
            AddForeignKey("dbo.PostCode", "StateId", "dbo.State", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PostCode", "StateId", "dbo.State");
            DropIndex("dbo.PostCode", new[] { "StateId" });
            AlterColumn("dbo.PostCode", "StateId", c => c.Int());
            RenameColumn(table: "dbo.PostCode", name: "StateId", newName: "State_Id");
            CreateIndex("dbo.PostCode", "State_Id");
            AddForeignKey("dbo.PostCode", "State_Id", "dbo.State", "Id");
        }
    }
}
