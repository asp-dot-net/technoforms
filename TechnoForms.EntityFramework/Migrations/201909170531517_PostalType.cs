namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class PostalType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostalType",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PostalTypeName = c.String(nullable: false, maxLength: 50),
                        PostalTypeCode = c.String(nullable: false, maxLength: 10),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PostalType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.PostalType", new[] { "IsDeleted" });
            DropTable("dbo.PostalType",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_PostalType_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
