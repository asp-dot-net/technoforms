namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Job : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Job",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        Company = c.String(maxLength: 100),
                        EmailId = c.String(nullable: false, maxLength: 50),
                        Phone = c.String(nullable: false, maxLength: 20),
                        Fax = c.String(maxLength: 20),
                        PostalAddressType = c.String(),
                        PostalDeliveryNumber = c.String(maxLength: 20),
                        PostalDeliveryType = c.String(),
                        StreetAddress = c.String(maxLength: 100),
                        UnitNo = c.String(maxLength: 20),
                        UnitType = c.String(),
                        StreetNo = c.String(nullable: false, maxLength: 20),
                        StreetName = c.String(nullable: false),
                        StreetType = c.String(nullable: false),
                        StreetCity = c.String(nullable: false, maxLength: 100),
                        StreetState = c.String(nullable: false, maxLength: 100),
                        StreetPostCode = c.String(nullable: false, maxLength: 20),
                        InstallationStreetAddress = c.String(maxLength: 100),
                        InstallationUnitNo = c.String(maxLength: 20),
                        InstallationUnitType = c.String(),
                        InstallationStreetNo = c.String(nullable: false, maxLength: 20),
                        InstallationStreetName = c.String(nullable: false),
                        InstallationStreetType = c.String(nullable: false),
                        InstallationStreetCity = c.String(nullable: false, maxLength: 100),
                        InstallationStreetState = c.String(nullable: false, maxLength: 100),
                        InstallationStreetPostCode = c.String(nullable: false, maxLength: 20),
                        installationAddresssameasHouseAddress = c.Boolean(nullable: false),
                        JobStatus_Id = c.Int(nullable: false),
                        HouseType = c.Int(nullable: false),
                        RoofType = c.Int(nullable: false),
                        NMINo = c.String(nullable: false, maxLength: 100),
                        RegPlanNo = c.String(maxLength: 20),
                        LotNo = c.String(maxLength: 20),
                        PanelName = c.String(nullable: false, maxLength: 200),
                        BrandName = c.String(maxLength: 200),
                        Panel_id = c.Int(nullable: false),
                        PanelModel = c.String(),
                        PanelSize = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NoOfPanels = c.Int(nullable: false),
                        SystemCapacity = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InverterName = c.String(nullable: false, maxLength: 200),
                        InverterBrandName = c.String(maxLength: 200),
                        Inverter_id = c.Int(nullable: false),
                        InverterModel = c.String(),
                        InverterSeries = c.String(maxLength: 200),
                        NoOfInverter = c.Decimal(nullable: false, precision: 18, scale: 2),
                        InverterOutput = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SolarType_Id = c.Int(nullable: false),
                        SystemType_Id = c.Int(nullable: false),
                        ExpectedInstallDate = c.DateTime(nullable: false),
                        RoofFacingPanel_Id = c.Int(nullable: false),
                        Sugstthisaddress_Id = c.Int(nullable: false),
                        LocationOfNewSystem_Id = c.Int(nullable: false),
                        Customer_Id = c.Int(nullable: false),
                        CreateFor_Id = c.Int(nullable: false),
                        Installer_Id = c.Int(nullable: false),
                        Designer_Id = c.Int(nullable: false),
                        Electrician_Id = c.Int(nullable: false),
                        ProjectNumber = c.String(),
                        InstallerNote = c.String(),
                        Typeofsystem_Id = c.Int(nullable: false),
                        Typeofconnection_Id = c.Int(nullable: false),
                        Mountingtype_Id = c.Int(nullable: false),
                        Auditreportavailable_Id = c.Int(nullable: false),
                        Installingcompleteunit_Id = c.Int(nullable: false),
                        AddcapMoredetailed = c.String(maxLength: 100),
                        RECYear_Id = c.Int(nullable: false),
                        Defaultresourceavailfigure_Id = c.Int(nullable: false),
                        ResourceAvailability = c.String(maxLength: 100),
                        AccreditationCode = c.String(maxLength: 100),
                        ExplanatoryNotes = c.String(maxLength: 100),
                        CeatingCert_Id = c.Int(nullable: false),
                        CECAccreditationStatement_Id = c.Int(nullable: false),
                        GovernmentSitingApprovals_Id = c.Int(nullable: false),
                        ElectricalSafetyDocumentation_Id = c.Int(nullable: false),
                        AustralianStandardsStatement_Id = c.Int(nullable: false),
                        NotgridConnectedStatement_Id = c.Int(nullable: false),
                        InstallPropertyName = c.String(maxLength: 100),
                        SGUatThisAddress_Id = c.Int(nullable: false),
                        DescribeAdequateIinfo = c.String(maxLength: 100),
                        AdditionalSystemInformation = c.String(maxLength: 100),
                        SystemMountingType_Id = c.Int(nullable: false),
                        OwnerAddressType_Id = c.Int(nullable: false),
                        CustomerAddressType_Id = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Job_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Job_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.TenantId)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Job", new[] { "IsDeleted" });
            DropIndex("dbo.Job", new[] { "TenantId" });
            DropTable("dbo.Job",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Job_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                    { "DynamicFilter_Job_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
