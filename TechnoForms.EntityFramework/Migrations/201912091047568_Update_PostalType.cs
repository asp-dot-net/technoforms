namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update_PostalType : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PostalType", "PostalTypeCode", c => c.String(nullable: false, maxLength: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PostalType", "PostalTypeCode", c => c.String(nullable: false, maxLength: 10));
        }
    }
}
