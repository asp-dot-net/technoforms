namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Panel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Panel",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LicenceOrCertiHolder = c.String(nullable: false, maxLength: 50),
                        ModelNo = c.String(nullable: false, maxLength: 50),
                        CECApprovedDate = c.DateTime(nullable: false),
                        ExpiryDate = c.DateTime(nullable: false),
                        Size = c.Decimal(nullable: false, precision: 18, scale: 2),
                        FireTested = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Panel_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Panel", new[] { "IsDeleted" });
            DropTable("dbo.Panel",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Panel_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
