// <auto-generated />
namespace TechnoForms.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class YesNo : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(YesNo));
        
        string IMigrationMetadata.Id
        {
            get { return "201909170558331_YesNo"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
