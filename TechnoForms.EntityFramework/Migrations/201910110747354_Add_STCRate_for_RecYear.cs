namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_STCRate_for_RecYear : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RecYear", "STCRate", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RecYear", "STCRate");
        }
    }
}
