namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SerialNo_Update : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ScanedInverter", "InverterSerialNumber", c => c.String(maxLength: 50));
            AlterColumn("dbo.ScanedPanel", "PanelSerialNumber", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ScanedPanel", "PanelSerialNumber", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.ScanedInverter", "InverterSerialNumber", c => c.String(nullable: false, maxLength: 50));
        }
    }
}
