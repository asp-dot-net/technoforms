namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateFields : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.CustInstEmp",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            TenantId = c.Int(nullable: false),
            //            InstallerId = c.Int(nullable: false),
            //            EmployeeId = c.Int(nullable: false),
            //            IsCreated = c.Boolean(nullable: false),
            //            IsDeleted = c.Boolean(nullable: false),
            //            DeleterUserId = c.Long(),
            //            DeletionTime = c.DateTime(),
            //            LastModificationTime = c.DateTime(),
            //            LastModifierUserId = c.Long(),
            //            CreationTime = c.DateTime(nullable: false),
            //            CreatorUserId = c.Long(),
            //        },
            //    annotations: new Dictionary<string, object>
            //    {
            //        { "DynamicFilter_CustInstEmp_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
            //        { "DynamicFilter_CustInstEmp_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
            //    })
            //    .PrimaryKey(t => t.Id)
            //    .Index(t => t.TenantId)
            //    .Index(t => t.IsDeleted);
            
            //CreateTable(
            //    "dbo.CustomerInstaller",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            TenantId = c.Int(nullable: false),
            //            InstallerId = c.Int(nullable: false),
            //            Inst_Request_status = c.Int(nullable: false),
            //            Inst_RejectReason = c.String(maxLength: 500),
            //            Request_status = c.Int(nullable: false),
            //            RejectReason = c.String(maxLength: 500),
            //            IsCreated = c.Boolean(nullable: false),
            //            Guid = c.Guid(nullable: false),
            //            IsDeleted = c.Boolean(nullable: false),
            //            DeleterUserId = c.Long(),
            //            DeletionTime = c.DateTime(),
            //            LastModificationTime = c.DateTime(),
            //            LastModifierUserId = c.Long(),
            //            CreationTime = c.DateTime(nullable: false),
            //            CreatorUserId = c.Long(),
            //        },
            //    annotations: new Dictionary<string, object>
            //    {
            //        { "DynamicFilter_CustomerInstaller_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
            //        { "DynamicFilter_CustomerInstaller_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
            //    })
            //    .PrimaryKey(t => t.Id)
            //    .Index(t => t.TenantId)
            //    .Index(t => t.IsDeleted);
            
        }
        
        public override void Down()
        {
            //DropIndex("dbo.CustomerInstaller", new[] { "IsDeleted" });
            //DropIndex("dbo.CustomerInstaller", new[] { "TenantId" });
            //DropIndex("dbo.CustInstEmp", new[] { "IsDeleted" });
            //DropIndex("dbo.CustInstEmp", new[] { "TenantId" });
            //DropTable("dbo.CustomerInstaller",
            //    removedAnnotations: new Dictionary<string, object>
            //    {
            //        { "DynamicFilter_CustomerInstaller_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
            //        { "DynamicFilter_CustomerInstaller_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
            //    });
            //DropTable("dbo.CustInstEmp",
            //    removedAnnotations: new Dictionary<string, object>
            //    {
            //        { "DynamicFilter_CustInstEmp_MustHaveTenant", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
            //        { "DynamicFilter_CustInstEmp_SoftDelete", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
            //    });
        }
    }
}
