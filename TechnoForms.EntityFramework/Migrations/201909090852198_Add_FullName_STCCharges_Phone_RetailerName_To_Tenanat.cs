namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_FullName_STCCharges_Phone_RetailerName_To_Tenanat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AbpTenants", "FullName", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.AbpTenants", "CompanyName", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.AbpTenants", "ABN", c => c.String(nullable: false, maxLength: 50));
            AddColumn("dbo.AbpTenants", "FAX", c => c.String(maxLength: 50));
            AddColumn("dbo.AbpTenants", "Phone", c => c.String(nullable: false, maxLength: 20));
            AddColumn("dbo.AbpTenants", "Mobile", c => c.String(maxLength: 20));
            AddColumn("dbo.AbpTenants", "Email", c => c.String(nullable: false, maxLength: 250));
            AddColumn("dbo.AbpTenants", "STCCharges", c => c.Single(nullable: false));
            AddColumn("dbo.AbpTenants", "RetailerName", c => c.String(nullable: false, maxLength: 200));
            AddColumn("dbo.AbpTenants", "RetailerABN", c => c.String(nullable: false, maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AbpTenants", "RetailerABN");
            DropColumn("dbo.AbpTenants", "RetailerName");
            DropColumn("dbo.AbpTenants", "STCCharges");
            DropColumn("dbo.AbpTenants", "Email");
            DropColumn("dbo.AbpTenants", "Mobile");
            DropColumn("dbo.AbpTenants", "Phone");
            DropColumn("dbo.AbpTenants", "FAX");
            DropColumn("dbo.AbpTenants", "ABN");
            DropColumn("dbo.AbpTenants", "CompanyName");
            DropColumn("dbo.AbpTenants", "FullName");
        }
    }
}
