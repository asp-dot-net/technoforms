namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Alter_Payment_Tenant : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.Payment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Invoice_ID = c.Int(nullable: false),
                        Modeofpayment_Id = c.Int(nullable: false),
                        date = c.String(nullable: false),
                        InstrumentDetail = c.String(maxLength: 500),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Payment_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.Payment", "TenantId", c => c.Int(nullable: false));
            CreateIndex("dbo.Payment", "TenantId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Payment", new[] { "TenantId" });
            DropColumn("dbo.Payment", "TenantId");
            AlterTableAnnotations(
                "dbo.Payment",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Invoice_ID = c.Int(nullable: false),
                        Modeofpayment_Id = c.Int(nullable: false),
                        date = c.String(nullable: false),
                        InstrumentDetail = c.String(maxLength: 500),
                        amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Payment_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
