namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class update_Tenant : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AbpTenants", "StreetAddress", c => c.String(maxLength: 300));
            AddColumn("dbo.AbpTenants", "UnitNo", c => c.String(maxLength: 50));
            AddColumn("dbo.AbpTenants", "UnitType", c => c.String(maxLength: 50));
            AddColumn("dbo.AbpTenants", "StreetNumber", c => c.String(maxLength: 50));
            AddColumn("dbo.AbpTenants", "StreetName", c => c.String(maxLength: 100));
            AddColumn("dbo.AbpTenants", "StreetType", c => c.String(maxLength: 50));
            AddColumn("dbo.AbpTenants", "Suburb", c => c.String(maxLength: 100));
            AddColumn("dbo.AbpTenants", "State", c => c.String(maxLength: 50));
            AddColumn("dbo.AbpTenants", "PostCode", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AbpTenants", "PostCode");
            DropColumn("dbo.AbpTenants", "State");
            DropColumn("dbo.AbpTenants", "Suburb");
            DropColumn("dbo.AbpTenants", "StreetType");
            DropColumn("dbo.AbpTenants", "StreetName");
            DropColumn("dbo.AbpTenants", "StreetNumber");
            DropColumn("dbo.AbpTenants", "UnitType");
            DropColumn("dbo.AbpTenants", "UnitNo");
            DropColumn("dbo.AbpTenants", "StreetAddress");
        }
    }
}
