// <auto-generated />
namespace TechnoForms.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class Add_FullName_STCCharges_Phone_RetailerName_To_Tenanat : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Add_FullName_STCCharges_Phone_RetailerName_To_Tenanat));
        
        string IMigrationMetadata.Id
        {
            get { return "201909090852198_Add_FullName_STCCharges_Phone_RetailerName_To_Tenanat"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
