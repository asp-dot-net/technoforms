namespace TechnoForms.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class Update_ScanedPanel_ScanedInverter : DbMigration
    {
        public override void Up()
        {
            AlterTableAnnotations(
                "dbo.Photo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImageName = c.String(nullable: false, maxLength: 50),
                        Guid = c.Guid(nullable: false),
                        FileType = c.Int(nullable: false),
                        OldSystemDirection = c.Int(),
                        ImageGuid = c.Guid(nullable: false),
                        Longitude = c.String(nullable: false, maxLength: 50),
                        Latitude = c.String(nullable: false, maxLength: 50),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Photo_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                    { 
                        "DynamicFilter_Photo_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                    { 
                        "DynamicFilter_ScanedPanels_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                    { 
                        "DynamicFilter_ScanedPanels_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ScanedInverter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        InverterSerialNumber = c.String(nullable: false, maxLength: 50),
                        Guid = c.Guid(nullable: false),
                        IsScan = c.Boolean(nullable: false),
                        InverterBarcodePhoto = c.String(nullable: false, maxLength: 100),
                        Longitude = c.String(nullable: false, maxLength: 50),
                        Latitude = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ScanedInverter_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ScanedPanel",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        PanelSerialNumber = c.String(nullable: false, maxLength: 50),
                        Guid = c.Guid(nullable: false),
                        IsScan = c.Boolean(nullable: false),
                        PanelBarcodePhoto = c.String(nullable: false, maxLength: 100),
                        Longitude = c.String(nullable: false, maxLength: 50),
                        Latitude = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ScanedPanel_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.ScanedInverter", "TenantId", c => c.Int(nullable: false));
            AddColumn("dbo.ScanedPanel", "TenantId", c => c.Int(nullable: false));
            CreateIndex("dbo.ScanedInverter", "TenantId");
            CreateIndex("dbo.ScanedPanel", "TenantId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.ScanedPanel", new[] { "TenantId" });
            DropIndex("dbo.ScanedInverter", new[] { "TenantId" });
            DropColumn("dbo.ScanedPanel", "TenantId");
            DropColumn("dbo.ScanedInverter", "TenantId");
            AlterTableAnnotations(
                "dbo.ScanedPanel",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        PanelSerialNumber = c.String(nullable: false, maxLength: 50),
                        Guid = c.Guid(nullable: false),
                        IsScan = c.Boolean(nullable: false),
                        PanelBarcodePhoto = c.String(nullable: false, maxLength: 100),
                        Longitude = c.String(nullable: false, maxLength: 50),
                        Latitude = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ScanedPanel_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.ScanedInverter",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TenantId = c.Int(nullable: false),
                        InverterSerialNumber = c.String(nullable: false, maxLength: 50),
                        Guid = c.Guid(nullable: false),
                        IsScan = c.Boolean(nullable: false),
                        InverterBarcodePhoto = c.String(nullable: false, maxLength: 100),
                        Longitude = c.String(nullable: false, maxLength: 50),
                        Latitude = c.String(nullable: false, maxLength: 50),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ScanedInverter_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            AlterTableAnnotations(
                "dbo.Photo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImageName = c.String(nullable: false, maxLength: 50),
                        Guid = c.Guid(nullable: false),
                        FileType = c.Int(nullable: false),
                        OldSystemDirection = c.Int(),
                        ImageGuid = c.Guid(nullable: false),
                        Longitude = c.String(nullable: false, maxLength: 50),
                        Latitude = c.String(nullable: false, maxLength: 50),
                        TenantId = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                        DeleterUserId = c.Long(),
                        DeletionTime = c.DateTime(),
                        LastModificationTime = c.DateTime(),
                        LastModifierUserId = c.Long(),
                        CreationTime = c.DateTime(nullable: false),
                        CreatorUserId = c.Long(),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_Photo_MustHaveTenant",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                    { 
                        "DynamicFilter_Photo_SoftDelete",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                    { 
                        "DynamicFilter_ScanedPanels_MustHaveTenant",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                    { 
                        "DynamicFilter_ScanedPanels_SoftDelete",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
        }
    }
}
