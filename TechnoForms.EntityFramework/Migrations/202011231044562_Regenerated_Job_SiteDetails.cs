namespace TechnoForms.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Regenerated_Job_SiteDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Job", "MeterNumber", c => c.String(maxLength: 50));
            AddColumn("dbo.Job", "EnergyDestributar", c => c.Int());
            AddColumn("dbo.Job", "EnergyRetailer", c => c.Int());
            AddColumn("dbo.Job", "AemoDERjobID", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Job", "AemoDERjobID");
            DropColumn("dbo.Job", "EnergyRetailer");
            DropColumn("dbo.Job", "EnergyDestributar");
            DropColumn("dbo.Job", "MeterNumber");
        }
    }
}
