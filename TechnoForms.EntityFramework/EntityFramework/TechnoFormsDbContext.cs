﻿using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using TechnoForms.Authorization.Roles;
using TechnoForms.Authorization.Users;
using TechnoForms.Chat;
using TechnoForms.Friendships;
using TechnoForms.Inverters;
using TechnoForms.MultiTenancy;
using TechnoForms.Panels;
using TechnoForms.Storage;
using TechnoForms.StreetName;
using TechnoForms.Postaltypes;
using TechnoForms.States;
using TechnoForms.PostCodes;
using TechnoForms.UnitTypes;
using TechnoForms.Installers;
using TechnoForms.InstallerEmployes;
using TechnoForms.JobStatusName;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;
using TechnoForms.Jobs;
using TechnoForms.StreetTypes;
using TechnoForms.STCPostCodes;
using TechnoForms.Invoices;
using TechnoForms.ModeOfPayments;
using TechnoForms.Payments;
using TechnoForms.CustInstEmps;
using TechnoForms.CustomerInstallers;
using TechnoForms.InvitationsStatus;
using TechnoForms.PhotoTypes;
using TechnoForms.Photos;
using TechnoForms.OldSystemDirections;
using TechnoForms.ScanedPanels;
using TechnoForms.ScanedInverters;
using TechnoForms.Messages;
using TechnoForms.Videos;
using TechnoForms.Audios;
using TechnoForms.RecUsers;
using TechnoForms.STCZoneRatings;
using TechnoForms.Energy;

namespace TechnoForms.EntityFramework
{
    /* Constructors of this DbContext is important and each one has it's own use case.
     * - Default constructor is used by EF tooling on design time.
     * - constructor(nameOrConnectionString) is used by ABP on runtime.
     * - constructor(existingConnection) is used by unit tests.
     * - constructor(existingConnection,contextOwnsConnection) can be used by ABP if DbContextEfTransactionStrategy is used.
     * See http://www.aspnetboilerplate.com/Pages/Documents/EntityFramework-Integration for more.
     */

    public class TechnoFormsDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        /* Define an IDbSet for each entity of the application */
        public virtual IDbSet<EnergyDestributar> EnergyDestributars { get; set; }

        public virtual IDbSet<EnergyRetailer> EnergyRetailers { get; set; }

        public virtual IDbSet<BinaryObject> BinaryObjects { get; set; }

        public virtual IDbSet<Friendship> Friendships { get; set; }

        public virtual IDbSet<ChatMessage> ChatMessages { get; set; }

        public virtual IDbSet<UnitType> UnitType { get; set; }

        public virtual IDbSet<StreetType> StreetType { get; set; }

        public virtual IDbSet<StreetNames> StreetName { get; set; }

        public virtual IDbSet<PostalType> PostalType { get; set; }

        public virtual IDbSet<State> State { get; set; }

        public virtual IDbSet<PostCode> PostCode { get; set; }

        public virtual IDbSet<Inverter> Inverter { get; set; }

        public virtual IDbSet<Panel> Panel { get; set; }

        public virtual IDbSet<Installer> Installer { get; set; }

        public virtual IDbSet<InstallerEmploye> InstallerEmploye { get; set; }

        public virtual IDbSet<PostalAddressType> PostalAddressType { get; set; }

        public virtual IDbSet<PostalDeliveryType> PostalDeliveryType { get; set; }

        public virtual IDbSet<JobStatus> JobStatus { get; set; }

        public virtual IDbSet<Job> Job { get; set; }

        public virtual IDbSet<HouseType> HouseType { get; set; }

        public virtual IDbSet<RoofType> RoofType { get; set; }

        public virtual IDbSet<SolarType> SolarType { get; set; }

        public virtual IDbSet<SystemType> SystemType { get; set; }

        public virtual IDbSet<RoofFacingPanel> RoofFacingPanel { get; set; }

        public virtual IDbSet<YesNo> YesNo { get; set; }

        public virtual IDbSet<TypeOfSystem> TypeOfSystem { get; set; }

        public virtual IDbSet<TypeOfConnection> TypeOfConnection { get; set; }

        public virtual IDbSet<MountingType> MountingType { get; set; }

        public virtual IDbSet<RecYear> RecYear { get; set; }

        public virtual IDbSet<SystemMountingType> SystemMountingType { get; set; }

        public virtual IDbSet<STCPostCode> STCPostCode { get; set; }

        public virtual IDbSet<Invoice> Invoice { get; set; }

        public virtual IDbSet<InvoiceItem> InvoiceItem { get; set; }

        public virtual IDbSet<ModeOfPayment> ModeOfPayment { get; set; }

        public virtual IDbSet<Payment> Payment { get; set; }

        public virtual IDbSet<CustInstEmp> CustInstEmp { get; set; }

        public virtual IDbSet<CustomerInstaller> CustomerInstaller { get; set; }

        public virtual IDbSet<InvitationStatus> InvitationStatus { get; set; }

        public virtual IDbSet<PhotoType> PhotoType { get; set; }

        public virtual IDbSet<Photo> Photo { get; set; }

        public virtual IDbSet<OldSystemDirection> OldSystemDirection { get; set; }

        public virtual IDbSet<ScanedPanel> ScanedPanel { get; set; }

        public virtual IDbSet<ScanedInverter> ScanedInverter { get; set; }

        public virtual IDbSet<Message> Message { get; set; }

        public virtual IDbSet<Token> Token { get; set; }

        public virtual IDbSet<CustomerSignLog> CustomerSignLog { get; set; }

        public virtual IDbSet<CustomerSignLogResponse> CustomerSignLogResponse { get; set; }

        public virtual IDbSet<video> video { get; set; }

        public virtual IDbSet<Audio> Audio { get; set; }

        public virtual IDbSet<RecUser> RecUser { get; set; }

        public virtual IDbSet<PanelBulkUpload> PanelBulkUpload { get; set; }

        public virtual IDbSet<InverterBulkUpload> InverterBulkUpload { get; set; }

        public virtual IDbSet<JobBulkUpload> JobBulkUpload { get; set; }

        public virtual IDbSet<STCZoneRating> STCZoneRating { get; set; }


        public TechnoFormsDbContext()
            : base("Default")
        {

        }

        public TechnoFormsDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {

        }

        public TechnoFormsDbContext(DbConnection existingConnection)
           : base(existingConnection, false)
        {

        }

        public TechnoFormsDbContext(DbConnection existingConnection, bool contextOwnsConnection)
            : base(existingConnection, contextOwnsConnection)
        {

        }
    }
}
