﻿using Abp;
using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using TechnoForms.EntityFramework;
using TechnoForms.EntityHistory;

namespace TechnoForms
{
    /// <summary>
    /// Entity framework module of the application.
    /// </summary>
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(TechnoFormsCoreModule))]
    public class TechnoFormsDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<TechnoFormsDbContext>());

            //web.config (or app.config for non-web projects) file should contain a connection string named "Default".
            Configuration.DefaultNameOrConnectionString = "Default";

            // Uncomment below line to write change logs for the entities below:
            //Configuration.EntityHistory.Selectors.Add("TechnoFormsEntities", EntityHistoryHelper.TrackedTypes);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
