﻿using Abp.Data;
using Abp.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.AdminDashboard.Dto;
using TechnoForms.EntityFramework;
using TechnoForms.EntityFramework.Repositories;
using TechnoForms.Jobs;

namespace TechnoForms.AdminDashboard
{
    public class AdminDashboardRepository : TechnoFormsRepositoryBase<Job, int>, IAdminDashboardRepository
    {

        private readonly IActiveTransactionProvider _transactionProvider;

        public AdminDashboardRepository(IDbContextProvider<TechnoFormsDbContext> dbContextProvider, IActiveTransactionProvider transactionProvider)
            : base(dbContextProvider)
        {
            _transactionProvider = transactionProvider;
        }

        public async Task<List<AdminDashboardJobList>> GetDashboardJobListAsync()
        {
            EnsureConnectionOpen();
            try
            {
                using (var command = CreateCommand("DashboardJobList", CommandType.StoredProcedure))
                {
                    using (var dataReader = await command.ExecuteReaderAsync())
                    {
                        var result = new List<AdminDashboardJobList>();

                        while (dataReader.Read())
                        {
                            result.Add(new AdminDashboardJobList()
                            {
                                ExpectedInstallDate = dataReader["ExpectedInstallDate"].ToString(),
                                ProjectNumber = dataReader["ProjectNumber"].ToString(),
                                StreetAddress = dataReader["StreetAddress"].ToString(),
                                StreetCity = dataReader["StreetCity"].ToString(),
                                StreetState = dataReader["StreetState"].ToString(),
                                StreetPostCode = dataReader["StreetPostCode"].ToString(),
                                FirstName = dataReader["FirstName"].ToString(),
                                LastName = dataReader["LastName"].ToString()
                            });
                        }
                        return result;
                    }
                }
            }
            catch(Exception e)
            {
                return null;
            }            
        }

        private void EnsureConnectionOpen()
        {
            var connection = Context.Database.Connection;

            if (connection.State != ConnectionState.Open)
            {
                connection.Open();
            }
        }

        private DbCommand CreateCommand(string commandText, CommandType commandType, params SqlParameter[] parameters)
        {
            var command = Context.Database.Connection.CreateCommand();

            command.CommandText = commandText;
            command.CommandType = commandType;
            //command.Transaction = GetActiveTransaction();

            foreach (var parameter in parameters)
            {
                command.Parameters.Add(parameter);
            }

            return command;
        }
    }
}
