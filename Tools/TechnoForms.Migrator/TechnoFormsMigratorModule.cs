using System.Data.Entity;
using System.Reflection;
using Abp.Events.Bus;
using Abp.Modules;
using Castle.MicroKernel.Registration;
using TechnoForms.EntityFramework;
using Abp.Configuration.Startup;
using Abp.EntityFramework.Uow;
using Abp.Dependency;

namespace TechnoForms.Migrator
{
    [DependsOn(typeof(TechnoFormsDataModule))]
    public class TechnoFormsMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<TechnoFormsDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
            Configuration.ReplaceService<IEfTransactionStrategy, DbContextEfTransactionStrategy>(DependencyLifeStyle.Transient);
            Configuration.ReplaceService(typeof(IEventBus), () =>
            {
                IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                );
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}