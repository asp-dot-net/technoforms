﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.ModeOfPayments;
using TechnoForms.Payment.Dto;
using TechnoForms.Payments;
using Abp.Linq.Extensions;using System.Linq.Dynamic;

namespace TechnoForms.Payment
{
    //[AbpAuthorize(AppPermissions.Pages_Invoice)]
    public class PaymentAppService : TechnoFormsAppServiceBase, IPaymentAppService
    {
        private readonly IRepository<TechnoForms.Payments.Payment> _paymentRepository;
        private readonly IRepository<ModeOfPayment> _modeOfPaymentRepository;
        private readonly IRepository<TechnoForms.Invoices.Invoice> _invoiceRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public PaymentAppService(IRepository<TechnoForms.Payments.Payment> paymentRepository,
            IRepository<ModeOfPayment> modeOfPaymentRepository,
            IRepository<TechnoForms.Invoices.Invoice> invoiceRepository,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _paymentRepository = paymentRepository;
            _modeOfPaymentRepository = modeOfPaymentRepository;
            _invoiceRepository = invoiceRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        public async Task CreatePayments(CreatePaymentInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var Payment = input.MapTo<TechnoForms.Payments.Payment>();
                await _paymentRepository.InsertAsync(Payment);

                decimal invoiceamount = (_invoiceRepository.GetAll().Where(p => p.TenantId == input.TenantId && p.Id == input.Invoice_ID).Select(p => p.InvoiceAmount).FirstOrDefault());
                decimal paidamount = input.amount;
                int totalpaidcount = (_paymentRepository.GetAll().Where(p => p.Invoice_ID == input.Invoice_ID && p.TenantId == input.TenantId).ToList().Count);
                if (totalpaidcount > 0)
                {
                    paidamount += (_paymentRepository.GetAll().Where(p => p.Invoice_ID == input.Invoice_ID && p.TenantId == input.TenantId).Select(p => p.amount).Sum());
                }
                if (paidamount == invoiceamount)
                {
                    var invoice = await _invoiceRepository.GetAsync(input.Invoice_ID);
                    invoice.Invoice_Status = 1;
                    await _invoiceRepository.UpdateAsync(invoice);
                }


            }
        }

        public ListResultDto<ModeOfPaymentListDto> GetModeOfPayment()
        {
            var ModeOfPayment = _modeOfPaymentRepository
                .GetAll()
                .OrderByDescending(p => p.Id)
                .ToList();
            var ModeOfPaymentlist = ModeOfPayment.MapTo<List<ModeOfPaymentListDto>>();
            return new ListResultDto<ModeOfPaymentListDto>(ModeOfPaymentlist);

        }
        public async Task<PagedResultDto<GetPaymentListDto>> GetPaymentListAsync(PaymentInputDto input)
        {
            var Payment = _paymentRepository
               .GetAll()
               .Where(p => p.Invoice_ID == input.InvoiceId);

            var resultCount = await Payment.CountAsync();
            var Payment_list = new List<GetPaymentListDto>();

            if (input.Sorting == "modeofpaymentName asc")
            {
                input.Sorting = "Modeofpayment_Id asc";
            }
            if (input.Sorting == "modeofpaymentName desc")
            {
                input.Sorting = "Modeofpayment_Id desc";
            }
            var results = await Payment
            .AsNoTracking()
            .OrderBy(input.Sorting)
            .PageBy(input)
            .ToListAsync();

            Payment_list = results.MapTo<List<GetPaymentListDto>>();
            foreach (var item in Payment_list)
            {
                item.ModeofpaymentName = _modeOfPaymentRepository.GetAll().Where(p => p.Id == item.Modeofpayment_Id).Select(p => p.ModeOfPaymentName).FirstOrDefault();
            }
            return new PagedResultDto<GetPaymentListDto>(resultCount, Payment_list.MapTo<List<GetPaymentListDto>>());
        }

    }
}
