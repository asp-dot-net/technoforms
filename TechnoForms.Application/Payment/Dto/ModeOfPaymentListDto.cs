﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.ModeOfPayments;

namespace TechnoForms.Payment.Dto
{
    [AutoMapFrom(typeof(ModeOfPayment))]
    public class ModeOfPaymentListDto : FullAuditedEntityDto
    {
        public string ModeOfPaymentName { get; set; }
    }

}
