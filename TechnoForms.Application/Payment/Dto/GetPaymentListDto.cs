﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Dto;

namespace TechnoForms.Payment.Dto
{
    [AutoMapFrom(typeof(TechnoForms.Payments.Payment))]
    public class GetPaymentListDto : FullAuditedEntityDto
    {
        public int Invoice_ID { get; set; }
        public int Modeofpayment_Id { get; set; }
        public string date { get; set; }
        public string InstrumentDetail { get; set; }
        public decimal amount { get; set; }
        public string ModeofpaymentName { get; set; }
    }
}
