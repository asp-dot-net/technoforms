﻿using Abp.Runtime.Validation;
using TechnoForms.Dto;

namespace TechnoForms.Payment.Dto
{
    public class PaymentInputDto : PagedAndSortedInputDto, IShouldNormalize
    {
        public int InvoiceId { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
