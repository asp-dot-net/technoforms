﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Payment.Dto
{
    [AutoMapTo(typeof(TechnoForms.Payments.Payment))]
    public class CreatePaymentInput
    {
        [Required]
        public int Invoice_ID { get; set; }

        [Required]
        public int Modeofpayment_Id { get; set; }

        [Required]
        public string date { get; set; }

        [MaxLength(TechnoForms.Payments.Payment.MaxInstrumentDetailLength)]
        public string InstrumentDetail { get; set; }

        [Required]
        public decimal amount { get; set; }

        public int TenantId { get; set; }
    }
}
