﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Payment.Dto;

namespace TechnoForms.Payment
{
    public interface IPaymentAppService : IApplicationService
    {
        Task CreatePayments(CreatePaymentInput input);
        ListResultDto<ModeOfPaymentListDto> GetModeOfPayment();
        Task<PagedResultDto<GetPaymentListDto>> GetPaymentListAsync(PaymentInputDto input);
    }
}
