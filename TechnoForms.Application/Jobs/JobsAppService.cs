﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Runtime.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.CustomerInstallers;
using TechnoForms.InstallerEmployes;
using TechnoForms.Installers;
using TechnoForms.Installers.Dto;
using TechnoForms.Inverters;
using TechnoForms.Jobs.Dto;
using TechnoForms.JobStatusName;
using TechnoForms.MultiTenancy;
using TechnoForms.Panels;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;
using TechnoForms.PostCodes;
using TechnoForms.PostCodes.Dto;
using TechnoForms.States;
using TechnoForms.States.Dto;
using TechnoForms.STCPostCodes;
using TechnoForms.StreetName;
using TechnoForms.StreetTypes;
using TechnoForms.UnitTypes;
using Abp.Linq.Extensions;using System.Linq.Dynamic;
using System.Data.Entity;
using TechnoForms.ScanedPanels;
using TechnoForms.ScanedInverters;
using TechnoForms.Authorization.Users;
using TechnoForms.Photos;
using TechnoForms.Web;
using TechnoForms.PhotoTypes;
using TechnoForms.OldSystemDirections;
using Abp.Net.Mail;
using System.Web;
using System.Net;
using System.IO;
using System.Data;
using Abp.BackgroundJobs;
using TechnoForms.BackgroundJobCompliance;
using TechnoForms.STCZoneRatings;
using TechnoForms.Energy;

namespace TechnoForms.Jobs
{
	[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs)]
	public class JobsAppService : TechnoFormsAppServiceBase, IJobsAppService
	{
		private readonly IRepository<Job> _jobRepository;
		private readonly IRepository<JobBulkUpload> _jobBulkUploadRepository;
		private readonly IRepository<PostalAddressType> _postalAddressTypeRepository;
		private readonly IRepository<PostalDeliveryType> _postalDeliveryTypeRepository;
		private readonly IRepository<UnitType> _unitTypeRepository;
		private readonly IRepository<StreetNames> _streetNamesRepository;
		private readonly IRepository<StreetType> _streetTypeRepository;
		private readonly IRepository<PostCode> _postCodeRepository;
		private readonly IRepository<HouseType> _houseTypeRepository;
		private readonly IRepository<RoofType> _roofTypeRepository;
		private readonly IRepository<Panel> _panelRepository;
		private readonly IRepository<Inverter> _inverterRepository;
		private readonly IRepository<SolarType> _solarTypeRepository;
		private readonly IRepository<SystemType> _systemTypeRepository;
		private readonly IRepository<RoofFacingPanel> _roofFacingPanelRepository;
		private readonly IRepository<YesNo> _yesNoRepository;
		private readonly IRepository<TypeOfSystem> _typeOfSystemRepository;
		private readonly IRepository<TypeOfConnection> _typeOfConnectionRepository;
		private readonly IRepository<MountingType> _mountingTypeRepository;
		private readonly IRepository<RecYear> _recyearRepository;
		private readonly IRepository<State> _stateRepository;
		private readonly IRepository<Installer> _installerRepository;
		private readonly IRepository<JobStatus> _jobStatusRepository;
		private readonly IRepository<InstallerEmploye> _installerEmployeRepository;
		private readonly IRepository<SystemMountingType> _systemMountingTypeRepository;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IRepository<Tenant> _tenantRepository;
		private readonly IRepository<STCPostCode> _stcPostCodeRepository;
		private readonly IRepository<CustomerInstaller> _customerInstallerRepository;
		private readonly IAbpSession _session;
		private readonly IRepository<ScanedPanel> _scanedPanelRepository;
		private readonly IRepository<ScanedInverter> _scanedInverterRepository;
		private readonly IRepository<User, long> _userRepository;
		private readonly IRepository<Photo> _photoRepository;
		private readonly IRepository<PhotoType> _photoTypeRepository;
		private readonly IRepository<OldSystemDirection> _oldSystemDirectionRepository;
		private readonly IRepository<CustomerSignLog> _customerSignLogRepository;
		private readonly IRepository<Token> _tokenRepository;
		private readonly IRepository<CustomerSignLogResponse> _customerSignLogResponseRepository;
		private readonly IRepository<STCZoneRating> _STCZoneRatingRepository;
		private readonly IEmailSender _emailSender;
		private readonly IWebUrlService _webUrlService;
		private readonly ICurrentUnitOfWorkProvider _unitOfWorkProvider;
		private readonly IUserEmailer _userEmailer;
		private readonly IBackgroundJobManager _backgroundJobManager;
		private readonly IJobRepository _jobListRepository;
		private readonly IRepository<EnergyDestributar> _energyDestributarRepository;
		private readonly IRepository<EnergyRetailer> _energyRetailerRepository;

		public JobsAppService(
		  IRepository<Job> jobRepository,
		  IRepository<JobBulkUpload> jobBulkUploadRepository,
		  IRepository<PostalAddressType> PostalAddressTypeRepository,
		  IRepository<PostalDeliveryType> PostalDeliveryTypeRepository,
		  IRepository<UnitType> UnitTypeRepository,
		  IRepository<StreetNames> StreetNamesRepository,
		  IRepository<StreetType> StreetTypeRepository,
		  IRepository<PostCode> postCodeRepository,
		  IRepository<HouseType> houseTypeRepository,
		  IRepository<RoofType> roofTypeRepository,
		  IRepository<Panel> panelRepository,
		  IRepository<Inverter> inverterRepository,
		  IRepository<SolarType> solarTypeRepository,
		  IRepository<SystemType> systemTypeRepository,
		  IRepository<RoofFacingPanel> roofFacingPanelRepository,
		  IRepository<YesNo> yesNoRepository,
		  IRepository<TypeOfSystem> typeOfSystemRepository,
		  IRepository<TypeOfConnection> typeOfConnectionRepository,
		  IRepository<MountingType> mountingTypeRepository,
		  IRepository<RecYear> recyearRepository,
		  IRepository<State> stateRepository,
		  IRepository<Installer> installerRepository,
		  IRepository<JobStatus> jobStatusRepository,
		  IRepository<InstallerEmploye> installerEmployeRepository,
		  IRepository<SystemMountingType> systemMountingTypeRepository,
		  IUnitOfWorkManager unitOfWorkManager,
		  IRepository<Tenant> tenantRepository,
		  IRepository<STCPostCode> stcPostCodeRepository,
		  IRepository<CustomerInstaller> CustomerRepository,
		  IAbpSession session,
		  IRepository<ScanedPanel> scanedPanelRepository,
		  IRepository<ScanedInverter> scanedInverterRepository,
		  IRepository<User, long> UserRepository,
		  IRepository<Photo> photoRepository,
		  IRepository<PhotoType> photoTypeRepository,
		  IRepository<OldSystemDirection> oldSystemDirectionRepository,
		  IRepository<CustomerSignLog> customerSignLogRepository,
		  IRepository<Token> tokenRepository,
		  IRepository<CustomerSignLogResponse> customerSignLogResponseRepository,
		  IRepository<STCZoneRating> STCZoneRatingRepository,
		  IEmailSender emailSender,
		  IWebUrlService webUrlService,
		  ICurrentUnitOfWorkProvider unitOfWorkProvider,
		  IUserEmailer userEmailer,
		  IBackgroundJobManager backgroundJobManager,
		  IJobRepository jobListRepository,
		  IRepository<EnergyDestributar> energyDestributarRepository,
		  IRepository<EnergyRetailer> energyRetailerRepository)
		{
			_jobRepository = jobRepository;
			_jobBulkUploadRepository = jobBulkUploadRepository;
			_postalAddressTypeRepository = PostalAddressTypeRepository;
			_postalDeliveryTypeRepository = PostalDeliveryTypeRepository;
			_unitTypeRepository = UnitTypeRepository;
			_streetTypeRepository = StreetTypeRepository;
			_streetNamesRepository = StreetNamesRepository;
			_postCodeRepository = postCodeRepository;
			_houseTypeRepository = houseTypeRepository;
			_roofTypeRepository = roofTypeRepository;
			_panelRepository = panelRepository;
			_inverterRepository = inverterRepository;
			_solarTypeRepository = solarTypeRepository;
			_systemTypeRepository = systemTypeRepository;
			_roofFacingPanelRepository = roofFacingPanelRepository;
			_yesNoRepository = yesNoRepository;
			_typeOfSystemRepository = typeOfSystemRepository;
			_typeOfConnectionRepository = typeOfConnectionRepository;
			_mountingTypeRepository = mountingTypeRepository;
			_recyearRepository = recyearRepository;
			_stateRepository = stateRepository;
			_installerRepository = installerRepository;
			_jobStatusRepository = jobStatusRepository;
			_installerEmployeRepository = installerEmployeRepository;
			_systemMountingTypeRepository = systemMountingTypeRepository;
			_unitOfWorkManager = unitOfWorkManager;
			_tenantRepository = tenantRepository;
			_stcPostCodeRepository = stcPostCodeRepository;
			_customerInstallerRepository = CustomerRepository;
			_session = session;
			_scanedPanelRepository = scanedPanelRepository;
			_scanedInverterRepository = scanedInverterRepository;
			_userRepository = UserRepository;
			_photoRepository = photoRepository;
			_photoTypeRepository = photoTypeRepository;
			_oldSystemDirectionRepository = oldSystemDirectionRepository;
			_customerSignLogRepository = customerSignLogRepository;
			_tokenRepository = tokenRepository;
			_customerSignLogResponseRepository = customerSignLogResponseRepository;
			_STCZoneRatingRepository = STCZoneRatingRepository;
			_emailSender = emailSender;
			_webUrlService = webUrlService;
			_unitOfWorkProvider = unitOfWorkProvider;
			_userEmailer = userEmailer;
			_backgroundJobManager = backgroundJobManager;
			_jobListRepository = jobListRepository;
			_energyDestributarRepository = energyDestributarRepository;
			_energyRetailerRepository = energyRetailerRepository;
		}

		[UnitOfWork]
		public async Task<PagedResultDto<JobsListDto>> GetJobsAsync(GetJobsInput input)
		{
			//  _backgroundJobManager.Enqueue<ComplianceBackgroundJob, int>(2, BackgroundJobPriority.Normal);

			long userid = (long)_session.UserId;
			var user = await UserManager.GetUserByIdAsync(userid);
			var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);
			var userspecific = grantedPermissions.Where(x => x.Name == "Pages.UserSpecificJob").ToList();

			var job = _jobRepository
			 .GetAll()
			 .WhereIf(
				  !input.Filter.IsNullOrEmpty(),
				  p => p.ProjectNumber.ToLower().Contains(input.Filter.ToLower())
			  )
			.WhereIf(
				  input.verified,
				  J => J.IsVerifyCS == true && J.IsVerifyCUCK == true && J.IsVerifyELE == true && J.IsVerifyImages == true && J.IsVerifyIN == true && J.IsVerifyIS == true && J.IsVerifyPA == true && J.IsVerifySTC == true
			)
			.WhereIf(
				  !input.verified,
				  J => (J.IsVerifyCS == false || J.IsVerifyCS == null) || (J.IsVerifyCUCK == false || J.IsVerifyCUCK == null) || (J.IsVerifyELE == false || J.IsVerifyELE == null) || (J.IsVerifyImages == false || J.IsVerifyImages == null) || (J.IsVerifyIN == false || J.IsVerifyIN == null) || (J.IsVerifyIS == false || J.IsVerifyIS == null) || (J.IsVerifyPA == false || J.IsVerifyPA == null) || (J.IsVerifySTC == false || J.IsVerifySTC == null)
			)
			.WhereIf(
				  !input.filterFormId.IsNullOrEmpty(),
				  p => p.FormId.ToLower().Contains(input.filterFormId.ToLower())
			  )
			  .WhereIf(
				  !input.filterOwnerName.IsNullOrEmpty(),
				  p => p.FirstName.ToLower().Contains(input.filterOwnerName.ToLower()) ||
				  p.LastName.ToLower().Contains(input.filterOwnerName.ToLower())
			  )
			  .WhereIf(
				  !input.filterCity.IsNullOrEmpty(),
				  p => p.StreetCity.ToLower().Contains(input.filterCity.ToLower())
			  )
			  .WhereIf(
					!input.filterState.IsNullOrEmpty(),
					p => p.StreetState.ToLower().Contains(input.filterState.ToLower())
			  )
			  .WhereIf(
				  (!string.IsNullOrEmpty(input.filterInstallerId.ToString()) && input.filterInstallerId != 0),
				  p => p.CreateFor_Id == input.filterInstallerId
			  )

			  .WhereIf(
				  (!string.IsNullOrEmpty(input.filterSystemtypeId.ToString()) && input.filterSystemtypeId != 0),
				  p => p.SystemType_Id == input.filterSystemtypeId
			  )
			  .WhereIf(
				  (!string.IsNullOrEmpty(input.filterJobStatusId.ToString()) && input.filterJobStatusId != 0),
				  p => p.JobStatus_Id == input.filterJobStatusId
			  );

			if (!string.IsNullOrEmpty(input.filterStartDate.ToString()) && !string.IsNullOrEmpty(input.filterEndDate.ToString()))
			//&& input.filterStartDate.Year != 1 && input.filterEndDate.Year != 1)
			{
				if (input.filterDateypeId == 1 || input.filterDateypeId == 0)
				{
					job = job.Where(p => p.ExpectedInstallDate >= input.filterStartDate && p.ExpectedInstallDate <= input.filterEndDate);
				}
				if (input.filterDateypeId == 2)
				{
					var panel = _panelRepository.GetAll().Where(p => p.ExpiryDate >= input.filterStartDate && p.ExpiryDate <= input.filterEndDate).ToList();
					var panelIds = "";
					foreach (var itempanel in panel)
					{
						panelIds += "," + itempanel.Id;
					}
					if (panelIds.StartsWith(","))
					{
						panelIds = panelIds.Remove(0, 1);
					}
					var pIds = panelIds.Split(',').Select(s => int.Parse(s));

					job = job.Where(p => pIds.Contains(p.Panel_id));
				}
				if (input.filterDateypeId == 3)
				{
					var inverter = _inverterRepository.GetAll().Where(p => p.ExpiryDate >= input.filterStartDate && p.ExpiryDate <= input.filterEndDate).ToList();
					var inverterIds = "";
					foreach (var iteminverter in inverter)
					{
						inverterIds += "," + iteminverter.Id;
					}
					if (inverterIds.StartsWith(","))
					{
						inverterIds = inverterIds.Remove(0, 1);
					}
					var InvIds = inverterIds.Split(',').Select(s => int.Parse(s));

					job = job.Where(p => InvIds.Contains(p.Inverter_id));
				}
				if (input.filterDateypeId == 4)
				{
					var installer = _installerEmployeRepository.GetAll().Where(p => p.InsAccreExDate >= input.filterStartDate && p.InsAccreExDate <= input.filterEndDate).ToList();
					var installerIds = "";
					foreach (var iteminverter in installer)
					{
						installerIds += "," + iteminverter.Id;
					}
					if (installerIds.StartsWith(","))
					{
						installerIds = installerIds.Remove(0, 1);
					}
					var InstIds = installerIds.Split(',').Select(s => int.Parse(s));

					job = job.Where(p => InstIds.Contains(p.Installer_Id));
				}
			}

			#region Received
			if (input.filterReceivedId != null)
			{
				if (input.filterReceivedId.Count() > 0)
				{
					for (int i = 0; i < input.filterReceivedId.Count(); i++)
					{
						var jobphotoguid_list = _photoRepository.GetAll().ToList();
						var jobphotoIds = "";
						if (!string.IsNullOrEmpty(jobphotoIds) && input.filterReceivedId[i] == 3)
						{
							foreach (var itemphoto in jobphotoguid_list)
							{
								jobphotoIds += "," + itemphoto.Guid;
							}
							if (jobphotoIds.StartsWith(","))
							{
								jobphotoIds = jobphotoIds.Remove(0, 1);
							}
						}
						var jobval = job.ToList();
						var jobScanedPanelIds = "";
						if (!string.IsNullOrEmpty(jobScanedPanelIds) && input.filterReceivedId[i] == 1)
						{
							for (int j = 0; j < jobval.Count(); j++)
							{
								var jobguid = jobval[i].Guid;
								var totalpanel = jobval[i].NoOfPanels;
								var totalScanedPanel = (_scanedPanelRepository.GetAll().Where(p => p.Guid == jobguid)).ToList();
								if (totalpanel == totalScanedPanel.Count)
								{
									jobScanedPanelIds += "," + jobval[i].Id;
								}
							}
						}
						var jobScanedInverterIds = "";
						if (!string.IsNullOrEmpty(jobScanedInverterIds) && input.filterReceivedId[i] == 2)
						{
							for (int j = 0; j < jobval.Count(); j++)
							{
								var jobguid = jobval[i].Guid;
								var totalInverter = jobval[i].NoOfInverter;
								var ScanedInverter = (_scanedInverterRepository.GetAll().Where(p => p.Guid == jobguid)).ToList();
								if (totalInverter == ScanedInverter.Count)
								{
									jobScanedInverterIds += "," + jobval[i].Id;
								}
							}
						}
						if (input.filterReceivedId[i] == 4 || input.filterReceivedId[i] == 5 || input.filterReceivedId[i] == 3 || input.filterReceivedId[i] == 1 || input.filterReceivedId[i] == 2)
						{
							try
							{
								job = job.Where(p =>
						   (p.SignatureImageInst != null && p.SignatureImageInst != string.Empty) ||
						   (p.SignatureImageOwner != null && p.SignatureImageOwner != string.Empty) ||
						   (jobScanedPanelIds.Contains(p.Id.ToString())) ||
						   (jobScanedInverterIds.Contains(p.Id.ToString())) ||
						   (jobphotoIds.Contains(p.Guid.ToString()))
						   );
							}
							catch (Exception e)
							{ }
						}
					}
				}
			}
			#endregion

			#region Not Received
			if (input.filterNotReceivedId != null)
			{
				if (input.filterNotReceivedId.Count() > 0)
				{
					for (int i = 0; i < input.filterNotReceivedId.Count(); i++)
					{
						var jobphotoguid_list = _photoRepository.GetAll().ToList();
						var jobphotoIds = "";
						if (!string.IsNullOrEmpty(jobphotoIds) && input.filterNotReceivedId[i] == 3)
						{
							foreach (var itemphoto in jobphotoguid_list)
							{
								jobphotoIds += "," + itemphoto.Guid;
							}
							if (jobphotoIds.StartsWith(","))
							{
								jobphotoIds = jobphotoIds.Remove(0, 1);
							}
						}
						var jobval = job.ToList();
						var jobScanedPanelIds = "";
						if (!string.IsNullOrEmpty(jobScanedPanelIds) && input.filterNotReceivedId[i] == 1)
						{
							for (int j = 0; j < jobval.Count(); j++)
							{
								var jobguid = jobval[i].Guid;
								var totalpanel = jobval[i].NoOfPanels;
								var totalScanedPanel = (_scanedPanelRepository.GetAll().Where(p => p.Guid == jobguid)).ToList();
								if (totalpanel > totalScanedPanel.Count)
								{
									jobScanedPanelIds += "," + jobval[i].Id;
								}
							}
						}
						var jobScanedInverterIds = "";
						if (!string.IsNullOrEmpty(jobScanedInverterIds) && input.filterNotReceivedId[i] == 2)
						{
							for (int j = 0; j < jobval.Count(); j++)
							{
								var jobguid = jobval[i].Guid;
								var totalInverter = jobval[i].NoOfInverter;
								var ScanedInverter = (_scanedInverterRepository.GetAll().Where(p => p.Guid == jobguid)).ToList();
								if (totalInverter > ScanedInverter.Count)
								{
									jobScanedInverterIds += "," + jobval[i].Id;
								}
							}
						}
						if (input.filterNotReceivedId[i] == 4 || input.filterNotReceivedId[i] == 5 || input.filterNotReceivedId[i] == 3 || input.filterNotReceivedId[i] == 1 || input.filterNotReceivedId[i] == 2)
						{
							try
							{
								job = job.Where(p =>
						   (p.SignatureImageInst == null && p.SignatureImageInst == string.Empty) ||
						   (p.SignatureImageOwner == null && p.SignatureImageOwner == string.Empty) ||
						   (jobScanedPanelIds.Contains(p.Id.ToString())) ||
						   (jobScanedInverterIds.Contains(p.Id.ToString())) ||
						   (jobphotoIds.Contains(p.Guid.ToString()))
						   );
							}
							catch (Exception e)
							{ }
						}
					}
				}
			}
			#endregion

			var resultCount = await job.CountAsync();

			if (input.Sorting == "fullName asc")
			{
				input.Sorting = "FirstName asc";
			}
			if (input.Sorting == "fullName desc")
			{
				input.Sorting = "FirstName desc";
			}
			if (input.Sorting == "installerName asc")
			{
				input.Sorting = "Installer_Id asc";
			}
			if (input.Sorting == "installerName desc")
			{
				input.Sorting = "Installer_Id desc";
			}
			if (input.Sorting == "createForName asc")
			{
				input.Sorting = "CreateFor_Id asc";
			}
			if (input.Sorting == "createForName desc")
			{
				input.Sorting = "CreateFor_Id desc";
			}
			var results = await job
		  .AsNoTracking()
		  .OrderBy(input.Sorting)
		  .PageBy(input)
		  .ToListAsync();

			if (userspecific.Count > 0)
			{
				results = results.Where(x => x.CreatorUserId == userid).ToList();
			}
			var joblist = results.MapTo<List<JobsListDto>>();
			foreach (var item in joblist)
			{
				item.FullName = item.FirstName + " " + item.LastName;
				item.StreetAddress = item.StreetAddress + ", " + item.StreetCity + ", " + item.StreetState + ", " + item.StreetPostCode;
				//var latitute = getLatLong(item.StreetAddress);
				item.NameWithAddress = HttpUtility.HtmlDecode("<div class='jobownerinfo ui-grid-cell-contents'><p><span class='ownername'>" + item.FullName + "</span> <span class='addressphonebox'><span class='streetaddress'>" + item.StreetAddress + "</span><span class='mobnumber'>" + item.Phone + "</span></span><p></div>");
				item.JobStatusName = _jobStatusRepository.GetAll().Where(p => p.Id == item.JobStatus_Id).Select(p => p.JobStatusName).FirstOrDefault();

				using (_unitOfWorkManager.Current.SetTenantId(null))
				{
					item.CreateForName = _installerRepository.GetAll().Where(p => p.Id == item.CreateFor_Id).Select(p => p.FullName).FirstOrDefault();
					item.InstallerName = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).Select(p => p.FullName).FirstOrDefault();
					item.DesignerName = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).Select(p => p.FullName).FirstOrDefault();
					item.ElectricianName = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Electrician_Id).Select(p => p.FullName).FirstOrDefault();
					item.HouseTypeName = _houseTypeRepository.GetAll().Where(p => p.Id == item.HouseType).Select(p => p.HouseTypeName).FirstOrDefault();
					item.RoofTypeName = _roofTypeRepository.GetAll().Where(p => p.Id == item.RoofType).Select(p => p.RoofTypeName).FirstOrDefault();
					//item.SolarTypeName = _solarTypeRepository.GetAll().Where(p => p.Id == item.SolarType_Id).Select(p => p.SolarTypeeName).FirstOrDefault();
					item.SystemTypeName = _systemTypeRepository.GetAll().Where(p => p.Id == item.SystemType_Id).Select(p => p.SystemTypeName).FirstOrDefault();
					//item.RoofFacingPanelName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == item.RoofFacingPanel_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
					//item.SugstthisaddressName = _yesNoRepository.GetAll().Where(p => p.Id == item.Sugstthisaddress_Id).Select(p => p.YesNoName).FirstOrDefault();
					//item.LocationOfNewSystemName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == item.LocationOfNewSystem_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
					//item.TypeofsystemName = _typeOfSystemRepository.GetAll().Where(p => p.Id == item.Typeofsystem_Id).Select(p => p.TypeOfSystemName).FirstOrDefault();
					//item.TypeofconnectionName = _typeOfConnectionRepository.GetAll().Where(p => p.Id == item.Typeofconnection_Id).Select(p => p.TypeOfConnectionName).FirstOrDefault();
					//item.MountingtypeName = _mountingTypeRepository.GetAll().Where(p => p.Id == item.Mountingtype_Id).Select(p => p.MountingTypeName).FirstOrDefault();
					//item.AuditreportavailableName = _yesNoRepository.GetAll().Where(p => p.Id == item.Auditreportavailable_Id).Select(p => p.YesNoName).FirstOrDefault();
					//item.InstallingcompleteunitName = _yesNoRepository.GetAll().Where(p => p.Id == item.Installingcompleteunit_Id).Select(p => p.YesNoName).FirstOrDefault();
					//item.RECYearName = _recyearRepository.GetAll().Where(p => p.Id == item.RECYear_Id).Select(p => p.REC_Year).FirstOrDefault();
					//item.DefaultresourceavailfigureName = _yesNoRepository.GetAll().Where(p => p.Id == item.Defaultresourceavailfigure_Id).Select(p => p.YesNoName).FirstOrDefault();
					//item.CeatingCertName = _yesNoRepository.GetAll().Where(p => p.Id == item.CeatingCert_Id).Select(p => p.YesNoName).FirstOrDefault();
					//item.CECAccreditationStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.CECAccreditationStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
					//item.GovernmentSitingApprovalsName = _yesNoRepository.GetAll().Where(p => p.Id == item.GovernmentSitingApprovals_Id).Select(p => p.YesNoName).FirstOrDefault();
					//item.ElectricalSafetyDocumentationName = _yesNoRepository.GetAll().Where(p => p.Id == item.ElectricalSafetyDocumentation_Id).Select(p => p.YesNoName).FirstOrDefault();
					//item.AustralianStandardsStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.AustralianStandardsStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
					//item.NotgridConnectedStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.NotgridConnectedStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
					//item.SGUatThisAddressName = _yesNoRepository.GetAll().Where(p => p.Id == item.SGUatThisAddress_Id).Select(p => p.YesNoName).FirstOrDefault();
					//item.SystemMountingTypeName = _systemMountingTypeRepository.GetAll().Where(p => p.Id == item.SystemMountingType_Id).Select(p => p.SystemMountingTypeName).FirstOrDefault();
					//item.OwnerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == item.OwnerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
					//item.CustomerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == item.CustomerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
					item.PanelModel = _panelRepository.GetAll().Where(p => p.Id == item.Panel_id).Select(p => p.ModelNo).FirstOrDefault();
					item.InverterModel = _inverterRepository.GetAll().Where(p => p.Id == item.Inverter_id).Select(p => p.ModelNumber).FirstOrDefault();
					item.PanelLogo = _panelRepository.GetAll().Where(p => p.Id == item.Panel_id).Select(p => p.PanelLogo).FirstOrDefault();
					item.InverterLogo = _inverterRepository.GetAll().Where(p => p.Id == item.Inverter_id).Select(p => p.InverterLogo).FirstOrDefault();
					item.PanelLogoPath = WebUrlService.WebSiteRootAddress + "Temp/PanelLogo/" + item.PanelLogo;
					item.PanelLogoPath = AppendTenantName(item.TenantId, item.PanelLogoPath);
					item.InverterLogoPath = WebUrlService.WebSiteRootAddress + "Temp/InverterLogo/" + item.InverterLogo;
					item.InverterLogoPath = AppendTenantName(item.TenantId, item.InverterLogoPath);
				}
				item.JobScanedPanel_List = (from itempanel in _scanedPanelRepository.GetAll()
											where itempanel.Guid == item.Guid
											select new JobsListDto.JobScanedPanel
											{
												Id = itempanel.Id,
												PanelSerialNumber = itempanel.PanelSerialNumber,
												PanelDeleteLink = "grid.appScope.jobScanedPaneldelete(" + itempanel.Id + ")"
											}).ToList();
				for (int i = 0; i < item.JobScanedPanel_List.Count; i++)
				{
					if (i == 0)
					{
						item.ScannedPanelSerialNoList = item.JobScanedPanel_List[i].PanelSerialNumber;
					}
					else
					{
						item.ScannedPanelSerialNoList = item.ScannedPanelSerialNoList + ", " + item.JobScanedPanel_List[i].PanelSerialNumber;
					}
				}

				item.JobScanedInverter_List = (from itemInverter in _scanedInverterRepository.GetAll()
											   where itemInverter.Guid == item.Guid
											   select new JobsListDto.JobScanedInverter
											   {
												   Id = itemInverter.Id,
												   InverterSerialNumber = itemInverter.InverterSerialNumber
											   }).ToList();
				for (int i = 0; i < item.JobScanedInverter_List.Count; i++)
				{
					if (i == 0)
					{
						item.ScannedInverterSerialNoList = item.JobScanedInverter_List[i].InverterSerialNumber;
					}
					else
					{
						item.ScannedInverterSerialNoList = item.ScannedInverterSerialNoList + ", " + item.JobScanedInverter_List[i].InverterSerialNumber;
					}
				}
				if (item.SystemType_Id == 1)
				{
					item.GridClass = "firstTime";
				}
				else
				{
					item.GridClass = "maintenance";
				}

				item.ScanedPanelClass = "dcbox disabled";
				item.ScanedPanelCount = item.JobScanedPanel_List.Count;
				if (_scanedPanelRepository.GetAll().Where(p => p.Guid == item.Guid && p.PanelBarcodePhoto != "NULL").ToList().Count > 0)
				{
					item.ScanedPanelClass = "dcbox";
					if (!string.IsNullOrEmpty(item.IsVerifyPA.ToString()))
					{
						if ((bool)item.IsVerifyPA)
						{
							item.ScanedPanelClass = "dcbox verified";
						}
					}
				}
				item.ScanedInverterClass = "dcbox disabled";
				item.ScanedInverterCount = item.JobScanedInverter_List.Count;
				if (_scanedInverterRepository.GetAll().Where(p => p.Guid == item.Guid && p.InverterBarcodePhoto != "NULL").ToList().Count > 0)
				{
					item.ScanedInverterClass = "dcbox";
					if (!string.IsNullOrEmpty(item.IsVerifyIN.ToString()))
					{
						if ((bool)item.IsVerifyIN)
						{
							item.ScanedInverterClass = "dcbox verified";
						}
					}
				}

				item.OtherImagesClass = "dcbox disabled";
				if ((_photoRepository.GetAll().Where(p => p.Guid == item.Guid).Count()) > 0)
				{
					item.OtherImagesClass = "dcbox";
					if (!string.IsNullOrEmpty(item.IsVerifyImages.ToString()))
					{
						if ((bool)item.IsVerifyImages)
						{
							item.OtherImagesClass = "dcbox verified";
						}
					}
				}

				item.CustomerSignatureClass = "dcbox disabled";
				if (!string.IsNullOrEmpty(item.OwnerPhoto) || !string.IsNullOrEmpty(item.SignatureImageOwner))
				{
					item.CustomerSignatureClass = "dcbox";
					if (!string.IsNullOrEmpty(item.IsVerifyCS.ToString()))
					{
						if ((bool)item.IsVerifyCS)
						{
							item.CustomerSignatureClass = "dcbox verified";
						}
					}
				}

				item.InstallerSignatureClass = "dcbox disabled";
				if (!string.IsNullOrEmpty(item.InstallerPhoto) || !string.IsNullOrEmpty(item.SignatureImageInst))
				{
					item.InstallerSignatureClass = "dcbox";
					if (!string.IsNullOrEmpty(item.IsVerifyIS.ToString()))
					{
						if ((bool)item.IsVerifyIS)
						{
							item.InstallerSignatureClass = "dcbox verified";
						}
					}
				}

				item.CecSignatureClass = "dcbox";
				if (!string.IsNullOrEmpty(item.IsVerifySTC.ToString()))
				{
					if ((bool)item.IsVerifySTC)
					{
						item.CecSignatureClass = "dcbox verified";
					}
				}

				item.EleSignatureClass = "dcbox";
				if (!string.IsNullOrEmpty(item.IsVerifyELE.ToString()))
				{
					if ((bool)item.IsVerifyELE)
					{
						item.EleSignatureClass = "dcbox verified";
					}
				}

				item.CuckSignatureClass = "dcbox";
				if (!string.IsNullOrEmpty(item.IsVerifyCUCK.ToString()))
				{
					if ((bool)item.IsVerifyCUCK)
					{
						item.CuckSignatureClass = "dcbox verified";
					}
				}

				if (!string.IsNullOrEmpty(item.LastModifierUserId.ToString()))
				{
					item.lastModifierUserName = _userRepository.GetAll().Where(p => p.Id == item.LastModifierUserId).Select(p => p.Name).FirstOrDefault() + " " + _userRepository.GetAll().Where(p => p.Id == item.LastModifierUserId).Select(p => p.Surname).FirstOrDefault();
				}
				if (!string.IsNullOrEmpty(item.CreatorUserId.ToString()))
				{
					item.EnteredBy = _userRepository.GetAll().Where(p => p.Id == item.CreatorUserId).Select(p => p.Name).FirstOrDefault() + " " + _userRepository.GetAll().Where(p => p.Id == item.CreatorUserId).Select(p => p.Surname).FirstOrDefault();
				}

				item.InstallerExpire = false;
				item.PanelExpire = false;
				item.InverterExpire = false;
				DateTime todatedate = DateTime.Now;
				using (_unitOfWorkManager.Current.SetTenantId(null))
				{
					var InsAccreExDate = _installerRepository.GetAll().Where(p => p.Id == item.Installer_Id).Select(p => p.InsAccreExDate).FirstOrDefault();
					if (InsAccreExDate != null)
					{
						DateTime InstallerExpiryDate = (DateTime)InsAccreExDate;
						DateTime InstallerExpiry_ReminderDate = InstallerExpiryDate.AddDays(-90);
						item.InstallerExpiryDays = (Math.Round((InstallerExpiryDate - todatedate).TotalDays)).ToString();
						if (InstallerExpiry_ReminderDate <= todatedate)
						{
							item.InstallerExpire = true;
						}
					}
					var panelExDate = _panelRepository.GetAll().Where(p => p.Id == item.Panel_id).Select(p => p.ExpiryDate).FirstOrDefault();
					if (panelExDate != null)
					{
						DateTime PanelExpiryDate = (DateTime)panelExDate;
						DateTime PanelExpiry_ReminderDate = PanelExpiryDate.AddDays(-90);
						item.PanelExpiryDays = (Math.Round((PanelExpiryDate - todatedate).TotalDays)).ToString();
						if (PanelExpiry_ReminderDate <= todatedate)
						{
							item.PanelExpire = true;
						}
					}
					var inverterExDate = _inverterRepository.GetAll().Where(p => p.Id == item.Inverter_id).Select(p => p.ExpiryDate).FirstOrDefault();
					if (inverterExDate != null)
					{
						DateTime InverterExpiryDate = (DateTime)inverterExDate;
						DateTime InverterExpiry_ReminderDate = InverterExpiryDate.AddDays(-90);
						item.InverterExpiryDays = (Math.Round((InverterExpiryDate - todatedate).TotalDays)).ToString();
						if (InverterExpiry_ReminderDate <= todatedate)
						{
							item.InverterExpire = true;
						}
					}

					//var newGuId = _jobRepository.GetAll().Where(p => p.Id == item.Id).Select(p => p.Guid).FirstOrDefault();
					var Scanedpanel_Images = _scanedPanelRepository.GetAll().Where(p => p.Guid == item.Guid && p.PanelBarcodePhoto == "NULL").Select(p => p.Id).ToList();

					item.IsScanedPanelImagesLinkShow = false;
					if (Scanedpanel_Images.Count > 0)
					{
						item.IsScanedPanelImagesLinkShow = true;
					}

					var Scanedinverter_Images = _scanedInverterRepository.GetAll().Where(p => p.Guid == item.Guid && p.InverterBarcodePhoto == "NULL").Select(p => p.Id).ToList();

					item.IsScanedInverterImagesLinkShow = false;
					if (Scanedinverter_Images.Count > 0)
					{
						item.IsScanedInverterImagesLinkShow = true;
					}
					item.IsHistoric = false;
					if (item.IsVerifyCS == true && item.IsVerifyCUCK == true && item.IsVerifyELE == true && item.IsVerifyImages == true && item.IsVerifyIN == true && item.IsVerifyIS == true && item.IsVerifyPA == true && item.IsVerifySTC == true)
					{
						item.IsHistoric = true;
					}

					if (!string.IsNullOrEmpty(item.JobLatitude))
					{
						item.JobLatLong = item.JobLatitude + "," + item.JobLongitude;
					}
				}
			}
			return new PagedResultDto<JobsListDto>(resultCount, joblist.MapTo<List<JobsListDto>>());
		}

		public async Task<PagedResultDto<JobList>> GetJobList(JobListInput input)
		{
			var deptlist = await _jobListRepository.GetJobListAsync(input);

			return new PagedResultDto<JobList>(deptlist.Count, deptlist.MapTo<List<JobList>>());
		}

		public string getLatLong(string streetaddress)
		{
			string latitute = "";

			string url = "https://maps.google.com/maps/api/geocode/xml?address=" + streetaddress + "&sensor=false&key=AIzaSyAryxX4w_6bxddhXvRFprkYrwqoiQueT4k";
			WebRequest request = WebRequest.Create(url);
			using (WebResponse response = (HttpWebResponse)request.GetResponse())
			{
				using (StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8))
				{
					DataSet dsResult = new DataSet();
					dsResult.ReadXml(reader);
					foreach (DataRow row in dsResult.Tables["result"].Rows)
					{
						string geometry_id = dsResult.Tables["geometry"].Select("result_id = " + row["result_id"].ToString())[0]["geometry_id"].ToString();
						DataRow location = dsResult.Tables["location"].Select("geometry_id = " + geometry_id)[0];
						latitute = location["lat"].ToString();
						//dtCoordinates.Rows.Add(row["result_id"], row["formatted_address"], location["lat"], location["lng"]);
					}
				}
			}
			return latitute;
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Create)]
		public async Task CreateJobs(CreateJobsInput input)
		{

			var job = input.MapTo<Job>();
			var getstsinput = new GetSTCInput();
			//int formid = 1001;
			if (string.IsNullOrEmpty(input.PanelModel))
			{
				using (_unitOfWorkManager.Current.SetTenantId(null))
				{
					var PanelModelNo = _panelRepository
					.GetAll()
					.Where(p => p.Id == input.Panel_id).Select(p => p.ModelNo).FirstOrDefault();
					input.PanelModel = PanelModelNo;
				}
			}
			if (string.IsNullOrEmpty(input.PanelModel))
			{
				using (_unitOfWorkManager.Current.SetTenantId(null))
				{
					var InvModelNo = _inverterRepository
					.GetAll()
					.Where(p => p.Id == input.Inverter_id).Select(p => p.ModelNumber).FirstOrDefault();
					input.InverterModel = InvModelNo;
				}
			}
			var TenantName = _tenantRepository.GetAll().Where(T => T.Id == AbpSession.TenantId).Select(T => T.TenancyName).FirstOrDefault();
			var Last_FormId = _jobRepository.GetAll().OrderByDescending(j => j.Id).Select(j => j.Id).FirstOrDefault();

			//var tenantlist = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
			//for (int i = 0; i < tenantlist.Count; i++)
			//{
			//	using (_unitOfWorkManager.Current.SetTenantId(tenantlist[i].Id))
			//	{
			//		string Latest_FormId = (from datajob in _jobRepository.GetAll() orderby datajob.Id descending select datajob.FormId).FirstOrDefault();
			//		if (Latest_FormId != null || Latest_FormId != string.Empty)
			//		{
			//			int latestFormId = Convert.ToInt32(Latest_FormId);
			//			if (latestFormId >= formid)
			//			{
			//				formid = latestFormId + 1;
			//			}
			//		}
			//	}
			//}

			job.InvoiceGenereted = false;
			job.Guid = System.Guid.NewGuid();
			//job.FormId = formid.ToString();
			if (Last_FormId == null)
			{
				job.FormId = "1001";
			}
			else
			{
				job.FormId = TenantName + "_" + Last_FormId;
			}

			if (job.installationAddresssameasHouseAddress == true)
			{
				job.Suburb_Id = job.InstallationSuburb_Id;
			}

			var id = await _jobRepository.InsertAsync(job);
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Create)]
		public async Task CreateJobBulkUpload(CreateJobBulkUploadDto input)
		{
			var JobBulkUpload = input.MapTo<JobBulkUpload>();
			await _jobBulkUploadRepository.InsertAsync(JobBulkUpload);
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Delete)]
		public async Task DeleteJobs(EntityDto input)
		{
			await _jobRepository.DeleteAsync(input.Id);
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Edit)]
		public async Task CompleteJob(EntityDto input)
		{
			var job = await _jobRepository.GetAsync(input.Id);
			job.JobStatus_Id = 2;
			await _jobRepository.UpdateAsync(job);
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Edit)]
		public async Task UpdateJobs(JobsEditDto input)
		{
			var job = await _jobRepository.GetAsync(input.Id);

			//var getstsinput = new GetSTCInput();
			//getstsinput.Capacity = Convert.ToString(input.SystemCapacity);
			//getstsinput.postcode = input.StreetPostCode;
			//getstsinput.Recyearid = Convert.ToString(input.RECYear_Id);

			//input.TotalSTS = GetSTCNo(getstsinput);
			job.FirstName = input.FirstName;
			job.LastName = input.LastName;
			job.Company = input.Company;
			job.EmailId = input.EmailId;
			job.Phone = input.Phone;
			job.Fax = input.Fax;
			job.PostalAddressType = input.PostalAddressType;
			job.PostalDeliveryNumber = input.PostalDeliveryNumber;
			job.PostalDeliveryType = input.PostalDeliveryType;
			job.StreetAddress = input.StreetAddress;
			job.UnitNo = input.UnitNo;
			job.UnitType = input.UnitType;
			job.StreetNo = input.StreetNo;
			job.StreetName = input.StreetName;
			job.StreetType = input.StreetType;
			job.StreetCity = input.StreetCity;
			job.Suburb_Id = input.Suburb_Id;
			job.StreetState = input.StreetState;
			job.StreetPostCode = input.StreetPostCode;

			//Installation Address Details

			job.InstallationStreetAddress = input.InstallationStreetAddress;
			job.InstallationUnitNo = input.InstallationUnitNo;
			job.InstallationUnitType = input.InstallationUnitType;
			job.InstallationStreetNo = input.InstallationStreetNo;
			job.InstallationStreetName = input.InstallationStreetName;
			job.InstallationStreetType = input.InstallationStreetType;
			job.InstallationStreetCity = input.InstallationStreetCity;
			job.InstallationSuburb_Id = input.InstallationSuburb_Id;
			job.InstallationStreetState = input.InstallationStreetState;
			job.InstallationStreetPostCode = input.InstallationStreetPostCode;

			//Site Details

			job.HouseType = input.HouseType;
			job.RoofType = input.RoofType;
			job.NMINo = input.NMINo;
			job.RegPlanNo = input.RegPlanNo;
			job.LotNo = input.LotNo;

			//panel Details
			job.PanelName = input.PanelName;
			job.BrandName = input.BrandName;
			job.Panel_id = input.Panel_id;
			job.PanelModel = input.PanelModel;
			job.PanelSize = input.PanelSize;
			job.NoOfPanels = input.NoOfPanels;
			job.SystemCapacity = input.SystemCapacity;

			//Inverter Details
			job.InverterName = input.InverterName;
			job.InverterBrandName = input.InverterBrandName;
			job.Inverter_id = input.Inverter_id;
			job.InverterModel = input.InverterModel;
			job.InverterSeries = input.InverterSeries;
			job.NoOfInverter = input.NoOfInverter;
			job.InverterOutput = input.InverterOutput;

			//Installation Details
			job.SolarType_Id = input.SolarType_Id;
			job.SystemType_Id = input.SystemType_Id;
			job.ExpectedInstallDate = input.ExpectedInstallDate;
			job.RoofFacingPanel_Id = input.RoofFacingPanel_Id;
			job.Sugstthisaddress_Id = input.Sugstthisaddress_Id;
			job.LocationOfNewSystem_Id = input.LocationOfNewSystem_Id;
			job.Customer_Id = input.Customer_Id;
			job.CreateFor_Id = input.CreateFor_Id;
			job.Installer_Id = input.Installer_Id;
			job.Designer_Id = input.Designer_Id;
			job.Electrician_Id = input.Electrician_Id;
			job.ProjectNumber = input.ProjectNumber;
			job.InstallerNote = input.InstallerNote;

			//REC Details
			job.Typeofsystem_Id = input.Typeofsystem_Id;
			job.Typeofconnection_Id = input.Typeofconnection_Id;
			job.Mountingtype_Id = input.Mountingtype_Id;
			job.Auditreportavailable_Id = input.Auditreportavailable_Id;

			job.Installingcompleteunit_Id = input.Installingcompleteunit_Id;

			job.AddcapMoredetailed = input.AddcapMoredetailed;
			job.RECYear_Id = input.RECYear_Id;
			job.Defaultresourceavailfigure_Id = input.Defaultresourceavailfigure_Id;

			job.ResourceAvailability = input.ResourceAvailability;
			job.AccreditationCode = input.AccreditationCode;
			job.ExplanatoryNotes = input.ExplanatoryNotes;
			job.CeatingCert_Id = input.CeatingCert_Id;

			job.CECAccreditationStatement_Id = input.CECAccreditationStatement_Id;
			job.GovernmentSitingApprovals_Id = input.GovernmentSitingApprovals_Id;
			job.ElectricalSafetyDocumentation_Id = input.ElectricalSafetyDocumentation_Id;
			job.AustralianStandardsStatement_Id = input.AustralianStandardsStatement_Id;

			job.NotgridConnectedStatement_Id = input.NotgridConnectedStatement_Id;
			job.InstallPropertyName = input.InstallPropertyName;
			job.SGUatThisAddress_Id = input.SGUatThisAddress_Id;
			job.DescribeAdequateIinfo = input.DescribeAdequateIinfo;

			job.AdditionalSystemInformation = input.AdditionalSystemInformation;
			job.SystemMountingType_Id = input.SystemMountingType_Id;
			job.OwnerAddressType_Id = input.OwnerAddressType_Id;
			job.CustomerAddressType_Id = input.CustomerAddressType_Id;
			job.ExpectedSTC = input.ExpectedSTC;
			await _jobRepository.UpdateAsync(job);
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_Jobs_Edit)]
		public async Task<JobsEditDto> GetJobsForEdit(EntityDto input)
		{
			var job = (await _jobRepository.GetAsync(input.Id)).MapTo<JobsEditDto>();
			return job;
		}

		public async Task<JobBulkUploadDto> getJobBulkUpload()
		{
			var Job = new JobBulkUploadDto();
			var Jobdata = _jobBulkUploadRepository.GetAll().OrderByDescending(P => P.Id).FirstOrDefault();

			Job = Jobdata.MapTo<JobBulkUploadDto>();

			Job.UploaderName = _userRepository.GetAll().Where(U => U.Id == Job.CreatorUserId).Select(U => U.Name).FirstOrDefault() + " " + _userRepository.GetAll().Where(U => U.Id == Job.CreatorUserId).Select(U => U.UserName).FirstOrDefault();

			return Job;
		}

		[UnitOfWork]
		public ListResultDto<PostalAddressTypeListDto> GetPostalAddressType()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var postalAddressType = _postalAddressTypeRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<PostalAddressTypeListDto>(postalAddressType.MapTo<List<PostalAddressTypeListDto>>());
			}

		}

		[UnitOfWork]
		public ListResultDto<PostalDeliveryTypeListDto> GetPostalDeliveryType()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var postalDeliveryType = _postalDeliveryTypeRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<PostalDeliveryTypeListDto>(postalDeliveryType.MapTo<List<PostalDeliveryTypeListDto>>());
			}

		}

		[UnitOfWork]
		//public ListResultDto<JobStreetNamesListDto> GetStreetNames()
		public List<typeAhead> GetStreetNames(TypeAheadInput input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				if (input.Text != null && input.Text != "")
				{
					var streetName = (from data in _streetNamesRepository
				.GetAll()
				.Where(e => e.StreetName.Contains(input.Text))
				.Take(20)
									  select new typeAhead
									  {
										  id = data.Id.ToString(),
										  name = data.StreetName
									  }).ToList();

					return streetName; // new ListResultDto<JobStreetNamesListDto>(streetName.MapTo<List<JobStreetNamesListDto>>());
				}
				else
				{
					var streetName = (from data in _streetNamesRepository
				.GetAll()
									  select new typeAhead
									  {
										  id = data.Id.ToString(),
										  name = data.StreetName
									  }).ToList();

					return streetName;
				}
			}

		}

		[UnitOfWork]
		//public ListResultDto<JobStreetTypeListDto> GetStreetType()
		public List<typeAhead> GetStreetType(TypeAheadInput input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				if (input.Text != null && input.Text != "")
				{
					var streetType = (from data in _streetTypeRepository
				.GetAll()
				.Where(e => e.StreetTypeName.Contains(input.Text))
				.Take(20)
									  select new typeAhead
									  {
										  id = data.Id.ToString(),
										  name = data.StreetTypeName
									  }).ToList();

					return streetType;
				}
				else
				{
					var streetType = (from data in _streetTypeRepository
				.GetAll()
									  select new typeAhead
									  {
										  id = data.Id.ToString(),
										  name = data.StreetTypeName
									  }).ToList();

					return streetType;
				}
			}
		}

		[UnitOfWork]
		//public ListResultDto<JobUnitTypeListDto> GetUnitType()
		public List<typeAhead> GetUnitType(TypeAheadInput input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				if (input.Text != null && input.Text != "")
				{
					var unitType = (from data in _unitTypeRepository
				.GetAll()
				.Where(e => e.UnitTypeName.Contains(input.Text))
				.Take(20)
									select new typeAhead
									{
										id = data.Id.ToString(),
										name = data.UnitTypeName
									}).ToList();

					return unitType;
				}
				else
				{
					var unitType = (from data in _unitTypeRepository
				.GetAll()
									select new typeAhead
									{
										id = data.Id.ToString(),
										name = data.UnitTypeName
									}).ToList();

					return unitType;
				}
			}
		}

		[UnitOfWork]
		//public ListResultDto<PostCodesListDto> GetAllPostCodes(string postCodeInput)
		public List<typeAhead> GetAllPostCodes(TypeAheadInput input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				if (input.Text != null && input.Text != "")
				{
					var postcodes = _postCodeRepository
				.GetAll()
				.Where(e => e.Suburb.Contains(input.Text))
				.Take(20)
				.ToList();

					var suburblist = (from Inst in postcodes
									  join In in _stateRepository.GetAll() on Inst.StateId equals In.Id into InJoin
									  from In in InJoin.DefaultIfEmpty()
									  select new typeAhead
									  {
										  id = Inst.Id.ToString(),
										  name = Inst.Suburb + " | " + In.StateName + " | " + Inst.Postcode
									  }).ToList();

					return suburblist;
				}
				else
				{
					var postcodes = _postCodeRepository
				.GetAll()
				.ToList();

					var suburblist = (from Inst in postcodes
									  join In in _stateRepository.GetAll() on Inst.StateId equals In.Id into InJoin
									  from In in InJoin.DefaultIfEmpty()
									  select new typeAhead
									  {
										  id = Inst.Id.ToString(),
										  name = Inst.Suburb + " | " + In.StateName + " | " + Inst.Postcode
									  }).ToList();

					return suburblist;
					//return null;
				}
			}
		}

		[UnitOfWork]
		public ListResultDto<PostCodesListDto> GetPostCodesListBySuburb(EntityDto Input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var ModelNo = _postCodeRepository
				.GetAll()
				.Where(p => p.Id == Input.Id)
				.ToList();

				var PostCodes = ModelNo.MapTo<List<PostCodesListDto>>();
				foreach (var item in PostCodes)
				{
					item.stateName = (from datastate in _stateRepository.GetAll() where datastate.Id == item.StateId select datastate.StateName).FirstOrDefault();
				}
				return new ListResultDto<PostCodesListDto>(PostCodes);
			}
		}

		[UnitOfWork]
		public ListResultDto<JobHouseTypeListDto> GetHouseType()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var housetype = _houseTypeRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<JobHouseTypeListDto>(housetype.MapTo<List<JobHouseTypeListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<RoofTypeListDto> GetRoofType()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var rooftype = _roofTypeRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<RoofTypeListDto>(rooftype.MapTo<List<RoofTypeListDto>>());
			}
		}

		public ListResultDto<JobGetEnergyDestributar> GetEnergyDestributar()
		{
			var destributar = _energyDestributarRepository
			.GetAll()
			.OrderBy(p => p.Id)
			.ToList();

			return new ListResultDto<JobGetEnergyDestributar>(destributar.MapTo<List<JobGetEnergyDestributar>>());
		}

		public ListResultDto<JobGetEnergyRetailer> GetEnergyRetailer()
		{
			var retailer = _energyRetailerRepository
			.GetAll()
			.OrderBy(p => p.Id)
			.ToList();

			return new ListResultDto<JobGetEnergyRetailer>(retailer.MapTo<List<JobGetEnergyRetailer>>());
		}

		[UnitOfWork]
		//public ListResultDto<JobPanelListDto> GetDistinctpanelList()
		public List<typeAhead> GetDistinctpanelList(TypeAheadInput input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				if (input.Text != null && input.Text != "")
				{
					var panels = (from data in _panelRepository
				.GetAll()
				.Where(e => e.LicenceOrCertiHolder.Contains(input.Text))
				.Take(20)
								  select new typeAhead
								  {
									  id = data.LicenceOrCertiHolder,
									  name = data.LicenceOrCertiHolder
								  }).Distinct().ToList();

					return panels;
				}
				else
				{
					var panels = (from data in _panelRepository
				.GetAll()
								  select new typeAhead
								  {
									  id = data.LicenceOrCertiHolder,
									  name = data.LicenceOrCertiHolder
								  }).Distinct().ToList();

					return panels;
				}
			}
		}

		[UnitOfWork]
		public ListResultDto<JobPanelListDto> GetpanelList()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var panels = _panelRepository
				.GetAll()
				.Where(p => p.Size != null && p.ExpiryDate > DateTime.Now)
				.OrderByDescending(p => p.Id).ToList();
				//.Select(p => p.LicenceOrCertiHolder).Distinct().ToList();
				return new ListResultDto<JobPanelListDto>(panels.MapTo<List<JobPanelListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<JobModelListDto> GetModelNoList(GetInput Input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var ModelNo = _panelRepository
				.GetAll()
				.Where(p => p.Size != null && p.ExpiryDate > DateTime.Now && p.LicenceOrCertiHolder == Input.Filter)
				.OrderByDescending(p => p.Id).ToList();

				return new ListResultDto<JobModelListDto>(ModelNo.MapTo<List<JobModelListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<JobModelListDto> GetPanelListById(GetInput Input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				int id = int.Parse(Input.Filter);
				var ModelNo = _panelRepository
					.GetAll()
					.Where(p => p.Size != null && p.ExpiryDate > DateTime.Now && p.Id == id).ToList();

				return new ListResultDto<JobModelListDto>(ModelNo.MapTo<List<JobModelListDto>>());
			}
		}

		[UnitOfWork]
		//public ListResultDto<JobInverterListDto> GetDistinctInverterList()
		public List<typeAhead> GetDistinctInverterList(TypeAheadInput input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				if (input.Text != null && input.Text != "")
				{
					var Inverter = (from data in _inverterRepository
				.GetAll()
				.Where(e => e.ManufacturarName.Contains(input.Text))
				.Take(20)
									select new typeAhead
									{
										id = data.ManufacturarName,
										name = data.ManufacturarName
									}).Distinct().ToList();

					return Inverter;
				}
				else
				{
					var Inverter = (from data in _inverterRepository
				.GetAll()
									select new typeAhead
									{
										id = data.ManufacturarName,
										name = data.ManufacturarName
									}).Distinct().ToList();

					return Inverter;
				}
			}
		}

		[UnitOfWork]
		public ListResultDto<JobInverterListDto> GetInverterList()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var Inverter = _inverterRepository
				.GetAll()
				.Where(p => p.Sizes != null && p.ExpiryDate > DateTime.Now)
				.OrderByDescending(p => p.Id).ToList();
				return new ListResultDto<JobInverterListDto>(Inverter.MapTo<List<JobInverterListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<JobInverterModelListDto> GetInverterModelNoList(GetInput Input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InverterModelNo = _inverterRepository
				.GetAll()
				.Where(p => p.Sizes != null && p.ExpiryDate > DateTime.Now && p.ManufacturarName == Input.Filter)
				.OrderByDescending(p => p.Id).ToList();

				return new ListResultDto<JobInverterModelListDto>(InverterModelNo.MapTo<List<JobInverterModelListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<JobInverterModelListDto> GetInverterListById(GetInput Input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				int id = int.Parse(Input.Filter);
				var InverterModelNo = _inverterRepository
					.GetAll()
					.Where(p => p.Sizes != null && p.ExpiryDate > DateTime.Now && p.Id == id).ToList();

				return new ListResultDto<JobInverterModelListDto>(InverterModelNo.MapTo<List<JobInverterModelListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<SolarTypeListDto> GetSolarType()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var solarType = _solarTypeRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<SolarTypeListDto>(solarType.MapTo<List<SolarTypeListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<SystemTypeListDto> GetSystemType()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var SystemType = _systemTypeRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<SystemTypeListDto>(SystemType.MapTo<List<SystemTypeListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<RoofFacingPanelListDto> GetRoofFacingPanel()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var RoofFacingPanel = _roofFacingPanelRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<RoofFacingPanelListDto>(RoofFacingPanel.MapTo<List<RoofFacingPanelListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<YesNoListDto> GetYesNo()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var YesNo = _yesNoRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<YesNoListDto>(YesNo.MapTo<List<YesNoListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<TypeOfSystemListDto> GetTypeOfSystem()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var typeOfSystem = _typeOfSystemRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<TypeOfSystemListDto>(typeOfSystem.MapTo<List<TypeOfSystemListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<TypeOfConnectionListDto> GetTypeofconnection()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var typeofconnection = _typeOfConnectionRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<TypeOfConnectionListDto>(typeofconnection.MapTo<List<TypeOfConnectionListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<MountingTypeListDto> GetMountingType()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var mountingType = _mountingTypeRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<MountingTypeListDto>(mountingType.MapTo<List<MountingTypeListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<SystemMountingTypeListDto> GetSystemMountingType()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var systemMounting = _systemMountingTypeRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

				return new ListResultDto<SystemMountingTypeListDto>(systemMounting.MapTo<List<SystemMountingTypeListDto>>());
			}
		}

		[UnitOfWork]
		public ListResultDto<RECYearListDto> GetRECYear()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var currentyear = DateTime.Now.Year;
				var RECYear = _recyearRepository
					.GetAll()
					.Where(p => p.Year == currentyear)
					.OrderByDescending(p => p.Id)
					.ToList();

				return new ListResultDto<RECYearListDto>(RECYear.MapTo<List<RECYearListDto>>());
			}
		}

		[UnitOfWork]
		public decimal GetExpectedSTC(GetSTCInput Input)
		{
			decimal ExpectedSTC = 0;
			try
			{
				using (_unitOfWorkManager.Current.SetTenantId(null))
				{
					int postcodes = int.Parse(Input.postcode);
					var Zonelist = _stcPostCodeRepository
					.GetAll()
					.Where(p => p.STCPostCodeFrom <= postcodes && p.STCPostCodeTo >= postcodes).Select(S => S.STCZoneID).FirstOrDefault();

					var Zoneid = _STCZoneRatingRepository.GetAll().Where(S => S.Id == Zonelist).Select(p => p.STCRating).FirstOrDefault();
					//int id = int.Parse(Input.Recyearid);
					var currentyear = DateTime.Now.Year;
					var stcrate = _recyearRepository
						.GetAll()
						.Where(p => p.Year == currentyear)
						.OrderByDescending(p => p.Id).Select(p => p.STCRate).FirstOrDefault();

					var totalstc = Convert.ToString((Convert.ToDecimal(Input.Capacity)) * Convert.ToDecimal(Zoneid) * stcrate);
					string[] Result = totalstc.Split(".");
					ExpectedSTC = Convert.ToDecimal(Result[0]);
				}
			}
			catch (Exception e) { }
			return ExpectedSTC;
		}

		public ListResultDto<JobStatusLstDto> GetJobStatus()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var jobstatus = _jobStatusRepository
					.GetAll()
					.OrderByDescending(p => p.Id)
					.ToList();

				return new ListResultDto<JobStatusLstDto>(jobstatus.MapTo<List<JobStatusLstDto>>());
			}
		}

		public async Task<ListResultDto<InstallerListDto>> GetAllInstallerAsync()
		{
			//long userid = (long)_session.UserId;
			//var user = await UserManager.GetUserByIdAsync(userid);
			//var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

			//var userspecific = grantedPermissions.Where(x => x.Name == "Pages.UserSpecific").ToList();

			//var installer = _installerRepository
			//.GetAll();

			//var installerlist = (from Inst in installer
			//                     join In in _customerInstallerRepository.GetAll() on Inst.Id equals In.InstallerId into InJoin
			//                     from In in InJoin.DefaultIfEmpty()
			//                     where (Inst.IsApproved == 1 && In.Inst_Request_status == 1 && In.Request_status == 1)
			//                     group Inst by Inst into instGrouped
			//                     select instGrouped.Key);

			//if (userspecific.Count > 0)
			//{
			//    installer = installer.Where(x => x.CreatorUserId == userid);
			//}

			////.Where(p => p.IsApproved == 1)
			////.ToList();
			//return new ListResultDto<InstallerListDto>(installerlist.MapTo<List<InstallerListDto>>());

			long userid = (long)_session.UserId;
			var user = await UserManager.GetUserByIdAsync(userid);
			var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);
			var Installer = new List<InstallerListDto>();
			var userspecific = grantedPermissions.Where(x => x.Name == "Pages.UserSpecificInstaller").ToList();
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var EmpList = _customerInstallerRepository
					.GetAll()
					.Where(c => c.Inst_Request_status == 1 && c.Request_status == 1 && c.IsCreated == false && c.TenantId == _session.TenantId)//
					.Select(p => p.InstallerId)
					.ToList();

				var installer = _installerRepository
					.GetAll()
					.WhereIf(
						EmpList.Count != 0,
						E => EmpList.Contains(E.Id) || E.TenantId == _session.TenantId
						);

				if (userspecific.Count > 0)
				{
					installer = installer.Where(x => x.CreatorUserId == userid);
				}

				var installerlist = (from Inst in installer
									 join In in _customerInstallerRepository.GetAll() on Inst.Id equals In.InstallerId into InJoin
									 from In in InJoin.DefaultIfEmpty()
									 where (In.Inst_Request_status == 1 && In.Request_status == 1)
									 group Inst by Inst into instGrouped
									 select instGrouped.Key);

				return new ListResultDto<InstallerListDto>(installerlist.MapTo<List<InstallerListDto>>());
			}

		}

		public async Task<JobDetailDto> GetJobsDetails(EntityDto input)
		{
			var job = (await _jobRepository.GetAsync(input.Id)).MapTo<JobDetailDto>();
			job.JobStatusName = _jobStatusRepository.GetAll().Where(p => p.Id == job.JobStatus_Id).Select(p => p.JobStatusName).FirstOrDefault();

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				job.CreateForName = _installerRepository.GetAll().Where(p => p.Id == job.CreateFor_Id).Select(p => p.FullName).FirstOrDefault();
				job.InstallerName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Installer_Id).Select(p => p.FullName).FirstOrDefault();
				job.DesignerName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Designer_Id).Select(p => p.FullName).FirstOrDefault();
				job.ElectricianName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Electrician_Id).Select(p => p.FullName).FirstOrDefault();

				job.HouseTypeName = _houseTypeRepository.GetAll().Where(p => p.Id == job.HouseType).Select(p => p.HouseTypeName).FirstOrDefault();
				job.RoofTypeName = _roofTypeRepository.GetAll().Where(p => p.Id == job.RoofType).Select(p => p.RoofTypeName).FirstOrDefault();
				job.SolarTypeName = _solarTypeRepository.GetAll().Where(p => p.Id == job.SolarType_Id).Select(p => p.SolarTypeeName).FirstOrDefault();
				job.SystemTypeName = _systemTypeRepository.GetAll().Where(p => p.Id == job.SystemType_Id).Select(p => p.SystemTypeName).FirstOrDefault();
				job.RoofFacingPanelName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == job.RoofFacingPanel_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
				job.SugstthisaddressName = _yesNoRepository.GetAll().Where(p => p.Id == job.Sugstthisaddress_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.LocationOfNewSystemName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == job.LocationOfNewSystem_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();

				job.TypeofsystemName = _typeOfSystemRepository.GetAll().Where(p => p.Id == job.Typeofsystem_Id).Select(p => p.TypeOfSystemName).FirstOrDefault();
				job.TypeofconnectionName = _typeOfConnectionRepository.GetAll().Where(p => p.Id == job.Typeofconnection_Id).Select(p => p.TypeOfConnectionName).FirstOrDefault();
				job.MountingtypeName = _mountingTypeRepository.GetAll().Where(p => p.Id == job.Mountingtype_Id).Select(p => p.MountingTypeName).FirstOrDefault();
				job.AuditreportavailableName = _yesNoRepository.GetAll().Where(p => p.Id == job.Auditreportavailable_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.InstallingcompleteunitName = _yesNoRepository.GetAll().Where(p => p.Id == job.Installingcompleteunit_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.RECYearName = _recyearRepository.GetAll().Where(p => p.Id == job.RECYear_Id).Select(p => p.REC_Year).FirstOrDefault();
				job.DefaultresourceavailfigureName = _yesNoRepository.GetAll().Where(p => p.Id == job.Defaultresourceavailfigure_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.CeatingCertName = _yesNoRepository.GetAll().Where(p => p.Id == job.CeatingCert_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.CECAccreditationStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.CECAccreditationStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.GovernmentSitingApprovalsName = _yesNoRepository.GetAll().Where(p => p.Id == job.GovernmentSitingApprovals_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.ElectricalSafetyDocumentationName = _yesNoRepository.GetAll().Where(p => p.Id == job.ElectricalSafetyDocumentation_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.AustralianStandardsStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.AustralianStandardsStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.NotgridConnectedStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.NotgridConnectedStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.SGUatThisAddressName = _yesNoRepository.GetAll().Where(p => p.Id == job.SGUatThisAddress_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.SystemMountingTypeName = _systemMountingTypeRepository.GetAll().Where(p => p.Id == job.SystemMountingType_Id).Select(p => p.SystemMountingTypeName).FirstOrDefault();
				job.OwnerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == job.OwnerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
				job.CustomerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == job.CustomerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
				job.InverterModel = _inverterRepository.GetAll().Where(p => p.Id == job.Inverter_id).Select(p => p.ModelNumber).FirstOrDefault();
				job.PanelModel = _panelRepository.GetAll().Where(p => p.Id == job.Panel_id).Select(p => p.ModelNo).FirstOrDefault();

			}
			return job;
		}

		public async Task<ListResultDto<InstEmployeListDto>> GetInstallerEmployeInstallerByInstallerIdAsync(EntityDto input)
		{
			long userid = (long)_session.UserId;
			var user = await UserManager.GetUserByIdAsync(userid);
			var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

			var userspecific = grantedPermissions.Where(x => x.Name == "Pages.UserSpecificInstaller").ToList();
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var installerEmployee = _installerEmployeRepository
				.GetAll()
				.WhereIf(
					input.Id != 0,
					p => p.InstallerId == input.Id && p.IsInst == true
				).ToList();

				if (userspecific.Count > 0)
				{
					installerEmployee = installerEmployee.Where(x => x.CreatorUserId == userid).ToList();
				}
				return new ListResultDto<InstEmployeListDto>(installerEmployee.MapTo<List<InstEmployeListDto>>());
			}
		}

		public async Task<ListResultDto<InstEmployeListDto>> GetInstallerEmployeDesignerByInstallerIdAsync(EntityDto input)
		{
			long userid = (long)_session.UserId;
			var user = await UserManager.GetUserByIdAsync(userid);
			var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

			var userspecific = grantedPermissions.Where(x => x.Name == "Pages.UserSpecificInstaller").ToList();
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var installerEmployee = _installerEmployeRepository
				.GetAll()
				.WhereIf(
					input.Id != 0,
					p => p.InstallerId == input.Id && p.IsDesc == true
				).ToList();

				if (userspecific.Count > 0)
				{
					installerEmployee = installerEmployee.Where(x => x.CreatorUserId == userid).ToList();
				}
				return new ListResultDto<InstEmployeListDto>(installerEmployee.MapTo<List<InstEmployeListDto>>());
			}
		}

		public async Task<ListResultDto<InstEmployeListDto>> GetInstallerEmployeElectricianByInstallerIdAsync(EntityDto input)
		{
			long userid = (long)_session.UserId;
			var user = await UserManager.GetUserByIdAsync(userid);
			var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

			var userspecific = grantedPermissions.Where(x => x.Name == "Pages.UserSpecificInstaller").ToList();
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var installerEmployee = _installerEmployeRepository
			   .GetAll()
			   .WhereIf(
				   input.Id != 0,
				   p => p.InstallerId == input.Id && p.IsElec == true
			   ).ToList();

				if (userspecific.Count > 0)
				{
					installerEmployee = installerEmployee.Where(x => x.CreatorUserId == userid).ToList();
				}
				return new ListResultDto<InstEmployeListDto>(installerEmployee.MapTo<List<InstEmployeListDto>>());
			}
		}

		public ListResultDto<CalendarJobListDto> GetCalenderJobs(CalenderInputDto input)
		{
			var job = _jobRepository
				.GetAll()
				.OrderByDescending(p => p.Id)
				.ToList();

			if (input.CreateFor_Id != 0)
			{
				job = job.Where(x => x.CreateFor_Id == input.CreateFor_Id).ToList();
			}
			if (input.Installer_Id != 0)
			{
				job = job.Where(x => x.Installer_Id == input.Installer_Id).ToList();
			}
			if (input.projectNumber != "" && input.projectNumber != null)
			{
				job = job.Where(x => x.ProjectNumber == input.projectNumber).ToList();
			}
			var joblist = job.MapTo<List<CalendarJobListDto>>();

			foreach (var item in joblist)
			{
				item.FullName = item.FirstName + " " + item.LastName;
				item.day = item.ExpectedInstallDate.Day;
				item.month = item.ExpectedInstallDate.Month;
				item.year = item.ExpectedInstallDate.Year;
				item.ExpectedInstallDate = item.ExpectedInstallDate;
				item.InstallerName = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).Select(p => p.FullName).FirstOrDefault();
			}
			return new ListResultDto<CalendarJobListDto>(joblist);
		}

		#region Upload Documents
		public async Task<JobDetailDto> GetJobDetail(DocumentDetailInputDto input)
		{
			//Guid newGuId = new Guid(input.Guid);
			var JobId = _jobRepository.GetAll().Where(J => J.Guid == input.Guid).Select(J => J.Id).FirstOrDefault();
			var job = (await _jobRepository.GetAsync(JobId)).MapTo<JobDetailDto>();
			return job;
		}

		public async Task<PagedResultDto<DocumentListDto>> GetDocumentList(GetDocumentListInput input)
		{
			var docslist = new List<DocumentListDto>();
			var docs = _photoRepository
				.GetAll()
				.Where(P => P.Guid == input.Guid)
				.WhereIf(input.FileType != null, P => P.FileType == input.FileType);

			var resultCount = await docs.CountAsync();
			var results = await docs
				.AsNoTracking()
				.OrderBy(input.Sorting)
				.PageBy(input)
				.ToListAsync();

			docslist = results.MapTo<List<DocumentListDto>>();
			foreach (var item in docslist)
			{
				item.FileTypeName = _photoTypeRepository.GetAll().Where(P => P.Id == item.FileType).Select(P => P.PhotoTypeName).FirstOrDefault();
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/Photos" + item.ImageName;
			}

			return new PagedResultDto<DocumentListDto>(resultCount, docslist.MapTo<List<DocumentListDto>>());
		}

		public async Task CreateDocument(CreateDocumentDto input)
		{
			var document = input.MapTo<Photo>();
			document.ImageGuid = System.Guid.NewGuid();
			document.Longitude = "";
			document.Latitude = "";
			document.TenantId = (int)AbpSession.TenantId;
			await _photoRepository.InsertAsync(document);
		}

		[UnitOfWork]
		public ListResultDto<FileTypeListDto> GetFileType()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var PhotoType = _photoTypeRepository
				.GetAll()
				.OrderBy(p => p.Id)
				.ToList();

				return new ListResultDto<FileTypeListDto>(PhotoType.MapTo<List<FileTypeListDto>>());
			}
		}

		public async Task DeleteDocs(EntityDto input)
		{
			await _photoRepository.DeleteAsync(input.Id);
		}

		[UnitOfWork]
		public ListResultDto<OldSystemDirectionListDto> GetOldSystemDirection()
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var Direction = _oldSystemDirectionRepository
				.GetAll()
				.OrderBy(p => p.Id)
				.ToList();

				return new ListResultDto<OldSystemDirectionListDto>(Direction.MapTo<List<OldSystemDirectionListDto>>());
			}
		}
		#endregion

		#region Customer Signature Request
		public async Task CreateCustomerSignSMS(EntityDto input)
		{
			string Token = "";
			Guid guid = System.Guid.NewGuid();
			Token = RandomStrongToken();
			var token = new Token();
			token.TokenNo = Token;
			token.GuId = guid;
			token.IsExpired = false;
			token.IsOwner = true;
			token.IsSent = true;
			token.JobId = input.Id;
			token.JobGuId = _jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.Guid).FirstOrDefault();

			var tokendata = await _tokenRepository.InsertAndGetIdAsync(token);

			var CustomerSignLog = new CustomerSignLog();

			CustomerSignLog.TokenGuid = token.GuId;
			CustomerSignLog.Token = token.TokenNo;
			CustomerSignLog.TokenDate = token.CreationTime;
			CustomerSignLog.SenderUserId = (int)token.CreatorUserId;
			CustomerSignLog.SendBy = "SMS";
			CustomerSignLog.JobId = token.JobId;
			CustomerSignLog.JobGuId = token.JobGuId;
			await _customerSignLogRepository.InsertAsync(CustomerSignLog);

			var job = await _jobRepository.GetAsync(input.Id);
			job.RequestSignatureImageOwnerDate = DateTime.Now;
			await _jobRepository.UpdateAsync(job);

		}

		public async Task CreateCustomerSignEmail(EntityDto input)
		{
			string Token = "";
			Guid guid = System.Guid.NewGuid();
			Token = RandomStrongToken();
			var token = new Token();
			token.TokenNo = Token;
			token.GuId = guid;
			token.IsExpired = false;
			token.IsOwner = true;
			token.IsSent = true;
			token.JobId = input.Id;
			token.JobGuId = _jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.Guid).FirstOrDefault();

			var tokendata = await _tokenRepository.InsertAndGetIdAsync(token);

			var CustomerSignLog = new CustomerSignLog();

			CustomerSignLog.TokenGuid = token.GuId;
			CustomerSignLog.Token = token.TokenNo;
			CustomerSignLog.TokenDate = token.CreationTime;
			CustomerSignLog.SenderUserId = (int)token.CreatorUserId;
			CustomerSignLog.SendBy = "Email";
			CustomerSignLog.JobId = token.JobId;
			CustomerSignLog.JobGuId = token.JobGuId;
			await _customerSignLogRepository.InsertAsync(CustomerSignLog);

			var job = await _jobRepository.GetAsync(input.Id);
			job.RequestSignatureImageOwnerDate = DateTime.Now;
			await _jobRepository.UpdateAsync(job);

			#region Customer Signature Email

			var tenantid = (_jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.TenantId).FirstOrDefault());
			var tenancyName = GetTenancyNameOrNull(tenantid);
			string email = (_jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.EmailId).FirstOrDefault());
			string CustomerName = (_jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.FirstName).FirstOrDefault()) + " " + (_jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.LastName).FirstOrDefault());
			var link = _webUrlService.GetSiteRootAddress(tenancyName) + "signature/Index?jobguid=" +
					  token.JobGuId + "&rt=" + token.TokenNo;

			//var link = "http://localhost:6234/Application/Index#!/signature/eac63eb3-3419-4a11-9402-247dac8576f7";
			// var mailMessage = new StringBuilder();
			//mailMessage.AppendLine("<b>" + L("Detailsasfollow") + "</b><br />");
			//mailMessage.AppendLine(L("Hello") + "<b>: " + CustomerName + "</b><br /><br />");
			//mailMessage.AppendLine("<p>: " + L("CustomerSignatureRequestMessage") + " </p><br /><br />");
			//mailMessage.AppendLine(link + "<br /><br />");
			//mailMessage.AppendLine(L("ThanksandRegards") + "<br />");
			//mailMessage.AppendLine(L("emailfootermessage"));

			//string subject = "Signature Request";
			//await _emailSender.SendAsync(email, subject, mailMessage.ToString());

			var MailMessageDetails = new User();
			MailMessageDetails.EmailAddress = email;
			MailMessageDetails.Name = CustomerName;
			MailMessageDetails.TenantId = tenantid;

			//await _userEmailer.SendEmailCustomerSignatureRequestAsync(MailMessageDetails, link);


			#endregion

		}
		private string GetTenancyNameOrNull(int? tenantId)
		{
			if (tenantId == null)
			{
				return null;
			}

			using (_unitOfWorkProvider.Current.SetTenantId(null))
			{
				return _tenantRepository.Get(tenantId.Value).TenancyName;
			}
		}
		public static String RandomStrongToken()
		{
			String pwd = "";
			int length = 0;
			int index = 0;
			int numericIndex = -1;
			int upperCaseIndex = -1;
			Random rnd = new Random();
			const int MINLENGTH = 8;
			const int MAXLENGTH = 8;
			const String allowedChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@$*";
			const String upperCaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			const String numericChars = "0123456789";
			//Length of your password
			length = rnd.Next(MINLENGTH, MAXLENGTH);

			// You generate a password of the desired length
			for (int i = 0; i < length; i++)
			{
				// Generate an index that smaller than the size of your allowed chars
				index = rnd.Next(0, allowedChars.Length);

				pwd += allowedChars[index];
			}

			////*********************************************************
			// We make sure that there is at least one numeric
			// Replace one random char by a numeric
			numericIndex = rnd.Next(0, pwd.Length);

			// Generate a numeric, delete one char and replace it with a numeric
			index = rnd.Next(0, numericChars.Length);
			pwd = pwd.Remove(numericIndex, 1);
			pwd = pwd.Insert(numericIndex, numericChars[index].ToString());
			////*********************************************************


			////*********************************************************
			// We make sure that there is at least one uppercase
			// Replace one random char by a numeric
			upperCaseIndex = rnd.Next(0, pwd.Length);

			// We make sure our uppercase index is different
			// from our numeric index or we will overwrite our
			// only numeric value with our uppercase value
			while (upperCaseIndex == numericIndex)
			{
				upperCaseIndex = rnd.Next(0, pwd.Length);
			}

			// Generate a numeric, delete one char and replace it with a numeric
			index = rnd.Next(0, upperCaseChars.Length);
			pwd = pwd.Remove(upperCaseIndex, 1);
			pwd = pwd.Insert(upperCaseIndex, upperCaseChars[index].ToString());
			////*********************************************************

			return pwd;
		}
		#endregion

		#region Installer Signature Request
		public async Task CreateInstallerSignSMS(EntityDto input)
		{
			string Token = "";
			Guid guid = System.Guid.NewGuid();
			Token = RandomStrongToken();
			var token = new Token();
			token.TokenNo = Token;
			token.GuId = guid;
			token.IsExpired = false;
			token.IsOwner = false;
			token.IsSent = true;
			token.JobId = input.Id;
			token.JobGuId = _jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.Guid).FirstOrDefault();

			var tokendata = await _tokenRepository.InsertAndGetIdAsync(token);

			var CustomerSignLog = new CustomerSignLog();

			CustomerSignLog.TokenGuid = token.GuId;
			CustomerSignLog.Token = token.TokenNo;
			CustomerSignLog.TokenDate = token.CreationTime;
			CustomerSignLog.SenderUserId = (int)token.CreatorUserId;
			CustomerSignLog.SendBy = "Email & SMS";
			CustomerSignLog.JobId = token.JobId;
			CustomerSignLog.JobGuId = token.JobGuId;
			await _customerSignLogRepository.InsertAsync(CustomerSignLog);

			var job = await _jobRepository.GetAsync(input.Id);
			job.RequestSignatureImageInstDate = DateTime.Now;
			await _jobRepository.UpdateAsync(job);

			#region Installer Signature Email

			var tenantid = (_jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.TenantId).FirstOrDefault());
			var tenancyName = GetTenancyNameOrNull(tenantid);
			//string email = (_jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.EmailId).FirstOrDefault());
			//string CustomerName = (_jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.FirstName).FirstOrDefault()) + " " + (_jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.LastName).FirstOrDefault());

			var createforid = (_jobRepository.GetAll().Where(p => p.Id == input.Id).Select(p => p.CreateFor_Id).FirstOrDefault());
			string InstallerrName = "";
			string email = "";
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				email = (_installerRepository.GetAll().Where(p => p.Id == createforid).Select(p => p.EmailId).FirstOrDefault());
				InstallerrName = (_installerRepository.GetAll().Where(p => p.Id == createforid).Select(p => p.FullName).FirstOrDefault());
			}
			if (!string.IsNullOrEmpty(email))
			{
				var link = _webUrlService.GetSiteRootAddress(tenancyName) + "signature/Index?jobguid=" +
					token.JobGuId + "&rt=" + token.TokenNo;
				//var link = "http://localhost:6234/Application/Index#!/signature/eac63eb3-3419-4a11-9402-247dac8576f7";

				//var mailMessage = new StringBuilder();

				//mailMessage.AppendLine("<b>" + L("Detailsasfollow") + "</b><br />");
				//mailMessage.AppendLine(L("Hello") + "<b>: " + InstallerrName + "</b><br /><br />");
				//mailMessage.AppendLine("<p>: " + L("CustomerSignatureRequestMessage") + " </p><br /><br />");
				//mailMessage.AppendLine(link + "<br /><br />");
				//mailMessage.AppendLine(L("ThanksandRegards") + "<br />");
				//mailMessage.AppendLine(L("emailfootermessage"));

				//string subject = "Signature Request";
				//await _emailSender.SendAsync(email, subject, mailMessage.ToString());

				var MailMessageDetails = new User();
				MailMessageDetails.EmailAddress = email;
				MailMessageDetails.Name = InstallerrName;
				MailMessageDetails.TenantId = tenantid;

				//await _userEmailer.SendEmailCustomerSignatureRequestAsync(MailMessageDetails, link);

			}



			#endregion


		}
		#endregion

		#region
		[UnitOfWork]
		public ListResultDto<CustomerSignatureLogDto> GetCustomerSignatureLog(EntityDto input)
		{
			// var Cust_List = _customerSignLogRepository
			//.GetAll()
			//.ToList();

			// var UserList = (from user in Cust_List
			//                 join ur in _tokenRepository.GetAll() on user.Token equals ur.TokenNo into urJoined
			//                 from ur in urJoined.DefaultIfEmpty()
			//                 where (user != null && user.JobId == input.Id)
			//                 group user by user into userGrouped
			//                 select userGrouped.Key);


			var customersignatureLog = (from request in _customerSignLogRepository.GetAll()
										join tokendata in _tokenRepository.GetAll() on request.Token equals tokendata.TokenNo
										where request.JobId == input.Id
										select new CustomerSignatureLogDto
										{
											RequestSendBy = request.SendBy,
											TokenDate = request.TokenDate,
											IsOwner = tokendata.IsOwner,
											RequestByUserId = (long)request.CreatorUserId,
											ResponseData = false,
											ReplyPending = true,
											IsNotOwner = false,
											Id = request.Id,
											JobId = request.JobId
										}).ToList();
			foreach (var item in customersignatureLog)
			{
				using (_unitOfWorkManager.Current.SetTenantId(null))
				{
					item.ResponseDate = _customerSignLogResponseRepository.GetAll().Where(p => p.Request_Id == item.Id).Select(p => p.CreationTime).FirstOrDefault();
					if (item.ResponseDate.ToString() != "1/1/0001 12:00:00 AM")
					{
						item.ResponseDateFormat = item.ResponseDate.ToString("dd/MM/yyyy HH:mm:ss tt");
					}
					if (item.TokenDate.ToString() != "1/1/0001 12:00:00 AM")
					{
						item.TokenDateFormat = item.TokenDate.ToString("dd/MM/yyyy HH:mm:ss tt");
					}
					var response_userid = _customerSignLogResponseRepository.GetAll().Where(p => p.Request_Id == item.Id).Select(p => p.CreatorUserId).FirstOrDefault();
					if (!string.IsNullOrEmpty(response_userid.ToString()))
					{
						item.ResponseByUserName = _jobRepository.GetAll().Where(p => p.Id == item.JobId).Select(p => p.FirstName).FirstOrDefault() + " " + _jobRepository.GetAll().Where(p => p.Id == item.JobId).Select(p => p.LastName).FirstOrDefault();
						item.ResponseData = true;
						item.ReplyPending = false;
					}
					if (!string.IsNullOrEmpty(item.RequestByUserId.ToString()))
					{
						using (_unitOfWorkManager.Current.SetTenantId(null))
						{
							var FName = _userRepository.GetAll().Where(p => p.Id == item.RequestByUserId).Select(p => p.Name).FirstOrDefault();
							var LName = _userRepository.GetAll().Where(p => p.Id == item.RequestByUserId).Select(p => p.Surname).FirstOrDefault();
							item.RequestByUserName = FName + " " + LName;
						}
					}
					if (item.IsOwner == false)
					{
						item.IsNotOwner = true;
					}
				}

			}
			return new ListResultDto<CustomerSignatureLogDto>(customersignatureLog);
			//return new ListResultDto<CustomerSignatureLogDto>(UserList.MapTo<List<CustomerSignatureLogDto>>());
		}
		#endregion

		#region Panel Photos
		public ListResultDto<PanelPhotosListDto> GetPanelPhotos(JobGuidInputDto input)
		{
			var jobguid = _jobRepository.GetAll().Where(p => p.Id == input.JobId).Select(p => p.Guid).FirstOrDefault();
			var Panel_Photos = _scanedPanelRepository.GetAll().Where(p => p.Guid == jobguid && p.PanelBarcodePhoto != "NULL").ToList();
			foreach (var item in Panel_Photos)
			{
				item.Latitude = Convert.ToString(Math.Round(Convert.ToDecimal(item.Latitude), 6));
				item.Longitude = Convert.ToString(Math.Round(Convert.ToDecimal(item.Longitude), 6));
			}

			var PanelPhoto_List = Panel_Photos.MapTo<List<PanelPhotosListDto>>();
			if (PanelPhoto_List.Count > 0)
			{
				PanelPhoto_List[0].FormId = _jobRepository.GetAll().Where(p => p.Id == input.JobId).Select(p => p.FormId).FirstOrDefault();
			}
			return new ListResultDto<PanelPhotosListDto>(PanelPhoto_List);
		}

		#endregion
		#region Inverter Photos
		public ListResultDto<InverterPhotosListDto> GetInverterPhotos(JobGuidInputDto input)
		{
			var jobguid = _jobRepository.GetAll().Where(p => p.Id == input.JobId).Select(p => p.Guid).FirstOrDefault();
			var Inverter_Photos = _scanedInverterRepository.GetAll().Where(p => p.Guid == jobguid && p.InverterBarcodePhoto != "NULL").ToList();
			foreach (var item in Inverter_Photos)
			{
				item.Latitude = Convert.ToString(Math.Round(Convert.ToDecimal(item.Latitude), 6));
				item.Longitude = Convert.ToString(Math.Round(Convert.ToDecimal(item.Longitude), 6));
			}
			var InverterPhoto_List = Inverter_Photos.MapTo<List<InverterPhotosListDto>>();
			if (InverterPhoto_List.Count > 0)
			{
				InverterPhoto_List[0].FormId = _jobRepository.GetAll().Where(p => p.Id == input.JobId).Select(p => p.FormId).FirstOrDefault();
			}
			return new ListResultDto<InverterPhotosListDto>(InverterPhoto_List);
		}
		#endregion
		#region Other Photos
		public ListResultDto<OtherPhotosListDto> GetOtherPhotos(JobGuidInputDto input)
		{
			var jobguid = _jobRepository.GetAll().Where(p => p.Id == input.JobId).Select(p => p.Guid).FirstOrDefault();
			var Photos = _photoRepository.GetAll().Where(p => p.Guid == jobguid).ToList();
			var photolist = Photos.MapTo<List<OtherPhotosListDto>>();
			foreach (var item in photolist)
			{
				item.FormId = _jobRepository.GetAll().Where(p => p.Id == input.JobId).Select(p => p.FormId).FirstOrDefault();
				item.FileTypeName = (_photoTypeRepository.GetAll().Where(p => p.Id == item.FileType).Select(p => p.PhotoTypeName).FirstOrDefault());
				item.Latitude = Convert.ToString(Math.Round(Convert.ToDecimal(item.Latitude), 6));
				item.Longitude = Convert.ToString(Math.Round(Convert.ToDecimal(item.Longitude), 6));
			}
			return new ListResultDto<OtherPhotosListDto>(photolist);
		}
		#endregion

		#region Customer Signature
		public ListResultDto<JobsListDto> GetCustomerSignaturePhotos(JobGuidInputDto input)
		{
			var signaturelist = new List<Job>();
			//var job = _jobRepository.GetAll().Where(p => p.Id == input.JobId).Select(p => p.SignatureImageOwner).FirstOrDefault();

			var job = _jobRepository.GetAll().Where(p => p.Id == input.JobId).ToList();

			foreach (var jobitem in job)
			{
				var installerDetail = _installerRepository.GetAll().Where(e => e.Id == jobitem.CreateFor_Id).FirstOrDefault();

				signaturelist.Add(jobitem);
				if (!string.IsNullOrEmpty(jobitem.Latitude_CustSign) && !string.IsNullOrEmpty(jobitem.Longitude_CustSign))
				{
					jobitem.Latitude_CustSign = Convert.ToString(Math.Round(Convert.ToDecimal(jobitem.Latitude_CustSign), 6));
					jobitem.Longitude_CustSign = Convert.ToString(Math.Round(Convert.ToDecimal(jobitem.Longitude_CustSign), 6));
				}
				if (!string.IsNullOrEmpty(jobitem.Latitude_InstSign) && !string.IsNullOrEmpty(jobitem.Longitude_InstSign))
				{
					jobitem.Latitude_InstSign = Convert.ToString(Math.Round(Convert.ToDecimal(jobitem.Latitude_InstSign), 6));
					jobitem.Longitude_InstSign = Convert.ToString(Math.Round(Convert.ToDecimal(jobitem.Longitude_InstSign), 6));
				}
			}
			return new ListResultDto<JobsListDto>(signaturelist.MapTo<List<JobsListDto>>());
		}
		#endregion

		#region Verify Photo
		public async Task updateVerifyPA(JobGuidInputDto input)
		{
			var job = await _jobRepository.GetAsync(input.JobId);
			job.IsVerifyPA = true;
			await _jobRepository.UpdateAsync(job);
		}
		public async Task updateVerifyIN(JobGuidInputDto input)
		{
			var job = await _jobRepository.GetAsync(input.JobId);
			job.IsVerifyIN = true;
			await _jobRepository.UpdateAsync(job);
		}
		public async Task updateVerifyIM(JobGuidInputDto input)
		{
			var job = await _jobRepository.GetAsync(input.JobId);
			job.IsVerifyImages = true;
			await _jobRepository.UpdateAsync(job);
		}
		public async Task updateVerifyCS(JobGuidInputDto input)
		{
			var job = await _jobRepository.GetAsync(input.JobId);
			job.IsVerifyCS = true;
			await _jobRepository.UpdateAsync(job);
		}
		public async Task updateVerifyIS(JobGuidInputDto input)
		{
			var job = await _jobRepository.GetAsync(input.JobId);
			job.IsVerifyIS = true;
			await _jobRepository.UpdateAsync(job);
		}
		public async Task updateVerifyCec(JobGuidInputDto input)
		{
			var job = await _jobRepository.GetAsync(input.JobId);
			job.IsVerifySTC = true;
			await _jobRepository.UpdateAsync(job);
		}
		public async Task updateVerifyEle(JobGuidInputDto input)
		{
			var job = await _jobRepository.GetAsync(input.JobId);
			job.IsVerifyELE = true;
			await _jobRepository.UpdateAsync(job);
		}
		public async Task updateVerifyCuck(JobGuidInputDto input)
		{
			var job = await _jobRepository.GetAsync(input.JobId);
			job.IsVerifyCUCK = true;
			await _jobRepository.UpdateAsync(job);
		}
		#endregion

		[UnitOfWork]
		public async Task<JobDetailDto> GetJobsDetailsByGuid(JobGuidInputDto input)
		{
			var jobid = _jobRepository.GetAll().Where(p => p.Guid == input.JobGuid).Select(p => p.Id).FirstOrDefault();
			var job = (await _jobRepository.GetAsync(jobid)).MapTo<JobDetailDto>();
			//var job = _jobRepository.GetAll().Where(p => p.Guid == input.JobGuid).MapTo<JobDetailDto>();
			try
			{
				job.JobStatusName = _jobStatusRepository.GetAll().Where(p => p.Id == job.JobStatus_Id).Select(p => p.JobStatusName).FirstOrDefault();
				job.HouseTypeName = _houseTypeRepository.GetAll().Where(p => p.Id == job.HouseType).Select(p => p.HouseTypeName).FirstOrDefault();
				job.RoofTypeName = _roofTypeRepository.GetAll().Where(p => p.Id == job.RoofType).Select(p => p.RoofTypeName).FirstOrDefault();
				job.SolarTypeName = _solarTypeRepository.GetAll().Where(p => p.Id == job.SolarType_Id).Select(p => p.SolarTypeeName).FirstOrDefault();
				job.SystemTypeName = _systemTypeRepository.GetAll().Where(p => p.Id == job.SystemType_Id).Select(p => p.SystemTypeName).FirstOrDefault();
				job.RoofFacingPanelName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == job.RoofFacingPanel_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
				job.SugstthisaddressName = _yesNoRepository.GetAll().Where(p => p.Id == job.Sugstthisaddress_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.LocationOfNewSystemName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == job.LocationOfNewSystem_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
				using (_unitOfWorkManager.Current.SetTenantId(null))
				{
					job.CreateForName = _installerRepository.GetAll().Where(p => p.Id == job.CreateFor_Id).Select(p => p.FullName).FirstOrDefault();
					job.InstallerName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Installer_Id).Select(p => p.FullName).FirstOrDefault();
					job.DesignerName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Designer_Id).Select(p => p.FullName).FirstOrDefault();
					job.ElectricianName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Electrician_Id).Select(p => p.FullName).FirstOrDefault();
				}
				job.TypeofsystemName = _typeOfSystemRepository.GetAll().Where(p => p.Id == job.Typeofsystem_Id).Select(p => p.TypeOfSystemName).FirstOrDefault();
				job.TypeofconnectionName = _typeOfConnectionRepository.GetAll().Where(p => p.Id == job.Typeofconnection_Id).Select(p => p.TypeOfConnectionName).FirstOrDefault();
				job.MountingtypeName = _mountingTypeRepository.GetAll().Where(p => p.Id == job.Mountingtype_Id).Select(p => p.MountingTypeName).FirstOrDefault();
				job.AuditreportavailableName = _yesNoRepository.GetAll().Where(p => p.Id == job.Auditreportavailable_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.InstallingcompleteunitName = _yesNoRepository.GetAll().Where(p => p.Id == job.Installingcompleteunit_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.RECYearName = _recyearRepository.GetAll().Where(p => p.Id == job.RECYear_Id).Select(p => p.REC_Year).FirstOrDefault();
				job.DefaultresourceavailfigureName = _yesNoRepository.GetAll().Where(p => p.Id == job.Defaultresourceavailfigure_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.CeatingCertName = _yesNoRepository.GetAll().Where(p => p.Id == job.CeatingCert_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.CECAccreditationStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.CECAccreditationStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.GovernmentSitingApprovalsName = _yesNoRepository.GetAll().Where(p => p.Id == job.GovernmentSitingApprovals_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.ElectricalSafetyDocumentationName = _yesNoRepository.GetAll().Where(p => p.Id == job.ElectricalSafetyDocumentation_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.AustralianStandardsStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.AustralianStandardsStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.NotgridConnectedStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.NotgridConnectedStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.SGUatThisAddressName = _yesNoRepository.GetAll().Where(p => p.Id == job.SGUatThisAddress_Id).Select(p => p.YesNoName).FirstOrDefault();
				job.SystemMountingTypeName = _systemMountingTypeRepository.GetAll().Where(p => p.Id == job.SystemMountingType_Id).Select(p => p.SystemMountingTypeName).FirstOrDefault();
				job.OwnerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == job.OwnerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
				job.CustomerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == job.CustomerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
				job.InverterModel = _inverterRepository.GetAll().Where(p => p.Id == job.Inverter_id).Select(p => p.ModelNumber).FirstOrDefault();
				job.PanelModel = _panelRepository.GetAll().Where(p => p.Id == job.Panel_id).Select(p => p.ModelNo).FirstOrDefault();
			}
			catch (Exception e)
			{ }
			return job;
		}

		#region ScanedPanel
		public async Task InsertScanedPanels(ScanedPanelEditDto input)
		{
			var newGuId = _jobRepository.GetAll().Where(p => p.Id == input.JobId).Select(p => p.Guid).FirstOrDefault();
			int count = 0;
			if (input.PanelSerialNumber.Contains(','))
			{
				string[] strArr = null;
				char[] splitchar = { ',' };
				strArr = input.PanelSerialNumber.Split(splitchar);
				for (count = 0; count <= strArr.Length - 1; count++)
				{
					try
					{
						var ScanedPanels = new ScanedPanel();
						ScanedPanels.Guid = newGuId;
						ScanedPanels.PanelBarcodePhoto = "NULL";
						ScanedPanels.PanelSerialNumber = strArr[count];
						ScanedPanels.Longitude = "73.208";
						ScanedPanels.Latitude = "22.299";
						await _scanedPanelRepository.InsertAsync(ScanedPanels);
					}
					catch (Exception e)
					{

					}

				}
			}
			else
			{
				var ScanedPanels = new ScanedPanel();
				ScanedPanels.Guid = newGuId;
				ScanedPanels.PanelBarcodePhoto = "NULL";
				ScanedPanels.PanelSerialNumber = input.PanelSerialNumber;
				ScanedPanels.Longitude = "73.208";
				ScanedPanels.Latitude = "22.299";
				await _scanedPanelRepository.InsertAsync(ScanedPanels);
			}
		}

		public async Task DeleteScanedPanels(DeleteScanedPanel input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{

				var Panel = _scanedPanelRepository
				.GetAll()
				.WhereIf(
				 input.Id != 0,
				 p => p.Id == input.Id
				)
				.FirstOrDefault();
				#region Delete  Owner Signature 
				if (!String.IsNullOrEmpty(Panel.PanelBarcodePhoto))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\ScanedPanels\\" + Panel.PanelBarcodePhoto;
					System.IO.File.Delete(filepath);
				}

				await _scanedPanelRepository.DeleteAsync(input.Id);

				#endregion
			}
		}
		public async Task<ScanedPanelEditDto> GetScanedPanelsById(DeleteScanedPanel input)
		{
			var Panel = (await _scanedPanelRepository.GetAsync(input.Id)).MapTo<ScanedPanelEditDto>();
			return Panel;
		}
		public async Task UpdateJobScanedPanels(ScanedPanelEditDto input)
		{
			var ScanedPanels = await _scanedPanelRepository.GetAsync(input.Id);

			ScanedPanels.PanelSerialNumber = input.PanelSerialNumber;

			await _scanedPanelRepository.UpdateAsync(ScanedPanels);
		}
		public async Task InsertScanedPanelsImages(ScanedPanelEditDto input)
		{
			try
			{
				var newGuId = _jobRepository.GetAll().Where(p => p.Id == input.JobId).Select(p => p.Guid).FirstOrDefault();
				var scanedpanelid = _scanedPanelRepository.GetAll().Where(p => p.Guid == newGuId && p.PanelBarcodePhoto == "NULL").Select(p => p.Id).FirstOrDefault();
				var scanedPanel = await _scanedPanelRepository.GetAsync(scanedpanelid);

				scanedPanel.PanelBarcodePhoto = input.PanelBarcodePhoto;
				await _scanedPanelRepository.UpdateAsync(scanedPanel);
			}
			catch (Exception e)
			{ }

		}
		#endregion

		public ListResultDto<ScanedPanelViewDto> ScanedPanelView(ScanedPanelViewInputDto input)
		{
			var Panel_Photos = _scanedPanelRepository.GetAll().Where(p => p.Id == input.ScannedId && p.PanelBarcodePhoto != "NULL").ToList();
			foreach (var item in Panel_Photos)
			{
				item.Latitude = Convert.ToString(Math.Round(Convert.ToDecimal(item.Latitude), 6));
				item.Longitude = Convert.ToString(Math.Round(Convert.ToDecimal(item.Longitude), 6));
			}

			var PanelPhoto_List = Panel_Photos.MapTo<List<ScanedPanelViewDto>>();
			if (PanelPhoto_List.Count > 0)
			{
				Guid guid = Panel_Photos[0].Guid;
				PanelPhoto_List[0].FormId = _jobRepository.GetAll().Where(p => p.Guid == guid).Select(p => p.FormId).FirstOrDefault();
			}
			return new ListResultDto<ScanedPanelViewDto>(PanelPhoto_List);
		}

		public ListResultDto<ScanedInverterViewDto> ScanedInverterView(ScanedInverterViewInputDto input)
		{
			var Inverter_Photos = _scanedInverterRepository.GetAll().Where(p => p.Id == input.ScannedId && p.InverterBarcodePhoto != "NULL").ToList();
			foreach (var item in Inverter_Photos)
			{
				item.Latitude = Convert.ToString(Math.Round(Convert.ToDecimal(item.Latitude), 6));
				item.Longitude = Convert.ToString(Math.Round(Convert.ToDecimal(item.Longitude), 6));
			}

			var InverterPhoto_List = Inverter_Photos.MapTo<List<ScanedInverterViewDto>>();
			if (InverterPhoto_List.Count > 0)
			{
				Guid guid = Inverter_Photos[0].Guid;
				InverterPhoto_List[0].FormId = _jobRepository.GetAll().Where(p => p.Guid == guid).Select(p => p.FormId).FirstOrDefault();
			}
			return new ListResultDto<ScanedInverterViewDto>(InverterPhoto_List);
		}

		#region Scaned Inverter
		public async Task InsertScanedInverter(ScanedInverterEditDto input)
		{
			var newGuId = _jobRepository.GetAll().Where(p => p.Id == input.JobId).Select(p => p.Guid).FirstOrDefault();
			int count = 0;
			if (input.InverterSerialNumber.Contains(','))
			{
				string[] strArr = null;
				char[] splitchar = { ',' };
				strArr = input.InverterSerialNumber.Split(splitchar);
				for (count = 0; count <= strArr.Length - 1; count++)
				{
					try
					{
						var ScanedInverters = new ScanedInverter();
						ScanedInverters.Guid = newGuId;
						ScanedInverters.InverterBarcodePhoto = "NULL";
						ScanedInverters.InverterSerialNumber = strArr[count];
						ScanedInverters.Longitude = "73.208";
						ScanedInverters.Latitude = "22.299";
						await _scanedInverterRepository.InsertAsync(ScanedInverters);
					}
					catch (Exception e)
					{

					}

				}
			}
			else
			{
				var ScanedInverters = new ScanedInverter();
				ScanedInverters.Guid = newGuId;
				ScanedInverters.InverterBarcodePhoto = "NULL";
				ScanedInverters.InverterSerialNumber = input.InverterSerialNumber;
				ScanedInverters.Longitude = "73.208";
				ScanedInverters.Latitude = "22.299";
				await _scanedInverterRepository.InsertAsync(ScanedInverters);
			}
		}
		public async Task InsertScanedInverterImages(ScanedInverterEditDto input)
		{
			try
			{
				var newGuId = _jobRepository.GetAll().Where(p => p.Id == input.JobId).Select(p => p.Guid).FirstOrDefault();
				var scanedInverterid = _scanedInverterRepository.GetAll().Where(p => p.Guid == newGuId && p.InverterBarcodePhoto == "NULL").Select(p => p.Id).FirstOrDefault();
				var scanedscanedInverter = await _scanedInverterRepository.GetAsync(scanedInverterid);

				scanedscanedInverter.InverterBarcodePhoto = input.InverterBarcodePhoto;
				await _scanedInverterRepository.UpdateAsync(scanedscanedInverter);
			}
			catch (Exception e)
			{ }

		}
		public async Task<ScanedInverterEditDto> GetScanedInverterById(DeleteScanedInverterDto input)
		{
			var Inverter = (await _scanedInverterRepository.GetAsync(input.Id)).MapTo<ScanedInverterEditDto>();
			return Inverter;
		}
		public async Task UpdateJobScanedInverter(ScanedInverterEditDto input)
		{
			var Inverter = await _scanedInverterRepository.GetAsync(input.Id);

			Inverter.InverterSerialNumber = input.InverterSerialNumber;

			await _scanedInverterRepository.UpdateAsync(Inverter);
		}

		public async Task DeleteScanedInverters(DeleteScanedInverterDto input)
		{
			var Inverter = _scanedInverterRepository
			.GetAll()
			.WhereIf(
			 input.Id != 0,
			 p => p.Id == input.Id
			)
			.FirstOrDefault();
			#region Delete  Owner Signature 
			if (!String.IsNullOrEmpty(Inverter.InverterBarcodePhoto))
			{
				string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\ScanedInverters\\" + Inverter.InverterBarcodePhoto;
				System.IO.File.Delete(filepath);
			}

			await _scanedInverterRepository.DeleteAsync(input.Id);
		}
		#endregion

		public JobValidationStatus GetJobsByProjectNumberAndInstallerId(ProjectNumberInputDto input)
		{
			var status_val = new JobValidationStatus();
			status_val.Status = true;

			var job = _jobRepository
			   .GetAll()
			   .Where(p => p.ProjectNumber == input.ProjectNumber).ToList();

			int expectedinstalldate = input.ExpectedInstallDate.Day;
			int expectedinstallmonth = input.ExpectedInstallDate.Month;
			int expectedinstallyear = input.ExpectedInstallDate.Year;

			var installerjob = _jobRepository
			   .GetAll()
			   .Where(p => p.Installer_Id == input.Installer_Id && p.ExpectedInstallDate.Day == expectedinstalldate && p.ExpectedInstallDate.Month == expectedinstallmonth && p.ExpectedInstallDate.Year == expectedinstallyear).ToList();

			if (input.Id.ToString() != null || input.Id != 0)
			{
				job = job.Where(p => p.Id != input.Id).ToList();
				installerjob = installerjob.Where(p => p.Id != input.Id).ToList();
			}
			if (job.Count > 0 || installerjob.Count >= 2)
			{
				status_val.Status = false;
			}
			if (job.Count > 0)
			{
				status_val.Message = "Record with this Project Number already exists.";
			}
			if (installerjob.Count >= 2)
			{
				status_val.Message = "Installer is Assigned for more than 2 jobs for Same Date";
			}
			return status_val;
		}

		public JobValidationStatus GetJobsByProjectNumber(ProjectNumberInputDto input)
		{
			var status_val = new JobValidationStatus();
			status_val.Status = true;

			var job = _jobRepository
			   .GetAll()
			   .Where(p => p.ProjectNumber == input.ProjectNumber).ToList();

			if (input.Id.ToString() != null || input.Id != 0)
			{
				job = job.Where(p => p.Id != input.Id).ToList();
			}
			if (job.Count > 0)
			{
				status_val.Status = false;
			}
			if (job.Count > 0)
			{
				status_val.Message = "Record with this Project Number already exists.";
			}
			return status_val;
		}
		#endregion

		public string AppendTenantName(int TenantId, string Url)
		{
			var TenantName = _tenantRepository.GetAll().Where(T => T.Id == TenantId).Select(T => T.TenancyName).FirstOrDefault();
			string NewUrl = Url.Replace("{TENANCY_NAME}", TenantName);

			return NewUrl;
		}

		public string GetTenantName()
		{
			var TenantName = _tenantRepository.GetAll().Where(T => T.Id == AbpSession.TenantId).Select(T => T.TenancyName).FirstOrDefault();

			return TenantName;
		}

		public string GetFormId(Guid guid)
		{
			var FormId = _jobRepository.GetAll().Where(T => T.Guid == guid).Select(T => T.FormId).FirstOrDefault();

			return FormId;
		}

		public string GetFormIdFromId(int Id)
		{
			var FormId = _jobRepository.GetAll().Where(T => T.Id == Id).Select(T => T.FormId).FirstOrDefault();

			return FormId;
		}
	}
}

