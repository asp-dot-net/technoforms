﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using TechnoForms.Inverters;
using TechnoForms.Panels;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(Inverter))]
    public class JobInverterModelListDto : FullAuditedEntityDto
    {
        public string ManufacturarName { get; set; }
        public string Series { get; set; }
        public string ModelNumber { get; set; }
        public decimal ACPower { get; set; }
        public decimal Sizes { get; set; }
        public DateTime ApprovalDate { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}
