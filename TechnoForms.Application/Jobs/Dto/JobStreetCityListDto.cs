﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostCodes;
using TechnoForms.StreetTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(PostCode))]
    public class JobStreetCityListDto : FullAuditedEntityDto
    {
        public string Postcode { get; set; }
        public string Suburb { get; set; }
    }
}
