﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(SystemType))]
    public class SystemTypeListDto : FullAuditedEntityDto
    {
        public string SystemTypeName { get; set; }
    }
}
