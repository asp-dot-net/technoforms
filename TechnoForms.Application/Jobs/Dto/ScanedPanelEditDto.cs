﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.ScanedPanels;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(ScanedPanel))]
    public class ScanedPanelEditDto : FullAuditedEntityDto
    {
        public int TenantId { get; set; }
        public string PanelSerialNumber { get; set; }
        public Guid Guid { get; set; }
        public int JobId { get; set; }
        public string PanelBarcodePhoto { get; set; }
    }
}
