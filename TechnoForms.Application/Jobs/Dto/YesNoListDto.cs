﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(YesNo))]
    public class YesNoListDto : FullAuditedEntityDto
    {
        public string YesNoName { get; set; }
    }
}
