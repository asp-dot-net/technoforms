﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(RoofType))]
    public class RoofTypeListDto : FullAuditedEntityDto
    {
        public string RoofTypeName { get; set; }
    }
}
