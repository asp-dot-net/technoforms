﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    public class CustomerSignatureLogDto
    {
        public int Id { get; set; }
        public string RequestSendBy { get; set; }
        public DateTime TokenDate { get; set; }
        public string TokenDateFormat { get; set; }
        public bool IsOwner { get; set; }
        public DateTime ResponseDate { get; set; }
        public string ResponseDateFormat { get; set; }
        public string ResponseByUserName { get; set; }
        public string RequestByUserName { get; set; }
        public long RequestByUserId { get; set; }
        public bool ResponseData { get; set; }
        public bool ReplyPending { get; set; }
        public bool IsNotOwner { get; set; }
        public int JobId { get; set; }
    }
}
