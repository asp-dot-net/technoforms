﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    public class ProjectNumberInputDto
    {
        public int Id { get; set; }
        public string ProjectNumber { get; set; }
        public int Installer_Id { get; set; }
        public DateTime ExpectedInstallDate { get; set; }
    }
}
