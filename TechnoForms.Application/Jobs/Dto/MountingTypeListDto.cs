﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(MountingType))]
    public class MountingTypeListDto : FullAuditedEntityDto
    {
        public string MountingTypeName { get; set; }
    }
}
