﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(SystemMountingType))]
    public class SystemMountingTypeListDto : FullAuditedEntityDto
    {
        public string SystemMountingTypeName { get; set; }
    }
}
