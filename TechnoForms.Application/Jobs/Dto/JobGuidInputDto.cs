﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    public class JobGuidInputDto
    {
        public int JobId { get; set; }
        public Guid JobGuid { get; set; }
    }
}
