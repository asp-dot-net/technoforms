﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.ScanedPanels;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(ScanedPanel))]
    public class ScanedPanelViewDto : FullAuditedEntity
    {
        public string FormId { get; set; }
        public string PanelSerialNumber { get; set; }
        public string PanelBarcodePhoto { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
    }
}
