﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(RecYear))]
    public class RECYearListDto : FullAuditedEntityDto
    {
        public int Year { get; set; }
        public string REC_Year { get; set; }
    }
}
