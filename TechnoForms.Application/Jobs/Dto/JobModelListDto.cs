﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using TechnoForms.Panels;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(Panel))]
    public class JobModelListDto : FullAuditedEntityDto
    {
        public string LicenceOrCertiHolder { get; set; }
        public string ModelNo { get; set; }
        public DateTime CECApprovedDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public decimal Size { get; set; }
        public string FireTested { get; set; }
    }
}
