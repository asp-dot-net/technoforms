﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using TechnoForms.ScanedInverters;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(ScanedInverter))]
    public class InverterPhotosListDto : FullAuditedEntity
    {
        public string FormId { get; set; }
        public string InverterSerialNumber { get; set; }
        public Guid Guid { get; set; }
        public Boolean IsScan { get; set; }
        public string InverterBarcodePhoto { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string InverterPhotoURL { get; set; }
    }
}
