﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PhotoTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(PhotoType))]
    public class FileTypeListDto : EntityDto
    {
        public virtual string PhotoTypeName { get; set; }
    }
}
