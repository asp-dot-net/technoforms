﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Photos;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(Photo))]
    public class DocumentListDto : EntityDto
    {
        public virtual string ImageName { get; set; }
        
        public virtual string ImagePath { get; set; }

        public virtual int FileType { get; set; }

        public virtual string FileTypeName { get; set; }

        public virtual int? OldSystemDirection { get; set; }

        public virtual Guid ImageGuid { get; set; }

        public int TenantId { get; set; }
    }
}
