﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    [AutoMap(typeof(JobBulkUpload))]
    public class JobBulkUploadDto : FullAuditedEntity
    {
        public virtual string InverterFileName { get; set; }

        public virtual string UploaderName { get; set; }
    }
}
