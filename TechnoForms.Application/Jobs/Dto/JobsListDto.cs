﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.ScanedInverters;
using TechnoForms.ScanedPanels;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(Job))]
    public class JobsListDto : FullAuditedEntityDto
    {

        public string FormId { get; set; }
        public int TenantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Company { get; set; }
        public string EmailId { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string PostalAddressType { get; set; }
        public string PostalDeliveryNumber { get; set; }
        public string PostalDeliveryType { get; set; }
        public string StreetAddress { get; set; }
        public string UnitNo { get; set; }
        public string UnitType { get; set; }
        public string StreetNo { get; set; }
        public string StreetName { get; set; }
        public string StreetType { get; set; }
        public string StreetCity { get; set; }
        public virtual int? Suburb_Id { get; set; }
        public string StreetState { get; set; }
        public string StreetPostCode { get; set; }
        public int JobStatus_Id { get; set; }
        public string JobStatusName { get; set; }
        public string InstallationStreetAddress { get; set; }
        public string InstallationUnitNo { get; set; }
        public string InstallationUnitType { get; set; }
        public string InstallationStreetNo { get; set; }
        public string InstallationStreetName { get; set; }
        public string InstallationStreetType { get; set; }
        public string InstallationStreetCity { get; set; }
        public virtual int? InstallationSuburb_Id { get; set; }
        public string InstallationStreetState { get; set; }
        public string InstallationStreetPostCode { get; set; }
        public bool installationAddresssameasHouseAddress { get; set; }
        //Site Details
        public int HouseType { get; set; }
        public string HouseTypeName { get; set; }
        public int RoofType { get; set; }
        public string RoofTypeName { get; set; }
        public string NMINo { get; set; }
        //public string RegPlanNo { get; set; }
        //public string LotNo { get; set; }
        public string MeterNumber { get; set; }
        public int? EnergyDestributar { get; set; }
        public int? EnergyRetailer { get; set; }
        public string AemoDERjobID { get; set; }
        //Panel Details
        public string PanelName { get; set; }
        public string BrandName { get; set; }
        public int Panel_id { get; set; }
        public string PanelModel { get; set; }
        public string PanelLogo { get; set; }
        public string PanelLogoPath { get; set; }
        public decimal PanelSize { get; set; }
        public int NoOfPanels { get; set; }
        public decimal SystemCapacity { get; set; }

        //Inverters Details

        public string InverterName { get; set; }
        public string InverterBrandName { get; set; }
        public int Inverter_id { get; set; }
        public string InverterModel { get; set; }
        public string InverterLogo { get; set; }
        public string InverterLogoPath { get; set; }
        public string InverterSeries { get; set; }
        public decimal NoOfInverter { get; set; }
        public decimal InverterOutput { get; set; }

        //Installation Details

        //public int SolarType_Id { get; set; }
        //public string SolarTypeName { get; set; }

        public int SystemType_Id { get; set; }
        public string SystemTypeName { get; set; }

        public DateTime ExpectedInstallDate { get; set; }

        //public int RoofFacingPanel_Id { get; set; }
        //public string RoofFacingPanelName { get; set; }

        //public int Sugstthisaddress_Id { get; set; }
        //public string SugstthisaddressName { get; set; }

        //public int LocationOfNewSystem_Id { get; set; }
        //public string LocationOfNewSystemName { get; set; }

        public int Customer_Id { get; set; }
        public int CreateFor_Id { get; set; }
        public string CreateForName { get; set; }

        public int Installer_Id { get; set; }
        public string InstallerName { get; set; }

        public int Designer_Id { get; set; }
        public string DesignerName { get; set; }

        public int Electrician_Id { get; set; }
        public string ElectricianName { get; set; }

        public string ProjectNumber { get; set; }
        public string InstallerNote { get; set; }

        //REC Details
        //public int Typeofsystem_Id { get; set; }
        //public string TypeofsystemName { get; set; }

        //public int Typeofconnection_Id { get; set; }
        //public string TypeofconnectionName { get; set; }

        //public int Mountingtype_Id { get; set; }
        //public string MountingtypeName { get; set; }

        //public int Auditreportavailable_Id { get; set; }
        //public string AuditreportavailableName { get; set; }

        //public int Installingcompleteunit_Id { get; set; }
        //public string InstallingcompleteunitName { get; set; }

        //public string AddcapMoredetailed { get; set; }

        //public int RECYear_Id { get; set; }
        //public string RECYearName { get; set; }

        //public int Defaultresourceavailfigure_Id { get; set; }
        //public string DefaultresourceavailfigureName { get; set; }

        //public string ResourceAvailability { get; set; }
        //public string AccreditationCode { get; set; }
        //public string ExplanatoryNotes { get; set; }

        //public int CeatingCert_Id { get; set; }
        //public string CeatingCertName { get; set; }

        //public int CECAccreditationStatement_Id { get; set; }
        //public string CECAccreditationStatementName { get; set; }

        //public int GovernmentSitingApprovals_Id { get; set; }
        //public string GovernmentSitingApprovalsName { get; set; }

        //public int ElectricalSafetyDocumentation_Id { get; set; }
        //public string ElectricalSafetyDocumentationName { get; set; }

        //public int AustralianStandardsStatement_Id { get; set; }
        //public string AustralianStandardsStatementName { get; set; }

        //public int NotgridConnectedStatement_Id { get; set; }
        //public string NotgridConnectedStatementName { get; set; }

        //public string InstallPropertyName { get; set; }

        //public int SGUatThisAddress_Id { get; set; }
        //public string SGUatThisAddressName { get; set; }

        //public string DescribeAdequateIinfo { get; set; }
        //public string AdditionalSystemInformation { get; set; }

        //public int SystemMountingType_Id { get; set; }
        //public string SystemMountingTypeName { get; set; }

        //public int OwnerAddressType_Id { get; set; }
        //public string OwnerAddressTypeName { get; set; }

        //public int CustomerAddressType_Id { get; set; }
        //public string CustomerAddressTypeName { get; set; }

        public Guid Guid { get; set; }
        public bool InstallerExpire { get; set; }
        public bool PanelExpire { get; set; }
        public bool InverterExpire { get; set; }
        public string lastModifierUserName { get; set; }
        public string EnteredBy { get; set; }
        public int ScanedPanelCount { get; set; }
        public int ScanedInverterCount { get; set; }

        public string SignatureImageOwner { get; set; }
        public string OwnerPhoto { get; set; }

        public string InstComment { get; set; }
        public string OwnerComment { get; set; }

        public string SignatureImageInst { get; set; }
        public string InstallerPhoto { get; set; }

        public List<JobScanedPanel> JobScanedPanel_List { get; set; }
        public List<JobScanedInverter> JobScanedInverter_List { get; set; }

        public string ScannedPanelSerialNoList { get; set; }
        public string ScannedInverterSerialNoList { get; set; }

        public string GridClass { get; set; }
        public string ScanedPanelClass { get; set; }
        public string ScanedInverterClass { get; set; }
        public string OtherImagesClass { get; set; }
        public string CustomerSignatureClass { get; set; }
        public string InstallerSignatureClass { get; set; }

        public string CecSignatureClass { get; set; }
        public string EleSignatureClass { get; set; }
        public string CuckSignatureClass { get; set; }


        public bool IsScanedPanelImagesLinkShow { get; set; }
        public bool IsScanedInverterImagesLinkShow { get; set; }

        public virtual string Longitude_InstSign { get; set; }

        public virtual string Latitude_InstSign { get; set; }

        public virtual string Longitude_CustSign { get; set; }

        public virtual string Latitude_CustSign { get; set; }

        [AutoMapFrom(typeof(ScanedPanel))]
        public class JobScanedPanel
        {
            public int Id { get; set; }
            public string PanelSerialNumber { get; set; }
            public string PanelDeleteLink { get; set; }
        }
        [AutoMapFrom(typeof(ScanedInverter))]
        public class JobScanedInverter
        {
            public int Id { get; set; }
            public string InverterSerialNumber { get; set; }
        }

        public string NameWithAddress { get; set; }

        public Nullable<bool> IsVerifyCS { get; set; }
        public Nullable<bool> IsVerifyIN { get; set; }
        public Nullable<bool> IsVerifyPA { get; set; }
        public Nullable<bool> IsVerifyIS { get; set; }
        public Nullable<bool> IsVerifyPK { get; set; }
        public Nullable<bool> IsVerifySTC { get; set; }
        public Nullable<bool> IsVerifyELE { get; set; }
        public Nullable<bool> IsVerifyCUCK { get; set; }
        public Nullable<bool> IsVerifyImages { get; set; }

        public bool IsHistoric { get; set; }

        public string JobLatitude { get; set; }
        public string JobLongitude { get; set; }
        public string JobLatLong { get; set; }

        public string InstallerExpiryDays { get; set; }
        public string PanelExpiryDays { get; set; }
        public string InverterExpiryDays { get; set; }


        public string ElectricianSignature { get; set; }
        public string DesignerSignature { get; set; }
        public Nullable<System.DateTime> ElectricianSignatureDate { get; set; }
        public Nullable<System.DateTime> DesignerSignatureDate { get; set; }
        public string Longitude_ElecSign { get; set; }
        public string Latitude_ElecSign { get; set; }
        public string Longitude_DesiSign { get; set; }
        public string Latitude_DesiSign { get; set; }
        public virtual decimal? ExpectedSTC { get; set; }
    }


}
