﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    public class JobSTCDto
    {
        public string ExpectedSTC { get; set; }
    }
}
