﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(TypeOfConnection))]
    public class TypeOfConnectionListDto : FullAuditedEntityDto
    {
        public string TypeOfConnectionName { get; set; }
    }
}
