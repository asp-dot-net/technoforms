﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Energy;

namespace TechnoForms.Jobs.Dto
{
	[AutoMapFrom(typeof(EnergyDestributar))]
	public class JobGetEnergyDestributar : EntityDto
	{
		public virtual string Name { get; set; }
	}
}
