﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.Panels;

namespace TechnoForms.Jobs.Dto
{
    public class JobPanelDistinctListDto : FullAuditedEntityDto
    {
        public string LicenceOrCertiHolder { get; set; }
    }
}
