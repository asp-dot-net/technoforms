﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.Panels;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(Panel))]
    public class JobPanelListDto : FullAuditedEntityDto
    {
        public string LicenceOrCertiHolder { get; set; }
    }
}
