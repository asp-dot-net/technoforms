﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    public class JobValidationStatus
    {
        public bool Status { get; set; }
        public string Message { get; set; }

    }
}
