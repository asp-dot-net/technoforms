﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    public class MailMessageDetails
    {
        public string ToEmail { get; set; }
        public string CustomerName { get; set; }
    }
}
