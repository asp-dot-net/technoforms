﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    public class DeleteScanedInverterDto
    {
        public int Id { get; set; }

        public int TenantId { get; set; }
    }
}
