﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    public class CalenderInputDto
    {
        public int CreateFor_Id { get; set; }
        public int Installer_Id { get; set; }
        public string projectNumber { get; set; }
    }
}
