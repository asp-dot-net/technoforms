﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(TypeOfSystem))]
    public class TypeOfSystemListDto : FullAuditedEntityDto
    {
        public string TypeOfSystemName { get; set; }
    }
}
