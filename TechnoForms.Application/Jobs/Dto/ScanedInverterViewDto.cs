﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.ScanedInverters;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(ScanedInverter))]
    public class ScanedInverterViewDto : FullAuditedEntity
    {
        public string FormId { get; set; }
        public string InverterSerialNumber { get; set; }
        public string InverterBarcodePhoto { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
    }
}
