﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.ScanedInverters;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(ScanedInverter))]
    public class ScanedInverterEditDto : FullAuditedEntityDto
    {
        public int TenantId { get; set; }
        public string InverterSerialNumber { get; set; }
        public Guid Guid { get; set; }
        public string InverterBarcodePhoto { get; set; }
        public int JobId { get; set; }
    }
}
