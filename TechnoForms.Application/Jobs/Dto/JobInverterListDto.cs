﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.Inverters;
using TechnoForms.Panels;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(Inverter))]
    public class JobInverterListDto : FullAuditedEntityDto
    {
        public string ManufacturarName { get; set; }
    }
}
