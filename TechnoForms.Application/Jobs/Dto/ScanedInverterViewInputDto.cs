﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    public class ScanedInverterViewInputDto
    {
        public int ScannedId { get; set; }
    }
}
