﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(HouseType))]
    public class JobHouseTypeListDto : FullAuditedEntityDto
    {
        public string HouseTypeName { get; set; }
    }
}
