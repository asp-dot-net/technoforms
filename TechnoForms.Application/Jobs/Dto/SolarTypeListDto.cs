﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(SolarType))]
    public class SolarTypeListDto : FullAuditedEntityDto
    {
        public string SolarTypeeName { get; set; }
    }
}
