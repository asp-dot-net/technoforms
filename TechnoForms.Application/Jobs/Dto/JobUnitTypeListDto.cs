﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(UnitType))]
    public class JobUnitTypeListDto : FullAuditedEntityDto
    {
        public string UnitTypeName { get; set; }
    }
}
