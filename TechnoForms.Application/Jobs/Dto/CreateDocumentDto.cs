﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Photos;

namespace TechnoForms.Jobs.Dto
{
	[AutoMapTo(typeof(Photo))]
	public class CreateDocumentDto
	{
		[Required]
		[MaxLength(Photo.MaxImageNameLength)]
		public string ImageName { get; set; }

		public Guid Guid { get; set; }

		public int FileType { get; set; }

		public int? OldSystemDirection { get; set; }

		public Guid ImageGuid { get; set; }

		public string Longitude { get; set; }

		public string Latitude { get; set; }

		public int TenantId { get; set; }
	}
}
