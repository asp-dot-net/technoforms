﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.OldSystemDirections;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(OldSystemDirection))]
    public class OldSystemDirectionListDto : EntityDto
    {
        public virtual string DirectionName { get; set; }
    }
}
