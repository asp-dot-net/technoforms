﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(Job))]
    public class CalendarJobListDto
    {
        public int id { get; set; }
        public int day { get; set; }
        public int month { get; set; }
        public int year { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime ExpectedInstallDate { get; set; }
        public string InstallationStreetNo { get; set; }
        public string InstallationStreetName { get; set; }
        public string InstallationStreetType { get; set; }
        public string InstallationStreetCity { get; set; }
        public virtual int? InstallationSuburb_Id { get; set; }
        public string InstallationStreetState { get; set; }
        public string InstallationStreetPostCode { get; set; }
        public string Phone { get; set; }
        public string InstallerName { get; set; }
        public int Installer_Id { get; set; }
        public string projectNumber { get; set; }
    }
}
