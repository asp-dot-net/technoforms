﻿using Abp.Runtime.Validation;
using TechnoForms.Dto;

namespace TechnoForms.Jobs.Dto
{
    public class GetInput 
    {
        public string Filter { get; set; }
    }
}
