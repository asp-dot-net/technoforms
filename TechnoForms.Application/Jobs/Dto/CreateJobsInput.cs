﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;
using TechnoForms.StreetName;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapTo(typeof(Job))]
    public class CreateJobsInput
    {
        [Required]
        [MaxLength(Job.MaxFirstNameLength)]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Enter characters only.")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(Job.MaxLastNameLength)]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Enter characters only.")]
        public string LastName { get; set; }

        [MaxLength(Job.MaxCompanyLength)]
        public string Company { get; set; }

        [Required]
        [MaxLength(Job.MaxEmailIdLength)]
        [EmailAddress]
        public string EmailId { get; set; }

        [Required]
        [MaxLength(Job.MaxPhoneLength)]
        [RegularExpression(@"^(07|03|08|04|13|18)[\d]{8}$", ErrorMessage = "Enter valid Phone(eg. 07|03|08|04|13|18XXXXXXXX)")]
        public string Phone { get; set; }

        [MaxLength(Job.MaxFaxLength)]
        public string Fax { get; set; }

        public string PostalAddressType { get; set; }

        [MaxLength(Job.MaxPostalDeliveryNumberLength)]
        public string PostalDeliveryNumber { get; set; }

        public string PostalDeliveryType { get; set; }

        [MaxLength(Job.MaxStreetAddressLength)]
        public string StreetAddress { get; set; }

        [MaxLength(Job.MaxUnitNoLength)]
        public string UnitNo { get; set; }

        public string UnitType { get; set; }

        [Required]
        [MaxLength(Job.MaxStreetNoLength)]
        public string StreetNo { get; set; }

        [Required]
        public string StreetName { get; set; }

        [Required]
        public string StreetType { get; set; }

        [Required]
        [MaxLength(Job.MaxStreetCityLength)]
        public string StreetCity { get; set; }

        public virtual int? Suburb_Id { get; set; }

        [Required]
        [MaxLength(Job.MaxStreetStateLength)]
        public string StreetState { get; set; }

        [Required]
        [MaxLength(Job.MaxStreetPostCodeLength)]
        public string StreetPostCode { get; set; }

        //Installation Address Details

        [MaxLength(Job.MaxStreetAddressLength)]
        public string InstallationStreetAddress { get; set; }

        [MaxLength(Job.MaxUnitNoLength)]
        public string InstallationUnitNo { get; set; }

        public string InstallationUnitType { get; set; }

        [Required]
        [MaxLength(Job.MaxStreetNoLength)]
        public string InstallationStreetNo { get; set; }

        [Required]
        public string InstallationStreetName { get; set; }

        [Required]
        public string InstallationStreetType { get; set; }

        [Required]
        [MaxLength(Job.MaxStreetCityLength)]
        public string InstallationStreetCity { get; set; }

        public virtual int? InstallationSuburb_Id { get; set; }

        [Required]
        [MaxLength(Job.MaxStreetStateLength)]
        public string InstallationStreetState { get; set; }

        [Required]
        [MaxLength(Job.MaxStreetPostCodeLength)]
        public string InstallationStreetPostCode { get; set; }

        public bool installationAddresssameasHouseAddress { get; set; }
        public int JobStatus_Id { get; set; }
        //Site Details

        [Required]
        public int HouseType { get; set; }

        [Required]
        public int RoofType { get; set; }

        [Required]
        [MaxLength(Job.MaxNMINoLength)]
        public string NMINo { get; set; }

        [MaxLength(Job.MaxRegPlanNoLength)]
        public string RegPlanNo { get; set; }

        [MaxLength(Job.MaxLotNoLength)]
        public string LotNo { get; set; }

        [MaxLength(Job.MaxMeterNumberLength)]
        public string MeterNumber { get; set; }

        public int? EnergyDestributar { get; set; }

        public int? EnergyRetailer { get; set; }

        [MaxLength(Job.MaxAemoDERjobIDLength)]
        public string AemoDERjobID { get; set; }

        //Panel Details
        public const int MaxPanelNameLength = 20;

        [Required]
        [MaxLength(Job.MaxPanelNameLength)]
        public string PanelName { get; set; }

        [MaxLength(Job.MaxPanelNameLength)]
        public string BrandName { get; set; }

        [Required]
        public int Panel_id { get; set; }

        public string PanelModel { get; set; }
        public decimal PanelSize { get; set; }

        [Required]
        public int NoOfPanels { get; set; }
        public decimal SystemCapacity { get; set; }

        //Inverters Details

        [Required]
        [MaxLength(Job.MaxInverterNameLength)]
        public string InverterName { get; set; }

        [MaxLength(Job.MaxInverterNameLength)]
        public string InverterBrandName { get; set; }

        [Required]
        public int Inverter_id { get; set; }

        public string InverterModel { get; set; }
        [MaxLength(Job.MaxInverterNameLength)]
        public string InverterSeries { get; set; }

        [Required]
        public decimal NoOfInverter { get; set; }
        public decimal InverterOutput { get; set; }


        //Installation Details

        public int SolarType_Id { get; set; }
        public int SystemType_Id { get; set; }
        public DateTime ExpectedInstallDate { get; set; }
        public int RoofFacingPanel_Id { get; set; }
        public int Sugstthisaddress_Id { get; set; }
        public int LocationOfNewSystem_Id { get; set; }

        public int Customer_Id { get; set; }

        [Required]
        public int CreateFor_Id { get; set; }

        [Required]
        public int Installer_Id { get; set; }

        [Required]
        public int Designer_Id { get; set; }

        [Required]
        public int Electrician_Id { get; set; }

        [Required]
        public string ProjectNumber { get; set; }
        public string InstallerNote { get; set; }

        //GST Details

        public int? IsCustomerRegistredGST { get; set; }
        public string CustomerABN { get; set; }
        public string CustomerOrganizationName { get; set; }

        //REC Details
        public int Typeofsystem_Id { get; set; }
        public int Typeofconnection_Id { get; set; }
        public int Mountingtype_Id { get; set; }
        public int Auditreportavailable_Id { get; set; }

        public int Installingcompleteunit_Id { get; set; }
        [MaxLength(Job.MaxAddcapMoredetailedLength)]
        public string AddcapMoredetailed { get; set; }

        [Required]
        public int RECYear_Id { get; set; }
        public int Defaultresourceavailfigure_Id { get; set; }

        [MaxLength(Job.MaxResourceAvailabilityLength)]
        public string ResourceAvailability { get; set; }
        [MaxLength(Job.MaxAccreditationCodeLength)]
        public string AccreditationCode { get; set; }
        [MaxLength(Job.MaxExplanatoryNotesLength)]
        public string ExplanatoryNotes { get; set; }
        public int CeatingCert_Id { get; set; }

        public int CECAccreditationStatement_Id { get; set; }
        public int GovernmentSitingApprovals_Id { get; set; }
        public int ElectricalSafetyDocumentation_Id { get; set; }
        public int AustralianStandardsStatement_Id { get; set; }

        public int NotgridConnectedStatement_Id { get; set; }
        [MaxLength(Job.MaxInstallPropertyNameLength)]
        public string InstallPropertyName { get; set; }
        public int SGUatThisAddress_Id { get; set; }
        [MaxLength(Job.MaxDescribeAdequateIinfoLength)]
        public string DescribeAdequateIinfo { get; set; }

        [MaxLength(Job.MaxAdditionalSystemInformationLength)]
        public string AdditionalSystemInformation { get; set; }
        public int SystemMountingType_Id { get; set; }
        public int OwnerAddressType_Id { get; set; }
        public int CustomerAddressType_Id { get; set; }

        public virtual decimal ExpectedSTC { get; set; }

        public bool InvoiceGenereted { get; set; }

        public virtual string JobLatitude { get; set; }
        public virtual string JobLongitude { get; set; }
    }
}
