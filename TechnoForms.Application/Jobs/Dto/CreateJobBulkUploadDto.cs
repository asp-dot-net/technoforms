﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
	[AutoMapTo(typeof(JobBulkUpload))]
	public class CreateJobBulkUploadDto : FullAuditedEntity
	{
		[MaxLength(JobBulkUpload.MaxJobFileName)]
		public virtual string JobFileName { get; set; }
	}
}
