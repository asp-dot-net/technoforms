﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.STCPostCodes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(STCPostCode))]
    public class JobSTCPostCodeListDto : FullAuditedEntityDto
    {
        public int STCPostCodeFrom { get; set; }
        public int STCPostCodeTo { get; set; }
        public int STCZoneID { get; set; }
    }
}
