﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostalDeliveryTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(PostalDeliveryType))]
    public class PostalDeliveryTypeListDto : FullAuditedEntityDto
    {
        public string PostalDeliveryTypeName { get; set; }
    }
}
