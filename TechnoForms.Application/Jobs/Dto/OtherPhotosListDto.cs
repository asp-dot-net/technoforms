﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using TechnoForms.Photos;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(Photo))]
    public class OtherPhotosListDto : FullAuditedEntity
    {
        public string FormId { get; set; }
        public string ImageName { get; set; }
        public Guid Guid { get; set; }
        public int FileType { get; set; }
        public int? OldSystemDirection { get; set; }
        public Guid ImageGuid { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string FileTypeName { get; set; }
    }
}
