﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.StreetName;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(StreetNames))]
    public class JobStreetNamesListDto : FullAuditedEntityDto
    {
        public string StreetName { get; set; }
    }
}
