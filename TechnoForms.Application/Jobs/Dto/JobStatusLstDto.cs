﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.JobStatusName;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(JobStatus))]
    public class JobStatusLstDto : FullAuditedEntityDto
    {
        public string JobStatusName { get; set; }
    }
}
