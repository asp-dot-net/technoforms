﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.UnitTypes;

namespace TechnoForms.Jobs.Dto
{
    [AutoMapFrom(typeof(RoofFacingPanel))]
    public class RoofFacingPanelListDto : FullAuditedEntityDto
    {
        public string RoofFacingPanelName { get; set; }
    }
}
