﻿using Abp.Extensions;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Dto;

namespace TechnoForms.Jobs.Dto
{
    public class GetDocumentListInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public virtual int? FileType { get; set; }

        public virtual Guid Guid { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
