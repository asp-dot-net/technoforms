﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Installers.Dto;
using TechnoForms.Jobs.Dto;
using TechnoForms.PostCodes;
using TechnoForms.PostCodes.Dto;
using TechnoForms.States.Dto;

namespace TechnoForms.Jobs
{
    public interface IJobsAppService : IApplicationService
    {
        Task<PagedResultDto<JobsListDto>> GetJobsAsync(GetJobsInput input);
        Task<PagedResultDto<JobList>> GetJobList(JobListInput input);
        Task CreateJobs(CreateJobsInput input);
		Task CreateJobBulkUpload(CreateJobBulkUploadDto input);
		Task DeleteJobs(EntityDto input);
        Task CompleteJob(EntityDto input);
        Task UpdateJobs(JobsEditDto input);
        Task<JobsEditDto> GetJobsForEdit(EntityDto input);
        Task<JobBulkUploadDto> getJobBulkUpload();
        ListResultDto<PostalAddressTypeListDto> GetPostalAddressType();
        ListResultDto<PostalDeliveryTypeListDto> GetPostalDeliveryType();
        //ListResultDto<JobStreetNamesListDto> GetStreetNames();
        List<typeAhead> GetStreetNames(TypeAheadInput input);
        //ListResultDto<JobStreetTypeListDto> GetStreetType();
        List<typeAhead> GetStreetType(TypeAheadInput input);
        //ListResultDto<JobUnitTypeListDto> GetUnitType();
        List<typeAhead> GetUnitType(TypeAheadInput input);
        //ListResultDto<PostCodesListDto> GetAllPostCodes();
        List<typeAhead> GetAllPostCodes(TypeAheadInput input);
        ListResultDto<PostCodesListDto> GetPostCodesListBySuburb(EntityDto Input);
        ListResultDto<JobHouseTypeListDto> GetHouseType();
        ListResultDto<RoofTypeListDto> GetRoofType();
        ListResultDto<JobGetEnergyDestributar> GetEnergyDestributar();
        ListResultDto<JobGetEnergyRetailer> GetEnergyRetailer();
        ListResultDto<JobPanelListDto> GetpanelList();
        //ListResultDto<JobPanelListDto> GetDistinctpanelList();
        List<typeAhead> GetDistinctpanelList(TypeAheadInput input);
        ListResultDto<JobModelListDto> GetModelNoList(GetInput Input);
        ListResultDto<JobModelListDto> GetPanelListById(GetInput Input);

        //ListResultDto<JobInverterListDto> GetDistinctInverterList();
        List<typeAhead> GetDistinctInverterList(TypeAheadInput input);
        ListResultDto<JobInverterListDto> GetInverterList();
        ListResultDto<JobInverterModelListDto> GetInverterModelNoList(GetInput Input);
        ListResultDto<JobInverterModelListDto> GetInverterListById(GetInput Input);
        ListResultDto<SolarTypeListDto> GetSolarType();
        ListResultDto<SystemTypeListDto> GetSystemType();
        ListResultDto<RoofFacingPanelListDto> GetRoofFacingPanel();
        ListResultDto<YesNoListDto> GetYesNo();
        ListResultDto<TypeOfSystemListDto> GetTypeOfSystem();
        ListResultDto<TypeOfConnectionListDto> GetTypeofconnection();
        ListResultDto<MountingTypeListDto> GetMountingType();
        ListResultDto<SystemMountingTypeListDto> GetSystemMountingType();
        ListResultDto<RECYearListDto> GetRECYear();
        decimal GetExpectedSTC(GetSTCInput Input);
        Task<ListResultDto<InstallerListDto>> GetAllInstallerAsync();
        Task<JobDetailDto> GetJobsDetails(EntityDto input);
        ListResultDto<JobStatusLstDto> GetJobStatus();

        Task<ListResultDto<InstEmployeListDto>> GetInstallerEmployeInstallerByInstallerIdAsync(EntityDto input);
        Task<ListResultDto<InstEmployeListDto>> GetInstallerEmployeDesignerByInstallerIdAsync(EntityDto input);
        Task<ListResultDto<InstEmployeListDto>> GetInstallerEmployeElectricianByInstallerIdAsync(EntityDto input);
        ListResultDto<CalendarJobListDto> GetCalenderJobs(CalenderInputDto input);
        Task DeleteScanedPanels(DeleteScanedPanel input);
        Task<ScanedPanelEditDto> GetScanedPanelsById(DeleteScanedPanel input);
        Task UpdateJobScanedPanels(ScanedPanelEditDto input);

        Task<ScanedInverterEditDto> GetScanedInverterById(DeleteScanedInverterDto input);
        Task UpdateJobScanedInverter(ScanedInverterEditDto input);
        Task DeleteScanedInverters(DeleteScanedInverterDto input);

        Task<JobDetailDto> GetJobDetail(DocumentDetailInputDto input);
        Task<PagedResultDto<DocumentListDto>> GetDocumentList(GetDocumentListInput input);
        Task CreateDocument(CreateDocumentDto input);
        ListResultDto<FileTypeListDto> GetFileType();
        ListResultDto<OldSystemDirectionListDto> GetOldSystemDirection();
        Task DeleteDocs(EntityDto input);

        Task CreateCustomerSignSMS(EntityDto input);
        Task CreateCustomerSignEmail(EntityDto input);
        Task CreateInstallerSignSMS(EntityDto input);
        ListResultDto<CustomerSignatureLogDto> GetCustomerSignatureLog(EntityDto input);

        ListResultDto<ScanedPanelViewDto> ScanedPanelView(ScanedPanelViewInputDto input);
        ListResultDto<ScanedInverterViewDto> ScanedInverterView(ScanedInverterViewInputDto input);

        ListResultDto<PanelPhotosListDto> GetPanelPhotos(JobGuidInputDto input);
        ListResultDto<InverterPhotosListDto> GetInverterPhotos(JobGuidInputDto input);
        ListResultDto<OtherPhotosListDto> GetOtherPhotos(JobGuidInputDto input);
        ListResultDto<JobsListDto> GetCustomerSignaturePhotos(JobGuidInputDto input);
        Task updateVerifyPA(JobGuidInputDto input);
        Task updateVerifyIN(JobGuidInputDto input);
        Task updateVerifyIM(JobGuidInputDto input);
        Task updateVerifyCS(JobGuidInputDto input);
        Task updateVerifyIS(JobGuidInputDto input);

        Task updateVerifyCec(JobGuidInputDto input);
        Task updateVerifyEle(JobGuidInputDto input);
        Task updateVerifyCuck(JobGuidInputDto input);

        Task<JobDetailDto> GetJobsDetailsByGuid(JobGuidInputDto input);
        Task InsertScanedPanels(ScanedPanelEditDto input);
        Task InsertScanedPanelsImages(ScanedPanelEditDto input);
        Task InsertScanedInverter(ScanedInverterEditDto input);
        Task InsertScanedInverterImages(ScanedInverterEditDto input);
        JobValidationStatus GetJobsByProjectNumberAndInstallerId(ProjectNumberInputDto input);
        JobValidationStatus GetJobsByProjectNumber(ProjectNumberInputDto input);
        string GetTenantName();
        string GetFormId(Guid guid);
        string GetFormIdFromId(int Id);
    }
}
