﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.Linq.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.Inverters.Dto;
using System.Data.Entity;using System.Linq.Dynamic;
using System.IO;
using Abp.IO;
using TechnoForms.Authorization.Users;

namespace TechnoForms.Inverters
{
	[AbpAuthorize(AppPermissions.Pages_Masters_Inverter)]
    public class InverterAppService : TechnoFormsApplicationModule, IInverterAppService
    {
        private readonly IRepository<Inverter> _InverterRepository;
        private readonly IRepository<InverterBulkUpload> _InverterBulkUploadRepository;
        private readonly IAppFolders _appFolders;
        private readonly IRepository<User, long> _userRepository;

        public InverterAppService(IRepository<Inverter> InverterRepository,
            IRepository<InverterBulkUpload> InverterBulkUploadRepository,
            IAppFolders appFolders,
            IRepository<User, long> userRepository)
        {
            _InverterRepository = InverterRepository;
            _InverterBulkUploadRepository = InverterBulkUploadRepository;
            _appFolders = appFolders;
            _userRepository = userRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_Inverter_Create)]
        public async Task CreateInverter(CreateOrUpdateInverterDto input)
        {
            var Exist = _InverterRepository.GetAll().Where(p => p.ModelNumber == input.ModelNumber).Any();
            if (Exist)
            {
                var Inv = _InverterRepository.GetAll().Where(p => p.ModelNumber == input.ModelNumber).FirstOrDefault();

                if (Inv.InverterLogo != null)
                {
                    if (Inv.InverterLogo != Inv.InverterLogo)
                    {
                        var tempLogoPath = Path.Combine(_appFolders.TempFileDownloadFolder, Inv.InverterLogo);
                        FileHelper.DeleteIfExists(tempLogoPath);
                    }
                }

                //var inverter = await _InverterRepository.GetAsync(Inv);
                Inv.ManufacturarName = input.ManufacturarName;
                Inv.ModelNumber = input.ModelNumber;
                Inv.Series = input.Series;
                Inv.ACPower = input.ACPower;
                Inv.Sizes = input.Sizes;
                Inv.ApprovalDate = input.ApprovalDate;
                Inv.ExpiryDate = input.ExpiryDate;
                Inv.InverterLogo = input.InverterLogo;
                await _InverterRepository.UpdateAsync(Inv);

                if (Inv.InverterLogo != null)
                {
                    var ModelNoList = _InverterRepository.GetAll().Where(e => e.ManufacturarName == Inv.ManufacturarName).Select(e => e.ModelNumber).ToList();
                    foreach (var item in ModelNoList)
                    {
                        var LogoUpdate = _InverterRepository.GetAll().Where(e => e.ModelNumber == item).FirstOrDefault();
                        LogoUpdate.InverterLogo = Inv.InverterLogo;
                        await _InverterRepository.UpdateAsync(LogoUpdate);
                    }
                }
            }
            else
            {
                var Inverter = input.MapTo<Inverter>();
                if (Inverter.InverterLogo == null)
                {
                    var InverterLogo = _InverterRepository.GetAll().Where(e => e.ManufacturarName == Inverter.ManufacturarName && e.InverterLogo != null).Select(e => e.InverterLogo).FirstOrDefault();
                    Inverter.InverterLogo = InverterLogo;
                }
                await _InverterRepository.InsertAsync(Inverter);

                if (Inverter.InverterLogo != null)
                {
                    var ModelNoList = _InverterRepository.GetAll().Where(e => e.ManufacturarName == Inverter.ManufacturarName).Select(e => e.ModelNumber).ToList();
                    foreach (var item in ModelNoList)
                    {
                        var LogoUpdate = _InverterRepository.GetAll().Where(e => e.ModelNumber == item).FirstOrDefault();
                        LogoUpdate.InverterLogo = Inverter.InverterLogo;
                        await _InverterRepository.UpdateAsync(LogoUpdate);
                    }
                }
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_Inverter_Create)]
        public async Task CreateInverterBulkUpload(CreateInverterBulkUploadDto input)
        {
            var InverterBulkUpload = input.MapTo<InverterBulkUpload>();
            await _InverterBulkUploadRepository.InsertAsync(InverterBulkUpload);
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_Inverter_Delete)]
        public async Task DeleteInverter(EntityDto input)
        {
            var panel = _InverterRepository.GetAll().Where(P => P.Id == input.Id).FirstOrDefault();
            if (panel.InverterLogo != null)
            {
                var tempLogoPath = Path.Combine(_appFolders.TempFileDownloadFolder, panel.InverterLogo);
                FileHelper.DeleteIfExists(tempLogoPath);
            }
            await _InverterRepository.DeleteAsync(input.Id);
        }

        public async Task<PagedResultDto<InverterListDto>> getInverter(GetInverterInput input)
        {
            var inverter = _InverterRepository
            .GetAll()
            .WhereIf(
                !input.ManufacturarName.IsNullOrEmpty(),
                p => p.ManufacturarName.ToLower().Contains(input.ManufacturarName.ToLower())
            )
            .WhereIf(
                !input.ModelNumber.IsNullOrEmpty(),
                p => p.ModelNumber.ToLower().Contains(input.ModelNumber.ToLower())
            )
            .WhereIf(
                !Convert.ToString(input.ExpiryDate).IsNullOrEmpty(),
                p => p.ExpiryDate <= input.ExpiryDate
            );

            var resultCount = await inverter.CountAsync();
            var results = await inverter
                .AsNoTracking()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();
            return new PagedResultDto<InverterListDto>(resultCount, results.MapTo<List<InverterListDto>>());
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_Inverter_Edit)]
        public async Task<InverterEditDto> GetInverterForEdit(EntityDto input)
        {
            var inverter = (await _InverterRepository.GetAsync(input.Id)).MapTo<InverterEditDto>();
            //var Date =Convert.ToDate(inverter.ExpiryDate.) .  ToShortDateString();

            return inverter;
        }

        public async Task<InverterBulkUploadDto> GetInverterBulkUpload()
        {
            var inverter = new InverterBulkUploadDto();
            var inverterdata = _InverterBulkUploadRepository.GetAll().OrderByDescending(P => P.Id).FirstOrDefault();

            inverter = inverterdata.MapTo<InverterBulkUploadDto>();
            inverter.UploaderName = _userRepository.GetAll().Where(U => U.Id == inverter.CreatorUserId).Select(U => U.Name).FirstOrDefault() + " " + _userRepository.GetAll().Where(U => U.Id == inverter.CreatorUserId).Select(U => U.UserName).FirstOrDefault();

            return inverter;
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_Inverter_Edit)]
        public async Task UpdateInverter(InverterEditDto input)
        {
            var inverter = await _InverterRepository.GetAsync(input.Id);

            if (inverter.InverterLogo != null)
            {
                if (input.InverterLogo != inverter.InverterLogo)
                {
                    var tempLogoPath = Path.Combine(_appFolders.TempFileDownloadFolder, inverter.InverterLogo);
                    FileHelper.DeleteIfExists(tempLogoPath);
                }
            }

            inverter.ManufacturarName = input.ManufacturarName;
            inverter.ModelNumber = input.ModelNumber;
            inverter.Series = input.Series;
            inverter.ACPower = input.ACPower;
            inverter.Sizes = input.Sizes;
            inverter.ApprovalDate = input.ApprovalDate;
            inverter.ExpiryDate = input.ExpiryDate;
            inverter.InverterLogo = input.InverterLogo;
            await _InverterRepository.UpdateAsync(inverter);

            if (inverter.InverterLogo != null)
            {
                var ModelNoList = _InverterRepository.GetAll().Where(e => e.ManufacturarName == inverter.ManufacturarName).Select(e => e.ModelNumber).ToList();
                foreach (var item in ModelNoList)
                {
                    var LogoUpdate = _InverterRepository.GetAll().Where(e => e.ModelNumber == item).FirstOrDefault();
                    LogoUpdate.InverterLogo = inverter.InverterLogo;
                    await _InverterRepository.UpdateAsync(LogoUpdate);
                }
            }
        }

        public InverterOutputDto CheckInverterExists(InverterExistDto input)
        {
            var Result = new InverterOutputDto();
            Result.Status = true;

            var Panel = _InverterRepository
               .GetAll()
               .Where(I => I.ModelNumber == input.ModelNo).ToList();

            if (!string.IsNullOrEmpty(input.Id.ToString()) && input.Id != 0)
            {
                Panel = Panel.Where(p => p.Id != input.Id).ToList();
            }
            if (Panel.Count > 0)
            {
                Result.Status = false;
            }
            return Result;
        }
    }
}
