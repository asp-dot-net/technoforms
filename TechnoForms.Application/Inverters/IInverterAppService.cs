﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Inverters.Dto;

namespace TechnoForms.Inverters
{
    public interface IInverterAppService : IApplicationService
    {
        Task<PagedResultDto<InverterListDto>> getInverter(GetInverterInput input);
        Task CreateInverter(CreateOrUpdateInverterDto input);
		Task CreateInverterBulkUpload(CreateInverterBulkUploadDto input);
		Task DeleteInverter(EntityDto input);
        Task UpdateInverter(InverterEditDto input);
        Task<InverterEditDto> GetInverterForEdit(EntityDto input);
        Task<InverterBulkUploadDto> GetInverterBulkUpload();
        InverterOutputDto CheckInverterExists(InverterExistDto input);
    }
}
