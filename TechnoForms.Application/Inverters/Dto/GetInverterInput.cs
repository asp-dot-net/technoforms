﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Dto;

namespace TechnoForms.Inverters.Dto
{
    public class GetInverterInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public virtual string ManufacturarName { get; set; }

        public virtual string ModelNumber { get; set; }

        public virtual DateTime? ExpiryDate { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "ManufacturarName";
            }
        }
    }
}
