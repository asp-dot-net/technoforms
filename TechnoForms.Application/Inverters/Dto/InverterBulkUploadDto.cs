﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Inverters.Dto
{
    [AutoMap(typeof(InverterBulkUpload))]
    public class InverterBulkUploadDto : FullAuditedEntity
    {
        public virtual string InverterFileName { get; set; }
        
        public virtual string UploaderName { get; set; }
    }
}
