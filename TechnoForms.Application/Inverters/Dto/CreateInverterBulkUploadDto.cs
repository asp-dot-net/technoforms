﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Inverters.Dto
{
	[AutoMapTo(typeof(InverterBulkUpload))]
	public class CreateInverterBulkUploadDto : FullAuditedEntity
	{
		[MaxLength(InverterBulkUpload.MaxInverterFileName)]
		public virtual string InverterFileName { get; set; }
	}
}
