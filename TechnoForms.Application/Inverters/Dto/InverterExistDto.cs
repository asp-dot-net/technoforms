﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Inverters.Dto
{
    public class InverterExistDto : EntityDto
    {
        public string ModelNo { get; set; }
    }
}
