﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Inverters.Dto
{
    public class InverterOutputDto
    {
        public bool Status { get; set; }
    }
}
