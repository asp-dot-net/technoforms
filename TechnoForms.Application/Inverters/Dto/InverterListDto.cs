﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Inverters.Dto
{
    [AutoMapFrom(typeof(Inverter))]
    public class InverterListDto : FullAuditedEntityDto
    {
        public virtual string ManufacturarName { get; set; }

        public virtual string Series { get; set; }

        public virtual string ModelNumber { get; set; }

        public virtual decimal ACPower { get; set; }

        public virtual decimal Sizes { get; set; }

        public virtual DateTime ApprovalDate { get; set; }

        public virtual DateTime ExpiryDate { get; set; }
    }
}
