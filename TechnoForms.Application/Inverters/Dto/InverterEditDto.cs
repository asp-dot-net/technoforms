﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Inverters.Dto
{
    [AutoMap(typeof(Inverter))]
    public class InverterEditDto : EntityDto
    {
        [Required]
        [MaxLength(Inverter.MaxManufacturarNameLength)]
        public virtual string ManufacturarName { get; set; }

        [Required]
        [MaxLength(Inverter.MaxSeriesNameLength)]
        public virtual string Series { get; set; }

        [Required]
        [MaxLength(Inverter.MaxModelNumberLength)]
        public virtual string ModelNumber { get; set; }

        [Required]
        public virtual decimal ACPower { get; set; }

        [Required]
        public virtual decimal Sizes { get; set; }

        [Required]
        public virtual DateTime ApprovalDate { get; set; }

        [Required]
        public virtual DateTime ExpiryDate { get; set; }

		//[MaxLength(Inverter.MaxInverterLogo)]
		public virtual string InverterLogo { get; set; }
	}
}
