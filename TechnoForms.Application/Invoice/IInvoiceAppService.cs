﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Invoice.Dto;

namespace TechnoForms.Invoice
{
    public interface IInvoiceAppService : IApplicationService
    {
        Task<PagedResultDto<PendingInvoiceList>> GetGenerateInvoice(PendingInvoiceInput input);
        //ListResultDto<PendingInvoiceList> GetGenerateInvoice(PendingInvoiceInput input);
        PendingInvoiceList GetInvoiceDetailByTenantId(PendingInvoiceInput input);
        ListResultDto<InvoiceJobListDto> GetJobByTenantId(PendingInvoiceInput input);
        Task generateInvoice(PendingInvoiceInput input);
        Task<PagedResultDto<GeneratedInvoiceListDto>> GetGeneratedInvoice(GeneratedInvoiceInputDto input);
        GeneratedInvoiceListDto GetInvoiceDetailByInvoiceId(GetInvoiceInputDto input);
    }
}
