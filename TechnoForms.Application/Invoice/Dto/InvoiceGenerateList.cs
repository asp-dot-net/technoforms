﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.MultiTenancy;

namespace TechnoForms.Invoice.Dto
{
    // [AutoMapFrom(typeof(Tenant))]
    public class InvoiceGenerateList
    {
        public int id { get; set; }
        public string TenantName { get; set; }
        public decimal? TotalSTC { get; set; }
        public float STRRate { get; set; }
        public decimal TotalInvoiceAmount { get; set; }
    }
}
