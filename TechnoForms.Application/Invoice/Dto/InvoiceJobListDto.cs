﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Invoice.Dto
{
    public class InvoiceJobListDto : FullAuditedEntityDto
    {
        public int JobId { get; set; }
        public string ProjectNumber { get; set; }
        public decimal? TotalSTC { get; set; }
    }
}
