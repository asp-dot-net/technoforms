﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Invoice.Dto
{
    [AutoMapTo(typeof(TechnoForms.Invoices.Invoice))]
    public class CreateInvoiceInput
    {
        public string InvoiceNumber { get; set; }
        public int TenentId { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string JobIds { get; set; }
    }
}
