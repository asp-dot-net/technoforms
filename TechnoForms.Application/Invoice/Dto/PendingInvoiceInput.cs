﻿using System;
using TechnoForms.Dto;

namespace TechnoForms.Invoice.Dto
{
    public class PendingInvoiceInput : PagedAndSortedInputDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int TenantId { get; set; }

        public string invoiceNumber { get; set; }
    }
}
