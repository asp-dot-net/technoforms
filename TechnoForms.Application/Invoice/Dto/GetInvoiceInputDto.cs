﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Invoice.Dto
{
    public class GetInvoiceInputDto
    {
        public int InvoiceId { get; set; }
        public int tenantId { get; set; }
    }
}
