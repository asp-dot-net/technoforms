﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Invoice.Dto
{
    public class GeneratedInvoiceListDto : FullAuditedEntityDto
    {
        public string InvoiceNumber { get; set; }
        public string CustomerName { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal PaidAmount { get; set; }
        public decimal OutStandingAmount { get; set; }
        public decimal TenantId { get; set; }
    }
}
