﻿using System;
using TechnoForms.Dto;

namespace TechnoForms.Invoice.Dto
{
    public class InvoiceGenerateInput : PagedAndSortedInputDto
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public int TenantId { get; set; }
    }
}
