﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.Invoice.Dto;
using TechnoForms.Invoices;
using TechnoForms.Jobs;
using TechnoForms.MultiTenancy;
using Abp.Linq.Extensions;using System.Linq.Dynamic;

namespace TechnoForms.Invoice
{

    [AbpAuthorize(AppPermissions.Pages_Invoice)]
    public class InvoiceAppService : TechnoFormsAppServiceBase, IInvoiceAppService
    {
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<TechnoForms.Invoices.Invoice> _invoiceRepository;
        private readonly IRepository<InvoiceItem> _invoiceItemRepository;
        private readonly IRepository<TechnoForms.Payments.Payment> _paymentRepository;
        public InvoiceAppService(IRepository<Tenant> tenantRepository,
            IRepository<Job> jobRepository, IUnitOfWorkManager unitOfWorkManager,
            IRepository<TechnoForms.Invoices.Invoice> invoiceRepository,
            IRepository<InvoiceItem> invoiceItemRepository,
            IRepository<TechnoForms.Payments.Payment> paymentRepository)
        {
            _tenantRepository = tenantRepository;
            _jobRepository = jobRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _invoiceRepository = invoiceRepository;
            _invoiceItemRepository = invoiceItemRepository;
            _paymentRepository = paymentRepository;
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Invoice_Generate)]
        public async Task<PagedResultDto<PendingInvoiceList>> GetGenerateInvoice(PendingInvoiceInput input)
        {
            var now = DateTime.Now;
            var firstDayCurrentMonth = new DateTime(now.Year, now.Month, 1);
            var lastDayLastMonth = firstDayCurrentMonth.AddDays(-1);

            var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).Distinct().ToList();
            var invoicelist1 = new List<PendingInvoiceList>();

            var resultCount = 0;
            for (int i = 0; i < tenant.Count; i++)
            {
                using (_unitOfWorkManager.Current.SetTenantId(tenant[i].Id))
                {
                    var p_invoice = new PendingInvoiceList();
                    p_invoice.TenantName = tenant[i].FullName;
                    p_invoice.id = tenant[i].Id;
                    p_invoice.STRRate = tenant[i].STCCharges;

                    try
                    {


                        var job_count = (from jobdata in _jobRepository.GetAll()
                                         join t in _tenantRepository.GetAll() on jobdata.TenantId equals t.Id
                                         where jobdata.CreationTime <= lastDayLastMonth && jobdata.InvoiceGenereted == false
                                         select jobdata).ToList();
                        if (job_count.Count() > 0)
                        {
                            p_invoice.TotalSTC = job_count.Select(p => p.TotalSTC).Sum();
                            p_invoice.TotalInvoiceAmount = Convert.ToDecimal(job_count.Select(p => p.TotalSTC).Sum()) * Convert.ToDecimal(tenant[i].STCCharges);

                            if (p_invoice.TotalSTC != 0)
                            {
                                resultCount++;
                                invoicelist1.Add(p_invoice);
                            }

                        }
                    }
                    catch (Exception e)
                    { }
                }
            }
            return new PagedResultDto<PendingInvoiceList>(resultCount, invoicelist1);
            //  var invoice_list = (from tenant in _tenantRepository.GetAll()
            //                      join job in _jobRepository.GetAll() on tenant.Id equals job.TenantId
            //                      where tenant.IsDeleted == false && job.CreationTime <= lastDayLastMonth && job.InvoiceGenereted == false
            //                      select new PendingInvoiceList
            //                      {
            //                          TenantName = tenant.FullName,
            //                          id = tenant.Id,
            //                          STRRate = tenant.STCCharges
            //                      }).Distinct();

            //  var resultCount = await invoice_list.CountAsync();

            //  var results = await invoice_list
            //.AsNoTracking()
            //.ToListAsync();

            //  foreach (var item in results)
            //  {
            //      using (_unitOfWorkManager.Current.SetTenantId(item.id))
            //      {
            //          item.TotalSTS = _jobRepository.GetAll().Where(p => p.TenantId == item.id && p.CreationTime <= lastDayLastMonth).Select(p => p.TotalSTS).Sum();
            //          item.TotalInvoiceAmount = item.TotalSTS * Convert.ToDecimal(item.STRRate);
            //      }
            //  }
            //  return new PagedResultDto<PendingInvoiceList>(resultCount, results);
        }

        [UnitOfWork]
        public PendingInvoiceList GetInvoiceDetailByTenantId(PendingInvoiceInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var now = DateTime.Now;
                var firstDayCurrentMonth = new DateTime(now.Year, now.Month, 1);
                var lastDayLastMonth = firstDayCurrentMonth.AddDays(-1);

                var invoice = new PendingInvoiceList();
                var tenant = _tenantRepository.GetAll().Where(x => x.Id == input.TenantId).FirstOrDefault();

                invoice.TenantName = tenant.FullName;
                invoice.id = tenant.Id;
                invoice.STRRate = tenant.STCCharges;

                var job_count = (from jobdata in _jobRepository.GetAll()
                                 join t in _tenantRepository.GetAll() on jobdata.TenantId equals t.Id
                                 where jobdata.CreationTime <= lastDayLastMonth
                                 select jobdata).ToList();
                if (job_count.Count() > 0)
                {
                    invoice.TotalSTC = job_count.Select(p => p.TotalSTC).Sum();
                    invoice.TotalInvoiceAmount = Convert.ToDecimal(invoice.TotalSTC) * Convert.ToDecimal(invoice.STRRate);
                }

                //var invoice = (from tenant in _tenantRepository.GetAll()
                //               join job in _jobRepository.GetAll() on tenant.Id equals job.TenantId
                //               where tenant.IsDeleted == false && job.CreationTime <= lastDayLastMonth && tenant.Id == input.TenantId && job.InvoiceGenereted == false
                //               select new PendingInvoiceList
                //               {
                //                   TenantName = tenant.FullName,
                //                   id = tenant.Id,
                //                   STRRate = tenant.STCCharges
                //               }).FirstOrDefault();

                //invoice.TotalSTC = _jobRepository.GetAll().Where(p => p.TenantId == input.TenantId && p.CreationTime <= lastDayLastMonth).Select(p => p.TotalSTS).Sum();
                //invoice.TotalInvoiceAmount = invoice.TotalSTC * Convert.ToDecimal(invoice.STRRate);

                return invoice;
            }

        }

        [UnitOfWork]
        public ListResultDto<InvoiceJobListDto> GetJobByTenantId(PendingInvoiceInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var now = DateTime.Now;
                var firstDayCurrentMonth = new DateTime(now.Year, now.Month, 1);

                var lastDayLastMonth = firstDayCurrentMonth.AddDays(-1);

                var invoice = (from job in _jobRepository.GetAll()
                               where job.CreationTime <= lastDayLastMonth && job.TenantId == input.TenantId && job.InvoiceGenereted == false
                               //  where tenant.IsDeleted == false && job.CreationTime >= input.StartDate && job.CreationTime <= input.lastDayLastMonth
                               select new InvoiceJobListDto
                               {
                                   JobId = job.Id,
                                   ProjectNumber = job.ProjectNumber,
                                   TotalSTC = job.TotalSTC
                               }).ToList();

                return new ListResultDto<InvoiceJobListDto>(invoice);
            }
        }

        public async Task generateInvoice(PendingInvoiceInput input)
        {
            var now = DateTime.Now;
            var firstDayCurrentMonth = new DateTime(now.Year, now.Month, 1);
            var lastDayLastMonth = firstDayCurrentMonth.AddDays(-1);
            string jobids = "";

            var joblist = (from job in _jobRepository.GetAll()
                           where job.CreationTime <= lastDayLastMonth && job.TenantId == input.TenantId && job.InvoiceGenereted == false
                           select job).ToList();

            foreach (var item in joblist)
            {
                jobids += "," + item.Id;
            }
            if (jobids.StartsWith(","))
            {
                jobids = jobids.Remove(0, 1);
            }

            decimal TotalInvoiceAmount = 0;
            float STCRate = _tenantRepository.GetAll().Where(p => p.Id == input.TenantId).Select(p => p.STCCharges).FirstOrDefault();
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                decimal? TotalSTC = _jobRepository.GetAll().Where(p => p.TenantId == input.TenantId && p.CreationTime <= lastDayLastMonth).Select(p => p.TotalSTC).Sum();
                TotalInvoiceAmount = Convert.ToDecimal(TotalSTC * Convert.ToDecimal(STCRate));
            }

            var job_list = new TechnoForms.Invoices.Invoice();
            job_list.TenantId = input.TenantId;
            job_list.InvoiceNumber = input.invoiceNumber;
            job_list.InvoiceAmount = TotalInvoiceAmount;
            job_list.Invoice_Status = 0;
            job_list.JobIds = jobids;

            await _invoiceRepository.InsertAndGetIdAsync(job_list);

            foreach (var item in joblist)
            {
                var invoiceitem = new InvoiceItem();
                invoiceitem.InvoiceId = job_list.Id;
                invoiceitem.JobId = item.Id;
                invoiceitem.TenantId = input.TenantId;
                await _invoiceItemRepository.InsertAsync(invoiceitem);
                var job = await _jobRepository.GetAsync(item.Id);
                job.InvoiceGenereted = true;
                await _jobRepository.UpdateAsync(job);
            }

        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Invoice_Generated)]
        public async Task<PagedResultDto<GeneratedInvoiceListDto>> GetGeneratedInvoice(GeneratedInvoiceInputDto input)
        {
            var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            var invoicelist1 = new List<GeneratedInvoiceListDto>();

            var resultCount = 0;
            for (int i = 0; i < tenant.Count; i++)
            {
                using (_unitOfWorkManager.Current.SetTenantId(tenant[i].Id))
                {
                    try
                    {


                        var invoice = (from datainvoice in _invoiceRepository.GetAll()
                                       join datatenant in _tenantRepository.GetAll() on datainvoice.TenantId equals datatenant.Id
                                       where datainvoice.Invoice_Status == 0
                                       select new GeneratedInvoiceListDto
                                       {
                                           InvoiceNumber = datainvoice.InvoiceNumber,
                                           InvoiceAmount = datainvoice.InvoiceAmount,
                                           CustomerName = datatenant.FullName,
                                           Id = datainvoice.Id,
                                           TenantId = datainvoice.TenantId,
                                           CreationTime = datainvoice.CreationTime
                                       });
                        resultCount = await invoice.CountAsync();


                        var results = await invoice
                   .AsNoTracking()
                   .OrderBy(input.Sorting)
                   .PageBy(input)
                   .ToListAsync();

                        //            var results = await invoice
                        //.AsNoTracking()
                        //.ToListAsync();

                        foreach (var invoiceitem in results)
                        {
                            invoicelist1.Add(invoiceitem);
                        }
                    }
                    catch (Exception e) { }
                }
            }
            return new PagedResultDto<GeneratedInvoiceListDto>(resultCount, invoicelist1);
        }
        [UnitOfWork]
        public GeneratedInvoiceListDto GetInvoiceDetailByInvoiceId(GetInvoiceInputDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(input.tenantId))
            {
                var invoicedetail = (from invoice in _invoiceRepository.GetAll()
                                     join tanantdata in _tenantRepository.GetAll() on invoice.TenantId equals tanantdata.Id
                                     where invoice.Id == input.InvoiceId
                                     select new GeneratedInvoiceListDto
                                     {
                                         CustomerName = tanantdata.FullName,
                                         InvoiceAmount = invoice.InvoiceAmount,
                                         InvoiceNumber = invoice.InvoiceNumber
                                     }).FirstOrDefault();

                int totalpayment = (_paymentRepository.GetAll().Where(p => p.Invoice_ID == input.InvoiceId).ToList().Count);
                if (totalpayment > 0)
                {
                    invoicedetail.PaidAmount = _paymentRepository.GetAll().Where(p => p.Invoice_ID == input.InvoiceId).Sum(p => p.amount);
                }
                else
                {
                    invoicedetail.PaidAmount = 0;
                }

                invoicedetail.OutStandingAmount = invoicedetail.InvoiceAmount - invoicedetail.PaidAmount;

                return invoicedetail;
            }
        }
    }
}
