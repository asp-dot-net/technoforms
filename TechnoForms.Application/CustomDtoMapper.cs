using AutoMapper;
using TechnoForms.Authorization.Users;
using TechnoForms.Authorization.Users.Dto;

namespace TechnoForms
{
    internal static class CustomDtoMapper
    {
        public static void CreateMappings(IMapperConfigurationExpression mapper)
        {
            mapper.CreateMap<User, UserEditDto>()
                .ForMember(dto => dto.Password, options => options.Ignore())
                .ReverseMap()
                .ForMember(user => user.Password, options => options.Ignore());
        }
    }
}