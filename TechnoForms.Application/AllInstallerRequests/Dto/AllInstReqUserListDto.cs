﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization.Users;

namespace TechnoForms.AllInstallerRequests.Dto
{
    [AutoMapFrom(typeof(User))]
    public class AllInstReqUserListDto : FullAuditedEntityDto
    {
        public string UserName { get; set; }

        public List<AdminUserListRoleDto> Roles { get; set; }

        [AutoMapFrom(typeof(UserRole))]
        public class AdminUserListRoleDto
        {
            public int RoleId { get; set; }

            public string RoleName { get; set; }
        }
    }
}
