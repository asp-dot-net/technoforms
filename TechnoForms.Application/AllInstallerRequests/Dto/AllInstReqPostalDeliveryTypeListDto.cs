﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostalDeliveryTypes;

namespace TechnoForms.AllInstallerRequests.Dto
{
    [AutoMapFrom(typeof(PostalDeliveryType))]
    public class AllInstReqPostalDeliveryTypeListDto : FullAuditedEntityDto
    {
        public virtual string PostalDeliveryTypeName { get; set; }
    }
}
