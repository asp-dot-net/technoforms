﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostCodes;

namespace TechnoForms.AllInstallerRequests.Dto
{
    [AutoMapFrom(typeof(PostCode))]
    public class AllInstReqPostCodeListDto : FullAuditedEntityDto
    {
        public string PostCode { get; set; }
    }
}
