﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.AllInstallerRequests.Dto;
using TechnoForms.Authorization.Roles;
using TechnoForms.Authorization.Users;
using TechnoForms.CustomerInstallers;
using TechnoForms.Installers;
using TechnoForms.InvitationsStatus;
using TechnoForms.MultiTenancy;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;
using TechnoForms.PostCodes;
using TechnoForms.States;
using TechnoForms.StreetName;
using TechnoForms.StreetTypes;
using TechnoForms.UnitTypes;
using Abp.Linq.Extensions;using System.Linq.Dynamic;

namespace TechnoForms.AllInstallerRequests
{
    public class AllInstallerRequestsAppService : TechnoFormsApplicationModule, IAllInstallerRequestsAppService
    {
        private readonly IRepository<Installer> _InstallerRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<CustomerInstaller> _customerInstallerRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<PostCode> _postCodeRepository;
        private readonly IRepository<PostalAddressType> _addressTypeRepository;
        private readonly IRepository<PostalDeliveryType> _delevaryTypeRepository;
        private readonly IRepository<UnitType> _unitTypeRepository;
        private readonly IRepository<StreetType> _streetTypeRepository;
        private readonly IRepository<StreetNames> _streetNameRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<InvitationStatus> _invitationStatusRepository;

        public AllInstallerRequestsAppService(IRepository<Installer> InstallerRepository, IRepository<User, long> UserRepository, IRepository<UserRole, long> UserRoleRepository, IRepository<Role> RoleRepository, IRepository<State> StateRepository, IRepository<PostCode> PostCodeRepository, IRepository<PostalAddressType> AddressTypeRepository, IRepository<PostalDeliveryType> DeliveryTypeRepository, IRepository<UnitType> UnitTypeRepository, IRepository<StreetType> StreetTypeRepository, IRepository<StreetNames> StreetNameRepository, IRepository<CustomerInstaller> CustomerRepository, IUnitOfWorkManager unitOfWorkManager, IRepository<Tenant> tenantRepository, IRepository<InvitationStatus> InvitationStatusRepository)
        {
            _InstallerRepository = InstallerRepository;
            _userRepository = UserRepository;
            _userroleRepository = UserRoleRepository;
            _roleRepository = RoleRepository;
            _customerInstallerRepository = CustomerRepository;

            _stateRepository = StateRepository;
            _postCodeRepository = PostCodeRepository;
            _addressTypeRepository = AddressTypeRepository;
            _delevaryTypeRepository = DeliveryTypeRepository;
            _unitTypeRepository = UnitTypeRepository;
            _streetTypeRepository = StreetTypeRepository;
            _streetNameRepository = StreetNameRepository;

            _unitOfWorkManager = unitOfWorkManager;
            _tenantRepository = tenantRepository;
            _invitationStatusRepository = InvitationStatusRepository;
        }

        [UnitOfWork]
        public async Task<PagedResultDto<AllInstReqInvitationListDto>> GetAllInstReqInvitation(GetAllInstReqInvitationInput input)
        {
            var resultCount = 0;
            var Invitation = new List<AllInstReqInvitationListDto>();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                //IsApproved == 0 ----- Pending
                //IsApproved == 1 ----- Approved
                //IsApproved == 2 ----- Rejected
                var Invitation_List = _customerInstallerRepository
               .GetAll();

                var ApprovedStatus = new int[] { 0, 1, 2 };
                var InvitationList = (from Inst in Invitation_List
                                      join Us in _InstallerRepository.GetAll() on Inst.InstallerId equals Us.Id into Usjoined
                                      from Us in Usjoined.DefaultIfEmpty()
                                      where (Us.IsApproved == 1 && Inst.Inst_Request_status == input.InstallerStatus && Inst.Request_status == input.AdminStatus)
                                      group Inst by Inst into instGrouped
                                      select instGrouped.Key);

                resultCount = await InvitationList.CountAsync();
                var results = await InvitationList
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                Invitation = results.MapTo<List<AllInstReqInvitationListDto>>();
                foreach (var item in Invitation)
                {
                    item.FullName = _InstallerRepository.GetAll().Where(p => p.Id == item.InstallerId).Select(p => p.FullName).FirstOrDefault();
                    item.InstallerExpiry = _InstallerRepository.GetAll().Where(p => p.Id == item.InstallerId).Select(p => p.InstallerExpiry).FirstOrDefault();
                    item.InsAccreExDate = _InstallerRepository.GetAll().Where(p => p.Id == item.InstallerId).Select(p => p.InsAccreExDate).FirstOrDefault();
                    item.CustName = _tenantRepository.GetAll().Where(p => p.Id == item.TenantId).Select(p => p.FullName).FirstOrDefault();
                }
            }
            return new PagedResultDto<AllInstReqInvitationListDto>(resultCount, Invitation.MapTo<List<AllInstReqInvitationListDto>>());
        }

        [UnitOfWork]
        public async Task<AllInstReqInvitationDetailDto> GetAllInstReqInvitationDetail(EntityDto input)
        {
            var installer = new AllInstReqInvitationDetailDto();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var custInst = (await _customerInstallerRepository.GetAsync(input.Id));

                installer = (await _InstallerRepository.GetAsync(custInst.InstallerId)).MapTo<AllInstReqInvitationDetailDto>();
                installer.AddressTypeName = _addressTypeRepository.GetAll().Where(p => p.Id == installer.AddressType).Select(p => p.PostalAddressTypeName).FirstOrDefault();
                installer.PostalDelTypeName = _delevaryTypeRepository.GetAll().Where(p => p.Id == installer.PostalDelType).Select(p => p.PostalDeliveryTypeName).FirstOrDefault();
            }
            return installer;
        }



        public ListResultDto<AllInstReqInvitationStatusListDto> GetInvitationStatus()
        {
            var invitationStatus = _invitationStatusRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AllInstReqInvitationStatusListDto>(invitationStatus.MapTo<List<AllInstReqInvitationStatusListDto>>());
        }

        public ListResultDto<AllInstReqPostalAddressTypeListDto> GetPostalAddressType()
        {
            var addresstype = _addressTypeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AllInstReqPostalAddressTypeListDto>(addresstype.MapTo<List<AllInstReqPostalAddressTypeListDto>>());
        }

        public ListResultDto<AllInstReqPostalDeliveryTypeListDto> GetPostalDeliveryType()
        {
            var delevarytype = _delevaryTypeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AllInstReqPostalDeliveryTypeListDto>(delevarytype.MapTo<List<AllInstReqPostalDeliveryTypeListDto>>());
        }

        public ListResultDto<AllInstReqPostCodeListDto> GetPostCode()
        {
            var postcode = _postCodeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AllInstReqPostCodeListDto>(postcode.MapTo<List<AllInstReqPostCodeListDto>>());
        }

        public ListResultDto<AllInstReqStateListDto> GetState()
        {
            var states = _stateRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AllInstReqStateListDto>(states.MapTo<List<AllInstReqStateListDto>>());
        }

        public ListResultDto<AllInstReqStreetNameListDto> GetStreetName()
        {
            var streername = _streetNameRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AllInstReqStreetNameListDto>(streername.MapTo<List<AllInstReqStreetNameListDto>>());
        }

        public ListResultDto<AllInstReqStreetTypeListDto> GetStreetType()
        {
            var streettype = _streetTypeRepository
              .GetAll()
              .OrderByDescending(p => p.Id)
              .ToList();

            return new ListResultDto<AllInstReqStreetTypeListDto>(streettype.MapTo<List<AllInstReqStreetTypeListDto>>());
        }

        public ListResultDto<AllInstReqSuburbListDto> GetSuburb()
        {
            var suburb = _postCodeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AllInstReqSuburbListDto>(suburb.MapTo<List<AllInstReqSuburbListDto>>());
        }

        public ListResultDto<AllInstReqUnitTypeListDto> GetUnitType()
        {
            var unittype = _unitTypeRepository
                .GetAll()
                .OrderByDescending(p => p.Id)
                .ToList();

            return new ListResultDto<AllInstReqUnitTypeListDto>(unittype.MapTo<List<AllInstReqUnitTypeListDto>>());
        }

        [UnitOfWork]
        public ListResultDto<AllInstReqUserListDto> GetUsers()
        {
            var userlist = new List<User>();
            var user_list = new List<AllInstReqUserListDto>();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)
            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var User = _userRepository
                    .GetAll()
                    .OrderByDescending(p => p.Id)
                    .ToList();

                var Useritem = (from user in User
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                where (us != null && us.DisplayName == "Installer" && user.TenantId != null)
                                group user by user into userGrouped
                                select userGrouped.Key);
                foreach (var User_List in Useritem)
                {
                    userlist.Add(User_List);
                    user_list = userlist.MapTo<List<AllInstReqUserListDto>>();
                }
            }
            //}

            return new ListResultDto<AllInstReqUserListDto>(user_list);
        }
    }
}
