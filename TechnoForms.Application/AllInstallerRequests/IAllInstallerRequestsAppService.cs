﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.AllInstallerRequests.Dto;

namespace TechnoForms.AllInstallerRequests
{
    public interface IAllInstallerRequestsAppService
    {
        Task<PagedResultDto<AllInstReqInvitationListDto>> GetAllInstReqInvitation(GetAllInstReqInvitationInput input);
        Task<AllInstReqInvitationDetailDto> GetAllInstReqInvitationDetail(EntityDto input);

        ListResultDto<AllInstReqInvitationStatusListDto> GetInvitationStatus();
        ListResultDto<AllInstReqUserListDto> GetUsers();
        ListResultDto<AllInstReqStateListDto> GetState();
        ListResultDto<AllInstReqPostCodeListDto> GetPostCode();
        ListResultDto<AllInstReqSuburbListDto> GetSuburb();
        ListResultDto<AllInstReqPostalAddressTypeListDto> GetPostalAddressType();
        ListResultDto<AllInstReqPostalDeliveryTypeListDto> GetPostalDeliveryType();
        ListResultDto<AllInstReqUnitTypeListDto> GetUnitType();
        ListResultDto<AllInstReqStreetTypeListDto> GetStreetType();
        ListResultDto<AllInstReqStreetNameListDto> GetStreetName();
    }
}
