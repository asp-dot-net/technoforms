﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using TechnoForms.AdminInvitation.Dto;
using TechnoForms.Authorization.Roles;
using TechnoForms.Authorization.Users;
using TechnoForms.CustomerInstallers;
using TechnoForms.Installers;
using TechnoForms.MultiTenancy;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;
using TechnoForms.PostCodes;
using TechnoForms.States;
using TechnoForms.StreetName;
using TechnoForms.StreetTypes;
using TechnoForms.UnitTypes;
using System.Data.Entity;using Abp.Linq.Extensions;using System.Linq.Dynamic;
using System.Data.Entity;using Abp.Linq.Extensions;using System.Linq.Dynamic;
using TechnoForms.InvitationsStatus;
using Abp.Runtime.Session;

namespace TechnoForms.AdminInvitation
{
    public class AdminInvitationAppService : TechnoFormsApplicationModule, IAdminInvitationAppService
    {
        private readonly IRepository<Installer> _InstallerRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<CustomerInstaller> _customerInstallerRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<PostCode> _postCodeRepository;
        private readonly IRepository<PostalAddressType> _addressTypeRepository;
        private readonly IRepository<PostalDeliveryType> _delevaryTypeRepository;
        private readonly IRepository<UnitType> _unitTypeRepository;
        private readonly IRepository<StreetType> _streetTypeRepository;
        private readonly IRepository<StreetNames> _streetNameRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<InvitationStatus> _invitationStatusRepository;
        private readonly IUserEmailer _userEmailer;
        private readonly IAbpSession _session;

        public AdminInvitationAppService(IRepository<Installer> InstallerRepository, IRepository<User, long> UserRepository,
            IRepository<UserRole, long> UserRoleRepository, IRepository<Role> RoleRepository, IRepository<State> StateRepository,
            IRepository<PostCode> PostCodeRepository, IRepository<PostalAddressType> AddressTypeRepository,
            IRepository<PostalDeliveryType> DeliveryTypeRepository, IRepository<UnitType> UnitTypeRepository,
            IRepository<StreetType> StreetTypeRepository, IRepository<StreetNames> StreetNameRepository,
            IRepository<CustomerInstaller> CustomerRepository, IUnitOfWorkManager unitOfWorkManager,
            IRepository<Tenant> tenantRepository, IRepository<InvitationStatus> InvitationStatusRepository
            , IUserEmailer userEmailer, IAbpSession session)
        {
            _InstallerRepository = InstallerRepository;
            _userRepository = UserRepository;
            _userroleRepository = UserRoleRepository;
            _roleRepository = RoleRepository;
            _customerInstallerRepository = CustomerRepository;

            _stateRepository = StateRepository;
            _postCodeRepository = PostCodeRepository;
            _addressTypeRepository = AddressTypeRepository;
            _delevaryTypeRepository = DeliveryTypeRepository;
            _unitTypeRepository = UnitTypeRepository;
            _streetTypeRepository = StreetTypeRepository;
            _streetNameRepository = StreetNameRepository;

            _unitOfWorkManager = unitOfWorkManager;
            _tenantRepository = tenantRepository;
            _invitationStatusRepository = InvitationStatusRepository;
            _userEmailer = userEmailer;
            _session = session;
        }

        [UnitOfWork]
        public async Task ApproveAdminInvitation(EntityDto input)
        {
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var invitationApprove = _customerInstallerRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();

                invitationApprove.Request_status = 1;

                var tenantid = _session.TenantId;

                string InstallerEmail = _InstallerRepository.GetAll().Where(p => p.Id == invitationApprove.InstallerId).Select(p => p.EmailId).FirstOrDefault();
                string InstallerFullName = _InstallerRepository.GetAll().Where(p => p.Id == invitationApprove.InstallerId).Select(p => p.FullName).FirstOrDefault();

                var tenid = _InstallerRepository.GetAll().Where(p => p.Id == invitationApprove.InstallerId).Select(p => p.TenantId).FirstOrDefault();
                //await _userEmailer.SendEmailInstallerApprovalResponseAsync(InstallerEmail, tenid.ToString(), InstallerFullName, "Approved", "");

                await _customerInstallerRepository.UpdateAsync(invitationApprove);
            }
        }

        [UnitOfWork]
        public async Task<PagedResultDto<AdminInvitationListDto>> GetAdminInvitation(GetInvitationInput input)
        {
            var resultCount = 0;
            var Invitation = new List<AdminInvitationListDto>();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                //IsApproved == 0 ----- Pending
                //IsApproved == 1 ----- Approved
                //IsApproved == 2 ----- Rejected

                var Instid = _InstallerRepository.GetAll().WhereIf(input.InstName != null, I => I.FullName.Contains(input.InstName)).Select(I => I.Id).ToList();
                var CustId = _tenantRepository.GetAll().WhereIf(input.CustName != null, T => T.FullName.Contains(input.CustName)).Select(I => I.Id).ToList();

                var ApprovedStatus = new int[] { 0, 1, 2 };
                if (input.PageStatus == 0)
                {
                    var Invitation_List = _customerInstallerRepository
                  .GetAll()
                  .WhereIf(input.InstName != null, C => Instid.Contains(C.InstallerId))
                  .WhereIf(input.CustName != null, C => CustId.Contains(C.TenantId));

                    var InvitationList = (from Inst in Invitation_List
                                          join Us in _InstallerRepository.GetAll() on Inst.InstallerId equals Us.Id into Usjoined
                                          from Us in Usjoined.DefaultIfEmpty()
                                          where (Us.IsApproved == 1 && Inst.Request_status == 0)
                                          group Inst by Inst into instGrouped
                                          select instGrouped.Key);
                    resultCount = await InvitationList.CountAsync();
                    var results = await InvitationList
                        .AsNoTracking()
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();
                    Invitation = results.MapTo<List<AdminInvitationListDto>>();
                }
                else
                {
                    var Invitation_List = _customerInstallerRepository
                  .GetAll()
                  .WhereIf(input.InstallerStatus != null, C => C.Inst_Request_status == input.InstallerStatus)
                  .WhereIf(input.AdminStatus != null, C => C.Request_status == input.AdminStatus)
                  .WhereIf(input.InstName != null, C => Instid.Contains(C.InstallerId))
                  .WhereIf(input.CustName != null, C => CustId.Contains(C.TenantId));

                    var InvitationList = (from Inst in Invitation_List
                                          join Us in _InstallerRepository.GetAll() on Inst.InstallerId equals Us.Id into Usjoined
                                          from Us in Usjoined.DefaultIfEmpty()
                                          where (Us.IsApproved == 1)
                                          group Inst by Inst into instGrouped
                                          select instGrouped.Key);
                    resultCount = await InvitationList.CountAsync();
                    var results = await InvitationList
                        .AsNoTracking()
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();
                    Invitation = results.MapTo<List<AdminInvitationListDto>>();
                }

                foreach (var item in Invitation)
                {
                    item.FullName = _InstallerRepository.GetAll().Where(p => p.Id == item.InstallerId).Select(p => p.FullName).FirstOrDefault();
                    item.InstallerExpiry = _InstallerRepository.GetAll().Where(p => p.Id == item.InstallerId).Select(p => p.InstallerExpiry).FirstOrDefault();
                    item.InsAccreExDate = _InstallerRepository.GetAll().Where(p => p.Id == item.InstallerId).Select(p => p.InsAccreExDate).FirstOrDefault();
                    item.CustName = _tenantRepository.GetAll().Where(p => p.Id == item.TenantId).Select(p => p.FullName).FirstOrDefault();
                }
            }
            return new PagedResultDto<AdminInvitationListDto>(resultCount, Invitation.MapTo<List<AdminInvitationListDto>>());
        }

        [UnitOfWork]
        public async Task<AdminInvitationDetailDto> GetAdminInvitationDetail(EntityDto input)
        {
            var installer = new AdminInvitationDetailDto();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var custInst = (await _customerInstallerRepository.GetAsync(input.Id));

                installer = (await _InstallerRepository.GetAsync(custInst.InstallerId)).MapTo<AdminInvitationDetailDto>();
                installer.AddressTypeName = _addressTypeRepository.GetAll().Where(p => p.Id == installer.AddressType).Select(p => p.PostalAddressTypeName).FirstOrDefault();
                installer.PostalDelTypeName = _delevaryTypeRepository.GetAll().Where(p => p.Id == installer.PostalDelType).Select(p => p.PostalDeliveryTypeName).FirstOrDefault();
            }
            return installer;
        }

        [UnitOfWork]
        public async Task<GetAdminInvitationForReject> GetAdminInvitationForReject(EntityDto input)
        {
            var invitation = new GetAdminInvitationForReject();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                invitation = (await _customerInstallerRepository.GetAsync(input.Id)).MapTo<GetAdminInvitationForReject>();
                invitation.FullName = _InstallerRepository.GetAll().Where(p => p.Id == invitation.InstallerId).Select(p => p.FullName).FirstOrDefault();

            }
            return invitation;
        }

        [UnitOfWork]
        public async Task RejectAdminInvitation(GetAdminInvitationForReject input)
        {
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var invitationReject = _customerInstallerRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();

                invitationReject.Request_status = 2;
                invitationReject.RejectReason = input.RejectReason;

                var tenantid = _session.TenantId;

                string InstallerEmail = _InstallerRepository.GetAll().Where(p => p.Id == invitationReject.InstallerId).Select(p => p.EmailId).FirstOrDefault();
                string InstallerFullName = _InstallerRepository.GetAll().Where(p => p.Id == invitationReject.InstallerId).Select(p => p.FullName).FirstOrDefault();

                //await _userEmailer.SendEmailInstallerApprovalResponseAsync(InstallerEmail, tenantid.ToString(), InstallerFullName, "Rejected", invitationReject.RejectReason);

                await _customerInstallerRepository.UpdateAsync(invitationReject);
            }
        }


        public ListResultDto<AdminInvitationStatusListDto> GetInvitationStatus()
        {
            var invitationStatus = _invitationStatusRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AdminInvitationStatusListDto>(invitationStatus.MapTo<List<AdminInvitationStatusListDto>>());
        }

        public ListResultDto<AdminPostalAddressTypeListDto> GetPostalAddressType()
        {
            var addresstype = _addressTypeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AdminPostalAddressTypeListDto>(addresstype.MapTo<List<AdminPostalAddressTypeListDto>>());
        }

        public ListResultDto<AdminPostalDeliveryTypeListDto> GetPostalDeliveryType()
        {
            var delevarytype = _delevaryTypeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AdminPostalDeliveryTypeListDto>(delevarytype.MapTo<List<AdminPostalDeliveryTypeListDto>>());
        }

        public ListResultDto<AdminPostCodeListDto> GetPostCode()
        {
            var postcode = _postCodeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AdminPostCodeListDto>(postcode.MapTo<List<AdminPostCodeListDto>>());
        }

        public ListResultDto<AdminStateListDto> GetState()
        {
            var states = _stateRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AdminStateListDto>(states.MapTo<List<AdminStateListDto>>());
        }

        public ListResultDto<AdminStreetNameListDto> GetStreetName()
        {
            var streername = _streetNameRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AdminStreetNameListDto>(streername.MapTo<List<AdminStreetNameListDto>>());
        }

        public ListResultDto<AdminStreetTypeListDto> GetStreetType()
        {
            var streettype = _streetTypeRepository
              .GetAll()
              .OrderByDescending(p => p.Id)
              .ToList();

            return new ListResultDto<AdminStreetTypeListDto>(streettype.MapTo<List<AdminStreetTypeListDto>>());
        }

        public ListResultDto<AdminSuburbListDto> GetSuburb()
        {
            var suburb = _postCodeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<AdminSuburbListDto>(suburb.MapTo<List<AdminSuburbListDto>>());
        }

        public ListResultDto<AdminUnitTypeListDto> GetUnitType()
        {
            var unittype = _unitTypeRepository
                .GetAll()
                .OrderByDescending(p => p.Id)
                .ToList();

            return new ListResultDto<AdminUnitTypeListDto>(unittype.MapTo<List<AdminUnitTypeListDto>>());
        }

        [UnitOfWork]
        public ListResultDto<AdminUserListDto> GetUsers()
        {
            var userlist = new List<User>();
            var user_list = new List<AdminUserListDto>();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)
            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var User = _userRepository
                    .GetAll()
                    .OrderByDescending(p => p.Id)
                    .ToList();

                var Useritem = (from user in User
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                where (us != null && us.DisplayName == "Installer" && user.TenantId != null)
                                group user by user into userGrouped
                                select userGrouped.Key);
                foreach (var User_List in Useritem)
                {
                    userlist.Add(User_List);
                    user_list = userlist.MapTo<List<AdminUserListDto>>();
                }
            }
            //}

            return new ListResultDto<AdminUserListDto>(user_list);
        }
    }
}
