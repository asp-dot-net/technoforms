﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.AdminInvitation.Dto;

namespace TechnoForms.AdminInvitation
{
    public interface IAdminInvitationAppService : IApplicationService
    {
        Task<PagedResultDto<AdminInvitationListDto>> GetAdminInvitation(GetInvitationInput input);
        Task ApproveAdminInvitation(EntityDto input);
        Task<GetAdminInvitationForReject> GetAdminInvitationForReject(EntityDto input);
        Task RejectAdminInvitation(GetAdminInvitationForReject input);
        Task<AdminInvitationDetailDto> GetAdminInvitationDetail(EntityDto input);

        ListResultDto<AdminInvitationStatusListDto> GetInvitationStatus();
        ListResultDto<AdminUserListDto> GetUsers();
        ListResultDto<AdminStateListDto> GetState();
        ListResultDto<AdminPostCodeListDto> GetPostCode();
        ListResultDto<AdminSuburbListDto> GetSuburb();
        ListResultDto<AdminPostalAddressTypeListDto> GetPostalAddressType();
        ListResultDto<AdminPostalDeliveryTypeListDto> GetPostalDeliveryType();
        ListResultDto<AdminUnitTypeListDto> GetUnitType();
        ListResultDto<AdminStreetTypeListDto> GetStreetType();
        ListResultDto<AdminStreetNameListDto> GetStreetName();
    }
}
