﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.CustomerInstallers;
using TechnoForms.Installers;

namespace TechnoForms.AdminInvitation.Dto
{
    [AutoMapFrom(typeof(CustomerInstaller))]
    public class AdminInvitationListDto : FullAuditedEntityDto
    {
        public int TenantId { get; set; }

        public int InstallerId { get; set; }

        public int Inst_Request_status { get; set; }

        public string Inst_RejectReason { get; set; }

        public int Request_status { get; set; }

        public string RejectReason { get; set; }

        public virtual string FullName { get; set; }

        public virtual string CustName { get; set; }

        public virtual string InstallerExpiry { get; set; }

        public virtual DateTime? InsAccreExDate { get; set; }

        //public List<InstallerListRoleDto> Installer { get; set; }

        //[AutoMapFrom(typeof(Installer))]
        //public class InstallerListRoleDto
        //{
        //    public int Id { get; set; }

        //    public virtual string FullName { get; set; }
        //}
    }
}
