﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.CustomerInstallers;
using TechnoForms.Installers;

namespace TechnoForms.AdminInvitation.Dto
{
    [AutoMap(typeof(CustomerInstaller))]
    public class GetAdminInvitationForReject : FullAuditedEntityDto
    {
        public virtual int InstallerId { get; set; }
        
        public virtual string FullName { get; set; }

        public virtual string RejectReason { get; set; }
    }
}
