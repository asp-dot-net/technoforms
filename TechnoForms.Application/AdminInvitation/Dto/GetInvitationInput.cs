﻿using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Dto;

namespace TechnoForms.AdminInvitation.Dto
{
    public class GetInvitationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int PageStatus { get; set; }

        public int? AdminStatus { get; set; }

        public int? InstallerStatus { get; set; }

        public string InstName { get; set; }

        public string CustName { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "fullName asc";
            }
            if (Sorting == "fullName desc")
            {
                Sorting = "InstallerId desc";
            }
            if (Sorting == "fullName asc")
            {
                Sorting = "InstallerId asc";
            }
            if (Sorting == "custName asc")
            {
                Sorting = "TenantId asc";
            }
            if (Sorting == "custName desc")
            {
                Sorting = "TenantId desc";
            }
            
        }
    }
}
