﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.UnitTypes;

namespace TechnoForms.AdminInvitation.Dto
{
    [AutoMapFrom(typeof(UnitType))]
    public class AdminUnitTypeListDto : FullAuditedEntityDto
    {
        public string UnitTypeName { get; set; }
    }
}
