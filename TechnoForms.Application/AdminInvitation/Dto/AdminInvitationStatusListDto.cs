﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.InvitationsStatus;

namespace TechnoForms.AdminInvitation.Dto
{
    [AutoMapFrom(typeof(InvitationStatus))]
    public class AdminInvitationStatusListDto
    {
        public virtual int StatusCode { get; set; }

        public virtual string StatusName { get; set; }
    }
}
