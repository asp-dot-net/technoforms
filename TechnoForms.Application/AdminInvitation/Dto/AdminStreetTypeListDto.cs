﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.StreetTypes;

namespace TechnoForms.AdminInvitation.Dto
{
    [AutoMapFrom(typeof(StreetType))]
    public class AdminStreetTypeListDto : FullAuditedEntityDto
    {
        public string StreetTypeName { get; set; }
    }
}
