﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Messages.Dto;

namespace TechnoForms.Messages
{
    public interface IMessageAppService : IApplicationService
    {
        Task CreateMessage(CreateMessageInputDto input);
        Task<PagedResultDto<MessageListDto>> GetMessage(GetMessageInputDto input);
        Task DeleteMessage(EntityDto input);
    }
}
