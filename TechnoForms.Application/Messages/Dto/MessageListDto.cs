﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Messages.Dto
{
    [AutoMapFrom(typeof(TechnoForms.Messages.Message))]
    public class MessageListDto : FullAuditedEntityDto
    {
        public int job_id { get; set; }
        public string MessageValue { get; set; }
        public Guid JobGuid { get; set; }
        public string MessageSendBy { get; set; }
    }
}
