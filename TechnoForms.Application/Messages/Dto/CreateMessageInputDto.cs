﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Messages.Dto
{
    [AutoMapTo(typeof(TechnoForms.Messages.Message))]
    public class CreateMessageInputDto
    {
        public int job_id { get; set; }
        public string MessageValue { get; set; }
        public Guid JobGuid { get; set; }
    }
}
