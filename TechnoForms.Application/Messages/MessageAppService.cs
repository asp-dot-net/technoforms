﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.Jobs;
using TechnoForms.Messages;
using TechnoForms.Messages.Dto;
using Abp.Linq.Extensions;
using System.Linq.Dynamic;
using System.Data.Entity;
using TechnoForms.Authorization.Users;
using TechnoForms.Installers;
using TechnoForms.MultiTenancy;
using Abp.Domain.Uow;
using TechnoForms.Identity;
using TechnoForms.SmsSender;

namespace TechnoForms.Messages
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Jobs)]
    public class MessageAppService : TechnoFormsAppServiceBase, IMessageAppService
    {
        private readonly IRepository<Message> _messageRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<User, long> _userRepository;

        private readonly IRepository<Installer> _installerRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly ISmsSender _smsSender;

        public MessageAppService(IRepository<Message> messageRepository,
            IRepository<Job> jobRepository,
              IRepository<User, long> userRepository,
              IRepository<Installer> installerRepository,
              IRepository<Tenant> tenantRepository,
              IUnitOfWorkManager unitOfWorkManager,
              ISmsSender smsSender
            )
        {
            _messageRepository = messageRepository;
            _jobRepository = jobRepository;
            _userRepository = userRepository;
            _installerRepository = installerRepository;
            _tenantRepository = tenantRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _smsSender = smsSender;
        }

        [UnitOfWork]
        public async Task CreateMessage(CreateMessageInputDto input)
        {
            var JobDetail = _jobRepository.GetAll().Where(p => p.Id == input.job_id).FirstOrDefault();
            input.JobGuid = JobDetail.Guid;
            var InstallerName = "";
            var InstallerNumber = "";
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                InstallerName = _installerRepository.GetAll().Where(I => I.Id == JobDetail.CreateFor_Id).Select(I => I.FullName).FirstOrDefault();
                InstallerNumber = _installerRepository.GetAll().Where(I => I.Id == JobDetail.CreateFor_Id).Select(I => I.Phone).FirstOrDefault();
            }
            var TenantName = _tenantRepository.GetAll().Where(I => I.Id == JobDetail.TenantId).Select(I => I.FullName).FirstOrDefault();
            input.MessageValue = "Hello " + InstallerName + " " + input.MessageValue + " Regards, " + TenantName;

            #region Send Sms
            

            var error = "";
            if (InstallerNumber.Substring(0, 1) == "0")
            {
                InstallerNumber = InstallerNumber.Substring(1);
            }
            InstallerNumber = "+61" + InstallerNumber;
            string mobileno = InstallerNumber;
            string messagetext = input.MessageValue;

            if (mobileno != string.Empty && messagetext != string.Empty)
            {
                //TwilioSmsSenderAppService twilioSmsSenderAppService = new TwilioSmsSenderAppService();
                //string AccountSid = "ACacccbeef8290e36e26dc06282682d616";
                //string AuthToken = "b259e084b00045d28e6722e77268c93f";
                //var twilio = new Twilio.TwilioRestClient(AccountSid, AuthToken);
                //var Response = twilio.SendMessage("+61488854509", mobileno, messagetext);
                await _smsSender.SendAsync(mobileno, messagetext);
            }
            #endregion

            var message = input.MapTo<Message>();
            await _messageRepository.InsertAsync(message);
        }

        public async Task<PagedResultDto<MessageListDto>> GetMessage(GetMessageInputDto input)
        {
            var message = _messageRepository
                .GetAll().Where(p => p.job_id == input.jobId);
            //.WhereIf(!input.Filter.IsNullOrEmpty(),
            //    p => p.MessageValue.Contains(input.Filter) ||
            //         p.MessageValue.Contains(input.Filter));

            if (input.Sorting == "messageSendBy asc")
            {
                input.Sorting = "CreatorUserId asc";
            }
            if (input.Sorting == "messageSendBy desc")
            {
                input.Sorting = "CreatorUserId desc";
            }
            var resultCount = await message.CountAsync();
            var results = await message
                .AsNoTracking()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            var results_val = results.MapTo<List<MessageListDto>>();
            foreach (var item in results_val)
            {
                item.MessageSendBy = _userRepository.GetAll().Where(p => p.Id == item.CreatorUserId).Select(p => p.Name).FirstOrDefault();
            }

            return new PagedResultDto<MessageListDto>(resultCount, results_val);
        }

        public async Task DeleteMessage(EntityDto input)
        {
            await _messageRepository.DeleteAsync(input.Id);
        }
    }
}
