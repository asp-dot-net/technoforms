﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Linq.Extensions;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Energy;
using TechnoForms.EnergyDestributars.Dto;
using System.Data.Entity;
using Abp.Extensions;
using Abp.Runtime.Session;
using Abp.Authorization;
using TechnoForms.Authorization;

namespace TechnoForms.EnergyDestributars
{
	[AbpAuthorize(AppPermissions.Pages_Tenant_EnergyDestributars)]
	public class EnergyDestributarAppService : TechnoFormsApplicationModule, IEnergyDestributarAppService
	{
		private readonly IRepository<EnergyDestributar> _energyDestributarRepository;
		private readonly IAbpSession _session;

		public EnergyDestributarAppService(IRepository<EnergyDestributar> energyDestributarRepository
			, IAbpSession session)
		{
			_energyDestributarRepository = energyDestributarRepository;
			_session = session;
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_EnergyDestributars_Create)]
		public async Task CreateEnergyDestributar(CreateEnergyDestributarDto input)
		{
			var EnergyDestributars = input.MapTo<EnergyDestributar>();
			EnergyDestributars.TenantId = (int)_session.TenantId;
			await _energyDestributarRepository.InsertAsync(EnergyDestributars);
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_EnergyDestributars_Delete)]
		public async Task DeleteEnergyDestributar(EntityDto input)
		{
			await _energyDestributarRepository.DeleteAsync(input.Id);
		}

		public async Task<PagedResultDto<EnergyDestributarListDto>> GetEnergyDestributar(GetEnergyDestributarInput input)
		{
			var EnergyDestributars = _energyDestributarRepository
				.GetAll()
				.WhereIf(
					!input.Name.IsNullOrEmpty(),
					p => p.Name.ToLower().Contains(input.Name.ToLower())
				);

			var resultCount = await EnergyDestributars.CountAsync();
			var results = await EnergyDestributars
				.AsNoTracking()
				.OrderBy(input.Sorting)
				.PageBy(input)
				.ToListAsync();

			return new PagedResultDto<EnergyDestributarListDto>(resultCount, results.MapTo<List<EnergyDestributarListDto>>());
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_EnergyDestributars_Edit)]
		public async Task<EditEnergyDestributarDto> GetEnergyDestributarForEdit(EntityDto input)
		{
			var EnergyDestributars = (await _energyDestributarRepository.GetAsync(input.Id)).MapTo<EditEnergyDestributarDto>(); ;
			return EnergyDestributars;
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_EnergyDestributars_Edit)]
		public async Task UpdateEnergyDestributar(EditEnergyDestributarDto input)
		{
			var EnergyDestributars = await _energyDestributarRepository.GetAsync(input.Id);
			EnergyDestributars.Name = input.Name;
			EnergyDestributars.NSW = input.NSW;
			EnergyDestributars.SA = input.SA;
			EnergyDestributars.QLD = input.QLD;
			EnergyDestributars.VIC = input.VIC;
			EnergyDestributars.WA = input.WA;
			EnergyDestributars.ACT = input.ACT;
			EnergyDestributars.TAS = input.TAS;
			EnergyDestributars.NT = input.NT;
			EnergyDestributars.ElectDistABB = input.ElectDistABB;
			EnergyDestributars.ElecDistAppReq = input.ElecDistAppReq;
			EnergyDestributars.GreenBoatDistributor = input.GreenBoatDistributor;
			await _energyDestributarRepository.UpdateAsync(EnergyDestributars);
		}
	}
}
