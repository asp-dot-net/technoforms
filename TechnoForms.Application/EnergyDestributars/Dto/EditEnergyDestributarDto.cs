﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Energy;

namespace TechnoForms.EnergyDestributars.Dto
{
	[AutoMap(typeof(EnergyDestributar))]
	public class EditEnergyDestributarDto : EntityDto
	{
		public virtual string Name { get; set; }

		public virtual bool NSW { get; set; }

		public virtual bool SA { get; set; }

		public virtual bool QLD { get; set; }

		public virtual bool VIC { get; set; }

		public virtual bool WA { get; set; }

		public virtual bool ACT { get; set; }

		public virtual bool TAS { get; set; }

		public virtual bool NT { get; set; }

		public virtual string ElectDistABB { get; set; }

		public virtual bool ElecDistAppReq { get; set; }

		public virtual int? GreenBoatDistributor { get; set; }
	}
}
