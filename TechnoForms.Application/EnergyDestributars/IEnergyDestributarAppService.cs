﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.EnergyDestributars.Dto;

namespace TechnoForms.EnergyDestributars
{
	public interface IEnergyDestributarAppService : IApplicationService
	{
		Task<PagedResultDto<EnergyDestributarListDto>> GetEnergyDestributar(GetEnergyDestributarInput input);
		Task CreateEnergyDestributar(CreateEnergyDestributarDto input);
		Task DeleteEnergyDestributar(EntityDto input);
		Task UpdateEnergyDestributar(EditEnergyDestributarDto input);
		Task<EditEnergyDestributarDto> GetEnergyDestributarForEdit(EntityDto input);
	}
}
