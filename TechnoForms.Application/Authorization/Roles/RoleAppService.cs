﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Linq.Extensions;
using Abp.Extensions;
using TechnoForms.Authorization.Permissions;
using TechnoForms.Authorization.Permissions.Dto;
using TechnoForms.Authorization.Roles.Dto;
using Abp.Domain.Uow;
using TechnoForms.MultiTenancy;
using Abp.Domain.Repositories;

namespace TechnoForms.Authorization.Roles
{
    /// <summary>
    /// Application service that is used by 'role management' page.
    /// </summary>
    [AbpAuthorize(AppPermissions.Pages_Administration_Roles)]
    public class RoleAppService : TechnoFormsAppServiceBase, IRoleAppService
    {
        private readonly RoleManager _roleManager;

        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Tenant> _tenantRepository;

        public RoleAppService(RoleManager roleManager, IUnitOfWorkManager unitOfWorkManager, IRepository<Tenant> tenantRepository)
        {
            _roleManager = roleManager;

            _unitOfWorkManager = unitOfWorkManager;
            _tenantRepository = tenantRepository;
        }

        [UnitOfWork]
        public async Task<ListResultDto<RoleListDto>> GetRoles(GetRolesInput input)
        {
            //var userlist = new List<Role>();
            //var user_list = new List<RoleListDto>();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();

            //for (int i = 0; i < tenant.Count; i++)
            //{
            //    using (_unitOfWorkManager.Current.SetTenantId(null))
            //    {
            var roles = await _roleManager
                .Roles
                .WhereIf(!input.Permission.IsNullOrWhiteSpace(),
                 r => r.Permissions.Any(rp => rp.Name == input.Permission && rp.IsGranted))
                .ToListAsync();

            //        foreach (var User_List in roles)
            //        {
            //            userlist.Add(User_List);
            //            user_list = userlist.MapTo<List<RoleListDto>>();
            //        }
            //    }
            //}

            //return new ListResultDto<RoleListDto>(user_list);
            return new ListResultDto<RoleListDto>(roles.MapTo<List<RoleListDto>>());

        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Roles_Create, AppPermissions.Pages_Administration_Roles_Edit)]
        public async Task<GetRoleForEditOutput> GetRoleForEdit(NullableIdDto input)
        {
            var permissions = PermissionManager.GetAllPermissions();
            var grantedPermissions = new Permission[0];
            RoleEditDto roleEditDto;

            if (input.Id.HasValue) //Editing existing role?
            {
                var role = await _roleManager.GetRoleByIdAsync(input.Id.Value);
                grantedPermissions = (await _roleManager.GetGrantedPermissionsAsync(role)).ToArray();
                roleEditDto = role.MapTo<RoleEditDto>();
            }
            else
            {
                roleEditDto = new RoleEditDto();
            }

            return new GetRoleForEditOutput
            {
                Role = roleEditDto,
                Permissions = permissions.MapTo<List<FlatPermissionDto>>().OrderBy(p => p.DisplayName).ToList(),
                GrantedPermissionNames = grantedPermissions.Select(p => p.Name).ToList()
            };
        }

        public async Task CreateOrUpdateRole(CreateOrUpdateRoleInput input)
        {
            if (input.Role.Id.HasValue)
            {
                await UpdateRoleAsync(input);
            }
            else
            {
                await CreateRoleAsync(input);
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Roles_Delete)]
        public async Task DeleteRole(EntityDto input)
        {
            var role = await _roleManager.GetRoleByIdAsync(input.Id);
            CheckErrors(await _roleManager.DeleteAsync(role));
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Roles_Edit)]
        protected virtual async Task UpdateRoleAsync(CreateOrUpdateRoleInput input)
        {
            Debug.Assert(input.Role.Id != null, "input.Role.Id should be set.");

            var role = await _roleManager.GetRoleByIdAsync(input.Role.Id.Value);
            role.DisplayName = input.Role.DisplayName;
            role.IsDefault = input.Role.IsDefault;

            await UpdateGrantedPermissionsAsync(role, input.GrantedPermissionNames);
        }

        [AbpAuthorize(AppPermissions.Pages_Administration_Roles_Create)]
        protected virtual async Task CreateRoleAsync(CreateOrUpdateRoleInput input)
        {
            var role = new Role(AbpSession.TenantId, input.Role.DisplayName) { IsDefault = input.Role.IsDefault };
            CheckErrors(await _roleManager.CreateAsync(role));
            await CurrentUnitOfWork.SaveChangesAsync(); //It's done to get Id of the role.
            await UpdateGrantedPermissionsAsync(role, input.GrantedPermissionNames);
        }

        private async Task UpdateGrantedPermissionsAsync(Role role, List<string> grantedPermissionNames)
        {
            var grantedPermissions = PermissionManager.GetPermissionsFromNamesByValidating(grantedPermissionNames);
            await _roleManager.SetGrantedPermissionsAsync(role, grantedPermissions);
        }
    }
}
