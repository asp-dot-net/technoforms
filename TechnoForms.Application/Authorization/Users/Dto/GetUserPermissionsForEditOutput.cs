﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using TechnoForms.Authorization.Permissions.Dto;

namespace TechnoForms.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}