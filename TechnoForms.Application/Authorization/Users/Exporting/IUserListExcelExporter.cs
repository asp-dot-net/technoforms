using System.Collections.Generic;
using TechnoForms.Authorization.Users.Dto;
using TechnoForms.Dto;

namespace TechnoForms.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}