﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TechnoForms.Authorization.Permissions.Dto;

namespace TechnoForms.Authorization.Permissions
{
    public interface IPermissionAppService : IApplicationService
    {
        ListResultDto<FlatPermissionWithLevelDto> GetAllPermissions();
    }
}
