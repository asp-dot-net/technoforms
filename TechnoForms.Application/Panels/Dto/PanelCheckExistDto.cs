﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Panels.Dto
{
    public class PanelCheckExistDto : EntityDto
    {
        //public string LicenceOrCertiHolder { get; set; }
        public string ModelNo { get; set; }
    }
}
