﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Panels.Dto
{
    [AutoMapFrom(typeof(Panel))]
    public class PanelListDto : EntityDto
    {
        public virtual string LicenceOrCertiHolder { get; set; }

        public virtual string ModelNo { get; set; }

        public virtual DateTime CECApprovedDate { get; set; }

        public virtual DateTime ExpiryDate { get; set; }

        public virtual decimal Size { get; set; }

        public virtual string FireTested { get; set; }
    }
}
