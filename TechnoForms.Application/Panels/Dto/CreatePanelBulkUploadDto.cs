﻿using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Panels.Dto
{
	[AutoMapTo(typeof(PanelBulkUpload))]
	public class CreatePanelBulkUploadDto : FullAuditedEntity
	{
		[MaxLength(PanelBulkUpload.MaxPanelFileName)]
		public virtual string PanelFileName { get; set; }
	}
}
