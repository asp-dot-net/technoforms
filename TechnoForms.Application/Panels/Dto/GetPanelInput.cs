﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Dto;

namespace TechnoForms.Panels.Dto
{
    public class GetPanelInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public virtual string LicenceOrCertiHolder { get; set; }

        public virtual string ModelNo { get; set; }

        public virtual DateTime? ExpiryDate { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "LicenceOrCertiHolder";
            }
        }
    }
}
