﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Panels.Dto
{
    public class PanelValidationMessage
    {
        public string Message { get; set; }
        public bool Status { get; set; }
        public int id { get; set; }
    }
}
