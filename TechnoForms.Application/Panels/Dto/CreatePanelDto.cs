﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Panels.Dto
{
    [AutoMapTo(typeof(Panel))]
    public class CreatePanelDto : EntityDto
    {
        [Required]
        [MaxLength(Panel.MaxLicenceOrCertiHolder)]
        public virtual string LicenceOrCertiHolder { get; set; }

        [Required]
        [MaxLength(Panel.MaxModelNo)]
        public virtual string ModelNo { get; set; }

        [Required]
        public virtual DateTime CECApprovedDate { get; set; }

        [Required]
        public virtual DateTime ExpiryDate { get; set; }

        [Required]
        public virtual decimal Size { get; set; }

		//[MaxLength(Panel.MaxPanelLogo)]
		public virtual string PanelLogo { get; set; }

		[Required]
        [MaxLength(Panel.MaxFireTested)]
        public virtual string FireTested { get; set; }

        public string ResponsibleSupplier { get; set; }
        public string ResponsibleSupplierABN { get; set; }

        public string ResponsibleSupplier2 { get; set; }
        public string ResponsibleSupplierABN2 { get; set; }

        public string ResponsibleSupplier3 { get; set; }
        public string ResponsibleSupplierABN3 { get; set; }
    }
}
