﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Panels.Dto;

namespace TechnoForms.Panels
{
    public interface IPanelAppService : IApplicationService
    {
        Task<PagedResultDto<PanelListDto>> getPanel(GetPanelInput input);
        Task CreatePanel(CreatePanelDto input);
		Task CreatePanelBulkUpload(CreatePanelBulkUploadDto input);
		Task DeletePanel(EntityDto input);
        Task UpdatePanel(PanelEditDto input);
        Task UpdatePanelSize(CreatePanelDto input);
        Task<PanelEditDto> GetPanelForEdit(EntityDto input);
        Task<PanelBulkUploadDto> getPanelBulkUpload();
        PanelValidationMessage CheckPanelExists(PanelCheckExistDto input);
    }
}
