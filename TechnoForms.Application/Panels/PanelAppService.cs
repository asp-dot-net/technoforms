﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.Panels.Dto;
using System.Data.Entity;using Abp.Linq.Extensions;using System.Linq.Dynamic;
using System.IO;
using Abp.IO;
using TechnoForms.Authorization.Users;

namespace TechnoForms.Panels
{
	[AbpAuthorize(AppPermissions.Pages_Masters_Panel)]
	public class PanelAppService : TechnoFormsApplicationModule, IPanelAppService
	{
		private readonly IRepository<Panel> _panelRepository;
		private readonly IRepository<PanelBulkUpload> _panelBulkUploadRepository;
		private readonly IAppFolders _appFolders;
		private readonly IRepository<User, long> _userRepository;

		public PanelAppService(IRepository<Panel> panelRepository,
			IRepository<PanelBulkUpload> panelBulkUploadRepository,
			IAppFolders appFolders,
			IRepository<User, long> userRepository)
		{
			_panelRepository = panelRepository;
			_panelBulkUploadRepository = panelBulkUploadRepository;
			_appFolders = appFolders;
			_userRepository = userRepository;
		}

		[AbpAuthorize(AppPermissions.Pages_Masters_Panel_Create)]
		public async Task CreatePanel(CreatePanelDto input)
		{
			try
			{

				var Exist = _panelRepository.GetAll().Where(p => p.ModelNo == input.ModelNo).Any();
				if (Exist)
				{
					var panel = _panelRepository.GetAll().Where(p => p.ModelNo == input.ModelNo).FirstOrDefault();

					if (panel.PanelLogo != null)
					{
						if (input.PanelLogo != panel.PanelLogo)
						{
							var tempLogoPath = Path.Combine(_appFolders.TempFileDownloadFolder, panel.PanelLogo);
							FileHelper.DeleteIfExists(tempLogoPath);
						}
					}

					//var panel = await _panelRepository.GetAsync(input.Id);
					panel.LicenceOrCertiHolder = input.LicenceOrCertiHolder;
					panel.ModelNo = input.ModelNo;
					panel.CECApprovedDate = input.CECApprovedDate;
					panel.ExpiryDate = input.ExpiryDate;
					panel.Size = input.Size;
					panel.FireTested = input.FireTested;
					panel.PanelLogo = input.PanelLogo;
					panel.ResponsibleSupplier = input.ResponsibleSupplier;
					panel.ResponsibleSupplier2 = input.ResponsibleSupplier2;
					panel.ResponsibleSupplier3 = input.ResponsibleSupplier3;
					panel.ResponsibleSupplierABN = input.ResponsibleSupplierABN;
					panel.ResponsibleSupplierABN2 = input.ResponsibleSupplierABN2;
					panel.ResponsibleSupplierABN3 = input.ResponsibleSupplierABN3;
					await _panelRepository.UpdateAsync(panel);

					if (panel.PanelLogo != null)
					{
						var ModelNoList = _panelRepository.GetAll().Where(e => e.LicenceOrCertiHolder == panel.LicenceOrCertiHolder).Select(e => e.ModelNo).ToList();
						foreach (var item in ModelNoList)
						{
							var LogoUpdate = _panelRepository.GetAll().Where(e => e.ModelNo == item).FirstOrDefault();
							LogoUpdate.PanelLogo = panel.PanelLogo;
							await _panelRepository.UpdateAsync(LogoUpdate);
						}
					}
				}
				else
				{
					var Panel = input.MapTo<Panel>();
					if (Panel.PanelLogo == null)
					{
						var InverterLogo = _panelRepository.GetAll().Where(e => e.LicenceOrCertiHolder == Panel.LicenceOrCertiHolder && e.PanelLogo != null).Select(e => e.PanelLogo).FirstOrDefault();
						Panel.PanelLogo = InverterLogo;
					}
					await _panelRepository.InsertAsync(Panel);

					if (Panel.PanelLogo != null)
					{
						var ModelNoList = _panelRepository.GetAll().Where(e => e.LicenceOrCertiHolder == Panel.LicenceOrCertiHolder).Select(e => e.ModelNo).ToList();
						foreach (var item in ModelNoList)
						{
							var LogoUpdate = _panelRepository.GetAll().Where(e => e.ModelNo == item).FirstOrDefault();
							LogoUpdate.PanelLogo = Panel.PanelLogo;
							await _panelRepository.UpdateAsync(LogoUpdate);
						}
					}
				}

			}
			catch (Exception e)
			{

			}
		}

		[AbpAuthorize(AppPermissions.Pages_Masters_Panel_Create)]
		public async Task CreatePanelBulkUpload(CreatePanelBulkUploadDto input)
		{
			var PanelBulkUpload = input.MapTo<PanelBulkUpload>();
			await _panelBulkUploadRepository.InsertAsync(PanelBulkUpload);
		}

		[AbpAuthorize(AppPermissions.Pages_Masters_Panel_Delete)]
		public async Task DeletePanel(EntityDto input)
		{
			var panel = _panelRepository.GetAll().Where(P => P.Id == input.Id).FirstOrDefault();
			if (panel.PanelLogo != null)
			{
				var tempLogoPath = Path.Combine(_appFolders.TempFileDownloadFolder, panel.PanelLogo);
				FileHelper.DeleteIfExists(tempLogoPath);
			}
			await _panelRepository.DeleteAsync(input.Id);
		}

		public async Task<PagedResultDto<PanelListDto>> getPanel(GetPanelInput input)
		{
			var panel = _panelRepository
				.GetAll()
				.WhereIf(
					!input.LicenceOrCertiHolder.IsNullOrEmpty(),
					p => p.LicenceOrCertiHolder.ToLower().Contains(input.LicenceOrCertiHolder.ToLower())
				)
				.WhereIf(
					!input.ModelNo.IsNullOrEmpty(),
					p => p.ModelNo.ToLower().Contains(input.ModelNo.ToLower())
				)
				.WhereIf(
					!Convert.ToString(input.ExpiryDate).IsNullOrEmpty(),
					p => p.ExpiryDate <= input.ExpiryDate
				);

			var resultCount = await panel.CountAsync();
			var results = await panel
				.AsNoTracking()
				.OrderBy(input.Sorting)
				.PageBy(input)
				.ToListAsync();

			return new PagedResultDto<PanelListDto>(resultCount, results.MapTo<List<PanelListDto>>());
		}

		[AbpAuthorize(AppPermissions.Pages_Masters_Panel_Edit)]
		public async Task<PanelEditDto> GetPanelForEdit(EntityDto input)
		{
			var panel = (await _panelRepository.GetAsync(input.Id)).MapTo<PanelEditDto>();
			return panel;
		}

		[AbpAuthorize(AppPermissions.Pages_Masters_Panel_Edit)]
		public async Task UpdatePanel(PanelEditDto input)
		{
			try
			{
				var panel = await _panelRepository.GetAsync(input.Id);

				if (panel.PanelLogo != null)
				{
					if (input.PanelLogo != panel.PanelLogo)
					{
						var tempLogoPath = Path.Combine(_appFolders.TempFileDownloadFolder, panel.PanelLogo);
						FileHelper.DeleteIfExists(tempLogoPath);
					}
				}

				panel.LicenceOrCertiHolder = input.LicenceOrCertiHolder;
				panel.ModelNo = input.ModelNo;
				panel.CECApprovedDate = input.CECApprovedDate;
				panel.ExpiryDate = input.ExpiryDate;
				panel.Size = input.Size;
				panel.FireTested = input.FireTested;
				panel.PanelLogo = input.PanelLogo;
				panel.ResponsibleSupplier = input.ResponsibleSupplier;
				panel.ResponsibleSupplierABN = input.ResponsibleSupplierABN;
				panel.ResponsibleSupplier2 = input.ResponsibleSupplier2;
				panel.ResponsibleSupplierABN2 = input.ResponsibleSupplierABN2;
				panel.ResponsibleSupplier3 = input.ResponsibleSupplier3;
				panel.ResponsibleSupplierABN3 = input.ResponsibleSupplierABN3;
				await _panelRepository.UpdateAsync(panel);

				if (panel.PanelLogo != null)
				{
					var ModelNoList = _panelRepository.GetAll().Where(e => e.LicenceOrCertiHolder == panel.LicenceOrCertiHolder).Select(e => e.ModelNo).ToList();
					foreach (var item in ModelNoList)
					{
						var LogoUpdate = _panelRepository.GetAll().Where(e => e.ModelNo == item).FirstOrDefault();
						LogoUpdate.PanelLogo = panel.PanelLogo;
						await _panelRepository.UpdateAsync(LogoUpdate);
					}
				}
			}
			catch (Exception e) { }


		}

		public async Task<PanelBulkUploadDto> getPanelBulkUpload()
		{
			var Panel = new PanelBulkUploadDto();
			var Paneldata = _panelBulkUploadRepository.GetAll().OrderByDescending(P => P.Id).FirstOrDefault();

			Panel = Paneldata.MapTo<PanelBulkUploadDto>();

			if (Paneldata != null)
				Panel.UploaderName = _userRepository.GetAll().Where(U => U.Id == Panel.CreatorUserId).Select(U => U.Name).FirstOrDefault() + " " + _userRepository.GetAll().Where(U => U.Id == Panel.CreatorUserId).Select(U => U.UserName).FirstOrDefault();

			return Panel;
		}

		public PanelValidationMessage CheckPanelExists(PanelCheckExistDto input)
		{
			var status_val = new PanelValidationMessage();
			status_val.Status = true;
			try
			{
				var Panel = _panelRepository
			 .GetAll()
			 .Where(p => p.ModelNo == input.ModelNo).ToList();

				if (!string.IsNullOrEmpty(input.Id.ToString()) && input.Id != 0)
				{
					Panel = Panel.Where(p => p.Id != input.Id).ToList();
				}
				if (Panel.Count > 0)
				{
					status_val.Status = false;
					status_val.id = Panel.Select(p => p.Id).FirstOrDefault();
					status_val.Message = "Record with this name already exists.";
				}
			}
			catch (Exception e) { }
			return status_val;
		}

		public async Task UpdatePanelSize(CreatePanelDto input)
		{
			var Exist = _panelRepository.GetAll().Where(p => p.ModelNo == input.ModelNo).FirstOrDefault();

			if (Exist != null)
			{
				Exist.Size = input.Size;
				await _panelRepository.UpdateAsync(Exist);
			}
		}
	}
}
