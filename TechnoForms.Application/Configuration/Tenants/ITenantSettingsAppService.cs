﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TechnoForms.Configuration.Tenants.Dto;

namespace TechnoForms.Configuration.Tenants
{
    public interface ITenantSettingsAppService : IApplicationService
    {
        Task<TenantSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(TenantSettingsEditDto input);

        Task ClearLogo();

        Task ClearCustomCss();
    }
}
