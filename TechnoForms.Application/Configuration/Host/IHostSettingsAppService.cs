﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TechnoForms.Configuration.Host.Dto;

namespace TechnoForms.Configuration.Host
{
    public interface IHostSettingsAppService : IApplicationService
    {
        Task<HostSettingsEditDto> GetAllSettings();

        Task UpdateAllSettings(HostSettingsEditDto input);

        Task SendTestEmail(SendTestEmailInput input);
    }
}
