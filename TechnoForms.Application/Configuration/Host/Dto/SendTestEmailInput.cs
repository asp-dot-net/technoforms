﻿using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using TechnoForms.Authorization.Users;

namespace TechnoForms.Configuration.Host.Dto
{
    public class SendTestEmailInput
    {
        [Required]
        [MaxLength(User.MaxEmailAddressLength)]
        public string EmailAddress { get; set; }
    }
}