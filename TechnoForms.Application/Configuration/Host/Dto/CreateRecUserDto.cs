﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.RecUsers;

namespace TechnoForms.Configuration.Host.Dto
{
    [AutoMap(typeof(RecUser))]
    public class CreateRecUserDto : EntityDto
    {
        [Required]
        [MaxLength(RecUser.MaxRecUserNameLength)]
        public string RecUserName { get; set; }

        [Required]
        [MaxLength(RecUser.MaxRecPasswprdLength)]
        public virtual string RecPasswprd { get; set; }

        public int TenantId { get; set; }
    }
}
