﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using Abp;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Extensions;
using Abp.Linq.Extensions;
using Abp.Runtime.Security;
using TechnoForms.Authorization;
using TechnoForms.Authorization.Users;
using TechnoForms.Editions.Dto;
using TechnoForms.MultiTenancy.Dto;
using TechnoForms.PostCodes;
using TechnoForms.PostCodes.Dto;
using TechnoForms.States;
using TechnoForms.StreetName;
using TechnoForms.StreetTypes;
using TechnoForms.UnitTypes;

namespace TechnoForms.MultiTenancy
{
    [AbpAuthorize(AppPermissions.Pages_Tenants)]
    public class TenantAppService : TechnoFormsAppServiceBase, ITenantAppService
    {
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IRepository<State> _stateRepository;
		private readonly IRepository<PostCode> _postCodeRepository;
		private readonly IRepository<UnitType> _unitTypeRepository;
		private readonly IRepository<StreetType> _streetTypeRepository;
		private readonly IRepository<StreetNames> _streetNameRepository;

		public TenantAppService(IRepository<State> StateRepository,
			IRepository<PostCode> PostCodeRepository,
			IRepository<UnitType> UnitTypeRepository,
			IRepository<StreetType> StreetTypeRepository,
			IRepository<StreetNames> StreetNameRepository,
			IUnitOfWorkManager unitOfWorkManager)
		{
			_stateRepository = StateRepository;
			_postCodeRepository = PostCodeRepository;
			_unitTypeRepository = UnitTypeRepository;
			_streetTypeRepository = StreetTypeRepository;
			_streetNameRepository = StreetNameRepository;
			_unitOfWorkManager = unitOfWorkManager;
		}

		public async Task<PagedResultDto<TenantListDto>> GetTenants(GetTenantsInput input)
        {
            var query = TenantManager.Tenants
                .Include(t => t.Edition)
                .WhereIf(
                    !input.Filter.IsNullOrWhiteSpace(),
                    t =>
                        t.Name.Contains(input.Filter) ||
                        t.TenancyName.Contains(input.Filter)
                );

            var tenantCount = await query.CountAsync();
            var tenants = await query.OrderBy(input.Sorting).PageBy(input).ToListAsync();

            return new PagedResultDto<TenantListDto>(
                tenantCount,
                tenants.MapTo<List<TenantListDto>>()
                );
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_Create)]
        [UnitOfWork(IsDisabled = true)]
        public async Task CreateTenant(CreateTenantInput input)
        {
            await TenantManager.CreateWithAdminUserAsync(input.TenancyName,
                input.Name,
                input.AdminPassword,
                input.AdminEmailAddress,
                input.ConnectionString,
                input.IsActive,
                input.EditionId,
                input.ShouldChangePasswordOnNextLogin,
                input.SendActivationEmail,
                input.FullName,
                input.CompanyName,
                input.ABN,
                input.FAX,
                input.Phone,
                input.Mobile,
                input.Email,
                input.STCCharges,
                input.RetailerName,
                input.RetailerABN,
				input.StreetAddress,
				input.UnitType,
				input.UnitNo,
				input.StreetNumber,
				input.StreetName,
				input.StreetType,
				input.Suburb,
				input.State,
				input.PostCode);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_Edit)]
        public async Task<TenantEditDto> GetTenantForEdit(EntityDto input)
        {
            var tenantEditDto = (await TenantManager.GetByIdAsync(input.Id)).MapTo<TenantEditDto>();
            tenantEditDto.ConnectionString = SimpleStringCipher.Instance.Decrypt(tenantEditDto.ConnectionString);
            return tenantEditDto;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_Edit)]
        public async Task UpdateTenant(TenantEditDto input)
        {
            input.ConnectionString = SimpleStringCipher.Instance.Encrypt(input.ConnectionString);
            var tenant = await TenantManager.GetByIdAsync(input.Id);
            input.MapTo(tenant);
            await TenantManager.UpdateAsync(tenant);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_Delete)]
        public async Task DeleteTenant(EntityDto input)
        {
            var tenant = await TenantManager.GetByIdAsync(input.Id);
            await TenantManager.DeleteAsync(tenant);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_ChangeFeatures)]
        public async Task<GetTenantFeaturesForEditOutput> GetTenantFeaturesForEdit(EntityDto input)
        {
            var features = FeatureManager.GetAll();
            var featureValues = await TenantManager.GetFeatureValuesAsync(input.Id);

            return new GetTenantFeaturesForEditOutput
            {
                Features = features.MapTo<List<FlatFeatureDto>>().OrderBy(f => f.DisplayName).ToList(),
                FeatureValues = featureValues.Select(fv => new NameValueDto(fv)).ToList()
            };
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_ChangeFeatures)]
        public async Task UpdateTenantFeatures(UpdateTenantFeaturesInput input)
        {
            await TenantManager.SetFeatureValuesAsync(input.Id, input.FeatureValues.Select(fv => new NameValue(fv.Name, fv.Value)).ToArray());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenants_ChangeFeatures)]
        public async Task ResetTenantSpecificFeatures(EntityDto input)
        {
            await TenantManager.ResetAllFeaturesAsync(input.Id);
        }

        public async Task UnlockTenantAdmin(EntityDto input)
        {
            using (CurrentUnitOfWork.SetTenantId(input.Id))
            {
                var tenantAdmin = await UserManager.FindByNameAsync(User.AdminUserName);
                if (tenantAdmin != null)
                {
                    tenantAdmin.Unlock();
                }
            }
        }

        [UnitOfWork]
        //public ListResultDto<JobStreetNamesListDto> GetStreetNames()
        public List<typeAhead> GetStreetNames(TypeAheadInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                if (input.Text != null && input.Text != "")
                {
                    var streetName = (from data in _streetNameRepository
                .GetAll()
                .Where(e => e.StreetName.Contains(input.Text))
                .Take(20)
                                      select new typeAhead
                                      {
                                          id = data.Id.ToString(),
                                          name = data.StreetName
                                      }).ToList();

                    return streetName; // new ListResultDto<JobStreetNamesListDto>(streetName.MapTo<List<JobStreetNamesListDto>>());
                }
                else
                {
                    var streetName = (from data in _streetNameRepository
                .GetAll()
                                      select new typeAhead
                                      {
                                          id = data.Id.ToString(),
                                          name = data.StreetName
                                      }).ToList();

                    return streetName;
                }
            }

        }

        [UnitOfWork]
        //public ListResultDto<JobStreetTypeListDto> GetStreetType()
        public List<typeAhead> GetStreetType(TypeAheadInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                if (input.Text != null && input.Text != "")
                {
                    var streetType = (from data in _streetTypeRepository
                .GetAll()
                .Where(e => e.StreetTypeName.Contains(input.Text))
                .Take(20)
                                      select new typeAhead
                                      {
                                          id = data.Id.ToString(),
                                          name = data.StreetTypeName
                                      }).ToList();

                    return streetType;
                }
                else
                {
                    var streetType = (from data in _streetTypeRepository
                .GetAll()
                                      select new typeAhead
                                      {
                                          id = data.Id.ToString(),
                                          name = data.StreetTypeName
                                      }).ToList();

                    return streetType;
                }
            }
        }

        [UnitOfWork]
        //public ListResultDto<JobUnitTypeListDto> GetUnitType()
        public List<typeAhead> GetUnitType(TypeAheadInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                if (input.Text != null && input.Text != "")
                {
                    var unitType = (from data in _unitTypeRepository
                .GetAll()
                .Where(e => e.UnitTypeName.Contains(input.Text))
                .Take(20)
                                    select new typeAhead
                                    {
                                        id = data.Id.ToString(),
                                        name = data.UnitTypeName
                                    }).ToList();

                    return unitType;
                }
                else
                {
                    var unitType = (from data in _unitTypeRepository
                .GetAll()
                                    select new typeAhead
                                    {
                                        id = data.Id.ToString(),
                                        name = data.UnitTypeName
                                    }).ToList();

                    return unitType;
                }
            }
        }

        [UnitOfWork]
        //public ListResultDto<PostCodesListDto> GetAllPostCodes(string postCodeInput)
        public List<typeAhead> GetAllPostCodes(TypeAheadInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                if (input.Text != null && input.Text != "")
                {
                    var postcodes = _postCodeRepository
                .GetAll()
                .Where(e => e.Suburb.Contains(input.Text))
                .Take(20)
                .ToList();

                    var suburblist = (from Inst in postcodes
                                      join In in _stateRepository.GetAll() on Inst.StateId equals In.Id into InJoin
                                      from In in InJoin.DefaultIfEmpty()
                                      select new typeAhead
                                      {
                                          id = Inst.Id.ToString(),
                                          name = Inst.Suburb + " | " + In.StateName + " | " + Inst.Postcode
                                      }).ToList();

                    return suburblist;
                }
                else
                {
                    var postcodes = _postCodeRepository
                .GetAll()
                .ToList();

                    var suburblist = (from Inst in postcodes
                                      join In in _stateRepository.GetAll() on Inst.StateId equals In.Id into InJoin
                                      from In in InJoin.DefaultIfEmpty()
                                      select new typeAhead
                                      {
                                          id = Inst.Id.ToString(),
                                          name = Inst.Suburb + " | " + In.StateName + " | " + Inst.Postcode
                                      }).ToList();

                    return suburblist;
                    //return null;
                }
            }
        }
        [UnitOfWork]
		public ListResultDto<TenantPostCodesListDto> GetPostCodesListBySuburb(EntityDto Input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var ModelNo = _postCodeRepository
				.GetAll()
				.Where(p => p.Id == Input.Id).ToList();

				var PostCodes = ModelNo.MapTo<List<TenantPostCodesListDto>>();
				foreach (var item in PostCodes)
				{
					item.stateName = (from datastate in _stateRepository.GetAll() where datastate.Id == item.StateId select datastate.StateName).FirstOrDefault();
				}
				return new ListResultDto<TenantPostCodesListDto>(PostCodes);
			}
		}
		[UnitOfWork]
		public ListResultDto<TenantSuburbListDto> AutoCompleteSuburb(string element)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var suburb = _postCodeRepository
			   .GetAll()
			   .Where(P => P.Suburb.ToLower().Contains(element.ToLower()))
			   .ToList();

				var postcodelist = suburb.MapTo<List<TenantSuburbListDto>>();
				foreach (var item in postcodelist)
				{
					var stateName = _stateRepository.GetAll().Where(p => p.Id == item.StateId).Select(p => p.StateName).FirstOrDefault();
					item.stateName = stateName;
					item.SuburbstatePostcodes = item.Suburb + " | " + stateName + " | " + item.Postcode;
				}
				return new ListResultDto<TenantSuburbListDto>(postcodelist);
			}
		}
	}
}