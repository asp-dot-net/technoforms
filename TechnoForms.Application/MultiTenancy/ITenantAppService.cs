﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TechnoForms.MultiTenancy.Dto;
using TechnoForms.PostCodes.Dto;

namespace TechnoForms.MultiTenancy
{
    public interface ITenantAppService : IApplicationService
    {
        Task<PagedResultDto<TenantListDto>> GetTenants(GetTenantsInput input);

        Task CreateTenant(CreateTenantInput input);

        Task<TenantEditDto> GetTenantForEdit(EntityDto input);

        Task UpdateTenant(TenantEditDto input);

        Task DeleteTenant(EntityDto input);

        Task<GetTenantFeaturesForEditOutput> GetTenantFeaturesForEdit(EntityDto input);

        Task UpdateTenantFeatures(UpdateTenantFeaturesInput input);

        Task ResetTenantSpecificFeatures(EntityDto input);

        Task UnlockTenantAdmin(EntityDto input);

        List<typeAhead> GetStreetNames(TypeAheadInput input);
        List<typeAhead> GetStreetType(TypeAheadInput input);
        List<typeAhead> GetUnitType(TypeAheadInput input);
        List<typeAhead> GetAllPostCodes(TypeAheadInput input);

        ListResultDto<TenantSuburbListDto> AutoCompleteSuburb(string element);

		ListResultDto<TenantPostCodesListDto> GetPostCodesListBySuburb(EntityDto Input);
	}
}
