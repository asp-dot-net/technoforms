﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.MultiTenancy.Dto
{
	public class GetTenantInput
	{
		public string Filter { get; set; }
	}
}
