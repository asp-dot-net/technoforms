using System.ComponentModel.DataAnnotations;
using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.MultiTenancy;
using TechnoForms.Authorization.Users;

namespace TechnoForms.MultiTenancy.Dto
{
    public class CreateTenantInput
    {
        [Required]
        [StringLength(AbpTenantBase.MaxTenancyNameLength)]
        [RegularExpression(Tenant.TenancyNameRegex)]
        public string TenancyName { get; set; }

        [Required]
        [StringLength(Tenant.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(User.MaxEmailAddressLength)]
        public string AdminEmailAddress { get; set; }

        [StringLength(User.MaxPasswordLength)]
        [DisableAuditing]
        [RegularExpression(@"^(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$", ErrorMessage = "Password required at least 8 characters, 1 capital letter, 1 number, and 1 special character")]
        public string AdminPassword { get; set; }

        [MaxLength(AbpTenantBase.MaxConnectionStringLength)]
        [DisableAuditing]
        public string ConnectionString { get; set; }

        public bool ShouldChangePasswordOnNextLogin { get; set; }
        
        public bool SendActivationEmail { get; set; }

        public int? EditionId { get; set; }

        public bool IsActive { get; set; }

        [Required]
        [MaxLength(Tenant.MaxFullNameLength)]
        public virtual string FullName { get; set; }

        [Required]
        [MaxLength(Tenant.MaxCompanyNameLength)]
        public virtual string CompanyName { get; set; }

        [Required]
        [MaxLength(Tenant.MaxABNLength)]
        public virtual string ABN { get; set; }

        [MaxLength(Tenant.MaxFAXLength)]
        public virtual string FAX { get; set; }

        [Required]
        [MaxLength(Tenant.MaxPhoneLength)]
        [RegularExpression(@"^(07|03|08|04|13|18)[\d]{8}$", ErrorMessage = "Enter valid Phone(eg. 07|03|08|04|13|18XXXXXXXX)")]
        public virtual string Phone { get; set; }

        [MaxLength(Tenant.MaxMobileLength)]
        [RegularExpression(@"^(07|03|08|04|13|18)[\d]{8}$", ErrorMessage = "Enter valid Mobile(eg. 07|03|08|04|13|18XXXXXXXX)")]
        public virtual string Mobile { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(Tenant.MaxEmailLength)]
        public virtual string Email { get; set; }

        [Required]
        public virtual float STCCharges { get; set; }

        [Required]
        [MaxLength(Tenant.MaxRetailerNameLength)]
        public virtual string RetailerName { get; set; }

        [Required]
        [MaxLength(Tenant.MaxRetailerABNLength)]
        public virtual string RetailerABN { get; set; }

		[MaxLength(Tenant.MaxStreetAddressLength)]
		public virtual string StreetAddress { get; set; }

		[MaxLength(Tenant.MaxUnitNumberLength)]
		public virtual string UnitNo { get; set; }

		[MaxLength(Tenant.MaxUnitTypeLength)]
		public virtual string UnitType { get; set; }

		[Required]
		[MaxLength(Tenant.MaxStreetNumberLength)]
		public virtual string StreetNumber { get; set; }

		[Required]
		[MaxLength(Tenant.MaxStreetNameLength)]
		public virtual string StreetName { get; set; }

		[Required]
		[MaxLength(Tenant.MaxStreetTypeLength)]
		public virtual string StreetType { get; set; }

		[Required]
		[MaxLength(Tenant.MaxStreetCityLength)]
		public virtual string Suburb { get; set; }

        public virtual int? Suburb_Id { get; set; }

        [Required]
		[MaxLength(Tenant.MaxStreetStateLength)]
		public virtual string State { get; set; }

		[Required]
		[MaxLength(Tenant.MaxStreetPostCodeLength)]
		public virtual string PostCode { get; set; }
	}
}