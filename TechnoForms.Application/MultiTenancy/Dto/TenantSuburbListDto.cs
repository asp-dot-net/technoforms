﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostCodes;

namespace TechnoForms.MultiTenancy.Dto
{
	[AutoMapFrom(typeof(PostCode))]
	public class TenantSuburbListDto : FullAuditedEntityDto
	{
		public string Suburb { get; set; }
		public string SuburbstatePostcodes { get; set; }
		public virtual int StateId { get; set; }
		public string stateName { get; set; }
		public string Postcode { get; set; }
	}
}
