﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Report.Dto;

namespace TechnoForms.Report
{
	public interface IReportAppService : IApplicationService
	{
        string eleReport(GetReportInput input);
        string cecReport(GetReportInput input);
        string cuckReport(GetReportInput input);
        string swrReport(GetReportInput input);
        string documentsReport(GetReportInput input);

    }
}
