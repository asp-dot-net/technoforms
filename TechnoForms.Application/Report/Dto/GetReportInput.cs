﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Report.Dto
{
	public class GetReportInput
	{
		public int JobId { get; set; }
	}
}
