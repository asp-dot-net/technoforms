﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TechnoForms.InstallerEmployes;
using TechnoForms.Installers;
using TechnoForms.Jobs;
using TechnoForms.MultiTenancy;
using TechnoForms.Photos;
using TechnoForms.PhotoTypes;
using TechnoForms.Report.Dto;
using TechnoForms.ScanedInverters;
using TechnoForms.ScanedPanels;
using TechnoForms.Web;

namespace TechnoForms.Report
{
	public class ReportAppService : TechnoFormsAppServiceBase, IReportAppService
	{
		private readonly IAppFolders _appFolders;
		private readonly IRepository<Job> _jobRepository;
		private readonly IRepository<Installer> _installerRepository;
		private readonly IRepository<InstallerEmploye> _installerEmployeRepository;
		private readonly IRepository<Tenant> _tenantRepository;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IRepository<ScanedPanel> _scanedPanelRepository;
		private readonly IRepository<ScanedInverter> _scanedInverterRepository;
		private readonly IRepository<Photo> _photoRepository;
		private readonly IRepository<PhotoType> _photoTypeRepository;

		public ReportAppService(IAppFolders appFolders,
			IRepository<Job> jobRepository,
			IRepository<Installer> installerRepository,
			IRepository<InstallerEmploye> installerEmployeRepository,
			IRepository<Tenant> tenantRepository,
			IUnitOfWorkManager unitOfWorkManager,
			IRepository<ScanedPanel> scanedPanelRepository,
			IRepository<ScanedInverter> scanedInverterRepository,
			IRepository<Photo> photoRepository,
			IRepository<PhotoType> photoTypeRepository)
		{
			_appFolders = appFolders;
			_jobRepository = jobRepository;
			_installerRepository = installerRepository;
			_installerEmployeRepository = installerEmployeRepository;
			_tenantRepository = tenantRepository;
			_unitOfWorkManager = unitOfWorkManager;
			_scanedPanelRepository = scanedPanelRepository;
			_scanedInverterRepository = scanedInverterRepository;
			_photoRepository = photoRepository;
			_photoTypeRepository = photoTypeRepository;
		}

		//Testing&Compliance
		public string eleReport(GetReportInput input)
		{
			var report_parent = new Report.ELEReport();
			//Guid Newguid = new Guid(input.Guid);
			var Result = _jobRepository.GetAll().Where(J => J.Id == input.JobId).FirstOrDefault();
			var tenantName = _tenantRepository.GetAll().Where(e => e.Id == Result.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstDetail = _installerRepository.GetAll().Where(I => I.Id == Result.CreateFor_Id).FirstOrDefault();

				Telerik.Reporting.TextBox InstDate = report_parent.Items.Find("textBox82", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstMonth = report_parent.Items.Find("textBox81", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstYear = report_parent.Items.Find("textBox78", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox ElecNumber = report_parent.Items.Find("textBox84", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstName = report_parent.Items.Find("textBox91", true)[0] as Telerik.Reporting.TextBox;

				InstDate.Value = string.Format("{0:dd}", Convert.ToDateTime(Result.ElectricianSignatureDate));
				InstMonth.Value = string.Format("{0:MM}", Convert.ToDateTime(Result.ElectricianSignatureDate));
				InstYear.Value = string.Format("{0:yyyy}", Convert.ToDateTime(Result.ElectricianSignatureDate));
				ElecNumber.Value = InstDetail.InstallerExpiry;
				InstName.Value = InstDetail.FullName;
			}
			Telerik.Reporting.TextBox FirstName = report_parent.Items.Find("textBox102", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Surname = report_parent.Items.Find("textBox99", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Street = report_parent.Items.Find("textBox97", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Suburb = report_parent.Items.Find("textBox94", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox PostCode = report_parent.Items.Find("textBox92", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SignDate = report_parent.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SignMonth = report_parent.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SignYear = report_parent.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.PictureBox InstSign = report_parent.Items.Find("pictureBox2", true)[0] as Telerik.Reporting.PictureBox;

			string imgmap = "";
			if (!string.IsNullOrEmpty(Result.ElectricianSignature))
			{
				imgmap = WebUrlService.WebSiteRootAddress + "Temp/" + tenantName + "/" + Result.FormId + "/ElectricianSignature/" + Result.ElectricianSignature;
				imgmap = RemoveTenantName(imgmap);

				//if (System.IO.File.Exists(imgmap))
				//{
				InstSign.Value = Convert.ToString(imgmap);
				//}
			}

			FirstName.Value = Result.FirstName;
			Surname.Value = Result.LastName;
			Street.Value = Result.StreetAddress;
			Suburb.Value = Result.StreetCity;
			PostCode.Value = Result.StreetPostCode;

			SignDate.Value = string.Format("{0:dd}", Convert.ToDateTime(Result.ElectricianSignatureDate));
			SignMonth.Value = string.Format("{0:MM}", Convert.ToDateTime(Result.ElectricianSignatureDate));
			SignYear.Value = string.Format("{0:yyyy}", Convert.ToDateTime(Result.ElectricianSignatureDate));


			//ExportPdf(report_parent);
			var Filename = DateTime.Now.Ticks + "_EleReport";
			SavePDF(report_parent, Filename);
			return Filename;
		}

		//STC
		public string cecReport(GetReportInput input)
		{
			var report_parent = new Report.CECReport();
			//Guid Newguid = new Guid(input.Guid);
			var Result = _jobRepository.GetAll().Where(J => J.Id == input.JobId).FirstOrDefault();
			var PanelResult = _scanedPanelRepository.GetAll().Where(s => s.Guid == Result.Guid).Select(s => s.PanelSerialNumber).ToList();
			var InverterResult = _scanedInverterRepository.GetAll().Where(s => s.Guid == Result.Guid).Select(s => s.InverterSerialNumber).ToList();
			var tenantName = _tenantRepository.GetAll().Where(e => e.Id == Result.TenantId).Select(e => e.TenancyName).FirstOrDefault();

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var installer = _installerRepository.GetAll().Where(I => I.Id == Result.CreateFor_Id).FirstOrDefault();
				var InstDetail = _installerEmployeRepository.GetAll().Where(I => I.Id == Result.Installer_Id).FirstOrDefault();
				var DesiDetail = _installerEmployeRepository.GetAll().Where(I => I.Id == Result.Designer_Id).FirstOrDefault();
				var ElecDetail = _installerEmployeRepository.GetAll().Where(I => I.Id == Result.Electrician_Id).FirstOrDefault();

				Telerik.Reporting.Table InstallerDetail = report_parent.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
				Telerik.Reporting.Table DesignerDetail = report_parent.Items.Find("table15", true)[0] as Telerik.Reporting.Table;
				Telerik.Reporting.Table ElectricianDetail = report_parent.Items.Find("table16", true)[0] as Telerik.Reporting.Table;
				Telerik.Reporting.Table table10 = report_parent.Items.Find("table10", true)[0] as Telerik.Reporting.Table;
				Telerik.Reporting.Table Installer = report_parent.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
				Telerik.Reporting.Table Designer = report_parent.Items.Find("table5", true)[0] as Telerik.Reporting.Table;

				InstallerDetail.DataSource = InstDetail;
				DesignerDetail.DataSource = DesiDetail;
				ElectricianDetail.DataSource = ElecDetail;
				table10.DataSource = InstDetail;
				Installer.DataSource = InstDetail;
				Designer.DataSource = DesiDetail;

			}
			var TenantDetail = _tenantRepository.GetAll().Where(T => T.Id == Result.TenantId).FirstOrDefault();

			Telerik.Reporting.Table ContatDetail = report_parent.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.Table Panel_Inverter = report_parent.Items.Find("table6", true)[0] as Telerik.Reporting.Table;

			Telerik.Reporting.Table table17 = report_parent.Items.Find("table17", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.Table table18 = report_parent.Items.Find("table18", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.Table table19 = report_parent.Items.Find("table19", true)[0] as Telerik.Reporting.Table;
			//Telerik.Reporting.Table table11 = report_parent.Items.Find("table11", true)[0] as Telerik.Reporting.Table;

			Telerik.Reporting.CheckBox Building = report_parent.Items.Find("checkBox1", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Ground = report_parent.Items.Find("chkGround", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox NewE = report_parent.Items.Find("checkBox11", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox NewN = report_parent.Items.Find("checkBox12", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox NewW = report_parent.Items.Find("checkBox13", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox NewOther = report_parent.Items.Find("checkBox14", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox OldE = report_parent.Items.Find("checkBox15", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox OldN = report_parent.Items.Find("checkBox16", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox OldW = report_parent.Items.Find("checkBox17", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox OldOther = report_parent.Items.Find("checkBox18", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Residential = report_parent.Items.Find("chkResidential", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox School = report_parent.Items.Find("chkschool", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Commercial = report_parent.Items.Find("chkCommercial", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Single = report_parent.Items.Find("checkBox6", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Multi = report_parent.Items.Find("checkBox7", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox WithoutBattery = report_parent.Items.Find("checkBox3", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox WithBattery = report_parent.Items.Find("checkBox4", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Standalon = report_parent.Items.Find("checkBox5", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Addition = report_parent.Items.Find("checkBox2", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox CompleteUnit = report_parent.Items.Find("checkBox19", true)[0] as Telerik.Reporting.CheckBox;

			Telerik.Reporting.TextBox Date = report_parent.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox128 = report_parent.Items.Find("textBox128", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox140 = report_parent.Items.Find("textBox140", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox146 = report_parent.Items.Find("textBox146", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox153 = report_parent.Items.Find("textBox153", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox159 = report_parent.Items.Find("textBox159", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox expInstDate = report_parent.Items.Find("textBox49", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox74 = report_parent.Items.Find("textBox74", true)[0] as Telerik.Reporting.TextBox;

			Telerik.Reporting.TextBox MANUFAACTURER = report_parent.Items.Find("textBox103", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox PanelMODEL = report_parent.Items.Find("textBox176", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SERIALNUMBER = report_parent.Items.Find("textBox141", true)[0] as Telerik.Reporting.TextBox;

			Telerik.Reporting.TextBox BRAND = report_parent.Items.Find("textBox172", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox InvMODEL = report_parent.Items.Find("textBox152", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SERIES = report_parent.Items.Find("textBox157", true)[0] as Telerik.Reporting.TextBox;

			Telerik.Reporting.TextBox SGUComOrRes = report_parent.Items.Find("textBox100", true)[0] as Telerik.Reporting.TextBox;

			Telerik.Reporting.PictureBox InstSign = report_parent.Items.Find("pictureBox8", true)[0] as Telerik.Reporting.PictureBox;
			Telerik.Reporting.PictureBox DesiSign = report_parent.Items.Find("pictureBox9", true)[0] as Telerik.Reporting.PictureBox;
			Telerik.Reporting.PictureBox OwnerSign = report_parent.Items.Find("pictureBox10", true)[0] as Telerik.Reporting.PictureBox;
			Telerik.Reporting.PictureBox ElecSign = report_parent.Items.Find("pictureBox11", true)[0] as Telerik.Reporting.PictureBox;

			if (Result.SolarType_Id == 1)
			{
				SGUComOrRes.Value = "Domestic";
			}
			else
			{
				SGUComOrRes.Value = "Commercial";
			}

			ContatDetail.DataSource = Result;
			Panel_Inverter.DataSource = Result;

			var SerialNo = "";
			var Series = "";
			for (int i = 0; i < PanelResult.Count; i++)
			{
				if (Convert.ToString(PanelResult[i]) != null)
				{
					if (i == PanelResult.Count - 1)
					{
						SerialNo = SerialNo + PanelResult[i];
					}
					else
					{
						SerialNo = SerialNo + PanelResult[i] + ",";
					}
				}
			}
			for (int i = 0; i < InverterResult.Count; i++)
			{
				if (Convert.ToString(InverterResult[i]) != null)
				{
					if (i == InverterResult.Count - 1)
					{
						Series = Series + InverterResult[i];
					}
					else
					{
						Series = Series + InverterResult[i] + ",";
					}
				}
			}

			MANUFAACTURER.Value = Result.BrandName;
			PanelMODEL.Value = Result.PanelModel;
			SERIALNUMBER.Value = SerialNo;
			BRAND.Value = Result.InverterBrandName;
			InvMODEL.Value = Result.InverterModel;
			SERIES.Value = Series;

			//table11.DataSource = Result;
			table17.DataSource = TenantDetail;
			table18.DataSource = TenantDetail;
			table19.DataSource = TenantDetail;

			if (Result.Mountingtype_Id == 1)
			{
				Building.Value = true;
			}
			else if (Result.Mountingtype_Id == 2)
			{
				Ground.Value = true;
			}

			if (Result.SolarType_Id == 1)
			{
				Residential.Value = true;
			}
			else if (Result.SolarType_Id == 2)
			{
				Commercial.Value = true;
			}

			if (Result.RoofFacingPanel_Id == 1)
			{
				NewN.Value = true;
			}
			else if (Result.RoofFacingPanel_Id == 3)
			{
				NewE.Value = true;
			}
			else if (Result.RoofFacingPanel_Id == 7)
			{
				NewW.Value = true;
			}
			else if (Result.RoofFacingPanel_Id == 2 || Result.RoofFacingPanel_Id == 4 || Result.RoofFacingPanel_Id == 5 || Result.RoofFacingPanel_Id == 6 || Result.RoofFacingPanel_Id == 8)
			{
				NewOther.Value = true;
			}

			if (Result.Sugstthisaddress_Id == 1)
			{
				OldN.Value = true;
			}
			else if (Result.Sugstthisaddress_Id == 3)
			{
				OldE.Value = true;
			}
			else if (Result.Sugstthisaddress_Id == 7)
			{
				OldW.Value = true;
			}
			else if (Result.Sugstthisaddress_Id == 2 || Result.Sugstthisaddress_Id == 4 || Result.Sugstthisaddress_Id == 5 || Result.Sugstthisaddress_Id == 6 || Result.Sugstthisaddress_Id == 8)
			{
				OldOther.Value = true;
			}

			if (Result.HouseType == 3)
			{
				Single.Value = true;
			}
			else if (Result.HouseType == 2 || Result.HouseType == 1)
			{
				Multi.Value = true;
			}

			if (Result.SystemType_Id == 1)
			{
				textBox128.Value = "No";
			}
			else
			{
				textBox128.Value = "Yes";
			}

			if (Result.SystemType_Id == 1)
			{
				textBox140.Value = "No";
			}
			else
			{
				textBox140.Value = "Yes";
			}

			textBox146.Value = "Yes";

			if (Result.Sugstthisaddress_Id == 2)
			{
				textBox153.Value = "No";
			}
			else if (Result.Sugstthisaddress_Id == 1)
			{
				textBox153.Value = "Yes";
			}

			if (Result.Auditreportavailable_Id == 2)
			{
				textBox159.Value = "No";
			}
			else if (Result.Auditreportavailable_Id == 1)
			{
				textBox159.Value = "Yes";
			}
			expInstDate.Value = Result.ExpectedInstallDate.ToString("dd/MM/yyyy");
			textBox74.Value = Result.ExpectedInstallDate.ToString("dd/MM/yyyy");


			if (Result.ElectricalSafetyDocumentation_Id == 1)
			{
				WithoutBattery.Value = true;
			}
			else if (Result.ElectricalSafetyDocumentation_Id == 2)
			{
				WithBattery.Value = true;
			}
			else if (Result.ElectricalSafetyDocumentation_Id == 3)
			{
				Standalon.Value = true;
			}
			if (Result.SystemType_Id == 1)
			{
				CompleteUnit.Value = true;
			}
			else
			{
				Addition.Value = true;
			}

			string Instimgmap = "";
			if (!string.IsNullOrEmpty(Result.SignatureImageInst))
			{
				Instimgmap = WebUrlService.WebSiteRootAddress + "Temp/" + tenantName + "/" + Result.FormId + "/InstallerSignature/" + Result.SignatureImageInst;
				Instimgmap = RemoveTenantName(Instimgmap);

				//if (System.IO.File.Exists(Instimgmap))
				//{
				InstSign.Value = Convert.ToString(Instimgmap);
				//}
			}
			string Desiimgmap = "";
			if (!string.IsNullOrEmpty(Result.DesignerSignature))
			{
				Desiimgmap = WebUrlService.WebSiteRootAddress + "Temp/" + tenantName + "/" + Result.FormId + "/DesignerSignature/" + Result.DesignerSignature;
				Desiimgmap = RemoveTenantName(Desiimgmap);

				//if (System.IO.File.Exists(Desiimgmap))
				//{
				DesiSign.Value = Convert.ToString(Desiimgmap);
				//}
			}
			string Elecmap = "";
			if (!string.IsNullOrEmpty(Result.ElectricianSignature))
			{
				Elecmap = WebUrlService.WebSiteRootAddress + "Temp/" + tenantName + "/" + Result.FormId + "/ElectricianSignature/" + Result.ElectricianSignature;
				Elecmap = RemoveTenantName(Elecmap);

				//if (System.IO.File.Exists(Elecmap))
				//{
				ElecSign.Value = Convert.ToString(Elecmap);
				//}
			}
			if (Result.ElectricianSignatureDate != null)
			{
				Date.Value = Result.ElectricianSignatureDate.Value.ToString("dd/MM/yyyy");
			}
			string Ownermap = "";
			if (!string.IsNullOrEmpty(Result.SignatureImageOwner))
			{
				Ownermap = WebUrlService.WebSiteRootAddress + "Temp/" + tenantName + "/" + Result.FormId + "/OwnerSignature/" + Result.SignatureImageOwner;
				Ownermap = RemoveTenantName(Ownermap);

				//if (System.IO.File.Exists(Ownermap))
				//{
				OwnerSign.Value = Convert.ToString(Ownermap);
				//}
			}

			//ExportPdf(report_parent);
			var Filename = DateTime.Now.Ticks + "_CecReport";
			SavePDF(report_parent, Filename);
			return Filename;
		}

		//CustomerAcknowledgement
		public string cuckReport(GetReportInput input)
		{
			var report_parent = new Report.CUCKReport();
			//Guid Newguid = new Guid(input.Guid);
			var Result = _jobRepository.GetAll().Where(J => J.Id == input.JobId).FirstOrDefault();
			var tenantName = _tenantRepository.GetAll().Where(e => e.Id == Result.TenantId).Select(e => e.TenancyName).FirstOrDefault();

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstDetail = _installerRepository.GetAll().Where(I => I.Id == Result.CreateFor_Id).FirstOrDefault();
				Telerik.Reporting.TextBox InstName = report_parent.Items.Find("textBox56", true)[0] as Telerik.Reporting.TextBox;

				InstName.Value = InstDetail.FullName;
			}

			Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox53", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Address1 = report_parent.Items.Find("textBox54", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox InstDate = report_parent.Items.Find("textBox55", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustNo = report_parent.Items.Find("textBox57", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox ProjectNo = report_parent.Items.Find("textBox58", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Date = report_parent.Items.Find("textBox59", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.PictureBox InstSign = report_parent.Items.Find("pictureBox1", true)[0] as Telerik.Reporting.PictureBox;

			string imgmap = "";
			if (!string.IsNullOrEmpty(Result.ElectricianSignature))
			{
				imgmap = WebUrlService.WebSiteRootAddress + "Temp/" + tenantName + "/" + Result.FormId + "/ElectricianSignature/" + Result.ElectricianSignature;
				imgmap = RemoveTenantName(imgmap);

				//if (System.IO.File.Exists(imgmap))
				//{
				InstSign.Value = Convert.ToString(imgmap);
				//}
			}
			Name.Value = Result.FirstName + " " + Result.LastName;
			Address1.Value = Result.StreetAddress + ", " + Result.StreetCity + " " + Result.StreetState + "-" + Result.StreetPostCode;
			InstDate.Value = Result.ExpectedInstallDate.ToString("dd/MM/yyyy");
			CustNo.Value = Result.FormId;
			ProjectNo.Value = Result.ProjectNumber;

			Date.Value = Result.ExpectedInstallDate.ToString("dd/MM/yyyy");

			//ExportPdf(report_parent);
			var Filename = DateTime.Now.Ticks + "_CuckReport";
			SavePDF(report_parent, Filename);
			return Filename;
		}

		//Download All Documents
		public string documentsReport(GetReportInput input)
		{
			var report_parent = new Report.Documents();
			var Result = _jobRepository.GetAll().Where(J => J.Id == input.JobId).FirstOrDefault();
			var PanelResult = _scanedPanelRepository.GetAll().Where(s => s.Guid == Result.Guid).ToList();
			var InverterResult = _scanedInverterRepository.GetAll().Where(s => s.Guid == Result.Guid).ToList();

			var TenantDetail = _tenantRepository.GetAll().Where(T => T.Id == Result.TenantId).FirstOrDefault();

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				//STC
				var installer = _installerRepository.GetAll().Where(I => I.Id == Result.CreateFor_Id).FirstOrDefault();
				var InstDetail = _installerEmployeRepository.GetAll().Where(I => I.Id == Result.Installer_Id).FirstOrDefault();
				var DesiDetail = _installerEmployeRepository.GetAll().Where(I => I.Id == Result.Designer_Id).FirstOrDefault();
				var ElecDetail = _installerEmployeRepository.GetAll().Where(I => I.Id == Result.Electrician_Id).FirstOrDefault();
				

				Telerik.Reporting.Table InstallerDetail = report_parent.Items.Find("table8", true)[0] as Telerik.Reporting.Table;
				Telerik.Reporting.Table DesignerDetail = report_parent.Items.Find("table15", true)[0] as Telerik.Reporting.Table;
				Telerik.Reporting.Table ElectricianDetail = report_parent.Items.Find("table16", true)[0] as Telerik.Reporting.Table;
				Telerik.Reporting.Table table10 = report_parent.Items.Find("table10", true)[0] as Telerik.Reporting.Table;
				Telerik.Reporting.Table Installer = report_parent.Items.Find("table4", true)[0] as Telerik.Reporting.Table;
				Telerik.Reporting.Table Designer = report_parent.Items.Find("table5", true)[0] as Telerik.Reporting.Table;

				InstallerDetail.DataSource = InstDetail;
				DesignerDetail.DataSource = DesiDetail;
				ElectricianDetail.DataSource = ElecDetail;
				table10.DataSource = InstDetail;
				Installer.DataSource = InstDetail;
				Designer.DataSource = DesiDetail;

				//ELE
				Telerik.Reporting.TextBox InstELEDate = report_parent.Items.Find("textBox163", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstMonth = report_parent.Items.Find("textBox162", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstYear = report_parent.Items.Find("textBox160", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox ElecNumber = report_parent.Items.Find("textBox166", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstName = report_parent.Items.Find("textBox171", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.PictureBox InstELESign = report_parent.Items.Find("pictureBox5", true)[0] as Telerik.Reporting.PictureBox;
				string imgmap = "";
				if (!string.IsNullOrEmpty(Result.ElectricianSignature))
				{
					imgmap = WebUrlService.WebSiteRootAddress + "Temp/" + TenantDetail.TenancyName + "/" + Result.FormId + "/ElectricianSignature/" + Result.ElectricianSignature;
					imgmap = RemoveTenantName(imgmap);

					//if (System.IO.File.Exists(imgmap))
					//{
					InstELESign.Value = Convert.ToString(imgmap);
					//}
				}

				InstELEDate.Value = string.Format("{0:dd}", Convert.ToDateTime(Result.ElectricianSignatureDate));
				InstMonth.Value = string.Format("{0:MM}", Convert.ToDateTime(Result.ElectricianSignatureDate));
				InstYear.Value = string.Format("{0:yyyy}", Convert.ToDateTime(Result.ElectricianSignatureDate));
				ElecNumber.Value = InstDetail.InstallerExpiry;
				InstName.Value = InstDetail.FullName;

				//CUCK
				Telerik.Reporting.PictureBox InstCUCKSign = report_parent.Items.Find("pictureBox15", true)[0] as Telerik.Reporting.PictureBox;
				Telerik.Reporting.TextBox InstCUCKName = report_parent.Items.Find("textBox227", true)[0] as Telerik.Reporting.TextBox;
				InstCUCKName.Value = InstDetail.FullName;
				string imgcuckmap = "";
				if (!string.IsNullOrEmpty(Result.ElectricianSignature))
				{
					imgcuckmap = WebUrlService.WebSiteRootAddress + "Temp/" + TenantDetail.TenancyName + "/" + Result.FormId + "/ElectricianSignature/" + Result.ElectricianSignature;
					imgcuckmap = RemoveTenantName(imgcuckmap);
					//if (System.IO.File.Exists(imgcuckmap))
					//{
					InstCUCKSign.Value = Convert.ToString(imgcuckmap);
					//}
				}
			}

			//STC
			Telerik.Reporting.Table ContatDetail = report_parent.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.Table Panel_Inverter = report_parent.Items.Find("table6", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.Table table17 = report_parent.Items.Find("table17", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.Table table18 = report_parent.Items.Find("table18", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.Table table19 = report_parent.Items.Find("table19", true)[0] as Telerik.Reporting.Table;
			//Telerik.Reporting.Table table11 = report_parent.Items.Find("table11", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.CheckBox Building = report_parent.Items.Find("checkBox1", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Ground = report_parent.Items.Find("chkGround", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox NewE = report_parent.Items.Find("checkBox11", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox NewN = report_parent.Items.Find("checkBox12", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox NewW = report_parent.Items.Find("checkBox13", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox NewOther = report_parent.Items.Find("checkBox14", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox OldE = report_parent.Items.Find("checkBox15", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox OldN = report_parent.Items.Find("checkBox16", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox OldW = report_parent.Items.Find("checkBox17", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox OldOther = report_parent.Items.Find("checkBox18", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Residential = report_parent.Items.Find("chkResidential", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox School = report_parent.Items.Find("chkschool", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Commercial = report_parent.Items.Find("chkCommercial", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Single = report_parent.Items.Find("checkBox6", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Multi = report_parent.Items.Find("checkBox7", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox WithoutBattery = report_parent.Items.Find("checkBox3", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox WithBattery = report_parent.Items.Find("checkBox4", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Standalon = report_parent.Items.Find("checkBox5", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox Addition = report_parent.Items.Find("checkBox2", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.CheckBox CompleteUnit = report_parent.Items.Find("checkBox19", true)[0] as Telerik.Reporting.CheckBox;
			Telerik.Reporting.TextBox Date = report_parent.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox128 = report_parent.Items.Find("textBox128", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox140 = report_parent.Items.Find("textBox140", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox146 = report_parent.Items.Find("textBox146", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox153 = report_parent.Items.Find("textBox153", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox159 = report_parent.Items.Find("textBox159", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox expInstDate = report_parent.Items.Find("textBox49", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox textBox74 = report_parent.Items.Find("textBox74", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox MANUFAACTURER = report_parent.Items.Find("textBox103", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox PanelMODEL = report_parent.Items.Find("textBox176", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SERIALNUMBER = report_parent.Items.Find("textBox141", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox BRAND = report_parent.Items.Find("textBox172", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox InvMODEL = report_parent.Items.Find("textBox152", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SERIES = report_parent.Items.Find("textBox157", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SGUComOrRes = report_parent.Items.Find("textBox100", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.PictureBox InstSign = report_parent.Items.Find("pictureBox8", true)[0] as Telerik.Reporting.PictureBox;
			Telerik.Reporting.PictureBox DesiSign = report_parent.Items.Find("pictureBox9", true)[0] as Telerik.Reporting.PictureBox;
			Telerik.Reporting.PictureBox OwnerSign = report_parent.Items.Find("pictureBox10", true)[0] as Telerik.Reporting.PictureBox;
			Telerik.Reporting.PictureBox ElecSign = report_parent.Items.Find("pictureBox11", true)[0] as Telerik.Reporting.PictureBox;

			if (Result.SolarType_Id == 1)
			{
				SGUComOrRes.Value = "Domestic";
			}
			else
			{
				SGUComOrRes.Value = "Commercial";
			}
			ContatDetail.DataSource = Result;
			Panel_Inverter.DataSource = Result;
			var SerialNo = "";
			var Series = "";
			for (int i = 0; i < PanelResult.Count; i++)
			{
				if (Convert.ToString(PanelResult[i].PanelSerialNumber) != null)
				{
					if (i == PanelResult.Count - 1)
					{
						SerialNo = SerialNo + PanelResult[i].PanelSerialNumber;
					}
					else
					{
						SerialNo = SerialNo + PanelResult[i].PanelSerialNumber + ",";
					}
				}
			}
			for (int i = 0; i < InverterResult.Count; i++)
			{
				if (Convert.ToString(InverterResult[i].InverterSerialNumber) != null)
				{
					if (i == InverterResult.Count - 1)
					{
						Series = Series + InverterResult[i].InverterSerialNumber;
					}
					else
					{
						Series = Series + InverterResult[i].InverterSerialNumber + ",";
					}
				}
			}
			MANUFAACTURER.Value = Result.BrandName;
			PanelMODEL.Value = Result.PanelModel;
			SERIALNUMBER.Value = SerialNo;
			BRAND.Value = Result.InverterBrandName;
			InvMODEL.Value = Result.InverterModel;
			SERIES.Value = Series;
			//table11.DataSource = Result;
			table17.DataSource = TenantDetail;
			table18.DataSource = TenantDetail;
			table19.DataSource = TenantDetail;
			if (Result.Mountingtype_Id == 1)
			{
				Building.Value = true;
			}
			else if (Result.Mountingtype_Id == 2)
			{
				Ground.Value = true;
			}
			if (Result.SolarType_Id == 1)
			{
				Residential.Value = true;
			}
			else if (Result.SolarType_Id == 2)
			{
				Commercial.Value = true;
			}
			if (Result.RoofFacingPanel_Id == 1)
			{
				NewN.Value = true;
			}
			else if (Result.RoofFacingPanel_Id == 3)
			{
				NewE.Value = true;
			}
			else if (Result.RoofFacingPanel_Id == 7)
			{
				NewW.Value = true;
			}
			else if (Result.RoofFacingPanel_Id == 2 || Result.RoofFacingPanel_Id == 4 || Result.RoofFacingPanel_Id == 5 || Result.RoofFacingPanel_Id == 6 || Result.RoofFacingPanel_Id == 8)
			{
				NewOther.Value = true;
			}
			if (Result.Sugstthisaddress_Id == 1)
			{
				OldN.Value = true;
			}
			else if (Result.Sugstthisaddress_Id == 3)
			{
				OldE.Value = true;
			}
			else if (Result.Sugstthisaddress_Id == 7)
			{
				OldW.Value = true;
			}
			else if (Result.Sugstthisaddress_Id == 2 || Result.Sugstthisaddress_Id == 4 || Result.Sugstthisaddress_Id == 5 || Result.Sugstthisaddress_Id == 6 || Result.Sugstthisaddress_Id == 8)
			{
				OldOther.Value = true;
			}
			if (Result.HouseType == 3)
			{
				Single.Value = true;
			}
			else if (Result.HouseType == 2 || Result.HouseType == 1)
			{
				Multi.Value = true;
			}
			if (Result.SystemType_Id == 1)
			{
				textBox128.Value = "No";
			}
			else
			{
				textBox128.Value = "Yes";
			}

			if (Result.SystemType_Id == 1)
			{
				textBox140.Value = "No";
			}
			else
			{
				textBox140.Value = "Yes";
			}
			textBox146.Value = "Yes";
			if (Result.Sugstthisaddress_Id == 2)
			{
				textBox153.Value = "No";
			}
			else if (Result.Sugstthisaddress_Id == 1)
			{
				textBox153.Value = "Yes";
			}
			if (Result.Auditreportavailable_Id == 2)
			{
				textBox159.Value = "No";
			}
			else if (Result.Auditreportavailable_Id == 1)
			{
				textBox159.Value = "Yes";
			}
			expInstDate.Value = Result.ExpectedInstallDate.ToString("dd/MM/yyyy");
			textBox74.Value = Result.ExpectedInstallDate.ToString("dd/MM/yyyy");
			if (Result.ElectricalSafetyDocumentation_Id == 1)
			{
				WithoutBattery.Value = true;
			}
			else if (Result.ElectricalSafetyDocumentation_Id == 2)
			{
				WithBattery.Value = true;
			}
			else if (Result.ElectricalSafetyDocumentation_Id == 3)
			{
				Standalon.Value = true;
			}
			if (Result.SystemType_Id == 1)
			{
				CompleteUnit.Value = true;
			}
			else
			{
				Addition.Value = true;
			}
			string Instimgmap = "";
			if (!string.IsNullOrEmpty(Result.SignatureImageInst))
			{
				Instimgmap = WebUrlService.WebSiteRootAddress + "Temp/" + TenantDetail.TenancyName + "/" + Result.FormId + "/InstallerSignature/" + Result.SignatureImageInst;
				Instimgmap = RemoveTenantName(Instimgmap);

				//if (System.IO.File.Exists(Instimgmap))
				//{
				InstSign.Value = Convert.ToString(Instimgmap);
				//}
			}
			string Desiimgmap = "";
			if (!string.IsNullOrEmpty(Result.DesignerSignature))
			{
				Desiimgmap = WebUrlService.WebSiteRootAddress + "Temp/" + TenantDetail.TenancyName + "/" + Result.FormId + "/DesignerSignature/" + Result.DesignerSignature;
				Desiimgmap = RemoveTenantName(Desiimgmap);

				//if (System.IO.File.Exists(Desiimgmap))
				//{
				DesiSign.Value = Convert.ToString(Desiimgmap);
				//}
			}
			string Elecmap = "";
			if (!string.IsNullOrEmpty(Result.ElectricianSignature))
			{
				Elecmap = WebUrlService.WebSiteRootAddress + "Temp/" + TenantDetail.TenancyName + "/" + Result.FormId + "/ElectricianSignature/" + Result.ElectricianSignature;
				Elecmap = RemoveTenantName(Elecmap);

				//if (System.IO.File.Exists(Elecmap))
				//{
				ElecSign.Value = Convert.ToString(Elecmap);
				//}
			}
			if (Result.ElectricianSignatureDate != null)
			{
				Date.Value = Result.ElectricianSignatureDate.Value.ToString("dd/MM/yyyy");
			}
			string Ownermap = "";
			if (!string.IsNullOrEmpty(Result.SignatureImageOwner))
			{
				Ownermap = WebUrlService.WebSiteRootAddress + "Temp/" + TenantDetail.TenancyName + "/" + Result.FormId + "/OwnerSignature/" + Result.SignatureImageOwner;
				Ownermap = RemoveTenantName(Ownermap);

				//if (System.IO.File.Exists(Ownermap))
				//{
				OwnerSign.Value = Convert.ToString(Ownermap);
				//}
			}

			//ELE
			Telerik.Reporting.TextBox FirstName = report_parent.Items.Find("textBox186", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Surname = report_parent.Items.Find("textBox185", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Street = report_parent.Items.Find("textBox177", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Suburb = report_parent.Items.Find("textBox174", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox PostCode = report_parent.Items.Find("textBox173", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SignDate = report_parent.Items.Find("textBox158", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SignMonth = report_parent.Items.Find("textBox147", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox SignYear = report_parent.Items.Find("textBox143", true)[0] as Telerik.Reporting.TextBox;

			FirstName.Value = Result.FirstName;
			Surname.Value = Result.LastName;
			Street.Value = Result.StreetAddress;
			Suburb.Value = Result.StreetCity;
			PostCode.Value = Result.StreetPostCode;
			SignDate.Value = string.Format("{0:dd}", Convert.ToDateTime(Result.ElectricianSignatureDate));
			SignMonth.Value = string.Format("{0:MM}", Convert.ToDateTime(Result.ElectricianSignatureDate));
			SignYear.Value = string.Format("{0:yyyy}", Convert.ToDateTime(Result.ElectricianSignatureDate));

			//CUCK
			Telerik.Reporting.TextBox Name = report_parent.Items.Find("textBox230", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Address1 = report_parent.Items.Find("textBox229", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox InstDate = report_parent.Items.Find("textBox228", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustNo = report_parent.Items.Find("textBox226", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox ProjectNo = report_parent.Items.Find("textBox225", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CUCKDate = report_parent.Items.Find("textBox224", true)[0] as Telerik.Reporting.TextBox;

			Name.Value = Result.FirstName + " " + Result.LastName;
			Address1.Value = Result.StreetAddress + ", " + Result.StreetCity + " " + Result.StreetState + "-" + Result.StreetPostCode;
			InstDate.Value = Result.ExpectedInstallDate.ToString("dd/MM/yyyy");
			CustNo.Value = Result.FormId;
			ProjectNo.Value = Result.ProjectNumber;
			CUCKDate.Value = Result.ExpectedInstallDate.ToString("dd/MM/yyyy");

			//Panel Images
			Telerik.Reporting.Table table23 = report_parent.Items.Find("table23", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.TextBox PanelFormId = report_parent.Items.Find("textBox260", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox PanelDocType = report_parent.Items.Find("textBox264", true)[0] as Telerik.Reporting.TextBox;

			List<TempPanel> PanelBarcodePhoto = new List<TempPanel>();
			for (int i = 0; i < PanelResult.Count; i++)
			{
				TempPanel PanelBarcodePhoto1 = new TempPanel();
				PanelBarcodePhoto1.PanelBarcodePhoto = "Temp/" + TenantDetail.TenancyName + "/" + Result.FormId + "/ScanedPanels/" + PanelResult[i].PanelBarcodePhoto;
				PanelBarcodePhoto.Add(PanelBarcodePhoto1);
			}
			table23.DataSource = PanelBarcodePhoto;
			PanelFormId.Value = Result.FormId;
			PanelDocType.Value = "Panel Photos";

			//Inverter Images
			Telerik.Reporting.Table table24 = report_parent.Items.Find("table24", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.TextBox InverterFormId = report_parent.Items.Find("textBox262", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox InverterDocType = report_parent.Items.Find("textBox265", true)[0] as Telerik.Reporting.TextBox;

			List<TempInverter> InverterBarcodePhoto = new List<TempInverter>();
			for (int i = 0; i < InverterResult.Count; i++)
			{
				TempInverter InverterBarcodePhoto1 = new TempInverter();
				InverterBarcodePhoto1.InverterBarcodePhoto = "Temp/" + TenantDetail.TenancyName + "/" + Result.FormId + "/ScanedInverters/" + InverterResult[i].InverterBarcodePhoto;
				InverterBarcodePhoto.Add(InverterBarcodePhoto1);
			}
			table24.DataSource = InverterBarcodePhoto;
			InverterFormId.Value = Result.FormId;
			InverterDocType.Value = "Inverter Photos";

			//Other Images
			Telerik.Reporting.Table table25 = report_parent.Items.Find("table25", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.TextBox OtherFormId = report_parent.Items.Find("textBox267", true)[0] as Telerik.Reporting.TextBox;

			var PhotosResult = (from ph in _photoRepository.GetAll()
								join pt in _photoTypeRepository.GetAll() on ph.FileType equals pt.Id
								where ph.Guid == Result.Guid
								select new TempOther
								{
									FileName = "Temp/" + TenantDetail.TenancyName + "/" + Result.FormId + "/Photos/" + ph.ImageName,
									FileTypeName = pt.PhotoTypeName,
									Latitude = ph.Latitude,
									Longitude = ph.Longitude
								}
								).ToList();

			//List<TempOther> OtherPhotos = new List<TempOther>();
			//for (int i = 0; i < PhotosResult.Count; i++)
			//{
			//	TempOther OtherPhotos1 = new TempOther();

			//	OtherPhotos1.FileName = "Temp/Photos/" + PhotosResult[i].ImageName;
			//	OtherPhotos1.Latitude = PhotosResult[i].Latitude;
			//	OtherPhotos1.Longitude = PhotosResult[i].Longitude;
			//	//OtherPhotos1.FileTypeName = _photoTypeRepository.GetAll().Where(p => p.Id == PhotosResult[i].FileType).Select(p => p.PhotoTypeName).FirstOrDefault();
			//	OtherPhotos.Add(OtherPhotos1);
			//}
			table25.DataSource = PhotosResult;
			OtherFormId.Value = Result.FormId;

			//ExportPdf(report_parent);
			var Filename = DateTime.Now.Ticks + "_Documents";
			SavePDF(report_parent, Filename);
			return Filename;
		}

		public class TempPanel
		{
			public string PanelBarcodePhoto { get; set; }
		}
		public class TempInverter
		{
			public string InverterBarcodePhoto { get; set; }
		}
		public class TempOther
		{
			public string FileTypeName { get; set; }
			public string Longitude { get; set; }
			public string Latitude { get; set; }
			public string FileName { get; set; }
		}
		public string swrReport(GetReportInput input)
		{
			var report_parent = new Report.SWR();

			Telerik.Reporting.Table tbl_detail = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.Table tbl_detail2 = report_parent.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
			Telerik.Reporting.Table tbl_detail3 = report_parent.Items.Find("table3", true)[0] as Telerik.Reporting.Table;

			//ExportPdf(report_parent);
			var Filename = DateTime.Now.Ticks + "_SwrReport";
			SavePDF(report_parent, Filename);
			return Filename;
		}

		public string AusNetUpgradeReport(GetReportInput input)
		{
			var FileName = "";
			var report_parent = new Report.AusNetUpgrade();
			var Result = _jobRepository.GetAll().Where(J => J.Id == input.JobId).FirstOrDefault();

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstDetail = _installerRepository.GetAll().Where(I => I.Id == Result.CreateFor_Id).FirstOrDefault();
				var InstEmpDetail = _installerEmployeRepository.GetAll().Where(E => E.InstallerId == InstDetail.Id).FirstOrDefault();

				Telerik.Reporting.TextBox CompanyName = report_parent.Items.Find("textBox10", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Address = report_parent.Items.Find("textBox8", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Phone = report_parent.Items.Find("textBox9", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox Email = report_parent.Items.Find("textBox12", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstName = report_parent.Items.Find("textBox74", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstAccCode = report_parent.Items.Find("textBox66", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox InstDate = report_parent.Items.Find("textBox69", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox ElecName = report_parent.Items.Find("textBox83", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox ElecAccCode = report_parent.Items.Find("textBox81", true)[0] as Telerik.Reporting.TextBox;
				Telerik.Reporting.TextBox ElecDate = report_parent.Items.Find("textBox78", true)[0] as Telerik.Reporting.TextBox;

				CompanyName.Value = InstDetail.CompanyName;
				Address.Value = InstDetail.StreetAddress + " " + InstDetail.Suburb + "," + InstDetail.State + "-" + InstDetail.PostCode;
				Phone.Value = InstDetail.Phone;
				Email.Value = InstDetail.EmailId;
				InstName.Value = InstDetail.FullName;
				InstAccCode.Value = InstDetail.InstallerExpiry;
				InstDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
				ElecName.Value = InstEmpDetail.FullName;
				ElecAccCode.Value = InstEmpDetail.ElectricalExpiry;
				ElecDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
			}


			Telerik.Reporting.TextBox CustName = report_parent.Items.Find("textBox88", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustNMI = report_parent.Items.Find("textBox86", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustAddress = report_parent.Items.Find("textBox90", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustEmail = report_parent.Items.Find("textBox87", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustBHours = report_parent.Items.Find("textBox95", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustNameBottom = report_parent.Items.Find("textBox140", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustDate = report_parent.Items.Find("textBox102", true)[0] as Telerik.Reporting.TextBox;


			CustName.Value = Result.FirstName + " " + Result.LastName;
			CustNMI.Value = Result.NMINo;
			CustAddress.Value = Result.InstallationStreetAddress + " " + Result.InstallationStreetCity + "," + Result.InstallationStreetState + "-" + Result.InstallationStreetPostCode;
			CustEmail.Value = Result.EmailId;
			CustBHours.Value = Result.Phone;
			CustNameBottom.Value = Result.FirstName + " " + Result.LastName;
			CustDate.Value = DateTime.Now.ToString("dd/MM/yyyy");

			FileName = DateTime.Now.Ticks + "_AusNetUpgradeReport";
			SavePDF(report_parent, FileName);

			return FileName;
		}

		public string ElectricalWorksRequestReport(GetReportInput input)
		{
			var FileName = "";
			var report_parent = new Report.ElectricalWorksRequest();
			var Result = _jobRepository.GetAll().Where(J => J.Id == input.JobId).FirstOrDefault();

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstDetail = _installerRepository.GetAll().Where(I => I.Id == Result.CreateFor_Id).FirstOrDefault();
				var InstEmpDetail = _installerEmployeRepository.GetAll().Where(E => E.InstallerId == InstDetail.Id).FirstOrDefault();

			}

			Telerik.Reporting.TextBox NMI = report_parent.Items.Find("textBox4", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustomerName = report_parent.Items.Find("textBox19", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox CustPhone = report_parent.Items.Find("textBox23", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox UNO = report_parent.Items.Find("textBox27", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox StreetNumber = report_parent.Items.Find("textBox29", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox StreetName = report_parent.Items.Find("textBox31", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox Suburb = report_parent.Items.Find("textBox37", true)[0] as Telerik.Reporting.TextBox;
			Telerik.Reporting.TextBox ProjectNo = report_parent.Items.Find("textBox51", true)[0] as Telerik.Reporting.TextBox;

			NMI.Value = Result.NMINo;
			CustomerName.Value = Result.FirstName + " " + Result.LastName;
			CustPhone.Value = Result.Phone;
			UNO.Value = Result.UnitNo;
			StreetNumber.Value = Result.StreetNo;
			StreetName.Value = Result.StreetName;
			Suburb.Value = Result.StreetCity;
			ProjectNo.Value = Result.ProjectNumber;

			FileName = DateTime.Now.Ticks + "_ElectricalWorksRequestReport";
			SavePDF(report_parent, FileName);

			return FileName;
		}

		public string NswNewOriginReport(GetReportInput input)
		{
			var FileName = "";
			var report_parent = new Report.NSWOrigin();
			var Result = _jobRepository.GetAll().Where(J => J.Id == input.JobId).FirstOrDefault();

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstDetail = _installerRepository.GetAll().Where(I => I.Id == Result.CreateFor_Id).FirstOrDefault();
				var InstEmpDetail = _installerEmployeRepository.GetAll().Where(E => E.InstallerId == InstDetail.Id).FirstOrDefault();
			}
			Telerik.Reporting.TextBox Company = report_parent.Items.Find("textBox17", true)[0] as Telerik.Reporting.TextBox;
			//Telerik.Reporting.Table table19 = report_parent.Items.Find("table19", true)[0] as Telerik.Reporting.Table;

			Company.Value = Result.Company;

			FileName = DateTime.Now.Ticks + "_NswNewOriginReport";
			SavePDF(report_parent, FileName);

			return FileName;
		}

		public string SaOriginReport(GetReportInput input)
		{
			var FileName = "";
			var report_parent = new Report.SAOrigin();
			var Result = _jobRepository.GetAll().Where(J => J.Id == input.JobId).FirstOrDefault();

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstDetail = _installerRepository.GetAll().Where(I => I.Id == Result.CreateFor_Id).FirstOrDefault();
				var InstEmpDetail = _installerEmployeRepository.GetAll().Where(E => E.InstallerId == InstDetail.Id).FirstOrDefault();
			}
			FileName = DateTime.Now.Ticks + "_SaOriginReport";
			SavePDF(report_parent, FileName);

			return FileName;
		}

		public string SaPowerNetworksReport(GetReportInput input)
		{
			var FileName = "";
			var report_parent = new Report.SAPowerNetworks();
			var Result = _jobRepository.GetAll().Where(J => J.Id == input.JobId).FirstOrDefault();

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstDetail = _installerRepository.GetAll().Where(I => I.Id == Result.CreateFor_Id).FirstOrDefault();
				var InstEmpDetail = _installerEmployeRepository.GetAll().Where(E => E.InstallerId == InstDetail.Id).FirstOrDefault();
			}
			FileName = DateTime.Now.Ticks + "_SaPowerNetworksReport";
			SavePDF(report_parent, FileName);

			return FileName;
		}

		public string UnitedEnergyInverterReport(GetReportInput input)
		{
			var FileName = "";
			var report_parent = new Report.UnitedEnergyInverter();
			var Result = _jobRepository.GetAll().Where(J => J.Id == input.JobId).FirstOrDefault();

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstDetail = _installerRepository.GetAll().Where(I => I.Id == Result.CreateFor_Id).FirstOrDefault();
				var InstEmpDetail = _installerEmployeRepository.GetAll().Where(E => E.InstallerId == InstDetail.Id).FirstOrDefault();
			}
			FileName = DateTime.Now.Ticks + "_UnitedEnergyInverterReport";
			SavePDF(report_parent, FileName);

			return FileName;
		}

		public string RemoveTenantName(string Url)
		{
			var TenantName = _tenantRepository.GetAll().Where(T => T.Id == AbpSession.TenantId).Select(T => T.TenancyName).FirstOrDefault();
			string NewUrl = Url.Replace("{TENANCY_NAME}.", TenantName);

			return NewUrl;
		}

		//public void ExportPdf(Telerik.Reporting.Report report)
		//{
		//	Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
		//	Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();
		//	instanceReportSource.ReportDocument = report;

		//	Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

		//	string fileName = result.DocumentName + "." + result.Extension;
		//	string path = System.IO.Path.GetTempPath();
		//	string filePath = System.IO.Path.Combine(path, fileName);

		//	Response.Clear();
		//	Response.ContentType = result.MimeType;
		//	Response.Cache.SetCacheability(HttpCacheability.Private);
		//	Response.Expires = -1;
		//	Response.Buffer = true;

		//	Response.AddHeader("Content-Disposition",
		//						string.Format("{0};FileName=\"{1}\"",
		//									  "attachment",
		//									  fileName));

		//	Response.BinaryWrite(result.DocumentBytes);
		//	Response.End();
		//}

		public void SavePDF(Telerik.Reporting.Report rpt, string FileName)
		{
			Telerik.Reporting.Processing.ReportProcessor reportProcessor = new Telerik.Reporting.Processing.ReportProcessor();
			Telerik.Reporting.InstanceReportSource instanceReportSource = new Telerik.Reporting.InstanceReportSource();

			instanceReportSource.ReportDocument = rpt;

			Telerik.Reporting.Processing.RenderingResult result = reportProcessor.RenderReport("PDF", instanceReportSource, null);

			Random rnd = new Random();


			string fullPath = "";

			string fileName = FileName + "." + result.Extension;
			string ExcelFilename = fileName;

			fullPath = Path.Combine(_appFolders.ReportDownloadFolder, fileName);

			FileStream fs = new FileStream(fullPath, FileMode.Create, FileAccess.Write);
			fs.Write(result.DocumentBytes, 0, result.DocumentBytes.Length);
			fs.Close();

		}
	}
}
