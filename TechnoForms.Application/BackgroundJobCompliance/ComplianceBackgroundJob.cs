﻿using Abp.BackgroundJobs;
using Abp.Dependency;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Net.Mail;
using System.Collections.Generic;
using System.Linq;
using TechnoForms.Jobs;
using System.IO;
using TechnoForms.Jobs.Dto;
using Abp.AutoMapper;
using TechnoForms.BackgroundJobCompliance;
using TechnoForms.Dto;
using System.Web;
using TechnoForms.CompliaenceJob.Exporting;
using TechnoForms.CompliaenceJob.Dto;

namespace TechnoForms.BackgroundJobCompliance
{
    public class ComplianceBackgroundJob : BackgroundJob<int>, ITransientDependency
    {
        private readonly IRepository<Job> _jobRepository;
        private readonly IEmailSender _emailSender;
        private readonly IJobListExcelExporter _jobListExcelExporter;
        private readonly IAppFolders _appFolders;

        public ComplianceBackgroundJob(IEmailSender emailSender,
            IRepository<Job> jobRepository,
            IJobListExcelExporter jobListExcelExporter,
            IAppFolders appFolders)
        {
            _emailSender = emailSender;
            _jobRepository = jobRepository;
            _jobListExcelExporter = jobListExcelExporter;
            _appFolders = appFolders;
        }
        [UnitOfWork]
        public override void Execute(int args)
        {
            var job = _jobRepository
           .GetAll().Where(p => p.IsVerifyCS == true && p.IsVerifyIN == true && p.IsVerifyPA == true && p.IsVerifyIS == true
             && p.IsVerifyImages == true);
            var jobListDtos = job.MapTo<List<ComplaienceJobsListExportExcel>>();

            FileDto file = _jobListExcelExporter.ExportToFile(jobListDtos);

            var filePath = Path.Combine(_appFolders.TempFileDownloadFolder, file.FileToken);
            var fileBytes = System.IO.File.ReadAllBytes(filePath);
            System.IO.File.Delete(filePath);

            if (!string.IsNullOrEmpty(_appFolders.ComplienceDownloadFolder))
            {
                string newfilePath = _appFolders.ComplienceDownloadFolder + "/JobList.xlsx";
                File.WriteAllBytes(newfilePath, fileBytes);
            }

            _emailSender.SendAsync("dharamistry@meghtechnologies.com", "Test Email", "Test Email");
            //return _jobListExcelExporter.ExportToFile(jobListDtos);
        }

        //public override void Execute(int args)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
