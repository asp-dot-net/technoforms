using System.ComponentModel.DataAnnotations;

namespace TechnoForms.MobileAppAccess.Dto
{
    public class SendPasswordResetLink
    {
        //public string TenancyName { get; set; }

        //[Required]
        public string EmailAddress { get; set; }
    }
}