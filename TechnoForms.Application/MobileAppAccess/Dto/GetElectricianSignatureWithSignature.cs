﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;

namespace TechnoForms.MobileAppAccess.Dto
{
    [AutoMapFrom(typeof(Job))]
    public class GetElectricianSignatureWithSignature : EntityDto
    {
        public virtual int TenantId { get; set; }

        public virtual string ElectricianSignature { get; set; }

        public virtual string ImagePath { get; set; }

        public virtual string FormId { get; set; }
    }
}
