﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.MobileAppAccess.Dto
{
    public class InstallerSignatureRequest
    {
        public virtual Guid Guid { get; set; }

        public virtual int TenantId { get; set; }
    }
}
