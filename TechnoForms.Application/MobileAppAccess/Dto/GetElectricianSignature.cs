﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Installers;

namespace TechnoForms.MobileAppAccess.Dto
{
    [AutoMapFrom(typeof(Installer))]
    public class GetElectricianSignature
    {
        public virtual string ElectricianSignature { get; set; }

        public virtual string ImagePath { get; set; }
    }
}
