﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.CustomerInstallers;

namespace TechnoForms.MobileAppAccess.Dto
{
    [AutoMap(typeof(CustomerInstaller))]
    public class GetMobileInvitationForReject : FullAuditedEntityDto
    {
        public virtual string FullName { get; set; }

        public virtual string rejectReason { get; set; }
    }
}
