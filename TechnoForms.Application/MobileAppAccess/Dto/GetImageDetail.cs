﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;

namespace TechnoForms.MobileAppAccess.Dto
{    
    public class GetImageDetail
    {
        public virtual string GuId { get; set; }

        public virtual int TenantId { get; set; }
    }
}
