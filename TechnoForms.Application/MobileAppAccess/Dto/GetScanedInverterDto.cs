﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.ScanedInverters;

namespace TechnoForms.MobileAppAccess.Dto
{
    public class GetScanedInverterDto
    {
        public List<ScannedInverterList> InverterList { get; set; }

        public virtual int TotalCount { get; set; }
    }

    [AutoMapFrom(typeof(ScanedInverter))]
    public class ScannedInverterList : EntityDto
    {
        public int TenantId { get; set; }

        public virtual string InverterSerialNumber { get; set; }

        public virtual string InverterBarcodePhoto { get; set; }

        public virtual string ImagePath { get; set; }
    }
}
