﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Photos;

namespace TechnoForms.MobileAppAccess.Dto
{

    public class GetOldSystemImagesDto
    {
        public List<OldImageDataList> OldImageDataList { get; set; }

        public virtual int TotalCount { get; set; }
    }

    [AutoMapFrom(typeof(Photo))]
    public class OldImageDataList : EntityDto
    {
        public virtual int FileType { get; set; }

        public virtual Guid Guid { get; set; }

        public virtual Guid ImageGuid { get; set; }

        public virtual string ImageName { get; set; }

        public virtual string ImagePath { get; set; }

        public virtual int? OldSystemDirection { get; set; }
    }
}
