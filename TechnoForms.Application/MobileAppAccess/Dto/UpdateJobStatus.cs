﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.MobileAppAccess.Dto
{
    public class UpdateJobStatus
    {
        public virtual int TenantId { get; set; }

        public virtual int JobStatus { get; set; }

        public virtual string GuId { get; set; }
    }
}
