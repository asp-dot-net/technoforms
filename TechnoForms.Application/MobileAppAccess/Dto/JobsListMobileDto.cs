﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;

namespace TechnoForms.MobileAppAccess.Dto
{
    [AutoMapFrom(typeof(Job))]
    public class JobsListMobileDto : FullAuditedEntityDto
    {
        public virtual Guid Guid { get; set; }

        public virtual int TenantId { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string Company { get; set; }

        public virtual string CustomerCompany { get; set; }

        public virtual string Phone { get; set; }

        public virtual DateTime ExpectedInstallDate { get; set; }

        public virtual string ExpectedInstallShortDate { get; set; }        

        public virtual string StreetAddress { get; set; }

        public virtual string StreetCity { get; set; }

        public virtual int? Suburb_Id { get; set; }

        public virtual string StreetState { get; set; }

        public virtual string StreetPostCode { get; set; }

        public virtual int JobStatus_Id { get; set; }


        public virtual string SignatureImageInst { get; set; }

        public virtual string SignatureImageOwner { get; set; }

        public virtual string InstallerSignFlag { get; set; }

        public virtual string OwnerSignFlag { get; set; }

        public virtual string TotalInverters { get; set; }

        public virtual string TotalPanels { get; set; }

        public virtual string TotalImages { get; set; }
    }
}
