﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;

namespace TechnoForms.MobileAppAccess.Dto
{
    [AutoMapFrom(typeof(Job))]
    public class JobDetailMobileDto : EntityDto
    {
        public virtual int Installer_Id { get; set; }

        public virtual int TenantId { get; set; }

        public virtual string ProjectNumber { get; set; }

        public virtual string FormId { get; set; }

        public virtual Guid Guid { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual string Phone { get; set; }

        public virtual string EmailId { get; set; }
               
        public virtual string StreetAddress { get; set; }

        public virtual string StreetCity { get; set; }

        public virtual int? Suburb_Id { get; set; }

        public virtual string StreetState { get; set; }

        public virtual string StreetPostCode { get; set; }

        public virtual int RoofType { get; set; }

        public virtual string RoofType_Name { get; set; }

        public virtual int HouseType { get; set; }

        public virtual string HouseType_Name { get; set; }

        public virtual string InstallerNote { get; set; }

        public virtual DateTime ExpectedInstallDate { get; set; }

        public virtual string ExpectedInstallShortDate { get; set; }        
        
        public virtual int SystemType_Id { get; set; }

        public virtual string SystemType_Name { get; set; }
        
        public virtual string NMINo { get; set; }

        public virtual int JobStatus_Id { get; set; }
        
        public virtual string CustomerCompanyName { get; set; }

        public virtual string SolarTypeName { get; set; }

        public virtual int SolarType_Id { get; set; }

        public virtual string ElectricianSignature { get; set; }

        public virtual string DesignerSignature { get; set; }

        public virtual string SignatureImagePickList { get; set; }

        public virtual string SignatureImageInst { get; set; }

        public virtual string SignatureImageOwner { get; set; }

        public virtual string[] PanelSerialNo { get; set; }

        public virtual string[] InverterSerialNo { get; set; }

        public virtual Nullable<int> TotalPanels { get; set; }

        public virtual Nullable<int> TotalInverters { get; set; }

        public virtual Nullable<int> NotScannedPanels { get; set; }

        public virtual Nullable<int> NotScannedInverters { get; set; }

        public virtual Nullable<int> OwnerSignFlag { get; set; }

        public virtual Nullable<int> InstallerSignFlag { get; set; }

        public virtual Nullable<int> ElectricianSignFlag { get; set; }

        public virtual Nullable<int> DesignerSignFlag { get; set; }

        public virtual Nullable<int> PickListSignFlag { get; set; }

        public virtual Nullable<int> EFTReceiptFlag { get; set; }

        public virtual Nullable<int> FrontOfHouseFlag { get; set; }

        public virtual Nullable<int> MtBoxBeforeInstFlag { get; set; }

        public virtual Nullable<int> MtBoxAfterInstFlag { get; set; }

        public virtual Nullable<int> SwitchBoxOpenedFlag { get; set; }

        public virtual Nullable<int> InverterInstalledFlag { get; set; }

        public virtual Nullable<int> InstallationSketchFlag { get; set; }

        public virtual Nullable<int> PanelOnRoofFlag { get; set; }

        public virtual Nullable<int> DCIsolatorFlag { get; set; }

        public virtual Nullable<int> SysSplitFlag { get; set; }

        public virtual Nullable<int> PaymentFlag { get; set; }

        public virtual Nullable<int> OthersFlag { get; set; }

        public virtual Nullable<int> OldSystemPhotoFlag { get; set; }

        public virtual Nullable<int> SMSSentStatus { get; set; }

        public List<PanelList> PanelList { get; set; }

        public List<InverterList> InverterList { get; set; }

        public virtual Nullable<int> OwnerCommentFlag { get; set; }

        public virtual Nullable<int> InstCommentFlag { get; set; }
    }

    //[AutoMapFrom(typeof(Job))]
    public class PanelList
    {
        //Panel List
        public virtual string PanelName { get; set; }

        public virtual string BrandName { get; set; }

        public virtual int Panel_id { get; set; }

        public virtual string PanelModel { get; set; }

        public virtual decimal PanelSize { get; set; }

        public virtual int NoOfPanels { get; set; }

        public virtual decimal SystemCapacity { get; set; }
        //Panel List End
    }

    //[AutoMapFrom(typeof(Job))]
    public class InverterList
    {
        //Inverter List
        public virtual string InverterName { get; set; }

        public virtual string InverterBrandName { get; set; }

        public virtual int Inverter_id { get; set; }

        public virtual string InverterModel { get; set; }

        public virtual string InverterSeries { get; set; }

        public virtual decimal NoOfInverter { get; set; }

        public virtual decimal InverterOutput { get; set; }
        //Inverter List End
    }
}
