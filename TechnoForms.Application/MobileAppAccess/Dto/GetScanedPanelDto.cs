﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.ScanedPanels;

namespace TechnoForms.MobileAppAccess.Dto
{
    public class GetScanedPanelDto
    {
        public List<ScanedPanelList> PanelList { get; set; }

        public virtual int TotalCount { get; set; }
    }

    [AutoMapFrom(typeof(ScanedPanel))]
    public class ScanedPanelList : EntityDto
    {
        public int TenantId { get; set; }

        public virtual string PanelSerialNumber { get; set; }

        public virtual string PanelBarcodePhoto { get; set; }

        public virtual string ImagePath { get; set; }
    }

}
