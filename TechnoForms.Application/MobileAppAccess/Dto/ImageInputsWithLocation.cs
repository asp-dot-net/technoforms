﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.MobileAppAccess.Dto
{
    public class ImageInputsWithLocation
    {
        public virtual Guid Guid { get; set; }

        public virtual byte[] FileData { get; set; }

        public virtual string Ext { get; set; }

        public virtual byte[] ImageData { get; set; }

        public virtual int TenantId { get; set; }

        public virtual string Latitude { get; set; }

        public virtual string Longitude { get; set; }
    }
}
