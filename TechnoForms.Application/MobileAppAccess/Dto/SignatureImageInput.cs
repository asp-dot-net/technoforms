﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.MobileAppAccess.Dto
{
    public class SignatureImageInput : EntityDto
    {
        //public virtual int TenantId { get; set; }

        public virtual byte[] ImageData { get; set; }
    }
}
