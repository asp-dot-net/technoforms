﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.CustomerInstallers;
using TechnoForms.Installers;

namespace TechnoForms.MobileAppAccess.Dto
{
    [AutoMapFrom(typeof(CustomerInstaller))]
    public class MobileInvitationListDto : FullAuditedEntityDto
    {
        public int TenantId { get; set; }

        public int InstallerId { get; set; }

        public int Inst_Request_status { get; set; }

        public string Inst_RejectReason { get; set; }

        public int Request_status { get; set; }

        public string RejectReason { get; set; }

        public virtual string CustomerFullName { get; set; }

        public virtual Guid? CustLogoGuid { get; set; }

        public virtual string CustomerLogoPath { get; set; }

    }
}
