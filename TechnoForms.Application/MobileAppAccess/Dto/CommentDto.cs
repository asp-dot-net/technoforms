﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;

namespace TechnoForms.MobileAppAccess.Dto
{
    public class CommentDto
    {
        public virtual int TenantId { get; set; }

        public virtual string Guid { get; set; }

        public virtual string Comment { get; set; }
    }
}
