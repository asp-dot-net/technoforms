﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.MobileAppAccess.Dto
{
    public class ImageInputs
    {
        public virtual string GuId { get; set; }

        public virtual int TenantId { get; set; }

        public virtual string[] ImageData { get; set; }

        public virtual string Longitude { get; set; }

        public virtual string Latitude { get; set; }
    }
}
