﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;

namespace TechnoForms.MobileAppAccess.Dto
{
    [AutoMapFrom(typeof(Job))]
    public class GetOwnerComment
    {
        public virtual int TenantId { get; set; }

        public virtual Guid Guid { get; set; }

        public virtual string OwnerComment { get; set; }
    }
}
