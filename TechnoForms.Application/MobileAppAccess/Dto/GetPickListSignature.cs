﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.MobileAppAccess.Dto
{
    public class GetPickListSignature
    {
        public virtual string InstallerSignature { get; set; }

        public virtual string ImagePath { get; set; }
    }
}
