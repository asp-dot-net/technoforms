﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.MobileAppAccess.Dto;

namespace TechnoForms.MobileAppAccess
{
    public interface IMobileAppService : IApplicationService
    {
        Task<InstallerInvitationDetailDto> GetInstallerInvitationDetail();

        ListResultDto<MobileInvitationListDto> GetAllInstallerInvitation();

        ListResultDto<MobileInvitationListDto> GetInstallerInvitation();

        Task ApproveInstallerInvitation(EntityDto input);

        Task RejectInstallerInvitation(GetMobileInvitationForReject input);

        ListResultDto<JobsListMobileDto> GetJobsList(int RecordType);

        JobDetailMobileDto GetJobsDetails(int TenantId, string GuId);

        Task UpdateJobStatus(UpdateJobStatus input);

        Task CustomerSignSMS(int TenantId, string GuId);

        Task InsertInstallerImage(SignatureImageInput input);

        Task InsertElectricianImage(SignatureImageInput input);

        Task InsertDesignerImage(SignatureImageInput input);

        Task<GetInstallerSignature> GetInstallerImage(EntityDto input);

        Task<GetElectricianSignature> GetElectricianImage(EntityDto input);

        Task<GetDesignerSignature> GetDesignerImage(EntityDto input);

        Task DeleteInstallerImage(EntityDto input);

        Task DeleteElectricianImage(EntityDto input);

        Task DeleteDesignerImage(EntityDto input);

        //1
        Task InsertEFTReceipt(ImageInputs input);
        //2
        Task InsertMtBoxBeforeInst(ImageInputs input);
        //3
        Task InsertMtBoxAfterInst(ImageInputs input);
        //4
        Task InsertFrontOfHouse(ImageInputs input);
        //5
        Task InsertSwitchBoxOpened(ImageInputs input);
        //6
        Task InsertInverterInstalled(ImageInputs input);
        //7
        Task InsertInstallationSketch(ImageInputs input);
        //8
        Task InsertPanelOnRoof(ImageInputs input);
        //9
        Task InsertDCIsolator(ImageInputs input);
        //10
        Task InsertPayment(ImageInputs input);
        //11
        Task InsertSysSplit(ImageInputs input);
        //12
        Task InsertOthers(ImageInputs input);
        //18
        Task InsertOldSystemPhotos(OldSystemImageInput input);
                
        //1
        GetImagesDto GetEFTReceipt(GetImageDetail input);
        //2
        GetImagesDto GetMtBoxBeforeInst(GetImageDetail input);
        //3
        GetImagesDto GetMtBoxAfterInst(GetImageDetail input);
        //4
        GetImagesDto GetFrontOfHouse(GetImageDetail input);
        //5
        GetImagesDto GetSwitchBoxOpened(GetImageDetail input);
        //6
        GetImagesDto GetInverterInstalled(GetImageDetail input);
        //7
        GetImagesDto GetInstallationSketch(GetImageDetail input);
        //8
        GetImagesDto GetPanelOnRoof(GetImageDetail input);
        //9
        GetImagesDto GetDCIsolator(GetImageDetail input);
        //10
        GetImagesDto GetPayment(GetImageDetail input);
        //11
        GetImagesDto GetSysSplit(GetImageDetail input);
        //12
        GetImagesDto GetOthers(GetImageDetail input);
        //18
        GetOldSystemImagesDto GetOldSystemPhotos(GetImageDetail input);

        Task DeletePhotos(DeleteImage input);

        Task DeleteInstallerSignature(InstallerSignatureRequest input);

        ListResultDto<GetInstallerSignatureWithLocation> GetInstallerSignature(InstallerSignatureRequest input);

        Task InstallerSignatureWithLocation(ImageInputsWithLocation input);

        Task ElectricianSignatureWithLocation(ImageInputsWithLocation input);

        ListResultDto<GetElectricianSignatureWithSignature> GetElectricianSignature(InstallerSignatureRequest input);

        Task AddInstallerComment(CommentDto input);

        ListResultDto<GetInstComment> GetInstallerComment(int TenantId, string GuId);

        Task AddOwnerComment(CommentDto input);

        ListResultDto<GetOwnerComment> GetOwanerComment(int TenantId, string GuId);
        
        Task DeleteElectricianSignature(InstallerSignatureRequest input);

        Task DesignerSignatureWithLocation(ImageInputsWithLocation input);

        Task DeleteDesignerSignature(InstallerSignatureRequest input);

        ListResultDto<GetDesignerSignatureWithLocation> GetDesignerSignature(InstallerSignatureRequest input);

        Task OwnerSignatureWithLocation(ImageInputsWithLocation input);

        ListResultDto<GetOwanerSignatureWithLocation> GetownerSignature(InstallerSignatureRequest input);

        Task DeleteOwnerSignature(InstallerSignatureRequest input);

        Task SignatureImagePickListWithLocation(ImageInputsWithLocation input);

        ListResultDto<GetPickListSignatureWithLocation> GetSignatureImagePickList(InstallerSignatureRequest input);

        Task DeleteSignatureImagePickList(InstallerSignatureRequest input);

        Task InsertScanedPanels(ScanedImageInput input);

        Task InsertScanedInverters(ScanedImageInput input);

        //ListResultDto<GetScanedPanelDto> GetScanedPanels(GetImageDetail input);

        GetScanedPanelDto GetScanedPanels(GetImageDetail input);

        //ListResultDto<GetScanedInverterDto> GetScanedInverters(GetImageDetail input);

        GetScanedInverterDto GetScanedInverters(GetImageDetail input);

        Task DeleteScanedPanels(DeleteScanedImages input);

        Task DeleteScanedInverters(DeleteScanedImages input);


        Task SendPasswordResetLink(SendPasswordResetLink model);

    }
}
