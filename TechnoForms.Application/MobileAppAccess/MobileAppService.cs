﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Configuration;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Timing;
using Abp.Timing.Timezone;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using TechnoForms.Authorization;
using TechnoForms.Authorization.Users;
using TechnoForms.CustomerInstallers;
using TechnoForms.Installers;
using TechnoForms.Inverters;
using TechnoForms.Jobs;
using TechnoForms.MobileAppAccess.Dto;
using TechnoForms.MultiTenancy;
using TechnoForms.Panels;
using TechnoForms.Photos;
using TechnoForms.ScanedInverters;
using TechnoForms.ScanedPanels;
using TechnoForms.Web;

namespace TechnoForms.MobileAppAccess
{
	//[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
	public class MobileAppService : TechnoFormsAppServiceBase, IMobileAppService
	{
		private readonly IRepository<Job> _jobRepository;
		private readonly IUnitOfWorkManager _unitOfWorkManager;
		private readonly IRepository<Tenant> _tenantRepository;
		private readonly IRepository<Installer> _InstallerRepository;
		private readonly IRepository<Photo> _photoRepository;
		private readonly IRepository<ScanedPanel> _scanedPanelRepository;
		private readonly IRepository<ScanedInverter> _scanedInverterRepository;
		private readonly IRepository<CustomerInstaller> _customerInstallerRepository;
		private readonly IRepository<SolarType> _solarTypeRepository;
		private readonly IRepository<RoofType> _roofTypeRepository;
		private readonly IRepository<HouseType> _houseTypeRepository;
		private readonly IRepository<Token> _tokenRepository;
		private readonly IRepository<SystemType> _systemTypeRepository;
		private readonly IRepository<Panel> _panelRepository;
		private readonly IRepository<Inverter> _inverterRepository;
		private readonly IRepository<CustomerSignLog> _customerSignLogRepository;
		private readonly ITimeZoneConverter _timeZoneConverter;
		private readonly ISettingManager _settingManager;
		private readonly UserManager _userManager;
		private readonly IUserEmailer _userEmailer;

		public MobileAppService(IRepository<Job> jobRepository,
			IUnitOfWorkManager unitOfWorkManager,
			IRepository<Tenant> tenantRepository,
			IRepository<Installer> InstallerRepository,
			IRepository<Photo> photoRepository,
			IRepository<ScanedPanel> scanedPanelRepository,
			IRepository<ScanedInverter> scanedInverterRepository,
			IRepository<CustomerInstaller> CustomerRepository,
			IRepository<SolarType> solarTypeRepository,
			IRepository<RoofType> roofTypeRepository,
			IRepository<HouseType> houseTypeRepository,
			IRepository<Token> tokenRepository,
			IRepository<SystemType> systemTypeRepository,
			IRepository<Panel> panelRepository,
			IRepository<Inverter> inverterRepository,
			ITimeZoneConverter timeZoneConverter,
			ISettingManager settingManager,
			UserManager userManager,
			IUserEmailer userEmailer,
			IRepository<CustomerSignLog> customerSignLogRepository)
		{
			_jobRepository = jobRepository;
			_unitOfWorkManager = unitOfWorkManager;
			_tenantRepository = tenantRepository;
			_InstallerRepository = InstallerRepository;
			_photoRepository = photoRepository;
			_scanedPanelRepository = scanedPanelRepository;
			_scanedInverterRepository = scanedInverterRepository;
			_customerInstallerRepository = CustomerRepository;
			_solarTypeRepository = solarTypeRepository;
			_roofTypeRepository = roofTypeRepository;
			_houseTypeRepository = houseTypeRepository;
			_tokenRepository = tokenRepository;
			_systemTypeRepository = systemTypeRepository;
			_panelRepository = panelRepository;
			_inverterRepository = inverterRepository;
			_timeZoneConverter = timeZoneConverter;
			_settingManager = settingManager;
			_userManager = userManager;
			_userEmailer = userEmailer;
			_customerSignLogRepository = customerSignLogRepository;
		}


		#region Installer Detail
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task<InstallerInvitationDetailDto> GetInstallerInvitationDetail()
		{
			long? UserId = AbpSession.UserId;
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstUserId = _InstallerRepository
				.GetAll()
				.Where(I => I.UserId == UserId)
				.Select(I => I.Id)
				.FirstOrDefault();
				//(await _customerInstallerRepository.GetAsync(input.Id));

				var installer = (await _InstallerRepository.GetAsync(InstUserId)).MapTo<InstallerInvitationDetailDto>();
				if (installer.InsAccreExDate != null)
				{
					DateTime? tempdate = installer.InsAccreExDate;
					var applicationsTimezone = _settingManager.GetSettingValueForApplication(TimingSettingNames.TimeZone);
					var newdate = _timeZoneConverter.Convert(tempdate);
					installer.InsAccreExShortDate = newdate.Value.ToString("dd-MM-yyyy");
					//installer.InsAccreExShortDate = installer.InsAccreExDate.Value.ToString("dd-MM-yyyy");
				}
				if (installer.EleLicenceExDate != null)
				{
					DateTime? tempdate = installer.EleLicenceExDate;
					var applicationsTimezone = _settingManager.GetSettingValueForApplication(TimingSettingNames.TimeZone);
					var newdate = _timeZoneConverter.Convert(tempdate);
					installer.EleLicenceExShortDate = newdate.Value.ToString("dd-MM-yyyy");
					//installer.EleLicenceExShortDate = installer.EleLicenceExDate.Value.ToString("dd-MM-yyyy");
				}
				if (installer.DesignerAccreExDate != null)
				{
					DateTime? tempdate = installer.DesignerAccreExDate;
					var applicationsTimezone = _settingManager.GetSettingValueForApplication(TimingSettingNames.TimeZone);
					var newdate = _timeZoneConverter.Convert(tempdate);
					installer.DesignerAccreExShortDate = newdate.Value.ToString("dd-MM-yyyy");
					//installer.DesignerAccreExShortDate = installer.DesignerAccreExDate.Value.ToString("dd-MM-yyyy");
				}
				return installer;
			}

		}
		#endregion

		#region Invitation
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task ApproveInstallerInvitation(EntityDto input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var invitationApprove = _customerInstallerRepository
				.GetAll()
				.WhereIf(
					input.Id != 0,
					p => p.Id == input.Id
				)
				.FirstOrDefault();
				invitationApprove.Inst_Request_status = 1;

				await _customerInstallerRepository.UpdateAsync(invitationApprove);
			}
		}

		//For Pending Requests
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public ListResultDto<MobileInvitationListDto> GetAllInstallerInvitation()
		{
			//IsApproved == 0 ----- Pending
			//IsApproved == 1 ----- Approved
			//IsApproved == 2 ----- Rejected
			long? UserId = AbpSession.UserId;
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstId = _InstallerRepository
					 .GetAll()
					 .Where(I => I.UserId == UserId)
					 .Select(I => I.Id)
					 .FirstOrDefault();

				var Installer_Invitation = _customerInstallerRepository
			   .GetAll()
			   .Where(I => I.InstallerId == InstId)
			   .OrderByDescending(p => p.Id)
			   .ToList();

				var InstallerInvitation = (from Inst in Installer_Invitation
										   join Us in _InstallerRepository.GetAll() on Inst.InstallerId equals Us.Id into Usjoined
										   from Us in Usjoined.DefaultIfEmpty()
										   where (Us.IsApproved == 1 && Inst.Inst_Request_status == 0)
										   group Inst by Inst into instGrouped
										   select instGrouped.Key);
				var Installer = InstallerInvitation.MapTo<List<MobileInvitationListDto>>();

				foreach (var item in Installer)
				{
					item.CustomerFullName = _tenantRepository.GetAll().Where(p => p.Id == item.TenantId).Select(p => p.FullName).FirstOrDefault();
					item.CustLogoGuid = _tenantRepository.GetAll().Where(p => p.Id == item.TenantId).Select(p => p.LogoId).FirstOrDefault();
					if (item.CustLogoGuid != null)
					{
						item.CustomerLogoPath = WebUrlService.WebSiteRootAddress + "TenantCustomization/GetLogoForMobileApp?TenantId=" + item.TenantId;
						item.CustomerLogoPath = RemoveTenantName(item.CustomerLogoPath);
					}
					else
					{
						item.CustomerLogoPath = WebUrlService.WebSiteRootAddress + "/Common/Images/app-logo-on-dark.png";
						item.CustomerLogoPath = RemoveTenantName(item.CustomerLogoPath);
					}
				}
				return new ListResultDto<MobileInvitationListDto>(Installer);

			}
		}

		//For Approved Requests
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public ListResultDto<MobileInvitationListDto> GetInstallerInvitation()
		{
			//IsApproved == 0 ----- Pending
			//IsApproved == 1 ----- Approved
			//IsApproved == 2 ----- Rejected
			long? UserId = AbpSession.UserId;
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstId = _InstallerRepository
					.GetAll()
					.Where(I => I.UserId == UserId)
					.Select(I => I.Id)
					.FirstOrDefault();

				var Installer_Invitation = _customerInstallerRepository
			   .GetAll()
			   .Where(I => I.InstallerId == InstId)
			   .OrderByDescending(p => p.Id)
			   .ToList();

				var InstallerInvitation = (from Inst in Installer_Invitation
										   join Us in _InstallerRepository.GetAll() on Inst.InstallerId equals Us.Id into Usjoined
										   from Us in Usjoined.DefaultIfEmpty()
										   where (Us.IsApproved == 1 && Inst.Inst_Request_status == 1)
										   group Inst by Inst into instGrouped
										   select instGrouped.Key);
				var Installer = InstallerInvitation.MapTo<List<MobileInvitationListDto>>();

				foreach (var item in Installer)
				{
					item.CustomerFullName = _tenantRepository.GetAll().Where(p => p.Id == item.TenantId).Select(p => p.FullName).FirstOrDefault();
					item.CustLogoGuid = _tenantRepository.GetAll().Where(p => p.Id == item.TenantId).Select(p => p.LogoId).FirstOrDefault();
					if (item.CustLogoGuid != null)
					{
						item.CustomerLogoPath = WebUrlService.WebSiteRootAddress + "TenantCustomization/GetLogoForMobileApp?TenantId=" + item.TenantId;
						item.CustomerLogoPath = RemoveTenantName(item.CustomerLogoPath);
					}
					else
					{
						item.CustomerLogoPath = WebUrlService.WebSiteRootAddress + "/Common/Images/app-logo-on-dark.png";
						item.CustomerLogoPath = RemoveTenantName(item.CustomerLogoPath);
					}
				}
				//http://localhost:6234/TenantCustomization/GetLogo?id=ed00422b-a9a4-42d9-6313-39f2e2e8c4c3
				return new ListResultDto<MobileInvitationListDto>(Installer);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task RejectInstallerInvitation(GetMobileInvitationForReject input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var invitationReject = _customerInstallerRepository
				.GetAll()
				.WhereIf(
					input.Id != 0,
					p => p.Id == input.Id
				)
				.FirstOrDefault();

				invitationReject.Inst_Request_status = 2;
				invitationReject.Inst_RejectReason = input.rejectReason;

				await _customerInstallerRepository.UpdateAsync(invitationReject);
			}
		}
		#endregion

		#region Insert Get Delete Signature From Installer Table
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertInstallerImage(SignatureImageInput input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstDetail = _InstallerRepository
							.GetAll()
							.Where(I => I.Id == input.Id)
							.FirstOrDefault();

				string FileName = "InstallerSignature_" + DateTime.Now.Ticks.ToString() + ".jpeg";

				if (input.ImageData.Length > 0)
				{
					string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
					using (MemoryStream mStream = new MemoryStream(input.ImageData))
					{
						System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
					}
					GC.Collect();
				}

				InstDetail.InstallerSignature = FileName;

				await _InstallerRepository.UpdateAsync(InstDetail);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertElectricianImage(SignatureImageInput input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstDetail = _InstallerRepository
							.GetAll()
							.Where(I => I.Id == input.Id)
							.FirstOrDefault();

				string FileName = "ElectricianSignature_" + DateTime.Now.Ticks.ToString() + ".jpeg";

				if (input.ImageData.Length > 0)
				{
					string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
					using (MemoryStream mStream = new MemoryStream(input.ImageData))
					{
						System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
					}
					GC.Collect();
				}

				InstDetail.ElectricianSignature = FileName;

				await _InstallerRepository.UpdateAsync(InstDetail);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertDesignerImage(SignatureImageInput input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var InstDetail = _InstallerRepository
							.GetAll()
							.Where(I => I.Id == input.Id)
							.FirstOrDefault();

				string FileName = "DesignerSignature_" + DateTime.Now.Ticks.ToString() + ".jpeg";

				if (input.ImageData.Length > 0)
				{
					string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
					using (MemoryStream mStream = new MemoryStream(input.ImageData))
					{
						System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
					}
					GC.Collect();
				}

				InstDetail.DesignerSignature = FileName;

				await _InstallerRepository.UpdateAsync(InstDetail);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task<GetInstallerSignature> GetInstallerImage(EntityDto input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var installer = (await _InstallerRepository.GetAsync(input.Id)).MapTo<GetInstallerSignature>();
				installer.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/Photos/" + installer.InstallerSignature;
				installer.ImagePath = RemoveTenantName(installer.ImagePath);
				return installer;
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task<GetElectricianSignature> GetElectricianImage(EntityDto input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var Electrician = (await _InstallerRepository.GetAsync(input.Id)).MapTo<GetElectricianSignature>();
				Electrician.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/Photos/" + Electrician.ElectricianSignature;
				Electrician.ImagePath = RemoveTenantName(Electrician.ImagePath);
				return Electrician;
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task<GetDesignerSignature> GetDesignerImage(EntityDto input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var Designer = (await _InstallerRepository.GetAsync(input.Id)).MapTo<GetDesignerSignature>();
				Designer.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/Photos/" + Designer.DesignerSignature;
				Designer.ImagePath = RemoveTenantName(Designer.ImagePath);
				return Designer;
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DeleteInstallerImage(EntityDto input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var Inst = _InstallerRepository
					.GetAll()
					.Where(I => I.Id == input.Id)
					.FirstOrDefault();
				if (!String.IsNullOrEmpty(Inst.InstallerSignature))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\InstallerPhoto\\" + Inst.InstallerSignature;
					System.IO.File.Delete(filepath);
				}
				Inst.InstallerSignature = null;
				await _InstallerRepository.UpdateAsync(Inst);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DeleteElectricianImage(EntityDto input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var Inst = _InstallerRepository
					.GetAll()
					.Where(I => I.Id == input.Id)
					.FirstOrDefault();
				if (!String.IsNullOrEmpty(Inst.ElectricianSignature))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\InstallerPhoto\\" + Inst.ElectricianSignature;
					System.IO.File.Delete(filepath);
				}
				Inst.ElectricianSignature = null;
				await _InstallerRepository.UpdateAsync(Inst);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DeleteDesignerImage(EntityDto input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				var Inst = _InstallerRepository
					.GetAll()
					.Where(I => I.Id == input.Id)
					.FirstOrDefault();
				if (!String.IsNullOrEmpty(Inst.DesignerSignature))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\InstallerPhoto\\" + Inst.DesignerSignature;
					System.IO.File.Delete(filepath);
				}
				Inst.DesignerSignature = null;
				await _InstallerRepository.UpdateAsync(Inst);
			}
		}
		#endregion

		#region Job
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public ListResultDto<JobsListMobileDto> GetJobsList(int RecordType)
		{
			//long? UserId = AbpSession.UserId;
			//var JobResult = new List<JobsListMobileDto>();
			//var JobList = new List<JobsListMobileDto>();
			//var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
			//for (int i = 0; i < tenant.Count; i++)
			//{
			//    using (_unitOfWorkManager.Current.SetTenantId(tenant[i].Id))
			//    {
			//        var InstId = _InstallerRepository
			//        .GetAll()
			//        .Where(I => I.UserId == UserId)
			//        .Select(I => I.Id)
			//        .FirstOrDefault();
			//        //RecordType = 0 ==> Pending,RecordType = 1 ==> History,RecordType = 2 ==> Future Jobs
			//        var JobData = _jobRepository
			//            .GetAll()
			//            .WhereIf(RecordType == 0, J => J.JobStatus_Id == 3)
			//            .WhereIf(RecordType == 1, J => J.ExpectedInstallDate <= DateTime.Now)
			//            .WhereIf(RecordType == 2, J => J.ExpectedInstallDate >= DateTime.Now && J.JobStatus_Id != 3)
			//            .Where(p => p.CreateFor_Id == InstId)
			//            .ToList();

			//        JobList = JobList.MapTo<List<JobsListMobileDto>>();
			//        foreach (var item in JobList)
			//        {
			//            item.ExpectedInstallShortDate = item.ExpectedInstallDate.ToString("dd-MM-yyyy");
			//            item.CustomerCompany = _tenantRepository.GetAll().Where(T => T.Id == item.TenantId).Select(T => T.CompanyName).FirstOrDefault();
			//            //job.TotalPanels = _scanedPanelRepository.GetAll().Where(P => P.Guid == item.Guid).Select(P => P.Id).Count();
			//            //job.TotalInverters = _scanedInverterRepository.GetAll().Where(P => P.Guid == item.Guid).Select(P => P.Id).Count();
			//            //job.TotalImages = _scanedPanelRepository.GetAll().Where(P => P.Guid == item.Guid).Select(P => P.Id).Count();
			//            //job.InstallerSignFlag = item.SignatureImageInst == null || item.SignatureImageInst == "" ? 0 : 1;
			//            //job.OwnerSignFlag = item.SignatureImageOwner == null || item.SignatureImageOwner == "" ? 0 : 1;
			//        }
			//        foreach (var item in JobList)
			//        {
			//            JobResult.Add(item);
			//        }
			//    }
			//}
			//return new ListResultDto<JobsListMobileDto>(JobResult.MapTo<List<JobsListMobileDto>>());
			long? UserId = AbpSession.UserId;
			var JobResult = new List<JobsListMobileDto>();
			var JobList = new List<JobsListMobileDto>();
			var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
			var InstId = 0;

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				InstId = _InstallerRepository
					.GetAll()
					.Where(I => I.UserId == UserId)
					.Select(I => I.Id)
					.FirstOrDefault();
			}

			//using (CurrentUnitOfWork.SetTenantId(7))
			for (int i = 0; i < tenant.Count; i++)
			{
				using (_unitOfWorkManager.Current.SetTenantId(tenant[i].Id))
				{
					//RecordType = 0 ==> Pending,RecordType = 1 ==> History,RecordType = 2 ==> Future Jobs
					var JobData = _jobRepository
					.GetAll()
					.WhereIf(RecordType == 0, J => J.JobStatus_Id == 3)
					.WhereIf(RecordType == 1, J => J.ExpectedInstallDate <= DateTime.Now)
					.WhereIf(RecordType == 2, J => J.ExpectedInstallDate >= DateTime.Now && J.JobStatus_Id != 3)
					.Where(J => J.CreateFor_Id == InstId)
					.ToList();

					JobList = JobData.MapTo<List<JobsListMobileDto>>();
					foreach (var item in JobList)
					{
						DateTime? tempdate = item.ExpectedInstallDate;
						var applicationsTimezone = _settingManager.GetSettingValueForApplication(TimingSettingNames.TimeZone);
						var newdate = _timeZoneConverter.Convert(tempdate, item.TenantId);
						item.ExpectedInstallShortDate = newdate.Value.ToString("dd-MM-yyyy");

						//item.ExpectedInstallShortDate = item.ExpectedInstallDate.ToString("dd-MM-yyyy");
						item.CustomerCompany = _tenantRepository.GetAll().Where(T => T.Id == item.TenantId).Select(T => T.CompanyName).FirstOrDefault();

						//job.TotalPanels = _scanedPanelRepository.GetAll().Where(P => P.Guid == item.Guid).Select(P => P.Id).Count();
						//job.TotalInverters = _scanedInverterRepository.GetAll().Where(P => P.Guid == item.Guid).Select(P => P.Id).Count();
						//job.TotalImages = _scanedPanelRepository.GetAll().Where(P => P.Guid == item.Guid).Select(P => P.Id).Count();
						//job.InstallerSignFlag = item.SignatureImageInst == null || item.SignatureImageInst == "" ? 0 : 1;
						//job.OwnerSignFlag = item.SignatureImageOwner == null || item.SignatureImageOwner == "" ? 0 : 1;

					}
					foreach (var item in JobList)
					{
						JobResult.Add(item);
					}
				}
			}
			return new ListResultDto<JobsListMobileDto>(JobResult.MapTo<List<JobsListMobileDto>>());
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public ListResultDto<JobsListMobileDto> GetJobsListTest(int RecordType)
		{
			long? UserId = AbpSession.UserId;
			var JobResult = new List<JobsListMobileDto>();
			var JobList = new List<JobsListMobileDto>();
			var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
			var InstId = 0;

			using (_unitOfWorkManager.Current.SetTenantId(null))
			{
				InstId = _InstallerRepository
					.GetAll()
					.Where(I => I.UserId == UserId)
					.Select(I => I.Id)
					.FirstOrDefault();
			}

			//using (CurrentUnitOfWork.SetTenantId(7))
			for (int i = 0; i < tenant.Count; i++)
			{
				using (_unitOfWorkManager.Current.SetTenantId(tenant[i].Id))
				{
					//RecordType = 0 ==> Pending,RecordType = 1 ==> History,RecordType = 2 ==> Future Jobs
					var JobData = _jobRepository
					.GetAll()
					.WhereIf(RecordType == 0, J => J.JobStatus_Id == 3)
					.WhereIf(RecordType == 1, J => J.ExpectedInstallDate <= DateTime.Now)
					.WhereIf(RecordType == 2, J => J.ExpectedInstallDate >= DateTime.Now && J.JobStatus_Id != 3)
					.Where(J => J.CreateFor_Id == InstId)
					.ToList();

					JobList = JobData.MapTo<List<JobsListMobileDto>>();
					foreach (var item in JobList)
					{
						DateTime? tempdate = item.ExpectedInstallDate;
						var applicationsTimezone = _settingManager.GetSettingValueForApplication(TimingSettingNames.TimeZone);
						var newdate = _timeZoneConverter.Convert(tempdate, item.TenantId);
						item.ExpectedInstallShortDate = newdate.Value.ToString("dd-MM-yyyy");

						//item.ExpectedInstallShortDate = item.ExpectedInstallDate.ToString("dd-MM-yyyy");
						item.CustomerCompany = _tenantRepository.GetAll().Where(T => T.Id == item.TenantId).Select(T => T.CompanyName).FirstOrDefault();

						//job.TotalPanels = _scanedPanelRepository.GetAll().Where(P => P.Guid == item.Guid).Select(P => P.Id).Count();
						//job.TotalInverters = _scanedInverterRepository.GetAll().Where(P => P.Guid == item.Guid).Select(P => P.Id).Count();
						//job.TotalImages = _scanedPanelRepository.GetAll().Where(P => P.Guid == item.Guid).Select(P => P.Id).Count();
						//job.InstallerSignFlag = item.SignatureImageInst == null || item.SignatureImageInst == "" ? 0 : 1;
						//job.OwnerSignFlag = item.SignatureImageOwner == null || item.SignatureImageOwner == "" ? 0 : 1;

					}
					foreach (var item in JobList)
					{
						JobResult.Add(item);
					}
				}
			}
			return new ListResultDto<JobsListMobileDto>(JobResult.MapTo<List<JobsListMobileDto>>());
		}


		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public JobDetailMobileDto GetJobsDetails(int TenantId, string GuId)
		{
			using (_unitOfWorkManager.Current.SetTenantId(TenantId))
			{
				var PModel = "";
				var IModel = "";
				Guid newGuId = new Guid(GuId);
				JobDetailMobileDto job = new JobDetailMobileDto();

				var jobdetaillist = _jobRepository.GetAll().Where(J => J.Guid == newGuId).FirstOrDefault();

				job.TenantId = jobdetaillist.TenantId;
				job.ProjectNumber = jobdetaillist.ProjectNumber;
				job.FormId = jobdetaillist.FormId;
				job.Guid = jobdetaillist.Guid;
				job.FirstName = jobdetaillist.FirstName;
				job.LastName = jobdetaillist.LastName;
				job.Phone = jobdetaillist.Phone;
				job.EmailId = jobdetaillist.EmailId;
				job.StreetAddress = jobdetaillist.StreetAddress;
				job.StreetCity = jobdetaillist.StreetCity;
				job.StreetState = jobdetaillist.StreetState;
				job.StreetPostCode = jobdetaillist.StreetPostCode;
				job.RoofType = jobdetaillist.RoofType;
				job.HouseType = jobdetaillist.HouseType;
				job.InstallerNote = jobdetaillist.InstallerNote == null ? " " : jobdetaillist.InstallerNote;
				job.ExpectedInstallDate = jobdetaillist.ExpectedInstallDate;
				if (job.ExpectedInstallDate != null)
				{
					DateTime? tempdate = jobdetaillist.ExpectedInstallDate;
					var applicationsTimezone = _settingManager.GetSettingValueForApplication(TimingSettingNames.TimeZone);
					var newdate = _timeZoneConverter.Convert(tempdate, job.TenantId);
					job.ExpectedInstallShortDate = newdate.Value.ToString("dd-MM-yyyy");
					//job.ExpectedInstallShortDate = jobdetaillist.ExpectedInstallDate.ToString("dd-MM-yyyy");
				}
				job.SystemType_Id = jobdetaillist.SystemType_Id;
				job.NMINo = jobdetaillist.NMINo;
				job.JobStatus_Id = jobdetaillist.JobStatus_Id;
				job.SolarType_Id = jobdetaillist.SolarType_Id;
				job.ElectricianSignature = jobdetaillist.ElectricianSignature;
				job.DesignerSignature = jobdetaillist.DesignerSignature;
				job.SignatureImagePickList = jobdetaillist.SignatureImagePickList;
				job.SignatureImageInst = jobdetaillist.SignatureImageInst;
				job.SignatureImageOwner = jobdetaillist.SignatureImageOwner;
				job.Installer_Id = jobdetaillist.Installer_Id;

				using (_unitOfWorkManager.Current.SetTenantId(null))
				{
					job.RoofType_Name = _roofTypeRepository.GetAll().Where(p => p.Id == jobdetaillist.RoofType).Select(p => p.RoofTypeName).FirstOrDefault();
					job.HouseType_Name = _houseTypeRepository.GetAll().Where(p => p.Id == jobdetaillist.HouseType).Select(p => p.HouseTypeName).FirstOrDefault();
					job.SystemType_Name = _systemTypeRepository.GetAll().Where(p => p.Id == jobdetaillist.SystemType_Id).Select(p => p.SystemTypeName).FirstOrDefault();
					job.SolarTypeName = _solarTypeRepository.GetAll().Where(p => p.Id == jobdetaillist.SolarType_Id).Select(p => p.SolarTypeeName).FirstOrDefault();

					PModel = _panelRepository.GetAll().Where(P => P.Id == jobdetaillist.Panel_id).Select(P => P.ModelNo).FirstOrDefault();
					IModel = _inverterRepository.GetAll().Where(I => I.Id == jobdetaillist.Inverter_id).Select(I => I.ModelNumber).FirstOrDefault();
				}

				job.PanelSerialNo = _scanedPanelRepository.GetAll().Where(P => P.Guid == jobdetaillist.Guid).Select(P => P.PanelSerialNumber).ToArray<string>();
				job.InverterSerialNo = _scanedInverterRepository.GetAll().Where(P => P.Guid == jobdetaillist.Guid).Select(P => P.InverterSerialNumber).ToArray<string>();
				job.InstallerSignFlag = jobdetaillist.SignatureImageInst == null || jobdetaillist.SignatureImageInst == "" ? 0 : 1;
				job.OwnerSignFlag = jobdetaillist.SignatureImageOwner == null || jobdetaillist.SignatureImageOwner == "" ? 0 : 1;
				job.ElectricianSignFlag = jobdetaillist.ElectricianSignature == null || jobdetaillist.ElectricianSignature == "" ? 0 : 1;
				job.DesignerSignFlag = jobdetaillist.DesignerSignature == null || jobdetaillist.DesignerSignature == "" ? 0 : 1;
				job.PickListSignFlag = jobdetaillist.SignatureImagePickList == null || jobdetaillist.SignatureImagePickList == "" ? 0 : 1;
				job.EFTReceiptFlag = _photoRepository.GetAll().Where(P => P.FileType == 1 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.FrontOfHouseFlag = _photoRepository.GetAll().Where(P => P.FileType == 4 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.DCIsolatorFlag = _photoRepository.GetAll().Where(P => P.FileType == 9 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.InstallationSketchFlag = _photoRepository.GetAll().Where(P => P.FileType == 7 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.InverterInstalledFlag = _photoRepository.GetAll().Where(P => P.FileType == 6 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.MtBoxAfterInstFlag = _photoRepository.GetAll().Where(P => P.FileType == 3 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.MtBoxBeforeInstFlag = _photoRepository.GetAll().Where(P => P.FileType == 2 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.PanelOnRoofFlag = _photoRepository.GetAll().Where(P => P.FileType == 8 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.SwitchBoxOpenedFlag = _photoRepository.GetAll().Where(P => P.FileType == 5 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.PaymentFlag = _photoRepository.GetAll().Where(P => P.FileType == 10 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.SysSplitFlag = _photoRepository.GetAll().Where(P => P.FileType == 11 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.OthersFlag = _photoRepository.GetAll().Where(P => P.FileType == 12 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.OldSystemPhotoFlag = _photoRepository.GetAll().Where(P => P.FileType == 18 && P.Guid == jobdetaillist.Guid).Any() == false ? 0 : 1;
				job.SMSSentStatus = _tokenRepository.GetAll().Where(P => P.GuId == jobdetaillist.Guid && P.IsSent == true).Select(P => P.Id).FirstOrDefault() == 0 ? 0 : 1;
				job.TotalPanels = _scanedPanelRepository.GetAll().Where(P => P.Guid == jobdetaillist.Guid).Select(P => P.Id).Count();
				job.TotalInverters = _scanedInverterRepository.GetAll().Where(P => P.Guid == jobdetaillist.Guid).Select(P => P.Id).Count();
				job.NotScannedPanels = jobdetaillist.NoOfPanels - _scanedPanelRepository.GetAll().Where(P => P.Guid == jobdetaillist.Guid).Select(P => P.Id).Count();
				job.NotScannedInverters = Convert.ToInt32(jobdetaillist.NoOfInverter) - _scanedInverterRepository.GetAll().Where(P => P.Guid == jobdetaillist.Guid).Select(P => P.Id).Count();
				job.CustomerCompanyName = _tenantRepository.GetAll().Where(T => T.Id == jobdetaillist.TenantId).Select(T => T.FullName).FirstOrDefault();

				job.OwnerCommentFlag = jobdetaillist.OwnerComment == null ? 0 : 1;
				job.InstCommentFlag = jobdetaillist.InstComment == null ? 0 : 1;


				var PanelsListval = new List<PanelList>();
				PanelsListval.Add(new PanelList { BrandName = jobdetaillist.BrandName, NoOfPanels = jobdetaillist.NoOfPanels, PanelModel = PModel, PanelName = jobdetaillist.PanelName, PanelSize = jobdetaillist.PanelSize, Panel_id = jobdetaillist.Panel_id, SystemCapacity = jobdetaillist.SystemCapacity });//_jobRepository.GetAll().Where(j => j.Id == JobId).FirstOrDefault(); //GetAsync(JobId)).MapTo<PanelList>();
				job.PanelList = PanelsListval;


				var InvertersListval = new List<InverterList>();
				InvertersListval.Add(new InverterList { InverterBrandName = jobdetaillist.InverterBrandName, InverterModel = IModel, InverterName = jobdetaillist.InverterName, InverterOutput = jobdetaillist.InverterOutput, InverterSeries = jobdetaillist.InverterSeries, Inverter_id = jobdetaillist.Inverter_id, NoOfInverter = jobdetaillist.NoOfInverter });//_jobRepository.GetAll().Where(j => j.Id == JobId).FirstOrDefault(); //GetAsync(JobId)).MapTo<PanelList>();
				job.InverterList = InvertersListval;

				return job;
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task UpdateJobStatus(UpdateJobStatus input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				Guid newGuId = new Guid(input.GuId);
				var JobId = _jobRepository.GetAll().Where(J => J.Guid == newGuId).FirstOrDefault();
				JobId.JobStatus_Id = input.JobStatus;
				await _jobRepository.UpdateAsync(JobId);
			}
		}
		#endregion

		#region Customer Sign by SMS
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task CustomerSignSMS(int TenantId, string GuId)
		{
			using (_unitOfWorkManager.Current.SetTenantId(TenantId))
			{
				Guid newGuId = new Guid(GuId);
				long? UserId = AbpSession.UserId;
				var jobdetail = _jobRepository.GetAll().Where(J => J.Guid == newGuId).FirstOrDefault();
				string Token = "";
				Guid guid = System.Guid.NewGuid();
				Token = RandomStrongToken();
				var token = new Token();
				token.TokenNo = Token;
				token.GuId = guid;
				token.IsExpired = false;
				token.IsOwner = true;
				token.IsSent = true;
				token.JobId = jobdetail.Id;
				token.JobGuId = newGuId;

				var tokendata = await _tokenRepository.InsertAndGetIdAsync(token);

				var CustomerSignLog = new CustomerSignLog();

				CustomerSignLog.TokenGuid = token.GuId;
				CustomerSignLog.Token = token.TokenNo;
				CustomerSignLog.TokenDate = token.CreationTime;
				CustomerSignLog.SenderUserId = Convert.ToInt32(UserId);
				CustomerSignLog.SendBy = "SMS";
				CustomerSignLog.JobId = token.JobId;
				CustomerSignLog.JobGuId = token.JobGuId;
				await _customerSignLogRepository.InsertAsync(CustomerSignLog);

				var job = await _jobRepository.GetAsync(jobdetail.Id);
				job.RequestSignatureImageOwnerDate = DateTime.Now;
				await _jobRepository.UpdateAsync(job);
			}
		}

		public static String RandomStrongToken()
		{
			String pwd = "";
			int length = 0;
			int index = 0;
			int numericIndex = -1;
			int upperCaseIndex = -1;
			Random rnd = new Random();
			const int MINLENGTH = 8;
			const int MAXLENGTH = 8;
			const String allowedChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ@$*";
			const String upperCaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			const String numericChars = "0123456789";
			//Length of your password
			length = rnd.Next(MINLENGTH, MAXLENGTH);

			// You generate a password of the desired length
			for (int i = 0; i < length; i++)
			{
				// Generate an index that smaller than the size of your allowed chars
				index = rnd.Next(0, allowedChars.Length);

				pwd += allowedChars[index];
			}

			////*********************************************************
			// We make sure that there is at least one numeric
			// Replace one random char by a numeric
			numericIndex = rnd.Next(0, pwd.Length);

			// Generate a numeric, delete one char and replace it with a numeric
			index = rnd.Next(0, numericChars.Length);
			pwd = pwd.Remove(numericIndex, 1);
			pwd = pwd.Insert(numericIndex, numericChars[index].ToString());
			////*********************************************************


			////*********************************************************
			// We make sure that there is at least one uppercase
			// Replace one random char by a numeric
			upperCaseIndex = rnd.Next(0, pwd.Length);

			// We make sure our uppercase index is different
			// from our numeric index or we will overwrite our
			// only numeric value with our uppercase value
			while (upperCaseIndex == numericIndex)
			{
				upperCaseIndex = rnd.Next(0, pwd.Length);
			}

			// Generate a numeric, delete one char and replace it with a numeric
			index = rnd.Next(0, upperCaseChars.Length);
			pwd = pwd.Remove(upperCaseIndex, 1);
			pwd = pwd.Insert(upperCaseIndex, upperCaseChars[index].ToString());
			////*********************************************************

			return pwd;
		}
		#endregion

		#region Insert Images(1-12,18)
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertEFTReceipt(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "EFTReceipt_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 1;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertMtBoxBeforeInst(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "MTBeforeInstall_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 2;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertMtBoxAfterInst(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "MTAfterInstall_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 3;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertFrontOfHouse(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "FrontofHouse_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 4;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertSwitchBoxOpened(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "SwitchBoardOpened_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 5;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertInverterInstalled(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "TheInverterInstalled_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 6;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertInstallationSketch(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "InstallationSketch_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 7;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertPanelOnRoof(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "PanelInstalledonRoof_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 8;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertDCIsolator(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "DCIsolatorinstalled_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 9;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertPayment(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "Payment_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 10;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertSysSplit(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "SysSplit_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 11;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertOthers(ImageInputs input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "Others_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}

							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();
							var EFT = new Photo();
							EFT.FileType = 12;
							EFT.TenantId = input.TenantId;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertOldSystemPhotos(OldSystemImageInput input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);
					foreach (var item in input.ImageData)
					{
						if (input.ImageData.Length > 0)
						{
							//(from data in db.tbl_PVForm where data.GuID == newGuId select data.FormId).FirstOrDefault().ToString();

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(item);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							string FileName = "OldSystemPhoto_" + FormId + DateTime.Now.Ticks.ToString() + ".jpeg";
							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\Photos\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + "\\" + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}


							GC.Collect();

							var EFT = new Photo();
							EFT.FileType = 18;
							EFT.TenantId = input.TenantId;
							EFT.OldSystemDirection = input.OldSystemDirection;
							EFT.Guid = newGuId;
							EFT.ImageGuid = System.Guid.NewGuid();
							EFT.ImageName = FileName;
							EFT.Longitude = input.Longitude;
							EFT.Latitude = input.Latitude;
							await _photoRepository.InsertAsync(EFT);
						}
					}
				}
			}
		}
		#endregion

		#region Get Images(1-12,18)
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetEFTReceipt(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 1)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetMtBoxBeforeInst(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 2)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetMtBoxAfterInst(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 3)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetFrontOfHouse(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 4)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetSwitchBoxOpened(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 5)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetInverterInstalled(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 6)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetInstallationSketch(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 7)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetPanelOnRoof(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 8)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetDCIsolator(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 9)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetPayment(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 10)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetSysSplit(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 11)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetImagesDto GetOthers(GetImageDetail input)
		{
			var List = new GetImagesDto();
			var ImageList = new List<ImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 12)
					.ToList();
				ImageList = ResultData.MapTo<List<ImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.ImageDataList = ImageList;
			List.TotalCount = ImageList.Count;

			return List;
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetOldSystemImagesDto GetOldSystemPhotos(GetImageDetail input)
		{
			var List = new GetOldSystemImagesDto();
			var ImageList = new List<OldImageDataList>();
			Guid newGuid = new Guid(input.GuId);
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _photoRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.Where(P => P.FileType == 18)
					.ToList();
				ImageList = ResultData.MapTo<List<OldImageDataList>>();
			}
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			foreach (var item in ImageList)
			{
				item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/Photos/" + item.ImageName;
				item.ImagePath = RemoveTenantName(item.ImagePath);
			}
			List.OldImageDataList = ImageList;
			List.TotalCount = ImageList.Count;
			//Add(new GetImagesDto { ImageDataList = ImageList, TotalCount = ImageList.Count });

			return List;
		}
		#endregion

		#region Delete Images
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DeletePhotos(DeleteImage input)
		{
			if (input.GuId.Count() > 0)
			{
				var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					for (int i = 0; i < input.GuId.Count(); i++)
					{
						Guid newGuId = new Guid(input.GuId[i]);
						string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();

						var ResultData = _photoRepository
						.GetAll()
						.Where(P => P.ImageGuid == newGuId)
						.FirstOrDefault();
						if (!String.IsNullOrEmpty(ResultData.ImageName))
						{
							string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp/" + TenantName + "/" + FormId + "/Photos/" + ResultData.ImageName;
							System.IO.File.Delete(filepath);
						}
						await _photoRepository.DeleteAsync(ResultData.Id);
					}
				}
			}
		}
		#endregion

		#region upload  Installer Signature, delete and Get Details
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DeleteInstallerSignature(InstallerSignatureRequest input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var job = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
				#region Delete  Installer Signature  amd Photo
				if (!String.IsNullOrEmpty(job.SignatureImageInst))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + job.FormId + "\\InstallerPhoto\\" + job.SignatureImageInst;
					System.IO.File.Delete(filepath);
				}
				if (!String.IsNullOrEmpty(job.InstallerPhoto))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + job.FormId + "\\InstallerPhoto\\" + job.InstallerPhoto;
					System.IO.File.Delete(filepath);
				}
				job.SignatureImageInst = null;
				job.InstallerPhoto = null;
				await _jobRepository.UpdateAsync(job);
				#endregion
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public ListResultDto<GetInstallerSignatureWithLocation> GetInstallerSignature(InstallerSignatureRequest input)
		{
			var List = new List<GetInstallerSignatureWithLocation>();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{

				var ResultData = _jobRepository
					.GetAll()
				   .WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			).ToList();
				List = ResultData.MapTo<List<GetInstallerSignatureWithLocation>>();
			}
			foreach (var item in List)
			{
				if (item.SignatureImageInst != null)
				{
					item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + item.FormId + "/InstallerSignature/" + item.SignatureImageInst;
					//item.ImagePath = RemoveTenantName(item.ImagePath);
					item.ImagePath = AppendTenantName(item.TenantId, item.ImagePath);
				}
			}
			return new ListResultDto<GetInstallerSignatureWithLocation>(List.MapTo<List<GetInstallerSignatureWithLocation>>());
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InstallerSignatureWithLocation(ImageInputsWithLocation input)
		{
			string Installerphoto = "";
			string InstallerSignature = "";
			Installerphoto = "InstallerPhoto_" + DateTime.Now.Ticks + "." + input.Ext;
			InstallerSignature = "InstallerSignature_" + DateTime.Now.Ticks + "." + "jpeg";

			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{

				var installerPhotoExist = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				#region upload Installer Photo
				string FormId = _jobRepository.GetAll().Where(p => p.Guid == input.Guid).Select(P => P.FormId).FirstOrDefault();
				var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
				if (!String.IsNullOrEmpty(installerPhotoExist.InstallerPhoto))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + FormId + "\\InstallerPhoto\\" + installerPhotoExist.InstallerPhoto;
					System.IO.File.Delete(filepath);
				}
				if (input.ImageData.Length > 0)
				{

					//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\InstallerPhoto\\" + Installerphoto;
					//using (MemoryStream mStream = new MemoryStream(input.ImageData))
					//{
					//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
					//}
					//GC.Collect();
					
					string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
					if (System.IO.Directory.Exists(Path))
					{
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\InstallerPhoto\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								var FinalFileName = FinalPath + Installerphoto;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + Installerphoto;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\InstallerPhoto\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								var FinalFileName = FinalPath + Installerphoto;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + Installerphoto;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(Path);
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\InstallerPhoto\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + Installerphoto;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\InstallerPhoto\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + Installerphoto;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
					//return "Sucess";


				}
				#endregion

				#region upload INstaller Signature
				if (!String.IsNullOrEmpty(installerPhotoExist.SignatureImageInst))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + FormId + "\\InstallerSignature\\" + installerPhotoExist.SignatureImageInst;
					System.IO.File.Delete(filepath);
				}
				if (input.FileData.Length > 0)
				{

					//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\InstallerSignature\\" + InstallerSignature;
					//using (MemoryStream mStream = new MemoryStream(input.FileData))
					//{
					//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.FileData);
					//}
					//GC.Collect();
					string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
					if (System.IO.Directory.Exists(Path))
					{
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\InstallerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								var FinalFileName = FinalPath + InstallerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + InstallerSignature;

								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\InstallerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + InstallerSignature;
									
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(Path);
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\InstallerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + InstallerSignature;

								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\InstallerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + InstallerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
				}
				#endregion

				#region Savedata
				var job = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				job.SignatureImageInst = InstallerSignature;
				job.SignatureImageInstDate = DateTime.Now;
				job.InstallerPhoto = Installerphoto;
				job.Longitude_InstSign = input.Longitude;
				job.Latitude_InstSign = input.Latitude;
				await _jobRepository.UpdateAsync(job);
				#endregion
			}
		}
		#endregion

		#region upload  Electrician Signature, delete and Get Details
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task ElectricianSignatureWithLocation(ImageInputsWithLocation input)
		{
			string ElectricianSignature = "ElectricianSignature_" + DateTime.Now.Ticks + ".jpeg";
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var Exist = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				#region upload Electrician Signature
				//Delete Signature from folder
				string FormId = _jobRepository.GetAll().Where(p => p.Guid == input.Guid).Select(P => P.FormId).FirstOrDefault();
				var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
				try
				{
					if (!String.IsNullOrEmpty(Exist.ElectricianSignature))
					{
						string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + FormId + "\\ElectricianSignature\\" + Exist.ElectricianSignature;
						System.IO.File.Delete(filepath);
					}
				}
				catch { }

				if (input.FileData.Length > 0)
				{
					//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\ElectricianSignature\\" + ElectricianSignature;
					//using (MemoryStream mStream = new MemoryStream(input.FileData))
					//{
					//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.FileData);
					//}
					//GC.Collect();
					
					string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
					if (System.IO.Directory.Exists(Path))
					{
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\ElectricianSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								var FinalFileName = FinalPath + ElectricianSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + ElectricianSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\ElectricianSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + ElectricianSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(Path);
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\ElectricianSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + ElectricianSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\ElectricianSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + "\\" + ElectricianSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
				}
				#endregion
				#region Savedata
				var job = _jobRepository.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				job.ElectricianSignature = ElectricianSignature;
				job.ElectricianSignatureDate = DateTime.Now;
				job.Latitude_ElecSign = input.Latitude;
				job.Longitude_ElecSign = input.Longitude;
				await _jobRepository.UpdateAsync(job);
				#endregion

			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public ListResultDto<GetElectricianSignatureWithSignature> GetElectricianSignature(InstallerSignatureRequest input)
		{
			var List = new List<GetElectricianSignatureWithSignature>();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{

				var ResultData = _jobRepository
					.GetAll()
				   .WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			).ToList();

				List = ResultData.MapTo<List<GetElectricianSignatureWithSignature>>();
			}
			foreach (var item in List)
			{
				if (item.ElectricianSignature != null)
				{
					item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + item.FormId + "/ElectricianSignature/" + item.ElectricianSignature;
					//item.ImagePath = RemoveTenantName(item.ImagePath);
					item.ImagePath = AppendTenantName(item.TenantId, item.ImagePath);
				}
			}
			return new ListResultDto<GetElectricianSignatureWithSignature>(List.MapTo<List<GetElectricianSignatureWithSignature>>());
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DeleteElectricianSignature(InstallerSignatureRequest input)
		{
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var job = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				#region Delete  Electrician Signature 
				if (!String.IsNullOrEmpty(job.ElectricianSignature))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + job.FormId + "\\ElectricianSignature\\" + job.ElectricianSignature;
					System.IO.File.Delete(filepath);
				}

				job.ElectricianSignature = null;
				await _jobRepository.UpdateAsync(job);
				#endregion
			}
		}
		#endregion

		#region upload  Designer Signature, delete and Get Details
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DesignerSignatureWithLocation(ImageInputsWithLocation input)
		{
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			string DesignerSignature = "DesignerSignature_" + DateTime.Now.Ticks + ".jpeg";
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var Exist = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				#region upload Designer Signature
				//Delete Signature from folder
				if (!String.IsNullOrEmpty(Exist.DesignerSignature))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + Exist.FormId + "\\DesignerSignature\\" + Exist.DesignerSignature;
					System.IO.File.Delete(filepath);
				}
				if (input.FileData.Length > 0)
				{
					//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\DesignerSignature\\" + DesignerSignature;
					//using (MemoryStream mStream = new MemoryStream(input.FileData))
					//{
					//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.FileData);
					//}
					//GC.Collect();
					string FormId = _jobRepository.GetAll().Where(p => p.Guid == input.Guid).Select(P => P.FormId).FirstOrDefault();
					string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
					if (System.IO.Directory.Exists(Path))
					{
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\DesignerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								var FinalFileName = FinalPath + DesignerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + DesignerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\DesignerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + DesignerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(Path);
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\DesignerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + DesignerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\DesignerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + DesignerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
				}
				#endregion
				#region Savedata
				var job = _jobRepository.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				job.DesignerSignature = DesignerSignature;
				job.DesignerSignatureDate = DateTime.Now;
				job.Latitude_DesiSign = input.Latitude;
				job.Longitude_DesiSign = input.Longitude;
				await _jobRepository.UpdateAsync(job);
				#endregion

			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DeleteDesignerSignature(InstallerSignatureRequest input)
		{
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var job = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				#region Delete  Designer Signature 
				if (!String.IsNullOrEmpty(job.DesignerSignature))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + job.FormId + "\\DesignerSignature\\" + job.DesignerSignature;
					System.IO.File.Delete(filepath);
				}

				job.DesignerSignature = null;
				await _jobRepository.UpdateAsync(job);
				#endregion
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public ListResultDto<GetDesignerSignatureWithLocation> GetDesignerSignature(InstallerSignatureRequest input)
		{
			var List = new List<GetDesignerSignatureWithLocation>();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{

				var ResultData = _jobRepository
					.GetAll()
				   .WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			).ToList();
				List = ResultData.MapTo<List<GetDesignerSignatureWithLocation>>();
			}
			foreach (var item in List)
			{
				if (item.DesignerSignature != null)
				{
					item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + item.FormId + "/DesignerSignature/" + item.DesignerSignature;
					//item.ImagePath = RemoveTenantName(item.ImagePath);
					item.ImagePath = AppendTenantName(item.TenantId, item.ImagePath);
				}
			}
			return new ListResultDto<GetDesignerSignatureWithLocation>(List.MapTo<List<GetDesignerSignatureWithLocation>>());
		}
		#endregion

		#region upload  Owner Signature, delete and Get Details
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task OwnerSignatureWithLocation(ImageInputsWithLocation input)
		{
			string Ownerphoto = "ownerPhoto_" + DateTime.Now.Ticks + "." + input.Ext;
			string OwnerSignature = "ownerSignature_" + DateTime.Now.Ticks + "." + "jpeg";
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var PhotoExist = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				#region upload Owner Photo
				if (!String.IsNullOrEmpty(PhotoExist.OwnerPhoto))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + PhotoExist.FormId + "\\OwnerPhoto\\" + PhotoExist.OwnerPhoto;
					System.IO.File.Delete(filepath);
				}
				if (input.ImageData.Length > 0)
				{

					//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\OwnerPhoto\\" + Ownerphoto;
					//using (MemoryStream mStream = new MemoryStream(input.ImageData))
					//{
					//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
					//}
					//GC.Collect();
					
					string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
					if (System.IO.Directory.Exists(Path))
					{
						string FormIdPath = Path + "\\" + PhotoExist.FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\OwnerPhoto\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								var FinalFileName = FinalPath + Ownerphoto;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + Ownerphoto;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\OwnerPhoto\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + Ownerphoto;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(Path);
						string FormIdPath = Path + "\\" + PhotoExist.FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\OwnerPhoto\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + Ownerphoto;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\OwnerPhoto\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + Ownerphoto;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
					//return "Sucess";
				}
				#endregion
				#region upload Owner Signature
				if (!String.IsNullOrEmpty(PhotoExist.SignatureImageInst))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + PhotoExist.FormId + "\\OwnerSignature\\" + PhotoExist.SignatureImageInst;
					System.IO.File.Delete(filepath);
				}
				if (input.FileData.Length > 0)
				{
					//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + item.FormId + "\\OwnerSignature\\" + OwnerSignature;
					//using (MemoryStream mStream = new MemoryStream(input.FileData))
					//{
					//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.FileData);
					//}
					//GC.Collect();
					string FormId = _jobRepository.GetAll().Where(p => p.Guid == input.Guid).Select(P => P.FormId).FirstOrDefault();
					
					string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
					if (System.IO.Directory.Exists(Path))
					{
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\OwnerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								var FinalFileName = FinalPath + OwnerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + OwnerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\OwnerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + OwnerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(Path);
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\OwnerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + OwnerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\OwnerSignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + OwnerSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
					//return "Sucess";
				}
				#endregion

				#region Savedata
				var job = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				job.SignatureImageOwner = OwnerSignature;
				job.SignatureImageOwnerDate = DateTime.Now;
				job.OwnerPhoto = Ownerphoto;
				job.Longitude_CustSign = input.Longitude;
				job.Latitude_CustSign = input.Latitude;
				await _jobRepository.UpdateAsync(job);
				#endregion

			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public ListResultDto<GetOwanerSignatureWithLocation> GetownerSignature(InstallerSignatureRequest input)
		{
			var List = new List<GetOwanerSignatureWithLocation>();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{

				var ResultData = _jobRepository
					.GetAll()
				   .WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			).ToList();
				List = ResultData.MapTo<List<GetOwanerSignatureWithLocation>>();
			}
			foreach (var item in List)
			{
				if (item.SignatureImageOwner != null)
				{
					item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + item.FormId + "/OwnerPhoto/" + item.SignatureImageOwner;
					//item.ImagePath = RemoveTenantName(item.ImagePath);
					item.ImagePath = AppendTenantName(item.TenantId, item.ImagePath);
				}
			}
			return new ListResultDto<GetOwanerSignatureWithLocation>(List.MapTo<List<GetOwanerSignatureWithLocation>>());
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DeleteOwnerSignature(InstallerSignatureRequest input)
		{
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var job = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				#region Delete  Owner Signature 
				if (!String.IsNullOrEmpty(job.OwnerPhoto))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + job.FormId + "\\ownerSignature\\" + job.OwnerPhoto;
					System.IO.File.Delete(filepath);
				}
				job.OwnerPhoto = null;
				job.SignatureImageOwner = null;
				await _jobRepository.UpdateAsync(job);
				#endregion
			}
		}
		#endregion

		#region Upload Picklist Image,Get and Delete
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task SignatureImagePickListWithLocation(ImageInputsWithLocation input)
		{
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			string PicklistSignature = "picklistSignature_" + DateTime.Now.Ticks + "." + "jpeg";
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var Exist = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				#region upload Owner Photo
				if (!String.IsNullOrEmpty(Exist.SignatureImagePickList))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + Exist.FormId + "\\Picklistsignature\\" + Exist.SignatureImagePickList;
					System.IO.File.Delete(filepath);
				}
				if (input.ImageData.Length > 0)
				{
					//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Picklistsignature\\" + PicklistSignature;
					//using (MemoryStream mStream = new MemoryStream(input.ImageData))
					//{
					//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
					//}
					//GC.Collect();
					string FormId = _jobRepository.GetAll().Where(p => p.Guid == input.Guid).Select(P => P.FormId).FirstOrDefault();
					
					string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
					if (System.IO.Directory.Exists(Path))
					{
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\Picklistsignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								var FinalFileName = FinalPath + PicklistSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + PicklistSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\Picklistsignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + PicklistSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(Path);
						string FormIdPath = Path + "\\" + FormId;
						if (System.IO.Directory.Exists(FormIdPath))
						{
							string FinalPath = FormIdPath + "\\Picklistsignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + PicklistSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
						else
						{
							System.IO.Directory.CreateDirectory(FormIdPath);
							string FinalPath = FormIdPath + "\\Picklistsignature\\";
							if (System.IO.Directory.Exists(FinalPath))
							{
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(FinalPath);
								FinalPath = FinalPath + PicklistSignature;
								using (MemoryStream mStream = new MemoryStream(input.ImageData))
								{
									System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), input.ImageData);
								}
							}
						}
					}
					//return "Sucess";


				}
				#endregion
				#region Savedata
				var job = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				job.SignatureImagePickList = PicklistSignature;
				job.SignatureImaegePickListDate = DateTime.Now;
				job.Longitude_PickList = input.Longitude;
				job.Latitude_PickList = input.Latitude;
				await _jobRepository.UpdateAsync(job);
				#endregion

			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public ListResultDto<GetPickListSignatureWithLocation> GetSignatureImagePickList(InstallerSignatureRequest input)
		{
			var List = new List<GetPickListSignatureWithLocation>();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{

				var ResultData = _jobRepository
					.GetAll()
				   .WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			).ToList();
				List = ResultData.MapTo<List<GetPickListSignatureWithLocation>>();

				foreach (var item in List)
				{
					if (item.SignatureImagePickList != null)
					{
						item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + item.FormId + "/SignatureImagePickList/" + item.SignatureImagePickList;
						//item.ImagePath = RemoveTenantName(item.ImagePath);
						item.ImagePath = AppendTenantName(item.TenantId, item.ImagePath);
					}
				}
				return new ListResultDto<GetPickListSignatureWithLocation>(List.MapTo<List<GetPickListSignatureWithLocation>>());
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DeleteSignatureImagePickList(InstallerSignatureRequest input)
		{
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var job = _jobRepository
			.GetAll()
			.WhereIf(
				input.Guid != null,
				p => p.Guid == input.Guid
			)
			.FirstOrDefault();
				#region Delete  Picklist Signature 
				if (!String.IsNullOrEmpty(job.SignatureImagePickList))
				{
					string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + job.FormId + "\\Picklistsignature\\" + job.SignatureImagePickList;
					System.IO.File.Delete(filepath);
				}
				job.SignatureImagePickList = null;
				await _jobRepository.UpdateAsync(job);
				#endregion
			}
		}
		#endregion

		#region Panel & Inverter Scanned Images
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertScanedPanels(ScanedImageInput input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					int count = 0;
					foreach (var item in input.ImageData)
					{
						//try
						//{
						//var Exist = _scanedPanelRepository.GetAll().Where(P => P.PanelSerialNumber == item).Any();
						//if (!Exist)
						//{
						if (input.ImageData.Length > 0)
						{
							string FileName = "ScanedPanel_" + DateTime.Now.Ticks.ToString() + ".jpeg";

							//string test = Convert.ToString(input.ImageData[count]);
							//test = test.Replace(" ", "+");

							var buffer = new byte[2097152];
							//byte[] ByteArray = Convert.FromBase64String(test);
							byte[] ByteArray = Convert.FromBase64String(input.ImageData[count]);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\ScanedPanels\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}
							//GC.Collect();
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\ScanedPanels\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\ScanedPanels\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\ScanedPanels\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\ScanedPanels\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							try
							{
								var ScanedPanels = new ScanedPanel();
								ScanedPanels.TenantId = input.TenantId;
								ScanedPanels.Guid = newGuId;
								ScanedPanels.PanelBarcodePhoto = FileName;
								ScanedPanels.PanelSerialNumber = input.SerialNo[count];
								ScanedPanels.Longitude = input.Longitude;
								ScanedPanels.Latitude = input.Latitude;
								await _scanedPanelRepository.InsertAsync(ScanedPanels);
							}
							catch (Exception e)
							{

							}
						}
						//}
						count++;
						//}
						//catch (Exception e)
						//{

						//}
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task InsertScanedInverters(ScanedImageInput input)
		{
			if (input.GuId != null)
			{
				using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
				{
					Guid newGuId = new Guid(input.GuId);

					int count = 0;
					foreach (var item in input.ImageData)
					{
						//var Exist = _scanedInverterRepository.GetAll().Where(P => P.InverterSerialNumber == item).Any();
						//if (!Exist)
						//{
						if (input.ImageData.Length > 0)
						{
							string FileName = "ScanedInverter_" + DateTime.Now.Ticks.ToString() + ".jpeg";

							var buffer = new byte[2097152];
							byte[] ByteArray = Convert.FromBase64String(input.ImageData[count]);
							//string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\ScanedInverters\\" + FileName;
							//using (MemoryStream mStream = new MemoryStream(ByteArray))
							//{
							//	System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
							//}
							//GC.Collect();
							string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuId).Select(P => P.FormId).FirstOrDefault();
							var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
							string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
							if (System.IO.Directory.Exists(Path))
							{
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\ScanedInverters\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										var FinalFileName = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalFileName), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\ScanedInverters\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							else
							{
								System.IO.Directory.CreateDirectory(Path);
								string FormIdPath = Path + "\\" + FormId;
								if (System.IO.Directory.Exists(FormIdPath))
								{
									string FinalPath = FormIdPath + "\\ScanedInverters\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
								else
								{
									System.IO.Directory.CreateDirectory(FormIdPath);
									string FinalPath = FormIdPath + "\\ScanedInverters\\";
									if (System.IO.Directory.Exists(FinalPath))
									{
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
									else
									{
										System.IO.Directory.CreateDirectory(FinalPath);
										FinalPath = FinalPath + FileName;
										using (MemoryStream mStream = new MemoryStream(ByteArray))
										{
											System.IO.File.WriteAllBytes(string.Format("{0}", FinalPath), ByteArray);
										}
									}
								}
							}
							var ScanedInverters = new ScanedInverter();
							ScanedInverters.TenantId = input.TenantId;
							ScanedInverters.Guid = newGuId;
							ScanedInverters.InverterBarcodePhoto = FileName;
							ScanedInverters.InverterSerialNumber = input.SerialNo[count];
							ScanedInverters.Longitude = input.Longitude;
							ScanedInverters.Latitude = input.Latitude;
							await _scanedInverterRepository.InsertAsync(ScanedInverters);
						}
						//}
						count++;
					}
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetScanedPanelDto GetScanedPanels(GetImageDetail input)
		{
			Guid newGuid = new Guid(input.GuId);
			GetScanedPanelDto ListData = new GetScanedPanelDto();
			var ScannedPanelList = new List<ScanedPanelList>();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _scanedPanelRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.ToList();
				ScannedPanelList = ResultData.MapTo<List<ScanedPanelList>>();

				foreach (var item in ScannedPanelList)
				{
					if (item.PanelBarcodePhoto != null)
					{
						item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/ScanedPanels/" + item.PanelBarcodePhoto;
						//item.ImagePath = RemoveTenantName(item.ImagePath);
						item.ImagePath = AppendTenantName(item.TenantId, item.ImagePath);
					}
				}
				ListData.PanelList = ScannedPanelList;//.Add(new GetScanedPanelDto { PanelList = ScannedPanelList, TotalCount = ScannedPanelList.Count });
				ListData.TotalCount = _jobRepository.GetAll().Where(J => J.Guid == newGuid).Select(J => J.NoOfPanels).FirstOrDefault(); //ScannedPanelList.Count;
				return ListData;
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public GetScanedInverterDto GetScanedInverters(GetImageDetail input)
		{
			Guid newGuid = new Guid(input.GuId);
			GetScanedInverterDto List = new GetScanedInverterDto();
			var ScannedInverterList = new List<ScannedInverterList>();
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			string FormId = _jobRepository.GetAll().Where(p => p.Guid == newGuid).Select(P => P.FormId).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				var ResultData = _scanedInverterRepository
					.GetAll()
					.Where(P => P.Guid == newGuid)
					.ToList();

				ScannedInverterList = ResultData.MapTo<List<ScannedInverterList>>();

				foreach (var item in ScannedInverterList)
				{
					if (item.InverterBarcodePhoto != null)
					{
						item.ImagePath = WebUrlService.WebSiteRootAddress + "Temp/" + TenantName + "/" + FormId + "/ScanedInverters/" + item.InverterBarcodePhoto;
						//item.ImagePath = RemoveTenantName(item.ImagePath);
						item.ImagePath = AppendTenantName(item.TenantId, item.ImagePath);
					}
				}
				List.InverterList = ScannedInverterList; //.Add(new GetScanedInverterDto { InverterList = ScannedInverterList, TotalCount = ScannedInverterList.Count });
				List.TotalCount = Convert.ToInt32(_jobRepository.GetAll().Where(J => J.Guid == newGuid).Select(J => J.NoOfInverter).FirstOrDefault()); //ScannedInverterList.Count;
				return List;
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DeleteScanedPanels(DeleteScanedImages input)
		{
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				for (int i = 0; i < input.Id.Count(); i++)
				{
					var Panel = _scanedPanelRepository
					.GetAll()
					.WhereIf(
					 input.Id[i] != 0,
					 p => p.Id == input.Id[i]
					)
					.FirstOrDefault();
					string FormId = _jobRepository.GetAll().Where(p => p.Guid == Panel.Guid).Select(P => P.FormId).FirstOrDefault();
					#region Delete  Owner Signature 
					if (!String.IsNullOrEmpty(Panel.PanelBarcodePhoto))
					{
						string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + FormId + "\\ScanedPanels\\" + Panel.PanelBarcodePhoto;
						System.IO.File.Delete(filepath);
					}

					await _scanedPanelRepository.DeleteAsync(input.Id[i]);
				}
				#endregion
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task DeleteScanedInverters(DeleteScanedImages input)
		{
			var TenantName = _tenantRepository.GetAll().Where(e => e.Id == input.TenantId).Select(e => e.TenancyName).FirstOrDefault();

			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				for (int i = 0; i < input.Id.Count(); i++)
				{
					var Inverter = _scanedInverterRepository
					.GetAll()
					.WhereIf(
					 input.Id[i] != 0,
					 p => p.Id == input.Id[i]
					)
					.FirstOrDefault();
					string FormId = _jobRepository.GetAll().Where(p => p.Guid == Inverter.Guid).Select(P => P.FormId).FirstOrDefault();
					#region Delete  Owner Signature 
					if (!String.IsNullOrEmpty(Inverter.InverterBarcodePhoto))
					{
						string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName + "\\" + FormId + "\\ScanedInverters\\" + Inverter.InverterBarcodePhoto;
						System.IO.File.Delete(filepath);
					}

					await _scanedInverterRepository.DeleteAsync(input.Id[i]);
				}
				#endregion
			}
		}
		#endregion

		#region Comment Section
		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task AddInstallerComment(CommentDto input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				Guid newGuId = new Guid(input.Guid);
				var JobId = _jobRepository.GetAll().Where(J => J.Guid == newGuId).FirstOrDefault();
				JobId.InstComment = input.Comment;
				await _jobRepository.UpdateAsync(JobId);
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public ListResultDto<GetInstComment> GetInstallerComment(int TenantId, string GuId)
		{
			using (_unitOfWorkManager.Current.SetTenantId(TenantId))
			{
				Guid newGuid = new Guid(GuId);

				var ResultData = _jobRepository
						.GetAll()
					   .WhereIf(
					newGuid != null,
					p => p.Guid == newGuid
				).ToList();

				return new ListResultDto<GetInstComment>(ResultData.MapTo<List<GetInstComment>>());
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public async Task AddOwnerComment(CommentDto input)
		{
			using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
			{
				if (input.Guid != null)
				{
					Guid newGuid = new Guid(input.Guid);

					var job = _jobRepository
					.GetAll()
					.WhereIf(
						newGuid != null,
						p => p.Guid == newGuid
					)
					.FirstOrDefault();

					job.OwnerComment = input.Comment;
					await _jobRepository.UpdateAsync(job);
				}
			}
		}

		[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public ListResultDto<GetOwnerComment> GetOwanerComment(int TenantId, string GuId)
		{
			using (_unitOfWorkManager.Current.SetTenantId(TenantId))
			{
				Guid newGuid = new Guid(GuId);

				var ResultData = _jobRepository
						.GetAll()
					   .WhereIf(
					newGuid != null,
					p => p.Guid == newGuid
				).ToList();

				return new ListResultDto<GetOwnerComment>(ResultData.MapTo<List<GetOwnerComment>>());
			}
		}
		#endregion

		#region Forgot Password
		//[AbpAuthorize(AppPermissions.Pages_MobileAppAccess)]
		public virtual async Task SendPasswordResetLink(SendPasswordResetLink model)
		{
			var user = await GetUserByChecking(model.EmailAddress);

			user.SetNewPasswordResetCode();
			await _userEmailer.SendPasswordResetLinkAsync(user);

			await UnitOfWorkManager.Current.SaveChangesAsync();
		}

		private async Task<User> GetUserByChecking(string emailAddress)
		{
			var user = await _userManager.Users.Where(
				u => u.EmailAddress == emailAddress
				).FirstOrDefaultAsync();

			if (user == null)
			{
				throw new UserFriendlyException(L("InvalidEmailAddress"));
			}

			return user;
		}
		#endregion

		public string RemoveTenantName(string Url)
		{
			var TenantName = _tenantRepository.GetAll().Where(T => T.Id == AbpSession.TenantId).Select(T => T.TenancyName).FirstOrDefault();
			string NewUrl = Url.Replace("{TENANCY_NAME}.", TenantName);

			return NewUrl;
		}

		public string AppendTenantName(int TenantId, string Url)
		{
			var TenantName = _tenantRepository.GetAll().Where(T => T.Id == TenantId).Select(T => T.TenancyName).FirstOrDefault();
			string NewUrl = Url.Replace("{TENANCY_NAME}", TenantName);

			return NewUrl;
		}
	}
}