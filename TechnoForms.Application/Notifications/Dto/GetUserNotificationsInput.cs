﻿using Abp.Notifications;
using TechnoForms.Dto;

namespace TechnoForms.Notifications.Dto
{
    public class GetUserNotificationsInput : PagedInputDto
    {
        public UserNotificationState? State { get; set; }
    }
}