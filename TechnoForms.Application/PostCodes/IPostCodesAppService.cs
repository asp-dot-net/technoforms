﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;
using TechnoForms.PostCodes.Dto;

namespace TechnoForms.PostCodes
{
    public interface IPostCodesAppService : IApplicationService
    {
        Task<PagedResultDto<PostCodesListDto>> GetPostCodes(GetPostCodesInput input);
        Task CreatePostCodes(CreatePostCodesInput input);
        Task DeletePostCodes(EntityDto input);
        Task UpdatePostCodes(PostCodesEditDto input);
        Task<PostCodesEditDto> GetPostCodesForEdit(EntityDto input);
        PostCodesValidationMessage CheckPostCodesExists(PostCodesCheckExistDto input);
    }
}
