﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.ObjectMapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.PostCodes.Dto;
using TechnoForms.States;
using System.Data.Entity;using Abp.Linq.Extensions;using System.Linq.Dynamic;

namespace TechnoForms.PostCodes
{
    [AbpAuthorize(AppPermissions.Pages_Masters_PostCodes)]
    public class PostCodesAppService : TechnoFormsAppServiceBase, IPostCodesAppService
    {
        private readonly IRepository<PostCode> _postcodesRepository;
        private readonly IRepository<State> _stateRepository;

        public PostCodesAppService(
          IRepository<PostCode> postcodesRepository, IRepository<State> StateRepository)
        {
            _postcodesRepository = postcodesRepository;
            _stateRepository = StateRepository;
        }

        public async Task<PagedResultDto<PostCodesListDto>> GetPostCodes(GetPostCodesInput input)
        {
            var PostCodeList = new List<PostCodesListDto>();
            var postcodes = _postcodesRepository
                .GetAll()
                .WhereIf(
                    !input.Postcode.IsNullOrEmpty(),
                    p => p.Postcode.Contains(input.Postcode))

                    .WhereIf(
                    !input.Suburb.IsNullOrEmpty(),
                    p => p.Suburb.Contains(input.Suburb))

                    .WhereIf(
                    input.State != null,
                    p => p.StateId == input.State);

            var resultCount = await postcodes.CountAsync();            var results = await postcodes                .AsNoTracking()                .OrderBy(input.Sorting)                .PageBy(input)                .ToListAsync();            PostCodeList = results.MapTo<List<PostCodesListDto>>();
            foreach (var item in PostCodeList)
            {
                item.State = _stateRepository.GetAll().Where(p => p.Id == item.StateId).Select(p => p.StateName).FirstOrDefault();
            }            return new PagedResultDto<PostCodesListDto>(resultCount, PostCodeList.MapTo<List<PostCodesListDto>>());
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_PostCodes_Create)]
        public async Task CreatePostCodes(CreatePostCodesInput input)
        {
            var postcode = input.MapTo<PostCode>();
            await _postcodesRepository.InsertAsync(postcode);
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_PostCodes_Delete)]
        public async Task DeletePostCodes(EntityDto input)
        {
            await _postcodesRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_PostCodes_Edit)]
        public async Task UpdatePostCodes(PostCodesEditDto input)
        {
            var postcode = await _postcodesRepository.GetAsync(input.Id);
            postcode.Postcode = input.Postcode;
            postcode.Suburb = input.Suburb;
            postcode.POBoxes = input.POBoxes;
            postcode.Area = input.Area;
            postcode.StateId = input.StateId;
            await _postcodesRepository.UpdateAsync(postcode);
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_PostCodes_Edit)]
        public async Task<PostCodesEditDto> GetPostCodesForEdit(EntityDto input)
        {
            var postcode = (await _postcodesRepository.GetAsync(input.Id)).MapTo<PostCodesEditDto>();

            return postcode;
        }

        public PostCodesValidationMessage CheckPostCodesExists(PostCodesCheckExistDto input)
        {
            var status_val = new PostCodesValidationMessage();
            status_val.Status = true;
            var postcode = _postcodesRepository
               .GetAll()
               .Where(p => p.Postcode == input.Postcode && p.Suburb == input.Suburb).ToList();

            if (!string.IsNullOrEmpty(input.Id.ToString()) && input.Id != 0)
            {
                postcode = postcode.Where(p => p.Id != input.Id).ToList();
            }
            if (postcode.Count > 0)
            {
                status_val.Status = false;
            }
            if (postcode.Count > 0)
            {
                status_val.Message = "Record with this name already exists.";
            }
            return status_val;
        }
    }
}
