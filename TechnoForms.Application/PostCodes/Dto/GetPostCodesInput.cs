﻿using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Dto;
using TechnoForms.States;

namespace TechnoForms.PostCodes.Dto
{
    public class GetPostCodesInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Postcode { get; set; }

        public string Suburb { get; set; }

        public int? State { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Postcode";
            }
        }
    }
}
