﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.States;

namespace TechnoForms.PostCodes.Dto
{
    [AutoMapFrom(typeof(PostCode), typeof(State))]
    public class PostCodesEditDto : EntityDto
    {
        [Required]
        [MaxLength(PostCode.MaxPostCodeLength)]
        public string Postcode { get; set; }

        [Required]
        [MaxLength(PostCode.MaxSuburbLength)]
        public string Suburb { get; set; }

        [MaxLength(PostCode.MaxPOBoxesLength)]
        public string POBoxes { get; set; }

        [Required]
        [MaxLength(PostCode.MaxAreaLength)]
        public string Area { get; set; }

        public virtual int StateId { get; set; }
    }
}
