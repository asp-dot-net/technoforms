﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.PostCodes.Dto
{
    public class PostCodesCheckExistDto : EntityDto
    {
        public string Postcode { get; set; }
        public string Suburb { get; set; }
    }
}
