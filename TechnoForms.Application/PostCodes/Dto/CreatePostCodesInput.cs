﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.States;

namespace TechnoForms.PostCodes.Dto
{
    [AutoMapTo(typeof(PostCode))]
    public class CreatePostCodesInput
    {
        [Required]
        [MaxLength(PostCode.MaxPostCodeLength)]
        public virtual string Postcode { get; set; }

        [Required]
        [MaxLength(PostCode.MaxSuburbLength)]
        public virtual string Suburb { get; set; }

        [MaxLength(PostCode.MaxPOBoxesLength)]
        public virtual string POBoxes { get; set; }

        [Required]
        [MaxLength(PostCode.MaxAreaLength)]
        public virtual string Area { get; set; }

        public virtual int StateId { get; set; }
    }
}
