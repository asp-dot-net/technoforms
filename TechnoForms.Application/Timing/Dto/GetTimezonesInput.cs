﻿using Abp.Configuration;

namespace TechnoForms.Timing.Dto
{
    public class GetTimezonesInput
    {
        public SettingScopes DefaultTimezoneScope;
    }
}
