﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.InstallerInvitation.Dto;

namespace TechnoForms.InstallerInvitation
{
    public interface IInstallerInvitationAppService : IApplicationService
    {
        ListResultDto<InstallerInvitationListDto> GetAllInstallerInvitation(GetInstallerInvitationInput input);
        ListResultDto<InstallerInvitationListDto> GetInstallerInvitation(GetInstallerInvitationInput input);
        Task ApproveInstallerInvitation(EntityDto input);
        Task<GetInstallerInvitationForReject> GetInstallerInvitationForReject(EntityDto input);
        Task RejectInstallerInvitation(GetInstallerInvitationForReject input);
        Task<InstallerInvitationDetailDto> GetInstallerInvitationDetail(EntityDto input);

        ListResultDto<InstallerUserListDto> GetUsers();
        ListResultDto<InstallerStateListDto> GetState();
        ListResultDto<InstallerPostCodeListDto> GetPostCode();
        ListResultDto<InstallerSuburbListDto> GetSuburb();
        ListResultDto<InstallerPostalAddressTypeListDto> GetPostalAddressType();
        ListResultDto<InstallerPostalDeliveryTypeListDto> GetPostalDeliveryType();
        ListResultDto<InstallerUnitTypeListDto> GetUnitType();
        ListResultDto<InstallerStreetTypeListDto> GetStreetType();
        ListResultDto<InstallerStreetNameListDto> GetStreetName();
    }
}
