﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using TechnoForms.Authorization.Roles;
using TechnoForms.Authorization.Users;
using TechnoForms.CustomerInstallers;
using TechnoForms.InstallerInvitation.Dto;
using TechnoForms.Installers;
using TechnoForms.MultiTenancy;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;
using TechnoForms.PostCodes;
using TechnoForms.States;
using TechnoForms.StreetName;
using TechnoForms.StreetTypes;
using TechnoForms.UnitTypes;

namespace TechnoForms.InstallerInvitation
{
    public class InstallerInvitationAppService : TechnoFormsApplicationModule, IInstallerInvitationAppService
    {
        private readonly IRepository<Installer> _InstallerRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<CustomerInstaller> _customerInstallerRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<PostCode> _postCodeRepository;
        private readonly IRepository<PostalAddressType> _addressTypeRepository;
        private readonly IRepository<PostalDeliveryType> _delevaryTypeRepository;
        private readonly IRepository<UnitType> _unitTypeRepository;
        private readonly IRepository<StreetType> _streetTypeRepository;
        private readonly IRepository<StreetNames> _streetNameRepository;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IAbpSession _session;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public InstallerInvitationAppService(IRepository<Installer> InstallerRepository, IRepository<User, long> UserRepository, IRepository<UserRole, long> UserRoleRepository, IRepository<Role> RoleRepository, IRepository<State> StateRepository, IRepository<PostCode> PostCodeRepository, IRepository<PostalAddressType> AddressTypeRepository, IRepository<PostalDeliveryType> DeliveryTypeRepository, IRepository<UnitType> UnitTypeRepository, IRepository<StreetType> StreetTypeRepository, IRepository<StreetNames> StreetNameRepository, IRepository<CustomerInstaller> CustomerRepository, IRepository<Tenant> tenantRepository, IUnitOfWorkManager unitOfWorkManager, IAbpSession session)
        {
            _InstallerRepository = InstallerRepository;
            _userRepository = UserRepository;
            _userroleRepository = UserRoleRepository;
            _roleRepository = RoleRepository;

            _customerInstallerRepository = CustomerRepository;

            _stateRepository = StateRepository;
            _postCodeRepository = PostCodeRepository;
            _addressTypeRepository = AddressTypeRepository;
            _delevaryTypeRepository = DeliveryTypeRepository;
            _unitTypeRepository = UnitTypeRepository;
            _streetTypeRepository = StreetTypeRepository;
            _streetNameRepository = StreetNameRepository;
            _tenantRepository = tenantRepository;
            _session = session;
            _unitOfWorkManager = unitOfWorkManager;
        }

        public async Task ApproveInstallerInvitation(EntityDto input)
        {
            var invitationApprove = _customerInstallerRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();
            invitationApprove.Inst_Request_status = 1;

            await _customerInstallerRepository.UpdateAsync(invitationApprove);
        }

        public ListResultDto<InstallerInvitationListDto> GetAllInstallerInvitation(GetInstallerInvitationInput input)
        {
            //IsApproved == 0 ----- Pending
            //IsApproved == 1 ----- Approved
            //IsApproved == 2 ----- Rejected
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var InstId = _InstallerRepository
                     .GetAll()
                     .Where(I => I.UserId == input.UserId)
                     .Select(I => I.Id)
                     .FirstOrDefault();

                var Installer_Invitation = _customerInstallerRepository
               .GetAll()
               .Where(I => I.InstallerId == InstId)
               .OrderByDescending(p => p.Id)
               .ToList();

                var InstallerInvitation = (from Inst in Installer_Invitation
                                           join Us in _InstallerRepository.GetAll() on Inst.InstallerId equals Us.Id into Usjoined
                                           from Us in Usjoined.DefaultIfEmpty()
                                           where (Us.IsApproved == 1 && Inst.Inst_Request_status == 0)
                                           group Inst by Inst into instGrouped
                                           select instGrouped.Key);
                var Installer = InstallerInvitation.MapTo<List<InstallerInvitationListDto>>();

                foreach (var item in Installer)
                {
                    item.CustomerFullName = _tenantRepository.GetAll().Where(p => p.Id == item.TenantId).Select(p => p.FullName).FirstOrDefault();
                }
                return new ListResultDto<InstallerInvitationListDto>(Installer);


                ////IsApproved == 0 ----- Pending
                ////IsApproved == 1 ----- Approved
                ////IsApproved == 2 ----- Rejected
                //var Invitation_List = _InstallerRepository
                //   .GetAll()
                //   .OrderByDescending(p => p.Id)
                //   .ToList();

                //var InvitationList = (from Inst in Invitation_List
                //                      join Us in _userRepository.GetAll() on Inst.UserId equals Us.Id into Usjoined
                //                      from Us in Usjoined.DefaultIfEmpty()
                //                      where (Inst.UserId != 0 && Inst.IsApproved == 0)
                //                      group Inst by Inst into instGrouped
                //                      select instGrouped.Key);

                //return new ListResultDto<InstallerInvitationListDto>(InvitationList.MapTo<List<InstallerInvitationListDto>>());
            }
        }

        public ListResultDto<InstallerInvitationListDto> GetInstallerInvitation(GetInstallerInvitationInput input)
        {
            //IsApproved == 0 ----- Pending
            //IsApproved == 1 ----- Approved
            //IsApproved == 2 ----- Rejected
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var InstId = _InstallerRepository
                    .GetAll()
                    .Where(I => I.UserId == input.UserId)
                    .Select(I => I.Id)
                    .FirstOrDefault();

                var Installer_Invitation = _customerInstallerRepository
               .GetAll()
               .Where(I => I.InstallerId == InstId)
               .OrderByDescending(p => p.Id)
               .ToList();

                var InstallerInvitation = (from Inst in Installer_Invitation
                                           join Us in _InstallerRepository.GetAll() on Inst.InstallerId equals Us.Id into Usjoined
                                           from Us in Usjoined.DefaultIfEmpty()
                                           where (Us.IsApproved == 1 && Inst.Inst_Request_status == 1)
                                           group Inst by Inst into instGrouped
                                           select instGrouped.Key);
                var Installer = InstallerInvitation.MapTo<List<InstallerInvitationListDto>>();

                foreach (var item in Installer)
                {
                    item.CustomerFullName = _tenantRepository.GetAll().Where(p => p.Id == item.TenantId).Select(p => p.FullName).FirstOrDefault();
                }
                return new ListResultDto<InstallerInvitationListDto>(Installer);
            }

            ////IsApproved == 0 ----- Pending
            ////IsApproved == 1 ----- Approved
            ////IsApproved == 2 ----- Rejected
            //var Invitation_List = _InstallerRepository
            //   .GetAll()
            //   .OrderByDescending(p => p.Id)
            //   .ToList();

            //var InvitationList = (from Inst in Invitation_List
            //                      join Us in _userRepository.GetAll() on Inst.UserId equals Us.Id into Usjoined
            //                      from Us in Usjoined.DefaultIfEmpty()
            //                      where (Inst.UserId != 0 && Inst.IsApproved == 0)
            //                      group Inst by Inst into instGrouped
            //                      select instGrouped.Key);

            //return new ListResultDto<InstallerInvitationListDto>(InvitationList.MapTo<List<InstallerInvitationListDto>>());
        }

        public async Task<InstallerInvitationDetailDto> GetInstallerInvitationDetail(EntityDto input)
        {
            var InstUserId = _InstallerRepository
                .GetAll()
                .Where(I => I.UserId == input.Id)
                .Select(I => I.Id)
                .FirstOrDefault();
                //(await _customerInstallerRepository.GetAsync(input.Id));

            var installer = (await _InstallerRepository.GetAsync(InstUserId)).MapTo<InstallerInvitationDetailDto>();
            return installer;
        }

        public async Task<GetInstallerInvitationForReject> GetInstallerInvitationForReject(EntityDto input)
        {
            var invitationRej = (await _customerInstallerRepository.GetAsync(input.Id)).MapTo<GetInstallerInvitationForReject>();
            return invitationRej;
        }

        public async Task RejectInstallerInvitation(GetInstallerInvitationForReject input)
        {
            var invitationReject = _customerInstallerRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();

            invitationReject.Inst_Request_status = 2;
            invitationReject.Inst_RejectReason = input.rejectReason;

            await _customerInstallerRepository.UpdateAsync(invitationReject);
        }

        public ListResultDto<InstallerPostalAddressTypeListDto> GetPostalAddressType()
        {
            var addresstype = _addressTypeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InstallerPostalAddressTypeListDto>(addresstype.MapTo<List<InstallerPostalAddressTypeListDto>>());
        }

        public ListResultDto<InstallerPostalDeliveryTypeListDto> GetPostalDeliveryType()
        {
            var delevarytype = _delevaryTypeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InstallerPostalDeliveryTypeListDto>(delevarytype.MapTo<List<InstallerPostalDeliveryTypeListDto>>());
        }

        public ListResultDto<InstallerPostCodeListDto> GetPostCode()
        {
            var postcode = _postCodeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InstallerPostCodeListDto>(postcode.MapTo<List<InstallerPostCodeListDto>>());
        }

        public ListResultDto<InstallerStateListDto> GetState()
        {
            var states = _stateRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InstallerStateListDto>(states.MapTo<List<InstallerStateListDto>>());
        }

        public ListResultDto<InstallerStreetNameListDto> GetStreetName()
        {
            var streername = _streetNameRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InstallerStreetNameListDto>(streername.MapTo<List<InstallerStreetNameListDto>>());
        }

        public ListResultDto<InstallerStreetTypeListDto> GetStreetType()
        {
            var streettype = _streetTypeRepository
              .GetAll()
              .OrderByDescending(p => p.Id)
              .ToList();

            return new ListResultDto<InstallerStreetTypeListDto>(streettype.MapTo<List<InstallerStreetTypeListDto>>());
        }

        public ListResultDto<InstallerSuburbListDto> GetSuburb()
        {
            var suburb = _postCodeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InstallerSuburbListDto>(suburb.MapTo<List<InstallerSuburbListDto>>());
        }

        public ListResultDto<InstallerUnitTypeListDto> GetUnitType()
        {
            var unittype = _unitTypeRepository
                .GetAll()
                .OrderByDescending(p => p.Id)
                .ToList();

            return new ListResultDto<InstallerUnitTypeListDto>(unittype.MapTo<List<InstallerUnitTypeListDto>>());
        }

        //[UnitOfWork]
        public ListResultDto<InstallerUserListDto> GetUsers()
        {
            //using (_unitOfWorkManager.Current.SetTenantId(null))
            //{
            var User_List = _userRepository
           .GetAll()
           .OrderByDescending(p => p.Id)
           .ToList();

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Installer" && user.TenantId != null)
                            group user by user into userGrouped
                            select userGrouped.Key);

            return new ListResultDto<InstallerUserListDto>(UserList.MapTo<List<InstallerUserListDto>>());
            //}
        }
    }
}
