﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostalDeliveryTypes;

namespace TechnoForms.InstallerInvitation.Dto
{
    [AutoMapFrom(typeof(PostalDeliveryType))]
    public class InstallerPostalDeliveryTypeListDto : FullAuditedEntityDto
    {
        public string PostalDeliveryTypeName { get; set; }
    }
}
