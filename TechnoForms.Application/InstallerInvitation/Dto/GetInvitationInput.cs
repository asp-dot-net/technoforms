﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.InstallerInvitation.Dto
{
    public class GetInstallerInvitationInput
    {
        public virtual int UserId { get; set; }
    }
}
