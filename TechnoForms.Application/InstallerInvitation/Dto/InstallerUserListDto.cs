﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization.Users;

namespace TechnoForms.InstallerInvitation.Dto
{
    [AutoMapFrom(typeof(User))]
    public class InstallerUserListDto : FullAuditedEntityDto
    {
        public string UserName { get; set; }

        public List<InstallerUserListRoleDto> Roles { get; set; }

        [AutoMapFrom(typeof(UserRole))]
        public class InstallerUserListRoleDto
        {
            public int RoleId { get; set; }

            public string RoleName { get; set; }
        }
    }
}
