﻿using System.Collections.Generic;
using TechnoForms.Auditing.Dto;
using TechnoForms.Dto;

namespace TechnoForms.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);
    }
}
