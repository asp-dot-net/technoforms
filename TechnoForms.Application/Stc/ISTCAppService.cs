﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Stc.Dto;

namespace TechnoForms.Stc
{
    public interface ISTCAppService : IApplicationService
    {
        Task<PagedResultDto<STCListDto>> GetSTCList(GetSTCInput input);

        Task<JobDetailDto> GetJobsDetails(EntityDto input);

        ListResultDto<Jobs.Dto.SystemTypeListDto> GetSystemType();

        ListResultDto<TenantListDto> GetTenantList();

        Task<ListResultDto<InstallerListDto>> GetAllInstallerAsync(EntityDto input);
    }
}
