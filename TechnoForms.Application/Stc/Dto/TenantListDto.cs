﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.MultiTenancy;

namespace TechnoForms.Stc.Dto
{
    [AutoMapFrom(typeof(Tenant))]
    public class TenantListDto : FullAuditedEntityDto
    {
        public string FullName { get; set; }
    }
}
