﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;

namespace TechnoForms.Stc.Dto
{
    [AutoMapFrom(typeof(Job))]
    public class STCListDto : FullAuditedEntityDto
    {
        public virtual string FormId { get; set; }

        public virtual Guid Guid { get; set; }

        public virtual string PVDNumber { get; set; }

        public virtual string PVDStatus { get; set; }

        public virtual Nullable<System.DateTime> RECCreateDate { get; set; }

        public virtual decimal ExpectedSTC { get; set; }

        public virtual int TenantId { get; set; }

        public virtual string TenantName { get; set; }

        public virtual int JobStatus_Id { get; set; }

        public virtual string InstallationStreetPostCode { get; set; }

        public virtual string InstallationStreetState { get; set; }

        public virtual DateTime ExpectedInstallDate { get; set; }

        public virtual int CreateFor_Id { get; set; }

        public virtual string InstallerName { get; set; }

        public virtual string Phone { get; set; }

        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public string ProjectNumber { get; set; }

        public string StreetState { get; set; }

        public string Owner { get; set; }
        public int SystemType_Id { get; set; }

        public virtual DateTime STCAppliedDate { get; set; }

        public virtual decimal? TotalSTC { get; set; }

    }
}
