﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Dto;

namespace TechnoForms.Stc.Dto
{
    public class GetSTCInput : PagedAndSortedInputDto
    {
        public string filterText { get; set; }
        public string filterFormId { get; set; }
        public string filterOwnerName { get; set; }
        public string filterState { get; set; }
        public int? filterInstallerId { get; set; }
        public int? filterSystemtypeId { get; set; }
        public int? filterPVDStatusId { get; set; }
        public DateTime? filterStartDate { get; set; }
        public DateTime? filterEndDate { get; set; }
        public int? filterDateypeId { get; set; }

        public string filterPVDNumber { get; set; }
        public string filterStcApplied { get; set; }
        public string filterPVDNumberval { get; set; }
        public string filterPVDstatusval { get; set; }
        public int? filterTenantvalId { get; set; }
    }
}
