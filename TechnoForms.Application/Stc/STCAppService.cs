﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using TechnoForms.CustomerInstallers;
using TechnoForms.InstallerEmployes;
using TechnoForms.Installers;
using TechnoForms.Inverters;
using TechnoForms.Jobs;
using TechnoForms.JobStatusName;
using TechnoForms.MultiTenancy;
using TechnoForms.Panels;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;
using TechnoForms.Stc.Dto;

namespace TechnoForms.Stc
{
    public class STCAppService : TechnoFormsAppServiceBase, ISTCAppService
    {
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<PostalAddressType> _postalAddressTypeRepository;
        private readonly IRepository<PostalDeliveryType> _postalDeliveryTypeRepository;
        private readonly IRepository<HouseType> _houseTypeRepository;
        private readonly IRepository<RoofType> _roofTypeRepository;
        private readonly IRepository<Panel> _panelRepository;
        private readonly IRepository<Inverter> _inverterRepository;
        private readonly IRepository<SolarType> _solarTypeRepository;
        private readonly IRepository<SystemType> _systemTypeRepository;
        private readonly IRepository<RoofFacingPanel> _roofFacingPanelRepository;
        private readonly IRepository<YesNo> _yesNoRepository;
        private readonly IRepository<TypeOfSystem> _typeOfSystemRepository;
        private readonly IRepository<TypeOfConnection> _typeOfConnectionRepository;
        private readonly IRepository<MountingType> _mountingTypeRepository;
        private readonly IRepository<RecYear> _recyearRepository;
        private readonly IRepository<Installer> _installerRepository;
        private readonly IRepository<JobStatus> _jobStatusRepository;
        private readonly IRepository<InstallerEmploye> _installerEmployeRepository;
        private readonly IRepository<SystemMountingType> _systemMountingTypeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly ICurrentUnitOfWorkProvider _unitOfWorkProvider;
        private readonly IRepository<CustomerInstaller> _customerInstallerRepository;

        public STCAppService(IRepository<Job> jobRepository,
        IRepository<PostalAddressType> postalAddressTypeRepository,
        IRepository<PostalDeliveryType> postalDeliveryTypeRepository,
        IRepository<HouseType> houseTypeRepository,
        IRepository<RoofType> roofTypeRepository,
        IRepository<Panel> panelRepository,
        IRepository<Inverter> inverterRepository,
        IRepository<SolarType> solarTypeRepository,
        IRepository<SystemType> systemTypeRepository,
        IRepository<RoofFacingPanel> roofFacingPanelRepository,
        IRepository<YesNo> yesNoRepository,
        IRepository<TypeOfSystem> typeOfSystemRepository,
        IRepository<TypeOfConnection> typeOfConnectionRepository,
        IRepository<MountingType> mountingTypeRepository,
        IRepository<RecYear> recyearRepository,
        IRepository<Installer> installerRepository,
        IRepository<JobStatus> jobStatusRepository,
        IRepository<InstallerEmploye> installerEmployeRepository,
        IRepository<SystemMountingType> systemMountingTypeRepository,
        IUnitOfWorkManager unitOfWorkManager,
        IRepository<Tenant> tenantRepository,
        ICurrentUnitOfWorkProvider unitOfWorkProvider,
        IRepository<CustomerInstaller> CustomerRepository)
        {
            _jobRepository = jobRepository;
            _postalAddressTypeRepository = postalAddressTypeRepository;
            _postalDeliveryTypeRepository = postalDeliveryTypeRepository;
            _houseTypeRepository = houseTypeRepository;
            _roofTypeRepository = roofTypeRepository;
            _panelRepository = panelRepository;
            _inverterRepository = inverterRepository;
            _solarTypeRepository = solarTypeRepository;
            _systemTypeRepository = systemTypeRepository;
            _roofFacingPanelRepository = roofFacingPanelRepository;
            _yesNoRepository = yesNoRepository;
            _typeOfSystemRepository = typeOfSystemRepository;
            _typeOfConnectionRepository = typeOfConnectionRepository;
            _mountingTypeRepository = mountingTypeRepository;
            _recyearRepository = recyearRepository;
            _installerRepository = installerRepository;
            _jobStatusRepository = jobStatusRepository;
            _installerEmployeRepository = installerEmployeRepository;
            _systemMountingTypeRepository = systemMountingTypeRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _tenantRepository = tenantRepository;
            _unitOfWorkProvider = unitOfWorkProvider;
            _customerInstallerRepository = CustomerRepository;
        }

        //[UnitOfWork]
        public async Task<PagedResultDto<STCListDto>> GetSTCList(GetSTCInput input)
        {
            var joblist = new List<Job>();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            var resultCount = 0;
            //for (int i = 0; i < tenant.Count; i++)
            //{
            //    using (_unitOfWorkManager.Current.SetTenantId(tenant[i].Id))
            //    {
                    var job = _jobRepository.GetAll();
                    resultCount = await job.CountAsync();
                    foreach (var jobitem in job)
                    {
                        joblist.Add(jobitem);
                    }
            //    }
            //}
            var result = joblist.MapTo<List<STCListDto>>();
            if (!string.IsNullOrEmpty(input.filterText))
            {
                result = result.Where(p => p.ProjectNumber == input.filterText).ToList();
            }
            if (!string.IsNullOrEmpty(input.filterFormId))
            {
                result = result.Where(p => p.FormId == input.filterFormId).ToList();
            }
            if (!string.IsNullOrEmpty(input.filterOwnerName))
            {
                result = result.Where(p => p.FirstName.ToLower().Contains(input.filterOwnerName.ToLower()) ||
                         p.LastName.ToLower().Contains(input.filterOwnerName.ToLower())).ToList();
            }

            if (!string.IsNullOrEmpty(input.filterState))
            {
                result = result.Where(p => p.StreetState == input.filterState).ToList();
            }
            if (!string.IsNullOrEmpty(input.filterInstallerId.ToString()) && input.filterInstallerId != 0)
            {
                result = result.Where(p => p.CreateFor_Id == input.filterInstallerId).ToList();
            }
            if (!string.IsNullOrEmpty(input.filterSystemtypeId.ToString()) && input.filterSystemtypeId != 0)
            {
                result = result.Where(p => p.SystemType_Id == input.filterSystemtypeId).ToList();
            }
            if (!string.IsNullOrEmpty(input.filterPVDNumber))
            {
                result = result.Where(p => p.PVDNumber == input.filterPVDNumber).ToList();
            }
            if (!string.IsNullOrEmpty(input.filterPVDNumberval))
            {
                if (input.filterPVDNumberval == "Yes")
                {
                    result = result.Where(p => p.PVDNumber != "" && p.PVDNumber != null).ToList();
                }
                if (input.filterPVDNumberval == "No")
                {
                    result = result.Where(p => p.PVDNumber == "" || p.PVDNumber == null).ToList();
                }
            }
            if (!string.IsNullOrEmpty(input.filterPVDstatusval))
            {
                result = result.Where(p => p.PVDStatus == input.filterPVDstatusval).ToList();
            }
            if (!string.IsNullOrEmpty(input.filterStartDate.ToString()) && !string.IsNullOrEmpty(input.filterEndDate.ToString()))
                //&& input.filterStartDate.Year != 1 && input.filterEndDate.Year != 1)
            {
                if (input.filterDateypeId == 1 || input.filterDateypeId == 0)
                {
                    result = result.Where(p => p.ExpectedInstallDate >= input.filterStartDate && p.ExpectedInstallDate <= input.filterEndDate).ToList();
                }
                if (input.filterDateypeId == 2)
                {
                    result = result.Where(p => p.CreationTime >= input.filterStartDate && p.ExpectedInstallDate <= input.filterEndDate).ToList();
                }
            }
            if (!string.IsNullOrEmpty(input.filterTenantvalId.ToString()) && input.filterTenantvalId != 0)
            {
                result = result.Where(p => p.TenantId == input.filterTenantvalId).ToList();
            }

            foreach (var item in result)
            {
                item.Owner = item.FirstName + " " + item.LastName;
                using (_unitOfWorkManager.Current.SetTenantId(null))
                {
                    item.InstallerName = _installerRepository.GetAll().Where(T => T.Id == item.CreateFor_Id).Select(T => T.FullName).FirstOrDefault();
                }
                item.TotalSTC = item.TotalSTC == null ? 0 : item.TotalSTC;
            }
            return new PagedResultDto<STCListDto>(resultCount, result);
        }

        public async Task<JobDetailDto> GetJobsDetails(EntityDto input)
        {
            var job = (await _jobRepository.GetAsync(input.Id)).MapTo<JobDetailDto>();
            job.JobStatusName = _jobStatusRepository.GetAll().Where(p => p.Id == job.JobStatus_Id).Select(p => p.JobStatusName).FirstOrDefault();
            job.HouseTypeName = _houseTypeRepository.GetAll().Where(p => p.Id == job.HouseType).Select(p => p.HouseTypeName).FirstOrDefault();
            job.RoofTypeName = _roofTypeRepository.GetAll().Where(p => p.Id == job.RoofType).Select(p => p.RoofTypeName).FirstOrDefault();
            job.SolarTypeName = _solarTypeRepository.GetAll().Where(p => p.Id == job.SolarType_Id).Select(p => p.SolarTypeeName).FirstOrDefault();
            job.SystemTypeName = _systemTypeRepository.GetAll().Where(p => p.Id == job.SystemType_Id).Select(p => p.SystemTypeName).FirstOrDefault();
            job.RoofFacingPanelName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == job.RoofFacingPanel_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
            job.SugstthisaddressName = _yesNoRepository.GetAll().Where(p => p.Id == job.Sugstthisaddress_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.LocationOfNewSystemName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == job.LocationOfNewSystem_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                job.CreateForName = _installerRepository.GetAll().Where(p => p.Id == job.CreateFor_Id).Select(p => p.FullName).FirstOrDefault();
                job.InstallerName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Installer_Id).Select(p => p.FullName).FirstOrDefault();
                job.DesignerName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Designer_Id).Select(p => p.FullName).FirstOrDefault();
                job.ElectricianName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Electrician_Id).Select(p => p.FullName).FirstOrDefault();
            }
            job.TypeofsystemName = _typeOfSystemRepository.GetAll().Where(p => p.Id == job.Typeofsystem_Id).Select(p => p.TypeOfSystemName).FirstOrDefault();
            job.TypeofconnectionName = _typeOfConnectionRepository.GetAll().Where(p => p.Id == job.Typeofconnection_Id).Select(p => p.TypeOfConnectionName).FirstOrDefault();
            job.MountingtypeName = _mountingTypeRepository.GetAll().Where(p => p.Id == job.Mountingtype_Id).Select(p => p.MountingTypeName).FirstOrDefault();
            job.AuditreportavailableName = _yesNoRepository.GetAll().Where(p => p.Id == job.Auditreportavailable_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.InstallingcompleteunitName = _yesNoRepository.GetAll().Where(p => p.Id == job.Installingcompleteunit_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.RECYearName = _recyearRepository.GetAll().Where(p => p.Id == job.RECYear_Id).Select(p => p.REC_Year).FirstOrDefault();
            job.DefaultresourceavailfigureName = _yesNoRepository.GetAll().Where(p => p.Id == job.Defaultresourceavailfigure_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.CeatingCertName = _yesNoRepository.GetAll().Where(p => p.Id == job.CeatingCert_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.CECAccreditationStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.CECAccreditationStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.GovernmentSitingApprovalsName = _yesNoRepository.GetAll().Where(p => p.Id == job.GovernmentSitingApprovals_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.ElectricalSafetyDocumentationName = _yesNoRepository.GetAll().Where(p => p.Id == job.ElectricalSafetyDocumentation_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.AustralianStandardsStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.AustralianStandardsStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.NotgridConnectedStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.NotgridConnectedStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.SGUatThisAddressName = _yesNoRepository.GetAll().Where(p => p.Id == job.SGUatThisAddress_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.SystemMountingTypeName = _systemMountingTypeRepository.GetAll().Where(p => p.Id == job.SystemMountingType_Id).Select(p => p.SystemMountingTypeName).FirstOrDefault();
            job.OwnerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == job.OwnerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
            job.CustomerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == job.CustomerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
            job.InverterModel = _inverterRepository.GetAll().Where(p => p.Id == job.Inverter_id).Select(p => p.ModelNumber).FirstOrDefault();
            job.PanelModel = _panelRepository.GetAll().Where(p => p.Id == job.Panel_id).Select(p => p.ModelNo).FirstOrDefault();
            return job;
        }

        public ListResultDto<Jobs.Dto.SystemTypeListDto> GetSystemType()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var SystemType = _systemTypeRepository
                .GetAll()
                .OrderByDescending(p => p.Id)
                .ToList();

                return new ListResultDto<Jobs.Dto.SystemTypeListDto>(SystemType.MapTo<List<Jobs.Dto.SystemTypeListDto>>());
            }
        }

        public ListResultDto<TenantListDto> GetTenantList()
        {
            var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();

            //var result = (from ten in tenant
            //              join In in _jobRepository.GetAll() on ten.Id equals In.TenantId 
            //              select ten).ToList();
            return new ListResultDto<TenantListDto>(tenant.MapTo<List<TenantListDto>>());
        }

        public async Task<ListResultDto<InstallerListDto>> GetAllInstallerAsync(EntityDto input)
        {

            var Installer = new List<InstallerListDto>();
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var EmpList = _customerInstallerRepository
                    .GetAll()
                    .Where(c => c.Inst_Request_status == 1 && c.Request_status == 1 && c.IsCreated == false && c.TenantId == input.Id)//
                    .Select(p => p.InstallerId)
                    .ToList();

                var installer = _installerRepository
                    .GetAll()
                    .WhereIf(
                        EmpList.Count != 0,
                        E => EmpList.Contains(E.Id) || E.TenantId == input.Id
                        );

                var installerlist = (from Inst in installer
                                     join In in _customerInstallerRepository.GetAll() on Inst.Id equals In.InstallerId into InJoin
                                     from In in InJoin.DefaultIfEmpty()
                                     where (In.Inst_Request_status == 1 && In.Request_status == 1)
                                     group Inst by Inst into instGrouped
                                     select instGrouped.Key);
                return new ListResultDto<InstallerListDto>(installerlist.MapTo<List<InstallerListDto>>());

            }



        }

    }
}
