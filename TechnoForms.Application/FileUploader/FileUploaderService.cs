﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TechnoForms.Configuration;
using TechnoForms.FileUploader.Dto;
using TechnoForms.Installers;
using TechnoForms.Jobs;
using TechnoForms.Web;

namespace TechnoForms.FileUploader
{
    public class FileUploaderService : TechnoFormsApplicationModule,IFileUploaderService
    {
        private readonly IRepository<Job> _jobRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public FileUploaderService(IRepository<Job> JobRepository, IUnitOfWorkManager UnitOfWorkManager)
        {
            _jobRepository = JobRepository;
            _unitOfWorkManager = UnitOfWorkManager;
        }
        #region upload  Installer Signature, delete and Get Details
        public async Task DeleteInstallerSignature(InstallerSignatureRequest input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var job = _jobRepository
            .GetAll()
            .WhereIf(
                input.Id != 0,
                p => p.Id == input.Id
            )
            .FirstOrDefault();
                #region Delete  Installer Signature  amd Photo
                if (!String.IsNullOrEmpty(job.SignatureImageInst))
                {
                    string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\InstallerPhoto\\" + job.SignatureImageInst;
                    System.IO.File.Delete(filepath);
                }
                if (!String.IsNullOrEmpty(job.InstallerPhoto))
                {
                    string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\InstallerPhoto\\" + job.InstallerPhoto;
                    System.IO.File.Delete(filepath);
                }
                job.SignatureImageInst = "";
                job.InstallerPhoto = "";
                await _jobRepository.UpdateAsync(job);
                #endregion
            }
        }
        public ListResultDto<InstallerSignatureDetailDto> GetInstallerSignature(InstallerSignatureRequest input)
        {
            var List = new List<InstallerSignatureDetailDto>();
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {

                var ResultData = _jobRepository
                    .GetAll()
                   .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                ).ToList();
                List = ResultData.MapTo<List<InstallerSignatureDetailDto>>();
            }
            foreach (var item in List)
            {
                item.SignatureImageInst = WebUrlService.WebSiteRootAddress + "Temp/InstallerSignature/" + item.SignatureImageInst;
            }
            return new ListResultDto<InstallerSignatureDetailDto>(List.MapTo<List<InstallerSignatureDetailDto>>());
        }
        public async Task<ClsRepsonse> InstallerSignatureWithLocation(ClsFileupload input)
        {
            ClsRepsonse response = new ClsRepsonse();
            string Installerphoto = "";
            string InstallerSignature = "";
            Installerphoto = "InstallerPhoto_" + DateTime.Now.Ticks + "." + input.Ext;
            InstallerSignature = "InstallerSignature_" + DateTime.Now.Ticks + "." + "jpeg";
            try
            {
                using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
                {
                    var installerPhotoExist = _jobRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();
                    #region upload Installer Photo
                    if (!String.IsNullOrEmpty(installerPhotoExist.InstallerPhoto))
                    {
                        string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\InstallerPhoto\\" + installerPhotoExist.InstallerPhoto;
                        System.IO.File.Delete(filepath);
                    }
                    if (input.ImageData.Length > 0)
                    {

                        string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\InstallerPhoto\\" + Installerphoto;
                        using (MemoryStream mStream = new MemoryStream(input.ImageData))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
                        }
                        GC.Collect();
                        response.Message = "Sucess";
                        response.StatusCode = 1;
                        //return "Sucess";


                    }
                    else
                    {
                        response.Message = "Invalid File.";
                        response.StatusCode = 0;
                    }
                    #endregion

                    #region upload INstaller Signature
                    if (!String.IsNullOrEmpty(installerPhotoExist.SignatureImageInst))
                    {
                        string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\InstallerSignature\\" + installerPhotoExist.SignatureImageInst;
                        System.IO.File.Delete(filepath);
                    }
                    if (input.FileData.Length > 0)
                    {

                        string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\InstallerSignature\\" + InstallerSignature;
                        using (MemoryStream mStream = new MemoryStream(input.ImageData))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
                        }
                        GC.Collect();
                        response.Message = "Sucess";
                        response.StatusCode = 1;
                        //return "Sucess";


                    }
                    else
                    {
                        response.Message = "Invalid File.";
                        response.StatusCode = 0;
                    }
                    #endregion



                    #region Savedata
                    var job = _jobRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();
                    job.SignatureImageInst = InstallerSignature;
                    job.SignatureImageInstDate = DateTime.Now;
                    job.InstallerPhoto = Installerphoto;
                    job.Longitude_InstSign = input.Longitude;
                    job.Latitude_InstSign = input.Latitude;
                    await _jobRepository.UpdateAsync(job);
                    #endregion

                }
            }
            catch (UserFriendlyException ex)
            {
                response.Message = "Error";
                response.StatusCode = 0;
            }
            return response;
        }
        #endregion
        #region upload  Electrician Signature, delete and Get Details
        public async Task<ClsRepsonse> ElectricianSignatureWithLocation(ClsFileupload input)
        {
            ClsRepsonse response = new ClsRepsonse();
            string ElectricianSignature = "ElectricianSignature_" + DateTime.Now.Ticks + ".jpeg";
            try
            {
                using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
                {
                    var Exist = _jobRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();
                    #region upload Electrician Signature
                    //Delete Signature from folder
                    try
                    {
                        if (!String.IsNullOrEmpty(Exist.ElectricianSignature))
                        {
                            string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\ElectricianSignature\\" + Exist.ElectricianSignature;
                            System.IO.File.Delete(filepath);
                        }
                    }
                    catch { }

                    if (input.FileData.Length > 0)
                    {
                        string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\ElectricianSignature\\" + ElectricianSignature;
                        using (MemoryStream mStream = new MemoryStream(input.ImageData))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
                        }
                        GC.Collect();
                        response.Message = "Sucess";
                        response.StatusCode = 1;
                    }
                    else
                    {
                        response.Message = "Invalid File.";
                        response.StatusCode = 0;
                    }
                    #endregion
                    #region Savedata
                    var job = _jobRepository.GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();
                    job.ElectricianSignature = ElectricianSignature;
                    job.ElectricianSignatureDate = DateTime.Now;
                    job.Latitude_ElecSign = input.Latitude;
                    job.Longitude_ElecSign = input.Longitude;
                    await _jobRepository.UpdateAsync(job);
                    #endregion

                }
            }
            catch (UserFriendlyException ex)
            {
                response.Message = "Error";
                response.StatusCode = 0;
            }
            return response;
        }
        public ListResultDto<InstallerSignatureDetailDto> GetElectricianSignature(InstallerSignatureRequest input)
        {
            var List = new List<InstallerSignatureDetailDto>();
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {

                var ResultData = _jobRepository
                    .GetAll()
                   .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                ).ToList();
                List = ResultData.MapTo<List<InstallerSignatureDetailDto>>();
            }
            foreach (var item in List)
            {
                item.ElectricianSignature = WebUrlService.WebSiteRootAddress + "Temp/ElectricianSignature/" + item.ElectricianSignature;
            }
            return new ListResultDto<InstallerSignatureDetailDto>(List.MapTo<List<InstallerSignatureDetailDto>>());
        }
        public async Task DeleteElectricianSignature(InstallerSignatureRequest input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var job = _jobRepository
            .GetAll()
            .WhereIf(
                input.Id != 0,
                p => p.Id == input.Id
            )
            .FirstOrDefault();
                #region Delete  Electrician Signature 
                if (!String.IsNullOrEmpty(job.ElectricianSignature))
                {
                    string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\InstallerPhoto\\" + job.ElectricianSignature;
                    System.IO.File.Delete(filepath);
                }

                job.ElectricianSignature = "";
                await _jobRepository.UpdateAsync(job);
                #endregion
            }
        }
        #endregion
        #region upload  Designer Signature, delete and Get Details
        public async Task<ClsRepsonse> DesignerSignatureWithLocation(ClsFileupload input)
        {
            ClsRepsonse response = new ClsRepsonse();
            string DesignerSignature = "DesignerSignature_" + DateTime.Now.Ticks + ".jpeg";
            try
            {
                using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
                {
                    var Exist = _jobRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();
                    #region upload Designer Signature
                    //Delete Signature from folder
                    if (!String.IsNullOrEmpty(Exist.DesignerSignature))
                    {
                        string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\DesignerSignature\\" + Exist.DesignerSignature;
                        System.IO.File.Delete(filepath);
                    }
                    if (input.FileData.Length > 0)
                    {
                        string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\DesignerSignature\\" + DesignerSignature;
                        using (MemoryStream mStream = new MemoryStream(input.ImageData))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
                        }
                        GC.Collect();
                        response.Message = "Sucess";
                        response.StatusCode = 1;
                    }
                    else
                    {
                        response.Message = "Invalid File.";
                        response.StatusCode = 0;
                    }
                    #endregion
                    #region Savedata
                    var job = _jobRepository.GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();
                    job.DesignerSignature = DesignerSignature;
                    job.DesignerSignatureDate = DateTime.Now;
                    job.Latitude_DesiSign = input.Latitude;
                    job.Longitude_DesiSign = input.Longitude;
                    await _jobRepository.UpdateAsync(job);
                    #endregion

                }
            }
            catch (UserFriendlyException ex)
            {
                response.Message = "Error";
                response.StatusCode = 0;
            }
            return response;
        }
        public async Task DeleteDesignerSignature(InstallerSignatureRequest input)
        {

            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var job = _jobRepository
            .GetAll()
            .WhereIf(
                input.Id != 0,
                p => p.Id == input.Id
            )
            .FirstOrDefault();
                #region Delete  Designer Signature 
                if (!String.IsNullOrEmpty(job.DesignerSignature))
                {
                    string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\DesignerSignature\\" + job.DesignerSignature;
                    System.IO.File.Delete(filepath);
                }

                job.ElectricianSignature = "";
                await _jobRepository.UpdateAsync(job);
                #endregion
            }
        }
        public ListResultDto<InstallerSignatureDetailDto> GetDesignerSignature(InstallerSignatureRequest input)
        {
            var List = new List<InstallerSignatureDetailDto>();
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {

                var ResultData = _jobRepository
                    .GetAll()
                   .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                ).ToList();
                List = ResultData.MapTo<List<InstallerSignatureDetailDto>>();
            }
            foreach (var item in List)
            {
                item.DesignerSignature = WebUrlService.WebSiteRootAddress + "Temp/DesignerSignature/" + item.DesignerSignature;
            }
            return new ListResultDto<InstallerSignatureDetailDto>(List.MapTo<List<InstallerSignatureDetailDto>>());
        }
        #endregion

        #region upload  Owner Signature, delete and Get Details
        public async Task<ClsRepsonse> OwnerSignatureWithLocation(ClsFileupload input)
        {
            ClsRepsonse response = new ClsRepsonse();


            string Ownerphoto = "ownerPhoto_" + DateTime.Now.Ticks + "." + input.Ext;
            string OwnerSignature = "ownerSignature_" + DateTime.Now.Ticks + "." + "jpeg";
            try
            {
                using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
                {
                    var PhotoExist = _jobRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();
                    #region upload Owner Photo
                    if (!String.IsNullOrEmpty(PhotoExist.OwnerPhoto))
                    {
                        string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\OwnerPhoto\\" + PhotoExist.OwnerPhoto;
                        System.IO.File.Delete(filepath);
                    }
                    if (input.ImageData.Length > 0)
                    {

                        string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\OwnerPhoto\\" + Ownerphoto;
                        using (MemoryStream mStream = new MemoryStream(input.ImageData))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
                        }
                        GC.Collect();
                        response.Message = "Sucess";
                        response.StatusCode = 1;
                        //return "Sucess";


                    }
                    else
                    {
                        response.Message = "Invalid File.";
                        response.StatusCode = 0;
                    }
                    #endregion
                    #region upload Owner Signature
                    if (!String.IsNullOrEmpty(PhotoExist.SignatureImageInst))
                    {
                        string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\OwnerSignature\\" + PhotoExist.SignatureImageInst;
                        System.IO.File.Delete(filepath);
                    }
                    if (input.FileData.Length > 0)
                    {

                        string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\OwnerSignature\\" + OwnerSignature;
                        using (MemoryStream mStream = new MemoryStream(input.ImageData))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
                        }
                        GC.Collect();
                        response.Message = "Sucess";
                        response.StatusCode = 1;
                        //return "Sucess";
                    }
                    else
                    {
                        response.Message = "Invalid File.";
                        response.StatusCode = 0;
                    }
                    #endregion

                    #region Savedata
                    var job = _jobRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();
                    job.SignatureImageOwner = OwnerSignature;
                    job.SignatureImageOwnerDate = DateTime.Now;
                    job.OwnerPhoto = Ownerphoto;
                    job.Longitude_CustSign = input.Longitude;
                    job.Latitude_CustSign = input.Latitude;
                    await _jobRepository.UpdateAsync(job);
                    #endregion

                }
            }
            catch (UserFriendlyException ex)
            {
                response.Message = "Error";
                response.StatusCode = 0;
            }
            return response;
        }
        public ListResultDto<InstallerSignatureDetailDto> GetownerSignature(InstallerSignatureRequest input)
        {
            var List = new List<InstallerSignatureDetailDto>();
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {

                var ResultData = _jobRepository
                    .GetAll()
                   .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                ).ToList();
                List = ResultData.MapTo<List<InstallerSignatureDetailDto>>();
            }
            foreach (var item in List)
            {
                item.SignatureImageOwner = WebUrlService.WebSiteRootAddress + "Temp/OwnerPhoto/" + item.SignatureImageOwner;
            }
            return new ListResultDto<InstallerSignatureDetailDto>(List.MapTo<List<InstallerSignatureDetailDto>>());
        }
        public async Task DeleteOwnerSignature(InstallerSignatureRequest input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var job = _jobRepository
            .GetAll()
            .WhereIf(
                input.Id != 0,
                p => p.Id == input.Id
            )
            .FirstOrDefault();
                #region Delete  Owner Signature 
                if (!String.IsNullOrEmpty(job.OwnerPhoto))
                {
                    string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\ownerSignature\\" + job.OwnerPhoto;
                    System.IO.File.Delete(filepath);
                }
                job.OwnerPhoto = "";
                await _jobRepository.UpdateAsync(job);
                #endregion
            }
        }
        #endregion

        #region Upload Picklist Image,Get and Delete
        public async Task<ClsRepsonse> SignatureImagePickListWithLocation(ClsFileupload input)
        {
            ClsRepsonse response = new ClsRepsonse();
            string PicklistSignature = "picklistSignature_" + DateTime.Now.Ticks + "." + "jpeg";
            try
            {
                using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
                {
                    var Exist = _jobRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();
                    #region upload Owner Photo
                    if (!String.IsNullOrEmpty(Exist.SignatureImagePickList))
                    {
                        string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\Picklistsignature\\" + Exist.SignatureImagePickList;
                        System.IO.File.Delete(filepath);
                    }
                    if (input.ImageData.Length > 0)
                    {
                        string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\Picklistsignature\\" + PicklistSignature;
                        using (MemoryStream mStream = new MemoryStream(input.ImageData))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), input.ImageData);
                        }
                        GC.Collect();
                        response.Message = "Sucess";
                        response.StatusCode = 1;
                        //return "Sucess";


                    }
                    else
                    {
                        response.Message = "Invalid File.";
                        response.StatusCode = 0;
                    }
                    #endregion
                    #region Savedata
                    var job = _jobRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();
                    job.SignatureImagePickList = PicklistSignature;
                    job.SignatureImaegePickListDate = DateTime.Now;
                    job.Longitude_PickList = input.Longitude;
                    job.Latitude_PickList = input.Latitude;
                    await _jobRepository.UpdateAsync(job);
                    #endregion

                }
            }
            catch (UserFriendlyException ex)
            {
                response.Message = "Error";
                response.StatusCode = 0;
            }
            return response;
        }
        public ListResultDto<InstallerSignatureDetailDto> GetSignatureImagePickList(InstallerSignatureRequest input)
        {
            var List = new List<InstallerSignatureDetailDto>();
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {

                var ResultData = _jobRepository
                    .GetAll()
                   .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                ).ToList();
                List = ResultData.MapTo<List<InstallerSignatureDetailDto>>();
            }
            foreach (var item in List)
            {
                item.SignatureImagePickList = WebUrlService.WebSiteRootAddress + "Temp/SignatureImagePickList/" + item.SignatureImagePickList;
            }
            return new ListResultDto<InstallerSignatureDetailDto>(List.MapTo<List<InstallerSignatureDetailDto>>());
        }
        public async Task DeleteSignatureImagePickList(InstallerSignatureRequest input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(input.TenantId))
            {
                var job = _jobRepository
            .GetAll()
            .WhereIf(
                input.Id != 0,
                p => p.Id == input.Id
            )
            .FirstOrDefault();
                #region Delete  Picklist Signature 
                if (!String.IsNullOrEmpty(job.SignatureImagePickList))
                {
                    string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\Picklistsignature\\" + job.SignatureImagePickList;
                    System.IO.File.Delete(filepath);
                }
                job.SignatureImagePickList = "";
                await _jobRepository.UpdateAsync(job);
                #endregion
            }
        }        
        #endregion
        public async Task<ClsRepsonse> MUltipleFileUpload(ClsFileupload input)
        {
            ClsRepsonse data = new ClsRepsonse();
            foreach (var item in input.MultipleFileData)
            {
                if (item.Length > 0)
                {
                    string FileName = "MUltiple_" + DateTime.Now.Ticks.ToString() + ".jpeg";
                    try
                    {
                        var buffer = new byte[2097152];
                        byte[] ByteArray = Convert.FromBase64String(item);
                        string uriPath1 = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "temp\\Multiple\\" + FileName;
                        using (MemoryStream mStream = new MemoryStream(ByteArray))
                        {
                            System.IO.File.WriteAllBytes(string.Format("{0}", uriPath1), ByteArray);
                        }
                    }
                    catch
                    {
                    }
                }
                else
                {

                }
            }
            return data;
        }
    }
}
