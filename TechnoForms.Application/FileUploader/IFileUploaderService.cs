﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.FileUploader.Dto;

namespace TechnoForms.FileUploader
{
    public interface IFileUploaderService : IApplicationService
    {
        #region upload  Installer Signature, delete and Get Details
        Task<ClsRepsonse> InstallerSignatureWithLocation(ClsFileupload input);
        ListResultDto<InstallerSignatureDetailDto> GetInstallerSignature(InstallerSignatureRequest input);
        Task DeleteInstallerSignature(InstallerSignatureRequest input);
        #endregion

        #region upload  Electricain Signature, delete and Get Details
        Task<ClsRepsonse> ElectricianSignatureWithLocation(ClsFileupload input);
        ListResultDto<InstallerSignatureDetailDto> GetElectricianSignature(InstallerSignatureRequest input);
        Task DeleteElectricianSignature(InstallerSignatureRequest input);
        #endregion
        #region upload  Designer  Signature, delete and Get Details
        Task<ClsRepsonse> DesignerSignatureWithLocation(ClsFileupload input);
        ListResultDto<InstallerSignatureDetailDto> GetDesignerSignature(InstallerSignatureRequest input);
        Task DeleteDesignerSignature(InstallerSignatureRequest input);
        #endregion
        #region upload  Owner  Signature, delete and Get Details
        Task<ClsRepsonse> OwnerSignatureWithLocation(ClsFileupload input);
        ListResultDto<InstallerSignatureDetailDto> GetownerSignature(InstallerSignatureRequest input);
        Task DeleteOwnerSignature(InstallerSignatureRequest input);
        #endregion

        #region upload  PickLIst  Signature, delete and Get Details
        Task<ClsRepsonse> SignatureImagePickListWithLocation(ClsFileupload input);
        ListResultDto<InstallerSignatureDetailDto> GetSignatureImagePickList(InstallerSignatureRequest input);
        Task DeleteSignatureImagePickList(InstallerSignatureRequest input);
        #endregion

        Task<ClsRepsonse> MUltipleFileUpload (ClsFileupload input);
    }
}
