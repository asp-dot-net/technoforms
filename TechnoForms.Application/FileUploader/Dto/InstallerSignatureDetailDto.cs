﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;

namespace TechnoForms.FileUploader.Dto
{

    [AutoMapFrom(typeof(Job))]
    public class InstallerSignatureDetailDto : FullAuditedEntityDto
    {

        public string Longitude_ElecSign { get; set; }
        public string Latitude_ElecSign { get; set; }
        public string Longitude_DesiSign { get; set; }
        public string Latitude_DesiSign { get; set; }
        public string Longitude_CustSign { get; set; }
        public string Latitude_CustSign { get; set; }
        public string Longitude_InstSign { get; set; }
        public string Latitude_InstSign { get; set; }
        public string InstallerPhoto { get; set; }
        public string OwnerPhoto { get; set; }
        public string SignatureImageOwner { get; set; }
        public string ElectricianSignature { get; set; }
        public string SignatureImageInst { get; set; }
        public string DesignerSignature { get; set; }
        public string SignatureImagePickList { get; set; }

        public string Longitude_PickList { get; set; }
        public string Latitude_PickList { get; set; }

    }
    [AutoMapFrom(typeof(Job))]
    public class OwnerSignatureDetailDto : FullAuditedEntityDto
    {
        public string SignatureImageOwner { get; set; }
    }

}
