﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.FileUploader.Dto
{
    public class InstallerSignatureRequest : EntityDto
    {
        public virtual int TenantId { get; set; }
    }
}
