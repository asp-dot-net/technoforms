﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.FileUploader.Dto
{
    public class ClsFileupload : EntityDto
    {
        public virtual byte[] FileData { get; set; }
        public virtual String[] MultipleFileData { get; set; }
        public virtual string Ext { get; set; }
        public virtual byte[] ImageData { get; set; }
        public virtual int TenantId { get; set; }
        public virtual string Latitude { get; set; }
        public virtual string Longitude { get; set; }
    }
    public class Cls_Fileupload : EntityDto
    {
        public virtual String[] MultipleFileData { get; set; }
    }
}
