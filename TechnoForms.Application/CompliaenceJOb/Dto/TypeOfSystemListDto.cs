﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;

namespace TechnoForms.CompliaenceJob.Dto
{
    [AutoMapFrom(typeof(TypeOfSystem))]
    public class TypeOfSystemListDto : FullAuditedEntityDto
    {
        public string TypeOfSystemName { get; set; }
    }
}
