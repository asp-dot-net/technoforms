﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.States;

namespace TechnoForms.CompliaenceJob.Dto
{
    [AutoMapFrom(typeof(State))]
    public class CJStateListDto : FullAuditedEntityDto
    {
        public string StateName { get; set; }
    }
}
