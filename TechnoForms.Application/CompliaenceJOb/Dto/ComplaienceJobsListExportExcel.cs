﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;
using TechnoForms.ScanedInverters;
using TechnoForms.ScanedPanels;

namespace TechnoForms.CompliaenceJob.Dto
{
    [AutoMapFrom(typeof(Job))]
    public class ComplaienceJobsListExportExcel : FullAuditedEntityDto
    {
        public virtual string FormId { get; set; }
        public int TenantId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Company { get; set; }
        public string EmailId { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }

        public string PostalAddressType { get; set; }
        public string PostalDeliveryNumber { get; set; }
        public string PostalDeliveryType { get; set; }
        public string StreetAddress { get; set; }
        public string UnitNo { get; set; }
        public string UnitType { get; set; }
        public string StreetNo { get; set; }
        public string StreetName { get; set; }
        public string StreetType { get; set; }
        public string StreetCity { get; set; }
        public virtual int? Suburb_Id { get; set; }
        public string StreetState { get; set; }
        public string StreetPostCode { get; set; }

        public string InstallationStreetAddress { get; set; }
        public string InstallationUnitNo { get; set; }
        public string InstallationUnitType { get; set; }
        public string InstallationStreetNo { get; set; }
        public string InstallationStreetName { get; set; }
        public string InstallationStreetType { get; set; }
        public string InstallationStreetCity { get; set; }
        public virtual int? InstallationSuburb_Id { get; set; }
        public string InstallationStreetState { get; set; }
        public string InstallationStreetPostCode { get; set; }

        //Site Details
        public int HouseType { get; set; }
        public string HouseTypeName { get; set; }
        public int RoofType { get; set; }
        public string RoofTypeName { get; set; }
        public string NMINo { get; set; }
        public string RegPlanNo { get; set; }
        public string LotNo { get; set; }
        public string MeterNumber { get; set; }
        public int? EnergyDestributar { get; set; }
        public int? EnergyRetailer { get; set; }
        public string AemoDERjobID { get; set; }
        //Panel Details
        public int Panel_id { get; set; }
        public string BrandName { get; set; }
        public string PanelModel { get; set; }
        public int NoOfPanels { get; set; }
        public decimal SystemCapacity { get; set; }

        //Inverters Details        
        public int Inverter_id { get; set; }
        public string InverterBrandName { get; set; }
        public string InverterModel { get; set; }
        public string InverterSeries { get; set; }

        //Installation Details
        public int SolarType_Id { get; set; }
        public string SolarTypeName { get; set; }

        public int SystemType_Id { get; set; }
        public string SystemTypeName { get; set; }

        public DateTime ExpectedInstallDate { get; set; }
        public string ExpectedInstallShortDate { get; set; }

        public int RoofFacingPanel_Id { get; set; }
        public string RoofFacingPanelName { get; set; }

        public int Sugstthisaddress_Id { get; set; }
        public string SugstthisaddressName { get; set; }

        public int LocationOfNewSystem_Id { get; set; }
        public string LocationOfNewSystemName { get; set; }

        public int Customer_Id { get; set; }
        public int CreateFor_Id { get; set; }
        public string CreateForName { get; set; }

        public int Installer_Id { get; set; }
        public string InstallerName { get; set; }
        public string InstallerFName { get; set; }
        public string InstallerLName { get; set; }


        public int Designer_Id { get; set; }
        public string DesignerName { get; set; }
        public string DesignerFName { get; set; }
        public string DesignerLName { get; set; }

        public int Electrician_Id { get; set; }
        public string ElectricianName { get; set; }
        public string ElectricianFName { get; set; }
        public string ElectricianLName { get; set; }

        public string ProjectNumber { get; set; }
        public string InstallerNote { get; set; }

        //REC Details
        public int Typeofsystem_Id { get; set; }
        public string TypeofsystemName { get; set; }

        public int Typeofconnection_Id { get; set; }
        public string TypeofconnectionName { get; set; }

        public int Mountingtype_Id { get; set; }
        public string MountingtypeName { get; set; }

        public int Auditreportavailable_Id { get; set; }
        public string AuditreportavailableName { get; set; }

        public int Installingcompleteunit_Id { get; set; }
        public string InstallingcompleteunitName { get; set; }

        public string AddcapMoredetailed { get; set; }

        public int RECYear_Id { get; set; }
        public string RECYearName { get; set; }

        public int Defaultresourceavailfigure_Id { get; set; }
        public string DefaultresourceavailfigureName { get; set; }

        public string ResourceAvailability { get; set; }
        public string AccreditationCode { get; set; }
        public string ExplanatoryNotes { get; set; }

        public int CeatingCert_Id { get; set; }
        public string CeatingCertName { get; set; }

        public int CECAccreditationStatement_Id { get; set; }
        public string CECAccreditationStatementName { get; set; }

        public int GovernmentSitingApprovals_Id { get; set; }
        public string GovernmentSitingApprovalsName { get; set; }

        public int ElectricalSafetyDocumentation_Id { get; set; }
        public string ElectricalSafetyDocumentationName { get; set; }

        public int AustralianStandardsStatement_Id { get; set; }
        public string AustralianStandardsStatementName { get; set; }

        public int NotgridConnectedStatement_Id { get; set; }
        public string NotgridConnectedStatementName { get; set; }

        public string InstallPropertyName { get; set; }

        public int SGUatThisAddress_Id { get; set; }
        public string SGUatThisAddressName { get; set; }

        public string DescribeAdequateIinfo { get; set; }
        public string AdditionalSystemInformation { get; set; }

        public int SystemMountingType_Id { get; set; }
        public string SystemMountingTypeName { get; set; }

        public int OwnerAddressType_Id { get; set; }
        public string OwnerAddressTypeName { get; set; }

        public int CustomerAddressType_Id { get; set; }
        public string CustomerAddressTypeName { get; set; }

        public Guid Guid { get; set; }

        public string PanelSerialNumber { get; set; }

        public string InverterSerialNumber { get; set; }

        public string PanelSerialNumberList { get; set; }
        public string InverterSerialNumberList { get; set; }

        public string JobLatitude { get; set; }
        public string JobLongitude { get; set; }


        //Installer Detail
        public virtual string InstallerPhone { get; set; }
        public virtual string InstallerMobile { get; set; }
        public virtual string InstallerEmailId { get; set; }
        public virtual string InstallerFax { get; set; }
        public virtual string InstallerAddressTypeName { get; set; }
        public virtual string InstallerPostalDelNo { get; set; }
        public virtual string InstallerPostalDelTypeName { get; set; }
        public virtual string InstallerUnitNo { get; set; }
        public virtual string InstallerUnitType { get; set; }
        public virtual string InstallerStreetNumber { get; set; }
        public virtual string InstallerStreetName { get; set; }
        public virtual string InstallerStreetType { get; set; }
        public virtual string InstallerSuburb { get; set; }
        public virtual string InstallerState { get; set; }
        public virtual string InstallerPostCode { get; set; }
        public virtual string InstallerExpiry { get; set; }

        //Designer Detail
        public virtual string DesignerPhone { get; set; }
        public virtual string DesignerMobile { get; set; }
        public virtual string DesignerEmailId { get; set; }
        public virtual string DesignerFax { get; set; }
        public virtual string DesignerAddressTypeName { get; set; }
        public virtual string DesignerPostalDelNo { get; set; }
        public virtual string DesignerPostalDelTypeName { get; set; }
        public virtual string DesignerUnitNo { get; set; }
        public virtual string DesignerUnitType { get; set; }
        public virtual string DesignerStreetNumber { get; set; }
        public virtual string DesignerStreetName { get; set; }
        public virtual string DesignerStreetType { get; set; }
        public virtual string DesignerSuburb { get; set; }
        public virtual string DesignerState { get; set; }
        public virtual string DesignerPostCode { get; set; }
        public virtual string DesignerExpiry { get; set; }

        //Electraction Detail
        public virtual string ElectractionPhone { get; set; }
        public virtual string ElectractionMobile { get; set; }
        public virtual string ElectractionEmailId { get; set; }
        public virtual string ElectractionFax { get; set; }
        public virtual string ElectractionAddressTypeName { get; set; }
        public virtual string ElectractionPostalDelNo { get; set; }
        public virtual string ElectractionPostalDelTypeName { get; set; }
        public virtual string ElectractionUnitNo { get; set; }
        public virtual string ElectractionUnitType { get; set; }
        public virtual string ElectractionStreetNumber { get; set; }
        public virtual string ElectractionStreetName { get; set; }
        public virtual string ElectractionStreetType { get; set; }
        public virtual string ElectractionSuburb { get; set; }
        public virtual string ElectractionState { get; set; }
        public virtual string ElectractionPostCode { get; set; }
        public virtual string ElectractionExpiry { get; set; }

        //Retailer Detail
        public virtual string RetailerName { get; set; }
        public virtual string RetailerABN { get; set; }
        public string JobStatusName { get; set; }
        public int JobStatus_Id { get; set; }

    }
}
