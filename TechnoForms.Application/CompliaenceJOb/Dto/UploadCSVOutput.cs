﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.CompliaenceJob.Dto
{
    public class UploadCSVOutput
    {
        public string BulkuploadId { get; set; }

        public string NumberOfCertificate { get; set; }
    }
}
