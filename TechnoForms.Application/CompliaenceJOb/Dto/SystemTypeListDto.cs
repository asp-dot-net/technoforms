﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;

namespace TechnoForms.CompliaenceJob.Dto
{
    [AutoMapFrom(typeof(SystemType))]
    public class SystemTypeListDto : FullAuditedEntityDto
    {
        public string SystemTypeName { get; set; }
    }
}
