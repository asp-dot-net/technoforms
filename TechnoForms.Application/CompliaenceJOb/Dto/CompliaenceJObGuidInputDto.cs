﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.CompliaenceJob.Dto
{
    public class CompliaenceJObbGuidInputDto
    {
        public int JobId { get; set; }
        public Guid JobGuid { get; set; }
        public string RejectReason { get; set; }
    }
}
