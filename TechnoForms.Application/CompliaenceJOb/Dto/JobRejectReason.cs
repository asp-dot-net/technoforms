﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.CompliaenceJob.Dto
{
    public class JobRejectReason : FullAuditedEntityDto
    {
        public virtual string RejectReasonPA { get; set; }
        public virtual string RejectReasonIN { get; set; }
        public virtual string RejectReasonIM { get; set; }
        public virtual string RejectReasonCS { get; set; }
        public virtual string RejectReasonIS { get; set; }
        public virtual string RejectReasonPK { get; set; }
        public virtual string RejectReasonSTC { get; set; }
        public virtual string RejectReasonELE { get; set; }
        public virtual string RejectReasonCUCK { get; set; }
    }
}
