﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Audios;

namespace TechnoForms.CompliaenceJob.Dto
{
    [AutoMapFrom(typeof(Audio))]
    public class AudioListDto : FullAuditedEntityDto
    {
        public virtual int JobId { get; set; }

        public virtual string FileName { get; set; }

        public virtual string DisplayFileName { get; set; }

        public virtual string FilePath { get; set; }
    }
}
