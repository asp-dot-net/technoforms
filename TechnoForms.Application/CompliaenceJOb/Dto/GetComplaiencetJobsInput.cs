﻿using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Dto;

namespace TechnoForms.CompliaenceJob.Dto
{
    public class GetComplaiencetJobsInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }
        public string filterFormId { get; set; }
        public string filterOwnerName { get; set; }
        public string filterCity { get; set; }
        public string filterState { get; set; }
        public int? filterInstallerId { get; set; }
        public int? filterSystemtypeId { get; set; }
        public int? filterJobStatusId { get; set; }
        public DateTime? filterStartDate { get; set; }
        public DateTime? filterEndDate { get; set; }
        public int filterDateypeId { get; set; }
        public int[] filterReceivedId { get; set; }
        public int[] filterNotReceivedId { get; set; }
        public bool verified { get; set; }
        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }

}