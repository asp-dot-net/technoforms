﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Dto;

namespace TechnoForms.CompliaenceJob.Dto
{
    public class GetAudioInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public virtual int jobId { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
