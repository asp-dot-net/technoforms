﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Installers;

namespace TechnoForms.CompliaenceJob.Dto
{
    [AutoMapFrom(typeof(Installer))]
    public class InstallerListDto : FullAuditedEntityDto
    {
        public virtual int? UserId { get; set; }

        public virtual string FullName { get; set; }
    }
}
