﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Audios;

namespace TechnoForms.CompliaenceJob.Dto
{
    [AutoMapTo(typeof(Audio))]
    public class CreateAudioDto : EntityDto
    {
        public virtual int JobId { get; set; }

        [Required]
        [MaxLength(Audio.MaxFileNameLength)]
        public virtual string FileName { get; set; }

        [Required]
        [MaxLength(Audio.MaxDisplayFileNameLength)]
        public virtual string DisplayFileName { get; set; }
    }
}
