﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.CompliaenceJob.Dto;

namespace TechnoForms.CompliaenceJob
{
    public interface IComplaienceJobsAppService : IApplicationService
    {
        Task<PagedResultDto<ComplaienceJobsListDto>> GetComplaienceJobs(GetComplaiencetJobsInput input);
        Task<CJDetailDto> GetJobsDetails(EntityDto input);
        Task DeleteJobs(EntityDto input);

        ListResultDto<PostCodesListDto> GetAllPostCodes();
        ListResultDto<SystemTypeListDto> GetSystemType();
        ListResultDto<TypeOfSystemListDto> GetTypeOfSystem();
        Task<ListResultDto<InstallerListDto>> GetAllInstaller();
        ListResultDto<CJStateListDto> GetState();
        Task ApproveComplaiencePAPanels(CompliaenceJObbGuidInputDto input);
        Task RejectComplaiencePAPanels(CompliaenceJObbGuidInputDto input);
        Task ApproveComplaienceInPanels(CompliaenceJObbGuidInputDto input);
        Task RejectComplaienceINPanels(CompliaenceJObbGuidInputDto input);
        Task ApproveComplaienceIMPanels(CompliaenceJObbGuidInputDto input);
        Task RejectComplaienceIMPanels(CompliaenceJObbGuidInputDto input);
        Task ApproveComplaienceCS(CompliaenceJObbGuidInputDto input);
        Task RejectComplaienceCS(CompliaenceJObbGuidInputDto input);
        Task ApproveComplaienceIS(CompliaenceJObbGuidInputDto input);
        Task RejectComplaienceIS(CompliaenceJObbGuidInputDto input);
        Task ApproveComplaienceELE(CompliaenceJObbGuidInputDto input);
        Task RejectComplaienceELE(CompliaenceJObbGuidInputDto input);
        Task ApproveComplaienceCUCK(CompliaenceJObbGuidInputDto input);
        Task RejectComplaienceCUCK(CompliaenceJObbGuidInputDto input);
        Task ApproveComplaienceCEC(CompliaenceJObbGuidInputDto input);
        Task RejectComplaienceCEC(CompliaenceJObbGuidInputDto input);
        JobRejectReason GetRejectReasonByJobId(CompliaenceJObbGuidInputDto input);

        Task<PagedResultDto<AudioListDto>> GetAudio(GetAudioInput input);
        Task CreateAudio(CreateAudioDto input);
        Task DeleteAudio(EntityDto input);
        Task ApplyForSTC(GetComplaienceExportExcelInputDto input);
        string DownloadCSV(GetComplaienceExportExcelInputDto input);
        string RecCheck();
    }
}
