using System.Collections.Generic;
using TechnoForms.CompliaenceJob.Dto;
using TechnoForms.Dto;
using TechnoForms.Jobs.Dto;

namespace TechnoForms.CompliaenceJob.Exporting
{
    public interface IJobListExcelExporter
    {
        FileDto ExportToFile(List<ComplaienceJobsListExportExcel> jobListDtos);
    }
}