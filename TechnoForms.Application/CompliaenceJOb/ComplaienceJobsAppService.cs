﻿using Abp.Authorization;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.Jobs;
using TechnoForms.CustomerInstallers;
using TechnoForms.InstallerEmployes;
using TechnoForms.Installers;
using TechnoForms.Inverters;
using TechnoForms.JobStatusName;
using TechnoForms.MultiTenancy;
using TechnoForms.Panels;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;
using TechnoForms.PostCodes;
using TechnoForms.States;
using TechnoForms.STCPostCodes;
using TechnoForms.StreetName;
using TechnoForms.StreetTypes;
using TechnoForms.UnitTypes;
using Abp.Linq.Extensions;using System.Linq.Dynamic;
using System.Data.Entity;
using TechnoForms.ScanedPanels;
using TechnoForms.ScanedInverters;
using TechnoForms.Authorization.Users;
using TechnoForms.Photos;
using TechnoForms.Web;
using TechnoForms.PhotoTypes;
using TechnoForms.OldSystemDirections;
using Abp.Net.Mail;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using Abp.Extensions;
using TechnoForms.CompliaenceJob.Dto;
using Abp.AutoMapper;
using Abp.Application.Services.Dto;
using TechnoForms.Audios;
using System.IO;
using TechnoForms.Dto;
using TechnoForms.CompliaenceJob.Exporting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using Abp.UI;
using System.Web;
using System.Net;
using Abp.Timing.Timezone;
using Abp.Configuration;
using Abp.Timing;
using TechnoForms.RecUsers;

namespace TechnoForms.CompliaenceJob
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
    public class ComplaienceJobsAppService : TechnoFormsAppServiceBase, IComplaienceJobsAppService
    {
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<PostalAddressType> _postalAddressTypeRepository;
        private readonly IRepository<PostalDeliveryType> _postalDeliveryTypeRepository;
        private readonly IRepository<UnitType> _unitTypeRepository;
        private readonly IRepository<StreetNames> _streetNamesRepository;
        private readonly IRepository<StreetType> _streetTypeRepository;
        private readonly IRepository<PostCode> _postCodeRepository;
        private readonly IRepository<HouseType> _houseTypeRepository;
        private readonly IRepository<RoofType> _roofTypeRepository;
        private readonly IRepository<Panel> _panelRepository;
        private readonly IRepository<Inverter> _inverterRepository;
        private readonly IRepository<SolarType> _solarTypeRepository;
        private readonly IRepository<SystemType> _systemTypeRepository;
        private readonly IRepository<RoofFacingPanel> _roofFacingPanelRepository;
        private readonly IRepository<YesNo> _yesNoRepository;
        private readonly IRepository<TypeOfSystem> _typeOfSystemRepository;
        private readonly IRepository<TypeOfConnection> _typeOfConnectionRepository;
        private readonly IRepository<MountingType> _mountingTypeRepository;
        private readonly IRepository<RecYear> _recyearRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<Installer> _installerRepository;
        private readonly IRepository<JobStatus> _jobStatusRepository;
        private readonly IRepository<InstallerEmploye> _installerEmployeRepository;
        private readonly IRepository<SystemMountingType> _systemMountingTypeRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<STCPostCode> _stcPostCodeRepository;
        private readonly IRepository<CustomerInstaller> _customerInstallerRepository;
        private readonly IAbpSession _session;
        private readonly IRepository<ScanedPanel> _scanedPanelRepository;
        private readonly IRepository<ScanedInverter> _scanedInverterRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<Photo> _photoRepository;
        private readonly IRepository<PhotoType> _photoTypeRepository;
        private readonly IRepository<OldSystemDirection> _oldSystemDirectionRepository;
        private readonly IRepository<Audio> _audioRepository;
        private readonly IRepository<CustomerSignLog> _customerSignLogRepository;
        private readonly IRepository<Token> _tokenRepository;
        private readonly IRepository<CustomerSignLogResponse> _customerSignLogResponseRepository;
        private readonly IEmailSender _emailSender;
        private readonly IWebUrlService _webUrlService;
        private readonly ICurrentUnitOfWorkProvider _unitOfWorkProvider;
        private readonly IUserEmailer _userEmailer;
        private readonly IJobListExcelExporter _jobListExcelExporter;
        private readonly IAppFolders _appFolders;
        private readonly ITimeZoneConverter _timeZoneConverter;
        private readonly ISettingManager _settingManager;
        private readonly IRepository<RecUser> _recUserRepository;
        List<ComplaienceJobsListExportExcel> GlobalJobList = new List<ComplaienceJobsListExportExcel>();

        public ComplaienceJobsAppService(IRepository<Job> jobRepository,
          IRepository<PostalAddressType> PostalAddressTypeRepository,
          IRepository<PostalDeliveryType> PostalDeliveryTypeRepository,
          IRepository<UnitType> UnitTypeRepository,
          IRepository<StreetNames> StreetNamesRepository,
          IRepository<StreetType> StreetTypeRepository,
          IRepository<PostCode> postCodeRepository,
          IRepository<HouseType> houseTypeRepository,
          IRepository<RoofType> roofTypeRepository,
          IRepository<Panel> panelRepository,
          IRepository<Inverter> inverterRepository,
          IRepository<SolarType> solarTypeRepository,
          IRepository<SystemType> systemTypeRepository,
          IRepository<RoofFacingPanel> roofFacingPanelRepository,
          IRepository<YesNo> yesNoRepository,
          IRepository<TypeOfSystem> typeOfSystemRepository,
          IRepository<TypeOfConnection> typeOfConnectionRepository,
          IRepository<MountingType> mountingTypeRepository,
          IRepository<RecYear> recyearRepository,
          IRepository<State> stateRepository,
          IRepository<Installer> installerRepository,
          IRepository<JobStatus> jobStatusRepository,
          IRepository<InstallerEmploye> installerEmployeRepository,
          IRepository<SystemMountingType> systemMountingTypeRepository,
          IUnitOfWorkManager unitOfWorkManager,
          IRepository<Tenant> tenantRepository,
          IRepository<STCPostCode> stcPostCodeRepository,
          IRepository<CustomerInstaller> CustomerRepository,
          IAbpSession session,
          IRepository<ScanedPanel> scanedPanelRepository,
          IRepository<ScanedInverter> scanedInverterRepository,
          IRepository<User, long> UserRepository,
          IRepository<Photo> photoRepository,
          IRepository<PhotoType> photoTypeRepository,
          IRepository<OldSystemDirection> oldSystemDirectionRepository,
          IRepository<CustomerSignLog> customerSignLogRepository,
          IRepository<Token> tokenRepository,
          IRepository<CustomerSignLogResponse> customerSignLogResponseRepository,
          IEmailSender emailSender,
          IWebUrlService webUrlService,
          ICurrentUnitOfWorkProvider unitOfWorkProvider,
          IUserEmailer userEmailer,
          IRepository<Audio> audioRepository,
          IJobListExcelExporter jobListExcelExporter,
          IAppFolders appFolders,
          ITimeZoneConverter timeZoneConverter,
          ISettingManager settingManager,
          IRepository<RecUser> recUserRepository
          )
        {
            _jobRepository = jobRepository;
            _postalAddressTypeRepository = PostalAddressTypeRepository;
            _postalDeliveryTypeRepository = PostalDeliveryTypeRepository;
            _unitTypeRepository = UnitTypeRepository;
            _streetTypeRepository = StreetTypeRepository;
            _streetNamesRepository = StreetNamesRepository;
            _postCodeRepository = postCodeRepository;
            _houseTypeRepository = houseTypeRepository;
            _roofTypeRepository = roofTypeRepository;
            _panelRepository = panelRepository;
            _inverterRepository = inverterRepository;
            _solarTypeRepository = solarTypeRepository;
            _systemTypeRepository = systemTypeRepository;
            _roofFacingPanelRepository = roofFacingPanelRepository;
            _yesNoRepository = yesNoRepository;
            _typeOfSystemRepository = typeOfSystemRepository;
            _typeOfConnectionRepository = typeOfConnectionRepository;
            _mountingTypeRepository = mountingTypeRepository;
            _recyearRepository = recyearRepository;
            _stateRepository = stateRepository;
            _installerRepository = installerRepository;
            _jobStatusRepository = jobStatusRepository;
            _installerEmployeRepository = installerEmployeRepository;
            _systemMountingTypeRepository = systemMountingTypeRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _tenantRepository = tenantRepository;
            _stcPostCodeRepository = stcPostCodeRepository;
            _customerInstallerRepository = CustomerRepository;
            _session = session;
            _scanedPanelRepository = scanedPanelRepository;
            _scanedInverterRepository = scanedInverterRepository;
            _userRepository = UserRepository;
            _photoRepository = photoRepository;
            _photoTypeRepository = photoTypeRepository;
            _oldSystemDirectionRepository = oldSystemDirectionRepository;
            _customerSignLogRepository = customerSignLogRepository;
            _tokenRepository = tokenRepository;
            _customerSignLogResponseRepository = customerSignLogResponseRepository;
            _emailSender = emailSender;
            _webUrlService = webUrlService;
            _unitOfWorkProvider = unitOfWorkProvider;
            _userEmailer = userEmailer;
            _audioRepository = audioRepository;
            _jobListExcelExporter = jobListExcelExporter;
            _appFolders = appFolders;
            _timeZoneConverter = timeZoneConverter;
            _settingManager = settingManager;
            _recUserRepository = recUserRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        [UnitOfWork]
        public async Task<PagedResultDto<ComplaienceJobsListDto>> GetComplaienceJobs(GetComplaiencetJobsInput input)
        {
            long userid = (long)_session.UserId;
            var user = await UserManager.GetUserByIdAsync(userid);
            var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);

            var userspecific = grantedPermissions.Where(x => x.Name == "Pages.UserSpecificJob").ToList();
            var job = _jobRepository
             .GetAll()
             .WhereIf(
                  !input.Filter.IsNullOrEmpty(),
                  p => p.ProjectNumber.ToLower().Contains(input.Filter.ToLower())
              )
              .Where(J=>J.BulkUploadID == null);

            if (!string.IsNullOrEmpty(input.verified.ToString()))
            {
                if (input.verified)
                {
                    job = job.Where(J => J.IsComplaienceCS == true && J.IsComplaienceCUCK == true && J.IsComplaienceELE == true && J.ISComplaienceIM == true && J.IsComplaienceIN == true && J.IsComplaienceIS == true && J.IsComplaiencePA == true && J.IsComplaienceSTC == true);
                }
                else
                {
                    job = job.Where(J => (J.IsComplaienceCS == false || J.IsComplaienceCS == null) || (J.IsComplaienceCUCK == false || J.IsComplaienceCUCK == null) || (J.IsComplaienceELE == false || J.IsComplaienceELE == null) || (J.ISComplaienceIM == false || J.ISComplaienceIM == null) || (J.IsComplaienceIN == false || J.IsComplaienceIN == null) || (J.IsComplaienceIS == false || J.IsComplaienceIS == null) || (J.IsComplaiencePA == false || J.IsComplaiencePA == null) || (J.IsComplaienceSTC == false || J.IsComplaienceSTC == null));
                }
            }
            job = job.Where(p => p.IsVerifyCS == true && p.IsVerifyIN == true && p.IsVerifyPA == true && p.IsVerifyIS == true
                && p.IsVerifyImages == true);

            if (!string.IsNullOrEmpty(input.filterFormId))
            {
                job = job.Where(p => p.FormId.ToLower().Contains(input.filterFormId.ToLower()));
            }
            if (!string.IsNullOrEmpty(input.filterOwnerName))
            {
                job = job.Where(p => p.FirstName.ToLower().Contains(input.filterOwnerName.ToLower()) ||
                         p.LastName.ToLower().Contains(input.filterOwnerName.ToLower()));
            }
            if (!string.IsNullOrEmpty(input.filterCity))
            {
                job = job.Where(p => p.StreetCity.ToLower().Contains(input.filterCity.ToLower()));
            }
            if (!string.IsNullOrEmpty(input.filterState))
            {
                job = job.Where(p => p.StreetState.ToLower().Contains(input.filterState.ToLower()));
            }
            if (!string.IsNullOrEmpty(input.filterInstallerId.ToString()) && input.filterInstallerId != 0)
            {
                job = job.Where(p => p.CreateFor_Id == input.filterInstallerId);
            }
            if (!string.IsNullOrEmpty(input.filterSystemtypeId.ToString()) && input.filterSystemtypeId != 0)
            {
                job = job.Where(p => p.SystemType_Id == input.filterSystemtypeId);
            }
            if (!string.IsNullOrEmpty(input.filterJobStatusId.ToString()) && input.filterJobStatusId != 0)
            {
                job = job.Where(p => p.JobStatus_Id == input.filterJobStatusId);
            }

            if (!string.IsNullOrEmpty(input.filterStartDate.ToString()) && !string.IsNullOrEmpty(input.filterEndDate.ToString()))
            //&& input.filterStartDate.Year != 1 && input.filterEndDate.Year != 1)
            {
                if (input.filterDateypeId == 1 || input.filterDateypeId == 0)
                {
                    job = job.Where(p => p.ExpectedInstallDate >= input.filterStartDate && p.ExpectedInstallDate <= input.filterEndDate);
                }
                if (input.filterDateypeId == 2)
                {
                    var panel = _panelRepository.GetAll().Where(p => p.ExpiryDate >= input.filterStartDate && p.ExpiryDate <= input.filterEndDate).ToList();
                    var panelIds = "";
                    foreach (var itempanel in panel)
                    {
                        panelIds += "," + itempanel.Id;
                    }
                    if (panelIds.StartsWith(","))
                    {
                        panelIds = panelIds.Remove(0, 1);
                    }
                    var pIds = panelIds.Split(',').Select(s => int.Parse(s));

                    job = job.Where(p => pIds.Contains(p.Panel_id));
                }
                if (input.filterDateypeId == 3)
                {
                    var inverter = _inverterRepository.GetAll().Where(p => p.ExpiryDate >= input.filterStartDate && p.ExpiryDate <= input.filterEndDate).ToList();
                    var inverterIds = "";
                    foreach (var iteminverter in inverter)
                    {
                        inverterIds += "," + iteminverter.Id;
                    }
                    if (inverterIds.StartsWith(","))
                    {
                        inverterIds = inverterIds.Remove(0, 1);
                    }
                    var InvIds = inverterIds.Split(',').Select(s => int.Parse(s));

                    job = job.Where(p => InvIds.Contains(p.Inverter_id));
                }
                if (input.filterDateypeId == 4)
                {
                    var installer = _installerEmployeRepository.GetAll().Where(p => p.InsAccreExDate >= input.filterStartDate && p.InsAccreExDate <= input.filterEndDate).ToList();
                    var installerIds = "";
                    foreach (var iteminverter in installer)
                    {
                        installerIds += "," + iteminverter.Id;
                    }
                    if (installerIds.StartsWith(","))
                    {
                        installerIds = installerIds.Remove(0, 1);
                    }
                    var InstIds = installerIds.Split(',').Select(s => int.Parse(s));

                    job = job.Where(p => InstIds.Contains(p.Installer_Id));
                }
            }

            #region Received
            if (input.filterReceivedId != null)
            {
                if (input.filterReceivedId.Count() > 0)
                {
                    for (int i = 0; i < input.filterReceivedId.Count(); i++)
                    {
                        var jobphotoguid_list = _photoRepository.GetAll().ToList();
                        var jobphotoIds = "";
                        if (!string.IsNullOrEmpty(jobphotoIds) && input.filterReceivedId[i] == 3)
                        {
                            foreach (var itemphoto in jobphotoguid_list)
                            {
                                jobphotoIds += "," + itemphoto.Guid;
                            }
                            if (jobphotoIds.StartsWith(","))
                            {
                                jobphotoIds = jobphotoIds.Remove(0, 1);
                            }
                        }

                        var jobval = job.ToList();
                        var jobScanedPanelIds = "";
                        if (!string.IsNullOrEmpty(jobScanedPanelIds) && input.filterReceivedId[i] == 1)
                        {
                            for (int j = 0; j < jobval.Count(); j++)
                            {
                                var jobguid = jobval[i].Guid;
                                var totalpanel = jobval[i].NoOfPanels;
                                var totalScanedPanel = (_scanedPanelRepository.GetAll().Where(p => p.Guid == jobguid)).ToList();
                                if (totalpanel == totalScanedPanel.Count)
                                {
                                    jobScanedPanelIds += "," + jobval[i].Id;
                                }
                            }
                        }

                        var jobScanedInverterIds = "";
                        if (!string.IsNullOrEmpty(jobScanedInverterIds) && input.filterReceivedId[i] == 2)
                        {
                            for (int j = 0; j < jobval.Count(); j++)
                            {
                                var jobguid = jobval[i].Guid;
                                var totalInverter = jobval[i].NoOfInverter;
                                var ScanedInverter = (_scanedInverterRepository.GetAll().Where(p => p.Guid == jobguid)).ToList();
                                if (totalInverter == ScanedInverter.Count)
                                {
                                    jobScanedInverterIds += "," + jobval[i].Id;
                                }
                            }
                        }
                        if (input.filterReceivedId[i] == 4 || input.filterReceivedId[i] == 5 || input.filterReceivedId[i] == 3 || input.filterReceivedId[i] == 1 || input.filterReceivedId[i] == 2)
                        {
                            try
                            {
                                job = job.Where(p =>
                           (p.SignatureImageInst != null && p.SignatureImageInst != string.Empty) ||
                           (p.SignatureImageOwner != null && p.SignatureImageOwner != string.Empty) ||
                           (jobScanedPanelIds.Contains(p.Id.ToString())) ||
                           (jobScanedInverterIds.Contains(p.Id.ToString())) ||
                           (jobphotoIds.Contains(p.Guid.ToString()))
                           );
                            }
                            catch (Exception e)
                            { }
                        }
                    }
                }
            }
            #endregion

            #region Not Received
            if (input.filterNotReceivedId != null)
            {
                if (input.filterNotReceivedId.Count() > 0)
                {
                    for (int i = 0; i < input.filterNotReceivedId.Count(); i++)
                    {
                        var jobphotoguid_list = _photoRepository.GetAll().ToList();
                        var jobphotoIds = "";
                        if (!string.IsNullOrEmpty(jobphotoIds) && input.filterNotReceivedId[i] == 3)
                        {
                            foreach (var itemphoto in jobphotoguid_list)
                            {
                                jobphotoIds += "," + itemphoto.Guid;
                            }
                            if (jobphotoIds.StartsWith(","))
                            {
                                jobphotoIds = jobphotoIds.Remove(0, 1);
                            }
                        }

                        var jobval = job.ToList();
                        var jobScanedPanelIds = "";
                        if (!string.IsNullOrEmpty(jobScanedPanelIds) && input.filterNotReceivedId[i] == 1)
                        {
                            for (int j = 0; j < jobval.Count(); j++)
                            {
                                var jobguid = jobval[i].Guid;
                                var totalpanel = jobval[i].NoOfPanels;
                                var totalScanedPanel = (_scanedPanelRepository.GetAll().Where(p => p.Guid == jobguid)).ToList();
                                if (totalpanel > totalScanedPanel.Count)
                                {
                                    jobScanedPanelIds += "," + jobval[i].Id;
                                }
                            }
                        }

                        var jobScanedInverterIds = "";
                        if (!string.IsNullOrEmpty(jobScanedInverterIds) && input.filterNotReceivedId[i] == 2)
                        {
                            for (int j = 0; j < jobval.Count(); j++)
                            {
                                var jobguid = jobval[i].Guid;
                                var totalInverter = jobval[i].NoOfInverter;
                                var ScanedInverter = (_scanedInverterRepository.GetAll().Where(p => p.Guid == jobguid)).ToList();
                                if (totalInverter > ScanedInverter.Count)
                                {
                                    jobScanedInverterIds += "," + jobval[i].Id;
                                }
                            }
                        }
                        if (input.filterNotReceivedId[i] == 4 || input.filterNotReceivedId[i] == 5 || input.filterNotReceivedId[i] == 3 || input.filterNotReceivedId[i] == 1 || input.filterNotReceivedId[i] == 2)
                        {
                            try
                            {
                                job = job.Where(p =>
                           (p.SignatureImageInst == null && p.SignatureImageInst == string.Empty) ||
                           (p.SignatureImageOwner == null && p.SignatureImageOwner == string.Empty) ||
                           (jobScanedPanelIds.Contains(p.Id.ToString())) ||
                           (jobScanedInverterIds.Contains(p.Id.ToString())) ||
                           (jobphotoIds.Contains(p.Guid.ToString()))
                           );
                            }
                            catch (Exception e)
                            { }
                        }
                    }
                }
            }
            #endregion

            var resultCount = await job.CountAsync();

            if (input.Sorting == "fullName asc")
            {
                input.Sorting = "FirstName asc";
            }
            if (input.Sorting == "fullName desc")
            {
                input.Sorting = "FirstName desc";
            }
            if (input.Sorting == "installerName asc")
            {
                input.Sorting = "Installer_Id asc";
            }
            if (input.Sorting == "installerName desc")
            {
                input.Sorting = "Installer_Id desc";
            }
            if (input.Sorting == "createForName asc")
            {
                input.Sorting = "CreateFor_Id asc";
            }
            if (input.Sorting == "createForName desc")
            {
                input.Sorting = "CreateFor_Id desc";
            }
            var results = await job
          .AsNoTracking()
          .OrderBy(input.Sorting)
          .PageBy(input)
          .ToListAsync();

            if (userspecific.Count > 0)
            {
                results = results.Where(x => x.CreatorUserId == userid).ToList();
            }
            var joblist = results.MapTo<List<ComplaienceJobsListDto>>();
            foreach (var item in joblist)
            {
                item.verified = input.verified;
                item.FullName = item.FirstName + " " + item.LastName;
                item.StreetAddress = item.StreetAddress + ", " + item.StreetCity + ", " + item.StreetState + ", " + item.StreetPostCode;
                item.JobStatusName = _jobStatusRepository.GetAll().Where(p => p.Id == item.JobStatus_Id).Select(p => p.JobStatusName).FirstOrDefault();

                using (_unitOfWorkManager.Current.SetTenantId(null))
                {
                    item.CreateForName = _installerRepository.GetAll().Where(p => p.Id == item.CreateFor_Id).Select(p => p.FullName).FirstOrDefault();
                    item.InstallerName = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).Select(p => p.FullName).FirstOrDefault();
                    item.DesignerName = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).Select(p => p.FullName).FirstOrDefault();
                    item.ElectricianName = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Electrician_Id).Select(p => p.FullName).FirstOrDefault();

                    item.HouseTypeName = _houseTypeRepository.GetAll().Where(p => p.Id == item.HouseType).Select(p => p.HouseTypeName).FirstOrDefault();
                    item.RoofTypeName = _roofTypeRepository.GetAll().Where(p => p.Id == item.RoofType).Select(p => p.RoofTypeName).FirstOrDefault();
                    item.SolarTypeName = _solarTypeRepository.GetAll().Where(p => p.Id == item.SolarType_Id).Select(p => p.SolarTypeeName).FirstOrDefault();
                    item.SystemTypeName = _systemTypeRepository.GetAll().Where(p => p.Id == item.SystemType_Id).Select(p => p.SystemTypeName).FirstOrDefault();
                    item.RoofFacingPanelName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == item.RoofFacingPanel_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
                    item.SugstthisaddressName = _yesNoRepository.GetAll().Where(p => p.Id == item.Sugstthisaddress_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.LocationOfNewSystemName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == item.LocationOfNewSystem_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();

                    item.TypeofsystemName = _typeOfSystemRepository.GetAll().Where(p => p.Id == item.Typeofsystem_Id).Select(p => p.TypeOfSystemName).FirstOrDefault();
                    item.TypeofconnectionName = _typeOfConnectionRepository.GetAll().Where(p => p.Id == item.Typeofconnection_Id).Select(p => p.TypeOfConnectionName).FirstOrDefault();
                    item.MountingtypeName = _mountingTypeRepository.GetAll().Where(p => p.Id == item.Mountingtype_Id).Select(p => p.MountingTypeName).FirstOrDefault();
                    item.AuditreportavailableName = _yesNoRepository.GetAll().Where(p => p.Id == item.Auditreportavailable_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.InstallingcompleteunitName = _yesNoRepository.GetAll().Where(p => p.Id == item.Installingcompleteunit_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.RECYearName = _recyearRepository.GetAll().Where(p => p.Id == item.RECYear_Id).Select(p => p.REC_Year).FirstOrDefault();
                    item.DefaultresourceavailfigureName = _yesNoRepository.GetAll().Where(p => p.Id == item.Defaultresourceavailfigure_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.CeatingCertName = _yesNoRepository.GetAll().Where(p => p.Id == item.CeatingCert_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.CECAccreditationStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.CECAccreditationStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.GovernmentSitingApprovalsName = _yesNoRepository.GetAll().Where(p => p.Id == item.GovernmentSitingApprovals_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.ElectricalSafetyDocumentationName = _yesNoRepository.GetAll().Where(p => p.Id == item.ElectricalSafetyDocumentation_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.AustralianStandardsStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.AustralianStandardsStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.NotgridConnectedStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.NotgridConnectedStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.SGUatThisAddressName = _yesNoRepository.GetAll().Where(p => p.Id == item.SGUatThisAddress_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.SystemMountingTypeName = _systemMountingTypeRepository.GetAll().Where(p => p.Id == item.SystemMountingType_Id).Select(p => p.SystemMountingTypeName).FirstOrDefault();
                    item.OwnerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == item.OwnerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
                    item.CustomerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == item.CustomerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
                    item.PanelModel = _panelRepository.GetAll().Where(p => p.Id == item.Panel_id).Select(p => p.ModelNo).FirstOrDefault();
                    item.InverterModel = _inverterRepository.GetAll().Where(p => p.Id == item.Inverter_id).Select(p => p.ModelNumber).FirstOrDefault();

                    item.PanelLogo = _panelRepository.GetAll().Where(p => p.Id == item.Panel_id).Select(p => p.PanelLogo).FirstOrDefault();
                    item.InverterLogo = _inverterRepository.GetAll().Where(p => p.Id == item.Inverter_id).Select(p => p.InverterLogo).FirstOrDefault();

                    item.PanelLogoPath = WebUrlService.WebSiteRootAddress + "Temp/PanelLogo/" + item.PanelLogo;
                    item.PanelLogoPath = AppendTenantName(item.TenantId, item.PanelLogoPath);
                    item.InverterLogoPath = WebUrlService.WebSiteRootAddress + "Temp/InverterLogo/" + item.InverterLogo;
                    item.InverterLogoPath = AppendTenantName(item.TenantId, item.InverterLogoPath);
                }

                item.JobScanedPanel_List = (from itempanel in _scanedPanelRepository.GetAll()
                                            where itempanel.Guid == item.Guid
                                            select new ComplaienceJobsListDto.JobScanedPanel
                                            {
                                                Id = itempanel.Id,
                                                PanelSerialNumber = itempanel.PanelSerialNumber,
                                                PanelDeleteLink = "grid.appScope.jobScanedPaneldelete(" + itempanel.Id + ")"
                                            }).ToList();
                item.JobScanedInverter_List = (from itemInverter in _scanedInverterRepository.GetAll()
                                               where itemInverter.Guid == item.Guid
                                               select new ComplaienceJobsListDto.JobScanedInverter
                                               {
                                                   Id = itemInverter.Id,
                                                   InverterSerialNumber = itemInverter.InverterSerialNumber
                                               }).ToList();
                item.ScanedPanelClass = "dcbox";
                item.ScanedPanelCount = item.JobScanedPanel_List.Count;
                if (!string.IsNullOrEmpty(item.IsComplaiencePA.ToString()))
                {
                    if (item.IsComplaiencePA == true)
                    {
                        item.ScanedPanelClass = "dcbox verified";
                    }
                    if (item.IsComplaiencePA == false)
                    {
                        item.ScanedPanelClass = "dcbox rejected";
                    }
                }


                item.ScanedInverterClass = "dcbox";
                item.ScanedInverterCount = item.JobScanedInverter_List.Count;

                if (!string.IsNullOrEmpty(item.IsComplaienceIN.ToString()))
                {
                    if (item.IsComplaienceIN == true)
                    {
                        item.ScanedInverterClass = "dcbox verified";
                    }
                    if (item.IsComplaienceIN == false)
                    {
                        item.ScanedInverterClass = "dcbox rejected";
                    }
                }

                item.OtherImagesClass = "dcbox";
                if (!string.IsNullOrEmpty(item.ISComplaienceIM.ToString()))
                {
                    if (item.ISComplaienceIM == true)
                    {
                        item.OtherImagesClass = "dcbox verified";
                    }
                    if (item.ISComplaienceIM == false)
                    {
                        item.OtherImagesClass = "dcbox rejected";
                    }
                }

                item.CustomerSignatureClass = "dcbox";
                if (!string.IsNullOrEmpty(item.IsComplaienceCS.ToString()))
                {
                    if (item.IsComplaienceCS == true)
                    {
                        item.CustomerSignatureClass = "dcbox verified";
                    }
                    if (item.IsComplaienceCS == false)
                    {
                        item.CustomerSignatureClass = "dcbox rejected";
                    }
                }

                item.InstallerSignatureClass = "dcbox";

                if (!string.IsNullOrEmpty(item.IsComplaienceIS.ToString()))
                {
                    if (item.IsComplaienceIS == true)
                    {
                        item.InstallerSignatureClass = "dcbox verified";
                    }
                    if (item.IsComplaienceIS == false)
                    {
                        item.InstallerSignatureClass = "dcbox rejected";
                    }
                }
                item.CECClass = "dcbox";

                if (!string.IsNullOrEmpty(item.IsComplaienceSTC.ToString()))
                {
                    if (item.IsComplaienceSTC == true)
                    {
                        item.CECClass = "dcbox verified";
                    }
                    if (item.IsComplaienceSTC == false)
                    {
                        item.CECClass = "dcbox rejected";
                    }
                }

                item.ELEClass = "dcbox";

                if (!string.IsNullOrEmpty(item.IsComplaienceELE.ToString()))
                {
                    if (item.IsComplaienceELE == true)
                    {
                        item.ELEClass = "dcbox verified";
                    }
                    if (item.IsComplaienceELE == false)
                    {
                        item.ELEClass = "dcbox rejected";
                    }
                }
                item.CUCKClass = "dcbox";

                if (!string.IsNullOrEmpty(item.IsComplaienceCUCK.ToString()))
                {
                    if (item.IsComplaienceCUCK == true)
                    {
                        item.CUCKClass = "dcbox verified";
                    }
                    if (item.IsComplaienceCUCK == false)
                    {
                        item.CUCKClass = "dcbox rejected";
                    }
                }

                if (!string.IsNullOrEmpty(item.LastModifierUserId.ToString()))
                {
                    item.lastModifierUserName = _userRepository.GetAll().Where(p => p.Id == item.LastModifierUserId).Select(p => p.Name).FirstOrDefault() + " " + _userRepository.GetAll().Where(p => p.Id == item.LastModifierUserId).Select(p => p.Surname).FirstOrDefault();
                }

                item.InstallerExpire = false;
                item.PanelExpire = false;
                item.InverterExpire = false;
                DateTime todatedate = DateTime.Now;
                using (_unitOfWorkManager.Current.SetTenantId(null))
                {
                    var InsAccreExDate = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).Select(p => p.InsAccreExDate).FirstOrDefault();
                    if (InsAccreExDate != null)
                    {
                        DateTime InstallerExpiryDate = (DateTime)InsAccreExDate;
                        DateTime InstallerExpiry_ReminderDate = InstallerExpiryDate.AddDays(-90);
                        if (InstallerExpiry_ReminderDate <= todatedate)
                        {
                            item.InstallerExpire = true;
                        }
                    }
                    var panelExDate = _panelRepository.GetAll().Where(p => p.Id == item.Panel_id).Select(p => p.ExpiryDate).FirstOrDefault();
                    if (panelExDate != null)
                    {
                        DateTime PanelExpiryDate = (DateTime)panelExDate;
                        DateTime PanelExpiry_ReminderDate = PanelExpiryDate.AddDays(-90);
                        if (PanelExpiry_ReminderDate <= todatedate)
                        {
                            item.PanelExpire = true;
                        }
                    }
                    var inverterExDate = _inverterRepository.GetAll().Where(p => p.Id == item.Inverter_id).Select(p => p.ExpiryDate).FirstOrDefault();
                    if (inverterExDate != null)
                    {
                        DateTime InverterExpiryDate = (DateTime)inverterExDate;
                        DateTime InverterExpiry_ReminderDate = InverterExpiryDate.AddDays(-90);
                        if (InverterExpiry_ReminderDate <= todatedate)
                        {
                            item.InverterExpire = true;
                        }
                    }

                    var newGuId = _jobRepository.GetAll().Where(p => p.Id == item.Id).Select(p => p.Guid).FirstOrDefault();
                    var Scanedpanel_Images = _scanedPanelRepository.GetAll().Where(p => p.Guid == newGuId && p.PanelBarcodePhoto == "NULL").Select(p => p.Id).ToList();

                    item.IsScanedPanelImagesLinkShow = false;
                    if (Scanedpanel_Images.Count > 0)
                    {
                        item.IsScanedPanelImagesLinkShow = true;
                    }

                    var Scanedinverter_Images = _scanedInverterRepository.GetAll().Where(p => p.Guid == newGuId && p.InverterBarcodePhoto == "NULL").Select(p => p.Id).ToList();

                    item.IsScanedInverterImagesLinkShow = false;
                    if (Scanedinverter_Images.Count > 0)
                    {
                        item.IsScanedInverterImagesLinkShow = true;
                    }
                }
                if (!string.IsNullOrEmpty(item.JobLatitude))
                {
                    item.JobLatLong = item.JobLatitude + "," + item.JobLongitude;
                }

                if (!string.IsNullOrEmpty(item.IsComplaiencePA.ToString()))
                {
                    string ComplaiencePAApprovedBy = "";
                    //using (_unitOfWorkManager.Current.SetTenantId(null))
                    //{
                    var paverifiedid = item.PAVerifiedBy;
                    var approveby = _userRepository.GetAll().ToList();
                    var approvebylist = _userRepository.GetAll().Where(p => p.Id == item.PAVerifiedBy).ToList();
                    ComplaiencePAApprovedBy = approvebylist.Select(p => p.Name).FirstOrDefault() + " " + approvebylist.Select(p => p.Surname).FirstOrDefault();
                    //}
                    if (item.IsComplaiencePA == true)
                    {
                        item.ComplaiencePAMessage = "Approved By : " + ComplaiencePAApprovedBy + " Date : " + item.PAVerifiedDate;
                    }
                    if (item.IsComplaiencePA == false)
                    {
                        item.ComplaiencePAMessage = "Rejected By : " + ComplaiencePAApprovedBy + " Date : " + item.PAVerifiedDate;
                    }
                }

                if (!string.IsNullOrEmpty(item.IsComplaienceIN.ToString()))
                {
                    string ComplaienceINApprovedBy = "";

                    var approvebylist = _userRepository.GetAll().Where(p => p.Id == item.INVerifiedBy).ToList();
                    ComplaienceINApprovedBy = approvebylist.Select(p => p.Name).FirstOrDefault() + " " + approvebylist.Select(p => p.Surname).FirstOrDefault();
                    if (item.IsComplaienceIN == true)
                    {
                        item.ComplaienceINMessage = "Approved By : " + ComplaienceINApprovedBy + " Date : " + item.INVerifiedDate;
                    }
                    if (item.IsComplaienceIN == false)
                    {
                        item.ComplaienceINMessage = "Rejected By : " + ComplaienceINApprovedBy + " Date : " + item.INVerifiedDate;
                    }
                }

                if (!string.IsNullOrEmpty(item.ISComplaienceIM.ToString()))
                {
                    string ComplaienceIMApprovedBy = "";
                    var approvebylist = _userRepository.GetAll().Where(p => p.Id == item.IMVerifiedBy).ToList();
                    ComplaienceIMApprovedBy = approvebylist.Select(p => p.Name).FirstOrDefault() + " " + approvebylist.Select(p => p.Surname).FirstOrDefault();
                    if (item.ISComplaienceIM == true)
                    {
                        item.ComplaienceIMMessage = "Approved By : " + ComplaienceIMApprovedBy + " Date : " + item.IMVerifiedDate;
                    }
                    if (item.ISComplaienceIM == false)
                    {
                        item.ComplaienceIMMessage = "Rejected By : " + ComplaienceIMApprovedBy + " Date : " + item.IMVerifiedDate;
                    }
                }

                if (!string.IsNullOrEmpty(item.IsComplaienceIS.ToString()))
                {
                    string ComplaienceISApprovedBy = "";
                    var approvebylist = _userRepository.GetAll().Where(p => p.Id == item.ISVerifiedBy).ToList();
                    ComplaienceISApprovedBy = approvebylist.Select(p => p.Name).FirstOrDefault() + " " + approvebylist.Select(p => p.Surname).FirstOrDefault();
                    if (item.IsComplaienceIS == true)
                    {
                        item.ComplaienceISMessage = "Approved By : " + ComplaienceISApprovedBy + " Date : " + item.ISVerifiedDate;
                    }
                    if (item.IsComplaienceIS == false)
                    {
                        item.ComplaienceISMessage = "Rejected By : " + ComplaienceISApprovedBy + " Date : " + item.ISVerifiedDate;
                    }
                }

                if (!string.IsNullOrEmpty(item.IsComplaienceCS.ToString()))
                {
                    string ComplaienceCSApprovedBy = "";
                    var approvebylist = _userRepository.GetAll().Where(p => p.Id == item.CSVerifiedBy).ToList();
                    ComplaienceCSApprovedBy = approvebylist.Select(p => p.Name).FirstOrDefault() + " " + approvebylist.Select(p => p.Surname).FirstOrDefault();
                    if (item.IsComplaienceCS == true)
                    {
                        item.ComplaienceCSMessage = "Approved By : " + ComplaienceCSApprovedBy + " Date : " + item.CSVerifiedDate;
                    }
                    if (item.IsComplaienceCS == false)
                    {
                        item.ComplaienceCSMessage = "Rejected By : " + ComplaienceCSApprovedBy + " Date : " + item.CSVerifiedDate;
                    }
                }

                if (!string.IsNullOrEmpty(item.IsComplaienceSTC.ToString()))
                {
                    string ComplaienceSTCApprovedBy = "";
                    var approvebylist = _userRepository.GetAll().Where(p => p.Id == item.STCVerifiedBy).ToList();
                    ComplaienceSTCApprovedBy = approvebylist.Select(p => p.Name).FirstOrDefault() + " " + approvebylist.Select(p => p.Surname).FirstOrDefault();
                    if (item.IsComplaienceSTC == true)
                    {
                        item.ComplaienceSTCMessage = "Approved By : " + ComplaienceSTCApprovedBy + " Date : " + item.STCVerifiedDate;
                    }
                    if (item.IsComplaienceSTC == false)
                    {
                        item.ComplaienceSTCMessage = "Rejected By : " + ComplaienceSTCApprovedBy + " Date : " + item.STCVerifiedDate;
                    }
                }

                if (!string.IsNullOrEmpty(item.IsComplaienceELE.ToString()))
                {
                    string ComplaienceELEApprovedBy = "";
                    var approvebylist = _userRepository.GetAll().Where(p => p.Id == item.ELEVerifiedBy).ToList();
                    ComplaienceELEApprovedBy = approvebylist.Select(p => p.Name).FirstOrDefault() + " " + approvebylist.Select(p => p.Surname).FirstOrDefault();
                    if (item.IsComplaienceELE == true)
                    {
                        item.ComplaienceELEMessage = "Approved By : " + ComplaienceELEApprovedBy + " Date : " + item.ELEVerifiedDate;
                    }
                    if (item.IsComplaienceELE == false)
                    {
                        item.ComplaienceELEMessage = "Rejected By : " + ComplaienceELEApprovedBy + " Date : " + item.ELEVerifiedDate;
                    }
                }

                if (!string.IsNullOrEmpty(item.IsComplaienceCUCK.ToString()))
                {
                    string ComplaienceCUCKApprovedBy = "";
                    var approvebylist = _userRepository.GetAll().Where(p => p.Id == item.CUCKVerifiedBy).ToList();
                    ComplaienceCUCKApprovedBy = approvebylist.Select(p => p.Name).FirstOrDefault() + " " + approvebylist.Select(p => p.Surname).FirstOrDefault();
                    if (item.IsComplaienceCUCK == true)
                    {
                        item.ComplaienceCUCKMessage = "Approved By : " + ComplaienceCUCKApprovedBy + " Date : " + item.CUCKVerifiedDate;
                    }
                    if (item.IsComplaienceCUCK == false)
                    {
                        item.ComplaienceCUCKMessage = "Rejected By : " + ComplaienceCUCKApprovedBy + " Date : " + item.CUCKVerifiedDate;
                    }
                }


            }
            return new PagedResultDto<ComplaienceJobsListDto>(resultCount, joblist.MapTo<List<ComplaienceJobsListDto>>());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs_Detail)]
        public async Task<CJDetailDto> GetJobsDetails(EntityDto input)
        {
            var job = (await _jobRepository.GetAsync(input.Id)).MapTo<CJDetailDto>();
            job.JobStatusName = _jobStatusRepository.GetAll().Where(p => p.Id == job.JobStatus_Id).Select(p => p.JobStatusName).FirstOrDefault();
            job.HouseTypeName = _houseTypeRepository.GetAll().Where(p => p.Id == job.HouseType).Select(p => p.HouseTypeName).FirstOrDefault();
            job.RoofTypeName = _roofTypeRepository.GetAll().Where(p => p.Id == job.RoofType).Select(p => p.RoofTypeName).FirstOrDefault();
            job.SolarTypeName = _solarTypeRepository.GetAll().Where(p => p.Id == job.SolarType_Id).Select(p => p.SolarTypeeName).FirstOrDefault();
            job.SystemTypeName = _systemTypeRepository.GetAll().Where(p => p.Id == job.SystemType_Id).Select(p => p.SystemTypeName).FirstOrDefault();
            job.RoofFacingPanelName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == job.RoofFacingPanel_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
            job.SugstthisaddressName = _yesNoRepository.GetAll().Where(p => p.Id == job.Sugstthisaddress_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.LocationOfNewSystemName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == job.LocationOfNewSystem_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                job.CreateForName = _installerRepository.GetAll().Where(p => p.Id == job.CreateFor_Id).Select(p => p.FullName).FirstOrDefault();
                job.InstallerName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Installer_Id).Select(p => p.FullName).FirstOrDefault();
                job.DesignerName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Designer_Id).Select(p => p.FullName).FirstOrDefault();
                job.ElectricianName = _installerEmployeRepository.GetAll().Where(p => p.Id == job.Electrician_Id).Select(p => p.FullName).FirstOrDefault();
            }
            job.TypeofsystemName = _typeOfSystemRepository.GetAll().Where(p => p.Id == job.Typeofsystem_Id).Select(p => p.TypeOfSystemName).FirstOrDefault();
            job.TypeofconnectionName = _typeOfConnectionRepository.GetAll().Where(p => p.Id == job.Typeofconnection_Id).Select(p => p.TypeOfConnectionName).FirstOrDefault();
            job.MountingtypeName = _mountingTypeRepository.GetAll().Where(p => p.Id == job.Mountingtype_Id).Select(p => p.MountingTypeName).FirstOrDefault();
            job.AuditreportavailableName = _yesNoRepository.GetAll().Where(p => p.Id == job.Auditreportavailable_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.InstallingcompleteunitName = _yesNoRepository.GetAll().Where(p => p.Id == job.Installingcompleteunit_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.RECYearName = _recyearRepository.GetAll().Where(p => p.Id == job.RECYear_Id).Select(p => p.REC_Year).FirstOrDefault();
            job.DefaultresourceavailfigureName = _yesNoRepository.GetAll().Where(p => p.Id == job.Defaultresourceavailfigure_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.CeatingCertName = _yesNoRepository.GetAll().Where(p => p.Id == job.CeatingCert_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.CECAccreditationStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.CECAccreditationStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.GovernmentSitingApprovalsName = _yesNoRepository.GetAll().Where(p => p.Id == job.GovernmentSitingApprovals_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.ElectricalSafetyDocumentationName = _yesNoRepository.GetAll().Where(p => p.Id == job.ElectricalSafetyDocumentation_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.AustralianStandardsStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.AustralianStandardsStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.NotgridConnectedStatementName = _yesNoRepository.GetAll().Where(p => p.Id == job.NotgridConnectedStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.SGUatThisAddressName = _yesNoRepository.GetAll().Where(p => p.Id == job.SGUatThisAddress_Id).Select(p => p.YesNoName).FirstOrDefault();
            job.SystemMountingTypeName = _systemMountingTypeRepository.GetAll().Where(p => p.Id == job.SystemMountingType_Id).Select(p => p.SystemMountingTypeName).FirstOrDefault();
            job.OwnerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == job.OwnerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
            job.CustomerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == job.CustomerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
            job.InverterModel = _inverterRepository.GetAll().Where(p => p.Id == job.Inverter_id).Select(p => p.ModelNumber).FirstOrDefault();
            job.PanelModel = _panelRepository.GetAll().Where(p => p.Id == job.Panel_id).Select(p => p.ModelNo).FirstOrDefault();
            return job;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs_Delete)]
        public async Task DeleteJobs(EntityDto input)
        {
            await _jobRepository.DeleteAsync(input.Id);
        }


        [UnitOfWork]
        public ListResultDto<PostCodesListDto> GetAllPostCodes()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var postcodes = _postCodeRepository
                .GetAll()
                .OrderByDescending(p => p.Id)
                .ToList();

                var postcodelist = postcodes.MapTo<List<PostCodesListDto>>();
                //foreach (var item in postcodelist)
                //{
                //    item.stateName = _stateRepository.GetAll().Where(p => p.Id == item.StateId).Select(p => p.StateName).FirstOrDefault();
                //    item.SuburbstatePostcodes = item.Suburb + "-" + item.stateName + "-" + item.Postcode;
                //}
                return new ListResultDto<PostCodesListDto>(postcodelist);
            }
        }

        [UnitOfWork]
        public ListResultDto<SystemTypeListDto> GetSystemType()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var SystemType = _systemTypeRepository
                .GetAll()
                .OrderByDescending(p => p.Id)
                .ToList();

                return new ListResultDto<SystemTypeListDto>(SystemType.MapTo<List<SystemTypeListDto>>());
            }
        }

        [UnitOfWork]
        public ListResultDto<TypeOfSystemListDto> GetTypeOfSystem()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var typeOfSystem = _typeOfSystemRepository
                .GetAll()
                .OrderByDescending(p => p.Id)
                .ToList();

                return new ListResultDto<TypeOfSystemListDto>(typeOfSystem.MapTo<List<TypeOfSystemListDto>>());
            }
        }

        public async Task<ListResultDto<InstallerListDto>> GetAllInstaller()
        {
            long userid = (long)_session.UserId;
            var user = await UserManager.GetUserByIdAsync(userid);
            var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);
            var Installer = new List<InstallerListDto>();
            var userspecific = grantedPermissions.Where(x => x.Name == "Pages.UserSpecificInstaller").ToList();
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var EmpList = _customerInstallerRepository
                    .GetAll()
                    .Where(c => c.Inst_Request_status == 1 && c.Request_status == 1 && c.IsCreated == false && c.TenantId == _session.TenantId)//
                    .Select(p => p.InstallerId)
                    .ToList();

                var installer = _installerRepository
                    .GetAll()
                    .WhereIf(
                        EmpList.Count != 0,
                        E => EmpList.Contains(E.Id) || E.TenantId == _session.TenantId
                        );

                if (userspecific.Count > 0)
                {
                    installer = installer.Where(x => x.CreatorUserId == userid);
                }

                var installerlist = (from Inst in installer
                                     join In in _customerInstallerRepository.GetAll() on Inst.Id equals In.InstallerId into InJoin
                                     from In in InJoin.DefaultIfEmpty()
                                     where (In.Inst_Request_status == 1 && In.Request_status == 1)
                                     group Inst by Inst into instGrouped
                                     select instGrouped.Key);

                return new ListResultDto<InstallerListDto>(installerlist.MapTo<List<InstallerListDto>>());
            }

        }

        [UnitOfWork]
        public ListResultDto<CJStateListDto> GetState()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var states = _stateRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

                return new ListResultDto<CJStateListDto>(states.MapTo<List<CJStateListDto>>());
            }
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task ApproveComplaiencePAPanels(CompliaenceJObbGuidInputDto input)
        {
            #region Approve Complaience PA
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaiencePA = true;
            job.PAVerifiedBy = (int)userid;
            job.PAVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);

            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task RejectComplaiencePAPanels(CompliaenceJObbGuidInputDto input)
        {
            #region Reject Complaience PA
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaiencePA = false;
            job.PAVerifiedBy = (int)userid;
            job.RejectReasonPA = input.RejectReason;
            job.PAVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);

            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task ApproveComplaienceInPanels(CompliaenceJObbGuidInputDto input)
        {
            #region Approve Complaience In
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceIN = true;
            job.INVerifiedBy = (int)userid;
            job.INVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);

            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task RejectComplaienceINPanels(CompliaenceJObbGuidInputDto input)
        {
            #region Reject Complaience IN
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceIN = false;
            job.INVerifiedBy = (int)userid;
            job.RejectReasonIN = input.RejectReason;
            job.INVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);

            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task ApproveComplaienceIMPanels(CompliaenceJObbGuidInputDto input)
        {
            #region Approve Complaience IM
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.ISComplaienceIM = true;
            job.IMVerifiedBy = (int)userid;
            job.IMVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);

            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task RejectComplaienceIMPanels(CompliaenceJObbGuidInputDto input)
        {
            #region Reject Complaience IM
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.ISComplaienceIM = false;
            job.IMVerifiedBy = (int)userid;
            job.RejectReasonIM = input.RejectReason;
            job.IMVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);

            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task ApproveComplaienceCS(CompliaenceJObbGuidInputDto input)
        {
            #region Approve Complaience CS
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceCS = true;
            job.CSVerifiedBy = (int)userid;
            job.CSVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);
            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task RejectComplaienceCS(CompliaenceJObbGuidInputDto input)
        {
            #region RejectComplaience CS
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceCS = false;
            job.CSVerifiedBy = (int)userid;
            job.RejectReasonCS = input.RejectReason;
            job.CSVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);

            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task ApproveComplaienceIS(CompliaenceJObbGuidInputDto input)
        {
            #region Approve Complaience IS
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceIS = true;
            job.ISVerifiedBy = (int)userid;
            job.ISVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);
            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task RejectComplaienceIS(CompliaenceJObbGuidInputDto input)
        {
            #region RejectComplaience IS
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceIS = false;
            job.ISVerifiedBy = (int)userid;
            job.RejectReasonIS = input.RejectReason;
            job.ISVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);

            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task ApproveComplaienceELE(CompliaenceJObbGuidInputDto input)
        {
            #region Approve Complaience ELE
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceELE = true;
            job.ELEVerifiedBy = (int)userid;
            job.ELEVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);
            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task RejectComplaienceELE(CompliaenceJObbGuidInputDto input)
        {
            #region RejectComplaience ELE
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceELE = false;
            job.ELEVerifiedBy = (int)userid;
            job.RejectReasonELE = input.RejectReason;
            job.ELEVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);

            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task ApproveComplaienceCUCK(CompliaenceJObbGuidInputDto input)
        {
            #region Approve Complaience CUCK
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceCUCK = true;
            job.CUCKVerifiedBy = (int)userid;
            job.CUCKVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);
            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task RejectComplaienceCUCK(CompliaenceJObbGuidInputDto input)
        {
            #region RejectComplaience CUCK
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceCUCK = false;
            job.CUCKVerifiedBy = (int)userid;
            job.RejectReasonCUCK = input.RejectReason;
            job.CUCKVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);

            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task ApproveComplaienceCEC(CompliaenceJObbGuidInputDto input)
        {
            #region Approve Complaience CEC
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceSTC = true;
            job.STCVerifiedBy = (int)userid;
            job.STCVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);
            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task RejectComplaienceCEC(CompliaenceJObbGuidInputDto input)
        {
            #region RejectComplaience CEC
            long userid = (long)_session.UserId;
            var job = await _jobRepository.GetAsync(input.JobId);
            job.IsComplaienceSTC = false;
            job.STCVerifiedBy = (int)userid;
            job.RejectReasonSTC = input.RejectReason;
            job.STCVerifiedDate = DateTime.Now;
            await _jobRepository.UpdateAsync(job);

            #endregion
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public JobRejectReason GetRejectReasonByJobId(CompliaenceJObbGuidInputDto input)
        {
            var invitation = new JobRejectReason();
            var job = _jobRepository.GetAll().Where(p => p.Id == input.JobId).FirstOrDefault();
            invitation.RejectReasonPA = job.RejectReasonPA;
            invitation.RejectReasonIN = job.RejectReasonIN;
            invitation.RejectReasonIM = job.RejectReasonIM;
            invitation.RejectReasonCS = job.RejectReasonCS;
            invitation.RejectReasonIS = job.RejectReasonIS;
            invitation.RejectReasonPK = job.RejectReasonPK;
            invitation.RejectReasonSTC = job.RejectReasonSTC;
            invitation.RejectReasonELE = job.RejectReasonELE;
            invitation.RejectReasonCUCK = job.RejectReasonCUCK;
            return invitation;
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task<PagedResultDto<AudioListDto>> GetAudio(GetAudioInput input)
        {
            var audioList = new List<AudioListDto>();

            var audio = _audioRepository
                .GetAll()
                .Where(A => A.JobId == input.jobId
                );

            var resultCount = await audio.CountAsync();            var results = await audio                .AsNoTracking()                .OrderBy(input.Sorting)                .PageBy(input)                .ToListAsync();

            audioList = results.MapTo<List<AudioListDto>>();
            foreach (var item in audioList)
            {
                item.FilePath = WebUrlService.WebSiteRootAddress + "Temp/Audio/" + item.FileName;
            }            return new PagedResultDto<AudioListDto>(resultCount, audioList.MapTo<List<AudioListDto>>());
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task CreateAudio(CreateAudioDto input)
        {
            var audio = input.MapTo<Audio>();
            await _audioRepository.InsertAsync(audio);
        }

        [AbpAuthorize(AppPermissions.Pages_Tenant_CompliaenceJobs)]
        public async Task DeleteAudio(EntityDto input)
        {
            await _audioRepository.DeleteAsync(input.Id);
        }

        public async Task ApplyForSTC(GetComplaienceExportExcelInputDto input)
        {
            var FileName = "";
            var job = _jobRepository
                    .GetAll()
                    .Where(J => J.IsComplaienceCS == true && J.IsComplaienceCUCK == true && J.IsComplaienceELE == true && J.ISComplaienceIM == true && J.IsComplaienceIN == true && J.IsComplaienceIS == true && J.IsComplaiencePA == true && J.IsComplaienceSTC == true);

            var jobListDtos = job.MapTo<List<ComplaienceJobsListExportExcel>>();

            if (input.filterJobId != null)
            {
                jobListDtos = jobListDtos.Where(p => p.Id.IsIn(input.filterJobId)).ToList(); // p.Id.IsIn(input.filterJobId));
            }

            GlobalJobList = jobListDtos;

            foreach (var item in jobListDtos)
            {
                item.FullName = item.FirstName + " " + item.LastName;
                item.StreetAddress = item.StreetAddress + ", " + item.StreetCity + ", " + item.StreetState + ", " + item.StreetPostCode;
                //item.JobStatusName = _jobStatusRepository.GetAll().Where(p => p.Id == item.JobStatus_Id).Select(p => p.JobStatusName).FirstOrDefault();
                DateTime? tempdate = item.ExpectedInstallDate;
                var applicationsTimezone = _settingManager.GetSettingValueForApplication(TimingSettingNames.TimeZone);
                var newdate = _timeZoneConverter.Convert(tempdate);
                item.ExpectedInstallShortDate = newdate.Value.ToString("dd/MM/yyyy");
                item.ExpectedInstallShortDate =item.ExpectedInstallDate.ToString("dd/MM/yyyy");
                if (item.SolarType_Id == 1)
                {
                    item.SolarTypeName = "Residential";
                }
                else
                {
                    item.SolarTypeName = "Commercial";
                }
                if (item.HouseType == 3)
                {
                    item.HouseTypeName = "Single story";
                }
                else
                {
                    item.HouseTypeName = "Multi story";
                }

                var PanelList = _scanedPanelRepository.GetAll().Where(P => P.Guid == item.Guid).Select(P => P.PanelSerialNumber).ToList();

                for (int i = 0; i < PanelList.Count; i++)
                {
                    if (i == 0)
                    {
                        item.PanelSerialNumberList = PanelList[i];
                    }
                    else
                    {
                        item.PanelSerialNumberList = item.PanelSerialNumberList + ";" + PanelList[i];
                    }

                }

                using (_unitOfWorkManager.Current.SetTenantId(null))
                {
                    item.StreetType = _streetTypeRepository.GetAll().Where(S => S.StreetTypeName == item.StreetType).Select(S => S.StreetTypeCode).FirstOrDefault();
                    item.InstallationStreetType = _streetTypeRepository.GetAll().Where(S => S.StreetTypeName == item.InstallationStreetType).Select(S => S.StreetTypeCode).FirstOrDefault();


                    var Createfor = _installerRepository.GetAll().Where(p => p.Id == item.CreateFor_Id).FirstOrDefault();
                    var Installer = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).FirstOrDefault();
                    var Designer = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).FirstOrDefault();
                    var Electrician = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Electrician_Id).FirstOrDefault();
                    var Tenant = _tenantRepository.GetAll().Where(T => T.Id == item.TenantId).FirstOrDefault();

                    //item.CreateForName = _installerRepository.GetAll().Where(p => p.Id == item.CreateFor_Id).Select(p => p.FullName).FirstOrDefault();
                    //item.InstallerName = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).Select(p => p.FullName).FirstOrDefault();
                    //item.DesignerName = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).Select(p => p.FullName).FirstOrDefault();
                    //item.ElectricianName = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Electrician_Id).Select(p => p.FullName).FirstOrDefault();

                    char[] spaceSeparator = new char[] { ' ' };
                    string[] InstName = new string[2];
                    string[] EleName = new string[2];
                    string[] DesignerName = new string[2];
                    if (!string.IsNullOrEmpty(Installer.FullName))
                    {
                        InstName = Installer.FullName.Split(spaceSeparator);
                        item.InstallerFName = InstName[0];
                    }
                    if (!string.IsNullOrEmpty(Electrician.FullName))
                    {
                        EleName = Electrician.FullName.Split(spaceSeparator);
                    }
                    if (!string.IsNullOrEmpty(Designer.FullName))
                    {
                        DesignerName = Designer.FullName.Split(spaceSeparator);
                    }

                    if (InstName.Length > 1)
                    {
                        item.InstallerLName = InstName[1];
                    }
                    item.ElectricianFName = EleName[0];
                    if (EleName.Length > 1)
                    {
                        item.ElectricianLName = EleName[1];
                    }
                    item.DesignerFName = DesignerName[0];
                    if (DesignerName.Length > 1)
                    {
                        item.DesignerLName = DesignerName[1];
                    }

                    //Installer Detail
                    item.InstallerPhone = Installer.Phone;
                    item.InstallerMobile = Installer.Mobile;
                    item.InstallerEmailId = Installer.EmailId;
                    item.InstallerFax = Installer.Fax;
                    item.InstallerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(P => P.Id == Installer.AddressType).Select(P => P.PostalAddressTypeName).FirstOrDefault();
                    item.InstallerPostalDelNo = Installer.PostalDelNo;
                    item.InstallerPostalDelTypeName = _postalDeliveryTypeRepository.GetAll().Where(P => P.Id == Installer.PostalDelType).Select(P => P.PostalDeliveryTypeName).FirstOrDefault();
                    item.InstallerUnitNo = Installer.UnitNo;
                    item.InstallerUnitType = Installer.UnitType;
                    item.InstallerStreetNumber = Installer.StreetNumber;
                    item.InstallerStreetName = Installer.StreetName;
                    item.InstallerStreetType = _streetTypeRepository.GetAll().Where(S => S.StreetTypeName == Installer.StreetType).Select(S => S.StreetTypeCode).FirstOrDefault();
                    item.InstallerSuburb = Installer.Suburb;
                    item.InstallerState = Installer.State;
                    item.InstallerPostCode = Installer.PostCode;
                    item.InstallerExpiry = Installer.InstallerExpiry;

                    //Designer Detail
                    item.DesignerPhone = Designer.Phone;
                    item.DesignerMobile = Designer.Mobile; ;
                    item.DesignerEmailId = Designer.EmailId; ;
                    item.DesignerFax = Designer.Fax;
                    item.DesignerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(P => P.Id == Designer.AddressType).Select(P => P.PostalAddressTypeName).FirstOrDefault();
                    item.DesignerPostalDelNo = Designer.PostalDelNo;
                    item.DesignerPostalDelTypeName = _postalDeliveryTypeRepository.GetAll().Where(P => P.Id == Designer.PostalDelType).Select(P => P.PostalDeliveryTypeName).FirstOrDefault();
                    item.DesignerUnitNo = Designer.UnitNo;
                    item.DesignerUnitType = Designer.UnitType;
                    item.DesignerStreetNumber = Designer.StreetNumber;
                    item.DesignerStreetName = Designer.StreetName;
                    item.DesignerStreetType = _streetTypeRepository.GetAll().Where(S => S.StreetTypeName == Designer.StreetType).Select(S => S.StreetTypeCode).FirstOrDefault();
                    item.DesignerSuburb = Designer.Suburb;
                    item.DesignerState = Designer.State;
                    item.DesignerPostCode = Designer.PostCode;
                    item.DesignerExpiry = Designer.DesignerExpiry;

                    //Electraction Detail
                    item.ElectractionPhone = Electrician.Phone;
                    item.ElectractionMobile = Electrician.Mobile;
                    item.ElectractionEmailId = Electrician.EmailId;
                    item.ElectractionFax = Electrician.Fax;
                    item.ElectractionAddressTypeName = _postalAddressTypeRepository.GetAll().Where(P => P.Id == Electrician.AddressType).Select(P => P.PostalAddressTypeName).FirstOrDefault();
                    item.ElectractionPostalDelNo = Electrician.PostalDelNo;
                    item.ElectractionPostalDelTypeName = _postalDeliveryTypeRepository.GetAll().Where(P => P.Id == Electrician.PostalDelType).Select(P => P.PostalDeliveryTypeName).FirstOrDefault();
                    item.ElectractionUnitNo = Electrician.UnitNo;
                    item.ElectractionUnitType = Electrician.UnitType;
                    item.ElectractionStreetNumber = Electrician.StreetNumber;
                    item.ElectractionStreetName = Electrician.StreetName;
                    item.ElectractionStreetType = _streetTypeRepository.GetAll().Where(S => S.StreetTypeName == Electrician.StreetType).Select(S => S.StreetTypeCode).FirstOrDefault();
                    item.ElectractionSuburb = Electrician.Suburb;
                    item.ElectractionState = Electrician.State;
                    item.ElectractionPostCode = Electrician.PostCode;
                    item.ElectractionExpiry = Electrician.ElectricalExpiry;

                    //Tenant Detail
                    item.RetailerABN = Tenant.RetailerABN;
                    item.RetailerName = Tenant.RetailerName;


                    item.HouseTypeName = _houseTypeRepository.GetAll().Where(p => p.Id == item.HouseType).Select(p => p.HouseTypeName).FirstOrDefault();
                    item.RoofTypeName = _roofTypeRepository.GetAll().Where(p => p.Id == item.RoofType).Select(p => p.RoofTypeName).FirstOrDefault();
                    //item.SolarTypeName = _solarTypeRepository.GetAll().Where(p => p.Id == item.SolarType_Id).Select(p => p.SolarTypeeName).FirstOrDefault();
                    item.SystemTypeName = _systemTypeRepository.GetAll().Where(p => p.Id == item.SystemType_Id).Select(p => p.SystemTypeName).FirstOrDefault();
                    item.RoofFacingPanelName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == item.RoofFacingPanel_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
                    item.SugstthisaddressName = _yesNoRepository.GetAll().Where(p => p.Id == item.Sugstthisaddress_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.LocationOfNewSystemName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == item.LocationOfNewSystem_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();

                    item.TypeofsystemName = _typeOfSystemRepository.GetAll().Where(p => p.Id == item.Typeofsystem_Id).Select(p => p.TypeOfSystemName).FirstOrDefault();
                    item.TypeofconnectionName = _typeOfConnectionRepository.GetAll().Where(p => p.Id == item.Typeofconnection_Id).Select(p => p.TypeOfConnectionName).FirstOrDefault();
                    item.MountingtypeName = _mountingTypeRepository.GetAll().Where(p => p.Id == item.Mountingtype_Id).Select(p => p.MountingTypeName).FirstOrDefault();
                    item.AuditreportavailableName = _yesNoRepository.GetAll().Where(p => p.Id == item.Auditreportavailable_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.InstallingcompleteunitName = _yesNoRepository.GetAll().Where(p => p.Id == item.Installingcompleteunit_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.RECYearName = _recyearRepository.GetAll().Where(p => p.Id == item.RECYear_Id).Select(p => p.REC_Year).FirstOrDefault();
                    item.DefaultresourceavailfigureName = _yesNoRepository.GetAll().Where(p => p.Id == item.Defaultresourceavailfigure_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.CeatingCertName = _yesNoRepository.GetAll().Where(p => p.Id == item.CeatingCert_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.CECAccreditationStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.CECAccreditationStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.GovernmentSitingApprovalsName = _yesNoRepository.GetAll().Where(p => p.Id == item.GovernmentSitingApprovals_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.ElectricalSafetyDocumentationName = _yesNoRepository.GetAll().Where(p => p.Id == item.ElectricalSafetyDocumentation_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.AustralianStandardsStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.AustralianStandardsStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.NotgridConnectedStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.NotgridConnectedStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.SGUatThisAddressName = _yesNoRepository.GetAll().Where(p => p.Id == item.SGUatThisAddress_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.SystemMountingTypeName = _systemMountingTypeRepository.GetAll().Where(p => p.Id == item.SystemMountingType_Id).Select(p => p.SystemMountingTypeName).FirstOrDefault();
                    item.OwnerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == item.OwnerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
                    item.CustomerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == item.CustomerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
                    item.PanelModel = _panelRepository.GetAll().Where(p => p.Id == item.Panel_id).Select(p => p.ModelNo).FirstOrDefault();
                    item.InverterModel = _inverterRepository.GetAll().Where(p => p.Id == item.Inverter_id).Select(p => p.ModelNumber).FirstOrDefault();
                }
            }
            bool uploadonREC = false;
            //Generat CSV Code
            try
            {
                var csv = new StringBuilder();

                var TypeOfSystem = "Type of system";
                var InstallationDate = "Installation date";
                var PanelBrand = "System/panel brand";
                var PanelModel = "System/panel model";
                var InverterMan = "Inverter manufacturer";
                var InverterSeries = "Inverter series";
                var InvModel = "Inverter model number";
                var TypeofConnection = "Type of connection to the electricity grid";
                var stsremmounting = "System mounting type";
                var audit = "Is a site specific audit report available?";
                var completeunit = "Are you installing a complete unit (adding capacity to an existing system is not considered a complete unit)?";
                var aditionalcapacity = "If this system is additional capacity to an existing system please provide detailed information on the position of the new panels and inverter (if applicable). System upgrades without a note explaining new panel locations will be failed by the Clean Energy Regulator";
                var createREC = "For what period would you like to create RECs";
                var poweroutput = "What is the rated power output (in kW) of your small generation unit";
                var NumberofPanels = "Number of panels";
                var resourceavailability = "Do you wish to use the default resource availability figure?";
                var hoursperannum = "What is your resource availability (hours per annum) for your system?";
                var Equipmentmodelserialnumber = "Equipment model serial number(s)";
                var creatingcertificates = "Are you creating certificates for a system that has previously been failed by the Clean Energy Regulator?";
                var Accreditationcode = "Accreditation code";
                var recreatingcertificates = "Explanatory notes for re-creating certificates previously failed";
                var Reference = "Reference";
                var Certificatetag = "Certificate tag";
                var accreditation = "CEC accreditation statement";
                var Territorygovernment = "\"Local, State and Territory government siting approvals\"";
                var Electricalsafety = "Electrical safety documentation";
                var AustralianNewZealand = "Australian/New Zealand standards statement";
                var Notgridconnectedstatement = "Not grid-connected statement";
                var Installationpropertytype = "Installation property type";
                var Singleormultistory = "Single or multi-story";
                var Installationpropertyname = "\"Installation property name, boat name or chassis number\"";
                var Installationunittype = "Installation unit type";
                var Installationunitnumber = "Installation unit number";
                var Installationstreetnumber = "Installation street number";
                var Installationstreetname = "Installation street name";
                var Installationstreettype = "Installation street type";
                var InstallationtownSuburb = "Installation town/Suburb";
                var Installationstate = "Installation state";
                var Installationpostcode = "Installation postcode";
                var Installationlatitude = "Installation latitude";
                var Installationlongitude = "Installation longitude";
                var morethanoneSGU = "Is there more than one SGU at this address?";
                var addressentered = "If the address entered above does not adequately describe the location of the system please provide further detailed information for the Clean Energy Regulator to locate the system";
                var Additionalsysteminformation = "Additional system information";
                var Ownertype = "Owner type";
                var Ownerorganisationname = "Owner organisation name";
                var Ownerfirstname = "Owner first name";
                var Ownersurname = "Owner surname";
                var Ownerphone = "Owner phone";
                var Ownerfax = "Owner fax";
                var Ownermobile = "Owner mobile";
                var Owneremail = "Owner email";
                var Owneraddresstype = "Owner address type";
                var Ownerpostaldeliverytype = "Owner postal delivery type";
                var Ownerpostaldeliverynumber = "Owner postal delivery number";
                var Ownerunittype = "Owner unit type";
                var Ownerunitnumber = "Owner unit number";
                var Ownerstreetnumber = "Owner street number";
                var Ownerstreetname = "Owner street name";
                var Ownerstreettype = "Owner street type";
                var Ownertownsuburb = "Owner town/suburb";
                var Ownerstate = "Owner state";
                var Ownerpostcode = "Owner postcode";
                var Ownercountry = "Owner country";
                var Installerfirstname = "Installer first name";
                var Installersurname = "Installer surname";
                var CECaccreditedinstallernumber = "CEC accredited installer number";
                var Installerphone = "Installer phone";
                var Installerfax = "Installer fax";
                var Installermobile = "Installer mobile";
                var Installeremail = "Installer email";
                var Installeraddresstype = "Installer address type";
                var Installerpostaldeliverytype = "Installer postal delivery type";
                var Installerpostaldeliverynumber = "Installer postal delivery number";
                var Installerunittype = "Installer unit type";
                var Installerunitnumber = "Installer unit number";
                var Installerstreetnumber = "Installer street number";
                var Installerstreetname = "Installer street name";
                var Installerstreettype = "Installer street type";
                var Installertownsuburb = "Installer town/suburb";
                var Installerstate = "Installer state";
                var Installerpostcode = "Installer postcode";
                var Electricianfirstname = "Electrician first name";
                var Electriciansurname = "Electrician surname";
                var Licensedelectriciannumber = "Licensed electrician number";
                var Electricianphone = "Electrician phone";
                var Electricianfax = "Electrician fax";
                var Electricianmobile = "Electrician mobile";
                var Electricianemail = "Electrician email";
                var Electricianaddresstype = "Electrician address type";
                var Electricianpostaldeliverytype = "Electrician postal delivery type";
                var Electricianpostaldeliverynumber = "Electrician postal delivery number";
                var Electricianunittype = "Electrician unit type";
                var Electricianunitnumber = "Electrician unit number";
                var Electricianstreetnumber = "Electrician street number";
                var Electricianstreetname = "Electrician street name";
                var Electricianstreettype = "Electrician street type";
                var Electriciantownsuburb = "Electrician town/suburb";
                var Electricianstate = "Electrician state";
                var Electricianpostcode = "Electrician postcode";
                var Designerfirstname = "Designer first name";
                var Designersurname = "Designer surname";
                var CECaccrediteddesignernumber = "CEC accredited designer number";
                var Designerphone = "Designer phone";
                var Designerfax = "Designer fax";
                var Designermobile = "Designer mobile";
                var Designeremail = "Designer email";
                var Designeraddresstype = "Designer address type";
                var Designerpostaldeliverytype = "Designer postal delivery type";
                var Designerpostaldeliverynumber = "Designer postal delivery number";
                var Designerunittype = "Designer unit type";
                var Designerunitnumber = "Designer unit number";
                var Designerstreetnumber = "Designer street number";
                var Designerstreetname = "Designer street name";
                var Designerstreettype = "Designer street type";
                var Designertownsuburb = "Designer town/suburb";
                var Designerstate = "Designer state";
                var Designerpostcode = "Designer postcode";
                var RetailerName = "Retailer Name";
                var RetailerABN = "Retailer ABN";
                var Nationalmeteringidentifier = "National metering identifier (NMI)";
                var Batterystoragemanufacturer = "Battery storage manufacturer";
                var Batterystoragemodel = "Battery storage model";
                var batterysystem = "Is the battery system part of an aggregated control?";
                var defaultmanufacturer = "Has the installer changed default manufacturer setting of the battery storage system?";
                var SignedDataPackage = "Signed Data Package";
                var DocumentsZipFile = "Documents Zip File";

                var Header = $"{ TypeOfSystem},{ InstallationDate},{ PanelBrand},{ PanelModel},{ InverterMan},{ InverterSeries},{ InvModel},{ TypeofConnection},{ stsremmounting},{ audit},{ completeunit},{ aditionalcapacity},{ createREC},{ poweroutput},{ NumberofPanels},{ resourceavailability},{ hoursperannum},{ Equipmentmodelserialnumber},{ creatingcertificates},{ Accreditationcode},{ recreatingcertificates},{ Reference},{ Certificatetag},{ accreditation},{ Territorygovernment},{ Electricalsafety},{ AustralianNewZealand},{ Notgridconnectedstatement},{ Installationpropertytype},{ Singleormultistory},{ Installationpropertyname},{ Installationunittype},{ Installationunitnumber},{ Installationstreetnumber},{ Installationstreetname},{ Installationstreettype},{ InstallationtownSuburb},{ Installationstate},{ Installationpostcode},{ Installationlatitude},{ Installationlongitude},{ morethanoneSGU},{ addressentered },{ Additionalsysteminformation},{ Ownertype},{ Ownerorganisationname},{ Ownerfirstname},{ Ownersurname},{ Ownerphone},{ Ownerfax},{ Ownermobile},{ Owneremail},{ Owneraddresstype},{ Ownerpostaldeliverytype},{ Ownerpostaldeliverynumber},{ Ownerunittype},{ Ownerunitnumber},{ Ownerstreetnumber},{ Ownerstreetname},{ Ownerstreettype},{ Ownertownsuburb},{ Ownerstate},{ Ownerpostcode},{ Ownercountry},{ Installerfirstname},{ Installersurname},{ CECaccreditedinstallernumber},{ Installerphone},{ Installerfax},{ Installermobile},{ Installeremail},{ Installeraddresstype},{ Installerpostaldeliverytype},{ Installerpostaldeliverynumber},{ Installerunittype},{ Installerunitnumber},{ Installerstreetnumber},{ Installerstreetname},{ Installerstreettype},{ Installertownsuburb},{ Installerstate},{ Installerpostcode},{ Electricianfirstname},{ Electriciansurname},{ Licensedelectriciannumber},{ Electricianphone},{ Electricianfax},{ Electricianmobile},{ Electricianemail},{ Electricianaddresstype},{ Electricianpostaldeliverytype},{ Electricianpostaldeliverynumber},{ Electricianunittype},{ Electricianunitnumber},{ Electricianstreetnumber},{ Electricianstreetname},{ Electricianstreettype},{ Electriciantownsuburb},{ Electricianstate},{ Electricianpostcode},{ Designerfirstname},{ Designersurname},{ CECaccrediteddesignernumber},{ Designerphone},{ Designerfax},{ Designermobile},{ Designeremail},{ Designeraddresstype},{ Designerpostaldeliverytype},{ Designerpostaldeliverynumber},{ Designerunittype},{ Designerunitnumber},{ Designerstreetnumber},{ Designerstreetname},{ Designerstreettype},{ Designertownsuburb},{ Designerstate},{ Designerpostcode},{ RetailerName},{ RetailerABN},{ Nationalmeteringidentifier},{ Batterystoragemanufacturer},{ Batterystoragemodel},{ batterysystem},{ defaultmanufacturer},{ SignedDataPackage},{ DocumentsZipFile}";


                csv.AppendLine(Header);
                //in your loop
                foreach (var item in jobListDtos)
                {
                    var Blank = "";
                    var NO = "NO";
                    var AU = "AU";
                    //var newLine = $"{item.TypeofsystemName},{item.ExpectedInstallShortDate},{item.BrandName},{item.PanelModel},{item.InverterBrandName},{item.InverterSeries},{item.InverterModel},{item.TypeofconnectionName},{item.MountingtypeName}, {Blank},{item.InstallingcompleteunitName},{item.AddcapMoredetailed},{item.RECYearName},{item.SystemCapacity},{item.NoOfPanels},{item.DefaultresourceavailfigureName},{item.ResourceAvailability},{item.PanelSerialNumberList},{item.CeatingCertName},{item.AccreditationCode},{item.ExplanatoryNotes},{item.ProjectNumber},{Blank},{item.CECAccreditationStatementName},{item.GovernmentSitingApprovalsName},{item.ElectricalSafetyDocumentationName},{item.AustralianStandardsStatementName},{item.NotgridConnectedStatementName},{item.SolarTypeName},{item.HouseTypeName},{item.InstallPropertyName},{item.InstallationUnitType},{item.InstallationUnitNo},{item.InstallationStreetNo},{item.InstallationStreetName},{item.InstallationStreetType},{item.InstallationStreetCity},{item.InstallationStreetState},{item.InstallationStreetPostCode},{Blank},{Blank},{ NO},{ Blank},{ item.AdditionalSystemInformation},{ item.SystemMountingTypeName},{ item.Company},{ item.FirstName},{ item.LastName},{ item.Phone},{ item.Fax},{ item.Phone},{ item.EmailId},{ item.PostalAddressType},{ item.PostalDeliveryType},{ item.PostalDeliveryNumber},{ item.UnitType},{ item.UnitNo},{ item.StreetNo},{ item.StreetName},{ item.StreetType},{ item.StreetCity},{ item.StreetState},{ item.StreetPostCode},{ AU},{ item.InstallerFName},{ item.InstallerLName},{ item.InstallerExpiry},{ item.InstallerPhone},{ item.InstallerFax},{ item.InstallerMobile},{ item.InstallerEmailId},{ item.InstallerAddressTypeName},{ item.InstallerPostalDelTypeName},{ item.InstallerPostalDelNo},{ item.InstallerUnitType},{ item.InstallerUnitNo},{ item.InstallerStreetNumber},{ item.InstallerStreetName},{ item.InstallerStreetType},{ item.InstallerSuburb},{ item.InstallerState},{ item.InstallerPostCode},{ item.ElectricianFName},{ item.ElectricianLName},{ item.ElectractionExpiry},{ item.ElectractionPhone},{ item.ElectractionFax},{ item.ElectractionMobile},{ item.ElectractionEmailId},{ item.ElectractionAddressTypeName},{ item.ElectractionPostalDelTypeName},{ item.ElectractionPostalDelNo},{ item.ElectractionUnitType},{ item.ElectractionUnitNo},{ item.ElectractionStreetNumber},{ item.ElectractionStreetName},{ item.ElectractionStreetType},{ item.ElectractionSuburb},{ item.ElectractionState},{ item.ElectractionPostCode},{ item.DesignerFName},{ item.DesignerLName},{ item.DesignerExpiry},{ item.DesignerPhone},{ item.DesignerFax},{ item.DesignerMobile},{ item.DesignerEmailId},{ item.DesignerAddressTypeName},{ item.DesignerPostalDelTypeName},{ item.DesignerPostalDelNo},{ item.DesignerUnitType},{ item.DesignerUnitNo},{ item.DesignerStreetNumber},{ item.DesignerStreetName},{ item.DesignerStreetType},{ item.DesignerSuburb},{ item.DesignerState},{ item.DesignerPostCode},{ item.RetailerName},{ item.RetailerABN},{ item.NMINo},{ Blank},{ Blank},{ Blank},{ Blank},{ Blank},{ Blank}";
                    var newLine = $"{item.TypeofsystemName},{item.ExpectedInstallShortDate},{item.BrandName},{item.PanelModel},{item.InverterBrandName},{item.InverterSeries},{item.InverterModel},{item.TypeofconnectionName},{item.MountingtypeName}, {Blank},{item.InstallingcompleteunitName},{item.AddcapMoredetailed},{item.RECYearName},{item.SystemCapacity},{item.NoOfPanels},{item.DefaultresourceavailfigureName},{item.ResourceAvailability},{item.PanelSerialNumberList},{item.CeatingCertName},{item.AccreditationCode},{item.ExplanatoryNotes},{item.ProjectNumber},{Blank},{item.CECAccreditationStatementName},{item.GovernmentSitingApprovalsName},{item.ElectricalSafetyDocumentationName},{item.AustralianStandardsStatementName},{item.NotgridConnectedStatementName},{item.SolarTypeName},{item.HouseTypeName},{item.InstallPropertyName},{item.InstallationUnitType},{item.InstallationUnitNo},{item.InstallationStreetNo},{item.InstallationStreetName},{item.InstallationStreetType},{item.InstallationStreetCity},{item.InstallationStreetState},{item.InstallationStreetPostCode},{Blank},{Blank},{ NO},{ Blank},{ item.AdditionalSystemInformation},{ item.SystemMountingTypeName},{Blank},{ item.FirstName},{ item.LastName},{ item.Phone},{ item.Fax},{ item.Phone},{ item.EmailId},{ item.PostalAddressType},{ item.PostalDeliveryType},{ item.PostalDeliveryNumber},{ item.UnitType},{ item.UnitNo},{ item.StreetNo},{ item.StreetName},{ item.StreetType},{ item.StreetCity},{ item.StreetState},{ item.StreetPostCode},{ AU},{ item.InstallerFName},{ item.InstallerLName},{ item.InstallerExpiry},{ item.InstallerPhone},{ item.InstallerFax},{ item.InstallerMobile},{ item.InstallerEmailId},{ item.InstallerAddressTypeName},{ item.InstallerPostalDelTypeName},{ item.InstallerPostalDelNo},{ item.InstallerUnitType},{ item.InstallerUnitNo},{ item.InstallerStreetNumber},{ item.InstallerStreetName},{ item.InstallerStreetType},{ item.InstallerSuburb},{ item.InstallerState},{ item.InstallerPostCode},{ item.ElectricianFName},{ item.ElectricianLName},{ item.ElectractionExpiry},{ item.ElectractionPhone},{ item.ElectractionFax},{ item.ElectractionMobile},{ item.ElectractionEmailId},{ item.ElectractionAddressTypeName},{ item.ElectractionPostalDelTypeName},{ item.ElectractionPostalDelNo},{ item.ElectractionUnitType},{ item.ElectractionUnitNo},{ item.ElectractionStreetNumber},{ item.ElectractionStreetName},{ item.ElectractionStreetType},{ item.ElectractionSuburb},{ item.ElectractionState},{ item.ElectractionPostCode},{ item.DesignerFName},{ item.DesignerLName},{ item.DesignerExpiry},{ item.DesignerPhone},{ item.DesignerFax},{ item.DesignerMobile},{ item.DesignerEmailId},{ item.DesignerAddressTypeName},{ item.DesignerPostalDelTypeName},{ item.DesignerPostalDelNo},{ item.DesignerUnitType},{ item.DesignerUnitNo},{ item.DesignerStreetNumber},{ item.DesignerStreetName},{ item.DesignerStreetType},{ item.DesignerSuburb},{ item.DesignerState},{ item.DesignerPostCode},{ item.RetailerName},{ item.RetailerABN},{ item.NMINo},{ Blank},{ Blank},{ Blank},{ Blank},{ Blank},{ Blank}";

                    csv.AppendLine(newLine);
                }
                //after your loop
                FileName = DateTime.Now.Ticks + "_ComplienceJobList.csv";
                var filePath = Path.Combine(_appFolders.ComplienceDownloadFolder, FileName);
                File.WriteAllText(filePath, csv.ToString());

                var fileBytes = System.IO.File.ReadAllBytes(filePath);
                System.IO.File.Delete(filePath);

                if (!string.IsNullOrEmpty(_appFolders.ComplienceDownloadFolder))
                {
                    string newfilePath = _appFolders.ComplienceDownloadFolder + "\\" + FileName;
                    File.WriteAllBytes(newfilePath, fileBytes);
                }

                foreach (var item in jobListDtos)
                {
                    var UpdateFileName = _jobRepository.GetAll().Where(J => J.Id == item.Id).FirstOrDefault();
                    UpdateFileName.RECUploadFileName = FileName;
                    UpdateFileName.RECUploadDate = DateTime.Now;
                    _jobRepository.UpdateAsync(UpdateFileName);
                }
                uploadonREC = true;
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("OOps1...!", ex.Message.ToString());
            }
            if(uploadonREC)
            {
                //FileName = "data2.csv";
                UploadCSV(_appFolders.ComplienceDownloadFolder, FileName);
            }
        }
        
        public void UploadCSV(string filePath, string filename)
        {
            //IWebDriver driver = new ChromeDriver("D:\\driver");
            //driver.Navigate().GoToUrl("https://www.rec-registry.gov.au/rec-registry/app/home");
            //driver.FindElement(By.Name("j_username")).SendKeys("BAXIN56338");
            //driver.FindElement(By.Name("j_password")).SendKeys("Arise@746");
            //driver.FindElement(By.ClassName("btn")).Click();
            //driver.Navigate().GoToUrl("https://www.rec-registry.gov.au/rec-registry/app/smallunits/register-bulk-small-generation-unit");
            //driver.FindElement(By.Name("sguBulkUploadFile")).SendKeys(filePath + "\\" + filename);
            //driver.FindElement(By.Id("sgu-file-upload-submit")).Click();

            var Details = _recUserRepository.GetAll().FirstOrDefault();

            IWebDriver driver = new ChromeDriver("D:\\driver");
            driver.Navigate().GoToUrl("https://test.rec-registry.gov.au/rec-registry/app/home");
			//driver.FindElement(By.Name("j_username")).SendKeys("AGGAA59332");
			//driver.FindElement(By.Name("j_password")).SendKeys("Njdp@3539");
			driver.FindElement(By.Name("j_username")).SendKeys(Details.RecUserName);
			driver.FindElement(By.Name("j_password")).SendKeys(Details.RecPasswprd);
			driver.FindElement(By.ClassName("btn")).Click();
            driver.Navigate().GoToUrl("https://test.rec-registry.gov.au/rec-registry/app/smallunits/register-bulk-small-generation-unit");
            driver.FindElement(By.Name("sguBulkUploadFile")).SendKeys(filePath + "\\" + filename);
            driver.FindElement(By.Id("sgu-file-upload-submit")).Click();

            bool sucmsg = false;
            bool errormsg = false;
            string uploadError = string.Empty;
            while (sucmsg == false)
            {
                var elementdisplay = driver.FindElement(By.Id("success"));
                sucmsg = elementdisplay.Displayed;
                if (sucmsg == false)
                {
                    var displayerror = driver.FindElement(By.Id("error"));
                    if (displayerror.Displayed)
                    {
                        sucmsg = true;
                        errormsg = true;
                        uploadError = displayerror.GetAttribute("innerHTML");
                    }
                }
            }
            if (sucmsg == true && errormsg == false)
            {
                string[] msgarr = driver.FindElement(By.Id("success")).GetAttribute("innerHTML").Split("<br>");
                if (msgarr.Length == 3)
                {
                    string bulkuploadid = msgarr[1].Replace("Bulk upload ID: ", "");
                    //Update BulkuploadID & NoofCertificate success
                    foreach (var item in GlobalJobList)
                    {
                        var UpdateFileName = _jobRepository.GetAll().Where(J => J.Id == item.Id).FirstOrDefault();
                        UpdateFileName.BulkUploadID = bulkuploadid;
                        _jobRepository.UpdateAsync(UpdateFileName);
                    }

                    driver.Navigate().GoToUrl("https://test.rec-registry.gov.au/rec-registry/app/smallunits/small-generation-unit-search");
                    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    driver.FindElement(By.Name("bulkUploadId")).SendKeys(bulkuploadid);
                    driver.FindElement(By.Id("search-button")).Click();
                    //driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
                    bool sucmsgpvd = false;
                    while (sucmsgpvd == false)
                    {
                        var elementdisplaystc = driver.FindElement(By.Id("search-sgu-table"));
                        sucmsgpvd = elementdisplaystc.Displayed;
                    }
                    var rows = driver.FindElements(By.XPath(".//*[@id='search-sgu-table']/tbody/tr"));
                    Console.WriteLine("row count: " + rows.Count());
                    for (int rowindex = 1; rowindex <= rows.Count(); rowindex++)
                    {
                        var serials = driver.FindElement(By.XPath(".//*[@id='search-sgu-table']/tbody/tr[" + rowindex + "]/td[7]")).Text;
                        var applieddate = driver.FindElement(By.XPath(".//*[@id='search-sgu-table']/tbody/tr[" + rowindex + "]/td[10]")).Text;
                        var noofcerti = driver.FindElement(By.XPath(".//*[@id='search-sgu-table']/tbody/tr[" + rowindex + "]/td[11]")).Text;
                        var certipassed = driver.FindElement(By.XPath(".//*[@id='search-sgu-table']/tbody/tr[" + rowindex + "]/td[12]")).Text;
                        var certipending = driver.FindElement(By.XPath(".//*[@id='search-sgu-table']/tbody/tr[" + rowindex + "]/td[13]")).Text;
                        var certifailed = driver.FindElement(By.XPath(".//*[@id='search-sgu-table']/tbody/tr[" + rowindex + "]/td[14]")).Text;
                        var certiregistered = driver.FindElement(By.XPath(".//*[@id='search-sgu-table']/tbody/tr[" + rowindex + "]/td[15]")).Text;
                        var pvdnumber = driver.FindElement(By.XPath(".//*[@id='search-sgu-table']/tbody/tr[" + rowindex + "]/td[1]")).Text;
                        var pvdstatus = "Pending";
                        string totalstc = certipending;
                        if (Convert.ToInt32(certipassed) > 0)
                        {
                            pvdstatus = "Approved";
                            totalstc = certipassed;
                        }
                        if (Convert.ToInt32(certifailed) > 0)
                        {
                            pvdstatus = "Failed";
                            totalstc = certifailed;
                        }
                        //Update PVDNumber,PVDStatus,STC
                        string[] strialsarr = serials.Split(',');
                        if(strialsarr.Length>0)
                        {
                            var SerialNUmber = strialsarr[0];
                            var JobGUid = _scanedPanelRepository.GetAll().Where(P => P.PanelSerialNumber == SerialNUmber).Select(P => P.Guid).FirstOrDefault();

                            var UpdateFileName = _jobRepository.GetAll().Where(J => J.Guid == JobGUid).FirstOrDefault();
                            UpdateFileName.PVDNumber = pvdnumber;
                            UpdateFileName.PVDStatus = pvdstatus;
                            decimal? total = Math.Floor(Convert.ToDecimal(totalstc));
                            UpdateFileName.TotalSTC = total;
                            decimal? registred = Math.Floor(Convert.ToDecimal(noofcerti));
                            UpdateFileName.RegistredSTC = registred;
                            UpdateFileName.NumberOfCertificate = noofcerti;
                            UpdateFileName.STCAppliedDate = DateTime.Now;
                            UpdateFileName.JobStatus_Id = 2;
                            _jobRepository.UpdateAsync(UpdateFileName);
                        }
                    }
                }
                else
                {
                    throw new UserFriendlyException("OOps...!", "Please try again later");
                }
            }
            else
            {
                throw new UserFriendlyException("OOps...!", uploadError);
            }

            driver.Close();
        }

        public static String GetTimestamp(DateTime value)
        {
            return value.ToString("yyyyMMddHHmmssffff");
        }

        public string AppendTenantName(int TenantId, string Url)
        {
            var TenantName = _tenantRepository.GetAll().Where(T => T.Id == TenantId).Select(T => T.TenancyName).FirstOrDefault();
            string NewUrl = Url.Replace("{TENANCY_NAME}", TenantName);

            return NewUrl;
        }

        public string DownloadCSV(GetComplaienceExportExcelInputDto input)
        {
            var FileName = "";
            string newfilePath = "";
            var job = _jobRepository
                    .GetAll()
                    .Where(J => J.IsComplaienceCS == true && J.IsComplaienceCUCK == true && J.IsComplaienceELE == true && J.ISComplaienceIM == true && J.IsComplaienceIN == true && J.IsComplaienceIS == true && J.IsComplaiencePA == true && J.IsComplaienceSTC == true);

            var jobListDtos = job.MapTo<List<ComplaienceJobsListExportExcel>>();

            if (input.filterJobId != null)
            {
                jobListDtos = jobListDtos.Where(p => p.Id.IsIn(input.filterJobId)).ToList(); // p.Id.IsIn(input.filterJobId));
            }

            GlobalJobList = jobListDtos;

            foreach (var item in jobListDtos)
            {
                item.FullName = item.FirstName + " " + item.LastName;
                item.StreetAddress = item.StreetAddress + ", " + item.StreetCity + ", " + item.StreetState + ", " + item.StreetPostCode;
                DateTime? tempdate = item.ExpectedInstallDate;
                var applicationsTimezone = _settingManager.GetSettingValueForApplication(TimingSettingNames.TimeZone);
                var newdate = _timeZoneConverter.Convert(tempdate);
                item.ExpectedInstallShortDate = newdate.Value.ToString("dd/MM/yyyy");
                if (item.SolarType_Id == 1)
                {
                    item.SolarTypeName = "Residential";
                }
                else
                {
                    item.SolarTypeName = "Commercial";
                }
                if (item.HouseType == 3)
                {
                    item.HouseTypeName = "Single story";
                }
                else
                {
                    item.HouseTypeName = "Multi story";
                }

                var PanelList = _scanedPanelRepository.GetAll().Where(P => P.Guid == item.Guid).Select(P => P.PanelSerialNumber).ToList();

                for (int i = 0; i < PanelList.Count; i++)
                {
                    if (i == 0)
                    {
                        item.PanelSerialNumberList = PanelList[i];
                    }
                    else
                    {
                        item.PanelSerialNumberList = item.PanelSerialNumberList + ";" + PanelList[i];
                    }

                }

                using (_unitOfWorkManager.Current.SetTenantId(null))
                {
                    item.StreetType = _streetTypeRepository.GetAll().Where(S => S.StreetTypeName == item.StreetType).Select(S => S.StreetTypeCode).FirstOrDefault();
                    item.InstallationStreetType = _streetTypeRepository.GetAll().Where(S => S.StreetTypeName == item.InstallationStreetType).Select(S => S.StreetTypeCode).FirstOrDefault();


                    var Createfor = _installerRepository.GetAll().Where(p => p.Id == item.CreateFor_Id).FirstOrDefault();
                    var Installer = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).FirstOrDefault();
                    var Designer = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Installer_Id).FirstOrDefault();
                    var Electrician = _installerEmployeRepository.GetAll().Where(p => p.Id == item.Electrician_Id).FirstOrDefault();
                    var Tenant = _tenantRepository.GetAll().Where(T => T.Id == item.TenantId).FirstOrDefault();

                    char[] spaceSeparator = new char[] { ' ' };
                    string[] InstName = new string[2];
                    string[] EleName = new string[2];
                    string[] DesignerName = new string[2];
                    if (!string.IsNullOrEmpty(Installer.FullName))
                    {
                        InstName = Installer.FullName.Split(spaceSeparator);
                        item.InstallerFName = InstName[0];
                    }
                    if (!string.IsNullOrEmpty(Electrician.FullName))
                    {
                        EleName = Electrician.FullName.Split(spaceSeparator);
                    }
                    if (!string.IsNullOrEmpty(Designer.FullName))
                    {
                        DesignerName = Designer.FullName.Split(spaceSeparator);
                    }

                    if (InstName.Length > 1)
                    {
                        item.InstallerLName = InstName[1];
                    }
                    item.ElectricianFName = EleName[0];
                    if (EleName.Length > 1)
                    {
                        item.ElectricianLName = EleName[1];
                    }
                    item.DesignerFName = DesignerName[0];
                    if (DesignerName.Length > 1)
                    {
                        item.DesignerLName = DesignerName[1];
                    }

                    //Installer Detail
                    item.InstallerPhone = Installer.Phone;
                    item.InstallerMobile = Installer.Mobile;
                    item.InstallerEmailId = Installer.EmailId;
                    item.InstallerFax = Installer.Fax;
                    item.InstallerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(P => P.Id == Installer.AddressType).Select(P => P.PostalAddressTypeName).FirstOrDefault();
                    item.InstallerPostalDelNo = Installer.PostalDelNo;
                    item.InstallerPostalDelTypeName = _postalDeliveryTypeRepository.GetAll().Where(P => P.Id == Installer.PostalDelType).Select(P => P.PostalDeliveryTypeName).FirstOrDefault();
                    item.InstallerUnitNo = Installer.UnitNo;
                    item.InstallerUnitType = Installer.UnitType;
                    item.InstallerStreetNumber = Installer.StreetNumber;
                    item.InstallerStreetName = Installer.StreetName;
                    item.InstallerStreetType = _streetTypeRepository.GetAll().Where(S => S.StreetTypeName == Installer.StreetType).Select(S => S.StreetTypeCode).FirstOrDefault();
                    item.InstallerSuburb = Installer.Suburb;
                    item.InstallerState = Installer.State;
                    item.InstallerPostCode = Installer.PostCode;
                    item.InstallerExpiry = Installer.InstallerExpiry;

                    //Designer Detail
                    item.DesignerPhone = Designer.Phone;
                    item.DesignerMobile = Designer.Mobile; ;
                    item.DesignerEmailId = Designer.EmailId; ;
                    item.DesignerFax = Designer.Fax;
                    item.DesignerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(P => P.Id == Designer.AddressType).Select(P => P.PostalAddressTypeName).FirstOrDefault();
                    item.DesignerPostalDelNo = Designer.PostalDelNo;
                    item.DesignerPostalDelTypeName = _postalDeliveryTypeRepository.GetAll().Where(P => P.Id == Designer.PostalDelType).Select(P => P.PostalDeliveryTypeName).FirstOrDefault();
                    item.DesignerUnitNo = Designer.UnitNo;
                    item.DesignerUnitType = Designer.UnitType;
                    item.DesignerStreetNumber = Designer.StreetNumber;
                    item.DesignerStreetName = Designer.StreetName;
                    item.DesignerStreetType = _streetTypeRepository.GetAll().Where(S => S.StreetTypeName == Designer.StreetType).Select(S => S.StreetTypeCode).FirstOrDefault();
                    item.DesignerSuburb = Designer.Suburb;
                    item.DesignerState = Designer.State;
                    item.DesignerPostCode = Designer.PostCode;
                    item.DesignerExpiry = Designer.DesignerExpiry;

                    //Electraction Detail
                    item.ElectractionPhone = Electrician.Phone;
                    item.ElectractionMobile = Electrician.Mobile;
                    item.ElectractionEmailId = Electrician.EmailId;
                    item.ElectractionFax = Electrician.Fax;
                    item.ElectractionAddressTypeName = _postalAddressTypeRepository.GetAll().Where(P => P.Id == Electrician.AddressType).Select(P => P.PostalAddressTypeName).FirstOrDefault();
                    item.ElectractionPostalDelNo = Electrician.PostalDelNo;
                    item.ElectractionPostalDelTypeName = _postalDeliveryTypeRepository.GetAll().Where(P => P.Id == Electrician.PostalDelType).Select(P => P.PostalDeliveryTypeName).FirstOrDefault();
                    item.ElectractionUnitNo = Electrician.UnitNo;
                    item.ElectractionUnitType = Electrician.UnitType;
                    item.ElectractionStreetNumber = Electrician.StreetNumber;
                    item.ElectractionStreetName = Electrician.StreetName;
                    item.ElectractionStreetType = _streetTypeRepository.GetAll().Where(S => S.StreetTypeName == Electrician.StreetType).Select(S => S.StreetTypeCode).FirstOrDefault();
                    item.ElectractionSuburb = Electrician.Suburb;
                    item.ElectractionState = Electrician.State;
                    item.ElectractionPostCode = Electrician.PostCode;
                    item.ElectractionExpiry = Electrician.ElectricalExpiry;

                    //Tenant Detail
                    item.RetailerABN = Tenant.RetailerABN;
                    item.RetailerName = Tenant.RetailerName;


                    item.HouseTypeName = _houseTypeRepository.GetAll().Where(p => p.Id == item.HouseType).Select(p => p.HouseTypeName).FirstOrDefault();
                    item.RoofTypeName = _roofTypeRepository.GetAll().Where(p => p.Id == item.RoofType).Select(p => p.RoofTypeName).FirstOrDefault();
                    //item.SolarTypeName = _solarTypeRepository.GetAll().Where(p => p.Id == item.SolarType_Id).Select(p => p.SolarTypeeName).FirstOrDefault();
                    item.SystemTypeName = _systemTypeRepository.GetAll().Where(p => p.Id == item.SystemType_Id).Select(p => p.SystemTypeName).FirstOrDefault();
                    item.RoofFacingPanelName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == item.RoofFacingPanel_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();
                    item.SugstthisaddressName = _yesNoRepository.GetAll().Where(p => p.Id == item.Sugstthisaddress_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.LocationOfNewSystemName = _roofFacingPanelRepository.GetAll().Where(p => p.Id == item.LocationOfNewSystem_Id).Select(p => p.RoofFacingPanelName).FirstOrDefault();

                    item.TypeofsystemName = _typeOfSystemRepository.GetAll().Where(p => p.Id == item.Typeofsystem_Id).Select(p => p.TypeOfSystemName).FirstOrDefault();
                    item.TypeofconnectionName = _typeOfConnectionRepository.GetAll().Where(p => p.Id == item.Typeofconnection_Id).Select(p => p.TypeOfConnectionName).FirstOrDefault();
                    item.MountingtypeName = _mountingTypeRepository.GetAll().Where(p => p.Id == item.Mountingtype_Id).Select(p => p.MountingTypeName).FirstOrDefault();
                    item.AuditreportavailableName = _yesNoRepository.GetAll().Where(p => p.Id == item.Auditreportavailable_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.InstallingcompleteunitName = _yesNoRepository.GetAll().Where(p => p.Id == item.Installingcompleteunit_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.RECYearName = _recyearRepository.GetAll().Where(p => p.Id == item.RECYear_Id).Select(p => p.REC_Year).FirstOrDefault();
                    item.DefaultresourceavailfigureName = _yesNoRepository.GetAll().Where(p => p.Id == item.Defaultresourceavailfigure_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.CeatingCertName = _yesNoRepository.GetAll().Where(p => p.Id == item.CeatingCert_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.CECAccreditationStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.CECAccreditationStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.GovernmentSitingApprovalsName = _yesNoRepository.GetAll().Where(p => p.Id == item.GovernmentSitingApprovals_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.ElectricalSafetyDocumentationName = _yesNoRepository.GetAll().Where(p => p.Id == item.ElectricalSafetyDocumentation_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.AustralianStandardsStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.AustralianStandardsStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.NotgridConnectedStatementName = _yesNoRepository.GetAll().Where(p => p.Id == item.NotgridConnectedStatement_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.SGUatThisAddressName = _yesNoRepository.GetAll().Where(p => p.Id == item.SGUatThisAddress_Id).Select(p => p.YesNoName).FirstOrDefault();
                    item.SystemMountingTypeName = _systemMountingTypeRepository.GetAll().Where(p => p.Id == item.SystemMountingType_Id).Select(p => p.SystemMountingTypeName).FirstOrDefault();
                    item.OwnerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == item.OwnerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
                    item.CustomerAddressTypeName = _postalAddressTypeRepository.GetAll().Where(p => p.Id == item.CustomerAddressType_Id).Select(p => p.PostalAddressTypeName).FirstOrDefault();
                    item.PanelModel = _panelRepository.GetAll().Where(p => p.Id == item.Panel_id).Select(p => p.ModelNo).FirstOrDefault();
                    item.InverterModel = _inverterRepository.GetAll().Where(p => p.Id == item.Inverter_id).Select(p => p.ModelNumber).FirstOrDefault();
                }
            }
            //Generat CSV Code
            try
            {
                var csv = new StringBuilder();

                var TypeOfSystem = "Type of system";
                var InstallationDate = "Installation date";
                var PanelBrand = "System/panel brand";
                var PanelModel = "System/panel model";
                var InverterMan = "Inverter manufacturer";
                var InverterSeries = "Inverter series";
                var InvModel = "Inverter model number";
                var TypeofConnection = "Type of connection to the electricity grid";
                var stsremmounting = "System mounting type";
                var audit = "Is a site specific audit report available?";
                var completeunit = "Are you installing a complete unit (adding capacity to an existing system is not considered a complete unit)?";
                var aditionalcapacity = "If this system is additional capacity to an existing system please provide detailed information on the position of the new panels and inverter (if applicable). System upgrades without a note explaining new panel locations will be failed by the Clean Energy Regulator";
                var createREC = "For what period would you like to create RECs";
                var poweroutput = "What is the rated power output (in kW) of your small generation unit";
                var NumberofPanels = "Number of panels";
                var resourceavailability = "Do you wish to use the default resource availability figure?";
                var hoursperannum = "What is your resource availability (hours per annum) for your system?";
                var Equipmentmodelserialnumber = "Equipment model serial number(s)";
                var creatingcertificates = "Are you creating certificates for a system that has previously been failed by the Clean Energy Regulator?";
                var Accreditationcode = "Accreditation code";
                var recreatingcertificates = "Explanatory notes for re-creating certificates previously failed";
                var Reference = "Reference";
                var Certificatetag = "Certificate tag";
                var accreditation = "CEC accreditation statement";
                var Territorygovernment = "\"Local, State and Territory government siting approvals\"";
                var Electricalsafety = "Electrical safety documentation";
                var AustralianNewZealand = "Australian/New Zealand standards statement";
                var Notgridconnectedstatement = "Not grid-connected statement";
                var Installationpropertytype = "Installation property type";
                var Singleormultistory = "Single or multi-story";
                var Installationpropertyname = "\"Installation property name, boat name or chassis number\"";
                var Installationunittype = "Installation unit type";
                var Installationunitnumber = "Installation unit number";
                var Installationstreetnumber = "Installation street number";
                var Installationstreetname = "Installation street name";
                var Installationstreettype = "Installation street type";
                var InstallationtownSuburb = "Installation town/Suburb";
                var Installationstate = "Installation state";
                var Installationpostcode = "Installation postcode";
                var Installationlatitude = "Installation latitude";
                var Installationlongitude = "Installation longitude";
                var morethanoneSGU = "Is there more than one SGU at this address?";
                var addressentered = "If the address entered above does not adequately describe the location of the system please provide further detailed information for the Clean Energy Regulator to locate the system";
                var Additionalsysteminformation = "Additional system information";
                var Ownertype = "Owner type";
                var Ownerorganisationname = "Owner organisation name";
                var Ownerfirstname = "Owner first name";
                var Ownersurname = "Owner surname";
                var Ownerphone = "Owner phone";
                var Ownerfax = "Owner fax";
                var Ownermobile = "Owner mobile";
                var Owneremail = "Owner email";
                var Owneraddresstype = "Owner address type";
                var Ownerpostaldeliverytype = "Owner postal delivery type";
                var Ownerpostaldeliverynumber = "Owner postal delivery number";
                var Ownerunittype = "Owner unit type";
                var Ownerunitnumber = "Owner unit number";
                var Ownerstreetnumber = "Owner street number";
                var Ownerstreetname = "Owner street name";
                var Ownerstreettype = "Owner street type";
                var Ownertownsuburb = "Owner town/suburb";
                var Ownerstate = "Owner state";
                var Ownerpostcode = "Owner postcode";
                var Ownercountry = "Owner country";
                var Installerfirstname = "Installer first name";
                var Installersurname = "Installer surname";
                var CECaccreditedinstallernumber = "CEC accredited installer number";
                var Installerphone = "Installer phone";
                var Installerfax = "Installer fax";
                var Installermobile = "Installer mobile";
                var Installeremail = "Installer email";
                var Installeraddresstype = "Installer address type";
                var Installerpostaldeliverytype = "Installer postal delivery type";
                var Installerpostaldeliverynumber = "Installer postal delivery number";
                var Installerunittype = "Installer unit type";
                var Installerunitnumber = "Installer unit number";
                var Installerstreetnumber = "Installer street number";
                var Installerstreetname = "Installer street name";
                var Installerstreettype = "Installer street type";
                var Installertownsuburb = "Installer town/suburb";
                var Installerstate = "Installer state";
                var Installerpostcode = "Installer postcode";
                var Electricianfirstname = "Electrician first name";
                var Electriciansurname = "Electrician surname";
                var Licensedelectriciannumber = "Licensed electrician number";
                var Electricianphone = "Electrician phone";
                var Electricianfax = "Electrician fax";
                var Electricianmobile = "Electrician mobile";
                var Electricianemail = "Electrician email";
                var Electricianaddresstype = "Electrician address type";
                var Electricianpostaldeliverytype = "Electrician postal delivery type";
                var Electricianpostaldeliverynumber = "Electrician postal delivery number";
                var Electricianunittype = "Electrician unit type";
                var Electricianunitnumber = "Electrician unit number";
                var Electricianstreetnumber = "Electrician street number";
                var Electricianstreetname = "Electrician street name";
                var Electricianstreettype = "Electrician street type";
                var Electriciantownsuburb = "Electrician town/suburb";
                var Electricianstate = "Electrician state";
                var Electricianpostcode = "Electrician postcode";
                var Designerfirstname = "Designer first name";
                var Designersurname = "Designer surname";
                var CECaccrediteddesignernumber = "CEC accredited designer number";
                var Designerphone = "Designer phone";
                var Designerfax = "Designer fax";
                var Designermobile = "Designer mobile";
                var Designeremail = "Designer email";
                var Designeraddresstype = "Designer address type";
                var Designerpostaldeliverytype = "Designer postal delivery type";
                var Designerpostaldeliverynumber = "Designer postal delivery number";
                var Designerunittype = "Designer unit type";
                var Designerunitnumber = "Designer unit number";
                var Designerstreetnumber = "Designer street number";
                var Designerstreetname = "Designer street name";
                var Designerstreettype = "Designer street type";
                var Designertownsuburb = "Designer town/suburb";
                var Designerstate = "Designer state";
                var Designerpostcode = "Designer postcode";
                var RetailerName = "Retailer Name";
                var RetailerABN = "Retailer ABN";
                var Nationalmeteringidentifier = "National metering identifier (NMI)";
                var Batterystoragemanufacturer = "Battery storage manufacturer";
                var Batterystoragemodel = "Battery storage model";
                var batterysystem = "Is the battery system part of an aggregated control?";
                var defaultmanufacturer = "Has the installer changed default manufacturer setting of the battery storage system?";
                var SignedDataPackage = "Signed Data Package";
                var DocumentsZipFile = "Documents Zip File";

                var Header = $"{ TypeOfSystem},{ InstallationDate},{ PanelBrand},{ PanelModel},{ InverterMan},{ InverterSeries},{ InvModel},{ TypeofConnection},{ stsremmounting},{ audit},{ completeunit},{ aditionalcapacity},{ createREC},{ poweroutput},{ NumberofPanels},{ resourceavailability},{ hoursperannum},{ Equipmentmodelserialnumber},{ creatingcertificates},{ Accreditationcode},{ recreatingcertificates},{ Reference},{ Certificatetag},{ accreditation},{ Territorygovernment},{ Electricalsafety},{ AustralianNewZealand},{ Notgridconnectedstatement},{ Installationpropertytype},{ Singleormultistory},{ Installationpropertyname},{ Installationunittype},{ Installationunitnumber},{ Installationstreetnumber},{ Installationstreetname},{ Installationstreettype},{ InstallationtownSuburb},{ Installationstate},{ Installationpostcode},{ Installationlatitude},{ Installationlongitude},{ morethanoneSGU},{ addressentered },{ Additionalsysteminformation},{ Ownertype},{ Ownerorganisationname},{ Ownerfirstname},{ Ownersurname},{ Ownerphone},{ Ownerfax},{ Ownermobile},{ Owneremail},{ Owneraddresstype},{ Ownerpostaldeliverytype},{ Ownerpostaldeliverynumber},{ Ownerunittype},{ Ownerunitnumber},{ Ownerstreetnumber},{ Ownerstreetname},{ Ownerstreettype},{ Ownertownsuburb},{ Ownerstate},{ Ownerpostcode},{ Ownercountry},{ Installerfirstname},{ Installersurname},{ CECaccreditedinstallernumber},{ Installerphone},{ Installerfax},{ Installermobile},{ Installeremail},{ Installeraddresstype},{ Installerpostaldeliverytype},{ Installerpostaldeliverynumber},{ Installerunittype},{ Installerunitnumber},{ Installerstreetnumber},{ Installerstreetname},{ Installerstreettype},{ Installertownsuburb},{ Installerstate},{ Installerpostcode},{ Electricianfirstname},{ Electriciansurname},{ Licensedelectriciannumber},{ Electricianphone},{ Electricianfax},{ Electricianmobile},{ Electricianemail},{ Electricianaddresstype},{ Electricianpostaldeliverytype},{ Electricianpostaldeliverynumber},{ Electricianunittype},{ Electricianunitnumber},{ Electricianstreetnumber},{ Electricianstreetname},{ Electricianstreettype},{ Electriciantownsuburb},{ Electricianstate},{ Electricianpostcode},{ Designerfirstname},{ Designersurname},{ CECaccrediteddesignernumber},{ Designerphone},{ Designerfax},{ Designermobile},{ Designeremail},{ Designeraddresstype},{ Designerpostaldeliverytype},{ Designerpostaldeliverynumber},{ Designerunittype},{ Designerunitnumber},{ Designerstreetnumber},{ Designerstreetname},{ Designerstreettype},{ Designertownsuburb},{ Designerstate},{ Designerpostcode},{ RetailerName},{ RetailerABN},{ Nationalmeteringidentifier},{ Batterystoragemanufacturer},{ Batterystoragemodel},{ batterysystem},{ defaultmanufacturer},{ SignedDataPackage},{ DocumentsZipFile}";


                csv.AppendLine(Header);
                //in your loop
                foreach (var item in jobListDtos)
                {
                    var Blank = "";
                    var NO = "NO";
                    var AU = "AU";
                    //var newLine = $"{item.TypeofsystemName},{item.ExpectedInstallShortDate},{item.BrandName},{item.PanelModel},{item.InverterBrandName},{item.InverterSeries},{item.InverterModel},{item.TypeofconnectionName},{item.MountingtypeName}, {Blank},{item.InstallingcompleteunitName},{item.AddcapMoredetailed},{item.RECYearName},{item.SystemCapacity},{item.NoOfPanels},{item.DefaultresourceavailfigureName},{item.ResourceAvailability},{item.PanelSerialNumberList},{item.CeatingCertName},{item.AccreditationCode},{item.ExplanatoryNotes},{item.ProjectNumber},{Blank},{item.CECAccreditationStatementName},{item.GovernmentSitingApprovalsName},{item.ElectricalSafetyDocumentationName},{item.AustralianStandardsStatementName},{item.NotgridConnectedStatementName},{item.SolarTypeName},{item.HouseTypeName},{item.InstallPropertyName},{item.InstallationUnitType},{item.InstallationUnitNo},{item.InstallationStreetNo},{item.InstallationStreetName},{item.InstallationStreetType},{item.InstallationStreetCity},{item.InstallationStreetState},{item.InstallationStreetPostCode},{Blank},{Blank},{ NO},{ Blank},{ item.AdditionalSystemInformation},{ item.SystemMountingTypeName},{ item.Company},{ item.FirstName},{ item.LastName},{ item.Phone},{ item.Fax},{ item.Phone},{ item.EmailId},{ item.PostalAddressType},{ item.PostalDeliveryType},{ item.PostalDeliveryNumber},{ item.UnitType},{ item.UnitNo},{ item.StreetNo},{ item.StreetName},{ item.StreetType},{ item.StreetCity},{ item.StreetState},{ item.StreetPostCode},{ AU},{ item.InstallerFName},{ item.InstallerLName},{ item.InstallerExpiry},{ item.InstallerPhone},{ item.InstallerFax},{ item.InstallerMobile},{ item.InstallerEmailId},{ item.InstallerAddressTypeName},{ item.InstallerPostalDelTypeName},{ item.InstallerPostalDelNo},{ item.InstallerUnitType},{ item.InstallerUnitNo},{ item.InstallerStreetNumber},{ item.InstallerStreetName},{ item.InstallerStreetType},{ item.InstallerSuburb},{ item.InstallerState},{ item.InstallerPostCode},{ item.ElectricianFName},{ item.ElectricianLName},{ item.ElectractionExpiry},{ item.ElectractionPhone},{ item.ElectractionFax},{ item.ElectractionMobile},{ item.ElectractionEmailId},{ item.ElectractionAddressTypeName},{ item.ElectractionPostalDelTypeName},{ item.ElectractionPostalDelNo},{ item.ElectractionUnitType},{ item.ElectractionUnitNo},{ item.ElectractionStreetNumber},{ item.ElectractionStreetName},{ item.ElectractionStreetType},{ item.ElectractionSuburb},{ item.ElectractionState},{ item.ElectractionPostCode},{ item.DesignerFName},{ item.DesignerLName},{ item.DesignerExpiry},{ item.DesignerPhone},{ item.DesignerFax},{ item.DesignerMobile},{ item.DesignerEmailId},{ item.DesignerAddressTypeName},{ item.DesignerPostalDelTypeName},{ item.DesignerPostalDelNo},{ item.DesignerUnitType},{ item.DesignerUnitNo},{ item.DesignerStreetNumber},{ item.DesignerStreetName},{ item.DesignerStreetType},{ item.DesignerSuburb},{ item.DesignerState},{ item.DesignerPostCode},{ item.RetailerName},{ item.RetailerABN},{ item.NMINo},{ Blank},{ Blank},{ Blank},{ Blank},{ Blank},{ Blank}";
                    var newLine = $"{item.TypeofsystemName},{item.ExpectedInstallShortDate},{item.BrandName},{item.PanelModel},{item.InverterBrandName},{item.InverterSeries},{item.InverterModel},{item.TypeofconnectionName},{item.MountingtypeName}, {Blank},{item.InstallingcompleteunitName},{item.AddcapMoredetailed},{item.RECYearName},{item.SystemCapacity},{item.NoOfPanels},{item.DefaultresourceavailfigureName},{item.ResourceAvailability},{item.PanelSerialNumberList},{item.CeatingCertName},{item.AccreditationCode},{item.ExplanatoryNotes},{item.ProjectNumber},{Blank},{item.CECAccreditationStatementName},{item.GovernmentSitingApprovalsName},{item.ElectricalSafetyDocumentationName},{item.AustralianStandardsStatementName},{item.NotgridConnectedStatementName},{item.SolarTypeName},{item.HouseTypeName},{item.InstallPropertyName},{item.InstallationUnitType},{item.InstallationUnitNo},{item.InstallationStreetNo},{item.InstallationStreetName},{item.InstallationStreetType},{item.InstallationStreetCity},{item.InstallationStreetState},{item.InstallationStreetPostCode},{Blank},{Blank},{ NO},{ Blank},{ item.AdditionalSystemInformation},{ item.SystemMountingTypeName},{Blank},{ item.FirstName},{ item.LastName},{ item.Phone},{ item.Fax},{ item.Phone},{ item.EmailId},{ item.PostalAddressType},{ item.PostalDeliveryType},{ item.PostalDeliveryNumber},{ item.UnitType},{ item.UnitNo},{ item.StreetNo},{ item.StreetName},{ item.StreetType},{ item.StreetCity},{ item.StreetState},{ item.StreetPostCode},{ AU},{ item.InstallerFName},{ item.InstallerLName},{ item.InstallerExpiry},{ item.InstallerPhone},{ item.InstallerFax},{ item.InstallerMobile},{ item.InstallerEmailId},{ item.InstallerAddressTypeName},{ item.InstallerPostalDelTypeName},{ item.InstallerPostalDelNo},{ item.InstallerUnitType},{ item.InstallerUnitNo},{ item.InstallerStreetNumber},{ item.InstallerStreetName},{ item.InstallerStreetType},{ item.InstallerSuburb},{ item.InstallerState},{ item.InstallerPostCode},{ item.ElectricianFName},{ item.ElectricianLName},{ item.ElectractionExpiry},{ item.ElectractionPhone},{ item.ElectractionFax},{ item.ElectractionMobile},{ item.ElectractionEmailId},{ item.ElectractionAddressTypeName},{ item.ElectractionPostalDelTypeName},{ item.ElectractionPostalDelNo},{ item.ElectractionUnitType},{ item.ElectractionUnitNo},{ item.ElectractionStreetNumber},{ item.ElectractionStreetName},{ item.ElectractionStreetType},{ item.ElectractionSuburb},{ item.ElectractionState},{ item.ElectractionPostCode},{ item.DesignerFName},{ item.DesignerLName},{ item.DesignerExpiry},{ item.DesignerPhone},{ item.DesignerFax},{ item.DesignerMobile},{ item.DesignerEmailId},{ item.DesignerAddressTypeName},{ item.DesignerPostalDelTypeName},{ item.DesignerPostalDelNo},{ item.DesignerUnitType},{ item.DesignerUnitNo},{ item.DesignerStreetNumber},{ item.DesignerStreetName},{ item.DesignerStreetType},{ item.DesignerSuburb},{ item.DesignerState},{ item.DesignerPostCode},{ item.RetailerName},{ item.RetailerABN},{ item.NMINo},{ Blank},{ Blank},{ Blank},{ Blank},{ Blank},{ Blank}";

                    csv.AppendLine(newLine);
                }
                //after your loop
                FileName = DateTime.Now.Ticks + "_ComplienceJobList.csv";
                var filePath = Path.Combine(_appFolders.ComplienceDownloadFolder, FileName);
                File.WriteAllText(filePath, csv.ToString());

                var fileBytes = System.IO.File.ReadAllBytes(filePath);
                System.IO.File.Delete(filePath);

                if (!string.IsNullOrEmpty(_appFolders.ComplienceDownloadFolder))
                {
                    newfilePath = _appFolders.ComplienceDownloadFolder + "\\" + FileName;
                    File.WriteAllBytes(newfilePath, fileBytes);
                }

                foreach (var item in jobListDtos)
                {
                    var UpdateFileName = _jobRepository.GetAll().Where(J => J.Id == item.Id).FirstOrDefault();
                    UpdateFileName.RECUploadFileName = FileName;
                    UpdateFileName.RECUploadDate = DateTime.Now;
                    _jobRepository.UpdateAsync(UpdateFileName);
                }
            }
            catch (Exception ex)
            {
                throw new UserFriendlyException("OOps1...!", ex.Message.ToString());
            }

            return FileName;
        }

        public string RecCheck()
		{
            var Details = _recUserRepository.GetAll().FirstOrDefault();

            if(Details != null)
			{
                return Details.RecUserName;
            }
            else
			{
                return null;
			}
        }
    }
}
