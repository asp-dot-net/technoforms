﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.StreetName.Dto
{
    public class StreetNameCheckExistDto : EntityDto
    {
        public string StreetName { get; set; }
    }
}
