﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.StreetName.Dto
{
    [AutoMapFrom(typeof(StreetNames))]
    public class StreetNameListDto : FullAuditedEntityDto
    {
       
        public string StreetName { get; set; }
    }
}
