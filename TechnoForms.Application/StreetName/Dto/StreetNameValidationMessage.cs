﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.StreetName.Dto
{
    public class StreetNameValidationMessage
    {
        public string Message { get; set; }
        public bool Status { get; set; }
    }
}
