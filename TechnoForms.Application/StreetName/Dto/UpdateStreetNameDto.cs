﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.StreetName.Dto
{
    [AutoMap(typeof(StreetNames))]
    public class UpdateStreetNameDto : FullAuditedEntityDto
    {
        [Required]
        [MaxLength(StreetNames.MaxNameLength)]
        public string StreetName { get; set; }
    }
}
