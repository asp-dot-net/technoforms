﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.StreetName.Dto;
using System.Data.Entity;using Abp.Linq.Extensions;using System.Linq.Dynamic;

namespace TechnoForms.StreetName
{
    [AbpAuthorize(AppPermissions.Pages_Masters_StreetName)]
    public class StreetNameAppService : TechnoFormsAppServiceBase, IStreetNameAppService
    {
        private readonly IRepository<StreetNames> _streetnameRepository;
        public StreetNameAppService(
          IRepository<StreetNames> streetnameRepository)
        {
            _streetnameRepository = streetnameRepository;
        }
        public async Task<PagedResultDto<StreetNameListDto>> GetStreetName(GetStreetNameInput input)
        {
            var streetname = _streetnameRepository
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.StreetName.ToLower().Contains(input.Filter.ToLower())
                );

            var resultCount = await streetname.CountAsync();            var results = await streetname                .AsNoTracking()                .OrderBy(input.Sorting)                .PageBy(input)                .ToListAsync();            return new PagedResultDto<StreetNameListDto>(resultCount, results.MapTo<List<StreetNameListDto>>());
        }
        [AbpAuthorize(AppPermissions.Pages_Masters_StreetName_Create)]
        public async Task CreateStreetName(CreateStreetNameDto input)
        {
            var StreetName = ObjectMapper.Map<StreetNames>(input);
            await _streetnameRepository.InsertAsync(StreetName);
        }
        [AbpAuthorize(AppPermissions.Pages_Masters_StreetName_Edit)]
        public async Task<UpdateStreetNameDto> GetStreetNameForEdit(EntityDto input)
        {
            var StreetName = (await _streetnameRepository.GetAsync(input.Id)).MapTo<UpdateStreetNameDto>(); ;
            return StreetName;
        }
        [AbpAuthorize(AppPermissions.Pages_Masters_StreetName_Edit)]
        public async Task UpdateStreetName(UpdateStreetNameDto input)
        {
            var StreetName = await _streetnameRepository.GetAsync(input.Id);
            StreetName.StreetName = input.StreetName;
            await _streetnameRepository.UpdateAsync(StreetName);
        }
        [AbpAuthorize(AppPermissions.Pages_Masters_StreetName_Delete)]
        public async Task DeleteStreetName(EntityDto input)
        {
            await _streetnameRepository.DeleteAsync(input.Id);
        }

        public StreetNameValidationMessage CheckStreetNameExists(StreetNameCheckExistDto input)
        {
            var status_val = new StreetNameValidationMessage();
            status_val.Status = true;
            var StreetName = _streetnameRepository
               .GetAll()
               .Where(p => p.StreetName == input.StreetName).ToList();

            if (!string.IsNullOrEmpty(input.Id.ToString()) && input.Id != 0)
            {
                StreetName = StreetName.Where(p => p.Id != input.Id).ToList();
            }
            if (StreetName.Count > 0)
            {
                status_val.Status = false;
            }
            if (StreetName.Count > 0)
            {
                status_val.Message = "Record with this name already exists.";
            }
            return status_val;
        }

    }
}
