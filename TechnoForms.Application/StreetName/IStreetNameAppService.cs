﻿using Abp.Application.Services.Dto;
using Abp.Application.Services;
using TechnoForms.StreetName.Dto;
using System.Threading.Tasks;

namespace TechnoForms.StreetName
{
    public interface IStreetNameAppService : IApplicationService
    {
        Task<PagedResultDto<StreetNameListDto>> GetStreetName(GetStreetNameInput input);
        Task CreateStreetName(CreateStreetNameDto input);
        Task DeleteStreetName(EntityDto input);
        Task UpdateStreetName(UpdateStreetNameDto input);
        Task<UpdateStreetNameDto> GetStreetNameForEdit(EntityDto input);
        StreetNameValidationMessage CheckStreetNameExists(StreetNameCheckExistDto input);
    }
}
