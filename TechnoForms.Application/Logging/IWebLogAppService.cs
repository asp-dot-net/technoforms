﻿using Abp.Application.Services;
using TechnoForms.Dto;
using TechnoForms.Logging.Dto;

namespace TechnoForms.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
