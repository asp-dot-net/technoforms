﻿using System.Collections.Generic;

namespace TechnoForms.Logging.Dto
{
    public class GetLatestWebLogsOutput
    {
        public List<string> LatesWebLogLines { get; set; }
    }
}
