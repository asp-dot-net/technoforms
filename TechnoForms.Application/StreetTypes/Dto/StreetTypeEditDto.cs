﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.StreetTypes.Dto
{
    [AutoMap(typeof(StreetType))]
    public class StreetTypeEditDto : EntityDto
    {
        [Required]
        [StringLength(StreetType.MaxStreetTypeNameLength)]
        public string StreetTypeName { get; set; }

        [Required]
        [StringLength(StreetType.MaxStreetTypeCodeLength)]
        public string StreetTypeCode { get; set; }
    }
}
