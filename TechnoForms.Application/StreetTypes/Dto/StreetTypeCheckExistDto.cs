﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.StreetTypes.Dto
{
    public class StreetTypeCheckExistDto : EntityDto
    {
        public string StreetTypeName { get; set; }
        public string StreetTypeCode { get; set; }
    }
}
