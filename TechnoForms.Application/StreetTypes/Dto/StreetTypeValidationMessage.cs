﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.StreetTypes.Dto
{
    public class StreetTypeValidationMessage
    {
        public string Message { get; set; }
        public bool Status { get; set; }
    }
}
