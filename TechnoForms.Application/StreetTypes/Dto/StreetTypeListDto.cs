﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.StreetTypes.Dto
{
    [AutoMapFrom(typeof(StreetType))]
    public class StreetTypeListDto : FullAuditedEntityDto
    {
        public string StreetTypeName { get; set; }
        public string StreetTypeCode { get; set; }
    }
}
