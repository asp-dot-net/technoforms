﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.StreetTypes.Dto
{
    [AutoMapTo(typeof(StreetType))]
    public class CreateStreetTypeInput
    {
        [Required]
        [MaxLength(StreetType.MaxStreetTypeNameLength)]
        public string StreetTypeName { get; set; }

        [Required]
        [MaxLength(StreetType.MaxStreetTypeCodeLength)]
        public string StreetTypeCode { get; set; }
    }
}
