﻿using Abp.Runtime.Validation;
using TechnoForms.Dto;

namespace TechnoForms.StreetTypes.Dto
{
    public class GetStreetTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
