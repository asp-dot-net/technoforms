﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using TechnoForms.Authorization;
using TechnoForms.StreetTypes.Dto;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using System.Data.Entity;
using Abp.Linq.Extensions;
using System.Linq.Dynamic;

namespace TechnoForms.StreetTypes
{
    [AbpAuthorize(AppPermissions.Pages_Masters_StreetType)]
    public class StreetTypeAppService : TechnoFormsAppServiceBase, IStreetTypeAppService
    {
        private readonly IRepository<StreetType> _streettypeRepository;
        public StreetTypeAppService(
          IRepository<StreetType> streettypeRepository)
        {
            _streettypeRepository = streettypeRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_StreetType_Create)]
        public async Task CreateStreetType(CreateStreetTypeInput input)
        {
            var streettype = input.MapTo<StreetType>();
            await _streettypeRepository.InsertAsync(streettype);
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_StreetType_Delete)]
        public async Task DeleteStreetType(EntityDto input)
        {
            await _streettypeRepository.DeleteAsync(input.Id);
        }
        [AbpAuthorize(AppPermissions.Pages_Masters_StreetType_Edit)]
        public async Task UpdateStreetType(StreetTypeEditDto input)
        {
            var streettype = await _streettypeRepository.GetAsync(input.Id);
            streettype.StreetTypeCode = input.StreetTypeCode;
            streettype.StreetTypeName = input.StreetTypeName;
            await _streettypeRepository.UpdateAsync(streettype);
        }
        [AbpAuthorize(AppPermissions.Pages_Masters_StreetType_Edit)]
        public async Task<StreetTypeEditDto> GetStreetTypeForEdit(EntityDto input)
        {
            var streettype = (await _streettypeRepository.GetAsync(input.Id)).MapTo<StreetTypeEditDto>();
            return streettype;
        }
        public async Task<PagedResultDto<StreetTypeListDto>> GetStreetType(GetStreetTypeInput input)
        {
            var StreetTypes = _streettypeRepository
                .GetAll()
                .WhereIf(!input.Filter.IsNullOrEmpty(),
                    p => p.StreetTypeName.Contains(input.Filter) ||
                         p.StreetTypeCode.Contains(input.Filter)
                );

            var resultCount = await StreetTypes.CountAsync();
            var results = await StreetTypes
                .AsNoTracking()
                .OrderBy(input.Sorting)
                .PageBy(input)
                .ToListAsync();

            return new PagedResultDto<StreetTypeListDto>(resultCount, results.MapTo<List<StreetTypeListDto>>());
        }
        public StreetTypeValidationMessage CheckStreetTypeExists(StreetTypeCheckExistDto input)
        {
            var status_val = new StreetTypeValidationMessage();
            status_val.Status = true;
            var StreetType = _streettypeRepository
               .GetAll()
               .Where(p => p.StreetTypeName == input.StreetTypeName && p.StreetTypeCode == input.StreetTypeCode).ToList();

            if (!string.IsNullOrEmpty(input.Id.ToString()) && input.Id != 0)
            {
                StreetType = StreetType.Where(p => p.Id != input.Id).ToList();
            }
            if (StreetType.Count > 0)
            {
                status_val.Status = false;
            }
            if (StreetType.Count > 0)
            {
                status_val.Message = "Record with this name already exists.";
            }
            return status_val;
        }
    }
}
