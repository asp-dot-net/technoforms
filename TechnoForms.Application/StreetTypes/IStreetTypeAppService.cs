﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.StreetTypes.Dto;

namespace TechnoForms.StreetTypes
{
    public interface IStreetTypeAppService : IApplicationService
    {
        Task<PagedResultDto<StreetTypeListDto>> GetStreetType(GetStreetTypeInput input);
        Task CreateStreetType(CreateStreetTypeInput input);
        Task DeleteStreetType(EntityDto input);
        Task UpdateStreetType(StreetTypeEditDto input);
        Task<StreetTypeEditDto> GetStreetTypeForEdit(EntityDto input);
        StreetTypeValidationMessage CheckStreetTypeExists(StreetTypeCheckExistDto input);
    }
}
