﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.RecUsers;
using TechnoForms.RecUsers.Dto;
using System.Data.Entity;using Abp.Linq.Extensions;using System.Linq.Dynamic;
using Abp.Domain.Uow;

namespace TechnoForms.RecUsers
{
    [AbpAuthorize(AppPermissions.Pages_Administration_RECUser)]
    public class RecUserAppService : TechnoFormsApplicationModule, IRecUserAppService
    {
        private readonly IRepository<RecUser> _recUserRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;

        public RecUserAppService(IRepository<RecUser> recUserRepository,
            IUnitOfWorkManager unitOfWorkManager)
        {
            _recUserRepository = recUserRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Administration_RECUser_Create, AppPermissions.Pages_Administration_RECUser_Edit)]
        public RecUserValidationMessage CheckRecUserExists(RecUserCheckExistDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var status_val = new RecUserValidationMessage();
                status_val.Status = true;
                var rec = _recUserRepository
                   .GetAll()
                   .Where(p => p.RecUserName == input.RecUserName).ToList();

                if (!string.IsNullOrEmpty(input.Id.ToString()) && input.Id != 0)
                {
                    rec = rec.Where(p => p.Id != input.Id).ToList();
                }
                if (rec.Count > 0)
                {
                    status_val.Status = false;
                }
                if (rec.Count > 0)
                {
                    status_val.Message = "Record with this name already exists.";
                }
                return status_val;
            }
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Administration_RECUser_Create)]
        public async Task CreateRecUser(CreateRecUserDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var rec = input.MapTo<RecUser>();
                await _recUserRepository.InsertAsync(rec);
            }
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Administration_RECUser_Delete)]
        public async Task DeleteRecUser(EntityDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                await _recUserRepository.DeleteAsync(input.Id);
            }
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Administration_RECUser)]
        public async Task<PagedResultDto<RecUserListDto>> getRecUser(GetRecUserInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var rec = _recUserRepository
                .GetAll()
                .WhereIf(
                    !input.RecUserName.IsNullOrEmpty(),
                    p => p.RecUserName.ToLower().Contains(input.RecUserName.ToLower())
                );

                var resultCount = await rec.CountAsync();
                var results = await rec
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();

                return new PagedResultDto<RecUserListDto>(resultCount, results.MapTo<List<RecUserListDto>>());
            }
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Administration_RECUser_Edit)]
        public async Task<EditRecUserDto> GetRecUserForEdit(EntityDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var rec = (await _recUserRepository.GetAsync(input.Id)).MapTo<EditRecUserDto>();
                return rec;
            }
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Administration_RECUser_Edit)]
        public async Task UpdateRecUser(EditRecUserDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var rec = await _recUserRepository.GetAsync(input.Id);
                rec.RecUserName = input.RecUserName;
                rec.RecPasswprd = input.RecPasswprd;
                await _recUserRepository.UpdateAsync(rec);
            }
        }
    }
}
