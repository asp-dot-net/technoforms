﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.RecUsers.Dto;

namespace TechnoForms.RecUsers
{
    public interface IRecUserAppService : IApplicationService
    {
        Task<PagedResultDto<RecUserListDto>> getRecUser(GetRecUserInput input);
        Task CreateRecUser(CreateRecUserDto input);
        Task DeleteRecUser(EntityDto input);
        Task UpdateRecUser(EditRecUserDto input);
        Task<EditRecUserDto> GetRecUserForEdit(EntityDto input);
        RecUserValidationMessage CheckRecUserExists(RecUserCheckExistDto input);
    }
}
