﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.RecUsers.Dto
{
    public class RecUserCheckExistDto : EntityDto
    {
        public virtual string RecUserName { get; set; }
    }
}
