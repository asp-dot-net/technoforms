﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.RecUsers.Dto
{
    [AutoMapFrom(typeof(RecUser))]
    public class RecUserListDto : FullAuditedEntityDto
    {
        public string RecUserName { get; set; }

        public string RecPasswprd { get; set; }
    }
}
