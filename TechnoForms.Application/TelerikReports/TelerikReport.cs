﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace TechnoForms.TelerikReports
{
    public class TelerikReport : Controller
    {
        public ActionResult CostBreakDown(int AccId = 0)
        {

            //int id = 1;
            var report_parent = new TechnoForms.CECReport();

            //var data = db.getCostBreakdownReportdata(AccId).FirstOrDefault();
            //var data1 = db.getCostBreakdownReportdata(AccId).ToList();
            //Telerik.Reporting.HtmlTextBox header = report_parent.Items.Find("txtheader", true)[0] as Telerik.Reporting.HtmlTextBox;
            //header.Value = "<center>COST BREAKDOWN</center><br/><center><b>FUND CONTROL #" + data.AccountNo + "</b></center><br/><center>DIXIELINE BUILDERS FUND CONTROL, INC</center><br/><center>7700 RONSON ROAD, SAN DIEGO, CA 92111</center><br/><center>DBFC @DIXIELINE.COM WWW.DIXIELINE.COM</center><br/><center>________________________________________________________</center><br/><center>(858) 874-4354 (800) 443-7386</center>";
            //Telerik.Reporting.Table tbl_Accdetail = report_parent.Items.Find("table1", true)[0] as Telerik.Reporting.Table;
            //Telerik.Reporting.Table tbl_Job = report_parent.Items.Find("table2", true)[0] as Telerik.Reporting.Table;
            //Telerik.Reporting.Table tbl_AccountDetail = report_parent.Items.Find("table3", true)[0] as Telerik.Reporting.Table;
            //string ownercity, ownerstate, ownerzip = "";
            //string contractorcity, contractorstate, contractorzip = "";
            //string Lendercity, Lenderstate, Lenderzip = "";
            //if (data != null)
            //{
            //    tbl_Accdetail.DataSource = data;
            //    tbl_Job.DataSource = data;

            //}
            //if (data1 != null)
            //{
            //    tbl_AccountDetail.DataSource = data1;
            //}
            //Telerik.Reporting.HtmlTextBox Owner = report_parent.Items.Find("txtOwnerdetail", true)[0] as Telerik.Reporting.HtmlTextBox;
            //Telerik.Reporting.HtmlTextBox Contractor = report_parent.Items.Find("txtContractor", true)[0] as Telerik.Reporting.HtmlTextBox;
            //Telerik.Reporting.HtmlTextBox Lender = report_parent.Items.Find("txtLenderDetail", true)[0] as Telerik.Reporting.HtmlTextBox;
            //if (data.ownercity != null || data.ownerstate != null || data.ownerzip != null)
            //{
            //    ownercity = data.ownercity;
            //    ownerstate = data.ownerstate;
            //    ownerzip = data.ownerzip;
            //}
            //if (data.contractorcity != null || data.contractorstate != null || data.contractorzip != null)
            //{
            //    contractorcity = data.contractorcity;
            //    contractorstate = data.contractorstate;
            //    contractorzip = data.contractorzip;
            //}
            //if (data.lendercity != null || data.lenderstate != null || data.lenderzip != null)
            //{
            //    Lendercity = data.lendercity;
            //    Lenderstate = data.lenderstate;
            //    Lenderzip = data.lenderzip;
            //}
            //Owner.Value = data.ownername + "<br/>" + data.ownercity + "<br/>" + data.ownerstate + "<br/>" + data.ownerzip;
            //Contractor.Value = data.contractorname + "<br/> " + data.contractorcity + " <br/>" + data.contractorstate + "<br/>" + data.contractorzip;
            //Lender.Value = data.lendername + "<br/>" + data.lendercity + "<br/>" + data.lenderstate + "<br/>" + data.lenderzip;
            //ExportPdf(report_parent);
            return RedirectToAction("Index", "AccountDetail");

        }
    }
}
