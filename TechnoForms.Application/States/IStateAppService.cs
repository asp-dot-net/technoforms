﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.States.Dto;

namespace TechnoForms.States
{
    public interface IStateAppService : IApplicationService
    {
        ListResultDto<StateListDto> GetState();
    }
}
