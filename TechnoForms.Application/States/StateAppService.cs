﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.States.Dto;

namespace TechnoForms.States
{
    public class StateAppService : TechnoFormsAppServiceBase, IStateAppService
    {
        private readonly IRepository<State> _stateRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        public StateAppService(
          IRepository<State> stateRepository, IUnitOfWorkManager unitOfWorkManager
          )
        {
            _stateRepository = stateRepository;
            _unitOfWorkManager = unitOfWorkManager;
        }
        [UnitOfWork]
        public ListResultDto<StateListDto> GetState()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var states = _stateRepository
                .GetAll()
                .OrderByDescending(p => p.Id)
                .ToList();

                return new ListResultDto<StateListDto>(states.MapTo<List<StateListDto>>());
            }
        }
    }
}
