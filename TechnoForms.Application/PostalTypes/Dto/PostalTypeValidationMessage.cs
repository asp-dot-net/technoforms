﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Postaltypes.Dto
{
    public class PostalTypeValidationMessage
    {
        public string Message { get; set; }
        public bool Status { get; set; }
    }
}
