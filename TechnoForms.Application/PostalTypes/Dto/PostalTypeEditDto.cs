﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Postaltypes.Dto
{
    [AutoMap(typeof(PostalType))]
    public class PostalTypeEditDto : EntityDto
    {
        [Required]
        [StringLength(PostalType.MaxPostalTypeNameLength)]
        public string PostalTypeName { get; set; }

        [Required]
        [StringLength(PostalType.MaxPostalTypeCodeLength)]
        public string PostalTypeCode { get; set; }
    }
}
