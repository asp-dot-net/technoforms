﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Postaltypes.Dto
{
    [AutoMapFrom(typeof(PostalType))]
    public class PostalTypeListDto : FullAuditedEntityDto
    {
        public string PostalTypeName { get; set; }
        public string PostalTypeCode { get; set; }
    }
}
