﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Postaltypes.Dto
{
    public class PostalTypeCheckExistDto : EntityDto
    {
        public string PostalTypeName { get; set; }
        public string PostalTypeCode { get; set; }
    }
}
