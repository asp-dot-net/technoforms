﻿using Abp.Runtime.Validation;
using TechnoForms.Dto;

namespace TechnoForms.Postaltypes.Dto
{
    public class GetPostalTypeInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public string Filter { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "Id";
            }
        }
    }
}
