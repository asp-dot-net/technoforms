﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Postaltypes.Dto
{
    [AutoMapTo(typeof(PostalType))]
    public class CreatePostalTypeInput
    {
        [Required]
        [MaxLength(PostalType.MaxPostalTypeNameLength)]
        public string PostalTypeName { get; set; }

        [Required]
        [MaxLength(PostalType.MaxPostalTypeCodeLength)]
        public string PostalTypeCode { get; set; }
    }
}
