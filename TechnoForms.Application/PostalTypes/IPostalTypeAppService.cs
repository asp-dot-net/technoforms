﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Postaltypes.Dto;

namespace TechnoForms.Postaltypes
{
    public interface IPostalTypeAppService : IApplicationService
    {
        Task<PagedResultDto<PostalTypeListDto>> GetPostalType(GetPostalTypeInput input);
        Task CreatePostalType(CreatePostalTypeInput input);
        Task DeletePostalType(EntityDto input);
        Task UpdatePostalType(PostalTypeEditDto input);
        Task<PostalTypeEditDto> GetPostalTypeForEdit(EntityDto input);
        PostalTypeValidationMessage CheckPostalTypeExists(PostalTypeCheckExistDto input);
    }
}
