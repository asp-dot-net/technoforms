﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using TechnoForms.Authorization;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Extensions;
using TechnoForms.Postaltypes;
using TechnoForms.Postaltypes.Dto;
using System.Data.Entity;using Abp.Linq.Extensions;using System.Linq.Dynamic;

namespace TechnoForms.Postaltypes
{
    [AbpAuthorize(AppPermissions.Pages_Masters_PostalType)]
    public class PostalTypeAppService : TechnoFormsAppServiceBase, IPostalTypeAppService
    {
        private readonly IRepository<PostalType> _postaltypeRepository;
        public PostalTypeAppService(
          IRepository<PostalType> postaltypeRepository)
        {
            _postaltypeRepository = postaltypeRepository;
        }
        [AbpAuthorize(AppPermissions.Pages_Masters_PostalType_Create)]
        public async Task CreatePostalType(CreatePostalTypeInput input)
        {
            var postaltype = input.MapTo<PostalType>();
            await _postaltypeRepository.InsertAsync(postaltype);
        }
        [AbpAuthorize(AppPermissions.Pages_Masters_PostalType_Delete)]
        public async Task DeletePostalType(EntityDto input)
        {
            await _postaltypeRepository.DeleteAsync(input.Id);
        }
        [AbpAuthorize(AppPermissions.Pages_Masters_PostalType_Edit)]
        public async Task UpdatePostalType(PostalTypeEditDto input)
        {
            var postaltype = await _postaltypeRepository.GetAsync(input.Id);
            postaltype.PostalTypeCode = input.PostalTypeCode;
            postaltype.PostalTypeName = input.PostalTypeName;
            await _postaltypeRepository.UpdateAsync(postaltype);
        }
        [AbpAuthorize(AppPermissions.Pages_Masters_PostalType_Edit)]
        public async Task<PostalTypeEditDto> GetPostalTypeForEdit(EntityDto input)
        {
            var postaltype = (await _postaltypeRepository.GetAsync(input.Id)).MapTo<PostalTypeEditDto>(); ;
            return postaltype;
        }
        public async Task<PagedResultDto<PostalTypeListDto>> GetPostalType(GetPostalTypeInput input)
        {
            var postaltypes = _postaltypeRepository
                .GetAll()
                .WhereIf(
                    !input.Filter.IsNullOrEmpty(),
                    p => p.PostalTypeName.Contains(input.Filter) ||
                         p.PostalTypeCode.Contains(input.Filter)
                );
            var resultCount = await postaltypes.CountAsync();            var results = await postaltypes                .AsNoTracking()                .OrderBy(input.Sorting)                .PageBy(input)                .ToListAsync();            return new PagedResultDto<PostalTypeListDto>(resultCount, results.MapTo<List<PostalTypeListDto>>());
        }
        public PostalTypeValidationMessage CheckPostalTypeExists(PostalTypeCheckExistDto input)
        {
            var status_val = new PostalTypeValidationMessage();
            status_val.Status = true;
            var postaltypes = _postaltypeRepository
               .GetAll()
               .Where(p => p.PostalTypeName == input.PostalTypeName && p.PostalTypeCode == input.PostalTypeCode).ToList();

            if (!string.IsNullOrEmpty(input.Id.ToString()) && input.Id != 0)
            {
                postaltypes = postaltypes.Where(p => p.Id != input.Id).ToList();
            }
            if (postaltypes.Count > 0)
            {
                status_val.Status = false;
            }
            if (postaltypes.Count > 0)
            {
                status_val.Message = "Record with this name already exists.";
            }
            return status_val;
        }
    }
}
