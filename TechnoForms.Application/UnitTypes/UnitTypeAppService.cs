﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using Abp.AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.UnitTypes.Dto;
using Abp.ObjectMapping;
using AutoMapper;
using Abp.Extensions;
using Abp.Collections.Extensions;
using System.Data.Entity;using Abp.Linq.Extensions;using System.Linq.Dynamic;

namespace TechnoForms.UnitTypes
{
    [AbpAuthorize(AppPermissions.Pages_Masters_UnitType)]
    public class UnitTypeAppService : TechnoFormsApplicationModule, IUnitTypeAppService
    {
        private readonly IRepository<UnitType> _unittypeRepository;

        public UnitTypeAppService(IRepository<UnitType> unittypeRepository)
        {
            _unittypeRepository = unittypeRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_UnitType_Create)]
        public async Task CreateUnitType(CreateOrUpdateUnitTypeDto input)
        {
            var UnitTypes = input.MapTo<UnitType>();
            await _unittypeRepository.InsertAsync(UnitTypes);
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_UnitType_Delete)]
        public async Task DeleteUnitType(EntityDto input)
        {
            await _unittypeRepository.DeleteAsync(input.Id);
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_UnitType_Edit)]
        public async Task<EditUnitTypeDto> GetUnitTypeForEdit(EntityDto input)
        {
            var unittype = (await _unittypeRepository.GetAsync(input.Id)).MapTo<EditUnitTypeDto>(); ;
            return unittype;
        }

        [AbpAuthorize(AppPermissions.Pages_Masters_UnitType_Edit)]
        public async Task UpdateUnitType(EditUnitTypeDto input)
        {
            var unittype = await _unittypeRepository.GetAsync(input.Id);
            unittype.UnitTypeName = input.UnitTypeName;
            await _unittypeRepository.UpdateAsync(unittype);
        }

        public async Task<PagedResultDto<UnitTypeListDto>> getUnitType(GetUnitTypeInput input)
        {
            var unittype = _unittypeRepository
                .GetAll()
                .WhereIf(
                    !input.UnitTypeName.IsNullOrEmpty(),
                    p => p.UnitTypeName.ToLower().Contains(input.UnitTypeName.ToLower())
                );

            var resultCount = await unittype.CountAsync();            var results = await unittype                .AsNoTracking()                .OrderBy(input.Sorting)                .PageBy(input)                .ToListAsync();            return new PagedResultDto<UnitTypeListDto>(resultCount, results.MapTo<List<UnitTypeListDto>>());
        }

        public UnitTypeValidationMessage CheckUnitTypeExists(UnitTypeCheckExistDto input)
        {
            var status_val = new UnitTypeValidationMessage();
            status_val.Status = true;
            var unittype = _unittypeRepository
               .GetAll()
               .Where(p => p.UnitTypeName == input.UnitTypeName).ToList();

            if (!string.IsNullOrEmpty(input.Id.ToString()) && input.Id != 0)
            {
                unittype = unittype.Where(p => p.Id != input.Id).ToList();
            }
            if (unittype.Count > 0)
            {
                status_val.Status = false;
            }
            if (unittype.Count > 0)
            {
                status_val.Message = "Record with this name already exists.";
            }
            return status_val;
        }
    }
}