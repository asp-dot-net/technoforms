﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.UnitTypes.Dto;

namespace TechnoForms.UnitTypes
{
    public interface IUnitTypeAppService : IApplicationService
    {
        Task<PagedResultDto<UnitTypeListDto>> getUnitType(GetUnitTypeInput input);
        Task CreateUnitType(CreateOrUpdateUnitTypeDto input);
        Task DeleteUnitType(EntityDto input);
        Task UpdateUnitType(EditUnitTypeDto input);
        Task<EditUnitTypeDto> GetUnitTypeForEdit(EntityDto input);
        UnitTypeValidationMessage CheckUnitTypeExists(UnitTypeCheckExistDto input);
    }
}
