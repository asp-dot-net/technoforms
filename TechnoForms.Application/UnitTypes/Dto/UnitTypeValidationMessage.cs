﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.UnitTypes.Dto
{
    public class UnitTypeValidationMessage
    {
        public string Message { get; set; }
        public bool Status { get; set; }
    }
}
