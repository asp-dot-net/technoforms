﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.UnitTypes.Dto
{
    [AutoMap(typeof(UnitType))]
    public class EditUnitTypeDto : EntityDto
    {
        [Required]
        [MaxLength(UnitType.MaxUnitTypeNameLength)]
        public string UnitTypeName { get; set; }
    }
}
