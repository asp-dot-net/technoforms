﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.UnitTypes.Dto
{
    public class UnitTypeCheckExistDto : EntityDto
    {
        public string UnitTypeName { get; set; }
    }
}
