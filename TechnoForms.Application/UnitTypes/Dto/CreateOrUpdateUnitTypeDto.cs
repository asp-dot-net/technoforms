﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.UnitTypes.Dto
{
    [AutoMapTo(typeof(UnitType))]
    public class CreateOrUpdateUnitTypeDto : EntityDto
    {
        [Required]
        [MaxLength(UnitType.MaxUnitTypeNameLength)]
        public string UnitTypeName { get; set; }
    }
}
