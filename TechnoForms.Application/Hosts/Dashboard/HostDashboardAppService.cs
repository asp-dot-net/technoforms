﻿using Abp;
using Abp.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.Hosts.Dashboard.Dto;

namespace TechnoForms.Hosts.Dashboard
{
    [AbpAuthorize(AppPermissions.Pages_Host_Dashboard)]
    public class HostDashboardAppService : TechnoFormsAppServiceBase, IHostDashboardAppService
    {
        HostGetMemberActivityOutput IHostDashboardAppService.HostGetMemberActivityOutput()
        {
            return new HostGetMemberActivityOutput
            {
                TotalMembers = Enumerable.Range(0, 13).Select(i => RandomHelper.GetRandom(15, 40)).ToList(),
                NewMembers = Enumerable.Range(0, 13).Select(i => RandomHelper.GetRandom(3, 15)).ToList()
            };
        }
    }
}
