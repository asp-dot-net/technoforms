﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Hosts.Dashboard.Dto;

namespace TechnoForms.Hosts.Dashboard
{
    public interface IHostDashboardAppService : IApplicationService
    {
        HostGetMemberActivityOutput HostGetMemberActivityOutput();
    }
}
