﻿using System.Collections.Generic;

namespace TechnoForms.Hosts.Dashboard.Dto
{
    public class HostGetMemberActivityOutput
    {
        public List<int> TotalMembers { get; set; }

        public List<int> NewMembers { get; set; }
    }
}