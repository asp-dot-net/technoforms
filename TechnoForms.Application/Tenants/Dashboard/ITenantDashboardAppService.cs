﻿using Abp.Application.Services;
using TechnoForms.Tenants.Dashboard.Dto;

namespace TechnoForms.Tenants.Dashboard
{
    public interface ITenantDashboardAppService : IApplicationService
    {
        GetMemberActivityOutput GetMemberActivity();
    }
}
