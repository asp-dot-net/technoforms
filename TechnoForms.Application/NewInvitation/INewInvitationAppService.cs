﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.NewInvitation.Dto;

namespace TechnoForms.NewInvitation
{
    public interface INewInvitationAppService : IApplicationService
    {
        Task<PagedResultDto<InvitationListDto>> GetInvitation(GetNewInvitationInput input);        
        Task ApproveInvitation(InvGetInvitationForApprove input);
        Task<GetInvitationForReject> GetInvitationForReject(EntityDto input);
        Task RejectInvitation(GetInvitationForReject input);
        Task<InvitationDetailDto> GetInvitationDetail(EntityDto input);

        ListResultDto<InvInvitationStatusListDto> GetInvitationStatus();
        ListResultDto<InvUserListDto> GetUsers();
        ListResultDto<InvStateListDto> GetState();
        ListResultDto<InvPostCodeListDto> GetPostCode();
        ListResultDto<InvSuburbListDto> GetSuburb();
        ListResultDto<InvPostalAddressTypeListDto> GetPostalAddressType();
        ListResultDto<InvPostalDeliveryTypeListDto> GetPostalDeliveryType();
        ListResultDto<InvUnitTypeListDto> GetUnitType();
        ListResultDto<InvStreetTypeListDto> GetStreetType();
        ListResultDto<InvStreetNameListDto> GetStreetName();
    }
}
