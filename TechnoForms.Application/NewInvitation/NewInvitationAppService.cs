﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Runtime.Session;
using TechnoForms.Authorization;
using TechnoForms.Authorization.Roles;
using TechnoForms.Authorization.Users;
using TechnoForms.Installers;
using TechnoForms.MultiTenancy;
using TechnoForms.NewInvitation.Dto;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;
using TechnoForms.PostCodes;
using TechnoForms.States;
using TechnoForms.StreetName;
using TechnoForms.StreetTypes;
using TechnoForms.UnitTypes;
using System.Data.Entity;using Abp.Linq.Extensions;using System.Linq.Dynamic;
using Abp.Extensions;
using TechnoForms.InvitationsStatus;
using Abp.UI;

namespace TechnoForms.NewInvitation
{
    public class NewInvitationAppService : TechnoFormsApplicationModule, INewInvitationAppService
    {
        private readonly IRepository<Installer> _InstallerRepository;
        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<PostCode> _postCodeRepository;
        private readonly IRepository<PostalAddressType> _addressTypeRepository;
        private readonly IRepository<PostalDeliveryType> _delevaryTypeRepository;
        private readonly IRepository<UnitType> _unitTypeRepository;
        private readonly IRepository<StreetType> _streetTypeRepository;
        private readonly IRepository<StreetNames> _streetNameRepository;
        private readonly IRepository<InvitationStatus> _invitationStatusRepository;

        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IUserEmailer _userEmailer;
        private readonly IAbpSession _session;

        public NewInvitationAppService(IRepository<Installer> InstallerRepository, IRepository<User, long> UserRepository, IRepository<UserRole, long> UserRoleRepository, IRepository<Role> RoleRepository, IRepository<State> StateRepository, IRepository<PostCode> PostCodeRepository, IRepository<PostalAddressType> AddressTypeRepository, IRepository<PostalDeliveryType> DeliveryTypeRepository, IRepository<UnitType> UnitTypeRepository, IRepository<StreetType> StreetTypeRepository, IRepository<StreetNames> StreetNameRepository, IUnitOfWorkManager unitOfWorkManager, IRepository<Tenant> tenantRepository, IRepository<InvitationStatus> InvitationStatusRepository, IUserEmailer userEmailer, IAbpSession session)
        {
            _InstallerRepository = InstallerRepository;
            _userRepository = UserRepository;
            _userroleRepository = UserRoleRepository;
            _roleRepository = RoleRepository;

            _stateRepository = StateRepository;
            _postCodeRepository = PostCodeRepository;
            _addressTypeRepository = AddressTypeRepository;
            _delevaryTypeRepository = DeliveryTypeRepository;
            _unitTypeRepository = UnitTypeRepository;
            _streetTypeRepository = StreetTypeRepository;
            _streetNameRepository = StreetNameRepository;
            _invitationStatusRepository = InvitationStatusRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _tenantRepository = tenantRepository;
            _userEmailer = userEmailer;
            _session = session;
        }
        [UnitOfWork]
        public async Task<PagedResultDto<InvitationListDto>> GetInvitation(GetNewInvitationInput input)
        {
            var resultCount = 0;
            var Invitation = new List<InvitationListDto>();

            using (_unitOfWorkManager.Current.SetTenantId(null))            {
                //IsApproved == 0 ----- Pending
                //IsApproved == 1 ----- Approved
                //IsApproved == 2 ----- Rejected


                if (input.PageStatus == 0)
                {
                    var Invitation_List = _InstallerRepository
                    .GetAll()
                    .WhereIf(
                    !input.InstName.IsNullOrEmpty(),
                    p => p.FullName.ToLower().Contains(input.InstName.ToLower()))
                    .Where(I => I.IsApproved == 0);

                    resultCount = await Invitation_List.CountAsync();
                    var results = await Invitation_List
                        .AsNoTracking()
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();

                    Invitation = results.MapTo<List<InvitationListDto>>();
                }
                else
                {
                    var Invitation_List = _InstallerRepository
                    .GetAll()
                    .WhereIf(
                    !input.InstName.IsNullOrEmpty(),
                    p => p.FullName.ToLower().Contains(input.InstName.ToLower()))
                    .WhereIf(input.VerificationStatus != null, I => I.IsApproved == input.VerificationStatus);

                    resultCount = await Invitation_List.CountAsync();
                    var results = await Invitation_List
                        .AsNoTracking()
                        .OrderBy(input.Sorting)
                        .PageBy(input)
                        .ToListAsync();

                    Invitation = results.MapTo<List<InvitationListDto>>();
                }

                foreach (var item in Invitation)
                {
                    item.CustName = _tenantRepository.GetAll().Where(p => p.Id == item.TenantId).Select(p => p.FullName).FirstOrDefault();
                }
            }
            return new PagedResultDto<InvitationListDto>(resultCount, Invitation.MapTo<List<InvitationListDto>>());
        }
        [UnitOfWork]
        public async Task<InvitationDetailDto> GetInvitationDetail(EntityDto input)
        {
            var installer = new InvitationDetailDto();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)
            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                installer = (await _InstallerRepository.GetAsync(input.Id)).MapTo<InvitationDetailDto>();
                installer.AddressTypeName = _addressTypeRepository.GetAll().Where(p => p.Id == installer.AddressType).Select(p => p.PostalAddressTypeName).FirstOrDefault();
                installer.PostalDelTypeName = _delevaryTypeRepository.GetAll().Where(p => p.Id == installer.PostalDelType).Select(p => p.PostalDeliveryTypeName).FirstOrDefault();
            }
            //}
            return installer;
        }
        [UnitOfWork]
        public async Task ApproveInvitation(InvGetInvitationForApprove input)
        {
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)
            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var UserDetail = _userRepository
                  .GetAll()
                  .Where(
                       p => p.Id == input.UserId
                   )
                  .OrderByDescending(p => p.Id)
                  .FirstOrDefault();

                var invitationApprove = _InstallerRepository
                .GetAll()
                .WhereIf(
                input.Id != 0,
                p => p.Id == input.Id
                )
                .FirstOrDefault();

                if (invitationApprove.FullName.ToLower() != UserDetail.Name.ToLower() + " " + UserDetail.Surname.ToLower() && invitationApprove.Mobile != UserDetail.PhoneNumber && invitationApprove.EmailId.ToLower() != UserDetail.EmailAddress.ToLower())
                {
                    throw new UserFriendlyException("Ooppps! There is a problem!", "User Details Not Match With Installer");
                }
                if (invitationApprove.FullName.ToLower() != UserDetail.Name.ToLower() + " " + UserDetail.Surname.ToLower())
                {
                    throw new UserFriendlyException("Ooppps! There is a problem!", "User Full Name is Different Then Installer");
                }
                if (invitationApprove.Mobile != UserDetail.PhoneNumber)
                {
                    throw new UserFriendlyException("Ooppps! There is a problem!", "User Phone Number is Different Then Installer");
                }
                if (invitationApprove.EmailId.ToLower() != UserDetail.EmailAddress.ToLower())
                {
                    throw new UserFriendlyException("Ooppps! There is a problem!", "User Email Address is Different Then Installer");
                }

                invitationApprove.UserId = input.UserId;
                invitationApprove.UserName = UserDetail.UserName;
                //invitationApprove.FullName = UserDetail.Name + " " + UserDetail.Surname;
                //invitationApprove.Phone = UserDetail.PhoneNumber;
                //invitationApprove.EmailId = UserDetail.EmailAddress;
                invitationApprove.IsApproved = 1;

                var tenantid = _session.TenantId;

                //await _userEmailer.SendEmailInstallerVerificationResponseAsync(invitationApprove.EmailId, tenantid.ToString(), invitationApprove.FullName, "Approved", "");

                await _InstallerRepository.UpdateAsync(invitationApprove);
            }
            //}
        }
        [UnitOfWork]
        public async Task<GetInvitationForReject> GetInvitationForReject(EntityDto input)
        {
            var invitationRej = new GetInvitationForReject();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //using (_unitOfWorkManager.Current.SetTenantId(null))
            //{
            //for (int i = 0; i < tenant.Count; i++)
            //{
            invitationRej = (await _InstallerRepository.GetAsync(input.Id)).MapTo<GetInvitationForReject>();
            //}
            //}
            return invitationRej;
        }
        [UnitOfWork]
        public async Task RejectInvitation(GetInvitationForReject input)
        {
            var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            for (int i = 0; i < tenant.Count; i++)
            {
                var invitationReject = _InstallerRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.Id == input.Id
                )
                .FirstOrDefault();

                invitationReject.IsApproved = 2;
                invitationReject.RejectReason = input.RejectReason;

                //await _userEmailer.SendEmailInstallerVerificationResponseAsync(invitationReject.EmailId, _session.TenantId.ToString(), invitationReject.FullName, "Rejected", input.RejectReason);

                await _InstallerRepository.UpdateAsync(invitationReject);
            }
        }


        public ListResultDto<InvInvitationStatusListDto> GetInvitationStatus()
        {
            var invitationStatus = _invitationStatusRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InvInvitationStatusListDto>(invitationStatus.MapTo<List<InvInvitationStatusListDto>>());
        }

        public ListResultDto<InvPostalAddressTypeListDto> GetPostalAddressType()
        {
            var addresstype = _addressTypeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InvPostalAddressTypeListDto>(addresstype.MapTo<List<InvPostalAddressTypeListDto>>());
        }

        public ListResultDto<InvPostalDeliveryTypeListDto> GetPostalDeliveryType()
        {
            var delevarytype = _delevaryTypeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InvPostalDeliveryTypeListDto>(delevarytype.MapTo<List<InvPostalDeliveryTypeListDto>>());
        }

        public ListResultDto<InvPostCodeListDto> GetPostCode()
        {
            var postcode = _postCodeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InvPostCodeListDto>(postcode.MapTo<List<InvPostCodeListDto>>());
        }

        public ListResultDto<InvStateListDto> GetState()
        {
            var states = _stateRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InvStateListDto>(states.MapTo<List<InvStateListDto>>());
        }

        public ListResultDto<InvStreetNameListDto> GetStreetName()
        {
            var streername = _streetNameRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InvStreetNameListDto>(streername.MapTo<List<InvStreetNameListDto>>());
        }

        public ListResultDto<InvStreetTypeListDto> GetStreetType()
        {
            var streettype = _streetTypeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InvStreetTypeListDto>(streettype.MapTo<List<InvStreetTypeListDto>>());
        }

        public ListResultDto<InvSuburbListDto> GetSuburb()
        {
            var suburb = _postCodeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

            return new ListResultDto<InvSuburbListDto>(suburb.MapTo<List<InvSuburbListDto>>());
        }

        public ListResultDto<InvUnitTypeListDto> GetUnitType()
        {
            var unittype = _unitTypeRepository
                .GetAll()
                .OrderByDescending(p => p.Id)
                .ToList();

            return new ListResultDto<InvUnitTypeListDto>(unittype.MapTo<List<InvUnitTypeListDto>>());
        }

        [UnitOfWork]
        public ListResultDto<InvUserListDto> GetUsers()
        {
            var userlist = new List<User>();
            var user_list = new List<InvUserListDto>();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();

            //for (int i = 0; i < tenant.Count; i++)
            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var InstList = _InstallerRepository
                    .GetAll()
                    .Where(p => p.UserId != null)
                    .Select(p => p.UserId)
                    .ToList();

                var UserList = _userRepository
                    .GetAll()
                    .WhereIf(InstList.Count != 0, U => !InstList.Contains((int)U.Id))
                    .OrderByDescending(p => p.Id)
                    .ToList();

                var User = _userRepository
                .GetAll()
                .OrderByDescending(p => p.Id)
                .ToList();

                var Useritem = (from user in UserList
                                join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                                from ur in urJoined.DefaultIfEmpty()
                                join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                                from us in usJoined.DefaultIfEmpty()
                                where (us != null && us.DisplayName == "Installer" && user.TenantId == null)
                                group user by user into userGrouped
                                select userGrouped.Key);


                foreach (var User_List in Useritem)
                {
                    userlist.Add(User_List);
                    user_list = userlist.MapTo<List<InvUserListDto>>();
                }

                //using (_unitOfWorkManager.Current.SetTenantId(null))
                //{
                //    //using (_session.Use(null, null))
                //    //{
                //    var User_List = _userRepository
                //   .GetAll()
                //   .OrderByDescending(p => p.Id)
                //   .ToList();

                //    var UserList = (from user in User_List
                //                    join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                //                    from ur in urJoined.DefaultIfEmpty()
                //                    join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                //                    from us in usJoined.DefaultIfEmpty()
                //                    where (us != null && us.DisplayName == "Installer" && user.TenantId != null)
                //                    group user by user into userGrouped
                //                    select userGrouped.Key);

                //    return new ListResultDto<InvUserListDto>(UserList.MapTo<List<InvUserListDto>>());
                //}
                //    }
            }

            return new ListResultDto<InvUserListDto>(user_list);
        }
    }
}
