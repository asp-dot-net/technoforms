﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Installers;

namespace TechnoForms.NewInvitation.Dto
{
    [AutoMap(typeof(Installer))]
    public class RejectInvitationDto : FullAuditedEntityDto
    {
        [Required]
        public virtual string rejectReason { get; set; }
    }
}
