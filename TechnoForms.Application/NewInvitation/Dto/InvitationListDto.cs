﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Installers;

namespace TechnoForms.NewInvitation.Dto
{
    [AutoMapFrom(typeof(Installer))]
    public class InvitationListDto : FullAuditedEntityDto
    {
        public virtual int TenantId { get; set; }

        public virtual int? UserId { get; set; }

        public virtual string CustName { get; set; }

        public virtual string FullName { get; set; }

        public virtual Guid Guid { get; set; }

        public virtual string InstallerExpiry { get; set; }

        public virtual DateTime? InsAccreExDate { get; set; }

        public virtual int IsApproved { get; set; }

        public virtual string RejectReason { get; set; }
    }
}
