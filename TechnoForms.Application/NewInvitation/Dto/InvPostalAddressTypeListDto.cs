﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostalAddressTypes;

namespace TechnoForms.NewInvitation.Dto
{
    [AutoMapFrom(typeof(PostalAddressType))]
    public class InvPostalAddressTypeListDto : FullAuditedEntityDto
    {
        public string PostalAddressTypeName { get; set; }
    }
}
