﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Installers;

namespace TechnoForms.NewInvitation.Dto
{
    [AutoMap(typeof(Installer))]
    public class GetInvitationForReject : FullAuditedEntityDto
    {
        public virtual string InstallerExpiry { get; set; }

        public virtual string RejectReason { get; set; }
    }
}
