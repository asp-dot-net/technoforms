﻿using Abp.Application.Services.Dto;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization.Users;

namespace TechnoForms.NewInvitation.Dto
{
    [AutoMapFrom(typeof(User))]
    public class InvUserListDto : FullAuditedEntityDto
    {
        public string UserName { get; set; }

        public List<NewInvUserListRoleDto> Roles { get; set; }

        [AutoMapFrom(typeof(UserRole))]
        public class NewInvUserListRoleDto
        {
            public int RoleId { get; set; }

            public string RoleName { get; set; }
        }
    }
}