﻿using Abp.Application.Services.Dto;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Dto;

namespace TechnoForms.NewInvitation.Dto
{
    public class GetNewInvitationInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public int PageStatus { get; set; }

        public int? VerificationStatus { get; set; }

        public string InstName { get; set; }

        //public string CustName { get; set; }

        public void Normalize()
        {
            if (string.IsNullOrEmpty(Sorting))
            {
                Sorting = "FullName";
            }
            //if (Sorting == "custName desc")
            //{
            //    Sorting = "TenantId desc";
            //}
            //if (Sorting == "custName asc")
            //{
            //    Sorting = "TenantId asc";
            //}
        }
    }
}
