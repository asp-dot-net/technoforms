﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostalDeliveryTypes;

namespace TechnoForms.NewInvitation.Dto
{
    [AutoMapFrom(typeof(PostalDeliveryType))]
    public class InvPostalDeliveryTypeListDto : FullAuditedEntityDto
    {
        public string PostalDeliveryTypeName { get; set; }
    }
}
