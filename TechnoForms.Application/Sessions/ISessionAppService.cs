﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TechnoForms.Sessions.Dto;

namespace TechnoForms.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
