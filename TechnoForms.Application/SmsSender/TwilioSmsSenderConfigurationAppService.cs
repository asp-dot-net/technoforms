﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Dependency;
using Microsoft.Extensions.Configuration;
using TechnoForms.Configuration;

namespace TechnoForms.SmsSender
{
    public class TwilioSmsSenderConfigurationAppService : ITransientDependency
    {
        private readonly IConfigurationRoot _appConfiguration;

        public string AccountSid => _appConfiguration["Twilio:AC991f3f103f6a5f608278c1e283f139a1"];

        public string AuthToken => _appConfiguration["Twilio:be9c00d8443b360082ab02d3d78969b2"];

        public string SenderNumber => _appConfiguration["Twilio:+61418671214"];

        public TwilioSmsSenderConfigurationAppService(IAppConfigurationAccessor configurationAccessor)
        {
            _appConfiguration = configurationAccessor.Configuration;
        }
    }
}
