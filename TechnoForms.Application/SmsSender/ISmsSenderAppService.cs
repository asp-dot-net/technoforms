﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.SmsSender
{
    public interface ISmsSenderAppService
    {
        Task SendAsync(string number, string message);
    }
}
