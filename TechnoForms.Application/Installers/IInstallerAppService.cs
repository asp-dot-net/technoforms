﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TechnoForms.Installers.Dto;
using TechnoForms.PostCodes.Dto;

namespace TechnoForms.Installers
{
    public interface IInstallerAppService : IApplicationService
    {
        Task<PagedResultDto<InstallerListDto>> GetInstaller(GetInstallerInput input);
        Task CreateInstaller(CreateInstallerDto input);
        Task CreateExistInstaller(CreateExistInstallerDto input);
        Task DeleteInstaller(EntityDto input);
        Task UpdateInstaller(InstallerEditDto input);
        Task<InstallerEditDto> GetInstallerForEdit(EntityDto input);
        Task<InstallerDetailDto> GetInstallerForDetail(EntityDto input);
        Task<InstallerEditDto> GetInstallerNameForEmp(EntityDto input);


        ListResultDto<InstallerEmployeeListDto> GetInstallerEmployee(GetInstallerEmployeeInput input);
        Task CreateInstallerEmployee(CreateInstallerEmployeeDto input);
        Task DeleteInstallerEmployee(EntityDto input);
        Task UpdateInstallerEmployee(InstallerEmployeeEditDto input);
        Task<InstallerEmployeeEditDto> GetInstallerEmployeeForEdit(EntityDto input);

        int CheckInstallerCode(string InstCode);
        int CheckDesignerCode(string InstCode);
        int CheckElecCode(string InstCode);
        int CheckInstallerCodeEdit(string InstCode, int Id);
        int CheckDesignerCodeEdit(string InstCode, int Id);
        int CheckElecCodeEdit(string InstCode, int Id);

        ListResultDto<InstUserListDto> GetUsers();
        ListResultDto<InstStateListDto> GetState();
        ListResultDto<InstPostCodeListDto> GetPostCode();
        ListResultDto<InstPostCodeListDto> GetPostCodesListBySuburb(EntityDto Input);
        ListResultDto<InstPostalAddressTypeListDto> GetPostalAddressType();
        ListResultDto<InstPostalDeliveryTypeListDto> GetPostalDeliveryType();
        List<typeAhead> GetStreetNames(TypeAheadInput input);
        List<typeAhead> GetStreetType(TypeAheadInput input);
        List<typeAhead> GetUnitType(TypeAheadInput input);
        List<typeAhead> GetAllPostCodes(TypeAheadInput input);

        ListResultDto<InstEmployeListDto> GetInstallerEmployeInstallerByInstallerId(EntityDto input);
        ListResultDto<InstEmployeListDto> GetInstallerEmployeDesignerByInstallerId(EntityDto input);
        ListResultDto<InstEmployeListDto> GetInstallerEmployeElectricianByInstallerId(EntityDto input);

        ListResultDto<InstSuburbListDto> AutoCompleteSuburb(string element);
    }
}
