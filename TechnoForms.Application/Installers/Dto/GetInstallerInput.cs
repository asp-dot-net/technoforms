﻿using Abp.Application.Services.Dto;
using Abp.Extensions;
using Abp.Runtime.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Dto;

namespace TechnoForms.Installers.Dto
{
    public class GetInstallerInput : PagedAndSortedInputDto, IShouldNormalize
    {
        public virtual string FullName { get; set; }

        public virtual string State { get; set; }

        public void Normalize()
        {
            if (Sorting.IsNullOrWhiteSpace())
            {
                Sorting = "FullName";
            }
        }
    }
}
