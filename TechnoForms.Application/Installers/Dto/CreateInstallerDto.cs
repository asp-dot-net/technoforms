﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;

namespace TechnoForms.Installers.Dto
{
    [AutoMapTo(typeof(Installer))]
    public class CreateInstallerDto : EntityDto
    {
        public virtual int? UserId { get; set; }

        public virtual string UserName { get; set; }

        [Required]
        [MaxLength(Installer.MaxFullNameLength)]
        public virtual string FullName { get; set; }

        [Required]
        [MaxLength(Installer.MaxPhoneLength)]
        public virtual string Phone { get; set; }

        [MaxLength(Installer.MaxMobileLength)]
        [RegularExpression(@"^(07|03|08|04|13|18)[\d]{8}$", ErrorMessage = "Enter valid Mobile(eg. 07|03|08|04|13|18XXXXXXXX)")]
        public virtual string Mobile { get; set; }

        [Required]
        [MaxLength(Installer.MaxEmailLength)]
        public virtual string EmailId { get; set; }

        //[Required]
        [MaxLength(Installer.MaxCompanyNameLength)]
        public virtual string CompanyName { get; set; }

        //[Required]
        [MaxLength(Installer.MaxABNLength)]
        public virtual string ABN { get; set; }

        [MaxLength(Installer.MaxCompanyPhoneLength)]
        [RegularExpression(@"^(07|03|08|04|13|18)[\d]{8}$", ErrorMessage = "Enter valid Company Phone(eg. 07|03|08|04|13|18XXXXXXXX)")]
        public virtual string CompanyPhone { get; set; }

        [MaxLength(Installer.MaxFaxLength)]
        public virtual string Fax { get; set; }

        public virtual int AddressType { get; set; }

        [MaxLength(Installer.MaxPostalDelevaryNoLength)]
        public virtual string PostalDelNo { get; set; }

        public virtual int PostalDelType { get; set; }

        [Required]
        public virtual bool IsGST { get; set; }

        [Required]
        [MaxLength(Installer.MaxlogoLength)]
        public virtual string Logo { get; set; }
        //[Required]
        //[MaxLength(400)]
        //public string FileName { get; set; }
        
        [MaxLength(Installer.MaxStreetAddressLength)]
        public virtual string StreetAddress { get; set; }

        [MaxLength(Installer.MaxUnitNumberLength)]
        public virtual string UnitNo { get; set; }

        [MaxLength(Installer.MaxUnitTypeLength)]
        public virtual string UnitType { get; set; }

        [Required]
        [MaxLength(Installer.MaxStreetNumberLength)]
        public virtual string StreetNumber { get; set; }

        [Required]
        [MaxLength(Installer.MaxStreetNameLength)]
        public virtual string StreetName { get; set; }

        [Required]
        [MaxLength(Installer.MaxStreetTypeLength)]
        public virtual string StreetType { get; set; }

        [Required]
        [MaxLength(Installer.MaxStreetCityLength)]
        public virtual string Suburb { get; set; }

        public virtual int? Suburb_Id { get; set; }

        [Required]
        [MaxLength(Installer.MaxStreetStateLength)]
        public virtual string State { get; set; }

        [Required]
        [MaxLength(Installer.MaxStreetPostCodeLength)]
        public virtual string PostCode { get; set; }

        public virtual bool? IsInst { get; set; }

        [MaxLength(Installer.MaxInstallerExpiryLength)]
        public virtual string InstallerExpiry { get; set; }

        public virtual DateTime? InsAccreExDate { get; set; }

        public virtual bool? IsElec { get; set; }

        [MaxLength(Installer.MaxElectricalExpiryLength)]
        public virtual string ElectricalExpiry { get; set; }

        public virtual DateTime? EleLicenceExDate { get; set; }

        public virtual bool? IsDesc { get; set; }

        [MaxLength(Installer.MaxDesignerExpiryLength)]
        public virtual string DesignerExpiry { get; set; }

        public virtual DateTime? DesignerAccreExDate { get; set; }

        [Required]
        [MaxLength(Installer.MaxAccreditationAccPhotoLength)]
        public virtual string AccreditationAccPhoto { get; set; }

        [Required]
        [MaxLength(Installer.MaxLicencePhotoLength)]
        public virtual string LicencePhoto { get; set; }

        public virtual Guid Guid { get; set; }
    }
}
