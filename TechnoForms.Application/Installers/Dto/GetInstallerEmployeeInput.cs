﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Installers.Dto
{
    public class GetInstallerEmployeeInput
    {
        public virtual int InstallerId { get; set; }
    }
}
