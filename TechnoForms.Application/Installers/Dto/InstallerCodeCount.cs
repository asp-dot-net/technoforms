﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Installers.Dto
{
    [AutoMap(typeof(Installer))]
    public class InstallerCodeCount
    {
        public virtual int Count { get; set; }
    }
}
