﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostCodes;

namespace TechnoForms.Installers.Dto
{
    [AutoMapFrom(typeof(PostCode))]
    public class InstPostCodeListDto : FullAuditedEntityDto
    {
        public string Postcode { get; set; }
        public string Suburb { get; set; }
        public string POBoxes { get; set; }
        public string Area { get; set; }
        public virtual int StateId { get; set; }
        public string State { get; set; }
        public string stateName { get; set; }
        public string SuburbstatePostcodes { get; set; }
    }
}
