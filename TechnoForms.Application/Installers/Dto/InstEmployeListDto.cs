﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using TechnoForms.InstallerEmployes;

namespace TechnoForms.Installers.Dto
{
    [AutoMapFrom(typeof(InstallerEmploye))]
    public class InstEmployeListDto : FullAuditedEntityDto
    {
        public string FullName { get; set; }
    }
}
