﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.UnitTypes;

namespace TechnoForms.Installers.Dto
{
    [AutoMapFrom(typeof(UnitType))]
    public class InstUnitTypeListDto : FullAuditedEntityDto
    {
        public string UnitTypeName { get; set; }
    }
}
