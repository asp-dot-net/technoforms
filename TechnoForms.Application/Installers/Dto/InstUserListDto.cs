﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization.Users;
using static TechnoForms.Authorization.Users.Dto.UserListDto;

namespace TechnoForms.Installers.Dto
{
    [AutoMapFrom(typeof(User))]
    public class InstUserListDto : FullAuditedEntityDto
    {
        public string UserName { get; set; }

        public List<UserListRoleDto> Roles { get; set; }
    }
}