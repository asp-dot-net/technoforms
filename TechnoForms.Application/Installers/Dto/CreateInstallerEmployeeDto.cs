﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.InstallerEmployes;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;

namespace TechnoForms.Installers.Dto
{
    [AutoMapTo(typeof(InstallerEmploye))]
    public class CreateInstallerEmployeeDto : EntityDto
    {
        [Required]
        [MaxLength(InstallerEmploye.MaxFullNameLength)]
        public virtual string FullName { get; set; }

        [Required]
        [MaxLength(InstallerEmploye.MaxPhoneLength)]
        [RegularExpression(@"^(07|03|08|04|13|18)[\d]{8}$", ErrorMessage = "Enter valid Phone(eg. 07|03|08|04|13|18XXXXXXXX)")]
        public virtual string Phone { get; set; }

        [MaxLength(InstallerEmploye.MaxMobileLength)]
        [RegularExpression(@"^(07|03|08|04|13|18)[\d]{8}$", ErrorMessage = "Enter valid Mobile(eg. 07|03|08|04|13|18XXXXXXXX)")]
        public virtual string Mobile { get; set; }

        [Required]
        [MaxLength(InstallerEmploye.MaxEmailLength)]
        [EmailAddress]
        public virtual string EmailId { get; set; }

        [MaxLength(InstallerEmploye.MaxFaxLength)]
        public virtual string Fax { get; set; }

        [MaxLength(InstallerEmploye.MaxStreetAddressLength)]
        public virtual string StreetAddress { get; set; }

        [MaxLength(InstallerEmploye.MaxUnitNumberLength)]
        public virtual string UnitNo { get; set; }

        [MaxLength(InstallerEmploye.MaxUnitTypeLength)]
        public virtual string UnitType { get; set; }

        [Required]
        [MaxLength(InstallerEmploye.MaxStreetNumberLength)]
        public virtual string StreetNumber { get; set; }

        [Required]
        [MaxLength(InstallerEmploye.MaxStreetNameLength)]
        public virtual string StreetName { get; set; }

        [Required]
        [MaxLength(InstallerEmploye.MaxStreetTypeLength)]
        public virtual string StreetType { get; set; }

        [Required]
        [MaxLength(InstallerEmploye.MaxStreetCityLength)]
        public virtual string Suburb { get; set; }

        public virtual int? Suburb_Id { get; set; }

        [Required]
        [MaxLength(InstallerEmploye.MaxStreetStateLength)]
        public virtual string State { get; set; }

        [Required]
        [MaxLength(InstallerEmploye.MaxStreetPostCodeLength)]
        public virtual string PostCode { get; set; }

        [MaxLength(InstallerEmploye.MaxInstallerExpiryLength)]
        public virtual string InstallerExpiry { get; set; }

        public virtual DateTime? InsAccreExDate { get; set; }

        [MaxLength(InstallerEmploye.MaxElectricalExpiryLength)]
        public virtual string ElectricalExpiry { get; set; }

        public virtual DateTime? EleLicenceExDate { get; set; }

        [MaxLength(InstallerEmploye.MaxDesignerExpiryLength)]
        public virtual string DesignerExpiry { get; set; }

        public virtual DateTime? DesignerAccreExDate { get; set; }

        public virtual int InstallerId { get; set; }

        public virtual int AddressType { get; set; }

        [MaxLength(InstallerEmploye.MaxPostalDelevaryNoLength)]
        public virtual string PostalDelNo { get; set; }

        public virtual int PostalDelType { get; set; }

        public virtual bool? IsInst { get; set; }

        public virtual bool? IsElec { get; set; }

        public virtual bool? IsDesc { get; set; }
    }
}
