﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;

namespace TechnoForms.Installers.Dto
{
    [AutoMapFrom(typeof(Installer))]
    public class InstallerListDto : FullAuditedEntityDto
    {
        public virtual string AssignCustomer { get; set; }
        
        public virtual int? UserId { get; set; }

        public virtual string FullName { get; set; }

        public virtual string Phone { get; set; }

        public virtual string Mobile { get; set; }

        public virtual string EmailId { get; set; }

        public virtual string CompanyName { get; set; }

        public virtual string ABN { get; set; }

        public virtual string CompanyPhone { get; set; }

        public virtual string Fax { get; set; }

        public virtual int AddressType { get; set; }

        public virtual string PostalDelNo { get; set; }

        public virtual int PostalDelType { get; set; }

        public virtual bool IsGST { get; set; }

        public virtual string Logo { get; set; }

        public virtual string StreetAddress { get; set; }

        public virtual string UnitNo { get; set; }

        public virtual string UnitType { get; set; }

        public virtual string StreetNumber { get; set; }

        public virtual string StreetName { get; set; }

        public virtual string StreetType { get; set; }

        public virtual string Suburb { get; set; }

        public virtual int? Suburb_Id { get; set; }

        public virtual string State { get; set; }

        public virtual string PostCode { get; set; }

        public virtual bool IsInst { get; set; }

        public virtual string InstallerExpiry { get; set; }

        public virtual DateTime? InsAccreExDate { get; set; }

        public virtual bool IsElec { get; set; }

        public virtual string ElectricalExpiry { get; set; }

        public virtual DateTime? EleLicenceExDate { get; set; }

        public virtual bool IsDesc { get; set; }

        public virtual string DesignerExpiry { get; set; }

        public virtual DateTime? DesignerAccreExDate { get; set; }

        public virtual string AccreditationAccPhoto { get; set; }

        public virtual string LicencePhoto { get; set; }

        public virtual Guid Guid { get; set; }

        public virtual int IsApproved { get; set; }

        public virtual string RejectReason { get; set; }

        public virtual int Inst_Request_status { get; set; }

        public virtual string Inst_RejectReason { get; set; }

        public virtual int Request_status { get; set; }

        public virtual string Admin_RejectReason { get; set; }
    }
}
