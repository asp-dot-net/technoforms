﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostalDeliveryTypes;

namespace TechnoForms.Installers.Dto
{
    [AutoMapFrom(typeof(PostalDeliveryType))]
    public class InstPostalDeliveryTypeListDto : FullAuditedEntityDto
    {
        public string PostalDeliveryTypeName { get; set; }
    }
}
