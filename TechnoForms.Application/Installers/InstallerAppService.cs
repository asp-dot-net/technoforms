﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.AutoMapper;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Extensions;
using Abp.IO;
using Abp.UI;
using Microsoft.AspNet.Identity;
using TechnoForms.Authorization;
using TechnoForms.Authorization.Roles;
using TechnoForms.Authorization.Users;
using TechnoForms.InstallerEmployes;
using TechnoForms.Installers.Dto;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;
using TechnoForms.PostCodes;
using TechnoForms.States;
using TechnoForms.Storage;
using TechnoForms.StreetName;
using TechnoForms.StreetTypes;
using TechnoForms.UnitTypes;
using TechnoForms.CustInstEmps;
using TechnoForms.CustomerInstallers;
using Abp.Runtime.Session;
using Abp.Domain.Uow;
using TechnoForms.MultiTenancy;
using System.Data.Entity;using Abp.Linq.Extensions;using System.Linq.Dynamic;
using TechnoForms.InvitationsStatus;
using Abp.Notifications;
using TechnoForms.Notifications;
using Abp;
using Abp.Domain.Entities;
using TechnoForms.PostCodes.Dto;

namespace TechnoForms.Installers
{
    [AbpAuthorize(AppPermissions.Pages_Tenant_Installer)]
    public class InstallerAppService : TechnoFormsAppServiceBase, IInstallerAppService
    {
        private readonly IRepository<Installer> _InstallerRepository;
        private readonly IRepository<InstallerEmploye> _InstallerEmployeRepository;
        private readonly IRepository<State> _stateRepository;
        private readonly IRepository<PostCode> _postCodeRepository;
        private readonly IRepository<PostalAddressType> _addressTypeRepository;
        private readonly IRepository<PostalDeliveryType> _delevaryTypeRepository;
        private readonly IRepository<UnitType> _unitTypeRepository;
        private readonly IRepository<StreetType> _streetTypeRepository;
        private readonly IRepository<StreetNames> _streetNameRepository;
        private readonly IRepository<CustInstEmp> _custInstEmpRepository;
        private readonly IRepository<CustomerInstaller> _customerInstallerRepository;
        private readonly IRepository<InvitationStatus> _invitationStatusRepository;

        private readonly IRepository<User, long> _userRepository;
        private readonly IRepository<UserRole, long> _userroleRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IAbpSession _session;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IAppFolders _appFolders;
        private readonly IBinaryObjectManager _binaryObjectManager;
        private readonly IUserEmailer _userEmailer;

        private readonly INotificationPublisher _notificationPublisher;
        private readonly INotificationSubscriptionManager _notificationSubscriptionManager;
        private readonly IAppNotifier _appNotifier;

        public InstallerAppService(IRepository<Installer> InstallerRepository, IRepository<InstallerEmploye> InstallerEmployeRepository, IRepository<State> StateRepository, IRepository<PostCode> PostCodeRepository, IRepository<PostalAddressType> AddressTypeRepository, IRepository<PostalDeliveryType> DeliveryTypeRepository, IRepository<UnitType> UnitTypeRepository, IRepository<StreetType> StreetTypeRepository, IRepository<StreetNames> StreetNameRepository, IAppFolders appFolders, IBinaryObjectManager binaryObjectManager, IRepository<User, long> UserRepository, IRepository<UserRole, long> UserRoleRepository, IRepository<Role> RoleRepository, IRepository<CustInstEmp> CustInstEmpRepository, IRepository<CustomerInstaller> CustomerInstallerRepository, IAbpSession session, IUnitOfWorkManager unitOfWorkManager, IRepository<Tenant> tenantRepository, IRepository<InvitationStatus> InvitationStatusRepository,
          IUserEmailer userEmailer,
          INotificationPublisher notificationPublisher,
          INotificationSubscriptionManager notificationSubscriptionManager,
          IAppNotifier appNotifier)
        {
            _InstallerRepository = InstallerRepository;
            _InstallerEmployeRepository = InstallerEmployeRepository;
            _stateRepository = StateRepository;
            _postCodeRepository = PostCodeRepository;
            _addressTypeRepository = AddressTypeRepository;
            _delevaryTypeRepository = DeliveryTypeRepository;
            _unitTypeRepository = UnitTypeRepository;
            _streetTypeRepository = StreetTypeRepository;
            _streetNameRepository = StreetNameRepository;
            _appFolders = appFolders;
            _binaryObjectManager = binaryObjectManager;
            _userRepository = UserRepository;
            _userroleRepository = UserRoleRepository;
            _roleRepository = RoleRepository;
            _custInstEmpRepository = CustInstEmpRepository;
            _customerInstallerRepository = CustomerInstallerRepository;
            _invitationStatusRepository = InvitationStatusRepository;
            _session = session;
            _unitOfWorkManager = unitOfWorkManager;
            _tenantRepository = tenantRepository;
            _userEmailer = userEmailer;
            _notificationPublisher = notificationPublisher;
            _notificationSubscriptionManager = notificationSubscriptionManager;
            _appNotifier = appNotifier;
        }
        [UnitOfWork]
        public async Task<PagedResultDto<InstallerListDto>> GetInstaller(GetInstallerInput input)
        {
            long userid = (long)_session.UserId;
            var user = await UserManager.GetUserByIdAsync(userid);
            var grantedPermissions = await UserManager.GetGrantedPermissionsAsync(user);
            var Installer = new List<InstallerListDto>();
            var userspecific = grantedPermissions.Where(x => x.Name == "Pages.UserSpecificInstaller").ToList();
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var EmpList = _customerInstallerRepository
                    .GetAll()
                    .Where(c => c.IsCreated == false && c.TenantId == _session.TenantId)//c.Inst_Request_status == 1 && c.Request_status == 1 &&
                    .Select(p => p.InstallerId)
                    .ToList();

                var installer = _InstallerRepository
                    .GetAll()
                    .WhereIf(
                        EmpList.Count != 0,
                        E => EmpList.Contains(E.Id) || E.TenantId == _session.TenantId
                        )
                    .WhereIf(
                        !input.FullName.IsNullOrEmpty(),
                        p => p.FullName.ToLower().Contains(input.FullName.ToLower())
                    )
                    .WhereIf(
                        !input.State.IsNullOrEmpty(),
                        p => p.State.ToLower().Contains(input.State.ToLower())
                    );

                if (userspecific.Count > 0)
                {
                    installer = installer.Where(x => x.CreatorUserId == userid);
                }

                var resultCount = await installer.CountAsync();

                //var UserList = (from inst in installer
                //                join ur in _customerInstallerRepository.GetAll() on inst.Id equals ur.InstallerId into urJoined
                //                from ur in urJoined.DefaultIfEmpty()
                //                group user by user into userGrouped
                //                select userGrouped.Key);

                var results = await installer
                    .AsNoTracking()
                    .OrderBy(input.Sorting)
                    .PageBy(input)
                    .ToListAsync();
                Installer = results.MapTo<List<InstallerListDto>>();
                foreach (var item in Installer)
                {
                    item.Inst_Request_status = await _customerInstallerRepository.GetAll().Where(C => C.InstallerId == item.Id && C.TenantId == _session.TenantId).Select(C => C.Inst_Request_status).FirstOrDefaultAsync();
                    item.Request_status = await _customerInstallerRepository.GetAll().Where(C => C.InstallerId == item.Id && C.TenantId == _session.TenantId).Select(C => C.Request_status).FirstOrDefaultAsync();
                    item.Inst_RejectReason = await _customerInstallerRepository.GetAll().Where(C => C.InstallerId == item.Id && C.TenantId == _session.TenantId).Select(C => C.Inst_RejectReason).FirstOrDefaultAsync();
                    item.Admin_RejectReason = await _customerInstallerRepository.GetAll().Where(C => C.InstallerId == item.Id && C.TenantId == _session.TenantId).Select(C => C.RejectReason).FirstOrDefaultAsync();
                }

                return new PagedResultDto<InstallerListDto>(resultCount, Installer.MapTo<List<InstallerListDto>>());
            }        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Tenant_Installer_Create)]
        public async Task CreateInstaller(CreateInstallerDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                //var UserDetail = _userRepository
                //  .GetAll()
                //  .Where(
                //       p => p.Id == input.UserId
                //   )
                //  .OrderByDescending(p => p.Id)
                //  .FirstOrDefault();

                var Installer = new Installer();
                //Installer.UserName =  //UserDetail.UserName;
                Installer.FullName = input.FullName;//UserDetail.Name + " " + UserDetail.Surname; 
                Installer.Phone = input.Phone;//UserDetail.PhoneNumber;
                Installer.Mobile = input.Mobile;
                Installer.EmailId = input.EmailId;//UserDetail.EmailAddress;
                Installer.CompanyName = input.CompanyName;
                Installer.ABN = input.ABN;
                Installer.CompanyPhone = input.CompanyPhone;
                Installer.Fax = input.Fax;
                Installer.AddressType = input.AddressType;
                Installer.PostalDelNo = input.PostalDelNo;
                Installer.PostalDelType = input.PostalDelType;
                Installer.IsGST = input.IsGST;
                Installer.Logo = input.Logo;
                Installer.StreetAddress = input.StreetAddress;
                Installer.UnitNo = input.UnitNo;
                Installer.UnitType = input.UnitType;
                Installer.StreetNumber = input.StreetNumber;
                Installer.StreetName = input.StreetName;
                Installer.StreetType = input.StreetType;
                Installer.Suburb = input.Suburb;
                Installer.Suburb_Id = input.Suburb_Id;
                Installer.State = input.State;
                Installer.PostCode = input.PostCode;
                Installer.IsInst = input.IsInst;
                Installer.InstallerExpiry = input.InstallerExpiry;
                Installer.InsAccreExDate = input.InsAccreExDate;
                Installer.IsElec = input.IsElec;
                Installer.ElectricalExpiry = input.ElectricalExpiry;
                Installer.EleLicenceExDate = input.EleLicenceExDate;
                Installer.IsDesc = input.IsDesc;
                Installer.DesignerExpiry = input.DesignerExpiry;
                Installer.DesignerAccreExDate = input.DesignerAccreExDate;
                Installer.AccreditationAccPhoto = input.AccreditationAccPhoto;
                Installer.LicencePhoto = input.LicencePhoto;
                //Installer.UserId = input.UserId;
                //IsApproved == 0 ----- Pending
                //IsApproved == 1 ----- Approved
                //IsApproved == 2 ----- Rejected
                Installer.IsApproved = 0;
                Installer.Guid = System.Guid.NewGuid();
                Installer.TenantId = (int)_session.TenantId;
                Installer.CreatorUserId = (int)_session.UserId;
                await _InstallerRepository.InsertAndGetIdAsync(Installer);

                var InstallerEmployee = new InstallerEmploye();
                InstallerEmployee.FullName = input.FullName; //UserDetail.Name + " " + UserDetail.Surname;
                InstallerEmployee.Phone = input.Phone;//UserDetail.PhoneNumber;
                InstallerEmployee.Mobile = input.Mobile;
                InstallerEmployee.EmailId = input.EmailId; //UserDetail.EmailAddress;
                InstallerEmployee.Fax = input.Fax;
                InstallerEmployee.StreetAddress = input.StreetAddress;
                InstallerEmployee.UnitNo = input.UnitNo;
                InstallerEmployee.UnitType = input.UnitType;
                InstallerEmployee.StreetNumber = input.StreetNumber;
                InstallerEmployee.StreetName = input.StreetName;
                InstallerEmployee.StreetType = input.StreetType;
                InstallerEmployee.Suburb = input.Suburb;
                InstallerEmployee.Suburb_Id = input.Suburb_Id;
                InstallerEmployee.State = input.State;
                InstallerEmployee.PostCode = input.PostCode;
                InstallerEmployee.InstallerExpiry = input.InstallerExpiry;
                InstallerEmployee.InsAccreExDate = input.InsAccreExDate;
                InstallerEmployee.ElectricalExpiry = input.ElectricalExpiry;
                InstallerEmployee.EleLicenceExDate = input.EleLicenceExDate;
                InstallerEmployee.DesignerExpiry = input.DesignerExpiry;
                InstallerEmployee.DesignerAccreExDate = input.DesignerAccreExDate;
                InstallerEmployee.InstallerId = Installer.Id;
                InstallerEmployee.AddressType = input.AddressType;
                InstallerEmployee.PostalDelNo = input.PostalDelNo;
                InstallerEmployee.PostalDelType = input.PostalDelType;
                InstallerEmployee.IsInst = input.IsInst;
                InstallerEmployee.IsElec = input.IsElec;
                InstallerEmployee.IsDesc = input.IsDesc;
                InstallerEmployee.TenantId = (int)_session.TenantId;
                InstallerEmployee.CreatorUserId = (int)_session.UserId;
                await _InstallerEmployeRepository.InsertAndGetIdAsync(InstallerEmployee);

                var CustInstEmp = new CustInstEmp();
                CustInstEmp.EmployeeId = InstallerEmployee.Id;
                CustInstEmp.InstallerId = Installer.Id;
                CustInstEmp.IsCreated = true;
                CustInstEmp.TenantId = (int)_session.TenantId;
                CustInstEmp.CreatorUserId = (int)_session.UserId;
                await _custInstEmpRepository.InsertAndGetIdAsync(CustInstEmp);

                var CustomerInstaller = new CustomerInstaller();
                CustomerInstaller.InstallerId = Installer.Id;
                CustomerInstaller.IsCreated = true;
                CustomerInstaller.Guid = System.Guid.NewGuid();
                //IsApproved == 0 ----- Pending
                //IsApproved == 1 ----- Approved
                //IsApproved == 2 ----- Rejected
                CustomerInstaller.Request_status = 0;
                //IsApproved == 0 ----- Pending
                //IsApproved == 1 ----- Approved
                //IsApproved == 2 ----- Rejected
                CustomerInstaller.Inst_Request_status = 0;
                CustomerInstaller.TenantId = (int)_session.TenantId;
                CustomerInstaller.CreatorUserId = (int)_session.UserId;
                await _customerInstallerRepository.InsertAndGetIdAsync(CustomerInstaller);

                await _appNotifier.WelcomeInstallerAsync(Installer);
                await _notificationSubscriptionManager.SubscribeAsync(new UserIdentifier(Installer.TenantId, 2), "CommentPhoto", new EntityIdentifier(typeof(Installer), Installer.Id));
                await _notificationSubscriptionManager.SubscribeAsync(new UserIdentifier(null, 1), "SentFriendshipRequest");
                //await _notificationSubscriptionManager.SubscribeToAllAvailableNotificationsAsync(new UserIdentifier(Installer.TenantId, 2));
                //_notificationPublisher.Publish("TestNotification", null, null, NotificationSeverity.Info, null);
                //var mailMessage = new StringBuilder();
                //await _userEmailer.SendEmailInstallerVerificationRequestAsync(input.EmailId, "bhoomi@meghtechnologies.com", _session.TenantId.ToString(), input.FullName);
            }
        }

        [UnitOfWork]
        public async Task CreateExistInstaller(CreateExistInstallerDto input)
        {
            bool IsAny = false;

            bool Flag1 = false;
            bool Flag2 = false;

            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)
            //{
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var Data = _InstallerEmployeRepository
                .GetAll()
                .WhereIf(!input.InstallerExpiry.IsNullOrEmpty(), p => p.InstallerExpiry == input.InstallerExpiry)
                .FirstOrDefault();

                if (Data != null)
                {
                    var tenantid = _session.TenantId;
                    IsAny = _custInstEmpRepository.GetAll()
                             .Where(p => p.InstallerId == Data.InstallerId && p.TenantId == tenantid && p.EmployeeId == Data.Id)
                             .Any();

                    if (IsAny == false)
                    {

                        Flag1 = false;
                        Flag2 = false;
                        using (_unitOfWorkManager.Current.SetTenantId(null))
                        {
                            bool CheckExists = _customerInstallerRepository
                            .GetAll()
                            .Where(p => p.TenantId == _session.TenantId)
                            .Where(p => p.InstallerId == Data.InstallerId)
                            .Any();
                            if (!CheckExists)
                            {
                                var CustInstaller = new CustomerInstaller();
                                CustInstaller.InstallerId = Data.InstallerId;
                                CustInstaller.Guid = System.Guid.NewGuid();
                                //Request_status = 0 = Pending
                                //Request_status = 1 = Accepted
                                //Request_status = 2 = Rejected
                                CustInstaller.Request_status = 0;
                                //Inst_Request_status = 0 = Pending
                                //Inst_Request_status = 1 = Accepted
                                //Inst_Request_status = 2 = Rejected
                                CustInstaller.Inst_Request_status = 0;
                                CustInstaller.IsCreated = false;
                                CustInstaller.TenantId = (int)_session.TenantId;
                                CustInstaller.CreatorUserId = (int)_session.UserId;
                                await _customerInstallerRepository.InsertAsync(CustInstaller);
                            }
                            var CustInstEmp = new CustInstEmp();
                            CustInstEmp.InstallerId = Data.InstallerId;
                            CustInstEmp.EmployeeId = Data.Id;
                            CustInstEmp.IsCreated = false;
                            CustInstEmp.TenantId = (int)_session.TenantId;
                            CustInstEmp.CreatorUserId = (int)_session.UserId;
                            await _custInstEmpRepository.InsertAsync(CustInstEmp);
                        }
                        //break;
                    }
                    else
                    {
                        Flag2 = true;
                        //throw new UserFriendlyException("Ooppps! There is a problem!", "Installer is already exist under selected Customer.");
                    }
                }
                else
                {
                    Flag1 = true;
                    //throw new UserFriendlyException("Ooppps! There is a problem!", "Installer CEC Accreditation Code Does Not Exist in List.");
                }
                //}
            }
            if (Flag1 == true && Flag2 == false)
            {
                throw new UserFriendlyException("Ooppps! There is a problem!", "Installer CEC Accreditation Code Does Not Exist in List.");
            }
            if (Flag1 == true && Flag2 == true)
            {
                throw new UserFriendlyException("Ooppps! There is a problem!", "Installer already exist under selected Customer.");
            }
            if (Flag2)
            {
                throw new UserFriendlyException("Ooppps! There is a problem!", "Installer already exist under selected Customer.");
            }
            //throw new NotImplementedException();
        }
        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Tenant_Installer_Delete)]
        public async Task DeleteInstaller(EntityDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var installerEmployee = _InstallerEmployeRepository
                   .GetAll()
                   .WhereIf(
                       input.Id != 0,
                       p => p.InstallerId == input.Id
                   )
                   .ToList();

                for (int i = 0; i < installerEmployee.Count(); i++)
                {
                    await _InstallerEmployeRepository.DeleteAsync(installerEmployee[i].Id);
                }
				var inst = _InstallerRepository.GetAll().Where(I => I.Id == input.Id).FirstOrDefault();
				if (inst.Logo != null)
				{
					var tempLogoPath = Path.Combine(_appFolders.TempFileDownloadFolder, inst.Logo);
					FileHelper.DeleteIfExists(tempLogoPath);
				}
				if (inst.LicencePhoto != null)
				{
					var tempLogoPath = Path.Combine(_appFolders.TempFileDownloadFolder, inst.LicencePhoto);
					FileHelper.DeleteIfExists(tempLogoPath);
				}
				if (inst.AccreditationAccPhoto != null)
				{
					var tempLogoPath = Path.Combine(_appFolders.TempFileDownloadFolder, inst.AccreditationAccPhoto);
					FileHelper.DeleteIfExists(tempLogoPath);
				}
				await _InstallerRepository.DeleteAsync(input.Id);
            }
        }

        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Tenant_Installer_Edit)]
        public async Task<InstallerEditDto> GetInstallerForEdit(EntityDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var installer = (await _InstallerRepository.GetAsync(input.Id)).MapTo<InstallerEditDto>();
                return installer;
            }
        }

        [UnitOfWork]
        public async Task<InstallerDetailDto> GetInstallerForDetail(EntityDto input)
        {
            var installer = new InstallerDetailDto();
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                installer = (await _InstallerRepository.GetAsync(input.Id)).MapTo<InstallerDetailDto>();
                installer.AddressTypeName = _addressTypeRepository.GetAll().Where(p => p.Id == installer.AddressType).Select(p => p.PostalAddressTypeName).FirstOrDefault();
                installer.PostalDelTypeName = _delevaryTypeRepository.GetAll().Where(p => p.Id == installer.PostalDelType).Select(p => p.PostalDeliveryTypeName).FirstOrDefault();
            }
            return installer;
        }

        public async Task<InstallerEditDto> GetInstallerNameForEmp(EntityDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var installer = (await _InstallerRepository.GetAsync(input.Id)).MapTo<InstallerEditDto>();
                return installer;
            }
        }
        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Tenant_Installer_Edit)]
        public async Task UpdateInstaller(InstallerEditDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                try
                {
                    var installerEmployee = _InstallerEmployeRepository
                .GetAll()
                .WhereIf(
                    input.Id != 0,
                    p => p.InstallerId == input.Id
                )
                .FirstOrDefault();
                    installerEmployee.FullName = input.FullName;
                    installerEmployee.Phone = input.Phone;
                    installerEmployee.Mobile = input.Mobile;
                    installerEmployee.EmailId = input.EmailId;
                    installerEmployee.Fax = input.Fax;
                    installerEmployee.AddressType = input.AddressType;
                    installerEmployee.PostalDelNo = input.PostalDelNo;
                    installerEmployee.PostalDelType = input.PostalDelType;
                    installerEmployee.StreetAddress = input.StreetAddress;
                    installerEmployee.UnitNo = input.UnitNo;
                    installerEmployee.UnitType = input.UnitType;
                    installerEmployee.StreetNumber = input.StreetNumber;
                    installerEmployee.StreetName = input.StreetName;
                    installerEmployee.StreetType = input.StreetType;
                    installerEmployee.Suburb = input.Suburb;
                    installerEmployee.Suburb_Id = input.Suburb_Id;
                    installerEmployee.State = input.State;
                    installerEmployee.PostCode = input.PostCode;

                    installerEmployee.IsInst = input.IsInst;
                    installerEmployee.IsElec = input.IsElec;
                    installerEmployee.IsDesc = input.IsDesc;
                    installerEmployee.InstallerExpiry = input.InstallerExpiry;
                    installerEmployee.InsAccreExDate = input.InsAccreExDate;
                    installerEmployee.ElectricalExpiry = input.ElectricalExpiry;
                    installerEmployee.EleLicenceExDate = input.EleLicenceExDate;
                    installerEmployee.DesignerExpiry = input.DesignerExpiry;
                    installerEmployee.DesignerAccreExDate = input.DesignerAccreExDate;
                    await _InstallerEmployeRepository.UpdateAsync(installerEmployee);

                    var installer = await _InstallerRepository.GetAsync(input.Id);

                    if (input.Logo != installer.Logo)
                    {
                        var tempLogoPath = Path.Combine(_appFolders.TempFileDownloadFolder, installer.Logo);
                        FileHelper.DeleteIfExists(tempLogoPath);
                    }
                    if (input.AccreditationAccPhoto != installer.AccreditationAccPhoto)
                    {
                        var tempAccreditationAccPhotoPath = Path.Combine(_appFolders.TempFileDownloadFolder, installer.AccreditationAccPhoto);
                        FileHelper.DeleteIfExists(tempAccreditationAccPhotoPath);
                    }
                    if (input.LicencePhoto != installer.LicencePhoto)
                    {
                        var tempLicencePhotoPath = Path.Combine(_appFolders.TempFileDownloadFolder, installer.LicencePhoto);
                        FileHelper.DeleteIfExists(tempLicencePhotoPath);
                    }


                    installer.FullName = input.FullName;
                    installer.Phone = input.Phone;
                    installer.Mobile = input.Mobile;
                    installer.EmailId = input.EmailId;
                    installer.CompanyName = input.CompanyName;
                    installer.ABN = input.ABN;
                    installer.CompanyPhone = input.CompanyPhone;
                    installer.Fax = input.Fax;
                    installer.AddressType = input.AddressType;
                    installer.PostalDelNo = input.PostalDelNo;
                    installer.PostalDelType = input.PostalDelType;
                    installer.IsGST = input.IsGST;
                    installer.Logo = input.Logo;
                    installer.StreetAddress = input.StreetAddress;
                    installer.UnitNo = input.UnitNo;
                    installer.UnitType = input.UnitType;
                    installer.StreetNumber = input.StreetNumber;
                    installer.StreetName = input.StreetName;
                    installer.StreetType = input.StreetType;
                    installer.Suburb = input.Suburb;
                    installer.Suburb_Id = input.Suburb_Id;
                    installer.State = input.State;
                    installer.PostCode = input.PostCode;
                    installer.AccreditationAccPhoto = input.AccreditationAccPhoto;
                    installer.LicencePhoto = input.LicencePhoto;

                    installer.IsInst = input.IsInst;
                    installer.InstallerExpiry = input.InstallerExpiry;
                    installer.InsAccreExDate = input.InsAccreExDate;
                    installer.IsElec = input.IsElec;
                    installer.ElectricalExpiry = input.ElectricalExpiry;
                    installer.EleLicenceExDate = input.EleLicenceExDate;
                    installer.IsDesc = input.IsDesc;
                    installer.DesignerExpiry = input.DesignerExpiry;
                    installer.DesignerAccreExDate = input.DesignerAccreExDate;

                    await _InstallerRepository.UpdateAsync(installer);
                }
                catch (Exception e)
                {
                    throw new UserFriendlyException("Ooppps! There is a problem!", Convert.ToString(e.Message));
                }

            }
        }

        [UnitOfWork]
        public ListResultDto<InstallerEmployeeListDto> GetInstallerEmployee(GetInstallerEmployeeInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var installerEmployee = _InstallerEmployeRepository
                .GetAll()
                .WhereIf(
                    input.InstallerId != 0,
                    p => p.InstallerId == input.InstallerId
                )
                .AsQueryable();
                return new ListResultDto<InstallerEmployeeListDto>(installerEmployee.MapTo<List<InstallerEmployeeListDto>>());
                //var source = LocalizationManager.GetSource(input.SourceName);
                //var languageTexts = installerEmployee
                //    .GetAllStrings()
                //    .Select(localizedString => new InstallerEmployeeListDto
                //    {
                //        InstallerId = localizedString.InstallerId,
                //        FullName = localizedString.FullName,
                //        Mobile = localizedString.Mobile,
                //        Phone = localizedString.Phone
                //    })
                //    .AsQueryable();
                //var totalCount = installerEmployee.Count();
                //return new PagedResultDto<InstallerEmployeeListDto>(
                //    totalCount,
                //    installerEmployee.ToList()
                //    );
            }
        }
        [UnitOfWork]
        public async Task CreateInstallerEmployee(CreateInstallerEmployeeDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var InstallerEmployee = new InstallerEmploye();
                InstallerEmployee.FullName = input.FullName;
                InstallerEmployee.Phone = input.Phone;
                InstallerEmployee.Mobile = input.Mobile;
                InstallerEmployee.EmailId = input.EmailId;
                InstallerEmployee.Fax = input.Fax;
                InstallerEmployee.StreetAddress = input.StreetAddress;
                InstallerEmployee.UnitNo = input.UnitNo;
                InstallerEmployee.UnitType = input.UnitType;
                InstallerEmployee.StreetNumber = input.StreetNumber;
                InstallerEmployee.StreetName = input.StreetName;
                InstallerEmployee.StreetType = input.StreetType;
                InstallerEmployee.Suburb = input.Suburb;
                InstallerEmployee.Suburb_Id = input.Suburb_Id;
                InstallerEmployee.State = input.State;
                InstallerEmployee.PostCode = input.PostCode;
                InstallerEmployee.InstallerExpiry = input.InstallerExpiry;
                InstallerEmployee.InsAccreExDate = input.InsAccreExDate;
                InstallerEmployee.ElectricalExpiry = input.ElectricalExpiry;
                InstallerEmployee.EleLicenceExDate = input.EleLicenceExDate;
                InstallerEmployee.DesignerExpiry = input.DesignerExpiry;
                InstallerEmployee.DesignerAccreExDate = input.DesignerAccreExDate;
                InstallerEmployee.InstallerId = input.InstallerId;
                InstallerEmployee.AddressType = input.AddressType;
                InstallerEmployee.PostalDelNo = input.PostalDelNo;
                InstallerEmployee.PostalDelType = input.PostalDelType;
                InstallerEmployee.IsInst = input.IsInst;
                InstallerEmployee.IsElec = input.IsElec;
                InstallerEmployee.IsDesc = input.IsDesc;
                InstallerEmployee.TenantId = (int)_session.TenantId;
                InstallerEmployee.CreatorUserId = (int)_session.UserId;
                await _InstallerEmployeRepository.InsertAsync(InstallerEmployee);
            }
        }
        [UnitOfWork]
        public async Task DeleteInstallerEmployee(EntityDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                await _InstallerEmployeRepository.DeleteAsync(input.Id);
            }
        }
        [UnitOfWork]
        public async Task UpdateInstallerEmployee(InstallerEmployeeEditDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var installerEmployee = await _InstallerEmployeRepository.GetAsync(input.Id);

                installerEmployee.FullName = input.FullName;
                installerEmployee.Phone = input.Phone;
                installerEmployee.Mobile = input.Mobile;
                installerEmployee.EmailId = input.EmailId;
                installerEmployee.Fax = input.Fax;
                installerEmployee.AddressType = input.AddressType;
                installerEmployee.PostalDelNo = input.PostalDelNo;
                installerEmployee.PostalDelType = input.PostalDelType;
                installerEmployee.StreetAddress = input.StreetAddress;
                installerEmployee.UnitNo = input.UnitNo;
                installerEmployee.UnitType = input.UnitType;
                installerEmployee.StreetNumber = input.StreetNumber;
                installerEmployee.StreetName = input.StreetName;
                installerEmployee.StreetType = input.StreetType;
                installerEmployee.Suburb = input.Suburb;
                installerEmployee.Suburb_Id = input.Suburb_Id;
                installerEmployee.State = input.State;
                installerEmployee.PostCode = input.PostCode;
                await _InstallerEmployeRepository.UpdateAsync(installerEmployee);
            }
        }
        [UnitOfWork]
        public async Task<InstallerEmployeeEditDto> GetInstallerEmployeeForEdit(EntityDto input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var installerEmployee = new InstallerEmployeeEditDto();
                //var installerEmployee = (await _InstallerEmployeRepository.GetAsync(input.Id)).MapTo<InstallerEmployeeEditDto>();
                //installerEmployee 
                installerEmployee = (await _InstallerEmployeRepository.GetAsync(input.Id)).MapTo<InstallerEmployeeEditDto>();
                installerEmployee.IsApproved = _InstallerRepository.GetAll().Where(I => I.Id == installerEmployee.InstallerId).Select(IN => IN.IsApproved).FirstOrDefault();
                return installerEmployee;
            }
        }

        [UnitOfWork]
        public int CheckInstallerCode(string InstCode)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var instCodeCount = _InstallerEmployeRepository
               .GetAll()
               .WhereIf(!InstCode.IsNullOrEmpty(), p => p.InstallerExpiry == InstCode)
               .Count();

                return instCodeCount;
            }
        }

        [UnitOfWork]
        public int CheckDesignerCode(string DesignerCode)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var designarCodeCount = _InstallerEmployeRepository
               .GetAll()
               .WhereIf(!DesignerCode.IsNullOrEmpty(), p => p.DesignerExpiry == DesignerCode)
               .Count();

                return designarCodeCount;
            }
        }

        [UnitOfWork]
        public int CheckElecCode(string ElecCode)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var elecCodeCount = _InstallerEmployeRepository
               .GetAll()
               .WhereIf(!ElecCode.IsNullOrEmpty(), p => p.ElectricalExpiry == ElecCode)
               .Count();

                return elecCodeCount;
            }
        }

        [UnitOfWork]
        public int CheckInstallerCodeEdit(string InstCode, int Id)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var instCodeCount = _InstallerEmployeRepository
               .GetAll()
               .WhereIf(!InstCode.IsNullOrEmpty(), p => p.InstallerExpiry == InstCode && p.InstallerId != Id)
               .Count();

                return instCodeCount;
            }
        }

        [UnitOfWork]
        public int CheckDesignerCodeEdit(string DesignerCode, int Id)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var designarCodeCount = _InstallerEmployeRepository
               .GetAll()
               .WhereIf(!DesignerCode.IsNullOrEmpty(), p => p.DesignerExpiry == DesignerCode && p.InstallerId != Id)
               .Count();

                return designarCodeCount;
            }
        }

        [UnitOfWork]
        public int CheckElecCodeEdit(string ElecCode, int Id)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var elecCodeCount = _InstallerEmployeRepository
               .GetAll()
               .WhereIf(!ElecCode.IsNullOrEmpty(), p => p.ElectricalExpiry == ElecCode && p.InstallerId != Id)
               .Count();

                return elecCodeCount;
            }
        }

        [UnitOfWork]
        public ListResultDto<InstPostalAddressTypeListDto> GetPostalAddressType()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var addresstype = _addressTypeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

                return new ListResultDto<InstPostalAddressTypeListDto>(addresstype.MapTo<List<InstPostalAddressTypeListDto>>());
            }
        }
        [UnitOfWork]
        public ListResultDto<InstPostalDeliveryTypeListDto> GetPostalDeliveryType()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var delevarytype = _delevaryTypeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

                return new ListResultDto<InstPostalDeliveryTypeListDto>(delevarytype.MapTo<List<InstPostalDeliveryTypeListDto>>());
            }
        }
        [UnitOfWork]
        public ListResultDto<InstPostCodeListDto> GetPostCode()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var postcode = _postCodeRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

                return new ListResultDto<InstPostCodeListDto>(postcode.MapTo<List<InstPostCodeListDto>>());
            }
        }
        [UnitOfWork]
        public ListResultDto<InstStateListDto> GetState()
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var states = _stateRepository
               .GetAll()
               .OrderByDescending(p => p.Id)
               .ToList();

                return new ListResultDto<InstStateListDto>(states.MapTo<List<InstStateListDto>>());
            }
        }
        [UnitOfWork]
        //public ListResultDto<JobStreetNamesListDto> GetStreetNames()
        public List<typeAhead> GetStreetNames(TypeAheadInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                if (input.Text != null && input.Text != "")
                {
                    var streetName = (from data in _streetNameRepository
                .GetAll()
                .Where(e => e.StreetName.Contains(input.Text))
                .Take(20)
                                      select new typeAhead
                                      {
                                          id = data.Id.ToString(),
                                          name = data.StreetName
                                      }).ToList();

                    return streetName; // new ListResultDto<JobStreetNamesListDto>(streetName.MapTo<List<JobStreetNamesListDto>>());
                }
                else
                {
                    var streetName = (from data in _streetNameRepository
                .GetAll()
                                      select new typeAhead
                                      {
                                          id = data.Id.ToString(),
                                          name = data.StreetName
                                      }).ToList();

                    return streetName;
                }
            }

        }

        [UnitOfWork]
        //public ListResultDto<JobStreetTypeListDto> GetStreetType()
        public List<typeAhead> GetStreetType(TypeAheadInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                if (input.Text != null && input.Text != "")
                {
                    var streetType = (from data in _streetTypeRepository
                .GetAll()
                .Where(e => e.StreetTypeName.Contains(input.Text))
                .Take(20)
                                      select new typeAhead
                                      {
                                          id = data.Id.ToString(),
                                          name = data.StreetTypeName
                                      }).ToList();

                    return streetType;
                }
                else
                {
                    var streetType = (from data in _streetTypeRepository
                .GetAll()
                                      select new typeAhead
                                      {
                                          id = data.Id.ToString(),
                                          name = data.StreetTypeName
                                      }).ToList();

                    return streetType;
                }
            }
        }

        [UnitOfWork]
        //public ListResultDto<JobUnitTypeListDto> GetUnitType()
        public List<typeAhead> GetUnitType(TypeAheadInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                if (input.Text != null && input.Text != "")
                {
                    var unitType = (from data in _unitTypeRepository
                .GetAll()
                .Where(e => e.UnitTypeName.Contains(input.Text))
                .Take(20)
                                    select new typeAhead
                                    {
                                        id = data.Id.ToString(),
                                        name = data.UnitTypeName
                                    }).ToList();

                    return unitType;
                }
                else
                {
                    var unitType = (from data in _unitTypeRepository
                .GetAll()
                                    select new typeAhead
                                    {
                                        id = data.Id.ToString(),
                                        name = data.UnitTypeName
                                    }).ToList();

                    return unitType;
                }
            }
        }

        [UnitOfWork]
        //public ListResultDto<PostCodesListDto> GetAllPostCodes(string postCodeInput)
        public List<typeAhead> GetAllPostCodes(TypeAheadInput input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                if (input.Text != null && input.Text != "")
                {
                    var postcodes = _postCodeRepository
                .GetAll()
                .Where(e => e.Suburb.Contains(input.Text))
                .Take(20)
                .ToList();

                    var suburblist = (from Inst in postcodes
                                      join In in _stateRepository.GetAll() on Inst.StateId equals In.Id into InJoin
                                      from In in InJoin.DefaultIfEmpty()
                                      select new typeAhead
                                      {
                                          id = Inst.Id.ToString(),
                                          name = Inst.Suburb + " | " + In.StateName + " | " + Inst.Postcode
                                      }).ToList();

                    return suburblist;
                }
                else
                {
                    var postcodes = _postCodeRepository
                .GetAll()
                .ToList();

                    var suburblist = (from Inst in postcodes
                                      join In in _stateRepository.GetAll() on Inst.StateId equals In.Id into InJoin
                                      from In in InJoin.DefaultIfEmpty()
                                      select new typeAhead
                                      {
                                          id = Inst.Id.ToString(),
                                          name = Inst.Suburb + " | " + In.StateName + " | " + Inst.Postcode
                                      }).ToList();

                    return suburblist;
                    //return null;
                }
            }
        }
        [UnitOfWork]
        public ListResultDto<InstPostCodeListDto> GetPostCodesListBySuburb(EntityDto Input)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var ModelNo = _postCodeRepository
                .GetAll()
                .Where(p => p.Id == Input.Id)
                .ToList();

                var PostCodes = ModelNo.MapTo<List<InstPostCodeListDto>>();
                foreach (var item in PostCodes)
                {
                    item.stateName = (from datastate in _stateRepository.GetAll() where datastate.Id == item.StateId select datastate.StateName).FirstOrDefault();
                }
                return new ListResultDto<InstPostCodeListDto>(PostCodes);
            }
        }

        public ListResultDto<InstEmployeListDto> GetInstallerEmployeInstallerByInstallerId(EntityDto input)
        {
            var installerEmp = _InstallerEmployeRepository
                .GetAll()
                .WhereIf(input.Id != 0, p => p.InstallerId == input.Id && p.IsInst == true)
                .ToList();

            return new ListResultDto<InstEmployeListDto>(installerEmp.MapTo<List<InstEmployeListDto>>());
        }

        public ListResultDto<InstEmployeListDto> GetInstallerEmployeDesignerByInstallerId(EntityDto input)
        {
            var installerEmp = _InstallerEmployeRepository
                .GetAll()
                .WhereIf(input.Id != 0, p => p.InstallerId == input.Id && p.IsDesc == true)
                .ToList();

            return new ListResultDto<InstEmployeListDto>(installerEmp.MapTo<List<InstEmployeListDto>>());

        }

        public ListResultDto<InstEmployeListDto> GetInstallerEmployeElectricianByInstallerId(EntityDto input)
        {
            var installerEmp = _InstallerEmployeRepository
                .GetAll()
                .WhereIf(input.Id != 0, p => p.InstallerId == input.Id && p.IsElec == true)
                .ToList();

            return new ListResultDto<InstEmployeListDto>(installerEmp.MapTo<List<InstEmployeListDto>>());

        }
        //[UnitOfWork]
        public ListResultDto<InstUserListDto> GetUsers()
        {
            //using (_unitOfWorkManager.Current.SetTenantId(null))
            //{
            var User_List = _userRepository
           .GetAll()
           .OrderByDescending(p => p.Id)
           .ToList();

            var UserList = (from user in User_List
                            join ur in _userroleRepository.GetAll() on user.Id equals ur.UserId into urJoined
                            from ur in urJoined.DefaultIfEmpty()
                            join us in _roleRepository.GetAll() on ur.RoleId equals us.Id into usJoined
                            from us in usJoined.DefaultIfEmpty()
                            where (us != null && us.DisplayName == "Installer")
                            group user by user into userGrouped
                            select userGrouped.Key);

            return new ListResultDto<InstUserListDto>(UserList.MapTo<List<InstUserListDto>>());
            //}
        }

        [UnitOfWork]
        public ListResultDto<InstSuburbListDto> AutoCompleteSuburb(string element)
        {
            using (_unitOfWorkManager.Current.SetTenantId(null))
            {
                var suburb = _postCodeRepository
               .GetAll()
               .Where(P=>P.Suburb.ToLower().Contains(element.ToLower()))
               .ToList();

                //return new ListResultDto<InstSuburbListDto>(suburb.MapTo<List<InstSuburbListDto>>());

                var postcodelist = suburb.MapTo<List<InstSuburbListDto>>();
                foreach (var item in postcodelist)
                {
                    var stateName = _stateRepository.GetAll().Where(p => p.Id == item.StateId).Select(p => p.StateName).FirstOrDefault();
                    item.SuburbstatePostcodes = item.Suburb + " | " + stateName + " | " + item.Postcode;
                }
                return new ListResultDto<InstSuburbListDto>(postcodelist);
            }
        }
    }
}
