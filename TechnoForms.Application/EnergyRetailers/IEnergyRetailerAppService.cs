﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.EnergyRetailers.Dto;

namespace TechnoForms.EnergyRetailers
{
	public interface IEnergyRetailerAppService : IApplicationService
	{
		Task<PagedResultDto<EnergyRetailerListDto>> GetEnergyRetailer(GetEnergyRetailerInput input);
		Task CreateEnergyRetailer(CreateEnergyRetailerDto input);
		Task DeleteEnergyRetailer(EntityDto input);
		Task UpdateEnergyRetailer(EditEnergyRetailerDto input);
		Task<EditEnergyRetailerDto> GetEnergyRetailerForEdit(EntityDto input);
	}
}
