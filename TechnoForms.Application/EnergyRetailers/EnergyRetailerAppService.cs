﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using Abp.Linq.Extensions;
using System.Linq.Dynamic;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Energy;
using TechnoForms.EnergyRetailers.Dto;
using System.Data.Entity;
using Abp.Extensions;
using Abp.Runtime.Session;
using Abp.Authorization;
using TechnoForms.Authorization;

namespace TechnoForms.EnergyRetailers
{
	[AbpAuthorize(AppPermissions.Pages_Tenant_EnergyRetailers)]
	public class EnergyRetailerAppService : TechnoFormsApplicationModule, IEnergyRetailerAppService
	{
		private readonly IRepository<EnergyRetailer> _energyRetailerRepository;
		private readonly IAbpSession _session;

		public EnergyRetailerAppService(IRepository<EnergyRetailer> energyRetailerRepository
			, IAbpSession session)
		{
			_energyRetailerRepository = energyRetailerRepository;
			_session = session;
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_EnergyRetailers_Create)]
		public async Task CreateEnergyRetailer(CreateEnergyRetailerDto input)
		{
			var EnergyRetailers = input.MapTo<EnergyRetailer>();
			EnergyRetailers.TenantId = (int)_session.TenantId;
			await _energyRetailerRepository.InsertAsync(EnergyRetailers);
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_EnergyRetailers_Delete)]
		public async Task DeleteEnergyRetailer(EntityDto input)
		{
			await _energyRetailerRepository.DeleteAsync(input.Id);
		}

		public async Task<PagedResultDto<EnergyRetailerListDto>> GetEnergyRetailer(GetEnergyRetailerInput input)
		{
			var unittype = _energyRetailerRepository
				.GetAll()
				.WhereIf(
					!input.Name.IsNullOrEmpty(),
					p => p.Name.ToLower().Contains(input.Name.ToLower())
				);

			var resultCount = await unittype.CountAsync();
			var results = await unittype
				.AsNoTracking()
				.OrderBy(input.Sorting)
				.PageBy(input)
				.ToListAsync();

			return new PagedResultDto<EnergyRetailerListDto>(resultCount, results.MapTo<List<EnergyRetailerListDto>>());
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_EnergyRetailers_Edit)]
		public async Task<EditEnergyRetailerDto> GetEnergyRetailerForEdit(EntityDto input)
		{
			var EnergyRetailers = (await _energyRetailerRepository.GetAsync(input.Id)).MapTo<EditEnergyRetailerDto>(); ;
			return EnergyRetailers;
		}

		[AbpAuthorize(AppPermissions.Pages_Tenant_EnergyRetailers_Edit)]
		public async Task UpdateEnergyRetailer(EditEnergyRetailerDto input)
		{
			var EnergyRetailers = await _energyRetailerRepository.GetAsync(input.Id);
			EnergyRetailers.Name = input.Name;
			EnergyRetailers.NSW = input.NSW;
			EnergyRetailers.SA = input.SA;
			EnergyRetailers.QLD = input.QLD;
			EnergyRetailers.VIC = input.VIC;
			EnergyRetailers.WA = input.WA;
			EnergyRetailers.ACT = input.ACT;
			EnergyRetailers.TAS = input.TAS;
			EnergyRetailers.NT = input.NT;
			EnergyRetailers.ElectricityProviderId = input.ElectricityProviderId;
			await _energyRetailerRepository.UpdateAsync(EnergyRetailers);
		}
	}
}
