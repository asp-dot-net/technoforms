﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.AdminDashboard.Dto;

namespace TechnoForms.AdminDashboard
{
	public interface IAdminDashboardAppService : IApplicationService
	{
		AdminDashboardDto GetAdminDashboardCount();

		Task<ListResultDto<AdminDashboardJobListDto>> GetJobList();

		Task<List<AdminDashboardJobList>> GetDashboardJobList();

		int PanelCount(Guid guid);
		int InverterCount(Guid guid);
		int PhotoCount(Guid guid);
		//Task GetTest();
	}
}
