﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.AdminDashboard.Dto
{
    public class AdminDashboardDto
    {
        public int? TotalInProcress { get; set; }

        public int? TotalOnHold { get; set; }

        public int? TotalCompleted { get; set; }

        public int? TotalJob { get; set; }

        public decimal? TotalSTC { get; set; }

        public decimal? FailedSTC { get; set; }

        public decimal? ApprovedSTC { get; set; }

        public decimal? PendingSTC { get; set; }
    }
}
