﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs;

namespace TechnoForms.AdminDashboard.Dto
{
    [AutoMapFrom(typeof(Job))]
    public class AdminDashboardJobListDto
    {
        public virtual string FirstName { get; set; }

        public virtual string LastName { get; set; }

        public virtual DateTime ExpectedInstallDate { get; set; }

        public virtual string ExpectedInstallShortDate { get; set; }

        public virtual string StreetAddress { get; set; }

        public virtual string StreetCity { get; set; }

        public virtual int? Suburb_Id { get; set; }

        public virtual string StreetState { get; set; }

        public virtual string StreetPostCode { get; set; }

        public virtual Guid Guid { get; set; }

        public virtual string ProjectNumber { get; set; }

        public string ScanedPanelClass { get; set; }
        public string ScanedInverterClass { get; set; }
        public string OtherImagesClass { get; set; }
        public string CustomerSignatureClass { get; set; }
        public string InstallerSignatureClass { get; set; }

        public string SignatureImageOwner { get; set; }
        public string OwnerPhoto { get; set; }
        public string SignatureImageInst { get; set; }
        public string InstallerPhoto { get; set; }

        public Nullable<bool> IsVerifyCS { get; set; }
        public Nullable<bool> IsVerifyIN { get; set; }
        public Nullable<bool> IsVerifyPA { get; set; }
        public Nullable<bool> IsVerifyIS { get; set; }
        public Nullable<bool> IsVerifyImages { get; set; }
    }
}
