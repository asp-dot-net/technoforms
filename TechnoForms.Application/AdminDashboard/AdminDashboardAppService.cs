﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.Linq.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.AdminDashboard.Dto;
using TechnoForms.Dto;
using TechnoForms.Jobs;
using TechnoForms.MultiTenancy;
using TechnoForms.Photos;
using TechnoForms.ScanedInverters;
using TechnoForms.ScanedPanels;

namespace TechnoForms.AdminDashboard
{
    public class AdminDashboardAppService : TechnoFormsAppServiceBase, IAdminDashboardAppService
    {
        private readonly IRepository<Job> _jobRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<ScanedPanel> _scanedPanelRepository;
        private readonly IRepository<ScanedInverter> _scanedInverterRepository;
        private readonly IRepository<Photo> _photoRepository;
        private readonly IAppFolders _appFolders;
        private readonly IAdminDashboardRepository _adminDashboardRepository;


        public AdminDashboardAppService(IRepository<Job> jobRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Tenant> tenantRepository,
            IRepository<ScanedPanel> scanedPanelRepository,
            IRepository<ScanedInverter> scanedInverterRepository,
            IRepository<Photo> photoRepository,
            IAppFolders appFolders
            ,
            IAdminDashboardRepository adminDashboardRepository)
        {
            _jobRepository = jobRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _tenantRepository = tenantRepository;
            _scanedPanelRepository = scanedPanelRepository;
            _scanedInverterRepository = scanedInverterRepository;
            _photoRepository = photoRepository;
            _appFolders = appFolders;
            _adminDashboardRepository = adminDashboardRepository;
        }


        public AdminDashboardDto GetAdminDashboardCount()
        {
            int? TenantId = AbpSession.TenantId;
            //var result = new AdminDashboardDto();
            //var tenant = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            //for (int i = 0; i < tenant.Count; i++)
            using (_unitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var resultData = new AdminDashboardDto();
                resultData.TotalCompleted = _jobRepository.GetAll().Where(J => J.JobStatus_Id == 2).Count();
                resultData.TotalInProcress = _jobRepository.GetAll().Where(J => J.JobStatus_Id == 1).Count();
                resultData.TotalOnHold = _jobRepository.GetAll().Where(J => J.JobStatus_Id == 3).Count();
                resultData.TotalJob = _jobRepository.GetAll().Count();
                resultData.TotalSTC = _jobRepository.GetAll().Select(J => J.TotalSTC).Count() == 0 ? 0 : _jobRepository.GetAll().Select(J => J.TotalSTC).Sum();
                resultData.ApprovedSTC = _jobRepository.GetAll().Where(J => J.PVDStatus == "Approved").Select(J => J.TotalSTC).Count() == 0 ? 0 : _jobRepository.GetAll().Where(J => J.PVDStatus == "Approved").Select(J => J.TotalSTC).Sum();
                resultData.PendingSTC = _jobRepository.GetAll().Where(J => J.PVDStatus == "Pending" || J.PVDStatus == null).Select(J => J.TotalSTC).Count() == 0 ? 0 : _jobRepository.GetAll().Where(J => J.PVDStatus == "Pending" || J.PVDStatus == null).Select(J => J.TotalSTC).Sum();
                resultData.FailedSTC = _jobRepository.GetAll().Where(J => J.PVDStatus == "Failed").Select(J => J.TotalSTC).Count() == 0 ? 0 : _jobRepository.GetAll().Where(J => J.PVDStatus == "Failed").Select(J => J.TotalSTC).Sum();

                //    result.TotalCompleted = Convert.ToInt32(result.TotalCompleted) + Convert.ToInt32(resultData.TotalCompleted);
                //    result.TotalInProcress = Convert.ToInt32(result.TotalInProcress) + Convert.ToInt32(resultData.TotalInProcress);
                //    result.TotalOnHold = Convert.ToInt32(result.TotalOnHold) + Convert.ToInt32(resultData.TotalOnHold);
                //    result.TotalJob = Convert.ToInt32(result.TotalJob) + Convert.ToInt32(resultData.TotalJob);
                //    result.TotalSTC = Convert.ToInt32(result.TotalSTC) + Convert.ToInt32(resultData.TotalSTC);
                //    result.ApprovedSTC = Convert.ToInt32(result.ApprovedSTC) + Convert.ToInt32(resultData.ApprovedSTC);
                //    result.PendingSTC = Convert.ToInt32(result.PendingSTC) + Convert.ToInt32(resultData.PendingSTC);
                //    result.FailedSTC = Convert.ToInt32(result.FailedSTC) + Convert.ToInt32(resultData.FailedSTC);
                return resultData;
            }
            //return result;

        }

        public async Task<ListResultDto<AdminDashboardJobListDto>> GetJobList()
        {
            int? TenantId = AbpSession.TenantId;
            using (_unitOfWorkManager.Current.SetTenantId(TenantId))
            {
                //var listdata = new List<AdminDashboardJobListDto>();

                //try
                //{
                //var video = _jobRepository
                //.GetAll()
                //.Where(
                //J => J.NoOfPanels != (_scanedPanelRepository.GetAll().Where(P => P.Guid == J.Guid).Select(p => p.Id).ToList().AsEnumerable().Count())
                ////|| J.NoOfInverter != (_scanedInverterRepository.GetAll().Where(I => I.Guid == J.Guid).AsEnumerable().Select(i => i.Id).Count())
                ////|| (_photoRepository.GetAll().Where(P => P.Guid == J.Guid).AsEnumerable().Select(p => p.Id).Count()) <= 0
                ////||
                ////|| J.SignatureImageOwner != null
                ////|| J.SignatureImageInst != null
                //)
                //.OrderByDescending(J => J.ExpectedInstallDate)
                //.Take(5)
                //.ToList()

                //;

                ////var results = await video
                ////    .ToListAsync();

                //listdata = video.MapTo<List<AdminDashboardJobListDto>>();

                var video = _jobRepository
            .GetAll();

                try
                {
                    foreach (var item in video)
                    {
                        var Panel = PanelCount(item.Guid);
                        var Inverter = InverterCount(item.Guid);
                        var Photo = PhotoCount(item.Guid);

                        //video = video.Where(J => (J.NoOfInverter != inverter) || (J.NoOfPanels != panel) || (image == 0) || (J.SignatureImageInst == null) || (J.SignatureImageOwner == null));
                    }
                }
                catch (Exception e)
                {

                }
                var results = await video
                    .OrderByDescending(J => J.ExpectedInstallDate)
                    .Take(5)
                    .ToListAsync();

                var listdata = results.MapTo<List<AdminDashboardJobListDto>>();

                foreach (var item in listdata)
                {
                    if (item.ExpectedInstallDate != null)
                    {
                        item.ExpectedInstallShortDate = item.ExpectedInstallDate.ToString("dd-MM-yyyy");
                    }

                    item.ScanedPanelClass = "dcbox disabled";
                    if (_scanedPanelRepository.GetAll().Where(p => p.Guid == item.Guid && p.PanelBarcodePhoto != "NULL").ToList().Count > 0)
                    {
                        item.ScanedPanelClass = "dcbox";
                        if (!string.IsNullOrEmpty(item.IsVerifyPA.ToString()))
                        {
                            if ((bool)item.IsVerifyPA)
                            {
                                item.ScanedPanelClass = "dcbox verified";
                            }
                        }
                    }
                    item.ScanedInverterClass = "dcbox disabled";
                    if (_scanedInverterRepository.GetAll().Where(p => p.Guid == item.Guid && p.InverterBarcodePhoto != "NULL").ToList().Count > 0)
                    {
                        item.ScanedInverterClass = "dcbox";
                        if (!string.IsNullOrEmpty(item.IsVerifyIN.ToString()))
                        {
                            if ((bool)item.IsVerifyIN)
                            {
                                item.ScanedInverterClass = "dcbox verified";
                            }
                        }
                    }

                    item.OtherImagesClass = "dcbox disabled";
                    if ((_photoRepository.GetAll().Where(p => p.Guid == item.Guid).Count()) > 0)
                    {
                        item.OtherImagesClass = "dcbox";
                        if (!string.IsNullOrEmpty(item.IsVerifyImages.ToString()))
                        {
                            if ((bool)item.IsVerifyImages)
                            {
                                item.OtherImagesClass = "dcbox verified";
                            }
                        }
                    }

                    item.CustomerSignatureClass = "dcbox disabled";
                    if (!string.IsNullOrEmpty(item.OwnerPhoto) || !string.IsNullOrEmpty(item.SignatureImageOwner))
                    {
                        item.CustomerSignatureClass = "dcbox";
                        if (!string.IsNullOrEmpty(item.IsVerifyCS.ToString()))
                        {
                            if ((bool)item.IsVerifyCS)
                            {
                                item.CustomerSignatureClass = "dcbox verified";
                            }
                        }
                    }

                    item.InstallerSignatureClass = "dcbox disabled";
                    if (!string.IsNullOrEmpty(item.InstallerPhoto) || !string.IsNullOrEmpty(item.SignatureImageInst))
                    {
                        item.InstallerSignatureClass = "dcbox";
                        if (!string.IsNullOrEmpty(item.IsVerifyIS.ToString()))
                        {
                            if ((bool)item.IsVerifyIS)
                            {
                                item.InstallerSignatureClass = "dcbox verified";
                            }
                        }
                    }

                }
                //}
                //catch (Exception e)
                //{

                //}
                return new ListResultDto<AdminDashboardJobListDto>(listdata);

            }
        }

        public async Task<List<AdminDashboardJobList>> GetDashboardJobList()
        {
            int? TenantId = AbpSession.TenantId;
            using (_unitOfWorkManager.Current.SetTenantId(TenantId))
            {
                var deptlist = await _adminDashboardRepository.GetDashboardJobListAsync();
                return deptlist;
            }
        }

        public int PanelCount(Guid guid)
		{
            var Count = _scanedPanelRepository.GetAll().Where(p => p.Guid == guid).Select(e => e.Id).Count();

            return Count;
        }
        public int InverterCount(Guid guid)
        {
            var Count = _scanedInverterRepository.GetAll().Where(p => p.Guid == guid).Select(e => e.Id).Count();

            return Count;
        }
        public int PhotoCount(Guid guid)
        {
            var Count = _photoRepository.GetAll().Where(p => p.Guid == guid).Select(e => e.Id).Count();

            return Count;
        }
    }
}
