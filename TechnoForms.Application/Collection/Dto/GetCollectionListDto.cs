﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Collection.Dto
{
    public class GetCollectionListDto
    {
        public string CustomerName { get; set; }
        public string InvoiceNumber { get; set; }
        public string date { get; set; }
        public string InstrumentDetail { get; set; }
        public decimal amount { get; set; }
        public string ModeofpaymentName { get; set; }
        public int Id { get; set; }
    }
}
