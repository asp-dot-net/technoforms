﻿using Abp.Application.Services.Dto;
using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.Collection.Dto;
using TechnoForms.ModeOfPayments;
using TechnoForms.MultiTenancy;
using Abp.Linq.Extensions;using System.Linq.Dynamic;

namespace TechnoForms.Collection
{
    [AbpAuthorize(AppPermissions.Pages_Invoice_Collection)]
    public class CollectionAppService : TechnoFormsAppServiceBase, ICollectionAppService
    {
        private readonly IRepository<TechnoForms.Payments.Payment> _paymentRepository;
        private readonly IUnitOfWorkManager _unitOfWorkManager;
        private readonly IRepository<Tenant> _tenantRepository;
        private readonly IRepository<TechnoForms.Invoices.Invoice> _invoiceRepository;
        private readonly IRepository<ModeOfPayment> _modeOfPaymentRepository;
        public CollectionAppService(IRepository<TechnoForms.Payments.Payment> paymentRepository,
            IUnitOfWorkManager unitOfWorkManager,
            IRepository<Tenant> tenantRepository,
            IRepository<TechnoForms.Invoices.Invoice> invoiceRepository,
            IRepository<ModeOfPayment> modeOfPaymentRepository)
        {
            _paymentRepository = paymentRepository;
            _unitOfWorkManager = unitOfWorkManager;
            _tenantRepository = tenantRepository;
            _invoiceRepository = invoiceRepository;
            _modeOfPaymentRepository = modeOfPaymentRepository;
        }
        [UnitOfWork]
        [AbpAuthorize(AppPermissions.Pages_Invoice_Generate)]
        public async Task<PagedResultDto<GetCollectionListDto>> GetCollectionList(CollectionInputDto input)
        {
            var tenantlist = _tenantRepository.GetAll().OrderByDescending(p => p.Id).ToList();
            var tenantlist1 = new List<GetCollectionListDto>();

            var resultCount = 0;
            for (int i = 0; i < tenantlist.Count; i++)
            {
                using (_unitOfWorkManager.Current.SetTenantId(tenantlist[i].Id))
                {
                    try
                    {


                        var Payment_List = (from payment in _paymentRepository.GetAll()
                                            join tenant in _tenantRepository.GetAll() on payment.TenantId equals tenant.Id
                                            join Invoice in _invoiceRepository.GetAll() on payment.Invoice_ID equals Invoice.Id
                                            join modepayment in _modeOfPaymentRepository.GetAll() on payment.Modeofpayment_Id equals modepayment.Id
                                            select new GetCollectionListDto
                                            {
                                                CustomerName = tenant.FullName,
                                                date = payment.date,
                                                InvoiceNumber = Invoice.InvoiceNumber,
                                                InstrumentDetail = payment.InstrumentDetail,
                                                amount = payment.amount,
                                                ModeofpaymentName = modepayment.ModeOfPaymentName,
                                                Id = payment.Id
                                            });

                        resultCount = await Payment_List.CountAsync();

                        var results = await Payment_List
                   .AsNoTracking()
                   .OrderBy(input.Sorting)
                   .PageBy(input)
                   .ToListAsync();

                        //            var results = await Payment_List
                        //.AsNoTracking()
                        //.ToListAsync();

                        foreach (var Payment in results)
                        {
                            tenantlist1.Add(Payment);
                        }
                    }
                    catch (Exception e)
                    { }
                }
            }
            return new PagedResultDto<GetCollectionListDto>(resultCount, tenantlist1);
            // return new ListResultDto<GetCollectionListDto>(tenantlist1);
        }
    }
}

