﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Collection.Dto;

namespace TechnoForms.Collection
{
    public interface ICollectionAppService : IApplicationService
    {
        Task<PagedResultDto<GetCollectionListDto>> GetCollectionList(CollectionInputDto input);
    }
}
