﻿using Abp.Application.Services.Dto;

namespace TechnoForms.Caching.Dto
{
    public class CacheDto
    {
        public string Name { get; set; }
    }
}
