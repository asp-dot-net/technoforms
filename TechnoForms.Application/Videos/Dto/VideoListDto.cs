﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Videos.Dto
{
    [AutoMapFrom(typeof(video))]
    public class VideoListDto : FullAuditedEntityDto
    {
        public string Title { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
    }
}
