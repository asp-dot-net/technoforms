﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Videos.Dto
{
    [AutoMapTo(typeof(video))]
    public class CreateVideoDto
    {
        [Required]
        [MaxLength(video.MaxTitleLength)]
        public string Title { get; set; }

        [Required]
        public string FileName { get; set; }

    }
}
