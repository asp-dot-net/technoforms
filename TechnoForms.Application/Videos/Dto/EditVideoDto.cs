﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Videos.Dto
{
    [AutoMap(typeof(video))]
    public class EditVideoDto : FullAuditedEntityDto
    {
        [Required]
        [MaxLength(video.MaxTitleLength)]
        public string Title { get; set; }

        [Required]
        public string FileName { get; set; }
    }
}
