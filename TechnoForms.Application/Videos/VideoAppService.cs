﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Repositories;
using System.Threading.Tasks;
using TechnoForms.Authorization;
using TechnoForms.Videos.Dto;
using Abp.Extensions;
using Abp.Collections.Extensions;
using System.Data.Entity;using Abp.Linq.Extensions;using System.Linq.Dynamic;
using Abp.Application.Services.Dto;
using System.Collections.Generic;
using System;
using TechnoForms.Web;

namespace TechnoForms.Videos
{
    [AbpAuthorize(AppPermissions.Pages_Video, AppPermissions.Pages_MobileAppAccess)]
    public class VideoAppService : TechnoFormsApplicationModule, IVideoAppService
    {
        private readonly IRepository<video> _videoRepository;

        public VideoAppService(IRepository<video> videoRepository)
        {
            _videoRepository = videoRepository;
        }

        [AbpAuthorize(AppPermissions.Pages_Video_Create)]
        public async Task CreateVideo(CreateVideoDto input)
        {
            var video = input.MapTo<video>();
            await _videoRepository.InsertAsync(video);
        }
        public async Task<ListResultDto<VideoListDto>> GetVideos()
        {
            var video = await _videoRepository
                .GetAll().ToListAsync();

            var videolist = video.MapTo<List<VideoListDto>>();
            foreach (var item in videolist)
            {
                item.FilePath = WebUrlService.WebSiteRootAddress + "Temp/Video/" + item.FileName;
            }
            return new ListResultDto<VideoListDto>(videolist);
        }
        [AbpAuthorize(AppPermissions.Pages_Video_Edit)]
        public async Task<EditVideoDto> GetVideoForEdit(EntityDto input)
        {
            var video = (await _videoRepository.GetAsync(input.Id)).MapTo<EditVideoDto>();
            return video;
        }
        [AbpAuthorize(AppPermissions.Pages_Video_Edit)]
        public async Task UpdateVideo(EditVideoDto input)
        {

            var video = await _videoRepository.GetAsync(input.Id);

            if (!String.IsNullOrEmpty(input.FileName))
            {
                string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Video\\" + video.FileName;
                System.IO.File.Delete(filepath);
            }

            video.Title = input.Title;
            video.FileName = input.FileName;
            await _videoRepository.UpdateAsync(video);
        }

    }
}
