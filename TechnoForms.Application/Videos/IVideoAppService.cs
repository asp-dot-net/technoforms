﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Videos.Dto;

namespace TechnoForms.Videos
{
    public interface IVideoAppService : IApplicationService
    {
        Task<ListResultDto<VideoListDto>> GetVideos();
        Task CreateVideo(CreateVideoDto input);
        Task<EditVideoDto> GetVideoForEdit(EntityDto input);
        Task UpdateVideo(EditVideoDto input);
    }
}
