﻿using Abp.Dependency;

namespace TechnoForms
{
    public class AppFolders : IAppFolders, ISingletonDependency
    {
        public string TempFileDownloadFolder { get; set; }

        public string SampleProfileImagesFolder { get; set; }

        public string WebLogsFolder { get; set; }

        public string ComplienceDownloadFolder { get; set; }

        public string ReportDownloadFolder { get; set; }
        
    }
}