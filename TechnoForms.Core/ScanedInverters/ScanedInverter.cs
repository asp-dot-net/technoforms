﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.ScanedInverters
{
    [Table("ScanedInverter")]
    public class ScanedInverter : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxInverterSerialNumberLength = 50;
        public const int MaxInverterBarcodePhotoLength = 100;
        public const int MaxLongitudeLength = 50;
        public const int MaxLatitudeLength = 50;

        public int TenantId { get; set; }

        //[Required]
        [MaxLength(MaxInverterSerialNumberLength)]
        public virtual string InverterSerialNumber { get; set; }

        public virtual Guid Guid { get; set; }

        public virtual Boolean IsScan { get; set; }

        [Required]
        [MaxLength(MaxInverterBarcodePhotoLength)]
        public virtual string InverterBarcodePhoto { get; set; }

        [Required]
        [MaxLength(MaxLongitudeLength)]
        public virtual string Longitude { get; set; }

        [Required]
        [MaxLength(MaxLatitudeLength)]
        public virtual string Latitude { get; set; }
    }
}
