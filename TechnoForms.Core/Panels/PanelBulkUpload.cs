﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Panels
{
	[Table("PanelBulkUpload")]
	public class PanelBulkUpload : FullAuditedEntity
	{
		public const int MaxPanelFileName = 500;

		[Required]
		[MaxLength(MaxPanelFileName)]
		public virtual string PanelFileName { get; set; }
	}
}
