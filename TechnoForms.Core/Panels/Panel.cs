﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Panels
{
    [Table("Panel")]
    public class Panel : FullAuditedEntity
    {
        public const int MaxLicenceOrCertiHolder = 100;
        public const int MaxModelNo = 100;
        public const int MaxFireTested = 100;

        [Required]
        [MaxLength(MaxLicenceOrCertiHolder)]
        public virtual string LicenceOrCertiHolder { get; set; }

        [Required]
        [MaxLength(MaxModelNo)]
        public virtual string ModelNo { get; set; }

        [Required]
        public virtual DateTime CECApprovedDate { get; set; }

        [Required]
        public virtual DateTime ExpiryDate { get; set; }

        [Required]
        public virtual decimal Size { get; set; }

        [Required]
        [MaxLength(MaxFireTested)]
        public virtual string FireTested { get; set; }

        public virtual string PanelLogo { get; set; }

        public virtual string ResponsibleSupplier { get; set; }
        public virtual string ResponsibleSupplierABN { get; set; }

        public virtual string ResponsibleSupplier2 { get; set; }
        public virtual string ResponsibleSupplierABN2 { get; set; }

        public virtual string ResponsibleSupplier3 { get; set; }
        public virtual string ResponsibleSupplierABN3 { get; set; }

    }
}
