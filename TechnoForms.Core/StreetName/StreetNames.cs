﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.StreetName
{
    [Table("StreetName")]
    public class StreetNames : FullAuditedEntity
    {
        public const int MaxNameLength = 200;

        [Required]
        [MaxLength(MaxNameLength)]
        public virtual string StreetName { get; set; }
    }
}
