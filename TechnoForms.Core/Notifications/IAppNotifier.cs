﻿using System.Threading.Tasks;
using Abp;
using Abp.Notifications;
using TechnoForms.Authorization.Users;
using TechnoForms.Installers;
using TechnoForms.MultiTenancy;

namespace TechnoForms.Notifications
{
    public interface IAppNotifier
    {
        Task WelcomeToTheApplicationAsync(User user);

        Task NewUserRegisteredAsync(User user);

        Task NewTenantRegisteredAsync(Tenant tenant);

        Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info);

        Task WelcomeInstallerAsync(Installer installer);
    }
}
