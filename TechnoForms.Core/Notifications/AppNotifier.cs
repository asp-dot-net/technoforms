using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using Abp;
using Abp.Domain.Repositories;
using Abp.Localization;
using Abp.Notifications;
using TechnoForms.Authorization.Users;
using TechnoForms.Installers;
using TechnoForms.MultiTenancy;

namespace TechnoForms.Notifications
{
    public class AppNotifier : TechnoFormsDomainServiceBase, IAppNotifier
    {
        private readonly INotificationPublisher _notificationPublisher;
        private readonly IRepository<Tenant> _tenantRepository;

        public AppNotifier(INotificationPublisher notificationPublisher,
            IRepository<Tenant> tenantRepository)
        {
            _notificationPublisher = notificationPublisher;
            _tenantRepository = tenantRepository;
        }

        public async Task WelcomeToTheApplicationAsync(User user)
        {
            await _notificationPublisher.PublishAsync(
                AppNotificationNames.WelcomeToTheApplication,
                new MessageNotificationData(L("WelcomeToTheApplicationNotificationMessage")),
                severity: NotificationSeverity.Success,
                userIds: new[] { user.ToUserIdentifier() }
                );
        }

        public async Task NewUserRegisteredAsync(User user)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "NewUserRegisteredNotificationMessage",
                    TechnoFormsConsts.LocalizationSourceName
                    )
                );

            notificationData["userName"] = user.UserName;
            notificationData["emailAddress"] = user.EmailAddress;

            await _notificationPublisher.PublishAsync(AppNotificationNames.NewUserRegistered, notificationData, tenantIds: new[] { user.TenantId });
        }

        public async Task NewTenantRegisteredAsync(Tenant tenant)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "NewTenantRegisteredNotificationMessage",
                    TechnoFormsConsts.LocalizationSourceName
                    )
                );

            notificationData["tenancyName"] = tenant.TenancyName;
            await _notificationPublisher.PublishAsync(AppNotificationNames.NewTenantRegistered, notificationData);
        }

        //This is for test purposes
        public async Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info)
        {
            await _notificationPublisher.PublishAsync(
                "App.SimpleMessage",
                new MessageNotificationData(message),
                severity: severity,
                userIds: new[] { user }
                );
        }

        public async Task WelcomeInstallerAsync(Installer installer)
        {
            var notificationData = new LocalizableMessageNotificationData(
                new LocalizableString(
                    "NewTenantRegisteredNotificationMessage",
                    TechnoFormsConsts.LocalizationSourceName
                    )
                );
            var TenantName = _tenantRepository.GetAll().Where(T => T.Id == installer.TenantId).Select(T => T.TenancyName).FirstOrDefault();
            notificationData["tenancyName"] = TenantName;
            await _notificationPublisher.PublishAsync(AppNotificationNames.NewInstaller, notificationData);

            //await _notificationPublisher.PublishAsync(
            //    AppNotificationNames.NewInstaller,
            //    new MessageNotificationData(L("WelcomeToTheApplicationNotificationMessage")),
            //    severity: NotificationSeverity.Success,
            //    userIds: null
            //    );
        }
    }
}