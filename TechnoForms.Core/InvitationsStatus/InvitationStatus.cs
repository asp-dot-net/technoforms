﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.InvitationsStatus
{
    [Table("InvitationStatus")]
    public class InvitationStatus : FullAuditedEntity
    {
        public virtual int StatusCode { get; set; }

        public virtual string StatusName { get; set; }
    }
}
