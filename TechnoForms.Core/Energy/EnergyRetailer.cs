﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Energy
{
	[Table("EnergyRetailer")]
	public class EnergyRetailer : FullAuditedEntity,IMustHaveTenant
	{
		public virtual string Name { get; set; }

		public virtual bool NSW { get; set; }

		public virtual bool SA { get; set; }

		public virtual bool QLD { get; set; }

		public virtual bool VIC { get; set; }

		public virtual bool WA { get; set; }

		public virtual bool ACT { get; set; }

		public virtual bool TAS { get; set; }

		public virtual bool NT { get; set; }

		public virtual int? ElectricityProviderId { get; set; }

		public int TenantId { get; set; }
	}
}
