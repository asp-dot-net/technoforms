﻿using System;
using System.ComponentModel.DataAnnotations;
using Abp.MultiTenancy;
using TechnoForms.Authorization.Users;

namespace TechnoForms.MultiTenancy
{
    /// <summary>
    /// Represents a Tenant in the system.
    /// A tenant is a isolated customer for the application
    /// which has it's own users, roles and other application entities.
    /// </summary>
    public class Tenant : AbpTenant<User>
    {
        public const int MaxLogoMimeTypeLength = 64;
        public const int MaxFullNameLength = 200;
        public const int MaxCompanyNameLength = 200;
        public const int MaxABNLength = 50;
        public const int MaxFAXLength = 50;
        public const int MaxPhoneLength = 20;
        public const int MaxMobileLength = 20;
        public const int MaxEmailLength = 250;
        public const int MaxRetailerNameLength = 200;
        public const int MaxRetailerABNLength = 50;
        public const int MaxStreetAddressLength = 300;
        public const int MaxUnitNumberLength = 50;
        public const int MaxUnitTypeLength = 50;
        public const int MaxStreetNumberLength = 50;
        public const int MaxStreetNameLength = 100;
        public const int MaxStreetTypeLength = 50;
        public const int MaxStreetCityLength = 100;
        public const int MaxStreetStateLength = 50;
        public const int MaxStreetPostCodeLength = 50;
        //Can add application specific tenant properties here

        public virtual Guid? CustomCssId { get; set; }

        public virtual Guid? LogoId { get; set; }

        [MaxLength(MaxLogoMimeTypeLength)]
        public virtual string LogoFileType { get; set; }

        [Required]
        [MaxLength(MaxFullNameLength)]
        public virtual string FullName { get; set; }

        [Required]
        [MaxLength(MaxCompanyNameLength)]
        public virtual string CompanyName { get; set; }

        [Required]
        [MaxLength(MaxABNLength)]
        public virtual string ABN { get; set; }

        [MaxLength(MaxFAXLength)]
        public virtual string FAX { get; set; }

        [Required]
        [MaxLength(MaxPhoneLength)]
        public virtual string Phone { get; set; }

        [MaxLength(MaxMobileLength)]
        public virtual string Mobile { get; set; }

        [Required]
        [MaxLength(MaxEmailLength)]
        public virtual string Email { get; set; }

        [Required]
        public virtual float STCCharges { get; set; }

        [Required]
        [MaxLength(MaxRetailerNameLength)]
        public virtual string RetailerName { get; set; }

        [Required]
        [MaxLength(MaxRetailerABNLength)]
        public virtual string RetailerABN { get; set; }

        [MaxLength(MaxStreetAddressLength)]
        public virtual string StreetAddress { get; set; }

        [MaxLength(MaxUnitNumberLength)]
        public virtual string UnitNo { get; set; }

        [MaxLength(MaxUnitTypeLength)]
        public virtual string UnitType { get; set; }

        [MaxLength(MaxStreetNumberLength)]
        public virtual string StreetNumber { get; set; }

        [MaxLength(MaxStreetNameLength)]
        public virtual string StreetName { get; set; }

        [MaxLength(MaxStreetTypeLength)]
        public virtual string StreetType { get; set; }

        [MaxLength(MaxStreetCityLength)]
        public virtual string Suburb { get; set; }

        public virtual int? Suburb_Id { get; set; }

        [MaxLength(MaxStreetStateLength)]
        public virtual string State { get; set; }

        [MaxLength(MaxStreetPostCodeLength)]
        public virtual string PostCode { get; set; }

        protected Tenant()
        {

        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {

        }

        public virtual bool HasLogo()
        {
            return LogoId != null && LogoFileType != null;
        }

        public void ClearLogo()
        {
            LogoId = null;
            LogoFileType = null;
        }
    }
}