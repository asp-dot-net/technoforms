﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.UnitTypes
{    
    [Table("UnitType")]
    public class UnitType : FullAuditedEntity
    {
        public const int MaxUnitTypeNameLength = 50;

        [Required]
        [MaxLength(MaxUnitTypeNameLength)]
        public virtual string UnitTypeName { get; set; }
    }
}
