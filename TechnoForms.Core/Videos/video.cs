﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Videos
{
    [Table("video")]
    public class video : FullAuditedEntity
    {
        public const int MaxTitleLength = 50;

        [Required]
        [MaxLength(MaxTitleLength)]
        public virtual string Title { get; set; }

        [Required]
        public virtual string FileName { get; set; }
    }
}
