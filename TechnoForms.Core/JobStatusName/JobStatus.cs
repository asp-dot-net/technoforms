﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.JobStatusName
{
    [Table("JobStatus")]
    public class JobStatus : FullAuditedEntity
    {
        public virtual string JobStatusName { get; set; }
    }
}
