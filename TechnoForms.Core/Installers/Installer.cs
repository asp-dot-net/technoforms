﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.PostalAddressTypes;
using TechnoForms.PostalDeliveryTypes;

namespace TechnoForms.Installers
{
    [Table("Installer")]
    public class Installer : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxFullNameLength = 200;
        public const int MaxPhoneLength = 10;
        public const int MaxMobileLength = 10;
        public const int MaxEmailLength = 255;
        public const int MaxCompanyNameLength = 100;
        public const int MaxABNLength = 50;
        public const int MaxCompanyPhoneLength = 10;
        public const int MaxFaxLength = 20;
        public const int MaxPostalDelevaryNoLength = 50;
        public const int MaxlogoLength = 500;
        public const int MaxStreetAddressLength = 300;
        public const int MaxUnitNumberLength = 50;
        public const int MaxUnitTypeLength = 50;
        public const int MaxStreetNumberLength = 50;
        public const int MaxStreetNameLength = 100;
        public const int MaxStreetTypeLength = 50;
        public const int MaxStreetCityLength = 100;
        public const int MaxStreetStateLength = 50;
        public const int MaxStreetPostCodeLength = 50;
        public const int MaxInstallerExpiryLength = 100;
        public const int MaxElectricalExpiryLength = 100;
        public const int MaxDesignerExpiryLength = 100;
        public const int MaxAccreditationAccPhotoLength = 200;
        public const int MaxLicencePhotoLength = 200;
        public const int MaxInstallerSignatureLength = 50;
        public const int MaxElectricianSignatureLength = 50;
        public const int MaxDesignerSignatureLength = 50;

        public int TenantId { get; set; }

        public virtual int? UserId { get; set; }

        public virtual string UserName { get; set; }

        //[Required]
        [MaxLength(MaxFullNameLength)]
        public virtual string FullName { get; set; }

        //[Required]
        [MaxLength(MaxPhoneLength)]
        public virtual string Phone { get; set; }

        [MaxLength(MaxMobileLength)]
        public virtual string Mobile { get; set; }

        //[Required]
        [MaxLength(MaxEmailLength)]
        public virtual string EmailId { get; set; }

        //[Required]
        [MaxLength(MaxCompanyNameLength)]
        public virtual string CompanyName { get; set; }

        //[Required]
        [MaxLength(MaxABNLength)]
        public virtual string ABN { get; set; }

        [MaxLength(MaxCompanyPhoneLength)]
        public virtual string CompanyPhone { get; set; }

        [MaxLength(MaxFaxLength)]
        public virtual string Fax { get; set; }

        public virtual int AddressType { get; set; }

        [MaxLength(MaxPostalDelevaryNoLength)]
        public virtual string PostalDelNo { get; set; }

        public virtual int PostalDelType { get; set; }

        [Required]
        public virtual bool IsGST { get; set; }

        [Required]
        [MaxLength(MaxlogoLength)]
        public virtual string Logo { get; set; }

        [MaxLength(MaxStreetAddressLength)]
        public virtual string StreetAddress { get; set; }

        [MaxLength(MaxUnitNumberLength)]
        public virtual string UnitNo { get; set; }

        [MaxLength(MaxUnitTypeLength)]
        public virtual string UnitType { get; set; }

        [Required]
        [MaxLength(MaxStreetNumberLength)]
        public virtual string StreetNumber { get; set; }

        [Required]
        [MaxLength(MaxStreetNameLength)]
        public virtual string StreetName { get; set; }

        [Required]
        [MaxLength(MaxStreetTypeLength)]
        public virtual string StreetType { get; set; }

        [Required]
        [MaxLength(MaxStreetCityLength)]
        public virtual string Suburb { get; set; }

        public virtual int? Suburb_Id { get; set; }

        [Required]
        [MaxLength(MaxStreetStateLength)]
        public virtual string State { get; set; }

        [Required]
        [MaxLength(MaxStreetPostCodeLength)]
        public virtual string PostCode { get; set; }

        public virtual bool? IsInst { get; set; }

        [MaxLength(MaxInstallerExpiryLength)]
        public virtual string InstallerExpiry { get; set; }

        public virtual DateTime? InsAccreExDate { get; set; }

        public virtual bool? IsElec { get; set; }

        [MaxLength(MaxElectricalExpiryLength)]
        public virtual string ElectricalExpiry { get; set; }

        public virtual DateTime? EleLicenceExDate { get; set; }

        public virtual bool? IsDesc { get; set; }

        [MaxLength(MaxDesignerExpiryLength)]
        public virtual string DesignerExpiry { get; set; }

        public virtual DateTime? DesignerAccreExDate { get; set; }

        [Required]
        [MaxLength(MaxAccreditationAccPhotoLength)]
        public virtual string AccreditationAccPhoto { get; set; }

        [Required]
        [MaxLength(MaxLicencePhotoLength)]
        public virtual string LicencePhoto { get; set; }

        public virtual Guid Guid { get; set; }

        [MaxLength(MaxInstallerSignatureLength)]
        public virtual string InstallerSignature { get; set; }

        [MaxLength(MaxElectricianSignatureLength)]
        public virtual string ElectricianSignature { get; set; }

        [MaxLength(MaxDesignerSignatureLength)]
        public virtual string DesignerSignature { get; set; }

        public virtual int IsApproved { get; set; }

        public virtual string RejectReason { get; set; }
        ////public virtual string BankName { get; set; }
        ////public virtual string AccName { get; set; }
        ////public virtual string BSB { get; set; }
        ////public virtual string AccNumber { get; set; }
    }
}
