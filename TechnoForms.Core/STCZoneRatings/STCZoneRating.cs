﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.STCZoneRatings
{
    [Table("STCZoneRating")]
    public class STCZoneRating : FullAuditedEntity
    {
        public double STCRating { get; set; }

        public string upsize_ts { get; set; }
    }
}
