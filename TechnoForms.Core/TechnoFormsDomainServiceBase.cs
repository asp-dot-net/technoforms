﻿using Abp.Domain.Services;

namespace TechnoForms
{
    public abstract class TechnoFormsDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected TechnoFormsDomainServiceBase()
        {
            LocalizationSourceName = TechnoFormsConsts.LocalizationSourceName;
        }
    }
}
