﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Photos
{
    [Table("Photo")]
    public class Photo : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxImageNameLength = 100;
        public const int MaxLongitudeLength = 50;
        public const int MaxLatitudeLength = 50;

        [Required]
        [MaxLength(MaxImageNameLength)]
        public virtual string ImageName { get; set; }

        public virtual Guid Guid { get; set; }

        public virtual int FileType { get; set; }

        public virtual int? OldSystemDirection { get; set; }

        public virtual Guid ImageGuid { get; set; }

        [Required]
        [MaxLength(MaxLongitudeLength)]
        public virtual string Longitude { get; set; }

        [Required]
        [MaxLength(MaxLatitudeLength)]
        public virtual string Latitude { get; set; }

        public int TenantId { get; set; }
    }
}
