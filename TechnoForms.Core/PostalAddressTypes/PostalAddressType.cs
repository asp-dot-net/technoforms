﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.PostalAddressTypes
{
    [Table("PostalAddressType")]
    public class PostalAddressType : FullAuditedEntity
    {
        public virtual string PostalAddressTypeName { get; set; }
    }
}
