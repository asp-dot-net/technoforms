﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.OldSystemDirections
{
    [Table("OldSystemDirection")]
    public class OldSystemDirection : FullAuditedEntity
    {
        public const int MaxDairectionNameLength = 50;

        [Required]
        [MaxLength(MaxDairectionNameLength)]
        public virtual string DirectionName { get; set; }
    }
}
