﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("RecYear")]
    public class RecYear : FullAuditedEntity
    {
        public virtual int Year { get; set; }
        public virtual string REC_Year { get; set; }
        public virtual decimal STCRate { get; set; }
    }
}
