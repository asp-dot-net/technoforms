﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("TypeOfConnection")]
    public class TypeOfConnection : FullAuditedEntity
    {
        public virtual string TypeOfConnectionName { get; set; }
    }
}
