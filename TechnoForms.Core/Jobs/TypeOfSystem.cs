﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("TypeOfSystem")]
    public class TypeOfSystem : FullAuditedEntity
    {
        public virtual string TypeOfSystemName { get; set; }
    }
}
