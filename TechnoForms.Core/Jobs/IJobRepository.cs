﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.Jobs.Dto;

namespace TechnoForms.Jobs
{
    public interface IJobRepository
    {
        Task<List<JobList>> GetJobListAsync(JobListInput input);
    }
}
