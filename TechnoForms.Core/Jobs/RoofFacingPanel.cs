﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("RoofFacingPanel")]
    public class RoofFacingPanel : FullAuditedEntity
    {
        public const int MaxRoofFacingPanelLength = 100;

        [MaxLength(MaxRoofFacingPanelLength)]
        public virtual string RoofFacingPanelName { get; set; }
    }
}
