﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("Job")]
    public class Job : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxFirstNameLength = 100;
        public const int MaxLastNameLength = 100;
        public const int MaxCompanyLength = 100;
        public const int MaxEmailIdLength = 50;
        public const int MaxPhoneLength = 20;
        public const int MaxFaxLength = 20;
        public const int MaxPostalDeliveryNumberLength = 20;
        public const int MaxStreetAddressLength = 100;
        public const int MaxUnitNoLength = 20;
        public const int MaxStreetNoLength = 20;
        public const int MaxStreetCityLength = 100;
        public const int MaxStreetStateLength = 100;
        public const int MaxStreetPostCodeLength = 20;

        public virtual int TenantId { get; set; }

        [Required]
        [MaxLength(MaxFirstNameLength)]
        public virtual string FirstName { get; set; }

        [Required]
        [MaxLength(MaxLastNameLength)]
        public virtual string LastName { get; set; }

        [MaxLength(MaxCompanyLength)]
        public virtual string Company { get; set; }

        [Required]
        [MaxLength(MaxEmailIdLength)]
        public virtual string EmailId { get; set; }

        [Required]
        [MaxLength(MaxPhoneLength)]
        public virtual string Phone { get; set; }

        [MaxLength(MaxFaxLength)]
        public virtual string Fax { get; set; }

        public virtual string PostalAddressType { get; set; }

        [MaxLength(MaxPostalDeliveryNumberLength)]
        public virtual string PostalDeliveryNumber { get; set; }

        public virtual string PostalDeliveryType { get; set; }

        [MaxLength(MaxStreetAddressLength)]
        public virtual string StreetAddress { get; set; }

        [MaxLength(MaxUnitNoLength)]
        public virtual string UnitNo { get; set; }

        public virtual string UnitType { get; set; }

        [Required]
        [MaxLength(Job.MaxStreetNoLength)]
        public virtual string StreetNo { get; set; }

        [Required]
        public virtual string StreetName { get; set; }

        [Required]
        public virtual string StreetType { get; set; }

        [Required]
        [MaxLength(MaxStreetCityLength)]
        public virtual string StreetCity { get; set; }

        public virtual int? Suburb_Id { get; set; }

        [Required]
        [MaxLength(MaxStreetStateLength)]
        public virtual string StreetState { get; set; }

        [Required]
        [MaxLength(MaxStreetPostCodeLength)]
        public virtual string StreetPostCode { get; set; }


        [MaxLength(MaxStreetAddressLength)]
        public virtual string InstallationStreetAddress { get; set; }

        [MaxLength(MaxUnitNoLength)]
        public virtual string InstallationUnitNo { get; set; }

        public virtual string InstallationUnitType { get; set; }

        [Required]
        [MaxLength(MaxStreetNoLength)]
        public virtual string InstallationStreetNo { get; set; }

        [Required]
        public virtual string InstallationStreetName { get; set; }

        [Required]
        public virtual string InstallationStreetType { get; set; }

        [Required]
        [MaxLength(MaxStreetCityLength)]
        public virtual string InstallationStreetCity { get; set; }

        public virtual int? InstallationSuburb_Id { get; set; }

        [Required]
        [MaxLength(MaxStreetStateLength)]
        public virtual string InstallationStreetState { get; set; }

        [Required]
        [MaxLength(MaxStreetPostCodeLength)]
        public virtual string InstallationStreetPostCode { get; set; }

        public virtual bool installationAddresssameasHouseAddress { get; set; }

        public virtual int JobStatus_Id { get; set; }

        //Site Details
        public const int MaxNMINoLength = 100;
        public const int MaxRegPlanNoLength = 20;
        public const int MaxLotNoLength = 20;
        public const int MaxMeterNumberLength = 50;
        public const int MaxAemoDERjobIDLength = 50;

        [Required]
        public virtual int HouseType { get; set; }

        [Required]
        public virtual int RoofType { get; set; }

        [Required]
        [MaxLength(MaxNMINoLength)]
        public virtual string NMINo { get; set; }

        [MaxLength(MaxRegPlanNoLength)]
        public virtual string RegPlanNo { get; set; }

        [MaxLength(MaxLotNoLength)]
        public virtual string LotNo { get; set; }

        [MaxLength(MaxMeterNumberLength)]
        public virtual string MeterNumber { get; set; }

        public virtual int? EnergyDestributar { get; set; }
        
        public virtual int? EnergyRetailer { get; set; }

        [MaxLength(MaxAemoDERjobIDLength)]
        public virtual string AemoDERjobID { get; set; }

        //Panel Details
        public const int MaxPanelNameLength = 200;

        [Required]
        [MaxLength(MaxPanelNameLength)]
        public virtual string PanelName { get; set; }

        [MaxLength(MaxPanelNameLength)]
        public virtual string BrandName { get; set; }

        [Required]
        public virtual int Panel_id { get; set; }

        public virtual string PanelModel { get; set; }
        public virtual decimal PanelSize { get; set; }
        public virtual int NoOfPanels { get; set; }
        public virtual decimal SystemCapacity { get; set; }

        //Inverters Details
        public const int MaxInverterNameLength = 200;

        [Required]
        [MaxLength(MaxInverterNameLength)]
        public virtual string InverterName { get; set; }

        [MaxLength(MaxInverterNameLength)]
        public virtual string InverterBrandName { get; set; }

        [Required]
        public virtual int Inverter_id { get; set; }

        public virtual string InverterModel { get; set; }

        [MaxLength(MaxInverterNameLength)]
        public virtual string InverterSeries { get; set; }
        public virtual decimal NoOfInverter { get; set; }
        public virtual decimal InverterOutput { get; set; }


        //Installation Details

        public virtual int SolarType_Id { get; set; }
        public virtual int SystemType_Id { get; set; }
        public virtual DateTime ExpectedInstallDate { get; set; }
        public virtual int RoofFacingPanel_Id { get; set; }
        public virtual int Sugstthisaddress_Id { get; set; }
        public virtual int LocationOfNewSystem_Id { get; set; }
        public virtual int Customer_Id { get; set; }
        public virtual int CreateFor_Id { get; set; }
        public virtual int Installer_Id { get; set; }
        public virtual int Designer_Id { get; set; }
        public virtual int Electrician_Id { get; set; }
        public virtual string ProjectNumber { get; set; }
        public virtual string InstallerNote { get; set; }

        //GST Details

        public virtual int? IsCustomerRegistredGST { get; set; }
        public virtual string CustomerABN { get; set; }
        public virtual string CustomerOrganizationName { get; set; }

        //REC Details

        public const int MaxAddcapMoredetailedLength = 100;
        public const int MaxResourceAvailabilityLength = 100;
        public const int MaxAccreditationCodeLength = 100;
        public const int MaxExplanatoryNotesLength = 100;
        public const int MaxInstallPropertyNameLength = 100;
        public const int MaxDescribeAdequateIinfoLength = 100;
        public const int MaxAdditionalSystemInformationLength = 100;

        public virtual int Typeofsystem_Id { get; set; }
        public virtual int Typeofconnection_Id { get; set; }
        public virtual int Mountingtype_Id { get; set; }
        public virtual int Auditreportavailable_Id { get; set; }

        public virtual int Installingcompleteunit_Id { get; set; }
        [MaxLength(MaxAddcapMoredetailedLength)]
        public virtual string AddcapMoredetailed { get; set; }
        public virtual int RECYear_Id { get; set; }
        public virtual int Defaultresourceavailfigure_Id { get; set; }

        [MaxLength(MaxResourceAvailabilityLength)]
        public virtual string ResourceAvailability { get; set; }
        [MaxLength(MaxAccreditationCodeLength)]
        public virtual string AccreditationCode { get; set; }
        [MaxLength(MaxExplanatoryNotesLength)]
        public virtual string ExplanatoryNotes { get; set; }
        public virtual int CeatingCert_Id { get; set; }

        public virtual int CECAccreditationStatement_Id { get; set; }
        public virtual int GovernmentSitingApprovals_Id { get; set; }
        public virtual int ElectricalSafetyDocumentation_Id { get; set; }
        public virtual int AustralianStandardsStatement_Id { get; set; }

        public virtual int NotgridConnectedStatement_Id { get; set; }
        [MaxLength(MaxInstallPropertyNameLength)]
        public virtual string InstallPropertyName { get; set; }
        public virtual int SGUatThisAddress_Id { get; set; }
        [MaxLength(MaxDescribeAdequateIinfoLength)]
        public virtual string DescribeAdequateIinfo { get; set; }

        [MaxLength(MaxAdditionalSystemInformationLength)]
        public virtual string AdditionalSystemInformation { get; set; }
        public virtual int SystemMountingType_Id { get; set; }
        public virtual int OwnerAddressType_Id { get; set; }
        public virtual int CustomerAddressType_Id { get; set; }

        public virtual bool InvoiceGenereted { get; set; }


        public const int MaxSignatureImageInstLength = 100;
        public const int MaxSignatureImageOwnerLength = 100;
        public const int MaxRECUploadFileNameLength = 100;
        public const int MaxPVDNumberLength = 50;
        public const int MaxPVDStatusLength = 50;
        public const int MaxFormIdLength = 50;
        public const int MaxBulkUploadIDLength = 50;
        public const int MaxSignatureImagePickListLength = 100;
        public const int MaxOwnerPhotoLength = 100;
        public const int MaxInstallerPhotoLength = 100;
        public const int MaxLongitude_CustSignLength = 50;
        public const int MaxLatitude_CustSignLength = 50;
        public const int MaxLongitude_InstSignLength = 50;
        public const int MaxLatitude_InstSignLength = 50;
        public const int MaxRejectReasonPALength = 1000;
        public const int MaxRejectReasonINLength = 1000;
        public const int MaxRejectReasonIMLength = 1000;
        public const int MaxRejectReasonCSLength = 1000;
        public const int MaxRejectReasonISLength = 1000;
        public const int MaxRejectReasonPKLength = 1000;
        public const int MaxRejectReasonSTCLength = 1000;
        public const int MaxRejectReasonELELength = 1000;
        public const int MaxRejectReasonCUCKLength = 1000;
        public const int MaxElectricianSignatureLength = 100;
        public const int MaxDesignerSignatureLength = 100;
        public const int MaxLongitude_ElecSignLength = 50;
        public const int MaxLatitude_ElecSignLength = 50;
        public const int MaxLongitude_DesiSignLength = 50;
        public const int MaxLatitude_DesiSignLength = 50;
        public const int MaxLongitude_PickListLength = 50;
        public const int MaxLatitude_PickListLength = 50;
        public const int MaxCustomerIPLength = 50;
        public const int MaxInstallerIPLength = 50;

        public const int MaxInstCommentLength = 100;
        public const int MaxOwnerCommentLength = 100;

        public virtual Guid Guid { get; set; }
        [MaxLength(MaxFormIdLength)]
        public virtual string FormId { get; set; }
        [MaxLength(MaxSignatureImageInstLength)]
        public virtual string SignatureImageInst { get; set; }
        [MaxLength(MaxSignatureImageOwnerLength)]
        public virtual string SignatureImageOwner { get; set; }
        [MaxLength(MaxRECUploadFileNameLength)]
        public virtual string RECUploadFileName { get; set; }
        public virtual Nullable<System.DateTime> RECUploadDate { get; set; }
        [MaxLength(MaxBulkUploadIDLength)]
        public virtual string BulkUploadID { get; set; }
        [MaxLength(MaxPVDNumberLength)]
        public virtual string PVDNumber { get; set; }
        [MaxLength(MaxPVDStatusLength)]
        public virtual string PVDStatus { get; set; }
        public virtual Nullable<System.DateTime> RECCreateDate { get; set; }
        public virtual Nullable<System.DateTime> SignatureImageInstDate { get; set; }
        public virtual Nullable<System.DateTime> SignatureImageOwnerDate { get; set; }
        [MaxLength(MaxSignatureImagePickListLength)]
        public virtual string SignatureImagePickList { get; set; }
        public virtual Nullable<System.DateTime> SignatureImaegePickListDate { get; set; }
        public virtual Nullable<bool> IsVerifySTC { get; set; }
        public virtual Nullable<bool> IsVerifyImages { get; set; }
        [MaxLength(MaxOwnerPhotoLength)]
        public virtual string OwnerPhoto { get; set; }
        public virtual Nullable<bool> IsVerifyPanels { get; set; }
        public virtual Nullable<bool> IsVerifyInverters { get; set; }
        [MaxLength(MaxInstallerPhotoLength)]
        public virtual string InstallerPhoto { get; set; }
        public virtual Nullable<bool> IsReapply { get; set; }
        public virtual Nullable<bool> IsRenew { get; set; }
        public virtual Nullable<bool> IsVerifyCS { get; set; }
        public virtual Nullable<bool> IsVerifyIN { get; set; }
        public virtual Nullable<bool> IsVerifyPA { get; set; }
        public virtual Nullable<bool> IsVerifyIS { get; set; }
        public virtual Nullable<bool> IsVerifyPK { get; set; }
        public virtual Nullable<bool> IsVerifyELE { get; set; }
        public virtual Nullable<bool> IsVerifyCUCK { get; set; }
        [MaxLength(MaxLongitude_CustSignLength)]
        public virtual string Longitude_CustSign { get; set; }
        [MaxLength(MaxLatitude_CustSignLength)]
        public virtual string Latitude_CustSign { get; set; }
        [MaxLength(MaxLongitude_InstSignLength)]
        public virtual string Longitude_InstSign { get; set; }
        [MaxLength(MaxLatitude_InstSignLength)]
        public virtual string Latitude_InstSign { get; set; }

        public virtual Nullable<bool> IsComplaiencePA { get; set; }
        public virtual Nullable<int> PAVerifiedBy { get; set; }
        public virtual Nullable<DateTime> PAVerifiedDate { get; set; }

        public virtual Nullable<bool> IsComplaienceIN { get; set; }
        public virtual Nullable<int> INVerifiedBy { get; set; }
        public virtual Nullable<DateTime> INVerifiedDate { get; set; }

        public virtual Nullable<bool> ISComplaienceIM { get; set; }
        public virtual Nullable<int> IMVerifiedBy { get; set; }
        public virtual Nullable<DateTime> IMVerifiedDate { get; set; }

        public virtual Nullable<bool> IsComplaienceCS { get; set; }
        public virtual Nullable<int> CSVerifiedBy { get; set; }
        public virtual Nullable<DateTime> CSVerifiedDate { get; set; }

        public virtual Nullable<bool> IsComplaienceIS { get; set; }
        public virtual Nullable<int> ISVerifiedBy { get; set; }
        public virtual Nullable<DateTime> ISVerifiedDate { get; set; }

        public virtual Nullable<bool> IsComplaiencePK { get; set; }
        public virtual Nullable<int> PKVerifiedBy { get; set; }

        public virtual Nullable<bool> IsComplaienceSTC { get; set; }
        public virtual Nullable<int> STCVerifiedBy { get; set; }
        public virtual Nullable<DateTime> STCVerifiedDate { get; set; }

        public virtual Nullable<bool> IsComplaienceELE { get; set; }
        public virtual Nullable<int> ELEVerifiedBy { get; set; }
        public virtual Nullable<DateTime> ELEVerifiedDate { get; set; }

        public virtual Nullable<bool> IsComplaienceCUCK { get; set; }
        public virtual Nullable<int> CUCKVerifiedBy { get; set; }
        public virtual Nullable<DateTime> CUCKVerifiedDate { get; set; }

        public virtual Nullable<System.DateTime> RequestSignatureImageInstDate { get; set; }
        public virtual Nullable<System.DateTime> RequestSignatureImageOwnerDate { get; set; }
        [MaxLength(MaxRejectReasonPALength)]
        public virtual string RejectReasonPA { get; set; }
        [MaxLength(MaxRejectReasonINLength)]
        public virtual string RejectReasonIN { get; set; }
        [MaxLength(MaxRejectReasonIMLength)]
        public virtual string RejectReasonIM { get; set; }
        [MaxLength(MaxRejectReasonCSLength)]
        public virtual string RejectReasonCS { get; set; }
        [MaxLength(MaxRejectReasonISLength)]
        public virtual string RejectReasonIS { get; set; }
        [MaxLength(MaxRejectReasonPKLength)]
        public virtual string RejectReasonPK { get; set; }
        [MaxLength(MaxRejectReasonSTCLength)]
        public virtual string RejectReasonSTC { get; set; }
        [MaxLength(MaxRejectReasonELELength)]
        public virtual string RejectReasonELE { get; set; }
        [MaxLength(MaxRejectReasonCUCKLength)]
        public virtual string RejectReasonCUCK { get; set; }
        public virtual Nullable<int> RoofFacing { get; set; }
        [MaxLength(MaxElectricianSignatureLength)]
        public virtual string ElectricianSignature { get; set; }
        public virtual Nullable<System.DateTime> ElectricianSignatureDate { get; set; }
        [MaxLength(MaxDesignerSignatureLength)]
        public virtual string DesignerSignature { get; set; }
        public virtual Nullable<System.DateTime> DesignerSignatureDate { get; set; }
        [MaxLength(MaxLongitude_ElecSignLength)]
        public virtual string Longitude_ElecSign { get; set; }
        [MaxLength(MaxLatitude_ElecSignLength)]
        public virtual string Latitude_ElecSign { get; set; }
        [MaxLength(MaxLongitude_DesiSignLength)]
        public virtual string Longitude_DesiSign { get; set; }
        [MaxLength(MaxLatitude_DesiSignLength)]
        public virtual string Latitude_DesiSign { get; set; }
        [MaxLength(MaxCustomerIPLength)]
        public virtual string CustomerIP { get; set; }
        [MaxLength(MaxInstallerIPLength)]
        public virtual string InstallerIP { get; set; }
        [MaxLength(MaxLongitude_PickListLength)]
        public virtual string Longitude_PickList { get; set; }
        [MaxLength(MaxLatitude_PickListLength)]
        public virtual string Latitude_PickList { get; set; }

        [MaxLength(MaxInstCommentLength)]
        public virtual string InstComment { get; set; }
        [MaxLength(MaxOwnerCommentLength)]
        public virtual string OwnerComment { get; set; }

        public virtual string JobLatitude { get; set; }
        public virtual string JobLongitude { get; set; }

        public virtual DateTime? STCAppliedDate { get; set; }
        public virtual string NumberOfCertificate { get; set; }

        public virtual decimal? TotalSTC { get; set; }

        public virtual decimal? RegistredSTC { get; set; }

        public virtual decimal? ExpectedSTC { get; set; }
    }
}
