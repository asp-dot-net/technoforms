﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("HouseType")]
    public class HouseType : FullAuditedEntity
    {
        public const int MaxHouseTypeCodeLength = 100;

        [MaxLength(MaxHouseTypeCodeLength)]
        public virtual string HouseTypeName { get; set; }
    }
}
