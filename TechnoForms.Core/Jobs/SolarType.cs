﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("SolarType")]
    public class SolarType : FullAuditedEntity
    {
        public const int MaxSolarTypeLength = 100;

        [MaxLength(MaxSolarTypeLength)]
        public virtual string SolarTypeeName { get; set; }
    }
}
