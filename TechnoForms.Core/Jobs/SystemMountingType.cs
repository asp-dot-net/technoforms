﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("SystemMountingType")]
    public class SystemMountingType : FullAuditedEntity
    {
        public virtual string SystemMountingTypeName { get; set; }
    }
}
