﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("CustomerSignLog")]
    public class CustomerSignLog : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }
        public virtual int JobId { get; set; }
        public virtual Guid JobGuId { get; set; }
        public virtual int SenderUserId { get; set; }
        public virtual DateTime TokenDate { get; set; }
        public virtual Guid TokenGuid { get; set; }
        public virtual string Token { get; set; }
        public virtual string SendBy { get; set; }
        public virtual string Longitude { get; set; }
        public virtual string Latitude { get; set; }
    }
}
