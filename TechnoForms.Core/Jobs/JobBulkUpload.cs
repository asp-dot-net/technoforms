﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
	[Table("JobBulkUpload")]
	public class JobBulkUpload : FullAuditedEntity
	{
		public const int MaxJobFileName = 500;

		[Required]
		[MaxLength(MaxJobFileName)]
		public virtual string JobFileName { get; set; }
	}
}
