﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("Token")]
    public class Token : FullAuditedEntity, IMustHaveTenant
    {
        
        public const int MaxTokenLength = 100;
        public virtual int TenantId { get; set; }
        public virtual int JobId { get; set; }
        public virtual Guid JobGuId { get; set; }
        [Required]
        [MaxLength(MaxTokenLength)]
        public virtual string TokenNo { get; set; }

        public virtual Guid GuId { get; set; }
        public virtual bool IsExpired { get; set; }
        public virtual bool IsOwner { get; set; }
        public virtual bool IsSent { get; set; }
    }
}
