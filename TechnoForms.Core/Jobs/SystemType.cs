﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("SystemType")]
    public class SystemType : FullAuditedEntity
    {
        public const int MaxSystemTypeLength = 100;

        [MaxLength(MaxSystemTypeLength)]
        public virtual string SystemTypeName { get; set; }
    }
}
