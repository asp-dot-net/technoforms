﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("CustomerSignLogResponse")]
    public class CustomerSignLogResponse : FullAuditedEntity, IMustHaveTenant
    {
        public virtual int TenantId { get; set; }
        public virtual int JobId { get; set; }
        public virtual int Request_Id { get; set; }
        public virtual String Token { get; set; }
    }
}
