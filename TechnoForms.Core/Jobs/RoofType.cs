﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs
{
    [Table("RoofType")]
    public class RoofType : FullAuditedEntity
    {
        public const int MaxRoofTypeCodeLength = 100;

        [MaxLength(MaxRoofTypeCodeLength)]
        public virtual string RoofTypeName { get; set; }
    }
}
