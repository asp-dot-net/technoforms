﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Jobs.Dto
{
    public class JobListInput //: PagedAndSortedInputDto
    {
        public string ProjectNumber { get; set; }
        public string FilterFormId { get; set; }
        public string FilterOwnerName { get; set; }
        public int? FilterCity { get; set; }
        public string FilterState { get; set; }
        public int? FilterInstallerId { get; set; }
        public int? FilterSystemtypeId { get; set; }
        public int? FilterJobStatusId { get; set; }
        public int? FilterDateypeId { get; set; }
        public DateTime? FilterStartDate { get; set; }
        public DateTime? FilterEndDate { get; set; }        
        public int[] FilterReceivedId { get; set; }
        public int[] FilterNotReceivedId { get; set; }
        public bool IsVerified { get; set; }
        public int SkipCount { get; set; }
        public int MaxResultCount { get; set; }
        public string Sorting { get; set; }
        public string AscDesc { get; set; }
    }
}