﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.PostalDeliveryTypes
{
    [Table("PostalDeliveryType")]
    public class PostalDeliveryType : FullAuditedEntity
    {
        public virtual string PostalDeliveryTypeName { get; set; }
    }
}
