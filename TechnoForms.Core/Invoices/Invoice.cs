﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Invoices
{
    [Table("Invoice")]
    public class Invoice : FullAuditedEntity, IMustHaveTenant
    {
        public virtual string InvoiceNumber { get; set; }
        public virtual decimal InvoiceAmount { get; set; }
        public virtual string JobIds { get; set; }
        public virtual int TenantId { get; set; }
        public virtual int Invoice_Status { get; set; }
    }
}
