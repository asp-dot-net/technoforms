﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.ScanedPanels
{
    [Table("ScanedPanel")]
    public class ScanedPanel : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxPanelSerialNumberLength = 50;
        public const int MaxPanelBarcodePhotoLength = 100;
        public const int MaxLongitudeLength = 50;
        public const int MaxLatitudeLength = 50;

        public int TenantId { get; set; }

        //[Required]
        [MaxLength(MaxPanelSerialNumberLength)]
        public virtual string PanelSerialNumber { get; set; }

        public virtual Guid Guid { get; set; }

        public virtual Boolean IsScan { get; set; }

        [Required]
        [MaxLength(MaxPanelBarcodePhotoLength)]
        public virtual string PanelBarcodePhoto { get; set; }

        [Required]
        [MaxLength(MaxLongitudeLength)]
        public virtual string Longitude { get; set; }

        [Required]
        [MaxLength(MaxLatitudeLength)]
        public virtual string Latitude { get; set; }
    }
}
