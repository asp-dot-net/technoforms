﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TechnoForms.States
{
    [Table("State")]
    public class State : FullAuditedEntity
    {
        public const int MaxStateNameLength = 50;

        [Required]
        [MaxLength(MaxStateNameLength)]
        public virtual string StateName { get; set; }
    }
}
