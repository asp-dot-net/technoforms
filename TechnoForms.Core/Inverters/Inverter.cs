﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Inverters
{
    [Table("Inverter")]
    public class Inverter : FullAuditedEntity
    {
        public const int MaxManufacturarNameLength = 100;
        public const int MaxSeriesNameLength = 100;
        public const int MaxModelNumberLength = 100;

        [Required]
        [MaxLength(MaxManufacturarNameLength)]
        public virtual string ManufacturarName { get; set; }

        [Required]
        [MaxLength(MaxSeriesNameLength)]
        public virtual string Series { get; set; }

        [Required]
        [MaxLength(MaxModelNumberLength)]
        public virtual string ModelNumber { get; set; }

        [Required]
        public virtual decimal ACPower { get; set; }

        [Required]
        public virtual decimal Sizes { get; set; }

        [Required]
        public virtual DateTime ApprovalDate { get; set; }
        
        [Required]
        public virtual DateTime ExpiryDate { get; set; }

        public virtual string InverterLogo { get; set; }
    }
}
