﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Inverters
{
	[Table("InverterBulkUpload")]
	public class InverterBulkUpload : FullAuditedEntity
	{
		public const int MaxInverterFileName = 500;

		[Required]
		[MaxLength(MaxInverterFileName)]
		public virtual string InverterFileName { get; set; }
	}
}
