﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.AdminDashboard.Dto;

namespace TechnoForms.AdminDashboard
{
	public interface IAdminDashboardRepository
	{
		Task<List<AdminDashboardJobList>> GetDashboardJobListAsync();
	}
}
