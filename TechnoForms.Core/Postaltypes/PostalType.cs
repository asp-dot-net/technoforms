﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace TechnoForms.Postaltypes
{
    [Table("PostalType")]
    public class PostalType : FullAuditedEntity
    {
        public const int MaxPostalTypeNameLength = 50;
        public const int MaxPostalTypeCodeLength = 20;

        [Required]
        [MaxLength(MaxPostalTypeNameLength)]
        public virtual string PostalTypeName { get; set; }
        [Required]
        [MaxLength(MaxPostalTypeCodeLength)]
        public virtual string PostalTypeCode { get; set; }
    }
}
