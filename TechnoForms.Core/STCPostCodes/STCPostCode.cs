﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.STCPostCodes
{
    [Table("STCPostCode")]
    public class STCPostCode : FullAuditedEntity
    {
        public int STCPostCodeFrom { get; set; }
        public int STCPostCodeTo { get; set; }
        public int STCZoneID { get; set; }
    }
}
