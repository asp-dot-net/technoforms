﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.CustomerInstallers
{
    [Table("CustomerInstaller")]
    public class CustomerInstaller : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxInstRejectReasonLength = 500;
        public const int MaxRejectReasonLength = 500;

        public int TenantId { get; set; }

        public int InstallerId { get; set; }

        public virtual int? UserId { get; set; }

        public int Inst_Request_status { get; set; }

        [MaxLength(MaxInstRejectReasonLength)]
        public string Inst_RejectReason { get; set; }
        
        public int Request_status { get; set; }

        [MaxLength(MaxRejectReasonLength)]
        public string RejectReason { get; set; }

        public bool IsCreated { get; set; }

        public Guid Guid { get; set; }

    }
}
