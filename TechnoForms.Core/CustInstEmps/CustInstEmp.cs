﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.CustInstEmps
{
    [Table("CustInstEmp")]
    public class CustInstEmp : FullAuditedEntity, IMustHaveTenant
    {
        public int TenantId { get; set; }

        public int InstallerId { get; set; }

        public int EmployeeId { get; set; }

        public bool IsCreated { get; set; }
    }
}
