﻿using Abp.MultiTenancy;
using Abp.Zero.Configuration;

namespace TechnoForms.Authorization.Roles
{
    public static class AppRoleConfig
    {
        public static void Configure(IRoleManagementConfig roleManagementConfig)
        {
            //Static host roles

            roleManagementConfig.StaticRoles.Add(
                new StaticRoleDefinition(
                    StaticRoleNames.Host.Admin,
                    MultiTenancySides.Host,
                    grantAllPermissionsByDefault: true)
                );

            roleManagementConfig.StaticRoles.Add(
                new StaticRoleDefinition(
                    StaticRoleNames.Host.Installer,
                    MultiTenancySides.Host)
                {
                    GrantedPermissions = { "Pages.MobileAppAccess" }
                }
                );

            //Static tenant roles

            roleManagementConfig.StaticRoles.Add(
                new StaticRoleDefinition(
                    StaticRoleNames.Tenants.Admin,
                    MultiTenancySides.Tenant,
                    grantAllPermissionsByDefault: true)
                {
                    GrantedPermissions = { "Pages.Tenant.Jobs",
                    "Pages.Tenant.Jobs.Create",
                    "Pages.Tenant.Jobs.Delete",
                    "Pages.Tenant.Jobs.Edit",
                    "Pages.Tenant.Jobs.Detail",
                    "Pages.Tenant.CompliaenceJobs",
                    "Pages.Tenant.CompliaenceJobs.Delete",
                    "Pages.Tenant.CompliaenceJobs.Edit",
                    "Pages.Tenant.CompliaenceJobs.Detail",
                    "Pages.STC",
                    "Pages.InstallerInvitation",
                    "Pages.Tenant.Installer",
                    "Pages.Tenant.Installer.Create",
                    "Pages.Tenant.Installer.Detail",
                    "Pages.Tenant.Installer.Delete",
                    "Pages.Tenant.Installer.Edit",
                    "Pages.Tenant.Dashboard",
                    "Pages.Tenant.Calendar",
                    "Pages.Administration.Roles",
                    "Pages.Administration.Roles.Create",
                    "Pages.Administration.Roles.Edit",
                    "Pages.Administration.Roles.Delete",
                    "Pages.Administration.Users",
                    "Pages.Administration.Users.Create",
                    "Pages.Administration.Users.Edit",
                    "Pages.Administration.Users.Delete",
                    "Pages.Administration.Users.ChangePermissions",
                    "Pages.Administration.Users.Impersonation",
                    "Pages.Administration.Tenant.Settings",
                    "Pages.Administration.AuditLogs",
                    "Pages.Administration.OrganizationUnits",
                    "Pages.Administration.OrganizationUnits.ManageOrganizationTree",
                    "Pages.Administration.OrganizationUnits.ManageMembers",
                    "Pages",
                    "Pages.Administration"
                    }
                }
                );

            roleManagementConfig.StaticRoles.Add(
                new StaticRoleDefinition(
                    StaticRoleNames.Tenants.User,
                    MultiTenancySides.Tenant)
                );

            //roleManagementConfig.StaticRoles.Add(
            //     new StaticRoleDefinition(
            //         StaticRoleNames.Tenants.Installer,
            //         MultiTenancySides.Tenant)
            //     );
        }
    }
}
