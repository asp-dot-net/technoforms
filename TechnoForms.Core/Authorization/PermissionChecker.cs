﻿using Abp.Authorization;
using TechnoForms.Authorization.Roles;
using TechnoForms.Authorization.Users;
using TechnoForms.MultiTenancy;

namespace TechnoForms.Authorization
{
    /// <summary>
    /// Implements <see cref="PermissionChecker"/>.
    /// </summary>
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
