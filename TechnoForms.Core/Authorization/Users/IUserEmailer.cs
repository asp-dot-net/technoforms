﻿using System.Threading.Tasks;
using TechnoForms.Chat;

namespace TechnoForms.Authorization.Users
{
    public interface IUserEmailer
    {
        /// <summary>
        /// Send email activation link to user's email address.
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="plainPassword">
        /// Can be set to user's plain password to include it in the email.
        /// </param>
        Task SendEmailActivationLinkAsync(User user, string plainPassword = null);

        /// <summary>
        /// Sends a password reset link to user's email.
        /// </summary>
        /// <param name="user">User</param>
        Task SendPasswordResetLinkAsync(User user);

        /// <summary>
        /// Sends an email for unread chat message to user's email.
        /// </summary>
        /// <param name="user"></param>
        /// <param name="senderUsername"></param>
        /// <param name="senderTenancyName"></param>
        /// <param name="chatMessage"></param>
        void TryToSendChatMessageMail(User user, string senderUsername, string senderTenancyName, ChatMessage chatMessage);

        /// <summary>
        /// Send email activation link to user's email address.
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="plainPassword">
        /// Can be set to user's plain password to include it in the email.
        /// </param>
        Task SendEmailUserCreationkAsync(User user, string plainPassword = null);

        /// <summary>
        /// Send Customer Signature Request.
        /// </summary>
        /// <param name="user">User</param>
        /// Can be set to user's plain password to include it in the email.
        /// </param>
        Task SendEmailCustomerSignatureRequestAsync(User user, string link);

        Task SendEmailInstallerVerificationRequestAsync(string FromEmail, string ToEmail, string TenantId, string name);

        Task SendEmailInstallerVerificationResponseAsync(string ToEmail, string TenantId, string name, string status, string reason);

        Task SendEmailInstallerApprovalResponseAsync(string ToEmail, string TenantId, string name, string status, string reason);

    }
}
