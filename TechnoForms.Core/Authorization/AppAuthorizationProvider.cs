﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace TechnoForms.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));
            var master = pages.CreateChildPermission(AppPermissions.Pages_Master, L("Master"), multiTenancySides: MultiTenancySides.Host);

            
            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));

            //var RECUser = administration.CreateChildPermission(AppPermissions.Pages_Administration_RECUser, L("RECUser"));
            //RECUser.CreateChildPermission(AppPermissions.Pages_Administration_RECUser_Create, L("CreatingNewRECUser"));
            //RECUser.CreateChildPermission(AppPermissions.Pages_Administration_RECUser_Edit, L("EditingRECUser"));
            //RECUser.CreateChildPermission(AppPermissions.Pages_Administration_RECUser_Delete, L("DeletingRECUser"));

            //var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            //languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            //languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            //languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            //languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));

            //pages.CreateChildPermission(AppPermissions.Pages__JobsReport, L("JobReport"));

            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);
            pages.CreateChildPermission(AppPermissions.Pages_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);

            var Jobs = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs, L("Jobs"), multiTenancySides: MultiTenancySides.Tenant);
            Jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Create, L("CreatingNewJobs"), multiTenancySides: MultiTenancySides.Tenant);
            Jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Edit, L("EditingJobs"), multiTenancySides: MultiTenancySides.Tenant);
            Jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Delete, L("DeletingJobs"), multiTenancySides: MultiTenancySides.Tenant);
            Jobs.CreateChildPermission(AppPermissions.Pages_Tenant_Jobs_Detail, L("DetailJobs"), multiTenancySides: MultiTenancySides.Tenant);
            Jobs.CreateChildPermission(AppPermissions.Pages_UserSpecificJob, L("UserSpecificJob"), multiTenancySides: MultiTenancySides.Tenant);

            var CompliaenceJobs = pages.CreateChildPermission(AppPermissions.Pages_Tenant_CompliaenceJobs, L("CompliaenceJobs"), multiTenancySides: MultiTenancySides.Tenant);
            CompliaenceJobs.CreateChildPermission(AppPermissions.Pages_Tenant_CompliaenceJobs_Edit, L("EditingCompliaenceJobs"), multiTenancySides: MultiTenancySides.Tenant);
            CompliaenceJobs.CreateChildPermission(AppPermissions.Pages_Tenant_CompliaenceJobs_Delete, L("DeletingCompliaenceJobs"), multiTenancySides: MultiTenancySides.Tenant);
            CompliaenceJobs.CreateChildPermission(AppPermissions.Pages_Tenant_CompliaenceJobs_Detail, L("DetailCompliaenceJobs"), multiTenancySides: MultiTenancySides.Tenant);

            var energyDestributars = pages.CreateChildPermission(AppPermissions.Pages_Tenant_EnergyDestributars, L("EnergyDestributars"), multiTenancySides: MultiTenancySides.Tenant);
            energyDestributars.CreateChildPermission(AppPermissions.Pages_Tenant_EnergyDestributars_Create, L("CreatingNewEnergyDestributars"), multiTenancySides: MultiTenancySides.Tenant);
            energyDestributars.CreateChildPermission(AppPermissions.Pages_Tenant_EnergyDestributars_Edit, L("EditingEnergyDestributars"), multiTenancySides: MultiTenancySides.Tenant);
            energyDestributars.CreateChildPermission(AppPermissions.Pages_Tenant_EnergyDestributars_Delete, L("DeletingEnergyDestributars"), multiTenancySides: MultiTenancySides.Tenant);

            var energyRetailers = pages.CreateChildPermission(AppPermissions.Pages_Tenant_EnergyRetailers, L("EnergyRetailers"), multiTenancySides: MultiTenancySides.Tenant);
            energyRetailers.CreateChildPermission(AppPermissions.Pages_Tenant_EnergyRetailers_Create, L("CreatingNewEnergyRetailers"), multiTenancySides: MultiTenancySides.Tenant);
            energyRetailers.CreateChildPermission(AppPermissions.Pages_Tenant_EnergyRetailers_Edit, L("EditingEnergyRetailers"), multiTenancySides: MultiTenancySides.Tenant);
            energyRetailers.CreateChildPermission(AppPermissions.Pages_Tenant_EnergyRetailers_Delete, L("DeletingEnergyRetailers"), multiTenancySides: MultiTenancySides.Tenant);

            var installer = pages.CreateChildPermission(AppPermissions.Pages_Tenant_Installer, L("Installer"), multiTenancySides: MultiTenancySides.Tenant);
            installer.CreateChildPermission(AppPermissions.Pages_Tenant_Installer_Create, L("CreatingNewInstaller"), multiTenancySides: MultiTenancySides.Tenant);
            installer.CreateChildPermission(AppPermissions.Pages_Tenant_Installer_Edit, L("EditingInstaller"), multiTenancySides: MultiTenancySides.Tenant);
            installer.CreateChildPermission(AppPermissions.Pages_Tenant_Installer_Delete, L("DeletingInstaller"), multiTenancySides: MultiTenancySides.Tenant);
            installer.CreateChildPermission(AppPermissions.Pages_UserSpecificInstaller, L("UserSpecificInstaller"), multiTenancySides: MultiTenancySides.Tenant);
            //HOST-SPECIFIC PERMISSIONS

            //var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            //editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            //editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            //editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);

            var streettype = master.CreateChildPermission(AppPermissions.Pages_Masters_StreetType, L("StreetType"), multiTenancySides: MultiTenancySides.Host);
            streettype.CreateChildPermission(AppPermissions.Pages_Masters_StreetType_Create, L("CreateNewStreetType"), multiTenancySides: MultiTenancySides.Host);
            streettype.CreateChildPermission(AppPermissions.Pages_Masters_StreetType_Edit, L("EditingStreetType"), multiTenancySides: MultiTenancySides.Host);
            streettype.CreateChildPermission(AppPermissions.Pages_Masters_StreetType_Delete, L("DeletingStreetType"), multiTenancySides: MultiTenancySides.Host);

            var streetname = master.CreateChildPermission(AppPermissions.Pages_Masters_StreetName, L("StreetName"), multiTenancySides: MultiTenancySides.Host);
            streetname.CreateChildPermission(AppPermissions.Pages_Masters_StreetName_Create, L("CreateStreetName"), multiTenancySides: MultiTenancySides.Host);
            streetname.CreateChildPermission(AppPermissions.Pages_Masters_StreetName_Edit, L("EditingStreetName"), multiTenancySides: MultiTenancySides.Host);
            streetname.CreateChildPermission(AppPermissions.Pages_Masters_StreetName_Delete, L("DeletingStreetName"), multiTenancySides: MultiTenancySides.Host);

            var postaltype = master.CreateChildPermission(AppPermissions.Pages_Masters_PostalType, L("PostalType"), multiTenancySides: MultiTenancySides.Host);
            postaltype.CreateChildPermission(AppPermissions.Pages_Masters_PostalType_Create, L("CreatingNewPostalType"), multiTenancySides: MultiTenancySides.Host);
            postaltype.CreateChildPermission(AppPermissions.Pages_Masters_PostalType_Edit, L("EditingPostalType"), multiTenancySides: MultiTenancySides.Host);
            postaltype.CreateChildPermission(AppPermissions.Pages_Masters_PostalType_Delete, L("DeletingPostalType"), multiTenancySides: MultiTenancySides.Host);

            var postalcodes = master.CreateChildPermission(AppPermissions.Pages_Masters_PostCodes, L("PostCode"), multiTenancySides: MultiTenancySides.Host);
            postalcodes.CreateChildPermission(AppPermissions.Pages_Masters_PostCodes_Create, L("CreatingNewPostalCodes"), multiTenancySides: MultiTenancySides.Host);
            postalcodes.CreateChildPermission(AppPermissions.Pages_Masters_PostCodes_Edit, L("EditingPostalCodes"), multiTenancySides: MultiTenancySides.Host);
            postalcodes.CreateChildPermission(AppPermissions.Pages_Masters_PostCodes_Delete, L("DeletingPostalCodes"), multiTenancySides: MultiTenancySides.Host);

            var unittype = master.CreateChildPermission(AppPermissions.Pages_Masters_UnitType, L("UnitType"), multiTenancySides: MultiTenancySides.Host);
            unittype.CreateChildPermission(AppPermissions.Pages_Masters_UnitType_Create, L("CreatingNewUnitType"), multiTenancySides: MultiTenancySides.Host);
            unittype.CreateChildPermission(AppPermissions.Pages_Masters_UnitType_Edit, L("EditingUnitType"), multiTenancySides: MultiTenancySides.Host);
            unittype.CreateChildPermission(AppPermissions.Pages_Masters_UnitType_Delete, L("DeletingUnitType"), multiTenancySides: MultiTenancySides.Host);

            var inverter = master.CreateChildPermission(AppPermissions.Pages_Masters_Inverter, L("Inverter"), multiTenancySides: MultiTenancySides.Host);
            inverter.CreateChildPermission(AppPermissions.Pages_Masters_Inverter_Create, L("CreatingNewInverter"), multiTenancySides: MultiTenancySides.Host);
            inverter.CreateChildPermission(AppPermissions.Pages_Masters_Inverter_Edit, L("EditingInverter"), multiTenancySides: MultiTenancySides.Host);
            inverter.CreateChildPermission(AppPermissions.Pages_Masters_Inverter_Delete, L("DeletingInverter"), multiTenancySides: MultiTenancySides.Host);

            var panel = master.CreateChildPermission(AppPermissions.Pages_Masters_Panel, L("Panel"), multiTenancySides: MultiTenancySides.Host);
            panel.CreateChildPermission(AppPermissions.Pages_Masters_Panel_Create, L("CreatingNewPanel"), multiTenancySides: MultiTenancySides.Host);
            panel.CreateChildPermission(AppPermissions.Pages_Masters_Panel_Edit, L("EditingPanel"), multiTenancySides: MultiTenancySides.Host);
            panel.CreateChildPermission(AppPermissions.Pages_Masters_Panel_Delete, L("DeletingPanel"), multiTenancySides: MultiTenancySides.Host);

            var newInvitation = pages.CreateChildPermission(AppPermissions.Pages_New_Invitation, L("NewInstallerInvitation"), multiTenancySides: MultiTenancySides.Host);
            var invitation = pages.CreateChildPermission(AppPermissions.Pages_adminInvitation, L("Invitation"), multiTenancySides: MultiTenancySides.Host);
            var installerInvitation = pages.CreateChildPermission(AppPermissions.Pages_InstallerInvitation, L("Invitation"), multiTenancySides: MultiTenancySides.Tenant);

            var AllInstallers = pages.CreateChildPermission(AppPermissions.Pages_AllInstallers, L("AllInstallers"), multiTenancySides: MultiTenancySides.Host);
            var AllInstallerRequests = pages.CreateChildPermission(AppPermissions.Pages_AllInstallerRequests, L("AllInstallerRequests"), multiTenancySides: MultiTenancySides.Host);

            var MobileAppAccess = pages.CreateChildPermission(AppPermissions.Pages_MobileAppAccess, L("MobileAppAccess"), multiTenancySides: MultiTenancySides.Host);

            var invoice = pages.CreateChildPermission(AppPermissions.Pages_Invoice, L("Invoice"), multiTenancySides: MultiTenancySides.Host);
            invoice.CreateChildPermission(AppPermissions.Pages_Invoice_Generate, L("InvoiceGenerate"), multiTenancySides: MultiTenancySides.Host);
            invoice.CreateChildPermission(AppPermissions.Pages_Invoice_Generated, L("InvoiceGenerated"), multiTenancySides: MultiTenancySides.Host);
            invoice.CreateChildPermission(AppPermissions.Pages_Invoice_Collection, L("Collection"), multiTenancySides: MultiTenancySides.Host);

            var Calendar = pages.CreateChildPermission(AppPermissions.Pages_Calendar, L("Calendar"), multiTenancySides: MultiTenancySides.Tenant);
            var video = pages.CreateChildPermission(AppPermissions.Pages_Video, L("Video"), multiTenancySides: MultiTenancySides.Host);
            video.CreateChildPermission(AppPermissions.Pages_Video_Create, L("CreatingNewvideo"), multiTenancySides: MultiTenancySides.Host);
            video.CreateChildPermission(AppPermissions.Pages_Video_Edit, L("Editingvideo"), multiTenancySides: MultiTenancySides.Host);
            video.CreateChildPermission(AppPermissions.Pages_Video_Delete, L("Deletingvideo"), multiTenancySides: MultiTenancySides.Host);

            var STC = pages.CreateChildPermission(AppPermissions.Pages_STC, L("STCPage"), multiTenancySides: MultiTenancySides.Tenant);
            //var Test = pages.CreateChildPermission(AppPermissions.Pages_Test, L("Test"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, TechnoFormsConsts.LocalizationSourceName);
        }
    }
}
