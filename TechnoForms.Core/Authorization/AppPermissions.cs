﻿namespace TechnoForms.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_Administration = "Pages.Administration";
        public const string Pages_Master = "Pages.Master";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";

        //public const string Pages_Administration_RECUser = "Pages.Administration.RECUser";
        //public const string Pages_Administration_RECUser_Create = "Pages.Administration.RECUser.Create";
        //public const string Pages_Administration_RECUser_Edit = "Pages.Administration.RECUser.Edit";
        //public const string Pages_Administration_RECUser_Delete = "Pages.Administration.RECUser.Delete";

        //public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        //public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        //public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        //public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        //public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";
        public const string Pages_Host_Dashboard = "Pages.Host.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Tenant_Installer = "Pages.Tenant.Installer";
        public const string Pages_Tenant_Installer_Create = "Pages.Tenant.Installer.Create";
        public const string Pages_Tenant_Installer_Detail = "Pages.Tenant.Installer.Detail";
        public const string Pages_Tenant_Installer_Delete = "Pages.Tenant.Installer.Delete";
        public const string Pages_Tenant_Installer_Edit = "Pages.Tenant.Installer.Edit";
        public const string Pages_UserSpecificInstaller = "Pages.UserSpecificInstaller";

        public const string Pages_Tenant_Jobs = "Pages.Tenant.Jobs";
        public const string Pages_Tenant_Jobs_Create = "Pages.Tenant.Jobs.Create";
        public const string Pages_Tenant_Jobs_Delete = "Pages.Tenant.Jobs.Delete";
        public const string Pages_Tenant_Jobs_Edit = "Pages.Tenant.Jobs.Edit";
        public const string Pages_Tenant_Jobs_Detail = "Pages.Tenant.Jobs.Detail";
        public const string Pages_UserSpecificJob = "Pages.UserSpecificJob";

        public const string Pages_Tenant_CompliaenceJobs = "Pages.Tenant.CompliaenceJobs";
        public const string Pages_Tenant_CompliaenceJobs_Delete = "Pages.Tenant.CompliaenceJobs.Delete";
        public const string Pages_Tenant_CompliaenceJobs_Edit = "Pages.Tenant.CompliaenceJobs.Edit";
        public const string Pages_Tenant_CompliaenceJobs_Detail = "Pages.Tenant.CompliaenceJobs.Detail";

        public const string Pages_Tenant_EnergyDestributars = "Pages.Tenant.EnergyDestributars";
        public const string Pages_Tenant_EnergyDestributars_Create = "Pages.Tenant.EnergyDestributars.Create";
        public const string Pages_Tenant_EnergyDestributars_Delete = "Pages.Tenant.EnergyDestributars.Delete";
        public const string Pages_Tenant_EnergyDestributars_Edit = "Pages.Tenant.EnergyDestributars.Edit";

        public const string Pages_Tenant_EnergyRetailers = "Pages.Tenant.EnergyRetailers";
        public const string Pages_Tenant_EnergyRetailers_Create = "Pages.Tenant.EnergyRetailers.Create";
        public const string Pages_Tenant_EnergyRetailers_Delete = "Pages.Tenant.EnergyRetailers.Delete";
        public const string Pages_Tenant_EnergyRetailers_Edit = "Pages.Tenant.EnergyRetailers.Edit";

        //HOST-SPECIFIC PERMISSIONS
        public const string Pages_Masters_StreetType = "Pages.Masters.StreetType";
        public const string Pages_Masters_StreetType_Create = "Pages.Masters.StreetType.Create";
        public const string Pages_Masters_StreetType_Delete = "Pages.Masters.StreetType.Delete";
        public const string Pages_Masters_StreetType_Edit = "Pages.Masters.StreetType.Edit";


        public const string Pages_Masters_StreetName = "Pages.Masters.StreetName";
        public const string Pages_Masters_StreetName_Create = "Pages.Masters.StreetName.Create";
        public const string Pages_Masters_StreetName_Delete = "Pages.Masters.StreetName.Delete";
        public const string Pages_Masters_StreetName_Edit = "Pages.Masters.StreetName.Edit";

        public const string Pages_Masters_PostalType = "Pages.Masters.PostalType";
        public const string Pages_Masters_PostalType_Create = "Pages.Masters.PostalType.Create";
        public const string Pages_Masters_PostalType_Delete = "Pages.Masters.PostalType.Delete";
        public const string Pages_Masters_PostalType_Edit = "Pages.Masters.PostalType.Edit";

        public const string Pages_Masters_PostCodes = "Pages.Masters.PostCodes";
        public const string Pages_Masters_PostCodes_Create = "Pages.Masters.PostCodes.Create";
        public const string Pages_Masters_PostCodes_Delete = "Pages.Masters.PostCodes.Delete";
        public const string Pages_Masters_PostCodes_Edit = "Pages.Masters.PostCodes.Edit";

        public const string Pages_Masters_UnitType = "Pages.Masters.UnitType";
        public const string Pages_Masters_UnitType_Create = "Pages.Masters.UnitType.Create";
        public const string Pages_Masters_UnitType_Delete = "Pages.Masters.UnitType.Delete";
        public const string Pages_Masters_UnitType_Edit = "Pages.Masters.UnitType.Edit";

        public const string Pages_Masters_Inverter = "Pages.Masters.Inverter";
        public const string Pages_Masters_Inverter_Create = "Pages.Masters.Inverter.Create";
        public const string Pages_Masters_Inverter_Delete = "Pages.Masters.Inverter.Delete";
        public const string Pages_Masters_Inverter_Edit = "Pages.Masters.Inverter.Edit";

        public const string Pages_Masters_Panel = "Pages.Masters.Panel";
        public const string Pages_Masters_Panel_Create = "Pages.Masters.Panel.Create";
        public const string Pages_Masters_Panel_Delete = "Pages.Masters.Panel.Delete";
        public const string Pages_Masters_Panel_Edit = "Pages.Masters.Panel.Edit";

        public const string Pages_New_Invitation = "Pages.newInvitation";
        public const string Pages_adminInvitation = "Pages.AdminInvitation";
        public const string Pages_InstallerInvitation = "Pages.InstallerInvitation";

        public const string Pages_AllInstallers = "Pages.AllInstallers";
        public const string Pages_AllInstallerRequests = "Pages.AllInstallerRequests";

        //public const string Pages_Editions = "Pages.Editions";
        //public const string Pages_Editions_Create = "Pages.Editions.Create";
        //public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        //public const string Pages_Editions_Delete = "Pages.Editions.Delete";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";

        public const string Pages_Invoice = "Pages.Invoice";
        public const string Pages_Invoice_Generate = "Pages.Invoice.Generate";
        public const string Pages_Invoice_Generated = "Pages.Invoice.Generated";
        public const string Pages_Invoice_Collection = "Pages.Invoice.Collection";

        public const string Pages_Calendar = "Pages.Tenant.Calendar";
        public const string Pages_Video = "Pages.Video";
        public const string Pages_Video_Create = "Pages.Video.Create";
        public const string Pages_Video_Delete = "Pages.Video.Delete";
        public const string Pages_Video_Edit = "Pages.Video.Edit";

        public const string Pages_MobileAppAccess = "Pages.MobileAppAccess";

        public const string Pages_STC = "Pages.STC";
        //public const string Pages_Test = "Pages.Test";
    }
}