﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.StreetTypes
{
    [Table("StreetType")]
    public class StreetType : FullAuditedEntity
    {
        public const int MaxStreetTypeNameLength = 50;
        public const int MaxStreetTypeCodeLength = 10;

        [Required]
        [MaxLength(MaxStreetTypeNameLength)]
        public virtual string StreetTypeName { get; set; }

        [Required]
        [MaxLength(MaxStreetTypeCodeLength)]
        public virtual string StreetTypeCode { get; set; }

    }
}
