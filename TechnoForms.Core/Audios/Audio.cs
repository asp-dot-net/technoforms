﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Audios
{
    [Table("Audio")]
    public class Audio : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxFileNameLength = 500;
        public const int MaxDisplayFileNameLength = 500;

        public virtual int JobId { get; set; }

        [Required]
        [MaxLength(MaxFileNameLength)]
        public virtual string FileName { get; set; }

        [Required]
        [MaxLength(MaxDisplayFileNameLength)]
        public virtual string DisplayFileName { get; set; }

        public int TenantId { get; set; }
    }
}
