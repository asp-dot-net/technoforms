﻿using Abp.Domain.Entities.Auditing;
using System.ComponentModel.DataAnnotations.Schema;

namespace TechnoForms.ModeOfPayments
{
    [Table("ModeOfPayment")]
    public class ModeOfPayment : FullAuditedEntity
    {
        public virtual string ModeOfPaymentName { get; set; }
    }
}
