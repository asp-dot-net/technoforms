﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Identity
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}
