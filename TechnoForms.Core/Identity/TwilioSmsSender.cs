﻿using Abp.Dependency;
using Abp.UI;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace TechnoForms.Identity
{
    public class TwilioSmsSender : ISmsSender, ITransientDependency
    {
        //private TwilioSmsSenderConfiguration _twilioSmsSenderConfiguration;

        //public TwilioSmsSender(TwilioSmsSenderConfiguration twilioSmsSenderConfiguration)
        //{
        //    _twilioSmsSenderConfiguration = twilioSmsSenderConfiguration;
        //}

        public async Task SendAsync(string number, string message)
        {
            try
            {
                TwilioClient.Init("AC991f3f103f6a5f608278c1e283f139a1", "be9c00d8443b360082ab02d3d78969b2");

                MessageResource resource = await MessageResource.CreateAsync(
                body: message,
                @from: new Twilio.Types.PhoneNumber("+61418671214"),
                to: new Twilio.Types.PhoneNumber(number));
            }
            catch (Exception E)
            {
                throw new UserFriendlyException(E.Message);
            }
        }
    }
}
