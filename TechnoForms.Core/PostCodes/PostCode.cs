﻿using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechnoForms.States;

namespace TechnoForms.PostCodes
{
    [Table("PostCode")]
    public class PostCode : FullAuditedEntity
    {
        public const int MaxPostCodeLength = 50;
        public const int MaxSuburbLength = 50;
        public const int MaxPOBoxesLength = 50;
        public const int MaxAreaLength = 50;

        [Required]
        [MaxLength(MaxPostCodeLength)]
        public virtual string Postcode { get; set; }

        [Required]
        [MaxLength(MaxSuburbLength)]
        public virtual string Suburb { get; set; }

        [MaxLength(MaxPOBoxesLength)]
        public virtual string POBoxes { get; set; }

        [Required]
        [MaxLength(MaxAreaLength)]
        public virtual string Area { get; set; }

        [ForeignKey("State")]
        public virtual int StateId { get; set; }
        public State State { get; set; }
    }
}
