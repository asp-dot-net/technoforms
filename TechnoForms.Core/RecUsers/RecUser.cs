﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.RecUsers
{
    [Table("RecUser")]
    public class RecUser : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxRecUserNameLength = 50;
        public const int MaxRecPasswprdLength = 50;

        [Required]
        [MaxLength(MaxRecUserNameLength)]
        public virtual string RecUserName { get; set; }

        [Required]
        [MaxLength(MaxRecPasswprdLength)]
        public virtual string RecPasswprd { get; set; }

        public int TenantId { get; set; }
    }
}
