﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.Payments
{
    [Table("Payment")]
    public class Payment : FullAuditedEntity, IMustHaveTenant
    {
        public const int MaxInstrumentDetailLength = 500;

        [Required]
        public virtual int Invoice_ID { get; set; }

        [Required]
        public virtual int Modeofpayment_Id { get; set; }

        [Required]
        public virtual string date { get; set; }

        [MaxLength(MaxInstrumentDetailLength)]
        public virtual string InstrumentDetail { get; set; }

        [Required]
        public virtual decimal amount { get; set; }
        public virtual int TenantId { get; set; }
    }
}
