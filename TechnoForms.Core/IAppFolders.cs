﻿namespace TechnoForms
{
    public interface IAppFolders
    {
        string TempFileDownloadFolder { get; }

        string SampleProfileImagesFolder { get; }

        string WebLogsFolder { get; set; }

        string ComplienceDownloadFolder { get; set; }

        string ReportDownloadFolder { get; set; }
    }
}