﻿using Abp.Application.Services.Dto;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechnoForms.PhotoTypes
{
    [Table("PhotoType")]
    public class PhotoType : FullAuditedEntity
    {
        public const int MaxPhotoTypeNameLength = 50;

        [Required]
        [MaxLength(MaxPhotoTypeNameLength)]
        public virtual string PhotoTypeName { get; set; }
    }
}
