﻿namespace TechnoForms
{
    /// <summary>
    /// Some general constants for the application.
    /// </summary>
    public class TechnoFormsConsts
    {
        public const string LocalizationSourceName = "TechnoForms";

        public const bool MultiTenancyEnabled = true;
    }
}