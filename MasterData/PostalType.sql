--SET IDENTITY_INSERT [dbo].[PostalType] ON 

INSERT [dbo].[PostalType] ([PostalTypeName], [PostalTypeCode], [isDeleted], [CreationTime]) VALUES (N'CARE-OF POST OFFICE', N'CARE PO',0,'2019-09-17 12:20:25.340')
INSERT [dbo].[PostalType] ([PostalTypeName], [PostalTypeCode], [isDeleted], [CreationTime]) VALUES (N'COMMUNITY MAIL AGENT', N'CMA',0,'2019-09-17 12:20:25.340')
INSERT [dbo].[PostalType] ([PostalTypeName], [PostalTypeCode], [isDeleted], [CreationTime]) VALUES (N'COMMUNITY MAIL BAG', N'CMB',0,'2019-09-17 12:20:25.340')
INSERT [dbo].[PostalType] ([PostalTypeName], [PostalTypeCode], [isDeleted], [CreationTime]) VALUES (N'GENERAL POST OFFICE BOX', N'GPO BOX',0,'2019-09-17 12:20:25.340')
INSERT [dbo].[PostalType] ([PostalTypeName], [PostalTypeCode], [isDeleted], [CreationTime]) VALUES (N'LOCKED MAIL BAG SERVICE', N'LOCKED BAG',0,'2019-09-17 12:20:25.340')
INSERT [dbo].[PostalType] ([PostalTypeName], [PostalTypeCode], [isDeleted], [CreationTime]) VALUES (N'MAIL SERVICE', N'MS',0,'2019-09-17 12:20:25.340')
INSERT [dbo].[PostalType] ([PostalTypeName], [PostalTypeCode], [isDeleted], [CreationTime]) VALUES (N'POST OFFICE BOX', N'PO BOX',0,'2019-09-17 12:20:25.340')
INSERT [dbo].[PostalType] ([PostalTypeName], [PostalTypeCode], [isDeleted], [CreationTime]) VALUES (N'PRIVATE MAIL BAG SERVICE', N'PRIVATE BAG',0,'2019-09-17 12:20:25.340')
INSERT [dbo].[PostalType] ([PostalTypeName], [PostalTypeCode], [isDeleted], [CreationTime]) VALUES (N'MAIL SERVICE', N'RSD',0,'2019-09-17 12:20:25.340')
INSERT [dbo].[PostalType] ([PostalTypeName], [PostalTypeCode], [isDeleted], [CreationTime]) VALUES (N'ROADSIDE DELIVERY', N'RMB',0,'2019-09-17 12:20:25.340')
INSERT [dbo].[PostalType] ([PostalTypeName], [PostalTypeCode], [isDeleted], [CreationTime]) VALUES (N'ROADSIDE DELIVERY', N'RMC',0,'2019-09-17 12:20:25.340')
--SET IDENTITY_INSERT [dbo].[PostalType] OFF
