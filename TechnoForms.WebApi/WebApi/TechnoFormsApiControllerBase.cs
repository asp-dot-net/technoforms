using Abp.WebApi.Controllers;

namespace TechnoForms.WebApi
{
    public abstract class TechnoFormsApiControllerBase : AbpApiController
    {
        protected TechnoFormsApiControllerBase()
        {
            LocalizationSourceName = TechnoFormsConsts.LocalizationSourceName;
        }
    }
}