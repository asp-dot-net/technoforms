﻿using EntityFramework.DynamicFilters;
using TechnoForms.EntityFramework;

namespace TechnoForms.Tests.TestDatas
{
    public class TestDataBuilder
    {
        private readonly TechnoFormsDbContext _context;
        private readonly int _tenantId;

        public TestDataBuilder(TechnoFormsDbContext context, int tenantId)
        {
            _context = context;
            _tenantId = tenantId;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new TestOrganizationUnitsBuilder(_context, _tenantId).Create();

            _context.SaveChanges();
        }
    }
}
