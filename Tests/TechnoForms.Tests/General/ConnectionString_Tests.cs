﻿using System.Data.SqlClient;
using Shouldly;
using Xunit;

namespace TechnoForms.Tests.General
{
    public class ConnectionString_Tests
    {
        [Fact]
        public void SqlConnectionStringBuilder_Test()
        {
            var csb = new SqlConnectionStringBuilder("Server=localhost; Database=TechnoForms; Trusted_Connection=True;");
            csb["Database"].ShouldBe("TechnoForms");
        }
    }
}
