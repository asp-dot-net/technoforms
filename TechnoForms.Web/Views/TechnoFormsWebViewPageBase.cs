﻿using Abp.Dependency;
using Abp.Runtime.Session;
using Abp.Web.Mvc.Views;

namespace TechnoForms.Web.Views
{
    public abstract class TechnoFormsWebViewPageBase : TechnoFormsWebViewPageBase<dynamic>
    {
       
    }

    public abstract class TechnoFormsWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        public IAbpSession AbpSession { get; private set; }
        
        protected TechnoFormsWebViewPageBase()
        {
            AbpSession = IocManager.Instance.Resolve<IAbpSession>();
            LocalizationSourceName = TechnoFormsConsts.LocalizationSourceName;
        }
    }
}