﻿using System.Threading.Tasks;
using TechnoForms.Sessions.Dto;

namespace TechnoForms.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
