﻿using System.ComponentModel.DataAnnotations;
using Abp.Auditing;
using TechnoForms.Authorization.Users;
using TechnoForms.MultiTenancy;

namespace TechnoForms.Web.Models.TenantRegistration
{
    public class TenantRegistrationViewModel
    {
        [Required]
        [StringLength(Tenant.MaxTenancyNameLength)]
        public string TenancyName { get; set; }

        [Required]
        [StringLength(User.MaxNameLength)]
        public string Name { get; set; }

        [Required]
        [EmailAddress]
        [StringLength(User.MaxEmailAddressLength)]
        public string AdminEmailAddress { get; set; }

        [StringLength(User.MaxPlainPasswordLength)]
        [DisableAuditing]
        public string AdminPassword { get; set; }
    }
}