﻿using System.ComponentModel.DataAnnotations;
using Abp.Auditing;

namespace TechnoForms.Web.Models.Account
{
    public class LoginViewModel
    {
        public string TenancyName { get; set; }

        [Required(ErrorMessage = " ")]
        public string UsernameOrEmailAddress { get; set; }

        [Required(ErrorMessage = " ")]
        [DisableAuditing]
        public string Password { get; set; }

        public bool RememberMe { get; set; }
    }
}