﻿using System;

namespace TechnoForms.Web.Models.Layout
{
    public class TenantCustomCssViewModel
    {
        public Guid? CustomCssId { get; set; }
    }
}