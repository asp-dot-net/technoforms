﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TechnoForms.InstallerEmployes;
using TechnoForms.Installers;
using TechnoForms.Jobs;
using TechnoForms.MultiTenancy;
using TechnoForms.Report;
using TechnoForms.ScanedInverters;
using TechnoForms.ScanedPanels;
using Telerik.Reporting.Cache.File;
using Telerik.Reporting.Services;
using Telerik.Reporting.Services.Engine;
using Telerik.Reporting.Services.WebApi;

namespace TechnoForms.Web.Controllers
{
	public class ReportViewerController : ReportsControllerBase
	{
		static ReportServiceConfiguration preservedConfiguration;
		//private readonly IAppFolders _appFolders;
		private readonly IRepository<Job> _jobRepository;
		//private readonly IRepository<Installer> _installerRepository;
		//private readonly IRepository<InstallerEmploye> _installerEmployeRepository;
		//private readonly IRepository<Tenant> _tenantRepository;
		//private readonly IUnitOfWorkManager _unitOfWorkManager;
		//private readonly IRepository<ScanedPanel> _scanedPanelRepository;
		//private readonly IRepository<ScanedInverter> _scanedInverterRepository;

		IReportServiceConfiguration PreservedConfiguration
		{
			get
			{
				if (null == preservedConfiguration)
				{
					preservedConfiguration = new ReportServiceConfiguration
					{
						HostAppId = "DesignReportData",
						Storage = new FileStorage("/Temp/Reports"),
						ReportResolver = CreateResolver(),
						// ReportSharingTimeout = 0,
						// ClientSessionTimeout = 15,
					};
				}
				return preservedConfiguration;
			}
		}
		public ReportViewerController(
			//IAppFolders appFolders,
			IRepository<Job> jobRepository
			//IRepository<Installer> installerRepository,
			//IRepository<InstallerEmploye> installerEmployeRepository,
			//IRepository<Tenant> tenantRepository,
			//IUnitOfWorkManager unitOfWorkManager,
			//IRepository<ScanedPanel> scanedPanelRepository,
			//IRepository<ScanedInverter> scanedInverterRepository
			)
		{
			this.ReportServiceConfiguration = PreservedConfiguration;
			//_appFolders = appFolders;
			_jobRepository = jobRepository;
			//_installerRepository = installerRepository;
			//_installerEmployeRepository = installerEmployeRepository;
			//_tenantRepository = tenantRepository;
			//_unitOfWorkManager = unitOfWorkManager;
			//_scanedPanelRepository = scanedPanelRepository;
			//_scanedInverterRepository = scanedInverterRepository;
		}
		
		IReportResolver CreateResolver()
		{
			return new CustomReportResolver(_jobRepository);
			//var reportsPath = HttpContext.Current.Server.MapPath("~/Report Designer/Examples");
			//return new ReportFileResolver(reportsPath)
			//    .AddFallbackResolver(new ReportTypeResolver());
		}

		class CustomReportResolver : Telerik.Reporting.Services.Engine.IReportResolver
		{
			IRepository<Job> jobRepository;
			public Telerik.Reporting.ReportSource Resolve(string reporparam)
			{
				//== Report Name and design ID=====
				var JobId = 0;
				var reportdata = reporparam.Split(',');
				var reportName = reportdata[0];
				//if(reportdata[1] != "")
				//{
				//	JobId = Convert.ToInt32(reportdata[1]);
				//}
				
				//== Report Name and design ID=====
				//=== Company Logo - start======
				//string p_id = "0";
				//=== Company Logo - End======
				//=== Intiate datasource
				var instanceReportSource = new Telerik.Reporting.InstanceReportSource();

				#region ELEReport
				if (reportName == "ELEReport")
				{
					var report_parent = new Report.ELEReport();
					var db = new EntityFramework.TechnoFormsDbContext();
					if (JobId != 0)
					{
						//Guid Newguid = new Guid(JobId);                 
						//var Result = (from job in db.Job where (job.Id == JobId) select job).FirstOrDefault();

						//using (db.CurrentUnitOfWorkProvider.Current.SetTenantId(null))
						//{
						//	var InstDetail = (from inst in db.Installer where (inst.Id == Result.CreateFor_Id) select inst).FirstOrDefault();

						//	Telerik.Reporting.TextBox InstDate = report_parent.Items.Find("textBox82", true)[0] as Telerik.Reporting.TextBox;
						//	Telerik.Reporting.TextBox InstMonth = report_parent.Items.Find("textBox81", true)[0] as Telerik.Reporting.TextBox;
						//	Telerik.Reporting.TextBox InstYear = report_parent.Items.Find("textBox78", true)[0] as Telerik.Reporting.TextBox;
						//	Telerik.Reporting.TextBox ElecNumber = report_parent.Items.Find("textBox84", true)[0] as Telerik.Reporting.TextBox;
						//	Telerik.Reporting.TextBox InstName = report_parent.Items.Find("textBox91", true)[0] as Telerik.Reporting.TextBox;
						//	Telerik.Reporting.PictureBox InstSign = report_parent.Items.Find("pictureBox2", true)[0] as Telerik.Reporting.PictureBox;
						//	string imgmap = "";
						//	if (!string.IsNullOrEmpty(Result.ElectricianSignature))
						//	{
						//		imgmap = WebUrlService.WebSiteRootAddress + "Temp/ElectricianSignature/" + Result.ElectricianSignature;
						//		//imgmap = RemoveTenantName(imgmap);

						//		if (System.IO.File.Exists(imgmap))
						//		{
						//			InstSign.Value = Convert.ToString(imgmap);
						//		}
						//	}

						//	InstDate.Value = string.Format("{0:dd}", Convert.ToDateTime(Result.ElectricianSignatureDate));
						//	InstMonth.Value = string.Format("{0:MM}", Convert.ToDateTime(Result.ElectricianSignatureDate));
						//	InstYear.Value = string.Format("{0:yyyy}", Convert.ToDateTime(Result.ElectricianSignatureDate));
						//	ElecNumber.Value = InstDetail.InstallerExpiry;
						//	InstName.Value = InstDetail.FullName;
						//}
						//Telerik.Reporting.TextBox FirstName = report_parent.Items.Find("textBox102", true)[0] as Telerik.Reporting.TextBox;
						//Telerik.Reporting.TextBox Surname = report_parent.Items.Find("textBox99", true)[0] as Telerik.Reporting.TextBox;
						//Telerik.Reporting.TextBox Street = report_parent.Items.Find("textBox97", true)[0] as Telerik.Reporting.TextBox;
						//Telerik.Reporting.TextBox Suburb = report_parent.Items.Find("textBox94", true)[0] as Telerik.Reporting.TextBox;
						//Telerik.Reporting.TextBox PostCode = report_parent.Items.Find("textBox92", true)[0] as Telerik.Reporting.TextBox;

						//Telerik.Reporting.TextBox SignDate = report_parent.Items.Find("textBox1", true)[0] as Telerik.Reporting.TextBox;
						//Telerik.Reporting.TextBox SignMonth = report_parent.Items.Find("textBox2", true)[0] as Telerik.Reporting.TextBox;
						//Telerik.Reporting.TextBox SignYear = report_parent.Items.Find("textBox3", true)[0] as Telerik.Reporting.TextBox;

						//FirstName.Value = Result.FirstName;
						//Surname.Value = Result.LastName;
						//Street.Value = Result.StreetAddress;
						//Suburb.Value = Result.StreetCity;
						//PostCode.Value = Result.StreetPostCode;

						//SignDate.Value = string.Format("{0:dd}", Convert.ToDateTime(Result.ElectricianSignatureDate));
						//SignMonth.Value = string.Format("{0:MM}", Convert.ToDateTime(Result.ElectricianSignatureDate));
						//SignYear.Value = string.Format("{0:yyyy}", Convert.ToDateTime(Result.ElectricianSignatureDate));
					}

					instanceReportSource.ReportDocument = report_parent;
				}
				#endregion

				#region CECReport
				if (reportName == "CECReport")
				{
					var report_parent = new Report.CECReport();

					instanceReportSource.ReportDocument = report_parent;
				}
				#endregion

				#region CUCKReport
				if (reportName == "CUCKReport")
				{
					var report_parent = new Report.CUCKReport();

					instanceReportSource.ReportDocument = report_parent;
				}
				#endregion

				return instanceReportSource;
			}
			public CustomReportResolver(IRepository<Job> repository)
			{
				jobRepository = repository;
			}
			//public string RemoveTenantName(string Url)
			//{
			//	var db = new EntityFramework.TechnoFormsDbContext();

			//	var TenantName = (from tenant in db.Tenants
			//					  where tenant.Id == 
			//					  _tenantRepository.GetAll().Where(T => T.Id == AbpSession.TenantId).Select(T => T.TenancyName).FirstOrDefault();
			//	string NewUrl = Url.Replace("{TENANCY_NAME}.", TenantName);
			//	return NewUrl;
			//}
		}
	}
}