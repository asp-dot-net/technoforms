﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Abp.Auditing;
using Abp.Domain.Repositories;
using Abp.Runtime.Session;
using Abp.UI;
using Abp.Web.Models;
using Abp.Web.Mvc.Authorization;
using ExcelDataReader;
using TechnoForms.Dto;
using TechnoForms.Installers;
using TechnoForms.IO;
using TechnoForms.Jobs;
using TechnoForms.MultiTenancy;
using TechnoForms.Net.MimeTypes;
using TechnoForms.Storage;
using TechnoForms.StreetTypes;

namespace TechnoForms.Web.Controllers
{
	public class FileController : TechnoFormsControllerBase
	{
		private readonly IAppFolders _appFolders;
		private readonly IStreetTypeAppService _streetTypeAppService;
		private readonly IJobsAppService _jobsAppService;
		private readonly IBinaryObjectManager _binaryObjectManager;
		private readonly IRepository<Installer> _InstallerRepository;
		private readonly IRepository<Tenant> _tenantRepository;
		private readonly IAbpSession _abpsession;

		public FileController(IAppFolders appFolders,
			IStreetTypeAppService streetTypeAppService,
			IJobsAppService jobsAppService,
			IBinaryObjectManager binaryObjectManager,
			IRepository<Installer> InstallerRepository,
			IRepository<Tenant> tenantRepository,
			IAbpSession abpsession)
		{
			_appFolders = appFolders;
			_streetTypeAppService = streetTypeAppService;
			_jobsAppService = jobsAppService;
			_binaryObjectManager = binaryObjectManager;
			_InstallerRepository = InstallerRepository;
			_tenantRepository = tenantRepository;
			_abpsession = abpsession;
		}

		[AbpMvcAuthorize]
		[DisableAuditing]
		public ActionResult DownloadTempFile(FileDto file)
		{
			var filePath = Path.Combine(_appFolders.TempFileDownloadFolder, file.FileToken);
			if (!System.IO.File.Exists(filePath))
			{
				throw new UserFriendlyException(L("RequestedFileDoesNotExists"));
			}

			var fileBytes = System.IO.File.ReadAllBytes(filePath);
			System.IO.File.Delete(filePath);
			return File(fileBytes, file.FileType, file.FileName);
		}

		public ActionResult ImportData(HttpPostedFileBase file)
		{
			string fileName = System.Configuration.ConfigurationManager.AppSettings["FileToImport"].ToString();
			file.SaveAs(fileName);
			DataSet ds = GetDataFromExcel(fileName);
			_streetTypeAppService.CreateStreetType(new StreetTypes.Dto.CreateStreetTypeInput
			{

			});
			return Json(new { success = true });
		}

		public DataSet GetDataFromExcel(string fileName)
		{
			using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
			{

				// Auto-detect format, supports:
				//  - Binary Excel files (2.0-2003 format; *.xls)
				//  - OpenXml Excel files (2007 format; *.xlsx)
				using (var reader = ExcelReaderFactory.CreateReader(stream))
				{

					// Choose one of either 1 or 2:

					// 1. Use the reader methods
					do
					{
						while (reader.Read())
						{
							// reader.GetDouble(0);
						}
					} while (reader.NextResult());

					// 2. Use the AsDataSet extension method
					var result = reader.AsDataSet(new ExcelDataSetConfiguration()
					{

						// Gets or sets a value indicating whether to set the DataColumn.DataType 
						// property in a second pass.
						UseColumnDataType = true,

						// Gets or sets a callback to obtain configuration options for a DataTable. 
						ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
						{

							// Gets or sets a value indicating the prefix of generated column names.
							EmptyColumnNamePrefix = "Column",

							// Gets or sets a value indicating whether to use a row from the 
							// data as column names.
							UseHeaderRow = true,

							// Gets or sets a callback to determine which row is the header row. 
							// Only called when UseHeaderRow = true.
							ReadHeaderRow = (rowReader) =>
							{
								// F.ex skip the first row and use the 2nd row as column headers:
								//rowReader.Read();
							},

							// Gets or sets a callback to determine whether to include the 
							// current row in the DataTable.
							FilterRow = (rowReader) =>
							{
								return true;
							},

							// Gets or sets a callback to determine whether to include the specific
							// column in the DataTable. Called once per column after reading the 
							// headers.
							FilterColumn = (rowReader, columnIndex) =>
							{
								return true;
							}
						}
					});
					return result;
					// The result of each spreadsheet is in result.Tables
				}
			}
		}

		//Installer
		public JsonResult UploadLogo()
		{
			try
			{
				//Check input
				if (Request.Files.Count <= 0 || Request.Files[0] == null)
				{
					throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
				}

				var file = Request.Files[0];

				if (file.ContentLength > 1048576) //1MB.
				{
					throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
				}

				//Check file type & format
				var fileImage = Image.FromStream(file.InputStream);
				var acceptedFormats = new List<ImageFormat>
				{
					ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif
				};

				if (!acceptedFormats.Contains(fileImage.RawFormat))
				{
					throw new ApplicationException("Uploaded file is not an accepted image file !");
				}

				//Save new picture
				var fileInfo = new FileInfo(file.FileName);
				var tempFileName = "InstallerLogo_" + DateTime.Now.Ticks + fileInfo.Extension;
				var tempFilePath = Path.Combine(_appFolders.TempFileDownloadFolder, tempFileName);
				file.SaveAs(tempFilePath);

				using (var bmpImage = new Bitmap(tempFilePath))
				{
					return Json(new AjaxResponse(new { fileName = tempFileName, width = bmpImage.Width, height = bmpImage.Height }));
				}
			}
			catch (UserFriendlyException ex)
			{
				return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
			}
		}

		public JsonResult UploadAcc()
		{
			try
			{
				//Check input
				if (Request.Files.Count <= 0 || Request.Files[0] == null)
				{
					throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
				}

				var file = Request.Files[0];

				if (file.ContentLength > 1048576) //1MB.
				{
					throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
				}

				//Check file type & format
				var fileImage = Image.FromStream(file.InputStream);
				var acceptedFormats = new List<ImageFormat>
				{
					ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif
				};

				if (!acceptedFormats.Contains(fileImage.RawFormat))
				{
					throw new ApplicationException("Uploaded file is not an accepted image file !");
				}

				//Save new picture
				var fileInfo = new FileInfo(file.FileName);
				var tempFileName = "InstallerAccreditationPhoto_" + DateTime.Now.Ticks + fileInfo.Extension;
				var tempFilePath = Path.Combine(_appFolders.TempFileDownloadFolder, tempFileName);
				file.SaveAs(tempFilePath);

				using (var bmpImage = new Bitmap(tempFilePath))
				{
					return Json(new AjaxResponse(new { fileName = tempFileName, width = bmpImage.Width, height = bmpImage.Height }));
				}
			}
			catch (UserFriendlyException ex)
			{
				return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
			}
		}

		public JsonResult UploadLicence()
		{
			try
			{
				//Check input
				if (Request.Files.Count <= 0 || Request.Files[0] == null)
				{
					throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
				}

				var file = Request.Files[0];

				if (file.ContentLength > 1048576) //1MB.
				{
					throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
				}

				//Check file type & format
				var fileImage = Image.FromStream(file.InputStream);
				var acceptedFormats = new List<ImageFormat>
				{
					ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif
				};

				if (!acceptedFormats.Contains(fileImage.RawFormat))
				{
					throw new ApplicationException("Uploaded file is not an accepted image file !");
				}

				//Save new picture
				var fileInfo = new FileInfo(file.FileName);
				var tempFileName = "InstallerLicence_" + DateTime.Now.Ticks + fileInfo.Extension;
				var tempFilePath = Path.Combine(_appFolders.TempFileDownloadFolder, tempFileName);
				file.SaveAs(tempFilePath);

				using (var bmpImage = new Bitmap(tempFilePath))
				{
					return Json(new AjaxResponse(new { fileName = tempFileName, width = bmpImage.Width, height = bmpImage.Height }));
				}
			}
			catch (UserFriendlyException ex)
			{
				return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
			}
		}

		//Job Documents
		public JsonResult UploadDocument(Guid Id)
		{
			try
			{
				//Check input
				if (Request.Files.Count <= 0 || Request.Files[0] == null)
				{
					throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
				}

				var file = Request.Files[0];

				if (file.ContentLength > 5242880) //5MB.
				{
					throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
				}

				////Check file type & format
				//var fileImage = Image.FromStream(file.InputStream);
				//var acceptedFormats = new List<ImageFormat>
				//{
				//    ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif
				//};

				//if (!acceptedFormats.Contains(fileImage.RawFormat))
				//{
				//    throw new ApplicationException("Uploaded file is not an accepted image file !");
				//}

				//Save new picture
				var fileInfo = new FileInfo(file.FileName);
				var FileName = "Document_" + DateTime.Now.Ticks + fileInfo.Extension;
				var TenantName = _jobsAppService.GetTenantName();
				var FormId = _jobsAppService.GetFormId(Id);
				string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
				if (System.IO.Directory.Exists(Path))
				{
					string FormIdPath = Path + "\\" + FormId;
					if (System.IO.Directory.Exists(FormIdPath))
					{
						string FinalPath = FormIdPath + "\\Photos\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(FormIdPath);
						string FinalPath = FormIdPath + "\\Photos\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
				}
				else
				{
					System.IO.Directory.CreateDirectory(Path);
					string FormIdPath = Path + "\\" + FormId;
					if (System.IO.Directory.Exists(FormIdPath))
					{
						string FinalPath = FormIdPath + "\\Photos\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(FormIdPath);
						string FinalPath = FormIdPath + "\\Photos\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
				}

				//string fileName = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Photos\\" + FileName;
				//file.SaveAs(fileName);


				//var tempFileName = "Document_" + DateTime.Now.Ticks + fileInfo.Extension;
				//var tempFilePath = Path.Combine(_appFolders.TempFileDownloadFolder, tempFileName);
				//file.SaveAs(tempFilePath);


				return Json(new AjaxResponse(new { fileName = FileName }));
			}
			catch (UserFriendlyException ex)
			{
				return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
			}
		}

		//Video Master
		public JsonResult UploadVideo()
		{
			try
			{
				//Check input
				if (Request.Files.Count <= 0 || Request.Files[0] == null)
				{
					throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
				}

				var file = Request.Files[0];

				//Save new picture
				var fileInfo = new FileInfo(file.FileName);
				var FileName = "Video_" + DateTime.Now.Ticks + fileInfo.Extension;
				string fileName = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Video\\" + FileName;
				file.SaveAs(fileName);

				return Json(new AjaxResponse(new { fileName = FileName }));
			}
			catch (UserFriendlyException ex)
			{
				return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
			}
		}

		//Job Audio
		public JsonResult UploadAudio(Guid Id)
		{
			try
			{
				//Check input
				if (Request.Files.Count <= 0 || Request.Files[0] == null)
				{
					throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
				}

				var file = Request.Files[0];

				//Save new picture
				var fileInfo = new FileInfo(file.FileName);
				var FileName = "Audio_" + DateTime.Now.Ticks + fileInfo.Extension;
				//string fileName = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\Audio\\" + FileName;
				//file.SaveAs(fileName);
				var TenantName = _jobsAppService.GetTenantName();
				var FormId = _jobsAppService.GetFormId(Id);
				string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
				if (System.IO.Directory.Exists(Path))
				{
					string FormIdPath = Path + "\\" + FormId;
					if (System.IO.Directory.Exists(FormIdPath))
					{
						string FinalPath = FormIdPath + "\\Audio\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(FormIdPath);
						string FinalPath = FormIdPath + "\\Audio\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
				}
				else
				{
					System.IO.Directory.CreateDirectory(Path);
					string FormIdPath = Path + "\\" + FormId;
					if (System.IO.Directory.Exists(FormIdPath))
					{
						string FinalPath = FormIdPath + "\\Audio\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(FormIdPath);
						string FinalPath = FormIdPath + "\\Audio\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
				}

				return Json(new AjaxResponse(new { displayFileName = file.FileName, fileName = FileName }));
			}
			catch (UserFriendlyException ex)
			{
				return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
			}
		}

		//Panel Master Logo
		public JsonResult PanelLogo()
		{
			try
			{
				//Check input
				if (Request.Files.Count <= 0 || Request.Files[0] == null)
				{
					throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
				}

				var file = Request.Files[0];

				if (file.ContentLength > 1048576) //1MB.
				{
					throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
				}

				//Check file type & format
				var fileImage = Image.FromStream(file.InputStream);
				var acceptedFormats = new List<ImageFormat>
				{
					ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif
				};

				if (!acceptedFormats.Contains(fileImage.RawFormat))
				{
					throw new ApplicationException("Uploaded file is not an accepted image file !");
				}

				//Save new picture
				var fileInfo = new FileInfo(file.FileName);
				var tempFileName = "PanelLogo_" + DateTime.Now.Ticks + fileInfo.Extension;
				var tempFilePath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\PanelLogo\\" + tempFileName;
				//var tempFilePath = Path.Combine(_appFolders.TempFileDownloadFolder, tempFileName);
				file.SaveAs(tempFilePath);

				using (var bmpImage = new Bitmap(tempFilePath))
				{
					return Json(new AjaxResponse(new { fileName = tempFileName, width = bmpImage.Width, height = bmpImage.Height }));
				}
			}
			catch (UserFriendlyException ex)
			{
				return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
			}
		}

		//Inverter Master Logo
		public JsonResult InverterLogo()
		{
			try
			{
				//Check input
				if (Request.Files.Count <= 0 || Request.Files[0] == null)
				{
					throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
				}

				var file = Request.Files[0];

				if (file.ContentLength > 1048576) //1MB.
				{
					throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
				}

				//Check file type & format
				var fileImage = Image.FromStream(file.InputStream);
				var acceptedFormats = new List<ImageFormat>
				{
					ImageFormat.Jpeg, ImageFormat.Png, ImageFormat.Gif
				};

				if (!acceptedFormats.Contains(fileImage.RawFormat))
				{
					throw new ApplicationException("Uploaded file is not an accepted image file !");
				}

				//Save new picture
				var fileInfo = new FileInfo(file.FileName);
				var tempFileName = "InverterLogo_" + DateTime.Now.Ticks + fileInfo.Extension;
				var tempFilePath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\InverterLogo\\" + tempFileName;
				//var tempFilePath = Path.Combine(_appFolders.TempFileDownloadFolder, tempFileName);
				file.SaveAs(tempFilePath);

				using (var bmpImage = new Bitmap(tempFilePath))
				{
					return Json(new AjaxResponse(new { fileName = tempFileName, width = bmpImage.Width, height = bmpImage.Height }));
				}
			}
			catch (UserFriendlyException ex)
			{
				return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
			}
		}

		//Job Panel
		public JsonResult UploadScanedPanelImages(int Id)
		{
			try
			{
				//Check input
				if (Request.Files.Count <= 0 || Request.Files[0] == null)
				{
					throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
				}

				var file = Request.Files[0];

				if (file.ContentLength > 5242880) //5MB.
				{
					throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
				}

				//Save new picture
				var fileInfo = new FileInfo(file.FileName);
				var FileName = "ScanedPanel_" + DateTime.Now.Ticks + fileInfo.Extension;
				var TenantName = _jobsAppService.GetTenantName();
				var FormId = _jobsAppService.GetFormIdFromId(Id);
				string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
				if (System.IO.Directory.Exists(Path))
				{
					string FormIdPath = Path + "\\" + FormId;
					if (System.IO.Directory.Exists(FormIdPath))
					{
						string FinalPath = FormIdPath + "\\ScanedPanels\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(FormIdPath);
						string FinalPath = FormIdPath + "\\ScanedPanels\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
				}
				else
				{
					System.IO.Directory.CreateDirectory(Path);
					string FormIdPath = Path + "\\" + FormId;
					if (System.IO.Directory.Exists(FormIdPath))
					{
						string FinalPath = FormIdPath + "\\ScanedPanels\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(FormIdPath);
						string FinalPath = FormIdPath + "\\ScanedPanels\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
				}
				////Save new picture
				//var fileInfo = new FileInfo(file.FileName);
				//var FileName = "ScanedPanel_" + DateTime.Now.Ticks + fileInfo.Extension;
				//string fileName = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\ScanedPanels\\" + FileName;
				//file.SaveAs(fileName);

				return Json(new AjaxResponse(new { fileName = FileName }));
			}
			catch (UserFriendlyException ex)
			{
				return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
			}
		}

		//Job Inverter
		public JsonResult UploadScanedInverterImages(int Id)
		{
			try
			{
				//Check input
				if (Request.Files.Count <= 0 || Request.Files[0] == null)
				{
					throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
				}

				var file = Request.Files[0];
				if (file.ContentLength > 5242880) //5MB.
				{
					throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
				}

				//Save new picture
				var fileInfo = new FileInfo(file.FileName);
				var FileName = "ScanedInverter_" + DateTime.Now.Ticks + fileInfo.Extension;
				var TenantName = _jobsAppService.GetTenantName();
				var FormId = _jobsAppService.GetFormIdFromId(Id);
				string Path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\" + TenantName;
				if (System.IO.Directory.Exists(Path))
				{
					string FormIdPath = Path + "\\" + FormId;
					if (System.IO.Directory.Exists(FormIdPath))
					{
						string FinalPath = FormIdPath + "\\ScanedInverters\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(FormIdPath);
						string FinalPath = FormIdPath + "\\ScanedInverters\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
				}
				else
				{
					System.IO.Directory.CreateDirectory(Path);
					string FormIdPath = Path + "\\" + FormId;
					if (System.IO.Directory.Exists(FormIdPath))
					{
						string FinalPath = FormIdPath + "\\ScanedInverters\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
					else
					{
						System.IO.Directory.CreateDirectory(FormIdPath);
						string FinalPath = FormIdPath + "\\ScanedInverters\\";
						if (System.IO.Directory.Exists(FinalPath))
						{
							file.SaveAs(FinalPath + FileName);
						}
						else
						{
							System.IO.Directory.CreateDirectory(FinalPath);
							file.SaveAs(FinalPath + FileName);
						}
					}
				}
				////Save new picture
				//var fileInfo = new FileInfo(file.FileName);
				//var FileName = "ScanedInverter_" + DateTime.Now.Ticks + fileInfo.Extension;
				//string fileName = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\ScanedInverters\\" + FileName;
				//file.SaveAs(fileName);

				return Json(new AjaxResponse(new { fileName = FileName }));
			}
			catch (UserFriendlyException ex)
			{
				return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
			}
		}
	}
}