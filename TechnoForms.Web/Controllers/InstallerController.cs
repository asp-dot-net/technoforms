﻿using Abp.Domain.Repositories;
using Abp.Domain.Uow;
using Abp.IO.Extensions;
using Abp.UI;
using Abp.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TechnoForms.InstallerEmployes;
using TechnoForms.Installers;
using TechnoForms.Installers.Dto;
using TechnoForms.Storage;

namespace TechnoForms.Web.Controllers
{
    public class InstallerController : Controller
    {
        private readonly IRepository<Installer> _InstallerRepository;
        private readonly IRepository<InstallerEmploye> _InstallerEmployeRepository;
        // GET: Installer
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        [UnitOfWork]
        public virtual async Task<JsonResult> CreateInstaller(CreateInstallerDto input)
        {
            //try
            //{
            //    var Installer = new Installer();//input.MapTo<Installer>();
            //    Installer.FullName = input.FullName;
            //    Installer.Phone = input.Phone;
            //    Installer.Mobile = input.Mobile;
            //    Installer.EmailId = input.EmailId;
            //    Installer.CompanyName = input.CompanyName;
            //    Installer.ABN = input.ABN;
            //    Installer.CompanyPhone = input.CompanyPhone;
            //    Installer.Fax = input.Fax;
            //    Installer.AddressType = input.AddressType;
            //    Installer.PostalDelNo = input.PostalDelNo;
            //    Installer.PostalDelType = input.PostalDelType;
            //    Installer.IsGST = input.IsGST;
            //    Installer.Logo = input.Logo;
            //    Installer.StreetAddress = input.StreetAddress;
            //    Installer.UnitNo = input.UnitNo;
            //    Installer.UnitType = input.UnitType;
            //    Installer.StreetNumber = input.StreetNumber;
            //    Installer.StreetName = input.StreetName;
            //    Installer.StreetType = input.StreetType;
            //    Installer.Suburb = input.Suburb;
            //    Installer.State = input.State;
            //    Installer.PostCode = input.PostCode;
            //    Installer.IsInst = input.IsInst;
            //    Installer.InstallerExpiry = input.InstallerExpiry;
            //    Installer.InsAccreExDate = input.InsAccreExDate;
            //    Installer.IsElec = input.IsElec;
            //    Installer.ElectricalExpiry = input.ElectricalExpiry;
            //    Installer.EleLicenceExDate = input.EleLicenceExDate;
            //    Installer.IsDesc = input.IsDesc;
            //    Installer.DesignerExpiry = input.DesignerExpiry;
            //    Installer.DesignerAccreExDate = input.DesignerAccreExDate;
            //    Installer.AccreditationAccPhoto = input.AccreditationAccPhoto;
            //    Installer.LicencePhoto = input.LicencePhoto;
            //    Installer.Guid = System.Guid.NewGuid();
            //    Installer.UserId = 1;
            //    await _InstallerRepository.InsertAndGetIdAsync(Installer);

            //    var InstallerEmployee = new InstallerEmploye();
            //    InstallerEmployee.FullName = input.FullName;
            //    InstallerEmployee.Phone = input.Phone;
            //    InstallerEmployee.Mobile = input.Mobile;
            //    InstallerEmployee.EmailId = input.EmailId;
            //    InstallerEmployee.Fax = input.Fax;
            //    InstallerEmployee.StreetAddress = input.StreetAddress;
            //    InstallerEmployee.UnitNo = input.UnitNo;
            //    InstallerEmployee.UnitType = input.UnitType;
            //    InstallerEmployee.StreetNumber = input.StreetNumber;
            //    InstallerEmployee.StreetName = input.StreetName;
            //    InstallerEmployee.StreetType = input.StreetType;
            //    InstallerEmployee.Suburb = input.Suburb;
            //    InstallerEmployee.State = input.State;
            //    InstallerEmployee.PostCode = input.PostCode;
            //    InstallerEmployee.InstallerExpiry = input.InstallerExpiry;
            //    InstallerEmployee.InsAccreExDate = input.InsAccreExDate;
            //    InstallerEmployee.ElectricalExpiry = input.ElectricalExpiry;
            //    InstallerEmployee.EleLicenceExDate = input.EleLicenceExDate;
            //    InstallerEmployee.DesignerExpiry = input.DesignerExpiry;
            //    InstallerEmployee.DesignerAccreExDate = input.DesignerAccreExDate;
            //    InstallerEmployee.InstallerId = Installer.Id;
            //    InstallerEmployee.AddressType = input.AddressType;
            //    InstallerEmployee.PostalDelNo = input.PostalDelNo;
            //    InstallerEmployee.PostalDelType = input.PostalDelType;
            //    InstallerEmployee.IsInst = input.IsInst;
            //    InstallerEmployee.IsElec = input.IsElec;
            //    InstallerEmployee.IsDesc = input.IsDesc;
            //    await _InstallerEmployeRepository.InsertAsync(InstallerEmployee);


                return Json(new AjaxResponse());
            //}
            //catch (Exception e)
            //{
            //    return Json(new AjaxResponse(new ErrorInfo(e.Message)));
            //}
            

            //try
            //{
            //    //Check input
            //    if (Request.Files.Count <= 0 || Request.Files[0] == null)
            //    {
            //        throw new UserFriendlyException(L("ProfilePicture_Change_Error"));
            //    }

            //    var file = Request.Files[0];

            //    if (file.ContentLength > 30720) //30KB.
            //    {
            //        throw new UserFriendlyException(L("ProfilePicture_Warn_SizeLimit"));
            //    }

            //    //Get user
            //    var user = await _userManager.GetUserByIdAsync(AbpSession.GetUserId());

            //    //Delete old picture
            //    if (user.ProfilePictureId.HasValue)
            //    {
            //        await _binaryObjectManager.DeleteAsync(user.ProfilePictureId.Value);
            //    }

            //    //Save new picture
            //    var storedFile = new BinaryObject(AbpSession.TenantId, file.InputStream.GetAllBytes());
            //    await _binaryObjectManager.SaveAsync(storedFile);

            //    //Update new picture on the user
            //    user.ProfilePictureId = storedFile.Id;

            //    //Return success
            
            //}
            //catch (UserFriendlyException ex)
            //{
            //    //Return error message
            //    return Json(new AjaxResponse(new ErrorInfo(ex.Message)));
            //}
        }
    }
}