﻿using System.Web.Mvc;
using Abp.Auditing;

namespace TechnoForms.Web.Controllers
{
    public class ErrorController : TechnoFormsControllerBase
    {
        [DisableAuditing]
        public ActionResult E404()
        {
            return View();
        }
    }
}