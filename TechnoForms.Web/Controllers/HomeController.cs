﻿using Abp.Domain.Repositories;
using System.Linq;
using System.Web.Mvc;
using TechnoForms.Inverters;
using TechnoForms.Panels;
using TechnoForms.PostCodes;
using TechnoForms.States;
using TechnoForms.StreetName;
using TechnoForms.StreetTypes;
using TechnoForms.UnitTypes;

namespace TechnoForms.Web.Controllers
{
	public class HomeController : TechnoFormsControllerBase
	{
		private readonly IRepository<UnitType> _unittypeRepository;
		private readonly IRepository<StreetNames> _streetNamesRepository;
		private readonly IRepository<StreetType> _streetTypeRepository;
		private readonly IRepository<PostCode> _postCodeRepository;
		private readonly IRepository<State> _stateRepository;
		private readonly IRepository<Panel> _scanedPanelRepository;
		private readonly IRepository<Inverter> _scanedInverterRepository;

		public HomeController(IRepository<UnitType> unittypeRepository
			, IRepository<StreetNames> streetNamesRepository
			, IRepository<StreetType> streetTypeRepository
			, IRepository<PostCode> postCodeRepository
			, IRepository<State> stateRepository
			, IRepository<Panel> scanedPanelRepository
			, IRepository<Inverter> scanedInverterRepository)
		{
			_unittypeRepository = unittypeRepository;
			_streetNamesRepository = streetNamesRepository;
			_streetTypeRepository = streetTypeRepository;
			_postCodeRepository = postCodeRepository;
			_stateRepository = stateRepository;
			_scanedPanelRepository = scanedPanelRepository;
			_scanedInverterRepository = scanedInverterRepository;
		}

		public ActionResult Index()
		{
			return View();
		}

		[HttpPost]
		public JsonResult GetUnitType()
		{
			var unittype = _unittypeRepository.GetAll().ToList();

			return Json((from c in unittype
						 select new { UnitTypeName = c.UnitTypeName }).ToList());
		}

		[HttpPost]
		public JsonResult GetStreetType()
		{
			var streetType = _streetTypeRepository.GetAll().ToList();

			return Json((from c in streetType
						 select new { StreetTypeName = c.StreetTypeName }).ToList());
		}

		[HttpPost]
		public JsonResult GetStreetName()
		{
			var streetName = _streetNamesRepository.GetAll().ToList();

			return Json((from c in streetName
						 select new { StreetName = c.StreetName }).ToList());
		}

		[HttpPost]
		public JsonResult GetSuburb()
		{
			var postcode = _postCodeRepository.GetAll().ToList();

			foreach (var item in postcode)
			{
				item.Suburb = item.Suburb + " | " + _stateRepository.GetAll().Where(p => p.Id == item.StateId).Select(p => p.StateName).FirstOrDefault() + " | " + item.Postcode;
			}

			return Json((from c in postcode
						 select new { Suburb = c.Suburb }).ToList());
		}

		[HttpPost]
		public JsonResult GetPanel()
		{
			var panel = _scanedPanelRepository.GetAll().Distinct().ToList();

			return Json((from c in panel
						 select new { LicenceOrCertiHolder = c.LicenceOrCertiHolder }).ToList());
		}

		[HttpPost]
		public JsonResult GetInverter()
		{
			var inverter = _scanedInverterRepository.GetAll().ToList();

			return Json((from c in inverter
						 select new { ManufacturarName = c.ManufacturarName }).ToList());
		}
	}
}