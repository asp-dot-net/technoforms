﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Abp.Application.Services.Dto;
using Abp.Auditing;
using Abp.Domain.Repositories;
using Abp.UI;
using Abp.Web.Mvc.Authorization;
using ExcelDataReader;
using TechnoForms.Dto;
using TechnoForms.Inverters;
using TechnoForms.Jobs;
using TechnoForms.Jobs.Dto;
using TechnoForms.Panels;

namespace TechnoForms.Web.Controllers
{
    public class BulkDataUploadController : Controller
    {
        private readonly IAppFolders _appFolders;
        private readonly IPanelAppService _panelRepository;
        private readonly IInverterAppService _inverterRepository;
        private readonly IRepository<Job> _jobRepository;
        private readonly JobsAppService _JobsAppRepository;
        private readonly IRepository<Panel> _paneltabRepository;
        private readonly IRepository<Inverter> _invertertabRepository;
        private readonly IRepository<RecYear> _recyearRepository;

        public BulkDataUploadController(IAppFolders appFolders, IPanelAppService panelRepository,
            IInverterAppService inverterRepository, IRepository<Job> jobRepository, JobsAppService JobsAppRepository,
            IRepository<Panel> paneltabRepository,
            IRepository<Inverter> invertertabRepository,
            IRepository<RecYear> recyearRepository)
        {
            _appFolders = appFolders;
            _panelRepository = panelRepository;
            _inverterRepository = inverterRepository;
            _jobRepository = jobRepository;
            _JobsAppRepository = JobsAppRepository;
            _paneltabRepository = paneltabRepository;
            _invertertabRepository = invertertabRepository;
            _recyearRepository = recyearRepository;
        }

        public ActionResult PanelData(HttpPostedFileBase file)
        {
            if (file != null)
            {
                string fileName = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\BulkUploadFIles\\" + "Panel_" + DateTime.Now.Ticks.ToString() + ".xlsx"; //System.Configuration.ConfigurationManager.AppSettings["FileToImport"].ToString();
                file.SaveAs(fileName);
                DataTable dt = new DataTable();
                DataSet ds = GetDataFromExcel(fileName);
                dt = ds.Tables[0];

                try
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
						try
						{
							string LicenceOrCertiHolder = Convert.ToString(dt.Rows[j]["Licensee/Certificate Holder"]);
							string ModelNo = Convert.ToString(dt.Rows[j]["Model Number"]);
							DateTime CECApprovedDate = Convert.ToDateTime(dt.Rows[j]["CEC Approved Date"]);
							DateTime ExpiryDate = Convert.ToDateTime(dt.Rows[j]["Expiry Date"]);
							string FireTested = Convert.ToString(dt.Rows[j]["Fire Tested"]);

							//var PanelCheckExistDto = new Panels.Dto.PanelCheckExistDto();
							//PanelCheckExistDto.ModelNo = ModelNo;
							//var checkexist = _panelRepository.CheckPanelExists(PanelCheckExistDto);
							//if (!checkexist.Status)
							//{
							//	_panelRepository.UpdatePanel(new Panels.Dto.PanelEditDto
							//	{
							//		Id = checkexist.id,
							//		LicenceOrCertiHolder = LicenceOrCertiHolder,
							//		ModelNo = ModelNo,
							//		CECApprovedDate = CECApprovedDate,
							//		ExpiryDate = ExpiryDate,
							//		Size = 0,
							//		FireTested = FireTested
							//	});
							//}
							//else
							//{
								_panelRepository.CreatePanel(new Panels.Dto.CreatePanelDto
								{
									LicenceOrCertiHolder = LicenceOrCertiHolder,
									ModelNo = ModelNo,
									CECApprovedDate = CECApprovedDate,
									ExpiryDate = ExpiryDate,
									Size = 0,
									FireTested = FireTested
								});
							//}
						}
						catch(Exception ex)
						{

						}                        
                    }

					_panelRepository.CreatePanelBulkUpload(new Panels.Dto.CreatePanelBulkUploadDto
					{
						PanelFileName = fileName
					});
                }
                catch (Exception e) { }
                return Json(new { success = true });
            }
            else
            {
                return Json(new { success = false });
            }
        }

		public ActionResult PanelDataUpdate(HttpPostedFileBase file)
		{
			if (file != null)
			{
				string fileName = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\BulkUploadFIles\\" + "Panel_" + DateTime.Now.Ticks.ToString() + ".xlsx"; //System.Configuration.ConfigurationManager.AppSettings["FileToImport"].ToString();
				file.SaveAs(fileName);
				DataTable dt = new DataTable();
				DataSet ds = GetDataFromExcel(fileName);
				dt = ds.Tables[0];

				try
				{
					for (int j = 0; j < dt.Rows.Count; j++)
					{
						try
						{
							string LicenceOrCertiHolder = Convert.ToString(dt.Rows[j]["Licensee/Certificate Holder"]);
							string ModelNo = Convert.ToString(dt.Rows[j]["Model Number"]);
							DateTime CECApprovedDate = Convert.ToDateTime(dt.Rows[j]["CEC Approved Date"]);
							DateTime ExpiryDate = Convert.ToDateTime(dt.Rows[j]["Expiry Date"]);
							string FireTested = Convert.ToString(dt.Rows[j]["Fire Tested"]);

							string ResponsibleSupplier = Convert.ToString(dt.Rows[j]["Application: Importer/Responsible Supplier"]);
							string ResponsibleSupplierABN = Convert.ToString(dt.Rows[j]["Application: Importer/Responsible Supplier - ABN"]);
							string ResponsibleSupplier2 = Convert.ToString(dt.Rows[j]["Application: Importer/Responsible Supplier 2"]);
							string ResponsibleSupplierABN2 = Convert.ToString(dt.Rows[j]["Application: Importer/Responsible Supplier 2 - ABN"]);
							string ResponsibleSupplier3 = Convert.ToString(dt.Rows[j]["Application: Importer/Responsible Supplier 3"]);
							string ResponsibleSupplierABN3 = Convert.ToString(dt.Rows[j]["Application: Importer/Responsible Supplier 3 - ABN"]);

							var PanelCheckExistDto = new Panels.Dto.PanelCheckExistDto();
							PanelCheckExistDto.ModelNo = ModelNo;
							var checkexist = _panelRepository.CheckPanelExists(PanelCheckExistDto);
							if (!checkexist.Status)
							{
								_panelRepository.UpdatePanel(new Panels.Dto.PanelEditDto
								{
									Id = checkexist.id,
									LicenceOrCertiHolder = LicenceOrCertiHolder,
									ModelNo = ModelNo,
									CECApprovedDate = CECApprovedDate,
									ExpiryDate = ExpiryDate,
									Size = 0,
									FireTested = FireTested,
									ResponsibleSupplier = ResponsibleSupplier,
									ResponsibleSupplierABN = ResponsibleSupplierABN,
									ResponsibleSupplier2 = ResponsibleSupplier2,
									ResponsibleSupplierABN2 = ResponsibleSupplierABN2,
									ResponsibleSupplier3 = ResponsibleSupplier3,
									ResponsibleSupplierABN3 = ResponsibleSupplierABN3
								});
							}
							else
							{
								_panelRepository.CreatePanel(new Panels.Dto.CreatePanelDto
								{
									LicenceOrCertiHolder = LicenceOrCertiHolder,
									ModelNo = ModelNo,
									CECApprovedDate = CECApprovedDate,
									ExpiryDate = ExpiryDate,
									Size = 0,
									FireTested = FireTested,
									ResponsibleSupplier = ResponsibleSupplier,
									ResponsibleSupplierABN = ResponsibleSupplierABN,
									ResponsibleSupplier2 = ResponsibleSupplier2,
									ResponsibleSupplierABN2 = ResponsibleSupplierABN2,
									ResponsibleSupplier3 = ResponsibleSupplier3,
									ResponsibleSupplierABN3 = ResponsibleSupplierABN3
								});
							}
						}
						catch(Exception ex)
						{

						}						
					}

					_panelRepository.CreatePanelBulkUpload(new Panels.Dto.CreatePanelBulkUploadDto
					{
						PanelFileName = fileName
					});
				}
				catch (Exception e) { }
				return Json(new { success = true });
			}
			else
			{
				return Json(new { success = false });
			}
		}

        public ActionResult UpdatePanelSize(HttpPostedFileBase file)
        {
            if (file != null)
            {
                string fileName = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\BulkUploadFIles\\" + "Panel_" + DateTime.Now.Ticks.ToString() + ".xlsx"; //System.Configuration.ConfigurationManager.AppSettings["FileToImport"].ToString();
                file.SaveAs(fileName);
                DataTable dt = new DataTable();
                DataSet ds = GetDataFromExcel(fileName);
                dt = ds.Tables[0];

                try
                {
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        try
                        {
                            string ModelNumber = Convert.ToString(dt.Rows[j]["ModelNumber"]);
                            decimal Size = Convert.ToDecimal(dt.Rows[j]["Size"]);
                            _panelRepository.UpdatePanelSize(new Panels.Dto.CreatePanelDto
                            {
                                LicenceOrCertiHolder = "Test",
                                FireTested = "Test",
                                CECApprovedDate = DateTime.Now,
                                ExpiryDate = DateTime.Now,
                                ModelNo = ModelNumber,
                                Size = Size
                            });
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                catch (Exception e) { }
                return Json(new { success = true });
            }
            else
            {
                return Json(new { success = false });
            }
        }

        public async System.Threading.Tasks.Task<ActionResult> JobDataAsync(HttpPostedFileBase file)
        {
            string fname = "", FileName = "";
            string Status = "";

            if (file != null)
            {
                string fileName = System.Configuration.ConfigurationManager.AppSettings["FileToImport"].ToString();
                file.SaveAs(fileName);

				await _JobsAppRepository.CreateJobBulkUpload(new Jobs.Dto.CreateJobBulkUploadDto
				{
					JobFileName = fileName
				});

				DataTable dt = new DataTable();
                DataSet ds = GetDataFromExcel(fileName);
                dt = ds.Tables[0];

                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    string ProjectNo = dt.Rows[j]["Project Number"].ToString();

                    var JobId = _jobRepository.GetAll().Where(x => x.ProjectNumber == ProjectNo).Select(x => x.Id).FirstOrDefault();
                    if (JobId == 0)
                    {
                        CreateJobsInput PVForm = new CreateJobsInput();
                        //PVForm.Customer_Id = Convert.ToInt32(dt.Rows[j]["Quickcustid"]);
                        PVForm.SystemType_Id = Convert.ToInt32(dt.Rows[j]["System Type"].ToString());
                        PVForm.FirstName = dt.Rows[j]["First Name"].ToString();
                        PVForm.LastName = dt.Rows[j]["Last Name"].ToString();
                        PVForm.EmailId = dt.Rows[j]["Email"].ToString();
                        PVForm.Phone = "07" + dt.Rows[j]["Mobile"].ToString();
                        PVForm.UnitNo = dt.Rows[j]["UnitNo"].ToString();
                        PVForm.UnitType = dt.Rows[j]["UNitType"].ToString();
                        PVForm.StreetNo = dt.Rows[j]["StreetNo"].ToString();
                        PVForm.StreetName = dt.Rows[j]["StreetName"].ToString();
                        PVForm.StreetType = dt.Rows[j]["StreetType"].ToString();
                        PVForm.StreetCity = dt.Rows[j]["City"].ToString();
                        PVForm.StreetState = dt.Rows[j]["State"].ToString();
                        PVForm.StreetPostCode = dt.Rows[j]["PostCode"].ToString();

                        if (!string.IsNullOrEmpty(PVForm.UnitType))
                        {
                            PVForm.StreetAddress += PVForm.UnitType + " ";
                        }
                        if (!string.IsNullOrEmpty(PVForm.UnitNo))
                        {
                            PVForm.StreetAddress += PVForm.UnitNo + " ";
                        }

                        PVForm.StreetAddress = PVForm.StreetNo + " " + PVForm.StreetName + " " + PVForm.StreetType;
                        PVForm.InstallationStreetAddress = PVForm.StreetAddress;
                        PVForm.installationAddresssameasHouseAddress = true;
                        PVForm.InstallationUnitNo = dt.Rows[j]["UnitNo"].ToString();
                        PVForm.InstallationUnitType = dt.Rows[j]["UNitType"].ToString();
                        PVForm.InstallationStreetNo = dt.Rows[j]["StreetNo"].ToString();
                        PVForm.InstallationStreetName = dt.Rows[j]["StreetName"].ToString();
                        PVForm.InstallationStreetType = dt.Rows[j]["StreetType"].ToString();
                        PVForm.InstallationStreetCity = dt.Rows[j]["City"].ToString();
                        PVForm.InstallationStreetState = dt.Rows[j]["State"].ToString();
                        PVForm.InstallationStreetPostCode = dt.Rows[j]["PostCode"].ToString();
                        if (!string.IsNullOrEmpty(dt.Rows[j]["InstallerID"].ToString()))
                        {
                            PVForm.CreateFor_Id = Convert.ToInt32(dt.Rows[j]["InstallerID"].ToString());
                        }
                        PVForm.Installer_Id = Convert.ToInt32(dt.Rows[j]["InstallerID"].ToString());
                        PVForm.Electrician_Id = Convert.ToInt32(dt.Rows[j]["InstallerID"].ToString());
                        PVForm.Designer_Id = Convert.ToInt32(dt.Rows[j]["InstallerID"].ToString());
                        PVForm.NMINo = dt.Rows[j]["NMI NUMBER"].ToString();
                        PVForm.RegPlanNo = dt.Rows[j]["RegPlan Number"].ToString();
                        PVForm.LotNo = dt.Rows[j]["Lot Number"].ToString();
                        PVForm.HouseType = Convert.ToInt32(dt.Rows[j]["HouseTypeID"]);
                        PVForm.RoofType = Convert.ToInt32(dt.Rows[j]["RoofTypeID"]);

                        PVForm.Panel_id = Convert.ToInt32(dt.Rows[j]["PanelItemID"]);
                        decimal panelsize = 0;
                        if (!string.IsNullOrEmpty(PVForm.Panel_id.ToString()))
                        {
                            PVForm.PanelName = _paneltabRepository.GetAll().Where(p => p.Id == PVForm.Panel_id).Select(p => p.LicenceOrCertiHolder).FirstOrDefault();
                            panelsize = _paneltabRepository.GetAll().Where(p => p.Id == PVForm.Panel_id).Select(p => p.Size).FirstOrDefault();
                        }

                        //PVForm.PanelName = "AE Alternative Energy GmbH";

                        PVForm.NoOfPanels = Convert.ToInt32(dt.Rows[j]["NumberofPanels"]);
                        PVForm.NoOfInverter = Convert.ToInt32(dt.Rows[j]["NumberofInverters"]);


                        PVForm.Inverter_id = Convert.ToInt32(dt.Rows[j]["InverterItemID"]);
                        // PVForm.InverterName = "(ABB) Power-One Italy S.p.A";

                        if (!string.IsNullOrEmpty(PVForm.Inverter_id.ToString()))
                        {
                            PVForm.InverterName = _invertertabRepository.GetAll().Where(p => p.Id == PVForm.Inverter_id).Select(p => p.ManufacturarName).FirstOrDefault();
                        }

                        if (!string.IsNullOrEmpty(dt.Rows[j]["Installal booking date"].ToString()))
                        {
                            PVForm.ExpectedInstallDate = Convert.ToDateTime(dt.Rows[j]["Installal booking date"]);
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[j]["Project Number"].ToString()))
                        {
                            PVForm.ProjectNumber = dt.Rows[j]["Project Number"].ToString();
                        }
                        else
                        {
                            PVForm.ProjectNumber = "";
                        }
                        PVForm.InstallerNote = dt.Rows[j]["Installer Notes"].ToString();

                        //PVForm.SystemType_Id = 1;
                        //PVForm.NotgridConnectedStatement_Id = 1;
                        //PVForm.Mountingtype_Id = 1;

                        var currentyear = DateTime.Now.Year;
                        var RECYear = _recyearRepository
                            .GetAll()
                            .Where(p => p.Year == currentyear)
                            .OrderByDescending(p => p.Id)
                            .ToList();

                        PVForm.SystemCapacity = (panelsize * PVForm.NoOfPanels / 1000);
                        PVForm.RECYear_Id = RECYear.Select(p => p.Year).FirstOrDefault();//"Twelve years";
                        //PVForm.Defaultresourceavailfigure_Id = 0;
                        //PVForm.ResourceAvailability = "";
                        //PVForm.AccreditationCode = "";
                        //PVForm.ExplanatoryNotes = "";
                        //PVForm.AdditionalSystemInformation = "";
                        //PVForm.OwnerAddressType_Id = 1;
                        PVForm.JobStatus_Id = 1;
                        try
                        {
                           // PVForm.mode = "create";
                            await _JobsAppRepository.CreateJobs(PVForm);
                        }
                        catch (Exception e) { }
                    }
                    else
                    {
                        JobsEditDto PVForm = new JobsEditDto();
                        PVForm.Id = JobId;
                        //PVForm.Customer_Id = Convert.ToInt32(dt.Rows[j]["Quickcustid"]);
                        PVForm.SystemType_Id = Convert.ToInt32(dt.Rows[j]["System Type"].ToString());
                        PVForm.FirstName = dt.Rows[j]["First Name"].ToString();
                        PVForm.LastName = dt.Rows[j]["Last Name"].ToString();
                        PVForm.EmailId = dt.Rows[j]["Email"].ToString();
                        PVForm.Phone = "07" + dt.Rows[j]["Mobile"].ToString();
                        PVForm.UnitNo = dt.Rows[j]["UnitNo"].ToString();
                        PVForm.UnitType = dt.Rows[j]["UNitType"].ToString();
                        PVForm.StreetNo = dt.Rows[j]["StreetNo"].ToString();
                        PVForm.StreetName = dt.Rows[j]["StreetName"].ToString();
                        PVForm.StreetType = dt.Rows[j]["StreetType"].ToString();
                        PVForm.StreetCity = dt.Rows[j]["City"].ToString();
                        PVForm.StreetState = dt.Rows[j]["State"].ToString();
                        PVForm.StreetPostCode = dt.Rows[j]["PostCode"].ToString();

                        if (!string.IsNullOrEmpty(PVForm.UnitType))
                        {
                            PVForm.StreetAddress += PVForm.UnitType + " ";
                        }
                        if (!string.IsNullOrEmpty(PVForm.UnitNo))
                        {
                            PVForm.StreetAddress += PVForm.UnitNo + " ";
                        }

                        PVForm.StreetAddress = PVForm.StreetNo + " " + PVForm.StreetName + " " + PVForm.StreetType;
                        PVForm.InstallationStreetAddress = PVForm.StreetAddress;
                        PVForm.installationAddresssameasHouseAddress = true;
                        PVForm.InstallationUnitNo = dt.Rows[j]["UnitNo"].ToString();
                        PVForm.InstallationUnitType = dt.Rows[j]["UNitType"].ToString();
                        PVForm.InstallationStreetNo = dt.Rows[j]["StreetNo"].ToString();
                        PVForm.InstallationStreetName = dt.Rows[j]["StreetName"].ToString();
                        PVForm.InstallationStreetType = dt.Rows[j]["StreetType"].ToString();
                        PVForm.InstallationStreetCity = dt.Rows[j]["City"].ToString();
                        PVForm.InstallationStreetState = dt.Rows[j]["State"].ToString();
                        PVForm.InstallationStreetPostCode = dt.Rows[j]["PostCode"].ToString();
                        if (!string.IsNullOrEmpty(dt.Rows[j]["InstallerID"].ToString()))
                        {
                            PVForm.CreateFor_Id = Convert.ToInt32(dt.Rows[j]["InstallerID"].ToString());
                        }
                        PVForm.Installer_Id = Convert.ToInt32(dt.Rows[j]["InstallerID"].ToString());
                        PVForm.Electrician_Id = Convert.ToInt32(dt.Rows[j]["InstallerID"].ToString());
                        PVForm.Designer_Id = Convert.ToInt32(dt.Rows[j]["InstallerID"].ToString());
                        PVForm.NMINo = dt.Rows[j]["NMI NUMBER"].ToString();
                        PVForm.RegPlanNo = dt.Rows[j]["RegPlan Number"].ToString();
                        PVForm.LotNo = dt.Rows[j]["Lot Number"].ToString();
                        PVForm.HouseType = Convert.ToInt32(dt.Rows[j]["HouseTypeID"]);
                        PVForm.RoofType = Convert.ToInt32(dt.Rows[j]["RoofTypeID"]);

                        PVForm.Panel_id = Convert.ToInt32(dt.Rows[j]["PanelItemID"]);
                        decimal panelsize = 0;
                        if (!string.IsNullOrEmpty(PVForm.Panel_id.ToString()))
                        {
                            PVForm.PanelName = _paneltabRepository.GetAll().Where(p => p.Id == PVForm.Panel_id).Select(p => p.LicenceOrCertiHolder).FirstOrDefault();
                            panelsize = _paneltabRepository.GetAll().Where(p => p.Id == PVForm.Panel_id).Select(p => p.Size).FirstOrDefault();
                        }

                        //PVForm.PanelName = "AE Alternative Energy GmbH";

                        PVForm.NoOfPanels = Convert.ToInt32(dt.Rows[j]["NumberofPanels"]);
                        PVForm.NoOfInverter = Convert.ToInt32(dt.Rows[j]["NumberofInverters"]);


                        PVForm.Inverter_id = Convert.ToInt32(dt.Rows[j]["InverterItemID"]);
                        // PVForm.InverterName = "(ABB) Power-One Italy S.p.A";

                        if (!string.IsNullOrEmpty(PVForm.Inverter_id.ToString()))
                        {
                            PVForm.InverterName = _invertertabRepository.GetAll().Where(p => p.Id == PVForm.Inverter_id).Select(p => p.ManufacturarName).FirstOrDefault();
                        }

                        if (!string.IsNullOrEmpty(dt.Rows[j]["Installal booking date"].ToString()))
                        {
                            PVForm.ExpectedInstallDate = Convert.ToDateTime(dt.Rows[j]["Installal booking date"]);
                        }
                        if (!string.IsNullOrEmpty(dt.Rows[j]["Project Number"].ToString()))
                        {
                            PVForm.ProjectNumber = dt.Rows[j]["Project Number"].ToString();
                        }
                        else
                        {
                            PVForm.ProjectNumber = "";
                        }
                        PVForm.InstallerNote = dt.Rows[j]["Installer Notes"].ToString();

                        //PVForm.SystemType_Id = 1;
                        //PVForm.NotgridConnectedStatement_Id = 1;
                        //PVForm.Mountingtype_Id = 1;

                        var currentyear = DateTime.Now.Year;
                        var RECYear = _recyearRepository
                            .GetAll()
                            .Where(p => p.Year == currentyear)
                            .OrderByDescending(p => p.Id)
                            .ToList();

                        PVForm.SystemCapacity = (panelsize * PVForm.NoOfPanels / 1000);
                        PVForm.RECYear_Id = RECYear.Select(p => p.Year).FirstOrDefault();//"Twelve years";
                        //PVForm.Defaultresourceavailfigure_Id = 0;
                        //PVForm.ResourceAvailability = "";
                        //PVForm.AccreditationCode = "";
                        //PVForm.ExplanatoryNotes = "";
                        //PVForm.AdditionalSystemInformation = "";
                        //PVForm.OwnerAddressType_Id = 1;
                        PVForm.JobStatus_Id = 2;
                        PVForm.mode = "update";
                        await _JobsAppRepository.UpdateJobs(PVForm);
                    }
                }

				return Json(new { success = true });
            }
            else
            {
                return Json(new { success = false });
            }
        }

        public static int GetRandomNo()
        {
            Random rn = new Random();
            int i = 0;
            i = rn.Next(1, 10000);
            return i;
        }

        public ActionResult InverterData(HttpPostedFileBase file)
        {
            string fileName = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\BulkUploadFIles\\" + "Inverter_" + DateTime.Now.Ticks.ToString() + ".xlsx"; //System.Configuration.ConfigurationManager.AppSettings["FileToImport"].ToString();
            file.SaveAs(fileName);
            DataTable dt = new DataTable();
            DataSet ds = GetDataFromExcel(fileName);
            dt = ds.Tables[0];

			try
			{
				for (int j = 0; j < dt.Rows.Count; j++)
				{
					try
					{
						string Manufacturer = Convert.ToString(dt.Rows[j]["Manufacturer"]);
						string Series = Convert.ToString(dt.Rows[j]["Series"]);
						string ModelNumber = Convert.ToString(dt.Rows[j]["Model Number"]);
						decimal ACPower = Convert.ToDecimal(dt.Rows[j]["AC Power (kW)"]);
						DateTime ApprovalDate = Convert.ToDateTime(dt.Rows[j]["Approval Date"]);
						DateTime Expirydate = Convert.ToDateTime(dt.Rows[j]["Expiry date"]);

						_inverterRepository.CreateInverter(new Inverters.Dto.CreateOrUpdateInverterDto
						{
							ManufacturarName = Manufacturer,
							Series = Series,
							ModelNumber = ModelNumber,
							ACPower = ACPower,
							Sizes = 0,
							ApprovalDate = ApprovalDate,
							ExpiryDate = Expirydate
						});
					}
					catch (Exception e)
					{
						throw new UserFriendlyException("Ooppps! There is a problem!", "Fields Mismatch In Excel File");
					}
				}

				_inverterRepository.CreateInverterBulkUpload(new Inverters.Dto.CreateInverterBulkUploadDto
				{
					InverterFileName = fileName
				});
			}
			catch(Exception e)
			{

			}
            return Json(new { success = true });
        }

        public DataSet GetDataFromExcel(string fileName)
        {
            using (var stream = System.IO.File.Open(fileName, FileMode.Open, FileAccess.Read))
            {
                // Auto-detect format, supports:
                //  - Binary Excel files (2.0-2003 format; *.xls)
                //  - OpenXml Excel files (2007 format; *.xlsx)
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    // Choose one of either 1 or 2:

                    // 1. Use the reader methods
                    do
                    {
                        while (reader.Read())
                        {
                            // reader.GetDouble(0);
                        }
                    } while (reader.NextResult());

                    // 2. Use the AsDataSet extension method
                    var result = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {

                        // Gets or sets a value indicating whether to set the DataColumn.DataType 
                        // property in a second pass.
                        UseColumnDataType = true,

                        // Gets or sets a callback to obtain configuration options for a DataTable. 
                        ConfigureDataTable = (tableReader) => new ExcelDataTableConfiguration()
                        {

                            // Gets or sets a value indicating the prefix of generated column names.
                            EmptyColumnNamePrefix = "Column",

                            // Gets or sets a value indicating whether to use a row from the 
                            // data as column names.
                            UseHeaderRow = true,

                            // Gets or sets a callback to determine which row is the header row. 
                            // Only called when UseHeaderRow = true.
                            ReadHeaderRow = (rowReader) =>
                            {
                                // F.ex skip the first row and use the 2nd row as column headers:
                                //rowReader.Read();
                            },

                            // Gets or sets a callback to determine whether to include the 
                            // current row in the DataTable.
                            FilterRow = (rowReader) =>
                            {
                                return true;
                            },

                            // Gets or sets a callback to determine whether to include the specific
                            // column in the DataTable. Called once per column after reading the 
                            // headers.
                            FilterColumn = (rowReader, columnIndex) =>
                            {
                                return true;
                            }
                        }
                    });
                    return result;
                    // The result of each spreadsheet is in result.Tables
                }
            }
        }
    }
}