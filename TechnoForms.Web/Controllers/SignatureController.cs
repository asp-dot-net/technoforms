﻿using Abp.AutoMapper;
using Abp.Domain.Repositories;
using Abp.Net.Mail;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using TechnoForms.InstallerEmployes;
using TechnoForms.Installers;
using TechnoForms.Inverters;
using TechnoForms.Jobs;
using TechnoForms.Panels;
using TechnoForms.Web.Models.Jobs;

namespace TechnoForms.Web.Controllers
{
    public class SignatureController : Controller
    {
        private readonly IRepository<Job> _jobRepository;
        private readonly IRepository<Panel> _panelRepository;
        private readonly IRepository<Inverter> _inverterRepository;
        private readonly IRepository<RecYear> _recyearRepository;
        private readonly IRepository<Token> _tokenRepository;
        private readonly IRepository<Installer> _installerRepository;
        private readonly IRepository<CustomerSignLog> _customerSignLogRepository;
        private readonly IRepository<CustomerSignLogResponse> _customerSignLogResponseRepository;
        private readonly IRepository<InstallerEmploye> _installerEmployeRepository;
        private readonly IEmailSender _emailSender;
        static string StatusMessage = "";
        static Guid GuId;
        static Boolean isOwner;
        static string Token = "";
        static string FileName = "";
        // GET: Signature
        public SignatureController(IRepository<Job> jobRepository,
            IRepository<Panel> panelRepository,
            IRepository<Inverter> inverterRepository,
            IRepository<RecYear> recyearRepository,
            IRepository<Token> tokenRepository,
            IRepository<Installer> installerRepository,
            IRepository<CustomerSignLog> customerSignLogRepository,
            IRepository<CustomerSignLogResponse> customerSignLogResponseRepository,
            IRepository<InstallerEmploye> installerEmployeRepository,
            IEmailSender emailSender)
        {
            _jobRepository = jobRepository;
            _panelRepository = panelRepository;
            _inverterRepository = inverterRepository;
            _recyearRepository = recyearRepository;
            _tokenRepository = tokenRepository;
            _installerRepository = installerRepository;
            _customerSignLogRepository = customerSignLogRepository;
            _customerSignLogResponseRepository = customerSignLogResponseRepository;
            _installerEmployeRepository = installerEmployeRepository;
            _emailSender = emailSender;

        }
        public ActionResult Index(Guid jobguid, string rt)
        {
            bool IsExpired = false;
            string Name = "";
            DateTime TokenDate = new DateTime();
            if (!string.IsNullOrEmpty(jobguid.ToString()) && !string.IsNullOrEmpty(rt))
            {
                if (jobguid.ToString().Length == Guid.Empty.ToString().Length)
                {
                    GuId = new Guid(jobguid.ToString());
                    bool IsExists = (from data in _jobRepository.GetAll() where data.Guid == GuId select data.Guid).Any();
                    if (IsExists == true)
                    {
                        var tokenData = (from data in _tokenRepository.GetAll() where data.JobGuId == GuId && data.TokenNo == rt select data).FirstOrDefault();
                        if (tokenData.IsOwner)
                        {
                            Name = (from data in _jobRepository.GetAll() where data.Guid == GuId select data.FirstName + " " + data.LastName).FirstOrDefault();
                        }
                        else
                        {
                            int InstallerId = (int)(from a in _jobRepository.GetAll() where a.Guid == GuId select a.CreateFor_Id).FirstOrDefault();
                            Name = (from a in _installerRepository.GetAll() where a.Id == InstallerId select a.FullName).FirstOrDefault();
                        }

                        isOwner = (Boolean)tokenData.IsOwner;

                        TokenDate = Convert.ToDateTime(tokenData.CreationTime);
                        TokenDate = TokenDate.AddHours(48);

                        if (DateTime.Now > TokenDate)
                        {
                            IsExpired = true;
                        }
                        if (IsExists && tokenData.IsExpired == false && IsExpired == false)
                        {
                            ViewBag.StatusMessage = "Success";
                        }
                        else
                        {
                            ViewBag.StatusMessage = "Error";
                        }
                    }
                }
                else
                {
                    ViewBag.StatusMessage = "Error";
                }
            }
            else
            {
                ViewBag.StatusMessage = "Error";
            }

            ViewBag.Name = Name;
            if (isOwner == false)
            {
                ViewBag.Title = "Installer";

            }
            else
            {
                ViewBag.Title = "Customer";
            }
            Token = rt;
            ViewBag.rt = rt;
            ViewBag.GuId = GuId;

            ViewBag.IPAddress = "11111";

            var jobmodel = (from data in _jobRepository.GetAll()
                            where data.Guid == jobguid
                            select new JobDetails
                            {
                                FirstName = data.FirstName,
                                LastName = data.LastName,
                                Company = data.Company,
                                StreetAddress = data.StreetAddress,
                                StreetCity = data.StreetCity,
                                StreetState = data.StreetState,
                                StreetPostCode = data.StreetPostCode,
                                ExpectedInstallDate = data.ExpectedInstallDate,
                                ProjectNumber = data.ProjectNumber,
                                BrandName = data.BrandName,
                                Panel_id = data.Panel_id,
                                //PanelModel = _panelRepository.GetAll().Where(p => p.Id == data.Panel_id).Select(p => p.ModelNo).FirstOrDefault(),
                                InverterBrandName = data.InverterBrandName,
                                Inverter_id = data.Inverter_id,
                                //InverterModel = _inverterRepository.GetAll().Where(p => p.Id == data.Inverter_id).Select(p => p.ModelNumber).FirstOrDefault(),
                                SystemCapacity = data.SystemCapacity,
                                NoOfPanels = data.NoOfPanels,
                                RECYear_Id = data.RECYear_Id,
                                //RECYearName = _recyearRepository.GetAll().Where(p => p.Id == data.RECYear_Id).Select(p => p.REC_Year).FirstOrDefault(),
                                EmailId = data.EmailId,
                                CreationTime = DateTime.Now
                            }).FirstOrDefault();

            jobmodel.PanelModel = _panelRepository.GetAll().Where(p => p.Id == jobmodel.Panel_id).Select(p => p.ModelNo).FirstOrDefault();
            jobmodel.InverterModel = _inverterRepository.GetAll().Where(p => p.Id == jobmodel.Inverter_id).Select(p => p.ModelNumber).FirstOrDefault();
            jobmodel.RECYearName = _recyearRepository.GetAll().Where(p => p.Id == jobmodel.RECYear_Id).Select(p => p.REC_Year).FirstOrDefault();
            jobmodel.Latitude = "1753521";
            jobmodel.Longitude = "1753521";
            return View(jobmodel);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<JsonResult> SaveSignAsync(string value, string rt, string GuId, string log, string lat, string IP)
        {
            Guid newGuId = new Guid(GuId);
            var jodid = (from data in _jobRepository.GetAll() where data.Guid == newGuId select data.Id).FirstOrDefault();

            int Request_Id = (from data in _customerSignLogRepository.GetAll()
                              where data.Token == rt
                              select data.Id).FirstOrDefault();

            int Id = 0;
            int TenantId = (from data in _jobRepository.GetAll() where data.Guid == newGuId select data.TenantId).FirstOrDefault();
            Id = (from a in _tokenRepository.GetAll() where a.JobGuId == newGuId && a.TokenNo == rt && a.IsExpired == false select a.Id).FirstOrDefault();
            if (isOwner == true)
            {
                if (Id != 0)
                {
                    try
                    {
                        string exists = "";
                        exists = (from data in _jobRepository.GetAll() where data.Guid == newGuId select data.SignatureImageOwner).FirstOrDefault();
                        if (!string.IsNullOrEmpty(exists))
                        {

                            string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\OwnerSignature\\" + exists;
                            System.IO.File.Delete(filepath);
                        }


                        //FileName = (from a in _jobRepository.GetAll() where a.Guid == newGuId select a.FormId).FirstOrDefault();
                        FileName = "ownerSignature_" + DateTime.Now.Ticks + ".jpg";
                        //FileName += AdminSiteConfiguration.GetRandomNo() + "Owner.jpg";
                        byte[] bytIn = null;
                        value = value.Replace("data:image/png;base64,", "");
                        bytIn = Convert.FromBase64String(value);
                        var path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\OwnerSignature\\" + FileName;


                        FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write);
                        fs.Write(bytIn, 0, bytIn.Length);
                        fs.Close();



                        var job = await _jobRepository.GetAsync(jodid);
                        job.SignatureImageOwner = FileName;
                        job.SignatureImageOwnerDate = DateTime.Now;
                        job.Longitude_CustSign = log;
                        job.Latitude_CustSign = lat;
                        job.CustomerIP = IP;
                        await _jobRepository.UpdateAsync(job);

                        var token = await _tokenRepository.GetAsync(Id);
                        token.IsExpired = true;
                        await _tokenRepository.UpdateAsync(token);

                        var CustomerSignLogResponse = new CustomerSignLogResponse();
                        CustomerSignLogResponse.Request_Id = Request_Id;
                        CustomerSignLogResponse.JobId = jodid;
                        CustomerSignLogResponse.Token = rt;
                        CustomerSignLogResponse.TenantId = TenantId;
                        await _customerSignLogResponseRepository.InsertAsync(CustomerSignLogResponse);

                        #region SendMail
                        try
                        {
                            string CustomerName = (from data in _jobRepository.GetAll() where data.Guid == newGuId select data.FirstName + " " + data.LastName).FirstOrDefault();
                            string ProjectNumber = (from data in _jobRepository.GetAll() where data.Guid == newGuId select data.ProjectNumber).FirstOrDefault();
                            string Email = (from data in _jobRepository.GetAll() where data.Guid == newGuId select data.EmailId).FirstOrDefault();

                            var mailMessage = new StringBuilder();

                            mailMessage.AppendLine("<b>Details as follow :</b><br />");
                            mailMessage.AppendLine("Hello <br/><b>: " + CustomerName + "</b><br /><br />");
                            mailMessage.AppendLine(CustomerName + "has just submitted his / her signature for project no :" + ProjectNumber);
                            mailMessage.AppendLine("Thanks and Regards<br />");
                            mailMessage.AppendLine("Copyrights © 2020 TechnoForms. All rights reserved.");

                            string subject = "Signature Confirmation";
                            await _emailSender.SendAsync(Email, subject, mailMessage.ToString());
                            StatusMessage = "Success";
                        }
                        catch
                        {
                            StatusMessage = "Error";
                        }

                        #endregion


                    }
                    catch (Exception e) { }
                    StatusMessage = "Success";
                }
                else
                {
                    StatusMessage = "Error";
                }
                return Json(StatusMessage, JsonRequestBehavior.AllowGet);
            }
            else
            {
                if (Id != 0)
                {
                    try
                    {
                        string exists = "";
                        exists = (from data in _jobRepository.GetAll() where data.Guid == newGuId select data.SignatureImageInst).FirstOrDefault();
                        if (!string.IsNullOrEmpty(exists))
                        {

                            string filepath = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\InstallerSignature\\" + exists;
                            System.IO.File.Delete(filepath);
                        }

                    }
                    catch { }
                    FileName = "InstallerSignature_" + DateTime.Now.Ticks + ".jpg";
                    byte[] bytIn = null;
                    value = value.Replace("data:image/png;base64,", "");
                    bytIn = Convert.FromBase64String(value);
                    var path = System.Web.HttpContext.Current.Request.PhysicalApplicationPath + "Temp\\InstallerSignature\\" + FileName;

                    FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write);
                    fs.Write(bytIn, 0, bytIn.Length);
                    fs.Close();


                    int ElecId = (from d in _jobRepository.GetAll() where d.Guid == newGuId select d.Electrician_Id).FirstOrDefault();
                    int descId = (from d in _jobRepository.GetAll() where d.Guid == newGuId select d.Designer_Id).FirstOrDefault();
                    int InstId = (from d in _jobRepository.GetAll() where d.Guid == newGuId select d.Installer_Id).FirstOrDefault();
                    int IsElec = (from data in _installerEmployeRepository.GetAll()
                                  where data.IsElec == true && data.Id == ElecId
                                  select data.Id).FirstOrDefault();
                    int IsDesc = (from data in _installerEmployeRepository.GetAll()
                                  where data.IsDesc == true && data.Id == descId
                                  select data.Id).FirstOrDefault();
                    int IsInst = (from data in _installerEmployeRepository.GetAll()
                                  where data.IsInst == true && data.Id == InstId
                                  select data.Id).FirstOrDefault();

                    try
                    {

                        var job = await _jobRepository.GetAsync(jodid);
                        if (IsInst != 0)
                        {
                            job.SignatureImageInst = FileName;
                            job.SignatureImageInstDate = DateTime.Now;
                            job.Longitude_InstSign = log;
                            job.Latitude_InstSign = lat;
                            job.InstallerIP = IP;
                        }
                        if (IsDesc != 0)
                        {
                            job.DesignerSignature = FileName;
                            job.DesignerSignatureDate = DateTime.Now;
                            job.Longitude_DesiSign = log;
                            job.Latitude_DesiSign = lat;
                            job.InstallerIP = IP;
                        }
                        if (IsElec != 0)
                        {
                            job.ElectricianSignature = FileName;
                            job.ElectricianSignatureDate = DateTime.Now;
                            job.Longitude_ElecSign = log;
                            job.Latitude_ElecSign = lat;
                            job.InstallerIP = IP;
                        }

                        await _jobRepository.UpdateAsync(job);

                        var token = await _tokenRepository.GetAsync(Id);
                        token.IsExpired = true;
                        await _tokenRepository.UpdateAsync(token);

                        var CustomerSignLogResponse = new CustomerSignLogResponse();
                        CustomerSignLogResponse.Request_Id = Request_Id;
                        CustomerSignLogResponse.JobId = jodid;
                        CustomerSignLogResponse.Token = rt;
                        CustomerSignLogResponse.TenantId = TenantId;
                        await _customerSignLogResponseRepository.InsertAsync(CustomerSignLogResponse);

                    }
                    catch (Exception e) { }
                    #region SendMail
                    try
                    {
                        int Installer_Id = (from data in _jobRepository.GetAll() where data.Guid == newGuId select data.Installer_Id).FirstOrDefault();
                        string InstallerName = (from data in _installerRepository.GetAll() where data.Id == Installer_Id select data.FullName).FirstOrDefault();
                        string ProjectNumber = (from data in _jobRepository.GetAll() where data.Guid == newGuId select data.ProjectNumber).FirstOrDefault();
                        string Email = (from data in _installerRepository.GetAll() where data.Id == Installer_Id select data.EmailId).FirstOrDefault();

                        var mailMessage = new StringBuilder();

                        mailMessage.AppendLine("<b>Details as follow :</b><br />");
                        mailMessage.AppendLine("Hello <br/><b>: " + InstallerName + "</b><br /><br />");
                        mailMessage.AppendLine(InstallerName + "has just submitted his / her signature for project no :" + ProjectNumber);
                        mailMessage.AppendLine("Thanks and Regards<br />");
                        mailMessage.AppendLine("Copyrights © 2020 TechnoForms. All rights reserved.");

                        string subject = "Signature Confirmation";
                        await _emailSender.SendAsync(Email, subject, mailMessage.ToString());
                        StatusMessage = "Success";
                    }
                    catch
                    {
                        StatusMessage = "Error";
                    }
                    #endregion
                    ViewBag.StatusMessage = "Success";
                    StatusMessage = "Success";
                    return Json(StatusMessage, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    StatusMessage = "Error";
                }
            }
            return Json(StatusMessage, JsonRequestBehavior.AllowGet);
        }
    }
}