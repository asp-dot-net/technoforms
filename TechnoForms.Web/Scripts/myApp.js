﻿var app = angular.module('myApp', ['angucomplete-alt']);
app.controller('ngAutoCompleteController', ['$scope', '$http', function ($scope, $http) {
    $scope.Countries = [];
    $scope.SelectedCountry = null;


    //After select country event
    $scope.afterSelectedCountry = function (selected) {
        if (selected) {
            $scope.SelectedCountry = selected.originalObject;
        }
    }

    //Populate data from database 
    $http({
        method: 'GET',
        url:'/home/GetCountries'
    }).then(function (data) {
        $scope.Countries = data.data;
    }, function () {
        alert('Error');
    })
}]);