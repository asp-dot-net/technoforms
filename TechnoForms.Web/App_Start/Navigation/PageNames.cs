namespace TechnoForms.Web.Navigation
{
    public static class PageNames
    {
        public static class App
        {
            public static class Common
            {
                public const string Administration = "Administration";
                public const string Roles = "Administration.Roles";
                public const string Users = "Administration.Users";
                public const string AuditLogs = "Administration.AuditLogs";
                public const string OrganizationUnits = "Administration.OrganizationUnits";
                public const string Languages = "Administration.Languages";
                //public const string RECUser = "Administration.RECUser";
            }

            public static class Host
            {
                public const string Dashboard = "Dashboard.Host";
                public const string Tenants = "Tenants";
                public const string Editions = "Editions";
                public const string Maintenance = "Administration.Maintenance";
                public const string Settings = "Administration.Settings.Host";
                public const string Master = "Master";
                public const string UnitType = "Master.UnitType";
                public const string Streettype = "Master.StreetType";
                public const string StreetName = "Master.StreetName";
                public const string PostCodes = "Master.PostCodes";
                public const string Postaltype = "Master.PostalType";
                public const string Inverter = "Master.Inverter";
                public const string Panel = "Master.Panel";
                public const string JobReport = "Master.JobReport";
                public const string AdminInvitation = "Invitation";
                public const string InstallerInvitation = "Invitation";
                public const string NewInstallerInvitation = "NewInstallerInvitation";
                public const string Invoice = "Invoice";
                public const string Invoice_Generate = "Invoice.Generate";
                public const string Invoice_Generated = "Invoice.Generated";
                public const string Invoice_Collection = "Invoice.Collection";
                public const string Video = "Video.Tenant";

                public const string AllInstaller = "AllInstallerRequests";
                public const string AllInstallerRequest = "AllInstallerRequests";
                public const string STC = "STC";
                //public const string Test = "Test";
            }

            public static class Tenant
            {
                public const string Dashboard = "Dashboard.Tenant";
                public const string Settings = "Administration.Settings.Tenant";
                public const string Installer = "Installer.Tenant";
                public const string EnergyDestributars = "EnergyDestributars.Tenant";
                public const string EnergyRetailers = "EnergyRetailers.Tenant";
                public const string Jobs = "Installer.Jobs";
                public const string CompliaenceJobs = "Jobs.CompliaenceJobs";
                public const string Calendar = "Calendar.Tenant";
            }
        }

        public static class Frontend
        {
            public const string Home = "Frontend.Home";
            public const string About = "Frontend.About";
        }
    }
}