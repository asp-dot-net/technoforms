﻿(function () {

    appModule.controller('common.views.roles.index', [
        '$scope', '$state', '$uibModal', '$templateCache', 'abp.services.app.role', 'uiGridConstants',
        function ($scope, $state, $uibModal, $templateCache, roleService, uiGridConstants) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            vm.requestParams = {
                permission: ''
            };

            vm.permissions = {
                create: abp.auth.hasPermission('Pages.Administration.Roles.Create'),
                edit: abp.auth.hasPermission('Pages.Administration.Roles.Edit'),
                'delete': abp.auth.hasPermission('Pages.Administration.Roles.Delete')
            };

            vm.roleGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        enableColumnMenu: false,
                        headerCellClass:'center',   
                        cellClass: 'centeralign',
                        width:80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.edit" ng-click="grid.appScope.editRole(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="!row.entity.isStatic && grid.appScope.permissions.delete" ng-click="grid.appScope.deleteRole(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('RoleName'),
                        enableColumnMenu: false,
                        field: 'displayName',
                        minWidth:200,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  {{COL_FIELD CUSTOM_FILTERS}} &nbsp;' +
                            '  <span ng-show="row.entity.isStatic" class="label label-info" uib-popover="' + app.localize('StaticRole_Tooltip') + '" popover-placement="bottom" popover-trigger="\'mouseenter click\'">' + app.localize('Static') + '</span>&nbsp;' +
                            '  <span ng-show="row.entity.isDefault" class="label label-default" uib-popover="' + app.localize('DefaultRole_Description') + '" popover-placement="bottom" popover-trigger="\'mouseenter click\'">' + app.localize('Default') + '</span>' +
                            '</div>'
                    },
                    {
                        name: app.localize('CreationTime'),
                        enableColumnMenu: false,
                        field: 'creationTime', cellClass:'red',
                        width:180, 
                        headerCellClass:'centeralign',
                        cellClass:'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            vm.requestParams.sorting = null;
                        } else {
                            vm.requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getRoles();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        vm.requestParams.skipCount = (pageNumber - 1) * pageSize;
                        vm.requestParams.maxResultCount = pageSize;

                        vm.getRoles();
                    });
                },
                data: []
            };

            if (!vm.permissions.edit && !vm.permissions.delete) {
                vm.roleGridOptions.columnDefs.shift();
            }

            vm.getRoles = function () {
                vm.loading = true;
                roleService.getRoles(vm.requestParams).then(function (result) {
                    vm.roleGridOptions.data = result.data.items;
                    if (result.data.items.length == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.editRole = function (role) {
                openCreateOrEditRoleModal(role.id);
            };

            vm.deleteRole = function (role) {
                abp.message.confirm(
                    app.localize('RoleDeleteWarningMessage', role.displayName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            roleService.deleteRole({
                                id: role.id
                            }).then(function () {
                                vm.getRoles();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.createRole = function () {
                openCreateOrEditRoleModal(null);
            };

            function openCreateOrEditRoleModal(roleId) {
                //$state.go('CreateOrEditRole', {
                //    Id: roleId
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '~/App/common/views/roles/createOrEditModal.cshtml',
                    controller: 'common.views.roles.createOrEditModal as vm',
                    backdrop: 'static',
                    resolve: {
                        roleId: function () {
                            return roleId;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getRoles();
                });
            }

            vm.getRoles();
        }
    ]);
})();