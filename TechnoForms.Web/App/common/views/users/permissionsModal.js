﻿(function () {
    appModule.controller('common.views.users.permissionsModal', [
        '$scope', '$stateParams', '$state', '$uibModalInstance', 'abp.services.app.user', 'user',
        function ($scope, $stateParams, $state, $uibModalInstance, userService, user) {
            //'$scope', '$uibModalInstance', 'abp.services.app.user', 'user',
            //function ($scope, $uibModalInstance, userService, user) {
            var vm = this;

            vm.saving = false;
            vm.resettingPermissions = false;
            //vm.user = user;
            //user = null;
            vm.permissionEditData = null;
            vm.userid = $stateParams.Id;
            vm.resetPermissions = function () {
                vm.resettingPermissions = true;
                userService.resetUserSpecificPermissions({
                    id: user
                    // id: vm.user.id
                }).then(function () {
                    abp.notify.info(app.localize('ResetSuccessfully'));
                    loadPermissions();
                }).finally(function () {
                    vm.resettingPermissions = false;
                });
            };

            vm.save = function () {
                vm.saving = true;
                userService.updateUserPermissions({
                    id: user,
                    //id: vm.user.id,
                    grantedPermissionNames: vm.permissionEditData.grantedPermissionNames
                }).then(function () {
                    //$state.go('users');
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $uibModalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                //$state.go('users');
                $uibModalInstance.dismiss();
            };

            function loadPermissions() {
                userService.getUserPermissionsForEdit({
                    id: user
                   // id: vm.user.id
                }).then(function (result) {
                    vm.permissionEditData = result.data;
                    });

                userService.getUserForEdit({
                    Id: user
                }).then(function (result) {
                    vm.user = result.data.user;
                });
            }

            loadPermissions();
        }
    ]);
})();