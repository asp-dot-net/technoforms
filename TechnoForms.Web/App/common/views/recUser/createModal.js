﻿(function () {
    appModule.controller('common.views.recUser.createModel', [
        '$scope', '$state', 'abp.services.app.recUser',
        function ($scope, $state, recUserService) {
            var vm = this;

            vm.saving = false;
            vm.exist = false;
            vm.message = "";
            vm.recUser = {};

            vm.save = function () {
                vm.saving = true;
                recUserService.checkRecUserExists({ recUserName: vm.recUser.recUserName }).then(function (result) {
                    if (result.data.status == true) {
                        recUserService.createRecUser(vm.recUser).then(function () {
                            $state.go('recUser', {
                            });
                            abp.notify.info(app.localize('SavedSuccessfully'));
                        }).finally(function () {
                            vm.saving = false;
                        });
                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        vm.saving = false;
                    }
                });

            };

            vm.cancel = function () {
                $state.go('recUser', {
                });
            };
        }
    ]);
})();