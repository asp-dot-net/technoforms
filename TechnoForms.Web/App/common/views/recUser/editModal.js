﻿(function () {
    appModule.controller('common.views.recUser.editModel', [
        '$scope', '$state', '$stateParams', 'abp.services.app.recUser',
        function ($scope, $state, $stateParams, recUserService) {
            var vm = this;

            vm.saving = false;
            vm.recUser = null;
            vm.editions = [];
            vm.recUserId = $stateParams.Id;

            vm.exist = false;
            vm.message = "";

            vm.save = function () {
                vm.saving = true;
                recUserService.checkRecUserExists({ recUserName: vm.recUser.recUserName, Id: vm.recUserId }).then(function (result) {
                    if (result.data.status == true) {
                        recUserService.updateRecUser(vm.recUser)
                            .then(function () {
                                $state.go('recUser', {
                                });
                                abp.notify.info(app.localize('SavedSuccessfully'));
                            }).finally(function () {
                                vm.saving = false;
                            });
                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        vm.saving = false;
                    }
                });
            };

            vm.cancel = function () {
                $state.go('recUser', {
                });
            };

            function init() {
                recUserService.getRecUserForEdit({
                    id: vm.recUserId
                }).then(function (result) {
                    vm.recUser = result.data;
                });
            }
            init();
        }
    ]);
})();