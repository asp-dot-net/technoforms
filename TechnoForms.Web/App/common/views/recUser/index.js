﻿(function () {
    appModule.controller('common.views.recUser.index', [
        '$scope', '$state', '$uibModal', '$http', 'uiGridConstants', 'abp.services.app.recUser',

        function ($scope, $state, $uibModal, $http, uiGridConstants, recUserService) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;

            vm.permissions = {
                createrecUser: abp.auth.hasPermission('Pages.Administration.RECUser.Create'),
                editrecUser: abp.auth.hasPermission('Pages.Administration.RECUser.Edit'),
                deleterecUser: abp.auth.hasPermission('Pages.Administration.RECUser.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.recUserGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        cellClass: 'centeralign',
                        headerCellClass: 'centeralign',
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.editrecUser" ng-click="grid.appScope.editrecUser(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.deleterecUser" ng-click="grid.appScope.deleterecUser(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('RecUserName'),
                        enableColumnMenu: false,
                        field: 'recUserName',
                        minWidth: 150
                    },
                    {
                        name: app.localize('CreationTime'),
                        enableColumnMenu: false,
                        field: 'creationTime',
                        cellClass: 'centeralign',
                        headerCellClass: 'centeralign',
                        width: 170,
                        cellFilter: 'momentFormat: \'L\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getRecUser();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getRecUser();
                    });
                },
                data: []
            };

            if (!vm.permissions.editrecUser && !vm.permissions.deleterecUser) {
                vm.recUserGridOptions.columnDefs.shift();
            }

            vm.getRecUser = function () {
                vm.loading = true;
                recUserService.getRecUser({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    recUserName: vm.filterText
                }).then(function (result) {
                    vm.recUserGridOptions.totalItems = result.data.totalCount;
                    vm.recUserGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createrecUser = function () {
                $state.go('createrecUser', {
                });
            };

            vm.deleterecUser = function (rec) {
                abp.message.confirm(
                    app.localize('recUserDeleteWarningMessage', rec.recUserName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            recUserService.deleteRecUser({
                                id: rec.id
                            }).then(function () {
                                vm.getRecUser();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.editrecUser = function (recUser) {
                $state.go('editrecUser', {
                    Id: recUser.id
                });
            };
            vm.getRecUser();
        }
    ]);
})();