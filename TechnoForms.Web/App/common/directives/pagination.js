﻿
    (function () {
        var app = angular.module("app");
        app.directive('pagination', function () {
            return {
                templateUrl: "/App/common/directives/pagination.cshtml",
                scope: {
                    itemsPerpage: "@",
                    totalRecord: "@",
                    skipCount: "=",
                    //refresh: "&",
                    refresh: '=method'
                },
                link: function (scope, element, attrs) {
                    var init = function () {
                        scope.currentPage = 0;
                    };

                    scope.range = function () {
                        var rangeSize = 4;
                        var ps = [];
                        var start;
                        start = scope.currentPage;
                        if (start > scope.pageCount() - rangeSize) {
                            start = scope.pageCount() - rangeSize + 1;
                        }
                        for (var i = start; i < start + rangeSize; i++) {

                            if (i != -1 && i != -2 && i != -3 && i != -4) {
                                ps.push(i);
                            }
                        }
                        return ps;
                    };

                    scope.prevPage = function () {
                        if (scope.currentPage > 0) {
                            scope.currentPage--;
                            scope.skipCount = scope.itemsPerpage * scope.currentPage;
                            scope.refresh(scope.skipCount);
                        }
                    };

                    scope.firstPage = function () {
                        scope.currentPage = 0;
                        scope.skipCount = scope.itemsPerpage * scope.currentPage;
                        scope.refresh(scope.skipCount);
                    };

                    scope.lastPage = function () {
                        scope.currentPage = scope.pageCount();
                        scope.skipCount = scope.itemsPerpage * scope.currentPage;
                        scope.refresh(scope.skipCount);
                    };

                    scope.DisablePrevPage = function () {
                        return scope.currentPage === 0 ? "disabled" : "";
                    };

                    scope.pageCount = function () {
                        return Math.ceil(scope.totalRecord / scope.itemsPerpage) - 1;
                    };

                    scope.nextPage = function () {
                        if (scope.currentPage < scope.pageCount()) {
                            scope.currentPage++;
                            scope.skipCount = scope.itemsPerpage * scope.currentPage;
                            scope.refresh(scope.skipCount);
                        }
                    };

                    scope.DisableNextPage = function () {
                        if (scope.pageCount() < 0) {
                            return scope.currentPage === 0 ? "disabled" : "";
                        }
                        else {
                            return scope.currentPage === scope.pageCount() ? "disabled" : "";
                        }
                    };

                    scope.DisableFirstPage = function () {
                        return scope.currentPage === 0 ? "disabled" : "";
                    };

                    scope.DisableLastPage = function () {
                        return scope.currentPage === scope.pageCount() ? "disabled" : "";
                    };

                    scope.setPage = function (n) {
                        scope.currentPage = n;
                        scope.skipCount = scope.itemsPerpage * n;
                        scope.refresh(scope.skipCount);
                    };

                    init();
                }
            };
        });
    })();