﻿(function () {
    appModule.controller('tenant.views.compliaencejobs.inverterimage', [
        '$scope', '$uibModalInstance', 'abp.services.app.jobs', 'jobId',
        function ($scope, $uibModalInstance, jobsService, jobId) {
            var vm = this;

            vm.saving = false;
            vm.inverterimagelist = {};
            vm.displayVerifyIN = true;
            vm.save = function () {
            };
            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var currIndex = 0;
            vm.invertercount = 0;
            var slides = $scope.slides = [];

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.verify = function () {

                abp.message.confirm(
                    app.localize('verifyinverterphoto'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            jobsService.updateVerifyIN({ JobId: jobId }).then(function () {
                                abp.notify.info(app.localize('SavedSuccessfully'));
                                $uibModalInstance.close();
                                $state.go('tenant.jobs');
                            }).finally(function () {
                                vm.saving = false;
                            });
                        }
                    }
                );
            }

            function init() {


                jobsService.getInverterPhotos({ JobId: jobId }).then(function (result) {
                    vm.inverterimagelist = result.data.items;

                    angular.forEach(result.data.items, function (value, key) {
                        vm.invertercount++
                    });

                    angular.forEach(result.data.items, function (value, key) {
                        if (value.inverterBarcodePhoto != null || value.inverterBarcodePhoto != 'undefined') {
                            slides.push({
                                id: value.id,
                                inverterSerialNumber: value.inverterSerialNumber,
                                photo: value.inverterBarcodePhoto,
                                longitude: value.longitude,
                                latitude: value.latitude,
                                date: value.creationTime,
                                imagecount: vm.invertercount
                            });
                        }
                    });

                }).finally(function () {
                    vm.loading = false;
                });

                jobsService.getJobsForEdit({ Id: jobId }).then(function (result) {
                    if (result.data.isVerifyIN || vm.invertercount == 0) {
                        vm.displayVerifyIN = false;
                    }
                }).finally(function () {
                    vm.loading = false;
                });

            }

            init();
        }
    ]);
})();