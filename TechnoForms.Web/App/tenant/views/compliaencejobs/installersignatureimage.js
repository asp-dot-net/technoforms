﻿(function () {
    appModule.controller('tenant.views.compliaencejobs.installersignatureimage', [
        '$scope', '$uibModalInstance', 'abp.services.app.jobs', 'jobId',
        function ($scope, $uibModalInstance, jobsService, jobId) {
            var vm = this;

            vm.saving = false;
            vm.panelimagelist = {};
            vm.installersignaturecount = 0;
            vm.displayVerifyIS = true;
            vm.save = function () {
            };

            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var currIndex = 0;
            var slides = $scope.slides = [];

            function shuffle(array) {
                var tmp, current, top = array.length;

                if (top) {
                    while (--top) {
                        current = Math.floor(Math.random() * (top + 1));
                        tmp = array[current];
                        array[current] = array[top];
                        array[top] = tmp;
                    }
                }

                return array;
            }
            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
            vm.verify = function () {

                abp.message.confirm(
                    app.localize('verifyinstallerphoto'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            jobsService.updateVerifyIS({ JobId: jobId }).then(function () {
                                abp.notify.info(app.localize('SavedSuccessfully'));
                                $uibModalInstance.close();
                                $state.go('tenant.jobs');
                            }).finally(function () {
                                vm.saving = false;
                            });
                        }
                    }
                );
            }
            function init() {


                jobsService.getCustomerSignaturePhotos({ JobId: jobId }).then(function (result) {
                    vm.panelimagelist = result.data.items;
                    //angular.forEach(result.data.items, function (value, key) {
                    if (result.data.items[0].signatureImageInst != null && result.data.items[0].signatureImageInst != 'undefined') {
                        vm.installersignaturecount++;
                    }
                    if (result.data.items[0].installerPhoto != null && result.data.items[0].installerPhoto != 'undefined') {
                        vm.installersignaturecount++;
                    }
                    console.log(result.data.items);
                    if (result.data.items[0].signatureImageInst != null && result.data.items[0].signatureImageInst != 'undefined') {
                        slides.push({
                            panelBarcodePhoto: "../../Temp/InstallerSignature/" + result.data.items[0].signatureImageInst,
                            longitude: result.data.items[0].longitude_InstSign,
                            latitude: result.data.items[0].latitude_InstSign,
                            date: result.data.items[0].creationTime,
                            imagecount: vm.installersignaturecount,
                            instcomment: 0
                        });
                    }
                    if (result.data.items[0].installerPhoto != null && result.data.items[0].installerPhoto != 'undefined') {
                        slides.push({
                            panelBarcodePhoto: "../../Temp/InstallerPhoto/" + result.data.items[0].installerPhoto,
                            longitude: result.data.items[0].longitude_InstSign,
                            latitude: result.data.items[0].latitude_InstSign,
                            date: result.data.items[0].creationTime,
                            imagecount: vm.installersignaturecount,
                            instcomment: 1
                        });
                    }
                    console.log(slides);
                    //  });
                }).finally(function () {
                    vm.loading = false;
                });

                jobsService.getJobsForEdit({ Id: jobId }).then(function (result) {
                    if (result.data.isVerifyIS || vm.installersignaturecount == 0) {
                        vm.displayVerifyIS = false;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            init();
        }
    ]);
})();