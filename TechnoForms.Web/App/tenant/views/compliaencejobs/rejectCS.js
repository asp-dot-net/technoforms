﻿(function () {
    appModule.controller('tenant.views.compliaencejobs.rejectCS', [
        '$scope', '$state', '$uibModalInstance', 'abp.services.app.complaienceJobs', 'jobId',
        function ($scope, $state, $uibModalInstance, complaienceJobsAppService, jobId) {
            var vm = this;

            vm.saving = false;
            vm.cjobs = {};
            vm.cjobs.formId = jobId;
            vm.save = function () {
                vm.saving = true;
                complaienceJobsAppService.rejectComplaienceCS({
                    JobId: vm.cjobs.formId,
                    RejectReason: vm.cjobs.rejectReasonCS
                }).then(function () {
                    $state.go('tenant.CompliaenceJobs');
                    $uibModalInstance.close();
                    abp.notify.success(app.localize('SuccessfullRejected'));
                });
            };

            function init() {
                complaienceJobsAppService.getRejectReasonByJobId({
                    JobId: vm.cjobs.formId
                }).then(function (result) {
                    console.log(result);
                    vm.cjobs.rejectReasonCS = result.data.rejectReasonCS;
                });
            }
            init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();