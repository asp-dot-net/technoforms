﻿(function () {
    appModule.controller('tenant.views.compliaencejobs.rejectELE', [
        '$scope', '$state', '$uibModalInstance', 'abp.services.app.complaienceJobs', 'jobId',
        function ($scope, $state, $uibModalInstance, complaienceJobsAppService, jobId) {
            var vm = this;

            vm.saving = false;
            vm.cjobs = {};
            vm.cjobs.formId = jobId;
            vm.save = function () {
                vm.saving = true;
                complaienceJobsAppService.rejectComplaienceELE({
                    JobId: vm.cjobs.formId,
                    RejectReason: vm.cjobs.rejectReasonELE
                }).then(function () {
                    $state.go('tenant.CompliaenceJobs');
                    $uibModalInstance.close()
                    abp.notify.success(app.localize('SuccessfullRejected'));
                   
                });
            };

            function init() {
                complaienceJobsAppService.getRejectReasonByJobId({
                    JobId: vm.cjobs.formId
                }).then(function (result) {
                    console.log(result);
                    vm.cjobs.rejectReasonELE = result.data.rejectReasonELE;
                })
            }
            init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();