﻿(function () {
    appModule.controller('tenant.views.compliaencejobs.panelimages', [
        '$scope', '$uibModalInstance', 'abp.services.app.jobs', 'jobId',
        function ($scope, $uibModalInstance, jobsService, jobId) {
            var vm = this;

            vm.saving = false;
            vm.panelimagelist = {};
            vm.panelcount = 0;

            vm.displayVerifyPA = true;
            vm.save = function () {
            };

            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var currIndex = 0;
            var slides = $scope.slides = [];

            function shuffle(array) {
                var tmp, current, top = array.length;

                if (top) {
                    while (--top) {
                        current = Math.floor(Math.random() * (top + 1));
                        tmp = array[current];
                        array[current] = array[top];
                        array[top] = tmp;
                    }
                }

                return array;
            }
            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.verify = function () {

                abp.message.confirm(
                    app.localize('verifypanelphoto'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            jobsService.updateVerifyPA({ JobId: jobId }).then(function () {
                                abp.notify.info(app.localize('SavedSuccessfully'));
                                $uibModalInstance.close();
                                $state.go('tenant.jobs');
                            }).finally(function () {
                                vm.saving = false;
                            });
                        }
                    }
                );
            }
            function init() {


                jobsService.getPanelPhotos({ JobId: jobId }).then(function (result) {
                    vm.panelimagelist = result.data.items;
                    angular.forEach(result.data.items, function (value, key) {
                        vm.panelcount++
                    });

                    angular.forEach(result.data.items, function (value, key) {
                        if (value.panelBarcodePhoto != null || value.panelBarcodePhoto != 'undefined') {
                            slides.push({
                                id: value.id,
                                panelSerialNumber: value.panelSerialNumber,
                                panelBarcodePhoto: value.panelBarcodePhoto,
                                longitude: value.longitude,
                                latitude: value.latitude,
                                date: value.creationTime,
                                imagecount: vm.panelcount
                            });
                        }
                    });
                }).finally(function () {
                    vm.loading = false;
                });

                jobsService.getJobsForEdit({ Id: jobId }).then(function (result) {
                    if (result.data.isVerifyPA || vm.panelcount == 0) {
                        vm.displayVerifyPA = false;
                    }
                }).finally(function () {
                    vm.loading = false;
                });

                //for (var i = 0; i < 4; i++) {
                //    var newWidth = 600 + slides.length + 1;
                //    slides.push({
                //        image: 'http://placekitten.com/' + newWidth + '/300',
                //        text: ['Nice image', 'Awesome photograph', 'That is so cool', 'I love that'][slides.length % 4],
                //        id: currIndex++
                //    });
                //}
            }

            init();
        }
    ]);
})();