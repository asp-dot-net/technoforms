﻿(function () {
    appModule.controller('tenant.views.compliaencejobs.detailModal', [
        '$scope', '$state', '$stateParams', 'abp.services.app.complaienceJobs',
        function ($scope, $state, $stateParams, complaienceJobsAppService) {
            var vm = this;
            vm.jobId = $stateParams.jobId;

            vm.jobs = {};
            vm.cancel = function () {
                $state.go('tenant.CompliaenceJobs', {
                });
            };
            function init() {
                complaienceJobsAppService.getJobsDetails({
                    id: vm.jobId
                }).then(function (result) {
                    if (result.data.systemType_Id == 2 || result.data.systemType_Id == 3) {
                        vm.systemTypeVal = true;
                    }
                    else {
                        vm.systemTypeVal = false;
                    }

                    vm.jobs = result.data;
                });
            }
            init();
        }
    ]);
})();