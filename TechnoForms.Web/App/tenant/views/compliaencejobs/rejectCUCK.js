﻿(function () {
    appModule.controller('tenant.views.compliaencejobs.rejectCUCK', [
        '$scope', '$state', '$uibModalInstance', 'abp.services.app.complaienceJobs', 'jobId',
        function ($scope, $state, $uibModalInstance, complaienceJobsAppService, jobId) {
            var vm = this;

            vm.saving = false;
            vm.cjobs = {};
            vm.cjobs.formId = jobId;
            vm.save = function () {
                vm.saving = true;
                complaienceJobsAppService.rejectComplaienceCUCK({
                    JobId: vm.cjobs.formId,
                    RejectReason: vm.cjobs.rejectReasonCUCK
                }).then(function () {
                    $state.go('tenant.CompliaenceJobs');
                    $uibModalInstance.close()
                    abp.notify.success(app.localize('SuccessfullRejected'));
                   
                });
            };

            function init() {
                complaienceJobsAppService.getRejectReasonByJobId({
                    JobId: vm.cjobs.formId
                }).then(function (result) {
                    console.log(result);
                    vm.cjobs.rejectReasonCUCK = result.data.rejectReasonCUCK;
                })
            }
            init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();