﻿(function () {
    appModule.controller('tenant.views.compliaencejobs.audioList', [
        '$scope', '$state', '$stateParams', '$uibModal', '$http', 'uiGridConstants', 'abp.services.app.complaienceJobs',

        function ($scope, $state, $stateParams, $uibModal, $http, uiGridConstants, complaienceJobsAppService) {
            var vm = this;
            vm.norecord = false;

            vm.jobId = $stateParams.jobId;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;

            vm.permissions = {
                createUnitType: abp.auth.hasPermission('Pages.Masters.UnitType.Create'),
                deleteUnitType: abp.auth.hasPermission('Pages.Masters.UnitType.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.openNewTab = function (test) {
                //alert(test);
                window.open(abp.appPath + 'Temp/Audio/' + test + '?v=' + new Date().valueOf(), '_blank');
            };
                        
            vm.audioGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    { width: 0},
                    {
                        name: app.localize('Actions'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        cellClass: 'centeralign',
                        headerCellClass: 'centeralign',
                        width: 120,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-click="grid.appScope.deleteUnitType(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('fileName'),
                        enableColumnMenu: false,
                        field: 'displayFileName'
                    },
                    {
                        name: app.localize('playAudio'),
                        enableColumnMenu: false,
                        minWidth: 200,
                        cellTemplate:
                            '<div>' +
                            ' <audio controls> ' +
                            '  <source src="{{row.entity.filePath}}" type="audio/ogg">' +
                            ' </audio>' +
                            '</div>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getAudio();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getAudio();
                    });
                },
                data: []
            };

            if (!vm.permissions.editUnitType && !vm.permissions.deleteUnitType) {
                vm.audioGridOptions.columnDefs.shift();
            }

            vm.getAudio = function () {
                vm.loading = true;
                complaienceJobsAppService.getAudio({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    jobId: vm.jobId
                }).then(function (result) {
                    vm.audioGridOptions.totalItems = result.data.totalCount;
                    vm.audioGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createAudio = function (id) {
                $state.go('createAudio', {
                    jobId: id
                });
            };

            vm.deleteUnitType = function (audio) {
                abp.message.confirm(
                    app.localize('AudioDeleteWarningMessage', audio.displayFileName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            complaienceJobsAppService.deleteAudio({
                                id: audio.id
                            }).then(function () {
                                vm.getAudio();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.getAudio();
        }
    ]);
})();