﻿(function () {
    appModule.controller('tenant.views.compliaencejobs.createAudio', [
        '$scope', '$state', 'FileUploader', '$stateParams', 'abp.services.app.complaienceJobs',
        function ($scope, $state, fileUploader, $stateParams, complaienceJobsAppService) {
            var vm = this;

            vm.saving = false;
            vm.exist = false;
            vm.message = "";
            vm.audio = {};

            vm.jobId = $stateParams.jobId;

            vm.logouploader = new fileUploader({
                url: abp.appPath + 'File/UploadAudio?Id=' + vm.jobId,
                headers: {
                    "X-XSRF-TOKEN": abp.security.antiForgery.getToken()
                },
                queueLimit: 1,
                autoUpload: true,
                removeAfterUpload: true,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        //File type check
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        //if ('|mp3|avi|mav|'.indexOf(type) === -1) {
                        //    abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                        //    return false;
                        //}
                        return true;
                    }
                }]
            });

            vm.logouploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    vm.audio.fileName = response.result.fileName;
                    vm.audio.displayFileName = response.result.displayFileName;
                } else {
                    abp.message.error(response.error.message);
                }
            };

            vm.save = function () {
                vm.saving = true;
                vm.audio.jobId = vm.jobId;
                complaienceJobsAppService.createAudio(vm.audio).then(function () {
                    $state.go('compliaenceAudio', {
                        jobId: vm.jobId
                    });
                    abp.notify.info(app.localize('SavedSuccessfully'));
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $state.go('host.unittype', {
                });
            };
        }
    ]);
})();