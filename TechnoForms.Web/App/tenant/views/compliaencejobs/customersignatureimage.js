﻿(function () {
    appModule.controller('tenant.views.compliaencejobs.customersignatureimage', [
        '$scope', '$uibModalInstance', 'abp.services.app.jobs', 'jobId',
        function ($scope, $uibModalInstance, jobsService, jobId) {
            var vm = this;

            vm.saving = false;
            vm.panelimagelist = {};
            vm.displayVerifyCS = true;
            vm.customercount = 0;
            vm.save = function () {
            };

            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var currIndex = 0;
            var slides = $scope.slides = [];

            function shuffle(array) {
                var tmp, current, top = array.length;

                if (top) {
                    while (--top) {
                        current = Math.floor(Math.random() * (top + 1));
                        tmp = array[current];
                        array[current] = array[top];
                        array[top] = tmp;
                    }
                }

                return array;
            }
            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.verify = function () {

                abp.message.confirm(
                    app.localize('verifycustomerphoto'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            jobsService.updateVerifyCS({ JobId: jobId }).then(function () {
                                abp.notify.info(app.localize('SavedSuccessfully'));
                                $uibModalInstance.close();
                                $state.go('tenant.jobs');
                            }).finally(function () {
                                vm.saving = false;
                            });
                        }
                    }
                );
            }

            function init() {

                jobsService.getCustomerSignaturePhotos({ JobId: jobId }).then(function (result) {
                    vm.panelimagelist = result.data.items;
                    //angular.forEach(result.data.items, function (value, key) {
                    if (result.data.items[0].signatureImageOwner != null && result.data.items[0].signatureImageOwner != 'undefined') {
                        vm.customercount++;
                    }
                    if (result.data.items[0].ownerPhoto != null && result.data.items[0].ownerPhoto != 'undefined') {
                        vm.customercount++;
                    }
                    if (result.data.items[0].signatureImageOwner != null && result.data.items[0].signatureImageOwner != 'undefined') {
                        slides.push({
                            panelBarcodePhoto: "../../Temp/OwnerSignature/" + result.data.items[0].signatureImageOwner,
                            longitude: result.data.items[0].longitude_CustSign,
                            latitude: result.data.items[0].latitude_CustSign,
                            date: result.data.items[0].creationTime,
                            imagecount: vm.customercount,
                            ownercomment: 0
                        });
                    }
                    if (result.data.items[0].ownerPhoto != null && result.data.items[0].ownerPhoto != 'undefined') {
                        slides.push({
                            panelBarcodePhoto: "../../Temp/OwnerPhoto/" + result.data.items[0].ownerPhoto,
                            longitude: result.data.items[0].longitude_CustSign,
                            latitude: result.data.items[0].latitude_CustSign,
                            date: result.data.items[0].creationTime,
                            imagecount: vm.customercount,
                            ownercomment: 1
                        });
                    }
                    console.log(slides);
                    //  });
                }).finally(function () {
                    vm.loading = false;
                });

                jobsService.getJobsForEdit({ Id: jobId }).then(function (result) {
                    if (result.data.IsVerifyCS || vm.customercount == 0) {
                        vm.displayVerifyCS = false;
                    }
                }).finally(function () {
                    vm.loading = false;
                });

            }

            init();
        }
    ]);
})();