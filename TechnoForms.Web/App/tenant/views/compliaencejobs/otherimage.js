﻿(function () {
    appModule.controller('tenant.views.compliaencejobs.otherimage', [
        '$scope', '$uibModalInstance', 'abp.services.app.jobs', 'jobId',
        function ($scope, $uibModalInstance, jobsService, jobId) {
            var vm = this;

            vm.saving = false;
            vm.otherimagelist = {};
            vm.displayVerifyOI = true;
            vm.othercount = 0;

            $scope.myInterval = 5000;
            $scope.noWrapSlides = false;
            $scope.active = 0;
            var currIndex = 0;
            var slides = $scope.slides = [];

            function shuffle(array) {
                var tmp, current, top = array.length;

                if (top) {
                    while (--top) {
                        current = Math.floor(Math.random() * (top + 1));
                        tmp = array[current];
                        array[current] = array[top];
                        array[top] = tmp;
                    }
                }

                return array;
            }

            vm.save = function () {
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.verify = function () {

                abp.message.confirm(
                    app.localize('verifyotherphoto'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            jobsService.updateVerifyIM({ JobId: jobId }).then(function () {
                                abp.notify.info(app.localize('SavedSuccessfully'));
                                $uibModalInstance.close();
                                $state.go('tenant.jobs');
                            }).finally(function () {
                                vm.saving = false;
                            });
                        }
                    }
                );
            }

            function init() {


                jobsService.getOtherPhotos({ JobId: jobId }).then(function (result) {
                    vm.otherimagelist = result.data.items;
                    angular.forEach(result.data.items, function (value, key) {
                        vm.othercount++
                    });

                    angular.forEach(result.data.items, function (value, key) {
                        if (value.imageName != null || value.imageName != 'undefined') {
                            slides.push({
                                id: value.id,
                                photo: value.imageName,
                                longitude: value.longitude,
                                latitude: value.latitude,
                                date: value.creationTime,
                                imagecount: vm.othercount,
                                fileTypeName: value.fileTypeName
                            });
                        }
                    });

                }).finally(function () {
                    vm.loading = false;
                });

                jobsService.getJobsForEdit({ Id: jobId }).then(function (result) {
                    if (result.data.IsVerifyImages || vm.othercount == 0) {
                        vm.displayVerifyOI = false;
                    }
                }).finally(function () {
                    vm.loading = false;
                });
            }

            init();
        }
    ]);
})();