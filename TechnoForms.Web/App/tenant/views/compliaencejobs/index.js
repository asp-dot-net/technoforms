(function () {
	appModule.controller('common.views.compliaencejobs.index', [
		'$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.complaienceJobs', 'abp.services.app.report',
		function ($scope, $state, $uibModal, uiGridConstants, complaienceJobsService, reportService) {
			var vm = this;
			vm.saving = false;

			vm.norecord = false;
			$scope.test = "111";
			vm.streetCity = [];
			vm.streetStateList = [];
			vm.installerList = [];
			vm.systemTypeList = [];
			vm.datetype = [];
			vm.received = [];
			vm.filterJobStatusId = 0;
			$scope.$on('$viewContentLoaded', function () {
				App.initAjax();
			});
			vm.signature = function () {
				$state.go('compliaencesignature', {
					guid: 'eac63eb3-3419-4a11-9402-247dac8576f7'
				});
			};

			//DatePIcker Code Start
			$scope.today = function () {
				$scope.dt = new Date();
			};
			//$scope.today();

			$scope.clear = function () {
				$scope.dt = null;
			};

			$scope.inlineOptions = {
				customClass: getDayClass,
				minDate: new Date(),
				showWeeks: true
			};
			////Disable past dates
			//$scope.dateOptions = {
			//    //dateDisabled: disabled,
			//    formatYear: 'yy',
			//    //maxDate: new Date(2020, 5, 22),
			//    minDate: new Date(),
			//    startingDay: 1
			//};

			//Enable all dates
			$scope.dateOptionsApproval = {
				//dateDisabled: disabled,
				formatYear: 'yy',
				startingDay: 1
			};

			//// Disable weekend selection
			//function disabled(data) {
			//    var date = data.date,
			//        mode = data.mode;
			//    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
			//}

			$scope.open1 = function () {
				$scope.popup1.opened = true;
			};

			$scope.open2 = function () {
				$scope.popup2.opened = true;
			};

			$scope.setDate = function (year, month, day) {
				$scope.dt = new Date(year, month, day);
			};

			$scope.formats = ['d/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			$scope.format = $scope.formats[0];
			$scope.altInputFormats = ['M!/d!/yyyy'];

			$scope.popup1 = {
				opened: false
			};

			$scope.popup2 = {
				opened: false
			};

			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);
			var afterTomorrow = new Date();
			afterTomorrow.setDate(tomorrow.getDate() + 1);
			$scope.events = [
				{
					date: tomorrow,
					status: 'full'
				},
				{
					date: afterTomorrow,
					status: 'partially'
				}
			];

			function getDayClass(data) {
				var date = data.date,
					mode = data.mode;
				if (mode === 'day') {
					var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

					for (var i = 0; i < $scope.events.length; i++) {
						var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

						if (dayToCheck === currentDay) {
							return $scope.events[i].status;
						}
					}
				}

				return '';
			}
			//DatePIcker Code End

			//start from here...
			vm.loading = false;

			vm.permissions = {
				deletejobs: abp.auth.hasPermission('Pages.Tenant.CompliaenceJobs.Delete'),
				editjobs: abp.auth.hasPermission('Pages.Tenant.CompliaenceJobs.Edit'),
				detailjobs: abp.auth.hasPermission('Pages.Tenant.CompliaenceJobs.Detail')
			};

			var requestParams = {
				skipCount: 0,
				maxResultCount: app.consts.grid.defaultPageSize,
				sorting: null
			};

			vm.cjGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				expandableRowTemplate: '/App/tenant/views/compliaencejobs/expandableRowTemplate.html',
				expandableRowHeight: 520,
				expandableRowScope: {
					subGridVariable: 'subGridScopeVariable'
				},
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableColumnMenu: false,
						enableSorting: false,
						headerCellClass: 'centeralign',
						width: 120,
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
							'    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
							'    <ul uib-dropdown-menu>' +
							'      <li><a ng-if="grid.appScope.permissions.detailjobs" ng-click="grid.appScope.detailJobs(row.entity)">' + app.localize('Detail') + '</a></li>' +
							'    </ul>' +
							'  </div>' +
							'</div>'
					},
					{
						name: app.localize(' '),
						enableColumnMenu: false,
						enableSorting: false,
						headerCellClass: 'centeralign',
						width: 20,
						cellTemplate:
							'<div ng-if=\"row.entity.verified\" class="form-group mt10 form-md-floating-label no-hint ng-scope">' +
							'<div class="md-checkbox-list">' +
							'<div class="md-checkbox">' +
							'   <input ng-if="row.entity.verified" class="ngSelectionCheckbox" id="SelectJobCheckBox_{{row.entity.id}}" type="checkbox" ng-click="grid.appScope.getComplaiencevalue(row.entity.id)" name="Get_Complaience_Excel" />' +
							'<label for="SelectJobCheckBox_{{row.entity.id}}">' +
							'<span class="inc"></span>' +
							'<span class="check"></span>' +
							'<span class="box"></span>' +
							'</label>' +
							'</div>' +
							'</div>' +
							'</div>',
						cellClass: 'centeralign'
					},
					{
						name: app.localize('jobid'),
						field: 'formId',
						enableColumnMenu: false,
						headerCellClass: 'centeralign',
						minWidth: 80,
						maxWidth: 100,
						cellClass: 'centeralign textbold'
					},
					{
						name: app.localize('sType'),
						field: 'systemTypeName',
						enableColumnMenu: false,
						headerCellClass: 'centeralign',
						minWidth: 80,
						maxWidth: 100,
						cellClass: 'centeralign textbold'
					},
					{
						name: app.localize('projectno#'),
						field: 'projectNumber',
						headerCellClass: 'centeralign',
						enableColumnMenu: false,
						maxWidth: 120,
						minWidth: 100,
						cellClass: 'centeralign textbold'
						//,
						//cellTooltip: function (row, col) { return row.entity.projectNumber },
					},
					{
						name: app.localize('ownerinfo'),
						field: 'fullName',
						enableColumnMenu: false,
						minWidth: 150,
						cellTemplate: '<div class="jobownerinfo ui-grid-cell-contents"><p><span class="ownername" >{{row.entity.fullName}}</span> <span class="addressphonebox"><span class="streetaddress"tooltip-placement="left" uib-tooltip={{row.entity.streetAddress}}>{{row.entity.streetAddress}}</span></span><p></div>'
					},
					{
						name: app.localize('phone'),
						field: 'phone',
						enableColumnMenu: false,
						minWidth: 80
					},
					{
						name: app.localize('Installer'),
						field: 'installerName',
						enableColumnMenu: false,
						minWidth: 130,
						maxWidth: 200
						//,
						//cellTooltip: function (row, col) { return row.entity.installerName },
					},
					{
						name: app.localize('InstallationDate'),
						field: 'expectedInstallDate',
						enableColumnMenu: false,
						headerCellClass: 'centeralign',
						minWidth: 130,
						maxWidth: 160,
						cellClass: 'centeralign',
						cellFilter: 'date:\'dd-MM-yyyy\''
						//,
						//cellTooltip: function (row, col) { return row.entity.expectedInstallDate },
					},
					{
						name: app.localize('Documents'),
						enableColumnMenu: false,
						headerCellClass: 'centeralign',
						width: 180,
						cellTemplate:
							'<div class="documentbox jobsgridbox">' +
							'<ul>' +
							//'<li><span class="{{row.entity.scanedpanelclass}} alwaysshowme"><a ng-click="grid.appscope.getpaimage(row.entity)" class="link_button">pa</a> <span class="showmeonhover">row.entity.complaiencepamessage</span></span></li>' +
							'<li><span class="{{row.entity.scanedPanelClass}}" tooltip-placement="left" uib-tooltip="{{row.entity.complaiencePAMessage}}"><a ng-click="grid.appScope.GetPAImage(row.entity)" class="link_button"><img src="/App/images/PA.svg" alt="Panel Images"></a></span></li>' +
							'<li><span class="{{row.entity.scanedInverterClass}}" tooltip-placement="left" uib-tooltip="{{row.entity.complaienceINMessage}}"><a ng-click="grid.appScope.GetINImage(row.entity)" class="link_button"><img src="/App/images/IN.svg" alt="Panel Images"></a></span></li>' +
							'<li><span class="{{row.entity.otherImagesClass}}" tooltip-placement="left" uib-tooltip="{{row.entity.complaienceIMMessage}}"><a ng-click="grid.appScope.GetOtherImage(row.entity)" class="link_button"><img src="/App/images/IM.svg" alt="Panel Images"></a></span></li>' +
							'<li><span class="{{row.entity.customerSignatureClass}}" tooltip-placement="left" uib-tooltip="{{row.entity.complaienceCSMessage}}"><a ng-click="grid.appScope.GetCustomersignature(row.entity)" class="link_button"><img src="/App/images/CS.svg" alt="Panel Images"></a></span></li>' +
							'<li><span class="{{row.entity.installerSignatureClass}}" tooltip-placement="left" uib-tooltip="{{row.entity.complaienceISMessage}}"><a ng-click="grid.appScope.GetInstallersignature(row.entity)" class="link_button"><img src="/App/images/IS.svg" alt="Panel Images"></a></span></li>' +
							//'<li><span class="pk dcbox disabled"><a ng-click="grid.appScope.GetInstallersignature(row.entity)" class="link_button">PK</a></span></li>' +
							'<li><span class="{{row.entity.cecClass}}" tooltip-placement="left" uib-tooltip="{{row.entity.complaienceSTCMessage}}"><a ng-click="grid.appScope.GetCEC(row.entity)" class="link_button"><img src="/App/images/CEC.svg" alt="Panel Images"></a></span></li>' +
							'<li><span class="{{row.entity.eleClass}}" tooltip-placement="left" uib-tooltip="{{row.entity.complaienceELEMessage}}"><a ng-click="grid.appScope.GetELE(row.entity)" class="link_button"><img src="/App/images/ELE.svg" alt="Panel Images"></a></span></li>' +
							'<li><span class="{{row.entity.cuckClass}}" tooltip-placement="left" uib-tooltip="{{row.entity.complaienceCUCKMessage}}"><a ng-click="grid.appScope.GetCUCK(row.entity)" class="link_button"><img src="/App/images/CUCK.svg" alt="Panel Images"></a></span></li>' +
							'</ul>' +
							'</div>'
					}
				],
				onRegisterApi: function (gridApi) {
					$scope.gridApi = gridApi;
					$scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
						if (!sortColumns.length || !sortColumns[0].field) {
							requestParams.sorting = null;
						} else {
							requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
						}

						vm.getJobs();
					});
					gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
						requestParams.skipCount = (pageNumber - 1) * pageSize;
						requestParams.maxResultCount = pageSize;

						vm.getJobs();
					});
				},
				data: []
			};

			if (!vm.permissions.detailjobs) {
				vm.cjGridOptions.columnDefs.shift();
			}

			vm.jobids = [];
			vm.getComplaiencevalue = function (jobid) {
				vm.jobids.push(jobid);
			};

			vm.applyForSTC = function () {
				vm.loading = true;
				if (vm.jobids.length > 5) {
					vm.loading = false;
					abp.notify.warn(app.localize('Maximum 5 Jobs Can be Applied At Once'));
				}
				else if (vm.jobids.length > 0) {
					complaienceJobsService.recCheck().then(function (result) {
						if (result.data != null) {
							complaienceJobsService.applyForSTC({ filterJobId: vm.jobids })
							    .then(function () {
							        $state.go('host.stc', {
							        });
							        abp.notify.success(app.localize('SuccessfullyApproved'));
							    }).finally(function () {
							        vm.jobids = [];
							        $("input[name='Get_Complaience_Excel']:checkbox").prop('checked', false);
							        vm.loading = false;
							    });
						}
						else {
							abp.notify.warn(app.localize('PleaseFillRECDetails'));
						}
					}).finally(function () {
						vm.jobids = [];
						$("input[name='Get_Complaience_Excel']:checkbox").prop('checked', false);
						vm.loading = false;
					});
				}
				else {
					vm.loading = false;
					abp.notify.warn(app.localize('Please Select At list One Job'));
				}

			};
			vm.downloadCSV = function () {
				vm.loading = true;
				if (vm.jobids.length > 5) {
					vm.loading = false;
					abp.notify.warn(app.localize('Maximum 5 Jobs Can be Applied At Once'));
				}
				else if (vm.jobids.length > 0) {
					complaienceJobsService.downloadCSV({ filterJobId: vm.jobids })
						.then(function (result) {
							//var url = "../Temp/CompliaenceJob";							//$http.post(url).then(function (res) {							window.location.href = "../Temp/CompliaenceJob/" + result.data;							//});
							abp.notify.success(app.localize('SuccessfullyApproved'));
						}).finally(function () {
							vm.jobids = [];
							$("input[name='Get_Complaience_Excel']:checkbox").prop('checked', false);
							vm.loading = false;
						});
				}
				else {
					vm.loading = false;
					abp.notify.warn(app.localize('Please Select At list One Job'));
				}

			};
			vm.GetPAImage = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/panelimages.cshtml',
					controller: 'tenant.views.jobs.panelimages as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.GetINImage = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/inverterimage.cshtml',
					controller: 'tenant.views.jobs.inverterimage as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.GetOtherImage = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/otherimage.cshtml',
					controller: 'tenant.views.jobs.otherimage as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.GetCustomersignature = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/customersignatureimage.cshtml',
					controller: 'tenant.views.jobs.customersignatureimage as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.GetInstallersignature = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/installersignatureimage.cshtml',
					controller: 'tenant.views.jobs.installersignatureimage as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};

			vm.GetCEC = function (jobs) {
				reportService.cecReport({ Guid: jobs.guid }).then(function () {
					vm.getJobs();
				});
			};
			vm.GetELE = function (jobs) {
				reportService.eleReport({ Guid: jobs.guid }).then(function () {
					vm.getJobs();
				});
			};
			vm.GetCUCK = function (jobs) {
				reportService.cuckReport({ Guid: jobs.guid }).then(function () {
					vm.getJobs();
				});
			};

			vm.cjaudio = function (jobs) {
				$state.go('compliaenceAudio', {
					jobId: jobs.id
				});
			};

			vm.approvePanelImages = function (jobid) {
				abp.message.confirm(
					app.localize('JobPanelApproveWarningMessage'),
					function (isConfirmed) {
						if (isConfirmed) {
							complaienceJobsService.approveComplaiencePAPanels({
								JobId: jobid
							}).then(function () {
								vm.getJobs();
								abp.notify.success(app.localize('SuccessfullyApproved'));
							});
						}
					}
				);
			};
			vm.rejectPanelImages = function (jobid) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/rejectPA.cshtml',
					controller: 'tenant.views.compliaencejobs.rejectPA as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobid
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.approveINImages = function (jobid) {
				abp.message.confirm(
					app.localize('JobINApproveWarningMessage'),
					function (isConfirmed) {
						if (isConfirmed) {
							complaienceJobsService.approveComplaienceInPanels({
								JobId: jobid
							}).then(function () {
								vm.getJobs();
								abp.notify.success(app.localize('SuccessfullyApproved'));
							});
						}
					}
				);
			};
			vm.rejectINImages = function (jobid) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/rejectIN.cshtml',
					controller: 'tenant.views.compliaencejobs.rejectIN as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobid
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.approveIMImages = function (jobid) {
				abp.message.confirm(
					app.localize('JobIMApproveWarningMessage'),
					function (isConfirmed) {
						if (isConfirmed) {
							complaienceJobsService.approveComplaienceIMPanels({
								JobId: jobid
							}).then(function () {
								vm.getJobs();
								abp.notify.success(app.localize('SuccessfullyApproved'));
							});
						}
					}
				);
			};
			vm.rejectIMImages = function (jobid) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/rejectIM.cshtml',
					controller: 'tenant.views.compliaencejobs.rejectIM as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobid
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.approveCSImages = function (jobid) {
				abp.message.confirm(
					app.localize('JobCSApproveWarningMessage'),
					function (isConfirmed) {
						if (isConfirmed) {
							complaienceJobsService.approveComplaienceCS({
								JobId: jobid
							}).then(function () {
								vm.getJobs();
								abp.notify.success(app.localize('SuccessfullyApproved'));
							});
						}
					}
				);
			};
			vm.rejectCSImages = function (jobid) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/rejectCS.cshtml',
					controller: 'tenant.views.compliaencejobs.rejectCS as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobid
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.approveISImages = function (jobid) {
				abp.message.confirm(
					app.localize('JobISApproveWarningMessage'),
					function (isConfirmed) {
						if (isConfirmed) {
							complaienceJobsService.approveComplaienceIS({
								JobId: jobid
							}).then(function () {
								vm.getJobs();
								abp.notify.success(app.localize('SuccessfullyApproved'));
							});
						}
					}
				);
			};
			vm.rejectISImages = function (jobid) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/rejectIS.cshtml',
					controller: 'tenant.views.compliaencejobs.rejectIS as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobid
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.approveELEImages = function (jobid) {
				abp.message.confirm(
					app.localize('JobELEApproveWarningMessage'),
					function (isConfirmed) {
						if (isConfirmed) {
							complaienceJobsService.approveComplaienceELE({
								JobId: jobid
							}).then(function () {
								vm.getJobs();
								abp.notify.success(app.localize('SuccessfullyApproved'));
							});
						}
					}
				);
			};
			vm.rejectELEImages = function (jobid) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/rejectELE.cshtml',
					controller: 'tenant.views.compliaencejobs.rejectELE as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobid
					}
				});

				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.approveCUCKImages = function (jobid) {
				abp.message.confirm(
					app.localize('JobCUCKApproveWarningMessage'),
					function (isConfirmed) {
						if (isConfirmed) {
							complaienceJobsService.approveComplaienceCUCK({
								JobId: jobid
							}).then(function () {
								vm.getJobs();
								abp.notify.success(app.localize('SuccessfullyApproved'));
							});
						}
					}
				);
			};
			vm.rejectCUCKImages = function (jobid) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/rejectCUCK.cshtml',
					controller: 'tenant.views.compliaencejobs.rejectCUCK as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobid
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.approveCECImages = function (jobid) {
				abp.message.confirm(
					app.localize('JobCECApproveWarningMessage'),
					function (isConfirmed) {
						if (isConfirmed) {
							complaienceJobsService.approveComplaienceCEC({
								JobId: jobid
							}).then(function () {
								vm.getJobs();
								abp.notify.success(app.localize('SuccessfullyApproved'));
							});
						}
					}
				);
			};
			vm.rejectCECImages = function (jobid) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/compliaencejobs/rejectCEC.cshtml',
					controller: 'tenant.views.compliaencejobs.rejectCEC as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobid
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};

			vm.getJobs = function () {
				vm.loading = true;
				complaienceJobsService.getComplaienceJobs({
					skipCount: requestParams.skipCount,
					maxResultCount: requestParams.maxResultCount,
					sorting: requestParams.sorting,
					filter: vm.filterText,
					filterFormId: vm.filterFormId,
					filterOwnerName: vm.filterOwnerName,
					filterCity: vm.filterCity,
					filterState: vm.filterState,
					filterInstallerId: vm.filterInstallerId,
					filterSystemtypeId: vm.filterSystemtypeId,
					filterJobStatusId: vm.filterJobStatusId,
					filterStartDate: vm.filterStartDate,
					filterEndDate: vm.filterEndDate,
					filterDateypeId: vm.filterDateypeId,
					filterReceivedId: vm.filterReceivedId,
					filterNotReceivedId: vm.filterNotReceivedId,
					verified: vm.verified
				}).then(function (result) {
					vm.cjGridOptions.totalItems = result.data.totalCount;
					vm.cjGridOptions.data = result.data.items;
					console.log(result.data.items);
					if (result.data.totalCount === 0) {
						//vm.norecord = true;
						abp.notify.info(app.localize('NoRecordFound'));
					}
					else { vm.norecord = false; }
				}).finally(function () {
					vm.loading = false;
				});
			};

			vm.deleteJobs = function (jobs) {
				abp.message.confirm(
					app.localize('JobsDeleteWarningMessage', jobs.firstName),
					function (isConfirmed) {
						if (isConfirmed) {
							complaienceJobsService.deleteJobs({
								id: jobs.id
							}).then(function () {
								vm.getJobs();
								abp.notify.success(app.localize('SuccessfullyDeleted'));
							});
						}
					}
				);
			};
			vm.detailJobs = function (jobs) {
				$state.go('detailcompliaenceJob', {
					jobId: jobs.id
				});
			};

			//vm.exportToExcel = function () {
			//    complaienceJobsService.getAuditLogsToExcel($.extend({}, vm.requestParams, vm.dateRangeModel))
			//        .then(function (result) {
			//            app.downloadTempFile(result.data);
			//        });
			//};

			complaienceJobsService.getAllPostCodes().then(function (result) {
				vm.streetCity = result.data.items;
				setTimeout(function () {
					$('#streetCitySelectionCombobox').selectpicker('refresh');
					$('#streetCitySelectionCombobox').selectpicker('refresh');
				}, 0);
				vm.loading = false;
			});

			complaienceJobsService.getState().then(function (result) {
				vm.streetStateList = result.data.items;
				setTimeout(function () {
					$('#streetStateSelectionCombobox').selectpicker('refresh');
					$('#streetStateSelectionCombobox').selectpicker('refresh');
				}, 0);
				vm.jobs.streetState = result.data.items[0].stateName;
				vm.loading = false;
			});

			complaienceJobsService.getAllInstaller().then(function (result) {
				vm.installerList = result.data.items;
				setTimeout(function () {
					$('#createforSelectionCombobox').selectpicker('refresh');
					$('#createforSelectionCombobox').selectpicker('refresh');
				}, 0);
				vm.loading = false;
			});

			complaienceJobsService.getSystemType().then(function (result) {
				vm.systemTypeList = result.data.items;
				setTimeout(function () {
					$('#systemTypeSelectionCombobox').selectpicker('refresh');
					$('#systemTypeSelectionCombobox').selectpicker('refresh');
				}, 0);
				vm.loading = false;
			});

			vm.datetype = [
				{
					id: '1',
					name: 'Install Date'
				}, {
					id: '2',
					name: 'Panel Expiry'
				}, {
					id: '3',
					name: 'Inverter Expiry'
				}, {
					id: '4',
					name: 'Installer Expiry'
				}];
			vm.received = [
				{
					id: '1',
					name: 'Panel'
				}, {
					id: '2',
					name: 'Inverter'
				}, {
					id: '3',
					name: 'Image'
				}, {
					id: '4',
					name: 'Installer Signature'
				}, {
					id: '5',
					name: 'Owner Signature'
				}];
			vm.jobStatus = function (status_id) {
				vm.filterJobStatusId = status_id;
				vm.getJobs();
			};
			vm.ClearAll = function () {
				vm.filterFormId = null;
				vm.filterOwnerName = null;
				vm.filterCity = null;
				vm.filterState = null;
				vm.filterEndDate = null;
				vm.filterReceivedId = null;

				vm.filterInstallerId = null;
				vm.filterSystemtypeId = null;

				setTimeout(function () {
					$('#createforSelectionCombobox').selectpicker('refresh');
					$('#streetCitySelectionCombobox').selectpicker('refresh');
					$('#streetStateSelectionCombobox').selectpicker('refresh');
					$('#systemTypeSelectionCombobox').selectpicker('refresh');
				}, 0);

				vm.getJobs();
			};

			vm.getJobs();

		}
	]);
})();