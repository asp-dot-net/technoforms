﻿(function () {
    appModule.controller('tenant.views.calendar.index', [
        '$scope', 'abp.services.app.jobs', 'abp.services.app.installer', '$timeout', '$compile',
        function ($scope, jobsService, installerService, $timeout, $compile) {
            var vm = this;
            vm.installerList = [];
            $scope.events = [];
            var eventsjobs = [];
            vm.projectNumber = "";
            vm.installer_Id = "";
            vm.createFor_Id = "";
            vm.jobdetails = "";

            vm.JobId = "";
            vm.PNumber = "";
            vm.Name = "";
            vm.Address = "";
            vm.Phone = "";
            vm.Installer = "";

            vm.showmodalpopup = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            jobsService.getAllInstaller().then(function (result) {
                vm.installerList = result.data.items;
                setTimeout(function () {
                    $('#createforSelectionCombobox').selectpicker('refresh');
                    $('#createforSelectionCombobox').selectpicker('refresh');
                }, 0);

                vm.jobs.createFor_Id = vm.installerList[0];

                vm.loading = false;
            });
            vm.getInstallerDetails = function (createFor_Id) {
                if (createFor_Id != null) {
                    installerService.getInstallerEmployeInstallerByInstallerId({ id: createFor_Id }).then(function (result) {
                        vm.installerEmployeeList = result.data.items;
                        setTimeout(function () {
                            $('#installerSelectionCombobox').selectpicker('refresh');
                            $('#installerSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });
                }
            }

            vm.BindJobs = function (customerId, installerId, projectNumber) {

                var sources = $scope.events;
                angular.forEach(sources, function (value, key) {
                    sources.splice(key);
                });

                jobsService.getCalenderJobs({
                    CreateFor_Id: customerId,
                    Installer_Id: installerId,
                    projectNumber: projectNumber
                }).then(function (result) {
                    var job = result.data.items;
                    angular.forEach(job, function (value, key) {
                        if (value.expectedInstallDate != null || value.expectedInstallDate != 'undefined') {
                            $scope.events.push({
                                title: 'Job Id : ' + value.id,
                                //title: 'Job Id : ' + value.id + '\n' + 'Project No :' + value.projectNumber + '\n' + 'Cust Name : ' + value.firstName + " " + value.lastName + '\n' + 'Address : ' + value.installationStreetNo + " " + value.installationStreetName + " " + value.installationStreetType + '\n' + value.installationStreetCity + ',' + value.installationStreetState + '-' + value.installationStreetPostCode + '\n' + 'Mobile : ' + value.phone + '\n' + 'Installer : ' + value.installerName,
                                start: new Date(value.year, value.month - 1, value.day),
                                allDay: true,
                                Id: value.id,
                                ProjectNumber: value.projectNumber,
                                Name: value.firstName + " " + value.lastName,
                                Address: value.installationStreetNo + " " + value.installationStreetName + " " + value.installationStreetType + '\n' + value.installationStreetCity + ',' + value.installationStreetState + '-' + value.installationStreetPostCode,
                                PhoneNo: value.phone,
                                Installer: value.installerName
                            });
                        }
                    });
                });
            };

            ///* event source that pulls from google.com */
            ///* event source that contains custom events on the scope */
            //$scope.events = [
            //    { title: 'All Day Event', start: new Date(y, m, 1) },
            //    { title: 'Long Event', start: new Date(y, m, d - 5), end: new Date(y, m, d - 2) },
            //    { id: 999, title: 'Repeating Event', start: new Date(y, m, d - 3, 16, 0), allDay: false },
            //    { id: 999, title: 'Repeating Event', start: new Date(y, m, d + 4, 16, 0), allDay: false },
            //    { title: 'Birthday Party', start: new Date(y, m, d + 1, 19, 0), end: new Date(y, m, d + 1, 22, 30), allDay: false },
            //    { title: 'Click for Google', start: new Date(y, m, 28), end: new Date(y, m, 29), url: 'http://google.com/' }
            //];
            ///* event source that calls a function on every view switch */
            ///* add and removes an event source of choice */
            $scope.addRemoveEventSource = function (sources, source) {
                var canAdd = 0;
                angular.forEach(sources, function (value, key) {
                    if (sources[key] === source) {
                        sources.splice(key, 1);
                        canAdd = 1;
                    }
                });
                if (canAdd === 0) {
                    sources.push(source);
                }
            };
            /* add custom event*/
            $scope.addEvent = function () {
                $scope.events.push({
                    title: 'Open Sesame',
                    start: new Date(y, m, 28),
                    end: new Date(y, m, 29),
                    className: ['openSesame']
                });
            };
            /* remove event */
            $scope.remove = function (index) {
                $scope.events.splice(index, 1);
            };
            /* Change View */
            //$scope.changeView = function (view, calendar) {
            //    console.log($scope.uiConfig.calendar);
            //    $scope.uiConfig.calendar.renderView(view);
            //};
            /* Change View */
            $scope.renderCalendar = function (calendar) {
                $timeout(function () {
                    if ($scope.uiConfig.calendar[calendar]) {
                        $scope.uiConfig.calendar[calendar].fullCalendar('render');
                    }
                });
            };
            /* Render Tooltip */
            $scope.eventRender = function (event, element, view) {
                element.attr({
                    'tooltip': event.title,
                    'tooltip-append-to-body': true
                });
                $compile(element)($scope);
            };
            /* config object */
            $scope.uiConfig = {
                calendar: {
                    height: 450,
                    editable: true,
                    header: {
                        left: 'title',
                        center: '',
                        right: 'month,basicWeek,basicDay,today prev,next',
                        month: 'month',
                        week: 'basicWeek',
                        day: 'basicDay'
                    }
                    ,
                    viewRender: function () {
                        var projectnumber = "";
                        if (vm.projectNumber != "") {
                            projectnumber = vm.projectNumber;
                        }
                        var InstallerId = 0;
                        if (vm.installer_Id != "") {
                            InstallerId = vm.installer_Id;
                        }
                        vm.BindJobs(0, InstallerId, projectnumber);
                    },
                    eventClick: function (event) {

                        vm.JobId = "";
                        vm.ProjectNumber = "";
                        vm.Name = "";
                        vm.Address = "";
                        vm.Phone = "";
                        vm.Installer = "";

                        $('#fullCalModal').modal();
                        vm.JobId = event.Id;
                        vm.PNumber = event.ProjectNumber;
                        vm.Name = event.Name;
                        vm.Address = event.Address;
                        vm.Phone = event.PhoneNo;
                        vm.Installer = event.Installer;

                        //jobsService.getJobsForEdit({
                        //    id: event.Id
                        //}).then(function (result) {
                        //    console.log(result.data.mobile);
                        //        vm.PNumber = result.data.projectNumber,
                        //        vm.Name = result.data.firstName + " " + result.data.lastName,
                        //        vm.Address = result.data.installationStreetNo + " " + result.data.installationStreetName + " " + result.data.installationStreetType + '\n' + result.data.installationStreetCity + ',' + result.data.installationStreetState + '-' + result.data.installationStreetPostCode,
                        //        vm.Phone = result.data.mobile,
                        //        vm.Installer = result.data.installerName
                        //});
                        //vm.showmodalpopup = true;
                    }
                }

            };
            vm.search = function () {
                var sources = $scope.events;
                angular.forEach(sources, function (value, key) {
                    sources.splice(key);
                });

                var projectnumber = "";
                if (vm.projectNumber != "") {
                    projectnumber = vm.projectNumber;
                }
                var InstallerId = 0;
                if (vm.installer_Id != "") {
                    InstallerId = vm.installer_Id;
                }
                vm.BindJobs(0, InstallerId, projectnumber);

                //  $scope.renderCalendar('myCalendar3');
                $scope.eventSources2 = [$scope.events];

            }


            /* event sources array*/
            //$scope.events = eventsjobs;

            $scope.eventSources2 = [$scope.events];
            console.log($scope.eventSources2);
        }
    ]);
})();