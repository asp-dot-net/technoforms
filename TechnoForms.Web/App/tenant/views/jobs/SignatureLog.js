﻿(function () {
    appModule.controller('tenant.views.jobs.SignatureLog', [
        '$scope', '$state', '$stateParams', 'abp.services.app.jobs',
        function ($scope, $state, $stateParams, jobsService) {
            var vm = this;
            vm.saving = false;
            vm.jobId = $stateParams.jobId;
           

            vm.signaturerequest_List = [];
            vm.getMessage = function () {
                vm.loading = true;
                jobsService.getCustomerSignatureLog({
                    Id: vm.jobId
                }).then(function (result) {
                    vm.signaturerequest_List = result.data.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.cancel = function () {
                $state.go('tenant.jobs');
            };

            vm.getMessage();
            //function init() {

            //}

            //init();
        }
    ]);
})();