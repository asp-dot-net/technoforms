﻿/// <reference path="forms/inverterenergysystem.js" />
(function () {
    appModule.controller('tenant.views.jobs.formList', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.jobs', 'uiGridConstants',
        function ($scope, $state, $stateParams, $uibModal, jobsService, uiGridConstants) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            //Grid data
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            
            vm.jobId = $stateParams.jobId;

            vm.loading = false;

            vm.openNewTab = function (test) {
                window.open(abp.appPath + 'Temp/Photos/' + test + '?v=' + new Date().valueOf(), '_blank');
            };

            function initialize() {
                function reloadWhenChange(variableName) {
                    $scope.$watch(variableName, function (newValue, oldValue) {
                        if (newValue == oldValue) {
                            return;
                        }

                        $state.go('formList', {
                            jobId: jobs.guid
                        });
                    });
                }
                reloadWhenChange('vm.jobId');
            }

            vm.getDocumentList = function () {
                vm.loading = true;
                jobsService.getJobDetail({
                    guid: vm.jobId
                }).then(function (result) {
                    vm.job = result.data;
                });

            };
             
            vm.ElectricalWorksRequest = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/tenant/views/jobs/Forms/electricalWorksRequest.cshtml',
                    controller: 'tenant.views.jobs.forms.electricalWorksRequest as vm',
                    backdrop: 'static',
                    windowClass: 'app-modal-window'
                });
                modalInstance.result.then(function (result) {
                    //vm.getJobs();
                });
                //$state.go('InverterEnergySystem', {
                //    jobId: vm.jobId
                //});
            };

            vm.UnitedEnergyInverter = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/tenant/views/jobs/Forms/unitedEnergyInverter.cshtml',
                    controller: 'tenant.views.jobs.forms.unitedEnergyInverter as vm',
                    backdrop: 'static',
                    windowClass: 'app-modal-window'
                });
                modalInstance.result.then(function (result) {
                    //vm.getJobs();
                });
                //$state.go('InverterEnergySystem', {
                //    jobId: vm.jobId
                //});
            };

            vm.UpgradeExistingPV = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/tenant/views/jobs/Forms/upgradeExistingPV.cshtml',
                    controller: 'tenant.views.jobs.forms.upgradeExistingPV as vm',
                    backdrop: 'static',
                    windowClass: 'app-modal-window'
                });
                modalInstance.result.then(function (result) {
                    //vm.getJobs();
                });
                //$state.go('InverterEnergySystem', {
                //    jobId: vm.jobId
                //});
            };

            vm.MicroEmbeddedGenerator = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/tenant/views/jobs/Forms/microEmbeddedGenerator.cshtml',
                    controller: 'tenant.views.jobs.forms.microEmbeddedGenerator as vm',
                    backdrop: 'static',
                    windowClass: 'app-modal-window'
                });
                modalInstance.result.then(function (result) {
                    //vm.getJobs();
                });
                //$state.go('InverterEnergySystem', {
                //    jobId: vm.jobId
                //});
            };

            vm.InverterEnergySystem = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/tenant/views/jobs/Forms/inverterEnergySystem.cshtml',
                    controller: 'tenant.views.jobs.forms.inverterEnergySystem as vm',
                    backdrop: 'static',
                    windowClass: 'app-modal-window'
                });
                modalInstance.result.then(function (result) {
                    //vm.getJobs();
                });
                //$state.go('InverterEnergySystem', {
                //    jobId: vm.jobId
                //});
            };

            vm.AusNet = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/tenant/views/jobs/Forms/ausNet.cshtml',
                    controller: 'tenant.views.jobs.forms.ausNet as vm',
                    backdrop: 'static',
                    windowClass: 'app-modal-window'
                });
                modalInstance.result.then(function (result) {
                    //vm.getJobs();
                });
                //$state.go('InverterEnergySystem', {
                //    jobId: vm.jobId
                //});
            };

            vm.backToJob = function () {
                $state.go('tenant.jobs', {
                    jobId: vm.jobId
                });
            };

            initialize();
            vm.getDocumentList();
        }
    ]);
})();