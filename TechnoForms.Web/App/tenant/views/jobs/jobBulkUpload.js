﻿(function () {
    appModule.controller('tenant.views.jobs.jobBulkUpload', [
        '$scope', '$state', '$http', '$rootScope', '$uibModalInstance', 'abp.services.app.jobs',
        function ($scope, $state, $http, $rootScope, $uibModalInstance, jobsService) {
            var vm = this;
            
            vm.saving = false;
            vm.job = null;

            vm.uploadFile = function (file) {
                vm.saving = true;
                var files = $('#filetoupload')[0].files[0];
                var uploadUrl = "../BulkDataUpload/JobDataAsync";
                var fd = new FormData();
                fd.append('file', files);                
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).then(function () {     
                    $scope.$parent.$emit('eventJobDataImported', null);
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $uibModalInstance.dismiss();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            
            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {
                jobsService.getJobBulkUpload({}).then(function (result) {
                    vm.job = result.data;
                });
            }
            init();
        }
    ]);
})();