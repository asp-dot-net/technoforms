﻿(function () {
    appModule.controller('tenant.views.jobs.editSerialNumber', [
        '$scope', '$state', '$uibModalInstance', 'abp.services.app.jobs', 'panId', 'tenantid',
        function ($scope, $state, $uibModalInstance, jobsService, panId, tenantid) {
            var vm = this;

            vm.saving = false;
            vm.scanedPanel = {};
            vm.save = function () {
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {
                jobsService.getScanedPanelsById({
                    Id: panId,
                    TenantId: tenantid
                }).then(function (result) {
                    vm.scanedPanel = result.data;
                });
            }

            vm.save = function () {
                vm.saving = true;
                vm.scanedPanel.id = panId;
                jobsService.updateJobScanedPanels(vm.scanedPanel)
                    .then(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                        $state.go('tenant.jobs');
                    }).finally(function () {
                        vm.saving = false;
                    });
            };

            init();
        }
    ]);
})();