﻿(function () {
    appModule.controller('tenant.views.jobs.uploadDocument', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.jobs', 'uiGridConstants',
        function ($scope, $state, $stateParams, $uibModal, jobsService, uiGridConstants) {
            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            //Grid data
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.jobId = $stateParams.jobId;

            vm.loading = false;

            vm.openNewTab = function (test) {
                window.open(abp.appPath + 'Temp/' + vm.tenantName + '/' + vm.formId + '/Photos/' + test + '?v=' + new Date().valueOf(), '_blank');
            };

            vm.uploadDocsGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        width: 150,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-click="grid.appScope.deleteDocs(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        minWidth:200,
                        name: app.localize('Document'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '<a id="Logophoto" ng-click="grid.appScope.openNewTab(row.entity.imageName)"><img src="/Common/Images/icon_downloadimage2.png" /></a>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Type'),
                        minWidth:200,
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'fileTypeName'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getDocumentList();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getDocumentList();
                    });
                },
                data: []
            };

            function initialize() {
                function reloadWhenChange(variableName) {
                    $scope.$watch(variableName, function (newValue, oldValue) {
                        if (newValue == oldValue) {
                            return;
                        }

                        $state.go('uploadDocument', {
                            jobId: vm.jobId
                        });
                    });
                }
                reloadWhenChange('vm.jobId');
            }
            jobsService.getTenantName().then(function (result) {
                vm.tenantName = result.data;
            });
            jobsService.getFormId(vm.jobId).then(function (result) {
                vm.formId = result.data;
            });
            vm.getDocumentList = function () {
                vm.loading = true;
                jobsService.getJobDetail({
                    guid: vm.jobId
                }).then(function (result) {
                    vm.job = result.data;
                });

                jobsService.getDocumentList({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    guid: vm.jobId
                }).then(function (result) {
                    vm.uploadDocsGridOptions.totalItems = result.data.totalCount;
                    vm.uploadDocsGridOptions.data = result.data.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.uploadDocs = function () {
                $state.go('createDocs', {
                    jobId: vm.jobId
                });
            };

            vm.backToJob = function () {
                $state.go('tenant.jobs', {
                    jobId: vm.jobId
                });
            };

            vm.deleteDocs = function (docs) {
                abp.message.confirm(
                    app.localize('DocumentDeleteWarningMessage', docs.fileTypeName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            jobsService.deleteDocs({
                                id: docs.id
                            }).then(function () {
                                vm.getDocumentList();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            initialize();
            vm.getDocumentList();
        }
    ]);
})();