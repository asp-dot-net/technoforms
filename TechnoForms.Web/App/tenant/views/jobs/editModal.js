﻿(function () {
	appModule.controller('tenant.views.jobs.editModal', [
		'$scope', '$state', '$stateParams', 'abp.services.app.jobs', 'abp.services.app.state', 'abp.services.app.installer',
		function ($scope, $state, $stateParams, jobsService, stateService, installerService) {
			var vm = this;

			vm.saving = false;
			vm.emailFormat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			vm.phonepattern = /^(07|03|08|04|13|18)[\d]{8}/;
			vm.jobs = {};
			vm.streetNames = [];
			vm.streetType = [];
			vm.postalAddressTypes = [];
			vm.postalDeliveryType = [];
			vm.unitType = [];
			vm.installationUnitType = [];
			vm.installationameasHouse = true;
			vm.houseType = [];
			vm.roofType = [];
			vm.panelList = [];
			vm.modelList = [];
			vm.inverterList = [];
			vm.invertermodelList = [];

			vm.solarTypeList = [];
			vm.systemTypeList = [];
			vm.roofFacingPanelsList = [];
			vm.sugatthisaddressList = [];
			vm.locationOfNewSystemList = [];

			vm.typeofsystemList = [];
			vm.typeofconnectionList = [];
			vm.mountingtypeList = [];
			vm.systemmountingtypeList = [];
			vm.recyearList = [];
			vm.streetStateList = [];
			vm.installationstreetStateList = [];
			vm.installationstreetPostcodes = [];

			vm.installerList = [];
			vm.installerEmployeeList = [];
			vm.installerDesignerList = [];
			vm.installerElectricianList = [];
			vm.jobstatusList = [];

			vm.systemTypeVal = false;
			vm.IsGoogleAddress = true;

			vm.jobId = $stateParams.jobId;
			vm.suburb = "";
			vm.activeTabIndex = 0;
			//vm.sitesdetailclass = "disabled";
			//vm.paneldetailclass = "disabled";
			//vm.inverterdetailclass = "disabled";
			//vm.installationdetailclass = "disabled";
			//vm.recdetailclass = "disabled";

			vm.setActiveTab = function (activeindex) {
				if (activeindex == 1) {
					if (vm.jobs.firstName != null && vm.jobs.lastName != null && vm.jobs.emailId != null && vm.jobs.phone != null && vm.jobs.streetNo != null && vm.jobs.streetName != null && vm.jobs.streetType != null && vm.jobs.streetCity != null && vm.jobs.streetState != null && vm.jobs.streetPostCode != null) {
						vm.activeTabIndex = activeindex;
					}
				}
				if (activeindex == 2) {
					if (vm.jobs.houseType != null && vm.jobs.roofType != null && vm.jobs.nmiNo != null) {
						// vm.sitesdetailclass = "uib-tab nav-item ng-scope ng-isolate-scope";
						vm.activeTabIndex = activeindex;
					}
					//else {
					//    vm.sitesdetailclass = "uib-tab nav-item ng-scope ng-isolate-scope disabled";
					//}
				}
				if (activeindex == 3) {
					if (vm.jobs.panelName != null && vm.jobs.brandName != null && vm.jobs.panel_id != null && vm.jobs.noOfPanels != null) {
						// vm.paneldetailclass = "uib-tab nav-item ng-scope ng-isolate-scope";
						vm.activeTabIndex = activeindex;
					}
					//else {
					//    vm.paneldetailclass = "uib-tab nav-item ng-scope ng-isolate-scope disabled";
					//}
				}
				if (activeindex == 4) {
					if (vm.jobs.inverterName != null && vm.jobs.inverterBrandName != null && vm.jobs.inverter_id != null && vm.jobs.noOfInverter != null) {
						vm.activeTabIndex = activeindex;
						// vm.inverterdetailclass = "uib-tab nav-item ng-scope ng-isolate-scope";
					}
					//else {
					//    vm.inverterdetailclass = "uib-tab nav-item ng-scope ng-isolate-scope disabled";
					//}
				}
				if (activeindex == 5) {
					if (vm.jobs.expectedInstallDate != null && vm.jobs.createFor_Id != null && vm.jobs.installer_Id != null && vm.jobs.designer_Id != null && vm.jobs.electrician_Id != null && vm.jobs.projectNumber != null) {
						vm.activeTabIndex = activeindex;
						//vm.installationdetailclass = "uib-tab nav-item ng-scope ng-isolate-scope";
					}
					//else {
					//    vm.installationdetailclass = "uib-tab nav-item ng-scope ng-isolate-scope disabled";
					//}
				}
			}
			//vm.setActiveTab = function (activeindex) {
			//    if (activeindex == 1) {
			//        if (vm.jobs.firstName != null && vm.jobs.lastName != null && vm.jobs.emailId != null && vm.jobs.phone != null && vm.jobs.streetNo != null && vm.jobs.streetName != null && vm.jobs.streetType != null && vm.jobs.streetCity != null && vm.jobs.streetState != null && vm.jobs.streetPostCode != null) {
			//            vm.activeTabIndex = activeindex;
			//        }
			//    }
			//    if (activeindex == 2) {
			//        if (vm.jobs.houseType != null && vm.jobs.roofType != null && vm.jobs.nmiNo != null) {
			//            vm.sitesdetailclass = "uib-tab nav-item ng-scope ng-isolate-scope";
			//            vm.activeTabIndex = activeindex;
			//        }
			//    }
			//    if (activeindex == 3) {
			//        if (vm.jobs.panelName != null && vm.jobs.brandName != null && vm.jobs.panel_id != null && vm.jobs.noOfPanels != null) {
			//            vm.activeTabIndex = activeindex;
			//        }
			//    }
			//    if (activeindex == 4) {
			//        if (vm.jobs.inverterName != null && vm.jobs.inverterBrandName != null && vm.jobs.inverter_id != null && vm.jobs.noOfInverter != null) {
			//            vm.activeTabIndex = activeindex;
			//        }
			//    }
			//    if (activeindex == 5) {
			//        if (vm.jobs.expectedInstallDate != null && vm.jobs.createFor_Id != null && vm.jobs.installer_Id != null && vm.jobs.designer_Id != null && vm.jobs.electrician_Id != null && vm.jobs.projectNumber != null) {
			//            vm.activeTabIndex = activeindex;
			//        }
			//    }
			//}

			//DatePIcker Code Start
			$scope.today = function () {
				$scope.dt = new Date();
			};
			//$scope.today();

			$scope.clear = function () {
				$scope.dt = null;
			};

			$scope.inlineOptions = {
				customClass: getDayClass,
				//minDate: new Date(),
				showWeeks: true
			};

			//Disable past dates
			$scope.dateOptions = {
				//dateDisabled: disabled,
				formatYear: 'yy',
				//maxDate: new Date(2020, 5, 22),
				//minDate: new Date(),
				startingDay: 1
			};

			// Disable weekend selection
			function disabled(data) {
				var date = data.date,
					mode = data.mode;
				return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
			}

			//$scope.toggleMin = function () {
			//    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
			//    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
			//};

			//$scope.toggleMin();

			$scope.open1 = function () {
				$scope.popup1.opened = true;
			};

			$scope.open2 = function () {
				$scope.popup2.opened = true;
			};

			$scope.setDate = function (year, month, day) {
				$scope.dt = new Date(year, month, day);
			};

			$scope.formats = ['d/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			$scope.format = $scope.formats[0];
			$scope.altInputFormats = ['M!/d!/yyyy'];

			$scope.popup1 = {
				opened: false
			};

			$scope.popup2 = {
				opened: false
			};

			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);
			var afterTomorrow = new Date();
			afterTomorrow.setDate(tomorrow.getDate() + 1);
			$scope.events = [
				{
					date: tomorrow,
					status: 'full'
				},
				{
					date: afterTomorrow,
					status: 'partially'
				}
			];

			function getDayClass(data) {
				var date = data.date,
					mode = data.mode;
				if (mode === 'day') {
					var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

					for (var i = 0; i < $scope.events.length; i++) {
						var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

						if (dayToCheck === currentDay) {
							return $scope.events[i].status;
						}
					}
				}

				return '';
			}
			//DatePIcker Code End

			vm.CheckNumber = function () {
				if (isNaN(event.key) || event.key === ' ' || event.key === '') {
					event.returnValue = '';
				}

			};
			//vm.getStreetAddress = function () {
			//	var unittype = "";
			//	var unitNo = "";
			//	var streetNo = "";
			//	var streetName = "";
			//	var streetType = "";
			//	if (vm.jobs.unitType != null) {
			//		unittype = vm.jobs.unitType;
			//	}
			//	if (vm.jobs.unitNo != null) {
			//		unitNo = vm.jobs.unitNo;
			//	}
			//	if (vm.jobs.streetNo != null) {
			//		streetNo = vm.jobs.streetNo;
			//	}
			//	if (vm.jobs.streetName != null) {
			//		streetName = vm.jobs.streetName;
			//	}
			//	if (vm.jobs.streetType != null) {
			//		streetType = vm.jobs.streetType;
			//	}
			//	vm.jobs.streetAddress = unittype + " " + unitNo + " " + streetNo + " " + streetName + " " + streetType;
			//};

			//vm.getInstallationStreetAddress = function () {
			//	var unittype = "";
			//	var unitNo = "";
			//	var streetNo = "";
			//	var streetName = "";
			//	var streetType = "";
			//	if (vm.jobs.installationUnitType != null) {
			//		unittype = vm.jobs.installationUnitType;
			//	}
			//	if (vm.jobs.installationUnitNo != null) {
			//		unitNo = vm.jobs.installationUnitNo;
			//	}
			//	if (vm.jobs.installationStreetNo != null) {
			//		streetNo = vm.jobs.installationStreetNo;
			//	}
			//	if (vm.jobs.installationstreetName != null) {
			//		streetName = vm.jobs.installationstreetName;
			//	}
			//	if (vm.jobs.installationStreetType != null) {
			//		streetType = vm.jobs.installationStreetType;
			//	}
			//	vm.jobs.installationStreetAddress = unittype + " " + unitNo + " " + streetNo + " " + streetName + " " + streetType;
			//};

			vm.projectnumberchange = function () {
				jobsService.getJobsByProjectNumber({ ProjectNumber: vm.jobs.projectNumber, Installer_Id: vm.jobs.installer_Id, ExpectedInstallDate: vm.jobs.expectedInstallDate, Id: vm.jobId }).then(function (result) {
					if (result.data.status == false) {
						vm.saving = false;
						abp.notify.info(app.localize(result.data.message));
					}
				});
			}
			vm.expectedinstalldatechange = function () {
				jobsService.getJobsByProjectNumberAndInstallerId({ ProjectNumber: vm.jobs.projectNumber, Installer_Id: vm.jobs.installer_Id, ExpectedInstallDate: vm.jobs.expectedInstallDate, Id: vm.jobId }).then(function (result) {
					if (result.data.status == false) {
						vm.saving = false;
						abp.notify.info(app.localize(result.data.message));
					}
				});
			}

			vm.save = function () {
				vm.jobs.streetNo = $("#street_number").val();
				vm.jobs.streetName = $("#routeName").val();
				vm.jobs.streetType = $("#routeType").val();
				vm.jobs.streetCity = $("#locality").val();
				var value = vm.jobs.streetCity.split("|");
				vm.jobs.suburb = value[0].trim();
				vm.jobs.streetState = $("#administrative_area_level_1").val();
				vm.jobs.streetPostCode = $("#postal_code").val();

				var unittype = "";
				var unitNo = "";
				var streetNo = "";
				var streetName = "";
				var streetType = "";
				if (vm.jobs.unitType != null) {
					unittype = vm.jobs.unitType;
				}
				if (vm.jobs.unitNo != null) {
					unitNo = vm.jobs.unitNo;
				}
				if (vm.jobs.streetNo != null) {
					streetNo = vm.jobs.streetNo;
				}
				if (vm.jobs.streetName != null) {
					streetName = vm.jobs.streetName;
				}
				if (vm.jobs.streetType != null) {
					streetType = vm.jobs.streetType;
				}
				vm.jobs.streetAddress = unittype + " " + unitNo + " " + streetNo + " " + streetName + " " + streetType;

				jobsService.getJobsByProjectNumberAndInstallerId({ ProjectNumber: vm.jobs.projectNumber, Installer_Id: vm.jobs.installer_Id, ExpectedInstallDate: vm.jobs.expectedInstallDate, Id: vm.jobId }).then(function (result) {
					if (result.data.status == true) {
						if (vm.jobs.installationAddresssameasHouseAddress == true) {
							vm.jobs.installationStreetAddress = vm.jobs.streetAddress;
							vm.jobs.installationUnitNo = vm.jobs.unitNo;
							vm.jobs.installationUnitType = vm.jobs.unitType;
							vm.jobs.installationStreetNo = vm.jobs.streetNo;
							vm.jobs.installationStreetName = vm.jobs.streetName;
							vm.jobs.installationStreetType = vm.jobs.streetType;
							vm.jobs.installationStreetCity = vm.jobs.streetCity;
							vm.jobs.InstallationSuburb_Id = vm.jobs.Suburb_Id;
							vm.jobs.installationStreetState = vm.jobs.streetState;
							vm.jobs.installationStreetPostCode = vm.jobs.streetPostCode;
						}

						jobsService.getPanelListById({ filter: vm.jobs.panel_id }).then(function (result) {
							vm.jobs.panelModel = result.data.items[0].modelNo;
						});

						vm.saving = true;
						jobsService.updateJobs(vm.jobs)
							.then(function () {
								$state.go('tenant.jobs', {
								});
								abp.notify.info(app.localize('SavedSuccessfully'));
								$uibModalInstance.close();
							}).finally(function () {
								vm.saving = false;
							});
					}
					else {
						abp.notify.info(app.localize(result.data.message));
					}
				});

			};

			vm.cancel = function () {
				$state.go('tenant.jobs', {
				});
			};

			vm.getinstallationAddresssameasHouseAddress = function () {
				vm.installationameasHouse = vm.jobs.installationAddresssameasHouseAddress;
			};

			vm.getPanelName = function (item) {
				vm.jobs.brandName = item;
				jobsService.getModelNoList({ filter: item }).then(function (result) {
					vm.modelList = result.data.items;
					setTimeout(function () {
						$('#modelSelectionCombobox').selectpicker('refresh');
						$('#modelSelectionCombobox').selectpicker('refresh');
					}, 0);
					vm.loading = false;
				});
			};
			vm.GetSize = function (panel_id) {
				if (panel_id != null) {
					jobsService.getPanelListById({ filter: panel_id }).then(function (result) {
						vm.jobs.panelSize = result.data.items[0].size;
						vm.getSystemCapacity(vm.jobs.noofPanels, vm.jobs.panelSize);
						vm.loading = false;
					});
					jobsService.getExpectedSTC({ capacity: vm.jobs.systemCapacity, postcode: vm.jobs.streetPostCode }).then(function (result) {
						vm.jobs.expectedSTC = result.data;
					});
				}
			};
			vm.getPostCodes = function (streetCity) {
				if (streetCity != null) {
					jobsService.getPostCodesListBySuburb({ id: streetCity }).then(function (result) {
						vm.jobs.streetCity = result.data.items[0].suburb;
						vm.streetPostcodes = result.data.items;
						setTimeout(function () {
							$('#streetPostCodeSelectionCombobox').selectpicker('refresh');
							$('#streetPostCodeSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.streetStateList = result.data.items;
						setTimeout(function () {
							$('#streetStateSelectionCombobox').selectpicker('refresh');
							$('#streetStateSelectionCombobox').selectpicker('refresh');
						}, 0);

						vm.jobs.streetState = result.data.items[0].stateName;
						vm.jobs.streetPostCode = result.data.items[0].postcode;

						vm.loading = false;
					});
				}
			};
			vm.getInstallationPostCodes = function (installationstreetCity) {
				if (installationstreetCity != null) {
					jobsService.getPostCodesListBySuburb({ id: installationstreetCity }).then(function (result) {
						vm.jobs.installationStreetCity = result.data.items[0].suburb;
						vm.installationstreetPostcodes = result.data.items;
						setTimeout(function () {
							$('#installationStreetPostCodeSelectionCombobox').selectpicker('refresh');
							$('#installationStreetPostCodeSelectionCombobox').selectpicker('refresh');
						}, 0);

						vm.installationstreetStateList = result.data.items;
						setTimeout(function () {
							$('#installationStreetStateSelectionCombobox').selectpicker('refresh');
							$('#installationStreetStateSelectionCombobox').selectpicker('refresh');
						}, 0);

						vm.jobs.installationStreetState = result.data.items[0].stateName;
						vm.jobs.installationStreetPostCode = result.data.items[0].postcode;

						vm.loading = false;
					});
				}
			};
			vm.getInverterName = function (item) {
				vm.jobs.inverterBrandName = item;

				jobsService.getInverterModelNoList({ filter: item }).then(function (result) {
					vm.invertermodelList = result.data.items;
					setTimeout(function () {
						$('#invertermodelSelectionCombobox').selectpicker('refresh');
						$('#invertermodelSelectionCombobox').selectpicker('refresh');
					}, 0);
					vm.loading = false;
				});

			};

			vm.getinverterSeries = function (inverter_id) {
				if (inverter_id != null) {
					jobsService.getInverterListById({ filter: inverter_id }).then(function (result) {
						vm.jobs.inverterSeries = result.data.items[0].series;
						vm.loading = false;
					});
				}
			};

			vm.getInstallerDetails = function (createFor_Id) {

				if (createFor_Id != null) {
					jobsService.getInstallerEmployeInstallerByInstallerId({ id: createFor_Id }).then(function (result) {
						vm.installerEmployeeList = result.data.items;
						vm.jobs.installer_Id = result.data.items[0].id;
						setTimeout(function () {
							$('#installerSelectionCombobox').selectpicker('refresh');
							$('#installerSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});

					jobsService.getInstallerEmployeDesignerByInstallerId({ id: createFor_Id }).then(function (result) {
						vm.installerDesignerList = result.data.items;
						vm.jobs.designer_Id = result.data.items[0].id;
						setTimeout(function () {
							$('#installerdesignerSelectionCombobox').selectpicker('refresh');
							$('#installerdesignerSelectionCombobox').selectpicker('refresh');
						}, 0);


						vm.loading = false;
					});

					jobsService.getInstallerEmployeElectricianByInstallerId({ id: createFor_Id }).then(function (result) {
						vm.installerElectricianList = result.data.items;
						vm.jobs.electrician_Id = result.data.items[0].id;
						setTimeout(function () {
							$('#installerelectricianSelectionCombobox').selectpicker('refresh');
							$('#installerelectricianSelectionCombobox').selectpicker('refresh');
						}, 0);


						vm.loading = false;
					});
				}
			};

			vm.getSystemCapacity = function (noofPanels, panelSize) {
				jobsService.getExpectedSTC({ capacity: vm.jobs.systemCapacity, postcode: vm.jobs.streetPostCode }).then(function (result) {
					vm.jobs.expectedSTC = result.data;
				});
				var systemcapacity = (panelSize * noofPanels / 1000);
				vm.jobs.systemCapacity = systemcapacity;

			};
			vm.sysyemTypeFirstTimeGet = function (systemType_Id) {
				if (systemType_Id == 2 || systemType_Id == 3) {
					vm.systemTypeVal = true;
				}
				else {
					vm.systemTypeVal = false;
				}
			};


			function init() {
				//jobsService.getStreetNames().then(function (result) {
				//    vm.streetNames = result.data.items;
				//    setTimeout(function () {
				//        $('#streetNameSelectionCombobox').selectpicker('refresh');
				//        $('#streetNameSelectionCombobox').selectpicker('refresh');
				//        $('#installationstreetNameSelectionCombobox').selectpicker('refresh');
				//        $('#installationstreetNameSelectionCombobox').selectpicker('refresh');

				//    }, 0);
				//    vm.loading = false;
				//jobsService.getStreetType().then(function (result) {
				//    vm.streetType = result.data.items;
				//    setTimeout(function () {
				//        $('#streetTypeSelectionCombobox').selectpicker('refresh');
				//        $('#streetTypeSelectionCombobox').selectpicker('refresh');
				//        $('#installationStreetTypeSelectionCombobox').selectpicker('refresh');
				//        $('#installationStreetTypeSelectionCombobox').selectpicker('refresh');

				//    }, 0);
				//    vm.loading = false;
				//});
				jobsService.getPostalAddressType().then(function (result) {
					vm.postalAddressTypes = result.data.items;
					setTimeout(function () {
						$('#postalAddressTypesSelectionCombobox').selectpicker('refresh');
						$('#postalAddressTypesSelectionCombobox').selectpicker('refresh');

						$('#ownerAddressTypemodelSelectionCombobox').selectpicker('refresh');
						$('#ownerAddressTypemodelSelectionCombobox').selectpicker('refresh');

						$('#customerAddressTypemodelSelectionCombobox').selectpicker('refresh');
						$('#customerAddressTypemodelSelectionCombobox').selectpicker('refresh');

					}, 0);
					vm.loading = false;
					//});
					jobsService.getPostalDeliveryType().then(function (result) {
						vm.postalDeliveryType = result.data.items;
						setTimeout(function () {
							$('#postalDeliveryTypeSelectionCombobox').selectpicker('refresh');
							$('#postalDeliveryTypeSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					//jobsService.getUnitType().then(function (result) {
					//    vm.unitType = result.data.items;
					//    vm.installationUnitType = result.data.items;
					//    setTimeout(function () {
					//        $('#unitTypeSelectionCombobox').selectpicker('refresh');
					//        $('#unitTypeSelectionCombobox').selectpicker('refresh');
					//        $('#installationunitTypeSelectionCombobox').selectpicker('refresh');
					//        $('#installationunitTypeSelectionCombobox').selectpicker('refresh');

					//    }, 0);
					//    vm.loading = false;
					//});
					//jobsService.getAllPostCodes().then(function (result) {
					//    vm.streetCity = result.data.items;
					//    vm.streetPostcodes = result.data.items;
					//    setTimeout(function () {
					//        $('#streetCitySelectionCombobox').selectpicker('refresh');
					//        $('#streetCitySelectionCombobox').selectpicker('refresh');
					//        $('#installationstreetCitySelectionCombobox').selectpicker('refresh');
					//        $('#installationstreetCitySelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});
					jobsService.getHouseType().then(function (result) {
						vm.houseType = result.data.items;
						setTimeout(function () {
							$('#houseTypeSelectionCombobox').selectpicker('refresh');
							$('#houseTypeSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					jobsService.getRoofType().then(function (result) {
						vm.roofType = result.data.items;
						setTimeout(function () {
							$('#roofTypeSelectionCombobox').selectpicker('refresh');
							$('#roofTypeSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					//jobsService.getDistinctpanelList().then(function (result) {
					//    vm.panelList = result.data.items;
					//    setTimeout(function () {
					//        $('#panelSelectionCombobox').selectpicker('refresh');
					//        $('#panelSelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});

					//jobsService.getpanelList().then(function (result) {
					//    vm.panelList = result.data.items;
					//    setTimeout(function () {
					//        $('#panelSelectionCombobox').selectpicker('refresh');
					//        $('#panelSelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});
					//jobsService.getDistinctInverterList().then(function (result) {
					//    vm.inverterList = result.data.items;
					//    setTimeout(function () {
					//        $('#inverterSelectionCombobox').selectpicker('refresh');
					//        $('#inverterSelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});

					//jobsService.getInverterList().then(function (result) {
					//    vm.inverterList = result.data.items;
					//    setTimeout(function () {
					//        $('#inverterSelectionCombobox').selectpicker('refresh');
					//        $('#inverterSelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});
					jobsService.getSolarType().then(function (result) {
						vm.solarTypeList = result.data.items;
						setTimeout(function () {
							$('#solarTypemodelSelectionCombobox').selectpicker('refresh');
							$('#solarTypemodelSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					jobsService.getSystemType().then(function (result) {
						vm.systemTypeList = result.data.items;
						setTimeout(function () {
							$('#systemTypeSelectionCombobox').selectpicker('refresh');
							$('#systemTypeSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					jobsService.getRoofFacingPanel().then(function (result) {
						vm.roofFacingPanelsList = result.data.items;
						vm.locationOfNewSystemList = result.data.items;
						setTimeout(function () {
							$('#roofFacingPanelsSelectionCombobox').selectpicker('refresh');
							$('#roofFacingPanelsSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					jobsService.getYesNo().then(function (result) {
						vm.sugatthisaddressList = result.data.items;
						setTimeout(function () {
							$('#sugatthisaddressSelectionCombobox').selectpicker('refresh');
							$('#sugatthisaddressSelectionCombobox').selectpicker('refresh');

							$('#auditreportavailablemodelSelectionCombobox').selectpicker('refresh');
							$('#auditreportavailablemodelSelectionCombobox').selectpicker('refresh');

							$('#installingacompleteunitmodelSelectionCombobox').selectpicker('refresh');
							$('#installingacompleteunitmodelSelectionCombobox').selectpicker('refresh');

							$('#defaultresourceavailfiguremodelSelectionCombobox').selectpicker('refresh');
							$('#defaultresourceavailfiguremodelSelectionCombobox').selectpicker('refresh');

							$('#ceatingCertmodelSelectionCombobox').selectpicker('refresh');
							$('#ceatingCertmodelSelectionCombobox').selectpicker('refresh');

							$('#cecaccreditationStatementmodelSelectionCombobox').selectpicker('refresh');
							$('#cecaccreditationStatementmodelSelectionCombobox').selectpicker('refresh');

							$('#governmentSitingApprovalsmodelSelectionCombobox').selectpicker('refresh');
							$('#governmentSitingApprovalsmodelSelectionCombobox').selectpicker('refresh');

							$('#electricalSafetyDocumentationmodelSelectionCombobox').selectpicker('refresh');
							$('#electricalSafetyDocumentationmodelSelectionCombobox').selectpicker('refresh');

							$('#australianStandardsStatementmodelSelectionCombobox').selectpicker('refresh');
							$('#australianStandardsStatementmodelSelectionCombobox').selectpicker('refresh');

							$('#notgridConnectedStatementmodelSelectionCombobox').selectpicker('refresh');
							$('#notgridConnectedStatementmodelSelectionCombobox').selectpicker('refresh');

							$('#sguatThisAddressmodelSelectionCombobox').selectpicker('refresh');
							$('#sguatThisAddressmodelSelectionCombobox').selectpicker('refresh');


						}, 0);
						vm.loading = false;
					});
					jobsService.getTypeOfSystem().then(function (result) {
						vm.typeofsystemList = result.data.items;
						setTimeout(function () {
							$('#typeofsystemmodelSelectionCombobox').selectpicker('refresh');
							$('#typeofsystemmodelSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					jobsService.getTypeofconnection().then(function (result) {
						vm.typeofconnectionList = result.data.items;
						setTimeout(function () {
							$('#typeofconnectionmodelSelectionCombobox').selectpicker('refresh');
							$('#typeofconnectionmodelSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					jobsService.getMountingType().then(function (result) {
						vm.mountingtypeList = result.data.items;
						setTimeout(function () {
							$('#systemmountingtypemodelSelectionCombobox').selectpicker('refresh');
							$('#systemmountingtypemodelSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					jobsService.getSystemMountingType().then(function (result) {
						vm.systemmountingtypeList = result.data.items;
						setTimeout(function () {
							$('#systemmountingtypeSelectionCombobox').selectpicker('refresh');
							$('#systemmountingtypeSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					jobsService.getRECYear().then(function (result) {
						vm.recyearList = result.data.items;
						setTimeout(function () {
							$('#recyearmodelSelectionCombobox').selectpicker('refresh');
							$('#recyearmodelSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					//stateService.getState().then(function (result) {
					//    vm.streetStateList = result.data.items;
					//    setTimeout(function () {
					//        $('#streetStateSelectionCombobox').selectpicker('refresh');
					//        $('#streetStateSelectionCombobox').selectpicker('refresh');

					//        $('#installationStreetStateSelectionCombobox').selectpicker('refresh');
					//        $('#installationStreetStateSelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});
					jobsService.getAllInstaller().then(function (result) {
						vm.installerList = result.data.items;
						setTimeout(function () {
							$('#createforSelectionCombobox').selectpicker('refresh');
							$('#createforSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					jobsService.getJobStatus().then(function (result) {
						vm.jobstatusList = result.data.items;
						vm.loading = false;
					});

				});

				jobsService.getJobsForEdit({
					id: vm.jobId
				}).then(function (result) {
					vm.jobs = result.data;
					vm.suburb = result.data.streetCity;
					vm.installationameasHouse = vm.jobs.installationAddresssameasHouseAddress;
					vm.jobs.expectedInstallDate = new Date(vm.jobs.expectedInstallDate);
					//vm.jobs.expectedInstallDate = $scope.setDate(2019, 12, 20);
					jobsService.getInverterModelNoList({ filter: vm.jobs.inverterName }).then(function (result) {
						vm.invertermodelList = result.data.items;
						setTimeout(function () {
							$('#invertermodelSelectionCombobox').selectpicker('refresh');
							$('#invertermodelSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});

					jobsService.getModelNoList({ filter: vm.jobs.panelName }).then(function (result) {
						vm.modelList = result.data.items;
						setTimeout(function () {
							$('#modelSelectionCombobox').selectpicker('refresh');
							$('#modelSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});
					vm.getInstallationPostCodes(vm.jobs.InstallationSuburb_Id);
					vm.getPostCodes(vm.jobs.suburb_Id);
					vm.getInstallerDetails(vm.jobs.createFor_Id);
					vm.sysyemTypeFirstTimeGet(vm.jobs.systemType_Id);
				});
			}
			init();

			vm.getUnitTypeList = function () {
				jobsService.getUnitType({
					text: vm.jobs.unitType
				}).then(function (result) {
					$(".UnitTypetypeahead").typeahead({ source: result.data });
				});
			};
			vm.getStreetNameList = function () {
				jobsService.getStreetNames({
					text: vm.jobs.streetName
				}).then(function (result) {
					$(".StreetNametypeahead").typeahead({ source: result.data });
				});
			};
			vm.getStreetTypeList = function () {
				jobsService.getStreetType({
					text: vm.jobs.streetType
				}).then(function (result) {
					$(".StreetTypetypeahead").typeahead({ source: result.data });
				});
			};
			vm.getSuburbList = function () {
				jobsService.getAllPostCodes({
					text: vm.jobs.suburb
				}).then(function (result) {
					$(".Fnametypeahead").typeahead({ source: result.data, onSelect: displayStatePostcode });
				});
			};
			function displayStatePostcode(item) {
				var value = item.text.split("|");
				vm.jobs.suburb = value[0].trim();
				vm.jobs.Suburb_Id = item.value;
				vm.jobs.streetCity = value[0].trim();
				vm.jobs.streetState = value[1].trim();
				vm.jobs.streetPostCode = value[2].trim();
			}
			vm.getPanelList = function () {
				jobsService.getDistinctpanelList({
					text: vm.jobs.panelName
				}).then(function (result) {
					$(".Paneltypeahead").typeahead({ source: result.data, onSelect: panelBrand });
				});
			};
			function panelBrand(item) {
				vm.jobs.brandName = item.text;

				jobsService.getModelNoList({ filter: item.text }).then(function (result) {
					vm.modelList = result.data.items;
					setTimeout(function () {
						$('#modelSelectionCombobox').selectpicker('refresh');
						$('#modelSelectionCombobox').selectpicker('refresh');
					}, 0);
					vm.loading = false;
				});
			};
			vm.getInverterList = function () {
				jobsService.getDistinctInverterList({
					text: vm.jobs.inverterName
				}).then(function (result) {
					$(".Invertertypeahead").typeahead({ source: result.data, onSelect: inverterBrand });
				});
			};
			function inverterBrand(item) {
				vm.jobs.inverterBrandName = item.text;

				jobsService.getInverterModelNoList({ filter: item.text }).then(function (result) {
					vm.invertermodelList = result.data.items;
					setTimeout(function () {
						$('#invertermodelSelectionCombobox').selectpicker('refresh');
						$('#invertermodelSelectionCombobox').selectpicker('refresh');
					}, 0);
					vm.loading = false;
				});
			};
			vm.getInstUnitTypeList = function () {
				jobsService.getUnitType({
					text: vm.jobs.installationUnitType
				}).then(function (result) {
					$(".Inst_UnitTypetypeahead").typeahead({ source: result.data });
				});
			};
			vm.getInstStreetNameList = function () {
				jobsService.getStreetNames({
					text: vm.jobs.installationstreetName
				}).then(function (result) {
					$(".Inst_StreetNametypeahead").typeahead({ source: result.data });
				});
			};
			vm.getInstStreetTypeList = function () {
				jobsService.getStreetType({
					text: vm.jobs.installationStreetType
				}).then(function (result) {
					$(".Inst_StreetTypetypeahead").typeahead({ source: result.data });
				});
			};
			vm.getInstSuburbList = function () {
				jobsService.getAllPostCodes({
					text: vm.jobs.instSuburb
				}).then(function (result) {
					$(".Inst_Fnametypeahead").typeahead({ source: result.data, onSelect: displayPostalStatePostcode });
				});
			};
			function displayPostalStatePostcode(item) {
				var value = item.text.split("|");
				vm.jobs.instSuburb = value[0].trim();
				vm.jobs.InstallationSuburb_Id = item.value;
				vm.jobs.installationStreetCity = value[0].trim();
				vm.jobs.installationStreetState = value[1].trim();
				vm.jobs.installationStreetPostCode = value[2].trim();
			}
			vm.getStreetNameList();
			vm.getSuburbList();
			vm.getUnitTypeList();
			vm.getStreetTypeList();
			vm.getInstStreetNameList();
			vm.getInstSuburbList();
			vm.getInstUnitTypeList();
			vm.getInstStreetTypeList();
			vm.getPanelList();
			vm.getInverterList();
			vm.google = function () {
				vm.IsGoogleAddress = true;
			};
			vm.manual = function () {
				vm.IsGoogleAddress = false;
			};
		}
	]);
})();