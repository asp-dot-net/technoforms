﻿(function () {
    appModule.controller('tenant.views.jobs.addPanelSerial', [
        '$scope', '$uibModalInstance', 'abp.services.app.jobs', 'jobId',
        function ($scope, $uibModalInstance, jobsService, jobId) {
            var vm = this;

            vm.saving = false;
            vm.scanedPanel = {};
            vm.save = function () {
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {
                //jobsService.getScanedPanelsById({
                //    Id: panId,
                //    TenantId: tenantid
                //}).then(function (result) {
                //    vm.scanedPanel = result.data;
                //});
            }

            vm.save = function () {
                vm.saving = true;
                vm.scanedPanel.JobId = jobId;
                jobsService.insertScanedPanels(vm.scanedPanel)
                    .then(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                        $state.go('tenant.jobs');
                    }).finally(function () {
                        vm.saving = false;
                    });
            };

            init();
        }
    ]);
})();