﻿(function () {
    appModule.controller('tenant.views.jobs.createDocument', [
        '$scope', '$state', '$stateParams', 'FileUploader', 'abp.services.app.jobs',
        function ($scope, $state, $stateParams, fileUploader, jobsService) {
            var vm = this;

            vm.docFIleName = null;
            vm.saving = false;
            vm.doctypes = [];
            vm.directions = [];
            vm.docs = {};

            vm.jobId = $stateParams.jobId;

            vm.logouploader = new fileUploader({
                url: abp.appPath + 'File/UploadDocument?Id=' + vm.jobId,
                headers: {
                    "X-XSRF-TOKEN": abp.security.antiForgery.getToken()
                },
                queueLimit: 1,
                autoUpload: true,
                removeAfterUpload: true,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        //File type check
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        //if ('|jpg|jpeg|png|gif|'.indexOf(type) === -1) {
                        //    abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                        //    return false;
                        //}

                        //File size check
                        if (item.size > 5242880) //5MB.
                        {
                            abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
                            return false;
                        }

                        return true;
                    }
                }]
            });

            vm.logouploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    var $LogoResize = $('#Logophoto');

                    var profileFilePath = abp.appPath + 'Temp/Photos/' + response.result.fileName + '?v=' + new Date().valueOf();
                    vm.docFIleName = response.result.fileName;
                    //alert(response.result.fileName);
                    $LogoResize.attr('src', profileFilePath);

                } else {
                    abp.message.error(response.error.message);
                }
            };

            vm.save = function () {
                //alert(vm.docFIleName);
                if (!vm.docFIleName) {
                    return;
                }
                vm.docs.imageName = vm.docFIleName;
                vm.docs.guid = vm.jobId;
                jobsService.createDocument(vm.docs).then(function () {
                    $state.go('uploadDocument', {
                        jobId: vm.jobId
                    });
                    abp.notify.info(app.localize('SavedSuccessfully'));
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                //alert(vm.jobId);
                jobsService.getJobDetail({
                    guid: vm.jobId
                }).then(function (result) {
                    vm.job = result.data;
                });

                jobsService.getFileType().then(function (result) {
                    vm.doctypes = result.data.items;
                    vm.flags = result.data.flags;
                    setTimeout(function () {
                        $('#doctypeSelectionCombobox').selectpicker('refresh');
                        $('#doctypeSelectionCombobox').selectpicker('refresh');
                    }, 0);
                    vm.loading = false;
                });

                jobsService.getOldSystemDirection().then(function (result) {
                    vm.directions = result.data.items;
                    vm.flags = result.data.flags;
                    setTimeout(function () {
                        $('#OldSystemSelectionCombobox').selectpicker('refresh');
                        $('#OldSystemSelectionCombobox').selectpicker('refresh');
                    }, 0);
                    vm.loading = false;
                });
            }
            init();
            vm.cancel = function () {
                $state.go('uploadDocument', {
                    jobId: vm.jobId
                });
            };
        }
    ]);
})();