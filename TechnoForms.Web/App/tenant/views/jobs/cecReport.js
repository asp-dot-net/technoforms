﻿(function () {
	appModule.controller('tenant.views.jobs.cecReport', [
		'$scope', '$uibModalInstance', 'abp.services.app.jobs', 'abp.services.app.report', 'jobId',
		function ($scope, $uibModalInstance, jobsService, reportService, jobId) {
			var vm = this;

			vm.saving = false;

			vm.cancel = function () {
				$uibModalInstance.dismiss();
			};

			vm.verify = function () {
				abp.message.confirm(
					app.localize('verifyinverterphoto'),
					function (isConfirmed) {
						if (isConfirmed) {
							jobsService.updateVerifyCec({ JobId: jobId }).then(function () {
								abp.notify.info(app.localize('SavedSuccessfully'));
								$uibModalInstance.close();
								$state.go('tenant.jobs');
							}).finally(function () {
								vm.saving = false;
							});
						}
					}
				);
			};

			vm.download = function () {
				//vm.saving = true;
				$(".page-spinner-bar").removeClass("hide");
				reportService.cecReport({ JobId: jobId }).then(function (result) {
					var file_path = abp.appPath + 'Temp/Reports/' + result.data + ".pdf";
					var a = document.createElement('A');
					a.href = file_path;
					a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
					document.body.appendChild(a);
					a.click();
					document.body.removeChild(a);
					//window.open(abp.appPath + 'Temp/Reports/' + result.data + ".pdf" + '?v=' + new Date().valueOf(), '_blank');
					//vm.saving = false;
					$(".page-spinner-bar").addClass("hide");
				});
			};

			vm.print = function () {

			};

			function init() {

			}
			init();
		}
	]);
})();