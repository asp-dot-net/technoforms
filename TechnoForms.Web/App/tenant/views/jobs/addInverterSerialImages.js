﻿(function () {
    appModule.controller('tenant.views.jobs.addInverterSerialImages', [
        '$scope', '$uibModalInstance', 'FileUploader', 'abp.services.app.jobs', 'jobId',
        function ($scope, $uibModalInstance, fileUploader, jobsService, jobId) {
            var vm = this;

            vm.docFIleName = null;
            vm.saving = false;
            vm.scanedInverter = {};
            vm.save = function () {
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            vm.logouploader = new fileUploader({
                url: abp.appPath + 'File/UploadScanedInverterImages?Id=' + jobId,
                headers: {
                    "X-XSRF-TOKEN": abp.security.antiForgery.getToken()
                },
                queueLimit: 1,
                autoUpload: true,
                removeAfterUpload: true,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        return true;
                    }
                }]
            });

            vm.logouploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    var $LogoResize = $('#Logophoto');
                    var profileFilePath = abp.appPath + 'Temp/ScanedInverters/' + response.result.fileName + '?v=' + new Date().valueOf();
                    vm.docFIleName = response.result.fileName;
                    $LogoResize.attr('src', profileFilePath);

                } else {
                    abp.message.error(response.error.message);
                }
            };

            function init() {
            }

            vm.save = function () {
                vm.saving = true;
                vm.scanedInverter.JobId = jobId;
                if (!vm.docFIleName) {
                    return;
                }
                vm.scanedInverter.InverterBarcodePhoto = vm.docFIleName;

                jobsService.insertScanedInverterImages(vm.scanedInverter)
                    .then(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                        $state.go('tenant.jobs');
                    }).finally(function () {
                        vm.saving = false;
                    });
            };

            init();
        }
    ]);
})();