﻿(function () {
	appModule.controller('tenant.views.jobs.scanedInverterview', [
		'$scope', '$uibModalInstance', 'abp.services.app.jobs', 'scanedInverterId',
		function ($scope, $uibModalInstance, jobsService, scanedInverterId) {
			var vm = this;

			vm.saving = false;
			vm.inverterimagelist = {};
			vm.displayVerifyIN = true;
			vm.save = function () {
			};
			$scope.myInterval = 5000;
			$scope.noWrapSlides = false;
			$scope.active = 0;
			var currIndex = 0;
			vm.invertercount = 0;
			var slides = $scope.slides = [];

			vm.cancel = function () {
				$uibModalInstance.dismiss();
			};

			function init() {

				jobsService.getTenantName().then(function (result) {
					vm.tenantName = result.data;
					jobsService.scanedInverterView({ scannedId: scanedInverterId }).then(function (result) {
						vm.inverterimagelist = result.data.items;
						console.log(vm.inverterimagelist);
						angular.forEach(result.data.items, function (value, key) {
							vm.invertercount++
						});

						angular.forEach(result.data.items, function (value, key) {
							if (value.inverterBarcodePhoto != null || value.inverterBarcodePhoto != 'undefined') {
								slides.push({
									id: value.id,
									inverterSerialNumber: value.inverterSerialNumber,
									photo: "../../Temp/" + vm.tenantName + "/" + result.data.items[0].formId + "/ScanedInverters/" + value.inverterBarcodePhoto,
									longitude: value.longitude,
									latitude: value.latitude,
									date: value.creationTime,
									imagecount: vm.invertercount
								});
							}
						});

					}).finally(function () {
						vm.loading = false;
					});
				});
			}
			init();
		}
	]);
})();