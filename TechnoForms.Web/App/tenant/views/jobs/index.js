(function () {
	appModule.controller('common.views.jobs.index', [
		'$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.jobs', 'abp.services.app.state', 'abp.services.app.report',
		function ($scope, $state, $uibModal, uiGridConstants, jobsService, stateService, reportService) {
			var vm = this;
			vm.norecord = false;
			$scope.test = "111";
			vm.streetCity = [];
			vm.streetStateList = [];
			vm.installerList = [];
			vm.systemTypeList = [];
			vm.datetype = [];
			vm.received = [];
			vm.filterJobStatusId = 0;
			vm.gridHeight = 0;

			$scope.selected = undefined;
			$scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois'];

			$scope.$on('$viewContentLoaded', function () {
				App.initAjax();
			});
			vm.signature = function () {
				$state.go('signature', {
					guid: 'eac63eb3-3419-4a11-9402-247dac8576f7'
				});
			};

			//DatePIcker Code Start
			$scope.today = function () {
				$scope.dt = new Date();
			};
			//$scope.today();

			$scope.clear = function () {
				$scope.dt = null;
			};

			$scope.inlineOptions = {
				customClass: getDayClass,
				minDate: new Date(),
				showWeeks: true
			};
			////Disable past dates
			//$scope.dateOptions = {
			//    //dateDisabled: disabled,
			//    formatYear: 'yy',
			//    //maxDate: new Date(2020, 5, 22),
			//    minDate: new Date(),
			//    startingDay: 1
			//};

			//Enable all dates
			$scope.dateOptionsApproval = {
				//dateDisabled: disabled,
				formatYear: 'yy',
				startingDay: 1
			};

			//// Disable weekend selection
			//function disabled(data) {
			//    var date = data.date,
			//        mode = data.mode;
			//    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
			//}

			$scope.open1 = function () {
				$scope.popup1.opened = true;
			};

			$scope.open2 = function () {
				$scope.popup2.opened = true;
			};

			$scope.setDate = function (year, month, day) {
				$scope.dt = new Date(year, month, day);
			};

			$scope.formats = ['d/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			$scope.format = $scope.formats[0];
			$scope.altInputFormats = ['M!/d!/yyyy'];

			$scope.popup1 = {
				opened: false
			};

			$scope.popup2 = {
				opened: false
			};

			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);
			var afterTomorrow = new Date();
			afterTomorrow.setDate(tomorrow.getDate() + 1);
			$scope.events = [
				{
					date: tomorrow,
					status: 'full'
				},
				{
					date: afterTomorrow,
					status: 'partially'
				}
			];

			function getDayClass(data) {
				var date = data.date,
					mode = data.mode;
				if (mode === 'day') {
					var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

					for (var i = 0; i < $scope.events.length; i++) {
						var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

						if (dayToCheck === currentDay) {
							return $scope.events[i].status;
						}
					}
				}

				return '';
			}
			//DatePIcker Code End

			//start from here....
			vm.getSuburbList = function () {
				jobsService.getAllPostCodes({
					text: vm.filterCity
				}).then(function (result) {
					$(".statetypeahead").typeahead({ source: result.data, onSelect: displayStatePostcode });
				});
			};
			function displayStatePostcode(item) {
				var value = item.text.split("|");
				vm.filterCity = value[0].trim();
			}
			vm.getSuburbList();
			vm.loading = false;

			vm.permissions = {
				createjobs: abp.auth.hasPermission('Pages.Tenant.Jobs.Create'),
				deletejobs: abp.auth.hasPermission('Pages.Tenant.Jobs.Delete'),
				editjobs: abp.auth.hasPermission('Pages.Tenant.Jobs.Edit'),
				detailjobs: abp.auth.hasPermission('Pages.Tenant.Jobs.Detail')
			};

			var requestParams = {
				skipCount: 0,
				maxResultCount: app.consts.grid.defaultPageSize,
				sorting: null
			};

			vm.jobsGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				expandableRowTemplate: '/App/tenant/views/jobs/expandableRowTemplate.html',
				expandableRowHeight: 520,
				expandableRowScope: {
					subGridVariable: 'subGridScopeVariable'
				},
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="{row.entity.gridClass} ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableColumnMenu: false,
						headerCellClass: 'centeralign',
						width: 110,
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
							'    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
							'    <ul uib-dropdown-menu>' +
							'      <li><a ng-show="!row.entity.verified" ng-if="grid.appScope.permissions.editjobs" target="_blank" ng-click="grid.appScope.editJobs(row.entity)">' + app.localize('Edit') + '</a></li>' +
							'      <li><a ng-if="grid.appScope.permissions.editjobs" ng-click="grid.appScope.completeJobs(row.entity)">' + app.localize('Complete') + '</a></li>' +
							'      <li><a ng-if="grid.appScope.permissions.deletejobs" ng-click="grid.appScope.deleteJobs(row.entity)">' + app.localize('Delete') + '</a></li>' +
							'      <li><a ng-if="grid.appScope.permissions.detailjobs" ng-click="grid.appScope.detailJobs(row.entity)">' + app.localize('Detail') + '</a></li>' +
							'    </ul>' +
							'  </div>' +
							'</div>'
					},
					{
						name: app.localize('jobid'),
						field: 'formId',
						enableColumnMenu: false,
						headerCellClass: 'centeralign',
						minWidth: 80,
						maxWidth: 100,
						cellClass: 'centeralign textbold',
						//cellTooltip: function (row, col) { return row.entity.id },
					},
					{
						name: app.localize('sType'),
						field: 'systemTypeName',
						enableColumnMenu: false,
						headerCellClass: 'centeralign',
						minWidth: 80,
						maxWidth: 100,
						cellClass: 'centeralign textbold'
					},
					{
						name: app.localize('projectno#'),
						field: 'projectNumber',
						headerCellClass: 'centeralign',
						enableColumnMenu: false,
						maxWidth: 120,
						minWidth: 100,
						cellClass: 'centeralign textbold',
						//cellTooltip: function (row, col) { return row.entity.projectNumber },
					},
					{
						name: app.localize('ownerinfo'),
						field: 'fullName',
						enableColumnMenu: false,
						minWidth: 150,
						cellTemplate: '<div class="jobownerinfo ui-grid-cell-contents"><p><span class="ownername" >{{row.entity.fullName}}</span> <span class="addressphonebox"><span class="streetaddress"tooltip-placement="left" uib-tooltip={{row.entity.streetAddress}}>{{row.entity.streetAddress}}</span></span><p></div>'
					},
					{
						name: app.localize('phone'),
						field: 'phone',
						enableColumnMenu: false,
						minWidth: 80
					},
					{
						name: app.localize('Installer'),
						field: 'installerName',
						enableColumnMenu: false,
						minWidth: 100,
						maxWidth: 150,
						//cellTooltip: function (row, col) { return row.entity.installerName },
					},
					{
						name: app.localize('InstallationDate'),
						field: 'expectedInstallDate',
						enableColumnMenu: false,
						headerCellClass: 'centeralign',
						minWidth: 130,
						maxWidth: 160,
						cellClass: 'centeralign',
						cellFilter: 'date:\'dd-MM-yyyy\'',
						//cellFilter: 'momentFormat: \'L\'',
						//cellTooltip: function (row, col) { return row.entity.expectedInstallDate },
					},
					{
						name: app.localize('Documents'),
						enableColumnMenu: false,
						headerCellClass: 'centeralign width300',
						cellClass: 'width300',
						minWidth: 180,
						maxWidth: 300,
						cellTemplate:
							'<div class="documentbox jobsgridbox">' +
							'<ul>' +
							//'<li><span ng-class="{{row.entity.isScanedPanel}}==true?\'pa dcbox\':\'pa dcbox disabled\'" ><a ng-click="grid.appScope.GetPAImage(row.entity)" class="link_button">PA</a></span></li>' +
							'<li><span class="{{row.entity.scanedPanelClass}}" tooltip-placement="left" uib-tooltip="Panel Images"><a ng-click="grid.appScope.GetPAImage(row.entity)" class="link_button"><img src="/App/images/PA.svg" alt="Panel Images"></a></span></li>' +
							'<li><span class="{{row.entity.scanedInverterClass}}" tooltip-placement="left" uib-tooltip="Inverter Images"><a ng-click="grid.appScope.GetINImage(row.entity)" class="link_button"><img src="/App/images/IN.svg" alt="Inverter Images"></a></span></li>' +
							'<li><span class="{{row.entity.otherImagesClass}}" tooltip-placement="left" uib-tooltip="Other Images"><a ng-click="grid.appScope.GetOtherImage(row.entity)" class="link_button"><img src="/App/images/IM.svg" alt="Other Images"></a></span></li>' +
							'<li><span class="{{row.entity.customerSignatureClass}}" tooltip-placement="left" uib-tooltip="Customer Signature"><a ng-click="grid.appScope.GetCustomersignature(row.entity)" class="link_button"><img src="/App/images/CS.svg" alt="Customer Signature"></a></span></li>' +
							'<li><span class="{{row.entity.installerSignatureClass}}" tooltip-placement="left" uib-tooltip="Installer Signature"><a ng-click="grid.appScope.GetInstallersignature(row.entity)" class="link_button"><img src="/App/images/IS.svg" alt="Installer Signature"></a></span></li>' +
							//'<li><span class="pk dcbox disabled"><a ng-click="grid.appScope.GetInstallersignature(row.entity)" class="link_button">PK</a></span></li>' +
							'<li><span class="{{row.entity.cecSignatureClass}}" tooltip-placement="left" uib-tooltip="STC"><a ng-click="grid.appScope.GetCecReport(row.entity)" class="link_button"><img src="/App/images/CEC.svg" alt="STC"></a></span></li>' +
							'<li><span class="{{row.entity.eleSignatureClass}}" tooltip-placement="left" uib-tooltip="Testing & Complaience"><a ng-click="grid.appScope.GetEleReport(row.entity)" class="link_button"><img src="/App/images/ELE.svg" alt="Testing & Complaience"></a></span></li>' +
							'<li><span class="{{row.entity.cuckSignatureClass}}" tooltip-placement="left" uib-tooltip="Customer Acknologement"><a ng-click="grid.appScope.GetCuckReport(row.entity)" class="link_button"><img src="/App/images/CUCK.svg" alt="Customer Acknologement"></a></span></li>' +
							'</ul>' +
							'</div>'
					},
				],
				onRegisterApi: function (gridApi) {
					$scope.gridApi = gridApi;
					$scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
						if (!sortColumns.length || !sortColumns[0].field) {
							requestParams.sorting = null;
						} else {
							requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
						}

						vm.getJobs();
					});
					gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
						requestParams.skipCount = (pageNumber - 1) * pageSize;
						requestParams.maxResultCount = pageSize;

						vm.getJobs();
					});
				},
				data: []
			};

			vm.uploadRec = function () {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/jobBulkUpload.cshtml',
					controller: 'tenant.views.jobs.jobBulkUpload as vm',
					scope: $scope,
					backdrop: 'static'
				});
				$scope.$on('eventJobDataImported', function (event, data) {
					vm.getJobs();
				});
			};

			if (!vm.permissions.editjobs && !vm.permissions.deletejobs && !vm.permissions.detailjobs) {
				vm.jobsGridOptions.columnDefs.shift();
			}

			vm.uploadDocuments = function (jobs) {
				$state.go('uploadDocument', {
					jobId: jobs.guid
				});
			};
			vm.installerSign = function (jobs) {
				abp.message.confirm(
					app.localize('InstallerRequestWarningMessage'),
					function (isConfirmed) {
						if (isConfirmed) {
							jobsService.createInstallerSignSMS({
								Id: jobs.id
							}).then(function () {
								abp.notify.success(app.localize('InstallerRequestSent'));
								vm.getJobs();
							});
						}
					}
				);
			};
			vm.customerSign = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/customerSignModal.cshtml',
					controller: 'tenant.views.jobs.customerSignModal as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.GetPAImage = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/panelimages.cshtml',
					controller: 'tenant.views.jobs.panelimages as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.GetINImage = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/inverterimage.cshtml',
					controller: 'tenant.views.jobs.inverterimage as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.GetOtherImage = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/otherimage.cshtml',
					controller: 'tenant.views.jobs.otherimage as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.GetCustomersignature = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/customersignatureimage.cshtml',
					controller: 'tenant.views.jobs.customersignatureimage as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.GetInstallersignature = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/installersignatureimage.cshtml',
					controller: 'tenant.views.jobs.installersignatureimage as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};

			vm.GetCecReport = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/cecReport.cshtml',
					controller: 'tenant.views.jobs.cecReport as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};

			vm.GetEleReport = function (jobs) {
				vm.loading = true;
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/eleReport.cshtml',
					controller: 'tenant.views.jobs.eleReport as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				vm.loading = false;
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};

			vm.GetCuckReport = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/cuckReport.cshtml',
					controller: 'tenant.views.jobs.cuckReport as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};

			vm.custLog = function (jobs) {
				$state.go('SignatureLog', {
					jobId: jobs.id
				});
			};
			vm.addPanelserial = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/addPanelSerial.cshtml',
					controller: 'tenant.views.jobs.addPanelSerial as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			}
			vm.addPanelImages = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/addPanelSerialImages.cshtml',
					controller: 'tenant.views.jobs.addPanelSerialImages as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			}
			vm.addInverterserial = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/addInverterSerial.cshtml',
					controller: 'tenant.views.jobs.addInverterSerial as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			}
			vm.addInverterImages = function (jobs) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/addInverterSerialImages.cshtml',
					controller: 'tenant.views.jobs.addInverterSerialImages as vm',
					backdrop: 'static',
					resolve: {
						jobId: jobs.id
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			}
			vm.downloadDocuments = function (jobs) {
				$(".page-spinner-bar").removeClass("hide");
				reportService.documentsReport({ JobId: jobs.id }).then(function (result) {
					var file_path = abp.appPath + 'Temp/Reports/' + result.data + ".pdf";
					var a = document.createElement('A');
					a.href = file_path;
					a.download = file_path.substr(file_path.lastIndexOf('/') + 1);
					document.body.appendChild(a);
					a.click();
					document.body.removeChild(a);
					$(".page-spinner-bar").addClass("hide");
				});
			};
			vm.jobform = function (jobs) {
				$state.go('formList', {
					jobId: jobs.guid
				});
			};
			vm.messages = function (jobs) {
				$state.go('Message', {
					jobId: jobs.id
				});
			};
			vm.jobScanedPaneldelete = function (panelid, tenantid, panelserialno) {
				abp.message.confirm(
					app.localize('PanelDeleteWarningMessage', panelserialno),
					function (isConfirmed) {
						if (isConfirmed) {
							jobsService.deleteScanedPanels({
								Id: panelid,
								TenantId: tenantid
							}).then(function () {
								abp.notify.success(app.localize('SuccessfullyDeleted'));
								vm.getJobs();
							});
						}
					}
				);
			};
			vm.jobScanedPanelEdit = function (panId, tenantid) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/editSerialNumber.cshtml',
					controller: 'tenant.views.jobs.editSerialNumber as vm',
					backdrop: 'static',
					resolve: {
						panId: panId,
						tenantid: tenantid
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.jobScanedPanelview = function (scanedPanelId) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/scanedPanelview.cshtml',
					controller: 'tenant.views.jobs.scanedPanelview as vm',
					backdrop: 'static',
					resolve: {
						scanedPanelId: scanedPanelId
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.jobScanedInverterview = function (scanedInverterId) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/scanedInverterview.cshtml',
					controller: 'tenant.views.jobs.scanedInverterview as vm',
					backdrop: 'static',
					resolve: {
						scanedInverterId: scanedInverterId
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};
			vm.jobScanedinverterEdit = function (inverterId, tenantid) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/jobs/editScanedinverterSerialNumber.cshtml',
					controller: 'tenant.views.jobs.editScanedinverterSerialNumber as vm',
					backdrop: 'static',
					resolve: {
						inverterId: inverterId,
						tenantid: tenantid
					}
				});
				modalInstance.result.then(function (result) {
					vm.getJobs();
				});
			};

			vm.jobScanedinverterdelete = function (inverterId, tenantid, inverterSeries) {
				abp.message.confirm(
					app.localize('InverterDeleteWarningMessage', inverterSeries),
					function (isConfirmed) {
						if (isConfirmed) {
							jobsService.deleteScanedInverters({
								Id: inverterId,
								TenantId: tenantid
							}).then(function () {
								abp.notify.success(app.localize('SuccessfullyDeleted'));
								vm.getJobs();
							});
						}
					}
				);
			};

			vm.getJobs = function () {
				vm.loading = true;
				jobsService.getJobs({
					skipCount: requestParams.skipCount,
					maxResultCount: requestParams.maxResultCount,
					sorting: requestParams.sorting,
					filter: vm.filterText,
					filterFormId: vm.filterFormId,
					filterOwnerName: vm.filterOwnerName,
					filterCity: vm.filterCity,
					filterState: vm.filterState,
					filterInstallerId: vm.filterInstallerId,
					filterSystemtypeId: vm.filterSystemtypeId,
					filterJobStatusId: vm.filterJobStatusId,
					filterStartDate: vm.filterStartDate,
					filterEndDate: vm.filterEndDate,
					filterDateypeId: vm.filterDateypeId,
					filterReceivedId: vm.filterReceivedId,
					filterNotReceivedId: vm.filterNotReceivedId,
					verified: vm.verified
				}).then(function (result) {
					vm.jobsGridOptions.totalItems = result.data.totalCount;
					vm.jobsGridOptions.data = result.data.items;
					vm.gridHeight = "min-height:" + (vm.jobsGridOptions.data.length * 70 + 35 + 61) + "px !important";
					if (result.data.totalCount == 0) {
						//vm.norecord = true;
						abp.notify.info(app.localize('NoRecordFound'));
					}
					else { vm.norecord = false; }
					//for (i = 0; i < result.data.items.length; i++) {
					//    result.data.items[i].subGridOptions = {
					//        columnDefs: [{ name: 'Id', field: 'id' }, { name: 'Panel Serial Number', field: 'panelSerialNumber' }],
					//        data: result.data.items[i].jobScanedPanel_List
					//        //data: [{ id: 0, name: "Rosanne Barrett" }, { id: 1, name: "Nita Chase" }, { id: 2, name: "Briggs Stark" },
					//        //    { id: 3, name: "Briggs Stark" }, { id: 4, name: "Briggs Stark" }, { id: 5, name: "Briggs Stark" }, { id: 6, name: "Briggs Stark" },
					//        //    { id: 7, name: "Briggs Stark" }, { id: 8, name: "Briggs Stark" }, { id: 9, name: "Briggs Stark" }, { id: 10, name: "Briggs Stark" }]
					//    };
					//    result.data.items[i].scanedInverterGridOptions = {
					//        columnDefs: [{ name: 'Id', field: 'id' }, { name: 'Inverter Serial Number', field: 'InverterSerialNumber' }],
					//        data: result.data.items[i].jobScanedInverter_List
					//    };
					//}
				}).finally(function () {
					vm.loading = false;
				});
			};

			vm.createJobs = function () {
				$state.go('createJob');
			};

			vm.deleteJobs = function (jobs) {
				abp.message.confirm(
					app.localize('JobsDeleteWarningMessage', jobs.firstName),
					function (isConfirmed) {
						if (isConfirmed) {
							jobsService.deleteJobs({
								id: jobs.id
							}).then(function () {
								vm.getJobs();
								abp.notify.success(app.localize('SuccessfullyDeleted'));
							});
						}
					}
				);
			};

			vm.completeJobs = function (jobs) {
				abp.message.confirm(
					app.localize('JobsCompleteWarningMessage', jobs.firstName),
					function (isConfirmed) {
						if (isConfirmed) {
							jobsService.completeJob({
								id: jobs.id
							}).then(function () {
								vm.getJobs();
								abp.notify.success(app.localize('SuccessfullyCompleted'));
							});
						}
					}
				);
			};


			vm.editJobs = function (jobs) {
				var url = $state.href('editJob', { jobId: jobs.id });
				window.open(url, '_blank');
				//$state.go('editJob', {
				//    jobId: jobs.id
				//});
			};

			vm.detailJobs = function (jobs) {
				$state.go('detailJob', {
					jobId: jobs.id
				});
			};

			//jobsService.getAllPostCodes().then(function (result) {

			//	vm.streetCity = result.data.items;
			//	setTimeout(function () {
			//		$('#streetCitySelectionCombobox').selectpicker('refresh');
			//		$('#streetCitySelectionCombobox').selectpicker('refresh');
			//	}, 0);
			//	vm.loading = false;
			//});

			stateService.getState().then(function (result) {
				vm.streetStateList = result.data.items;
				setTimeout(function () {
					$('#streetStateSelectionCombobox').selectpicker('refresh');
					$('#streetStateSelectionCombobox').selectpicker('refresh');
				}, 0);
				vm.jobs.streetState = result.data.items[0].stateName;
				vm.loading = false;
			});

			jobsService.getAllInstaller().then(function (result) {
				vm.installerList = result.data.items;
				setTimeout(function () {
					$('#createforSelectionCombobox').selectpicker('refresh');
					$('#createforSelectionCombobox').selectpicker('refresh');
				}, 0);
				vm.loading = false;
			});

			jobsService.getSystemType().then(function (result) {
				vm.systemTypeList = result.data.items;
				setTimeout(function () {
					$('#systemTypeSelectionCombobox').selectpicker('refresh');
					$('#systemTypeSelectionCombobox').selectpicker('refresh');
				}, 0);
				vm.loading = false;
			});

			vm.datetype = [
				{
					id: '1',
					name: 'Install Date'
				}, {
					id: '2',
					name: 'Panel Expiry'
				}, {
					id: '3',
					name: 'Inverter Expiry'
				}, {
					id: '4',
					name: 'Installer Expiry'
				}];
			vm.received = [
				{
					id: '1',
					name: 'Panel'
				}, {
					id: '2',
					name: 'Inverter'
				}, {
					id: '3',
					name: 'Image'
				}, {
					id: '4',
					name: 'Installer Signature'
				}, {
					id: '5',
					name: 'Owner Signature'
				}];
			vm.jobStatus = function (status_id) {
				vm.filterJobStatusId = status_id;
				vm.getJobs();
			}
			vm.ClearAll = function () {
				vm.filterFormId = null;
				vm.filterOwnerName = null;
				vm.filterCity = null;
				vm.filterState = null;
				vm.filterInstallerId = null;
				//vm.filterInstallerId = 0;
				vm.filterSystemtypeId = null;
				//vm.filterSystemtypeId = 0;
				vm.filterState = null;
				vm.filterStartDate = null;
				vm.filterEndDate = null;
				vm.filterDateypeId = null;
				//vm.filterDateypeId = 0;
				vm.filterReceivedId = null;
				vm.filterNotReceivedId = null;

				setTimeout(function () {
					$('#createforSelectionCombobox').selectpicker('refresh');
					$('#streetCitySelectionCombobox').selectpicker('refresh');
					$('#streetStateSelectionCombobox').selectpicker('refresh');
					$('#systemTypeSelectionCombobox').selectpicker('refresh');
					$('#systemDateTypeSelectionCombobox').selectpicker('refresh');
					$('#systemReceivedSelectionCombobox').selectpicker('refresh');
					$('#systemNotReceivedSelectionCombobox').selectpicker('refresh');
				}, 0);
				vm.getJobs();
			};

			vm.getJobs();

			vm.copyToClipBoardPanel = function (jobs) {
				//alert(jobs.scannedPanelSerialNoList);
				var copyElement = document.createElement("textarea");
				copyElement.style.position = 'fixed';
				copyElement.style.opacity = '0';
				copyElement.textContent = jobs.scannedPanelSerialNoList; //'http://example.com?from=' + decodeURI(name);
				var body = document.getElementsByTagName('body')[0];
				body.appendChild(copyElement);
				copyElement.select();
				document.execCommand('copy');
				body.removeChild(copyElement);
			};
			vm.copyToClipBoardInverter = function (jobs) {
				//alert(jobs.scannedInverterSerialNoList);
				var copyElement = document.createElement("textarea");
				copyElement.style.position = 'fixed';
				copyElement.style.opacity = '0';
				copyElement.textContent = jobs.scannedInverterSerialNoList; //'http://example.com?from=' + decodeURI(name);
				var body = document.getElementsByTagName('body')[0];
				body.appendChild(copyElement);
				copyElement.select();
				document.execCommand('copy');
				body.removeChild(copyElement);
			};
		}
	]);
})();