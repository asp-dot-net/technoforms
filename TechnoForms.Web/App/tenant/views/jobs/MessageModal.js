﻿(function () {
    appModule.controller('tenant.views.jobs.messageModal', [
        '$scope', '$state', '$stateParams', 'uiGridConstants', 'abp.services.app.message',
        function ($scope, $state, $stateParams, uiGridConstants, messageService) {
            var vm = this;
            vm.saving = false;
            vm.jobId = $stateParams.jobId;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.save = function () {
                vm.message.job_id = vm.jobId;
                vm.saving = true;
                //vm.message.messageValue = "Hello " +  + vm.message.messageValue;
                messageService.createMessage(vm.message).then(function () {
                    //$state.go('Message');
                    vm.getMessage();
                    vm.message.messageValue = "";
                    abp.notify.info(app.localize('SavedSuccessfully'));
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.jobsGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('MessageSendBy'),
                        field: 'messageSendBy',
                        enableColumnMenu: false,
                        width:200,
                        cellTooltip: function (row, col) { return row.entity.messageSendBy },
                    },
                    {
                        name: app.localize('MessageValue'),
                        field: 'messageValue',
                        enableColumnMenu: false,
                        minWidth:400,
                        cellTooltip: function (row, col) { return row.entity.messageValue },
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        enableColumnMenu: false,
                        width:150,
                        headercellClass:'centeralign',
                        cellClass:'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\'',
						//cellFilter: 'momentFormat: \'L\'',
                        cellTooltip: function (row, col) { return row.entity.creationTime },
                    }
                    //,
                    //{
                    //    name: app.localize('Delete'),
                    //    enableSorting: false,
                    //    width: 180,
                    //    cellTemplate:
                    //        '<div class=\"ui-grid-cell-contents\">' +
                    //        '<a class="btn btn-xs btn-primary blue" ng-click="grid.appScope.deleteMessage(row.entity)">' + app.localize('Delete') + '</a>'
                    //        + '</div>'
                    //}
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getMessage();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getMessage();
                    });
                },
                data: []
            };

            vm.getMessage = function () {
                vm.loading = true;
                messageService.getMessage({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText,
                    jobId: vm.jobId
                }).then(function (result) {
                    vm.jobsGridOptions.totalItems = result.data.totalCount;
                    vm.jobsGridOptions.data = result.data.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.deleteMessage = function (message) {
                abp.message.confirm(
                    app.localize('MessageDeleteWarningMessage', message.messageValue),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            messageService.deleteMessage({
                                id: message.id
                            }).then(function () {
                                vm.getMessage();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.cancel = function () {
                $state.go('tenant.jobs');
            };

            vm.getMessage();
            //function init() {

            //}

            //init();
        }
    ]);
})();