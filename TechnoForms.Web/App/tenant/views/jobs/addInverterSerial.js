﻿(function () {
    appModule.controller('tenant.views.jobs.addInverterSerial', [
        '$scope', '$uibModalInstance', 'abp.services.app.jobs', 'jobId',
        function ($scope, $uibModalInstance, jobsService, jobId) {
            var vm = this;

            vm.saving = false;
            vm.scanedInverter = {};
            vm.save = function () {
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {
            }

            vm.save = function () {
                vm.saving = true;
                vm.scanedInverter.JobId = jobId;
                jobsService.insertScanedInverter(vm.scanedInverter)
                    .then(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                        $state.go('tenant.jobs');
                    }).finally(function () {
                        vm.saving = false;
                    });
            };

            init();
        }
    ]);
})();