﻿(function () {
    appModule.controller('tenant.views.jobs.customerSignModal', [
        '$scope', '$uibModalInstance', 'abp.services.app.jobs', 'jobId',
        function ($scope, $uibModalInstance, jobsService, jobId) {
            var vm = this;

            vm.saving = false;
            // vm.jobId = $stateParams.jobId;

            vm.customerSignMessage = function () {
                //abp.message.confirm(
                //    app.localize('CustomerRequestUsingSMS'),
                //    function (isConfirmed) {
                //        if (isConfirmed) {
                jobsService.createCustomerSignSMS({
                    Id: jobId
                }).then(function () {
                    abp.notify.success(app.localize('CustomerRequestUsingSMSSent'));
                    vm.getJobs();
                });
                //        }
                //    }
                //);
            };
            vm.customerSignEmail = function () {
                abp.message.confirm(
                    app.localize('CustomerRequestUsingEmail'),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            jobsService.createCustomerSignEmail({
                                Id: jobId
                            }).then(function () {
                                abp.notify.success(app.localize('CustomerRequestUsingEmailSent'));
                                vm.getJobs();
                            });
                        }
                    }
                );
            };
            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {

            }

            init();
        }
    ]);
})();