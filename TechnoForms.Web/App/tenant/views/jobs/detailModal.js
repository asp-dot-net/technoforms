﻿(function () {
    appModule.controller('tenant.views.jobs.detailModal', [
        '$scope', '$state', '$stateParams', 'abp.services.app.jobs', 'abp.services.app.state', 'abp.services.app.installer',
        function ($scope, $state, $stateParams, jobsService, stateService, installerService) {
            var vm = this;
            vm.jobId = $stateParams.jobId;

            vm.jobs = {};
            vm.cancel = function () {
                $state.go('tenant.jobs', {
                });
            };
            function init() {
                jobsService.getJobsDetails({
                    id: vm.jobId
                }).then(function (result) {
                    if (result.data.systemType_Id == 2 || result.data.systemType_Id == 3) {
                        vm.systemTypeVal = true;
                    }
                    else {
                        vm.systemTypeVal = false;
                    }

                    vm.jobs = result.data;
                });
            }
            init();
        }
    ]);
})();