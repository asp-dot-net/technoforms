﻿(function () {
    appModule.controller('tenant.views.jobs.customerSignature', [
        '$scope', '$state', '$stateParams', 'abp.services.app.jobs',
        function ($scope, $state, $stateParams, jobsService) {
            var vm = this;
            vm.saving = false;
            vm.jobId = $stateParams.guid;
            $scope.date = new Date();
            vm.jobs = {};

            function init() {
                console.log(vm.jobId);
                jobsService.getJobsDetailsByGuid({
                    JobGuid: vm.jobId
                }).then(function (result) {
                    vm.jobs = result.data;
                    console.log(vm.jobs);
                });
            }
            init();
        }
    ]);
})();