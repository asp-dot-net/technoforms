﻿(function () {
	appModule.controller('tenant.views.jobs.installersignatureimage', [
		'$scope', '$uibModalInstance', 'abp.services.app.jobs', 'jobId',
		function ($scope, $uibModalInstance, jobsService, jobId) {
			var vm = this;

			vm.saving = false;
			vm.panelimagelist = {};
			vm.installersignaturecount = 0;
			vm.displayVerifyIS = true;
			vm.save = function () {
			};
			vm.imageName = "";
			$scope.myInterval = 5000;
			$scope.noWrapSlides = false;
			$scope.active = 0;
			var currIndex = 0;
			var slides = $scope.slides = [];

			function shuffle(array) {
				var tmp, current, top = array.length;

				if (top) {
					while (--top) {
						current = Math.floor(Math.random() * (top + 1));
						tmp = array[current];
						array[current] = array[top];
						array[top] = tmp;
					}
				}

				return array;
			}
			vm.cancel = function () {
				$uibModalInstance.dismiss();
			};
			vm.verify = function () {

				abp.message.confirm(
					app.localize('verifyinstallerphoto'),
					function (isConfirmed) {
						if (isConfirmed) {
							jobsService.updateVerifyIS({ JobId: jobId }).then(function () {
								abp.notify.info(app.localize('SavedSuccessfully'));
								$uibModalInstance.close();
								$state.go('tenant.jobs');
							}).finally(function () {
								vm.saving = false;
							});
						}
					}
				);
			}
			function init() {

				jobsService.getTenantName().then(function (result) {
					vm.tenantName = result.data;
					jobsService.getCustomerSignaturePhotos({ JobId: jobId }).then(function (result) {
						console.log(result.data.items);
						vm.panelimagelist = result.data.items;
						//angular.forEach(result.data.items, function (value, key) {
						if (result.data.items[0].signatureImageInst != null && result.data.items[0].signatureImageInst != 'undefined') {
							vm.installersignaturecount++;
						}
						if (result.data.items[0].installerPhoto != null && result.data.items[0].installerPhoto != 'undefined') {
							vm.installersignaturecount++;
						}
						console.log(result.data.items);
						if (result.data.items[0].signatureImageInst != null && result.data.items[0].signatureImageInst != 'undefined') {
							slides.push({
								panelBarcodePhoto: "../../Temp/" + vm.tenantName + "/" + result.data.items[0].formId + "/InstallerSignature/" + result.data.items[0].signatureImageInst,
								longitude: result.data.items[0].longitude_InstSign,
								latitude: result.data.items[0].latitude_InstSign,
								date: result.data.items[0].creationTime,
								imagecount: vm.installersignaturecount,
								instcomment: 0,
								nameWithAddress: "Installer Signature"
							});
						}
						if (result.data.items[0].installerPhoto != null && result.data.items[0].installerPhoto != 'undefined') {
							slides.push({
								panelBarcodePhoto: "../../Temp/" + vm.tenantName + "/" + result.data.items[0].formId + "/InstallerPhoto/" + result.data.items[0].installerPhoto,
								longitude: result.data.items[0].longitude_InstSign,
								latitude: result.data.items[0].latitude_InstSign,
								date: result.data.items[0].creationTime,
								imagecount: vm.installersignaturecount,
								instcomment: 0,
								nameWithAddress: "Installer Photo"
							});
						}

						if (result.data.items[0].electricianSignature != null && result.data.items[0].electricianSignature != 'undefined') {
							slides.push({
								panelBarcodePhoto: "../../Temp/" + vm.tenantName + "/" + result.data.items[0].formId + "/ElectricianSignature/" + result.data.items[0].electricianSignature,
								longitude: result.data.items[0].longitude_ElecSign,
								latitude: result.data.items[0].latitude_ElecSign,
								date: result.data.items[0].electricianSignatureDate,
								instcomment: 0,
								nameWithAddress: "Electrician Signature"
							});
						}
						if (result.data.items[0].installerPhoto != null && result.data.items[0].installerPhoto != 'undefined') {
							slides.push({
								panelBarcodePhoto: "../../Temp/" + vm.tenantName + "/" + result.data.items[0].formId + "/InstallerPhoto/" + result.data.items[0].installerPhoto,
								longitude: result.data.items[0].longitude_InstSign,
								latitude: result.data.items[0].latitude_InstSign,
								date: result.data.items[0].creationTime,
								instcomment: 0,
								nameWithAddress: "Electrician Photo"
							});
						}

						if (result.data.items[0].designerSignature != null && result.data.items[0].designerSignature != 'undefined') {
							slides.push({
								panelBarcodePhoto: "../../Temp/" + vm.tenantName + "/" + result.data.items[0].formId + "/DesignerSignature/" + result.data.items[0].designerSignature,
								longitude: result.data.items[0].longitude_DesiSign,
								latitude: result.data.items[0].latitude_DesiSign,
								date: result.data.items[0].designerSignatureDate,
								instcomment: 0,
								nameWithAddress: "Designer Signature"
							});
						}
						if (result.data.items[0].installerPhoto != null && result.data.items[0].installerPhoto != 'undefined') {
							slides.push({
								panelBarcodePhoto: "../../Temp/" + vm.tenantName + "/" + result.data.items[0].formId + "/InstallerPhoto/" + result.data.items[0].installerPhoto,
								longitude: result.data.items[0].longitude_InstSign,
								latitude: result.data.items[0].latitude_InstSign,
								date: result.data.items[0].creationTime,
								instcomment: 0,
								nameWithAddress: "Designer Photo"
							});
						}
						console.log(slides);
						//  });
					}).finally(function () {
						vm.loading = false;
					});
				});

				jobsService.getJobsForEdit({ Id: jobId }).then(function (result) {
					if (result.data.isVerifyIS) {
						vm.displayVerifyIS = false;
					}
				}).finally(function () {
					vm.loading = false;
				});
			}

			init();
		}
	]);
})();