﻿(function () {
    appModule.controller('tenant.views.jobs.editScanedinverterSerialNumber', [
        '$scope', '$state', '$uibModalInstance', 'abp.services.app.jobs', 'inverterId', 'tenantid',
        function ($scope, $state, $uibModalInstance, jobsService, inverterId, tenantid) {
            var vm = this;

            vm.saving = false;
            vm.scanedInverter = {};
            vm.save = function () {
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {
                jobsService.getScanedInverterById({
                    Id: inverterId,
                    TenantId: tenantid
                }).then(function (result) {
                    vm.scanedInverter = result.data;
                });
            }

            vm.save = function () {
                vm.saving = true;
                vm.scanedInverter.id = inverterId;
                jobsService.updateJobScanedInverter(vm.scanedInverter)
                    .then(function () {
                        abp.notify.info(app.localize('SavedSuccessfully'));
                        $uibModalInstance.close();
                        $state.go('tenant.jobs');
                    }).finally(function () {
                        vm.saving = false;
                    });
            };

            init();
        }
    ]);
})();