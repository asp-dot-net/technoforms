﻿(function () {
    appModule.controller('tenant.views.jobs.forms.electricalWorksRequest', [
        '$scope', '$uibModalInstance',
        function ($scope, $uibModalInstance) {
            var vm = this;

            vm.saving = false;

            vm.save = function () {

            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {

            }

            init();
        }
    ]);
})();