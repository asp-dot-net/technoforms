﻿(function () {
	appModule.controller('tenant.views.jobs.scanedPanelview', [
		'$scope', '$uibModalInstance', 'abp.services.app.jobs', 'scanedPanelId',
		function ($scope, $uibModalInstance, jobsService, scanedPanelId) {
			var vm = this;

			vm.saving = false;
			vm.panelimagelist = {};
			vm.panelcount = 0;

			vm.displayVerifyPA = true;
			vm.save = function () {
			};

			$scope.myInterval = 5000;
			$scope.noWrapSlides = false;
			$scope.active = 0;
			var slides = $scope.slides = [];

			vm.cancel = function () {
				$uibModalInstance.dismiss();
			};

			function init() {
				jobsService.getTenantName().then(function (result) {
					vm.tenantName = result.data;
					jobsService.scanedPanelView({ ScannedId: scanedPanelId }).then(function (result) {
						vm.panelimagelist = result.data.items;
						angular.forEach(result.data.items, function (value, key) {
							vm.panelcount++;
						});
						angular.forEach(result.data.items, function (value, key) {
							if (value.panelBarcodePhoto != null || value.panelBarcodePhoto != 'undefined') {
								slides.push({
									id: value.id,
									panelSerialNumber: value.panelSerialNumber,
									panelBarcodePhoto: "../../Temp/" + vm.tenantName + "/" + result.data.items[0].formId + "/ScanedPanels/" + value.panelBarcodePhoto,
									longitude: value.longitude,
									latitude: value.latitude,
									date: value.creationTime,
									imagecount: vm.panelcount
								});
							}
						});
					}).finally(function () {
						vm.loading = false;
					});
				});
			}
			init();
		}
	]);
})();