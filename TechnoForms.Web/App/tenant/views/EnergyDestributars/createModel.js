﻿(function () {
	appModule.controller('tenant.views.energyDestributars.createModel', [
		'$scope', '$state', '$uibModalInstance', '$uibModal', 'uiGridConstants', 'abp.services.app.energyDestributar',
		function ($scope, $state, $uibModalInstance, $uibModal, uiGridConstants, energyDestributarService) {
			var vm = this;
			vm.saving = false;
			vm.exist = false;
			vm.message = "";
			vm.energyDestributar = {};
			
			vm.save = function () {
				vm.saving = true;
				energyDestributarService.createEnergyDestributar(vm.energyDestributar).then(function () {
					abp.notify.info(app.localize('SavedSuccessfully'));
					$uibModalInstance.close();
				}).finally(function () {
					vm.saving = false;
					$uibModalInstance.close();
				});
			};

			vm.cancel = function () {
				$uibModalInstance.dismiss({});
			};
		}
	]);
})();