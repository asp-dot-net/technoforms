﻿(function () {
	appModule.controller('tenant.views.energyDestributars.index', [
		'$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.energyDestributar',
		function ($scope, $state, $uibModal, uiGridConstants, energyDestributarService) {
			var vm = this;
			vm.norecord = false;
			$scope.$on('$viewContentLoaded', function () {
				App.initAjax();
			});

			vm.loading = false;

			vm.permissions = {
				createed: abp.auth.hasPermission('Pages.Tenant.EnergyDestributars.Create'),
				edited: abp.auth.hasPermission('Pages.Tenant.EnergyDestributars.Edit'),
				deleteed: abp.auth.hasPermission('Pages.Tenant.EnergyDestributars.Delete')
			};

			var requestParams = {
				skipCount: 0,
				maxResultCount: app.consts.grid.defaultPageSize,
				sorting: null
			};

			vm.edGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						enableColumnMenu: false,
						cellClass: 'centeralign',
						headerCellClass: 'centeralign',
						width: 80,
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
							'    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
							'    <ul uib-dropdown-menu>' +
							'      <li><a ng-if="grid.appScope.permissions.edited" ng-click="grid.appScope.edited(row.entity)">' + app.localize('Edit') + '</a></li>' +
							'      <li><a ng-if="grid.appScope.permissions.deleteed && row.entity.isApproved == 0" ng-click="grid.appScope.deleteed(row.entity)">' + app.localize('Delete') + '</a></li>' +
							'    </ul>' +
							'  </div>' +
							'</div>'
					},
					{
						name: app.localize('Name'),
						field: 'name',
						enableColumnMenu: false,
					},
					{
						name: app.localize('NSW'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'nsw',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.nsw" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.nsw" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('SA'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'sa',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.sa" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.sa" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('QLD'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'qld',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.qld" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.qld" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('VIC'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'vic',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.vic" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.vic" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('WA'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'wa',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.wa" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.wa" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('ACT'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'act',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.act" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.act" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('TAS'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'tas',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.tas" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.tas" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('NT'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'nt',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.nt" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.nt" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('CreationTime'),
						field: 'creationTime',
						enableColumnMenu: false,
						width: 150,
						cellClass: 'cellwdth110 centeralign',
						cellFilter: 'date:\'dd-MM-yyyy\''
					},

				],
				onRegisterApi: function (gridApi) {
					$scope.gridApi = gridApi;
					$scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
						if (!sortColumns.length || !sortColumns[0].field) {
							requestParams.sorting = null;
						} else {
							requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
						}

						vm.geted();
					});
					gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
						requestParams.skipCount = (pageNumber - 1) * pageSize;
						requestParams.maxResultCount = pageSize;

						vm.geted();
					});
				},
				data: []
			};

			if (!vm.permissions.edited && !vm.permissions.deleteed) {
				vm.edGridOptions.columnDefs.shift();
			}

			vm.geted = function () {
				vm.loading = true;
				energyDestributarService.getEnergyDestributar({
					skipCount: requestParams.skipCount,
					maxResultCount: requestParams.maxResultCount,
					sorting: requestParams.sorting,
					fullName: vm.fullName,
					state: vm.filterstate
				}).then(function (result) {
					vm.edGridOptions.totalItems = result.data.totalCount;
					vm.edGridOptions.data = result.data.items;
					if (result.data.totalCount == 0) {
						abp.notify.info(app.localize('NoRecordFound'));
					}
					else { vm.norecord = false; }
				}).finally(function () {
					vm.loading = false;
				});
			};

			vm.ClearAll = function () {
				vm.fullName = null;
				vm.filterstate = null;
				setTimeout(function () {
					$('#stateSelectionCombobox').selectpicker('refresh');
				}, 0);

				vm.geted();
			};

			vm.createed = function () {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/energyDestributars/createModel.cshtml',
					controller: 'tenant.views.energyDestributars.createModel as vm',
					backdrop: 'static'
				});
				modalInstance.result.then(function () {
					vm.geted();
				});
			};

			vm.deleteed = function (ed) {
				abp.message.confirm(
					app.localize('EnergyDestributarDeleteWarningMessage', ed.fullName),
					function (isConfirmed) {
						if (isConfirmed) {
							energyDestributarService.deleteEnergyDestributar({
								id: ed.id
							}).then(function () {
								vm.geted();
								abp.notify.success(app.localize('SuccessfullyDeleted'));
							});
						}
					}
				);
			};

			vm.edited = function (ed) {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/energyDestributars/editModel.cshtml',
					controller: 'tenant.views.energyDestributars.editModel as vm',
					backdrop: 'static',
					resolve: {
						Id: function () {
							return ed.id;
						}
					}
				});

				modalInstance.result.then(function (result) {
					vm.geted();
				});
			};

			vm.geted();
		}
	]);
})();