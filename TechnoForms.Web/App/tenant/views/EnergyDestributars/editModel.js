﻿(function () {
	appModule.controller('tenant.views.energyDestributars.editModel', [
		'$scope', '$state', '$uibModalInstance', '$uibModal', 'uiGridConstants', 'abp.services.app.energyDestributar','Id',
		function ($scope, $state, $uibModalInstance, $uibModal, uiGridConstants, energyDestributarService, Id) {
			var vm = this;
			vm.saving = false;
			vm.energyDestributar = null;
			vm.editions = [];

			vm.exist = false;
			vm.message = "";

			vm.save = function () {
				vm.saving = true;
				energyDestributarService.updateEnergyDestributar(vm.energyDestributar)
					.then(function () {
						abp.notify.info(app.localize('SavedSuccessfully'));
						$uibModalInstance.close();
					}).finally(function () {
						vm.saving = false;
					});
			};

			vm.cancel = function () {
				$uibModalInstance.dismiss();
			};

			function init() {
				energyDestributarService.getEnergyDestributarForEdit({
					id: Id
				}).then(function (result) {
					vm.energyDestributar = result.data;
					console.log(vm.energyDestributar);
				});
			}
			init();
		}
	]);
})();