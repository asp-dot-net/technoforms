﻿(function () {
    appModule.controller('tenant.views.installerInvitation.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.installerInvitation',
        function ($scope, $uibModal, uiGridConstants, installerInvitationService) {
            var vm = this;

            vm.invitationlist = [];
            vm.editingInvitation = null;
            vm.invitationedit = [];
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.installerInvitationGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        width: 200,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-click="grid.appScope.detailInvitation(row.entity)">' + app.localize('Detail') + '</a></li>' +
                            '      <li><a ng-click="grid.appScope.approveInvitation(row.entity)">' + app.localize('Approve') + '</a></li>' +
                            '      <li><a ng-click="grid.appScope.rejectInvitation(row.entity)">' + app.localize('Reject') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('fullName'),
                        field: 'fullName'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getinvitationList();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getinvitationList();
                    });
                },
                data: []
            };

            vm.getinvitationList = function () {
                vm.loading = true;
                installerInvitationService.getInstallerInvitation({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).then(function (result) {
                    vm.installerInvitationGridOptions.totalItems = result.data.totalCount;
                    vm.installerInvitationGridOptions.data = result.data.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            
            vm.detailInvitation = function (installerInvitation) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/tenant/views/installerInvitation/detailInstallerModel.cshtml',
                    controller: 'tenant.views.installerInvitation.detailInstallerModel as vm',
                    backdrop: 'static',
                    resolve: {
                        InstallerInvitationId: function () {
                            return installerInvitation.id;                            
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getinvitationList();
                });
            };
            vm.approveInvitation = function (installerInvitation) {
                abp.message.confirm(
                    app.localize('AreYouSureToApproveinvitation', installerInvitation.fullName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            installerInvitationService.approveInstallerInvitation({
                                id: installerInvitation.id
                            }).then(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getinvitationList();
                            });
                        }
                    }
                );
            };

            vm.rejectInvitation = function (installerInvitation) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/tenant/views/installerInvitation/rejectModel.cshtml',
                    controller: 'tenant.views.installerInvitation.rejectModel as vm',
                    backdrop: 'static',
                    resolve: {
                        InstallerInvitationId: function () {
                            return installerInvitation.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getinvitationList();
                });
            };
            vm.getinvitationList();

        }
    ]);
})();