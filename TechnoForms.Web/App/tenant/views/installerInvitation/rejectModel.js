﻿(function () {
    appModule.controller('tenant.views.installerInvitation.rejectModel', [
        '$scope', '$uibModalInstance', 'abp.services.app.installerInvitation', 'InstallerInvitationId',
        function ($scope, $uibModalInstance, installerInvitationService, InstallerInvitationId) {
            var vm = this;

            vm.saving = false;
            vm.invitation = {};

            vm.save = function () {
                vm.saving = true;
                installerInvitationService.rejectInstallerInvitation(vm.invitation).then(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $uibModalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                installerInvitationService.getInstallerInvitationForReject({
                    id: InstallerInvitationId
                }).then(function (result) {
                    vm.invitation = result.data;
                });
            }
            init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();