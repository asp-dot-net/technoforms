﻿(function () {
    appModule.controller('tenant.views.installerInvitation.detailInstallerModel', [
        '$scope', '$uibModalInstance','$state', 'abp.services.app.installerInvitation', 'InstallerInvitationId',
        function ($scope, $uibModalInstance, $state, installerInvitationService, InstallerInvitationId) {
            var vm = this;

            vm.saving = false;
            vm.installer = {};
            vm.stateNames = [];
            vm.postCodes = [];
            vm.suburbs = [];
            vm.addressTypes = [];
            vm.deliveryTypes = [];
            vm.unitTypes = [];
            vm.streetNames = [];
            vm.streetTypes = [];

            function init() {
                installerInvitationService.getInstallerInvitationDetail({
                    id: InstallerInvitationId
                }).then(function (result) {
                    vm.installer = result.data;

                    installerInvitationService.getUsers().then(function (result) {
                        vm.users = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#userSelectionCombobox').selectpicker('refresh');
                            $('#userSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerInvitationService.getPostalAddressType().then(function (result) {
                        vm.addressTypes = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#addressTypeSelectionCombobox').selectpicker('refresh');
                            $('#addressTypeSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerInvitationService.getPostalDeliveryType().then(function (result) {
                        vm.deliveryTypes = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#deliveryTypeSelectionCombobox').selectpicker('refresh');
                            $('#deliveryTypeSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerInvitationService.getUnitType().then(function (result) {
                        vm.unitTypes = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#unitTypeSelectionCombobox').selectpicker('refresh');
                            $('#unitTypeSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerInvitationService.getStreetName().then(function (result) {
                        vm.streetNames = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#streetNameSelectionCombobox').selectpicker('refresh');
                            $('#streetNameSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerInvitationService.getStreetType().then(function (result) {
                        vm.streetTypes = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#streetTypeSelectionCombobox').selectpicker('refresh');
                            $('#streetTypeSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerInvitationService.getState().then(function (result) {
                        vm.stateNames = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#stateSelectionCombobox').selectpicker('refresh');
                            $('#stateSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerInvitationService.getPostCode().then(function (result) {
                        vm.postCodes = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#postCodeSelectionCombobox').selectpicker('refresh');
                            $('#postCodeSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerInvitationService.getSuburb().then(function (result) {
                        vm.suburbs = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#suburbSelectionCombobox').selectpicker('refresh');
                            $('#suburbSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                });
            }
            init();

            vm.cancel = function () {
                $state.go('tenant.installerInvitation', {
                });
            };
        }
    ]);
})();