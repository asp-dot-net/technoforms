﻿(function () {
	appModule.controller('tenant.views.energyRetailers.createModel', [
		'$scope', '$state', '$uibModalInstance', '$uibModal', 'uiGridConstants', 'abp.services.app.energyRetailer',
		function ($scope, $state, $uibModalInstance, $uibModal, uiGridConstants, energyRetailerservice) {
			var vm = this;
			vm.saving = false;
			vm.exist = false;
			vm.message = "";
			vm.energyRetailer = {};
			
			vm.save = function () {
				vm.saving = true;
				energyRetailerservice.createEnergyRetailer(vm.energyRetailer).then(function () {
					abp.notify.info(app.localize('SavedSuccessfully'));
					$uibModalInstance.close();
				}).finally(function () {
					vm.saving = false;
					$uibModalInstance.close();
				});
			};

			vm.cancel = function () {
				$uibModalInstance.dismiss({});
			};
		}
	]);
})();