﻿(function () {
	appModule.controller('tenant.views.energyRetailers.editModel', [
		'$scope', '$state', '$uibModalInstance', '$uibModal', 'uiGridConstants', 'abp.services.app.energyRetailer','Id',
		function ($scope, $state, $uibModalInstance, $uibModal, uiGridConstants, energyRetailerservice, Id) {
			var vm = this;
			vm.saving = false;
			vm.energyRetailer = null;
			vm.editions = [];

			vm.exist = false;
			vm.message = "";

			vm.save = function () {
				vm.saving = true;
				energyRetailerservice.updateEnergyRetailer(vm.energyRetailer)
					.then(function () {
						abp.notify.info(app.localize('SavedSuccessfully'));
						$uibModalInstance.close();
					}).finally(function () {
						vm.saving = false;
					});
			};

			vm.cancel = function () {
				$uibModalInstance.dismiss();
			};

			function init() {
				energyRetailerservice.getEnergyRetailerForEdit({
					id: Id
				}).then(function (result) {
					vm.energyRetailer = result.data;
				});
			}
			init();
		}
	]);
})();