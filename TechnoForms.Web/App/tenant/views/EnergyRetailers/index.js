﻿(function () {
	appModule.controller('tenant.views.energyRetailers.index', [
		'$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.energyRetailer',
		function ($scope, $state, $uibModal, uiGridConstants, energyRetailerservice) {
			var vm = this;
			vm.norecord = false;
			$scope.$on('$viewContentLoaded', function () {
				App.initAjax();
			});

			vm.loading = false;

			vm.permissions = {
				createer: abp.auth.hasPermission('Pages.Tenant.EnergyRetailers.Create'),
				editer: abp.auth.hasPermission('Pages.Tenant.EnergyRetailers.Edit'),
				deleteer: abp.auth.hasPermission('Pages.Tenant.EnergyRetailers.Delete')
			};

			var requestParams = {
				skipCount: 0,
				maxResultCount: app.consts.grid.defaultPageSize,
				sorting: null
			};

			vm.erGridOptions = {
				enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
				paginationPageSizes: app.consts.grid.defaultPageSizes,
				paginationPageSize: app.consts.grid.defaultPageSize,
				useExternalPagination: true,
				useExternalSorting: true,
				appScopeProvider: vm,
				rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
				columnDefs: [
					{
						name: app.localize('Actions'),
						enableSorting: false,
						enableColumnMenu: false,
						cellClass: 'centeralign',
						headerCellClass: 'centeralign',
						width: 80,
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
							'    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
							'    <ul uib-dropdown-menu>' +
							'      <li><a ng-if="grid.appScope.permissions.editer" ng-click="grid.appScope.editer(row.entity)">' + app.localize('Edit') + '</a></li>' +
							'      <li><a ng-if="grid.appScope.permissions.deleteer && row.entity.isApproved == 0" ng-click="grid.appScope.deleteer(row.entity)">' + app.localize('Delete') + '</a></li>' +
							'    </ul>' +
							'  </div>' +
							'</div>'
					},
					{
						name: app.localize('Name'),
						field: 'name',
						enableColumnMenu: false,
					},
					{
						name: app.localize('NSW'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'nsw',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.nsw" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.nsw" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('SA'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'sa',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.sa" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.sa" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('QLD'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'qld',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.qld" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.qld" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('VIC'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'vic',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.vic" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.vic" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('WA'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'wa',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.wa" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.wa" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('ACT'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'act',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.act" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.act" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('TAS'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'tas',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.tas" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.tas" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('NT'),
						enableColumnMenu: false,
						enableSorting: false,
						field: 'nt',
						cellTemplate:
							'<div class=\"ui-grid-cell-contents\">' +
							'  <span ng-show="row.entity.nt" class="label label-success">' + app.localize('Yes') + '</span>' +
							'  <span ng-show="!row.entity.nt" class="label label-default">' + app.localize('No') + '</span>' +
							'</div>',
						cellClass: 'centeralign',
						minWidth: 50
					},
					{
						name: app.localize('CreationTime'),
						field: 'creationTime',
						enableColumnMenu: false,
						width: 150,
						cellClass: 'cellwdth110 centeralign',
						cellFilter: 'date:\'dd-MM-yyyy\''
					},

				],
				onRegisterApi: function (gridApi) {
					$scope.gridApi = gridApi;
					$scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
						if (!sortColumns.length || !sortColumns[0].field) {
							requestParams.sorting = null;
						} else {
							requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
						}

						vm.geter();
					});
					gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
						requestParams.skipCount = (pageNumber - 1) * pageSize;
						requestParams.maxResultCount = pageSize;

						vm.geter();
					});
				},
				data: []
			};

			if (!vm.permissions.editer && !vm.permissions.deleteer) {
				vm.erGridOptions.columnDefs.shift();
			}

			vm.geter = function () {
				vm.loading = true;
				energyRetailerservice.getEnergyRetailer({
					skipCount: requestParams.skipCount,
					maxResultCount: requestParams.maxResultCount,
					sorting: requestParams.sorting,
					fullName: vm.fullName,
					state: vm.filterstate
				}).then(function (result) {
					vm.erGridOptions.totalItems = result.data.totalCount;
					vm.erGridOptions.data = result.data.items;
					if (result.data.totalCount == 0) {
						abp.notify.info(app.localize('NoRecordFound'));
					}
					else { vm.norecord = false; }
				}).finally(function () {
					vm.loading = false;
				});
			};

			vm.ClearAll = function () {
				vm.fullName = null;
				vm.filterstate = null;
				setTimeout(function () {
					$('#stateSelectionCombobox').selectpicker('refresh');
				}, 0);

				vm.geter();
			};

			vm.createer = function () {
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/energyRetailers/createModel.cshtml',
					controller: 'tenant.views.energyRetailers.createModel as vm',
					backdrop: 'static'
				});
				modalInstance.result.then(function () {
					vm.geter();
				});
			};

			vm.deleteer = function (er) {
				abp.message.confirm(
					app.localize('EnergyRetailerDeleteWarningMessage', er.fullName),
					function (isConfirmed) {
						if (isConfirmed) {
							energyRetailerservice.deleteEnergyDestributar({
								id: er.id
							}).then(function () {
								vm.geter();
								abp.notify.success(app.localize('SuccessfullyDeleted'));
							});
						}
					}
				);
			};

			vm.editer = function (er) {
				alert(er);
				console.log(er);
				var modalInstance = $uibModal.open({
					templateUrl: '/App/tenant/views/energyRetailers/editModel.cshtml',
					controller: 'tenant.views.energyRetailers.editModel as vm',
					backdrop: 'static',
					resolve: {
						Id: function () {
							return er.id;
						}
					}
				});

				modalInstance.result.then(function (result) {
					vm.geter();
				});
			};

			vm.geter();
		}
	]);
})();