﻿(function () {
    appModule.controller('tenant.views.installer.employeeList', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.installer', 'uiGridConstants',
        function ($scope, $state, $stateParams, $uibModal, installerService, uiGridConstants) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            //Grid data
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            //Combobox values
            vm.sourceNames = [];
            vm.languages = [];

            vm.InstallerId = $stateParams.InstallerId;

            vm.loading = false;

            vm.installerEmployeeGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        enableColumnMenu: false,
                        headerCellClass:'centeralign',
                        cellClass:'centeralign',
                        width:80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-click="grid.appScope.detailInstallerEmployee(row.entity)">' + app.localize('Detail') + '</a></li>' +
                            '      <li><a ng-click="grid.appScope.editInstallerEmployee(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-click="grid.appScope.deleteInstallerEmployee(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('FullName'),
                        width: 200,
                        enableColumnMenu: false,
                        field: 'fullName'
                    },
                    {
                        name: app.localize('EmailId'),
                        field: 'emailId',
                        enableColumnMenu: false,
                        minWidth:200
                    },
                    {
                        name: app.localize('Phone'),
                        width: 130,
                        enableColumnMenu: false,
                        headerCellClass:'centeralign',
                        cellClass:'centeralign',
                        field: 'phone'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        enableColumnMenu: false,
                        headerCellClass:'centeralign',
                        width: 150,
                        cellClass: 'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getInstallerEmployee();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getInstallerEmployee();
                    });
                },
                data: []
            };

            function initialize() {
                function reloadWhenChange(variableName) {
                    $scope.$watch(variableName, function (newValue, oldValue) {
                        if (newValue == oldValue) {
                            return;
                        }

                        $state.go('installerEmployee', {
                            InstallerId: vm.InstallerId
                        });
                    });
                }
                reloadWhenChange('vm.InstallerId');
            }

            vm.getInstallerEmployee = function () {
                vm.loading = true;

                installerService.getInstallerForEdit({
                    id: vm.InstallerId
                }).then(function (result) {
                    vm.installer = result.data;
                    vm.installerEmp.installerId = installerId;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                });

                installerService.getInstallerEmployee({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    InstallerId: vm.InstallerId
                }).then(function (result) {
                
                    vm.installerEmployeeGridOptions.totalItems = result.data.totalCount;
                    vm.installerEmployeeGridOptions.data = result.data.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createInstallerEmp = function (installer) {
                $state.go('createEmpInstaller', {
                    installerId: installer.id
                });
                //var modalInstance = $uibModal.open({
                //    templateUrl: '/App/tenant/views/installer/createEmpModel.cshtml',
                //    controller: 'tenant.views.installer.createEmpModel as vm',
                //    backdrop: 'static',
                //    resolve: {
                //        installerId: function () {
                //            return installer;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getInstallerEmployee();
                //});
            };

            vm.deleteInstallerEmployee = function (instEmp) {
                abp.message.confirm(
                    app.localize('InstallerEmpDeleteWarningMessage', instEmp.fullName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            installerService.deleteInstallerEmployee({
                                id: instEmp.id
                            }).then(function () {
                                vm.getInstallerEmployee();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.editInstallerEmployee = function (installer) {
                $state.go('editEmpInstaller', {
                    InstallerId: installer.installerId,
                    empId: installer.id
                });
                //var modalInstance = $uibModal.open({
                //    templateUrl: '/App/tenant/views/installer/editEmpModel.cshtml',
                //    controller: 'tenant.views.installer.editEmpModel as vm',
                //    backdrop: 'static',
                //    resolve: {
                //        installerEmpId: function () {
                //            return installer.id;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getInstallerEmployee();
                //});
            };

            vm.detailInstallerEmployee = function (installer) {
                $state.go('detailEmpInstaller', {
                    InstallerId: installer.installerId,
                    empId: installer.id
                });
                //var modalInstance = $uibModal.open({
                //    templateUrl: '/App/tenant/views/installer/detailEmpModel.cshtml',
                //    controller: 'tenant.views.installer.detailEmpModel as vm',
                //    backdrop: 'static',
                //    resolve: {
                //        installerEmpId: function () {
                //            return installer.id;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getInstallerEmployee();
                //});
            };

            initialize();
            vm.getInstallerEmployee();
            vm.cancel = function () {
                $state.go('tenant.installer', {
                });
            };
        }
    ]);
})();