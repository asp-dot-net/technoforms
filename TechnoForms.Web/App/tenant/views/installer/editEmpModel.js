﻿(function () {
	appModule.controller('tenant.views.installer.editEmpModel', [
		'$scope', '$state', '$stateParams', 'abp.services.app.installer', 'abp.services.app.jobs',
		function ($scope, $state, $stateParams, installerService, jobsService) {
			var vm = this;

			vm.saving = false;
			vm.installerEmp = {};
			vm.stateNames = [];
			vm.postCodes = [];
			vm.suburbs = [];
			vm.addressTypes = [];
			vm.deliveryTypes = [];
			vm.unitTypes = [];
			vm.streetNames = [];
			vm.streetTypes = [];
			vm.suburb = "";
			vm.IsGoogleAddress = true;

			vm.empId = $stateParams.empId;
			vm.InstallerId = $stateParams.InstallerId;

			//DatePIcker Code Start
			$scope.today = function () {
				$scope.dt = new Date();
			};
			//$scope.today();

			$scope.clear = function () {
				$scope.dt = null;
			};

			$scope.inlineOptions = {
				customClass: getDayClass,
				minDate: new Date(),
				showWeeks: true
			};

			//Disable past dates
			$scope.dateOptions = {
				//dateDisabled: disabled,
				formatYear: 'yy',
				//maxDate: new Date(2020, 5, 22),
				minDate: new Date(),
				startingDay: 1
			};

			// Disable weekend selection
			function disabled(data) {
				var date = data.date,
					mode = data.mode;
				return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
			}

			//$scope.toggleMin = function () {
			//    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
			//    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
			//};

			//$scope.toggleMin();

			$scope.open1 = function () {
				$scope.popup1.opened = true;
			};

			$scope.open2 = function () {
				$scope.popup2.opened = true;
			};

			$scope.open3 = function () {
				$scope.popup3.opened = true;
			};

			$scope.setDate = function (year, month, day) {
				$scope.dt = new Date(year, month, day);
			};

			$scope.formats = ['d/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			$scope.format = $scope.formats[0];
			$scope.altInputFormats = ['M!/d!/yyyy'];

			$scope.popup1 = {
				opened: false
			};

			$scope.popup2 = {
				opened: false
			};

			$scope.popup3 = {
				opened: false
			};

			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);
			var afterTomorrow = new Date();
			afterTomorrow.setDate(tomorrow.getDate() + 1);
			$scope.events = [
				{
					date: tomorrow,
					status: 'full'
				},
				{
					date: afterTomorrow,
					status: 'partially'
				}
			];

			function getDayClass(data) {
				var date = data.date,
					mode = data.mode;
				if (mode === 'day') {
					var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

					for (var i = 0; i < $scope.events.length; i++) {
						var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

						if (dayToCheck === currentDay) {
							return $scope.events[i].status;
						}
					}
				}

				return '';
			}
			//DatePIcker Code End

			//vm.getPostCodes = function (streetCity) {
			//    if (streetCity != null) {
			//        jobsService.getPostCodesListBySuburb({ id: streetCity }).then(function (result) {
			//            vm.installerEmp.suburb = result.data.items[0].suburb;
			//            vm.postCodes = result.data.items;
			//            setTimeout(function () {
			//                $('#postCodeSelectionCombobox').selectpicker('refresh');
			//                $('#postCodeSelectionCombobox').selectpicker('refresh');
			//            }, 0);
			//            vm.stateNames = result.data.items;
			//            setTimeout(function () {
			//                $('#stateSelectionCombobox').selectpicker('refresh');
			//                $('#stateSelectionCombobox').selectpicker('refresh');
			//            }, 0);
			//            console.log(vm.postCodes);
			//            console.log(vm.stateNames);
			//            vm.installerEmp.state = result.data.items[0].stateName;
			//            vm.installerEmp.postCode = result.data.items[0].postcode;

			//            vm.loading = false;
			//        });
			//    }
			//};

			//vm.getStreetAddress = function () {
			//    var unittype = "";
			//    var unitNo = "";
			//    var streetNo = "";
			//    var streetName = "";
			//    var streetType = "";
			//    if (vm.installerEmp.unitType != null) {
			//        unittype = vm.installerEmp.unitType;
			//    }
			//    if (vm.installerEmp.unitNo != null) {
			//        unitNo = vm.installerEmp.unitNo;
			//    }
			//    if (vm.installerEmp.streetNumber != null) {
			//        streetNo = vm.installerEmp.streetNumber;
			//    }
			//    if (vm.installerEmp.streetName != null) {
			//        streetName = vm.installerEmp.streetName;
			//    }
			//    if (vm.installerEmp.streetType != null) {
			//        streetType = vm.installerEmp.streetType;
			//    }
			//    vm.installerEmp.streetAddress = unittype + " " + unitNo + " " + streetNo + " " + streetName + " " + streetType;
			//};

			vm.checkInstallerCodeEdit = function () {
				if (vm.installerEmp.installerExpiry != null) {
					installerService.checkInstallerCodeEdit(vm.installerEmp.installerExpiry, vm.InstallerId).then(function (result) {
						if (result.data != 0) {
							vm.installerEmp.installerExpiry = "";
							abp.notify.info(app.localize('InstallerCodeAlreadyExist'));
						}
						vm.loading = false;
					});
				}
			};
			vm.checkDesignerCodeEdit = function () {
				if (vm.installerEmp.designerExpiry != null) {
					installerService.checkDesignerCodeEdit(vm.installerEmp.designerExpiry, vm.InstallerId).then(function (result) {
						if (result.data != 0) {
							vm.installerEmp.designerExpiry = "";
							abp.notify.info(app.localize('DesignerCodeAlreadyExist'));
						}
						vm.loading = false;
					});
				}
			};
			vm.checkElecCodeEdit = function () {
				if (vm.installerEmp.electricalExpiry != null) {
					installerService.checkElecCodeEdit(vm.installerEmp.electricalExpiry, vm.InstallerId).then(function (result) {
						if (result.data != 0) {
							vm.installerEmp.electricalExpiry = "";
							abp.notify.info(app.localize('EleLicenceCodeAlreadyExist'));
						}
						vm.loading = false;
					});
				}
			};

			vm.CheckNumber = function () {
				if (isNaN(event.key) || event.key === ' ' || event.key === '') {
					event.returnValue = '';
				}
			};

			vm.save = function () {
				vm.saving = true;
				vm.installerEmp.streetNumber = $("#street_number").val();
				vm.installerEmp.streetName = $("#routeName").val();
				vm.installerEmp.streetType = $("#routeType").val();
				vm.installerEmp.suburb = $("#locality").val();
				var value = vm.installerEmp.suburb.split("|");
				vm.installerEmp.suburb = value[0].trim();
				vm.installerEmp.state = $("#administrative_area_level_1").val();
				vm.installerEmp.postCode = $("#postal_code").val();

				var unittype = "";
				var unitNo = "";
				var streetNo = "";
				var streetName = "";
				var streetType = "";
				if (vm.installerEmp.unitType != null) {
					unittype = vm.installerEmp.unitType;
				}
				if (vm.installerEmp.unitNo != null) {
					unitNo = vm.installerEmp.unitNo;
				}
				if (vm.installerEmp.streetNumber != null) {
					streetNo = vm.installerEmp.streetNumber;
				}
				if (vm.installerEmp.streetName != null) {
					streetName = vm.installerEmp.streetName;
				}
				if (vm.installerEmp.streetType != null) {
					streetType = vm.installerEmp.streetName;
				}
				vm.installerEmp.streetAddress = unittype + " " + unitNo + " " + streetNo + " " + streetName + " " + streetType;

				installerService.updateInstallerEmployee(vm.installerEmp).then(function () {
					$state.go('installerEmployee', {
						InstallerId: vm.InstallerId
					});
					abp.notify.info(app.localize('SavedSuccessfully'));

				}).finally(function () {
					vm.saving = false;
				});
			};

			function init() {
				installerService.getInstallerEmployeeForEdit({
					id: vm.empId
				}).then(function (result) {
					vm.installerEmp = result.data;
					vm.suburb = result.data.suburb;
					vm.installerEmp.insAccreExDate = new Date(vm.installerEmp.insAccreExDate);
					vm.installerEmp.eleLicenceExDate = new Date(vm.installerEmp.eleLicenceExDate);
					vm.installerEmp.designerAccreExDate = new Date(vm.installerEmp.designerAccreExDate);

					installerService.getPostalAddressType().then(function (result) {
						vm.addressTypes = result.data.items;
						vm.flags = result.data.flags;
						setTimeout(function () {
							$('#addressTypeSelectionCombobox').selectpicker('refresh');
							$('#addressTypeSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});

					installerService.getPostalDeliveryType().then(function (result) {
						vm.deliveryTypes = result.data.items;
						vm.flags = result.data.flags;
						setTimeout(function () {
							$('#deliveryTypeSelectionCombobox').selectpicker('refresh');
							$('#deliveryTypeSelectionCombobox').selectpicker('refresh');
						}, 0);
						vm.loading = false;
					});

					//installerService.getUnitType().then(function (result) {
					//    vm.unitTypes = result.data.items;
					//    vm.flags = result.data.flags;
					//    setTimeout(function () {
					//        $('#unitTypeSelectionCombobox').selectpicker('refresh');
					//        $('#unitTypeSelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});

					//installerService.getStreetName().then(function (result) {
					//    vm.streetNames = result.data.items;
					//    vm.flags = result.data.flags;
					//    setTimeout(function () {
					//        $('#streetNameSelectionCombobox').selectpicker('refresh');
					//        $('#streetNameSelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});

					//installerService.getStreetType().then(function (result) {
					//    vm.streetTypes = result.data.items;
					//    vm.flags = result.data.flags;
					//    setTimeout(function () {
					//        $('#streetTypeSelectionCombobox').selectpicker('refresh');
					//        $('#streetTypeSelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});

					//installerService.getState().then(function (result) {
					//    vm.stateNames = result.data.items;
					//    vm.flags = result.data.flags;
					//    setTimeout(function () {
					//        $('#stateSelectionCombobox').selectpicker('refresh');
					//        $('#stateSelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});

					//installerService.getPostCode().then(function (result) {
					//    vm.postCodes = result.data.items;
					//    vm.flags = result.data.flags;
					//    setTimeout(function () {
					//        $('#postCodeSelectionCombobox').selectpicker('refresh');
					//        $('#postCodeSelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});

					//installerService.getSuburb().then(function (result) {
					//    vm.suburbs = result.data.items;
					//    vm.flags = result.data.flags;
					//    setTimeout(function () {
					//        $('#suburbSelectionCombobox').selectpicker('refresh');
					//        $('#suburbSelectionCombobox').selectpicker('refresh');
					//    }, 0);
					//    vm.loading = false;
					//});

				});
			}
			init();

			vm.cancel = function () {
				$state.go('installerEmployee', {
					InstallerId: vm.InstallerId
				});
			};

			vm.getUnitTypeList = function () {
				installerService.getUnitType({
					text: vm.installerEmp.unitType
				}).then(function (result) {
					$(".UnitTypetypeahead").typeahead({ source: result.data });
				});
			};
			vm.getStreetNameList = function () {
				installerService.getStreetNames({
					text: vm.installerEmp.streetName
				}).then(function (result) {
					$(".StreetNametypeahead").typeahead({ source: result.data });
				});
			};
			vm.getStreetTypeList = function () {
				installerService.getStreetType({
					text: vm.installerEmp.streetType
				}).then(function (result) {
					$(".StreetTypetypeahead").typeahead({ source: result.data });
				});
			};
			vm.getSuburbList = function () {
				installerService.getAllPostCodes({
					text: vm.suburb
				}).then(function (result) {
					$(".Fnametypeahead").typeahead({ source: result.data, onSelect: displayStatePostcode });
				});
			};
			function displayStatePostcode(item) {
				var value = item.text.split("|");
				vm.installerEmp.suburb = value[0].trim();
				vm.installerEmp.Suburb_Id = item.value;
				vm.installerEmp.state = value[1].trim();
				vm.installerEmp.postCode = value[2].trim();
			}
			vm.google = function () {
				vm.IsGoogleAddress = true;
			};
			vm.manual = function () {
				vm.IsGoogleAddress = false;
			};
			vm.getStreetNameList();
			vm.getSuburbList();
			vm.getUnitTypeList();
			vm.getStreetTypeList();
		}
	]);
})();