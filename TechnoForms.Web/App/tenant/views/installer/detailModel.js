﻿(function () {
    appModule.controller('tenant.views.installer.detailModel', [
        '$scope', '$state', '$stateParams', 'abp.services.app.installer',
        function ($scope, $state, $stateParams, installerService) {
            var vm = this;

            vm.saving = false;
            vm.installer = {};
            vm.stateNames = [];
            vm.postCodes = [];
            vm.suburbs = [];
            vm.addressTypes = [];
            vm.deliveryTypes = [];
            vm.unitTypes = [];
            vm.streetNames = [];
            vm.streetTypes = [];
            vm.users = [];
            vm.InstallerId = $stateParams.installerId;


            vm.openNewTab = function (test) {
                window.open(abp.appPath + 'Temp/Downloads/' + test + '?v=' + new Date().valueOf(), '_blank');
            };

            function init() {
                installerService.getInstallerForDetail({
                    id: vm.InstallerId
                }).then(function (result) {
                    vm.installer = result.data;

                    var logoFilePath = abp.appPath + 'Temp/Downloads/' + result.data.logo + '?v=' + new Date().valueOf();
                    vm.logoFileName = result.data.logo;
                    //var $LogoResize = $('#Logophoto');
                    //var $LogoDownload = $('#DownloadPhoto');
                    //$LogoResize.attr('src', logoFilePath);
                    //$LogoDownload.attr('href', logoFilePath);

                    var accFilePath = abp.appPath + 'Temp/Downloads/' + result.data.accreditationAccPhoto + '?v=' + new Date().valueOf();
                    vm.accFileName = result.data.accreditationAccPhoto;
                    //var $accResize = $('#accphoto');
                    //$accResize.attr('src', accFilePath);

                    var licenceFilePath = abp.appPath + 'Temp/Downloads/' + result.data.licencePhoto + '?v=' + new Date().valueOf();
                    vm.licenceFileName = result.data.licencePhoto;
                    //var $licenceResize = $('#licencephoto');
                    //$licenceResize.attr('href', licenceFilePath);
                    //$licenceResize.attr('target', '_blank');

                    installerService.getUsers().then(function (result) {
                        vm.users = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#userSelectionCombobox').selectpicker('refresh');
                            $('#userSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerService.getPostalAddressType().then(function (result) {
                        vm.addressTypes = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#addressTypeSelectionCombobox').selectpicker('refresh');
                            $('#addressTypeSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerService.getPostalDeliveryType().then(function (result) {
                        vm.deliveryTypes = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#deliveryTypeSelectionCombobox').selectpicker('refresh');
                            $('#deliveryTypeSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    //installerService.getUnitType().then(function (result) {
                    //    vm.unitTypes = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#unitTypeSelectionCombobox').selectpicker('refresh');
                    //        $('#unitTypeSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getStreetName().then(function (result) {
                    //    vm.streetNames = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#streetNameSelectionCombobox').selectpicker('refresh');
                    //        $('#streetNameSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getStreetType().then(function (result) {
                    //    vm.streetTypes = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#streetTypeSelectionCombobox').selectpicker('refresh');
                    //        $('#streetTypeSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getState().then(function (result) {
                    //    vm.stateNames = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#stateSelectionCombobox').selectpicker('refresh');
                    //        $('#stateSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getPostCode().then(function (result) {
                    //    vm.postCodes = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#postCodeSelectionCombobox').selectpicker('refresh');
                    //        $('#postCodeSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getSuburb().then(function (result) {
                    //    vm.suburbs = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#suburbSelectionCombobox').selectpicker('refresh');
                    //        $('#suburbSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});
                });
            }
            init();

            vm.cancel = function () {
                $state.go('tenant.installer', {
                });
            };
        }
    ]);
})();