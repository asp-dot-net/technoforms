﻿(function () {
    appModule.controller('tenant.views.installer.editModel', [
        '$scope', '$state', '$http', '$stateParams', 'abp.services.app.installer', 'FileUploader', 'abp.services.app.jobs',
        function ($scope, $state, $http, $stateParams, installerService, fileUploader, jobsService) {
            var vm = this;

            vm.saving = false;
            vm.installer = {};
            vm.emailFormat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            vm.phonepattern = /^(07|03|08|04|13|18)[\d]{8}/;
            vm.stateNames = [];
            vm.postCodes = [];
            vm.suburbs = [];
            vm.addressTypes = [];
            vm.deliveryTypes = [];
            vm.unitTypes = [];
            vm.streetNames = [];
            vm.streetTypes = [];
            vm.users = [];
            vm.InstallerId = $stateParams.installerId;
            vm.suburb = "";
            vm.IsGoogleAddress = true;

            vm.openNewTab = function (test) {
                window.open(abp.appPath + 'Temp/Downloads/' + test + '?v=' + new Date().valueOf(), '_blank');
            };

            //DatePIcker Code Start
            $scope.today = function () {
                $scope.dt = new Date();
            };
            //$scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            //Disable past dates
            $scope.dateOptions = {
                //dateDisabled: disabled,
                formatYear: 'yy',
                //maxDate: new Date(2020, 5, 22),
                minDate: new Date(),
                startingDay: 1
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            //$scope.toggleMin = function () {
            //    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            //    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            //};

            //$scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.open3 = function () {
                $scope.popup3.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['d/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.altInputFormats = ['M!/d!/yyyy'];

            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            $scope.popup3 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }
            //DatePIcker Code End

            //vm.getPostCodes = function (streetCity) {
            //    if (streetCity != null) {
            //        installerService.getPostCodesListBySuburb({ id: streetCity }).then(function (result) {
            //            vm.installer.suburb = result.data.items[0].suburb;
            //            vm.postCodes = result.data.items;
            //            setTimeout(function () {
            //                $('#postCodeSelectionCombobox').selectpicker('refresh');
            //                $('#postCodeSelectionCombobox').selectpicker('refresh');
            //            }, 0);
            //            vm.stateNames = result.data.items;
            //            setTimeout(function () {
            //                $('#stateSelectionCombobox').selectpicker('refresh');
            //                $('#stateSelectionCombobox').selectpicker('refresh');
            //            }, 0);

            //            vm.installer.state = result.data.items[0].stateName;
            //            vm.installer.postCode = result.data.items[0].postcode;

            //            vm.loading = false;
            //        });
            //    }
            //};

            //vm.getStreetAddress = function () {
            //    var unittype = "";
            //    var unitNo = "";
            //    var streetNo = "";
            //    var streetName = "";
            //    var streetType = "";
            //    if (vm.installer.unitType != null) {
            //        unittype = vm.installer.unitType;
            //    }
            //    if (vm.installer.unitNo != null) {
            //        unitNo = vm.installer.unitNo;
            //    }
            //    if (vm.installer.streetNumber != null) {
            //        streetNo = vm.installer.streetNumber;
            //    }
            //    if (vm.installer.streetName != null) {
            //        streetName = vm.installer.streetName;
            //    }
            //    if (vm.installer.streetType != null) {
            //        streetType = vm.installer.streetType;
            //    }
            //    vm.installer.streetAddress = unittype + " " + unitNo + " " + streetNo + " " + streetName + " " + streetType;
            //};

            vm.checkInstallerCodeEdit = function () {
                if (vm.installer.installerExpiry != null) {
                    installerService.checkInstallerCodeEdit(vm.installer.installerExpiry, vm.installer.id).then(function (result) {
                        if (result.data != 0) {
                            vm.installer.installerExpiry = "";
                            abp.notify.info(app.localize('InstallerCodeAlreadyExist'));
                        }
                        vm.loading = false;
                    });
                }
            };
            vm.checkDesignerCodeEdit = function () {
                if (vm.installer.designerExpiry != null) {
                    installerService.checkDesignerCodeEdit(vm.installer.designerExpiry, vm.installer.id).then(function (result) {
                        if (result.data != 0) {
                            vm.installer.designerExpiry = "";
                            abp.notify.info(app.localize('DesignerCodeAlreadyExist'));
                        }
                        vm.loading = false;
                    });
                }
            };
            vm.checkElecCodeEdit = function () {
                if (vm.installer.electricalExpiry != null) {
                    installerService.checkElecCodeEdit(vm.installer.electricalExpiry, vm.installer.id).then(function (result) {
                        if (result.data != 0) {
                            vm.installer.electricalExpiry = "";
                            abp.notify.info(app.localize('EleLicenceCodeAlreadyExist'));
                        }
                        vm.loading = false;
                    });
                }
            };

            vm.logoDownload = function () {
                vm.saving = true;
                var Path = abp.appPath + 'Temp/Downloads/' + vm.installer.logo + '?v=' + new Date().valueOf();
                var uploadUrl = "../File/DownloadPhoto";
                var fd = new FormData();
                fd.append('FilePath', Path);
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).then(function () {
                    abp.notify.success(app.localize('SavedSuccessfully'));
                    $uibModalInstance.dismiss();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.logouploader = new fileUploader({
                url: abp.appPath + 'File/UploadLogo',
                headers: {
                    "X-XSRF-TOKEN": abp.security.antiForgery.getToken()
                },
                queueLimit: 1,
                autoUpload: true,
                removeAfterUpload: true,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        //File type check
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|jpg|jpeg|png|gif|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                            return false;
                        }

                        //File size check
                        if (item.size > 1048576) //1MB
                        {
                            abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
                            return false;
                        }

                        return true;
                    }
                }]
            });

            vm.logouploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    var $LogoResize = $('#Logophoto');

                    var profileFilePath = abp.appPath + 'Temp/Downloads/' + response.result.fileName + '?v=' + new Date().valueOf();
                    vm.logoFileName = response.result.fileName;

                    $LogoResize.attr('src', profileFilePath);

                } else {
                    abp.message.error(response.error.message);
                }
            };

            vm.accuploader = new fileUploader({
                url: abp.appPath + 'File/UploadAcc',
                headers: {
                    "X-XSRF-TOKEN": abp.security.antiForgery.getToken()
                },
                queueLimit: 1,
                autoUpload: true,
                removeAfterUpload: true,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        //File type check
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|jpg|jpeg|png|gif|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                            return false;
                        }

                        //File size check
                        if (item.size > 1048576) //1MB
                        {
                            abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
                            return false;
                        }

                        return true;
                    }
                }]
            });

            vm.accuploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    var $accResize = $('#accphoto');

                    var profileFilePath = abp.appPath + 'Temp/Downloads/' + response.result.fileName + '?v=' + new Date().valueOf();
                    vm.accFileName = response.result.fileName;

                    $accResize.attr('src', profileFilePath);

                } else {
                    abp.message.error(response.error.message);
                }
            };

            vm.licuploader = new fileUploader({
                url: abp.appPath + 'File/UploadLicence',
                headers: {
                    "X-XSRF-TOKEN": abp.security.antiForgery.getToken()
                },
                queueLimit: 1,
                autoUpload: true,
                removeAfterUpload: true,
                filters: [{
                    name: 'imageFilter',
                    fn: function (item, options) {
                        //File type check
                        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                        if ('|jpg|jpeg|png|gif|'.indexOf(type) === -1) {
                            abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
                            return false;
                        }

                        //File size check
                        if (item.size > 1048576) //1MB
                        {
                            abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
                            return false;
                        }

                        return true;
                    }
                }]
            });

            vm.licuploader.onSuccessItem = function (fileItem, response, status, headers) {
                if (response.success) {
                    var $licenceResize = $('#licencephoto');

                    var profileFilePath = abp.appPath + 'Temp/Downloads/' + response.result.fileName + '?v=' + new Date().valueOf();
                    vm.licenceFileName = response.result.fileName;

                    $licenceResize.attr('src', profileFilePath);

                } else {
                    abp.message.error(response.error.message);
                }
            };

            vm.CheckNumber = function () {
                if (isNaN(event.key) || event.key === ' ' || event.key === '') {
                    event.returnValue = '';
                }
            };

            vm.save = function () {
                vm.saving = true;
                vm.installer.streetNumber = $("#street_number").val();
                vm.installer.streetName = $("#routeName").val();
                vm.installer.streetType = $("#routeType").val();
                vm.installer.suburb = $("#locality").val();
                var value = vm.installer.suburb.split("|");
                vm.installer.suburb = value[0].trim();
                vm.installer.state = $("#administrative_area_level_1").val();
                vm.installer.postCode = $("#postal_code").val();
                
                var unittype = "";
                var unitNo = "";
                var streetNo = "";
                var streetName = "";
                var streetType = "";
                if (vm.installer.unitType != null) {
                    unittype = vm.installer.unitType;
                }
                if (vm.installer.unitNo != null) {
                    unitNo = vm.installer.unitNo;
                }
                if (vm.installer.streetNumber != null) {
                    streetNo = vm.installer.streetNumber;
                }
                if (vm.installer.streetName != null) {
                    streetName = vm.installer.streetName;
                }
                if (vm.installer.streetType != null) {
                    streetType = vm.installer.streetName;
                }
                vm.installer.streetAddress = unittype + " " + unitNo + " " + streetNo + " " + streetName + " " + streetType;

                if (vm.logoFileName != null) {
                    vm.installer.logo = vm.logoFileName;
                }
                if (vm.accFileName != null) {
                    vm.installer.accreditationAccPhoto = vm.accFileName;
                }
                if (vm.licenceFileName != null) {
                    vm.installer.licencePhoto = vm.licenceFileName;
                }
                
                installerService.updateInstaller(vm.installer).then(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    abp.notify.info(app.localize('InstallerEditNameMesssage'));
                    $state.go('tenant.installer', {
                    });
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                installerService.getInstallerForEdit({
                    id: vm.InstallerId
                }).then(function (result) {
                    vm.installer = result.data;
                    vm.suburb = result.data.suburb;
                    vm.installer.insAccreExDate = new Date(vm.installer.insAccreExDate);
                    vm.installer.eleLicenceExDate = new Date(vm.installer.eleLicenceExDate);
                    vm.installer.designerAccreExDate = new Date(vm.installer.designerAccreExDate);

                    vm.getPostCodes(vm.installer.suburb_Id);

                    var logoFilePath = abp.appPath + 'Temp/Downloads/' + result.data.logo + '?v=' + new Date().valueOf();
                    vm.logoFileName = result.data.logo;
                    //var $LogoResize = $('#Logophoto');
                    //var $LogoDownload = $('#DownloadPhoto');
                    //$LogoResize.attr('src', logoFilePath);
                    //$LogoDownload.attr('href', logoFilePath);

                    var accFilePath = abp.appPath + 'Temp/Downloads/' + result.data.accreditationAccPhoto + '?v=' + new Date().valueOf();
                    vm.accFileName = result.data.accreditationAccPhoto;
                    //var $accResize = $('#accphoto');
                    //$accResize.attr('src', accFilePath);

                    var licenceFilePath = abp.appPath + 'Temp/Downloads/' + result.data.licencePhoto + '?v=' + new Date().valueOf();
                    vm.licenceFileName = result.data.licencePhoto;
                    //var $licenceResize = $('#licencephoto');
                    //$licenceResize.attr('src', licenceFilePath);

                    installerService.getUsers().then(function (result) {
                        vm.users = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#userSelectionCombobox').selectpicker('refresh');
                            $('#userSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerService.getPostalAddressType().then(function (result) {
                        vm.addressTypes = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#addressTypeSelectionCombobox').selectpicker('refresh');
                            $('#addressTypeSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerService.getPostalDeliveryType().then(function (result) {
                        vm.deliveryTypes = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#deliveryTypeSelectionCombobox').selectpicker('refresh');
                            $('#deliveryTypeSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    //installerService.getUnitType().then(function (result) {
                    //    vm.unitTypes = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#unitTypeSelectionCombobox').selectpicker('refresh');
                    //        $('#unitTypeSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getStreetName().then(function (result) {
                    //    vm.streetNames = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#streetNameSelectionCombobox').selectpicker('refresh');
                    //        $('#streetNameSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getStreetType().then(function (result) {
                    //    vm.streetTypes = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#streetTypeSelectionCombobox').selectpicker('refresh');
                    //        $('#streetTypeSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getState().then(function (result) {
                    //    vm.stateNames = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#stateSelectionCombobox').selectpicker('refresh');
                    //        $('#stateSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getPostCode().then(function (result) {
                    //    vm.postCodes = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#postCodeSelectionCombobox').selectpicker('refresh');
                    //        $('#postCodeSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getSuburb().then(function (result) {
                    //    debugger;
                    //    vm.suburbs = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#suburbSelectionCombobox').selectpicker('refresh');
                    //        $('#suburbSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                });

            }
            init();

            vm.cancel = function () {
                $state.go('tenant.installer', {
                });
            };

            vm.getUnitTypeList = function () {
                installerService.getUnitType({
                    text: vm.installer.unitType
                }).then(function (result) {
                    $(".UnitTypetypeahead").typeahead({ source: result.data });
                });
            };
            vm.getStreetNameList = function () {
                installerService.getStreetNames({
                    text: vm.installer.streetName
                }).then(function (result) {
                    $(".StreetNametypeahead").typeahead({ source: result.data });
                });
            };
            vm.getStreetTypeList = function () {
                installerService.getStreetType({
                    text: vm.installer.streetType
                }).then(function (result) {
                    $(".StreetTypetypeahead").typeahead({ source: result.data });
                });
            };
            vm.getSuburbList = function () {
                installerService.getAllPostCodes({
                    text: vm.suburb
                }).then(function (result) {
                    $(".Fnametypeahead").typeahead({ source: result.data, onSelect: displayStatePostcode });
                });
            };
            function displayStatePostcode(item) {
                var value = item.text.split("|");
                vm.installer.suburb = value[0].trim();
                vm.installer.Suburb_Id = item.value;
                vm.installer.state = value[1].trim();
                vm.installer.postCode = value[2].trim();
            }
            vm.google = function () {
                vm.IsGoogleAddress = true;
            };
            vm.manual = function () {
                vm.IsGoogleAddress = false;
            };
            vm.getStreetNameList();
            vm.getSuburbList();
            vm.getUnitTypeList();
            vm.getStreetTypeList();
        }
    ]);
})();