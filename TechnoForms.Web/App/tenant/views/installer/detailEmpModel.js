﻿(function () {
    appModule.controller('tenant.views.installer.detailEmpModel', [
        '$scope', '$state', '$stateParams', 'abp.services.app.installer',
        function ($scope, $state, $stateParams, installerService) {
            var vm = this;

            vm.saving = false;
            vm.installerEmp = {};
            vm.stateNames = [];
            vm.postCodes = [];
            vm.suburbs = [];
            vm.addressTypes = [];
            vm.deliveryTypes = [];
            vm.unitTypes = [];
            vm.streetNames = [];
            vm.streetTypes = [];

            vm.empId = $stateParams.empId;
            vm.InstallerId = $stateParams.InstallerId;

            function init() {
                installerService.getInstallerEmployeeForEdit({
                    id: vm.empId
                }).then(function (result) {
                    vm.installerEmp = result.data;

                    installerService.getPostalAddressType().then(function (result) {
                        vm.addressTypes = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#addressTypeSelectionCombobox').selectpicker('refresh');
                            $('#addressTypeSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    installerService.getPostalDeliveryType().then(function (result) {
                        vm.deliveryTypes = result.data.items;
                        vm.flags = result.data.flags;
                        setTimeout(function () {
                            $('#deliveryTypeSelectionCombobox').selectpicker('refresh');
                            $('#deliveryTypeSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });

                    //installerService.getUnitType().then(function (result) {
                    //    vm.unitTypes = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#unitTypeSelectionCombobox').selectpicker('refresh');
                    //        $('#unitTypeSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getStreetName().then(function (result) {
                    //    vm.streetNames = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#streetNameSelectionCombobox').selectpicker('refresh');
                    //        $('#streetNameSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getStreetType().then(function (result) {
                    //    vm.streetTypes = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#streetTypeSelectionCombobox').selectpicker('refresh');
                    //        $('#streetTypeSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getState().then(function (result) {
                    //    vm.stateNames = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#stateSelectionCombobox').selectpicker('refresh');
                    //        $('#stateSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getPostCode().then(function (result) {
                    //    vm.postCodes = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#postCodeSelectionCombobox').selectpicker('refresh');
                    //        $('#postCodeSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                    //installerService.getSuburb().then(function (result) {
                    //    vm.suburbs = result.data.items;
                    //    vm.flags = result.data.flags;
                    //    setTimeout(function () {
                    //        $('#suburbSelectionCombobox').selectpicker('refresh');
                    //        $('#suburbSelectionCombobox').selectpicker('refresh');
                    //    }, 0);
                    //    vm.loading = false;
                    //});

                });
            }
            init();

            vm.cancel = function () {
                $state.go('installerEmployee', {
                    InstallerId: vm.InstallerId
                });
            };
        }
    ]);
})();