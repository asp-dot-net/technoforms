﻿(function () {
    appModule.controller('tenant.views.installer.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.installer', 'abp.services.app.state',
        function ($scope, $state, $uibModal, uiGridConstants, installerService, stateService) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;

            vm.permissions = {
                createInstaller: abp.auth.hasPermission('Pages.Tenant.Installer.Create'),
                detailInstaller: abp.auth.hasPermission('Pages.Tenant.Installer.Detail'),
                editInstaller: abp.auth.hasPermission('Pages.Tenant.Installer.Edit'),
                deleteInstaller: abp.auth.hasPermission('Pages.Tenant.Installer.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.installerGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        enableColumnMenu: false,
                        cellClass:'centeralign', 
                        headerCellClass:'centeralign',
                        width: 80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-click="grid.appScope.getEmpList(row.entity)">' + app.localize('Emp') + '</a></li>' +
                            '      <li><a ng-click="grid.appScope.detailInstaller(row.entity)">' + app.localize('Detail') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.editInstaller" ng-click="grid.appScope.editInstaller(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.deleteInstaller && row.entity.isApproved == 0" ng-click="grid.appScope.deleteInstaller(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('FullName'),
                        field: 'fullName',
                        enableColumnMenu: false,
                    },
                    {
                        name: app.localize('Documents'),
                        enableSorting: false,
                        enableColumnMenu: false,
                        cellClass:'centeralign', 
                        headerCellClass:'centeralign',
                       
                        width: 110,                      
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.isApproved == 0" class="label label-default" >' + app.localize('Pending') + '</span>' +
                            '  <span ng-show="row.entity.isApproved == 1" class="label label-success" >' + app.localize('Approved') + '</span>' +
                            '  <span ng-show="row.entity.isApproved == 2" tooltip-placement="left" uib-tooltip="{{row.entity.rejectReason}}" class="label label-danger" >' + app.localize('Rejected') + '</span>' +
                            '</div>',
                         
                            
                    },
                    {
                        name: app.localize('Admin'),
                        enableSorting: false,
                        enableColumnMenu: false,
                        width: 110,
                        cellClass:'centeralign', 
                        headerCellClass:'centeralign',
                       
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.request_status == 0" class="label label-default">' + app.localize('Pending') + '</span>' +
                            '  <span ng-show="row.entity.request_status == 1" class="label label-success">' + app.localize('Approved') + '</span>' +
                            '  <span ng-show="row.entity.request_status == 2" tooltip-placement="left" uib-tooltip="{{row.entity.admin_RejectReason}}" class="label label-danger">' + app.localize('Rejected') + '</span>' +
                            '</div>',
                         
                    },
                    {
                        name: app.localize('Installer'),
                        enableSorting: false,
                        enableColumnMenu: false,
                        width: 110,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.inst_Request_status == 0" class="label label-default">' + app.localize('Pending') + '</span>' +
                            '  <span ng-show="row.entity.inst_Request_status == 1" class="label label-success">' + app.localize('Approved') + '</span>' +
                            '  <span ng-show="row.entity.inst_Request_status == 2" tooltip-placement="left" uib-tooltip="{{row.entity.inst_RejectReason}}" class="label label-danger">' + app.localize('Rejected') + '</span>' +
                            '</div>',
                            cellClass:'centeralign', 
                            headerCellClass:'centeralign',
                    },
                    {
                        name: app.localize('InstallerExpiryCEC'),
                        field: 'installerExpiry',
                        enableColumnMenu: false,
                        cellClass:'centeralign', 
                        headerCellClass:'centeralign',
                       width:180
                    },
                    {
                        name: app.localize('ExpiryDate'),
                        field: 'insAccreExDate',
                        enableColumnMenu: false,
                        width: 120,
                        cellClass: 'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    },
                    
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        enableColumnMenu: false,
                        width: 150,
                        cellClass: 'cellwdth110 centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    },
                  
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getInstaller();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getInstaller();
                    });
                },
                data: []
            };

            if (!vm.permissions.editInstaller && !vm.permissions.deleteInstaller && !vm.permissions.detailInstaller && !vm.permissions.editPerson) {
                vm.installerGridOptions.columnDefs.shift();
            }

            vm.getInstaller = function () {
                vm.loading = true;
                installerService.getInstaller({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    fullName: vm.fullName,
                    state: vm.filterstate
                }).then(function (result) {
                    vm.installerGridOptions.totalItems = result.data.totalCount;
                    vm.installerGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.ClearAll = function () {
                vm.fullName = null;
                vm.filterstate = null;
                setTimeout(function () {
                    $('#stateSelectionCombobox').selectpicker('refresh');
                }, 0);

                vm.getInstaller();
            };

            function init() {
                stateService.getState().then(function (result) {
                    vm.stateNames = result.data.items;
                    vm.flags = result.data.flags;
                    setTimeout(function () {
                        $('#stateSelectionCombobox').selectpicker('refresh');
                        $('#stateSelectionCombobox').selectpicker('refresh');
                    }, 0);
                    vm.loading = false;
                });
            }
            init();

            vm.createInstaller = function () {
                $state.go('createInstaller', {
                });
                //var modalInstance = $uibModal.open({
                //    templateUrl: '/App/tenant/views/installer/createModel.cshtml',
                //    controller: 'tenant.views.installer.createModel as vm',
                //    backdrop: 'static'
                //});
                //modalInstance.result.then(function () {
                //    vm.getInstaller();
                //});
            };

            vm.createExistingInstaller = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/tenant/views/installer/createExistModel.cshtml',
                    controller: 'tenant.views.installer.createExistModel as vm',
                    backdrop: 'static'
                });
                modalInstance.result.then(function () {
                    vm.getInstaller();
                });
            };

            vm.deleteInstaller = function (inst) {
                abp.message.confirm(
                    app.localize('InstallerDeleteWarningMessage', inst.fullName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            installerService.deleteInstaller({
                                id: inst.id
                            }).then(function () {
                                vm.getInstaller();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.editInstaller = function (installer) {
                $state.go('editInstaller', {
                    installerId: installer.id
                });
                //var modalInstance = $uibModal.open({
                //    templateUrl: '/App/tenant/views/installer/editModel.cshtml',
                //    controller: 'tenant.views.installer.editModel as vm',
                //    backdrop: 'static',
                //    resolve: {
                //        installerId: function () {
                //            return installer.id;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getInstaller();
                //});
            };

            vm.detailInstaller = function (installer) {
                $state.go('detailInstaller', {
                    installerId: installer.id
                });
                //var modalInstance = $uibModal.open({
                //    templateUrl: '/App/tenant/views/installer/detailModel.cshtml',
                //    controller: 'tenant.views.installer.detailModel as vm',
                //    backdrop: 'static',
                //    resolve: {
                //        installerId: function () {
                //            return installer.id;
                //        }
                //    }
                //});

                //modalInstance.result.then(function (result) {
                //    vm.getInstaller();
                //});
            };

            vm.createInstallerEmp = function (installer) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/tenant/views/installer/createEmpModel.cshtml',
                    controller: 'tenant.views.installer.createEmpModel as vm',
                    backdrop: 'static',
                    resolve: {
                        installerId: function () {
                            return installer.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getInstaller();
                });
            };

            vm.getEmpList = function (InstallerEmployee) {
                $state.go('installerEmployee', {
                    InstallerId: InstallerEmployee.id
                });
            };

            vm.getInstaller();
        }
    ]);
})();