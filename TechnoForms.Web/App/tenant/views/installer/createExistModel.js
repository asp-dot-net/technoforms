﻿(function () {
    appModule.controller('tenant.views.installer.createExistModel', [
        '$scope', '$uibModalInstance', 'abp.services.app.installer',
        function ($scope, $uibModalInstance, installerService ) {
            var vm = this;

            vm.saving = false;
            vm.installer = {};

            vm.save = function () {
                vm.saving = true;
                installerService.createExistInstaller(vm.installer).then(function () {
                    abp.notify.info(app.localize('InstallerRequestSentSuccessfully'));
                    $uibModalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            //modelChanged(newObj) {
            //    alert();
            //    // do something with new value
            //};

            function init() {
                
            }
            init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();