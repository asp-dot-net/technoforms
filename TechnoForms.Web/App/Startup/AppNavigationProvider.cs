﻿using Abp.Application.Navigation;
using Abp.Localization;
using TechnoForms.Authorization;
using TechnoForms.Web.Navigation;

namespace TechnoForms.Web.App.Startup
{
    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class AppNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Dashboard,
                    L("Dashboard"),
                    url: "host.dashboard",
                    icon: "flaticon-line-graph",
                    requiredPermissionName: AppPermissions.Pages_Host_Dashboard
                    )
                )
                .AddItem(new MenuItemDefinition(
                    PageNames.App.Tenant.Dashboard,
                    L("Dashboard"),
                    url: "tenant.dashboard",
                     icon: "flaticon-line-graph",
                    requiredPermissionName: AppPermissions.Pages_Tenant_Dashboard
                    )
                )
				//.AddItem(new MenuItemDefinition(
				//	PageNames.App.Host.Test,
				//	L("Test"),
				//	url: "host.test",
				//	icon: "dashboardicon",
				//	requiredPermissionName: AppPermissions.Pages_Test
				//	)
				//)
				.AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Master,
                    L("DataVaults"),
                    icon: "flaticon-layers"
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Host.UnitType,
                        L("UnitType"),
                        url: "host.unittype",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Masters_UnitType
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Streettype,
                        L("StreetType"),
                        url: "host.streettype",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Masters_StreetType
                        )
                    ).AddItem(new MenuItemDefinition(
                         PageNames.App.Host.StreetName,
                         L("StreetName"),
                         url: "host.streetname",
                         icon: "flaticon-more",
                         requiredPermissionName: AppPermissions.Pages_Masters_StreetName
                        )
                     ).AddItem(new MenuItemDefinition(
                         PageNames.App.Host.PostCodes,
                         L("PostCodes"),
                         url: "host.postcodes",
                         icon: "flaticon-more",
                         requiredPermissionName: AppPermissions.Pages_Masters_PostCodes
                         )
                     ).AddItem(new MenuItemDefinition(
                         PageNames.App.Host.Postaltype,
                         L("PostalType"),
                         url: "host.postaltype",
                         icon: "flaticon-more",
                         requiredPermissionName: AppPermissions.Pages_Masters_PostalType
                         )
                     ).AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Inverter,
                        L("Inverter"),
                        url: "host.inverter",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Masters_Inverter
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Panel,
                        L("Panel"),
                        url: "host.panel",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Masters_Panel
                        )
                    )
                )
                .AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Tenants,
                    L("Tenants"),
                    url: "host.tenants",
                    icon: "flaticon-users",
                    requiredPermissionName: AppPermissions.Pages_Tenants
                    )
                )
                .AddItem(new MenuItemDefinition(
                    PageNames.App.Host.Master,
                    L("Installer"),
                    icon: "flaticon-user"
                    ).AddItem(new MenuItemDefinition(
                       PageNames.App.Host.NewInstallerInvitation,
                        L("Verify"),
                        url: "host.newInvitation",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_New_Invitation
                        )
                    ).AddItem(new MenuItemDefinition(
                       PageNames.App.Host.AllInstaller,
                        L("AllInstallers"),
                        url: "host.allInstallers",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_AllInstallers
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                       PageNames.App.Host.AdminInvitation,
                        L("Request"),
                        url: "host.adminInvitation",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_adminInvitation
                        )
                    ).AddItem(new MenuItemDefinition(
                       PageNames.App.Host.AllInstallerRequest,
                        L("AllInstallerRequests"),
                        url: "host.allInstallerRequests",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_AllInstallerRequests
                        )
                    )
                )
                .AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Invoice,
                        L("Invoice"),
                        icon: "flaticon-list-1"
                        ).AddItem(
                            new MenuItemDefinition(
                            PageNames.App.Host.Invoice_Generate,
                            L("InvoiceGenerate"),
                            url: "host.pendinginvoice",
                            icon: "flaticon-more",
                            requiredPermissionName: AppPermissions.Pages_Invoice_Generate
                            )
                        ).AddItem(
                            new MenuItemDefinition(
                            PageNames.App.Host.Invoice_Generated,
                            L("InvoiceGenerated"),
                            url: "host.invoicegenerated",
                            icon: "flaticon-more",
                            requiredPermissionName: AppPermissions.Pages_Invoice_Generated
                            )
                        ).AddItem(
                             new MenuItemDefinition(
                             PageNames.App.Host.Invoice_Collection,
                             L("Collection"),
                             url: "host.collection",
                             icon: "flaticon-more",
                             requiredPermissionName: AppPermissions.Pages_Invoice_Collection
                             )
                         )
                    )
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Video,
                        L("Video"),
                        url: "host.video",
                        icon: "flaticon-imac",
                        requiredPermissionName: AppPermissions.Pages_Video
                        )
                     ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Installer,
                        L("Installer"),
                        url: "tenant.installer",
                        icon: "flaticon-user",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Installer
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.EnergyDestributars,
                        L("EnergyDestributars"),
                        url: "tenant.energyDestributars",
                        icon: "flaticon-user",
                        requiredPermissionName: AppPermissions.Pages_Tenant_EnergyDestributars
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.EnergyRetailers,
                        L("EnergyRetailers"),
                        url: "tenant.energyRetailers",
                        icon: "flaticon-user",
                        requiredPermissionName: AppPermissions.Pages_Tenant_EnergyRetailers
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Jobs,
                        L("Jobs"),
                        url: "tenant.jobs",
                        icon: "flaticon-suitcase",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Jobs
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.CompliaenceJobs,
                        L("CompliaenceJobs"),
                        url: "tenant.CompliaenceJobs",
                        icon: "flaticon-suitcase",
                        requiredPermissionName: AppPermissions.Pages_Tenant_CompliaenceJobs
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Host.STC,
                        L("STCPage"),
                        url: "host.stc",
                        icon: "flaticon-user-ok",
                        requiredPermissionName: AppPermissions.Pages_STC
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Calendar,
                        L("Calendar"),
                        url: "tenant.calendar",
                        icon: "flaticon-calendar-3",
                        requiredPermissionName: AppPermissions.Pages_Calendar
                        )
                    )

                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Administration,
                        L("Administration"),
                        icon: "flaticon-profile-1"
                        ).AddItem(new MenuItemDefinition(
                            PageNames.App.Common.Roles,
                            L("Roles"),
                            url: "roles",
                            icon: "flaticon-more",
                            requiredPermissionName: AppPermissions.Pages_Administration_Roles
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Common.Users,
                        L("Users"),
                        url: "users",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_Users
                        )
                    )
                    //.AddItem(new MenuItemDefinition(
                    //    PageNames.App.Common.RECUser,
                    //    L("RECUser"),
                    //    url: "recUser",
                    //    icon: "iconusers",
                    //    requiredPermissionName: AppPermissions.Pages_Administration_RECUser
                    //    )
                    //)
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Common.AuditLogs,
                        L("AuditLogs"),
                        url: "auditLogs",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_AuditLogs
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Maintenance,
                        L("Maintenance"),
                        url: "host.maintenance",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_Host_Maintenance
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                        PageNames.App.Host.Settings,
                        L("Settings"),
                        url: "host.settings",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_Host_Settings
                        )
                    ).AddItem(new MenuItemDefinition(
                        PageNames.App.Tenant.Settings,
                        L("Settings"),
                        url: "tenant.settings",
                        icon: "flaticon-more",
                        requiredPermissionName: AppPermissions.Pages_Administration_Tenant_Settings
                        )
                    )
                );
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, TechnoFormsConsts.LocalizationSourceName);
        }
    }
}
