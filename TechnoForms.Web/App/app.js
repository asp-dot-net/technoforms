﻿/* 'app' MODULE DEFINITION */
var appModule = angular.module("app", [
    "ui.router",
    "ui.bootstrap",
    'ui.utils',
    "ui.jq",
    'ui.grid',
    'ui.grid.expandable',
    'ui.grid.pagination',
    'ui.grid.autoResize',
    "oc.lazyLoad",
    "ngSanitize",
    'angularFileUpload',
    'daterangepicker',
    'angularMoment',
    'frapontillo.bootstrap-switch',
    'abp',
    'ui.bootstrap',
    'ui.calendar',
    'angucomplete-alt',
    'vsGoogleAutocomplete'
]);

/* LAZY LOAD CONFIG */

/* This application does not define any lazy-load yet but you can use $ocLazyLoad to define and lazy-load js/css files.
 * This code configures $ocLazyLoad plug-in for this application.
 * See it's documents for more information: https://github.com/ocombe/ocLazyLoad
 */
appModule.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        cssFilesInsertBefore: 'ng_load_plugins_before', // load the css files before a LINK element with this ID.
        debug: false,
        events: true,
        modules: []
    });
}]);

/* THEME SETTINGS */
App.setAssetsPath(abp.appPath + 'metronic/assets/');
appModule.factory('settings', ['$rootScope', function ($rootScope) {
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        layoutImgPath: App.getAssetsPath() + 'admin/layout4/img/',
        layoutCssPath: App.getAssetsPath() + 'admin/layout4/css/',
        assetsPath: abp.appPath + 'metronic/assets',
        globalPath: abp.appPath + 'metronic/assets/global',
        layoutPath: abp.appPath + 'metronic/assets/layouts/layout4'
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* ROUTE DEFINITIONS */

appModule.config([
    '$stateProvider', '$urlRouterProvider', '$qProvider',
    function ($stateProvider, $urlRouterProvider, $qProvider) {

        // Default route (overrided below if user has permission)
        $urlRouterProvider.otherwise("/welcome");

        //Welcome page
        $stateProvider.state('welcome', {
            url: '/welcome',
            templateUrl: '~/App/common/views/welcome/index.cshtml'
        });

        //COMMON routes

        if (abp.auth.hasPermission('Pages.Administration.Roles')) {
            $stateProvider.state('roles', {
                url: '/roles',
                templateUrl: '~/App/common/views/roles/index.cshtml'
            });

            $stateProvider.state('CreateOrEditRole', {
                url: '/roles/createOrEditModal/:Id',
                templateUrl: '~/App/common/views/roles/createOrEditModal.cshtml'
            });
        }

        //if (abp.auth.hasPermission('Pages.Test')) {
        //    $stateProvider.state('host.test', {
        //        url: '/test',
        //        templateUrl: '~/App/host/views/test/index.cshtml'
        //    });
        //}

        if (abp.auth.hasPermission('Pages.Administration.Users')) {
            $stateProvider.state('users', {
                url: '/users?filterText',
                templateUrl: '~/App/common/views/users/index.cshtml'
            });
            $stateProvider.state('CreateOrEditUser', {
                url: '/users/createOrEditModal/:Id',
                templateUrl: '~/App/common/views/users/createOrEditModal.cshtml'
            });
            $stateProvider.state('editPermissions', {
                url: '/users/permissionsModal/:Id',
                templateUrl: '~/App/common/views/users/permissionsModal.cshtml'
            });
        }

        //if (abp.auth.hasPermission('Pages.Administration.RECUser')) {
        //    $stateProvider.state('recUser', {
        //        url: '/recUser?filterText',
        //        templateUrl: '~/App/common/views/recUser/index.cshtml'
        //    });
        //    $stateProvider.state('createrecUser', {
        //        url: '/recUser/createModel',
        //        templateUrl: '~/App/common/views/recUser/createModal.cshtml'
        //    });
        //    $stateProvider.state('editrecUser', {
        //        url: '/recUser/permissionsModal/:Id',
        //        templateUrl: '~/App/common/views/recUser/editModal.cshtml'
        //    });
        //}

        if (abp.auth.hasPermission('Pages.Administration.AuditLogs')) {
            $stateProvider.state('auditLogs', {
                url: '/auditLogs',
                templateUrl: '~/App/common/views/auditLogs/index.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.OrganizationUnits')) {
            $stateProvider.state('organizationUnits', {
                url: '/organizationUnits',
                templateUrl: '~/App/common/views/organizationUnits/index.cshtml'
            });
        }

        $stateProvider.state('notifications', {
            url: '/notifications',
            templateUrl: '~/App/common/views/notifications/index.cshtml'
        });

        //HOST routes

        $stateProvider.state('host', {
            'abstract': true,
            url: '/host',
            template: '<div ui-view class="fade-in-up"></div>'
        });

        if (abp.auth.hasPermission('Pages.Tenant.Jobs')) {
            $stateProvider.state('tenant.jobs', {
                url: '/tenants?filterText',
                templateUrl: '~/App/tenant/views/jobs/index.cshtml'
            });

            $stateProvider.state('createJob', {
                url: '/jobs/createModel',
                templateUrl: '~/App/tenant/views/jobs/createModal.cshtml'
            });
            $stateProvider.state('editJob', {
                url: '/jobs/editModal/:jobId',
                templateUrl: '~/App/tenant/views/jobs/editModal.cshtml'
            });
            $stateProvider.state('detailJob', {
                url: '/jobs/detailModal/:jobId',
                templateUrl: '~/App/tenant/views/jobs/detailModal.cshtml'
            });

            $stateProvider.state('uploadDocument', {
                url: '/jobs/uploadDocument/:jobId',
                templateUrl: '~/App/tenant/views/jobs/uploadDocument.cshtml'
            });

            $stateProvider.state('createDocs', {
                url: '/jobs/uploadDocument/:jobId',
                templateUrl: '~/App/tenant/views/jobs/createDocument.cshtml'
            });

            $stateProvider.state('Message', {
                url: '/jobs/messageModal/:jobId',
                templateUrl: '~/App/tenant/views/jobs/messageModal.cshtml'
            });

            $stateProvider.state('formList', {
                url: '/jobs/formList/:jobId',
                templateUrl: '~/App/tenant/views/jobs/formList.cshtml'
            });

            $stateProvider.state('InverterEnergySystem', {
                url: '/jobs/Forms/InverterEnergySystem:jobId',
                templateUrl: '~/App/tenant/views/jobs/Forms/InverterEnergySystem.cshtml'
            });

            $stateProvider.state('SignatureLog', {
                url: '/jobs/SignatureLog/:jobId',
                templateUrl: '~/App/tenant/views/jobs/SignatureLog.cshtml'
            });

            $stateProvider.state('signature', {
                url: '/signature/:guid',
                templateUrl: '~/Signature/customerSignature.cshtml'
            });

        }

        if (abp.auth.hasPermission('Pages.Tenant.CompliaenceJobs')) {
            $stateProvider.state('tenant.CompliaenceJobs', {
                url: '/compliaencejobs',
                templateUrl: '~/App/tenant/views/compliaencejobs/index.cshtml'
            });
            $stateProvider.state('detailcompliaenceJob', {
                url: '/compliaencejobs/detailModal/:jobId',
                templateUrl: '~/App/tenant/views/compliaencejobs/detailModal.cshtml'
            });
            $stateProvider.state('editJobcompliaenceJob', {
                url: '/compliaencejobs/editModal/:jobId',
                templateUrl: '~/App/tenant/views/compliaencejobs/editModal.cshtml'
            });
            $stateProvider.state('compliaenceAudio', {
                url: '/compliaencejobs/audioList/:jobId',
                templateUrl: '~/App/tenant/views/compliaencejobs/audioList.cshtml'
            });
            $stateProvider.state('createAudio', {
                url: '/compliaencejobs/createAudio/:jobId',
                templateUrl: '~/App/tenant/views/compliaencejobs/createAudio.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenants')) {
            $stateProvider.state('host.tenants', {
                url: '/tenants?filterText',
                templateUrl: '~/App/host/views/tenants/index.cshtml'
            });

            $stateProvider.state('createTenant', {
                url: '/tenants/createModel',
                templateUrl: '~/App/host/views/tenants/createModal.cshtml'
            });

            $stateProvider.state('editTenant', {
                url: '/tenants/editModel/:tenantId',
                templateUrl: '~/App/host/views/tenants/editModal.cshtml'
            });

            $stateProvider.state('featuresTenant', {
                url: '/tenants/featuresModal/:tenantId',
                templateUrl: '~/App/host/views/tenants/featuresModal.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Invoice.Generate')) {
            $stateProvider.state('host.pendinginvoice', {
                url: '/pendinginvoice',
                templateUrl: '~/App/host/views/pendinginvoice/index.cshtml'
            });

            $stateProvider.state('InvoiceGenerateDetail', {
                url: '/pendinginvoice/InvoiceGenerateDetail/:TenantId',
                templateUrl: '~/App/host/views/pendinginvoice/InvoiceGenerateDetail.cshtml'
            });
        }
        if (abp.auth.hasPermission('Pages.Invoice.Generated')) {
            $stateProvider.state('host.invoicegenerated', {
                url: '/invoicegenerated',
                templateUrl: '~/App/host/views/invoicegenerated/index.cshtml'
            });

            $stateProvider.state('InvoiceGenerated', {
                url: '/invoicegenerated',
                templateUrl: '~/App/host/views/invoicegenerated/index.cshtml'
            });

            $stateProvider.state('PaymentPage', {
                url: '/invoicegenerated/PaymentPage/:InvoiceId/:TenantId',
                templateUrl: '~/App/host/views/invoicegenerated/PaymentPage.cshtml'
            });
        }
        if (abp.auth.hasPermission('Pages.Invoice.Collection')) {
            $stateProvider.state('host.collection', {
                url: '/collection',
                templateUrl: '~/App/host/views/collection/index.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Masters.StreetType')) {
            $stateProvider.state('host.streettype', {
                url: '/streettype',
                templateUrl: '~/App/host/views/streettype/index.cshtml'
            });

            $stateProvider.state('createStreetType', {
                url: '/streettype/createModel',
                templateUrl: '~/App/host/views/streettype/createModal.cshtml'
            });

            $stateProvider.state('editstreettype', {
                url: '/streettype/editModel/:Id',
                templateUrl: '~/App/host/views/streettype/editModal.cshtml'
            });

        }
        if (abp.auth.hasPermission('Pages.Masters.UnitType')) {
            $stateProvider.state('host.unittype', {
                url: '/unittype',
                templateUrl: '~/App/host/views/unittype/index.cshtml'
            });

            $stateProvider.state('createUnitType', {
                url: '/unittype/createModel',
                templateUrl: '~/App/host/views/unittype/createModel.cshtml'
            });

            $stateProvider.state('editUnitType', {
                url: '/unittype/editModel/:Id',
                templateUrl: '~/App/host/views/unittype/editModel.cshtml'
            });

        }
        if (abp.auth.hasPermission('Pages.Masters.StreetName')) {
            $stateProvider.state('host.streetname', {
                url: '/streetname',
                templateUrl: '~/App/host/views/streetname/index.cshtml'
            });

            $stateProvider.state('createStreetName', {
                url: '/streetname/createModel',
                templateUrl: '~/App/host/views/streetname/createStreetNamemodal.cshtml'
            });

            $stateProvider.state('editStreetName', {
                url: '/streetname/editModal/:Id',
                templateUrl: '~/App/host/views/streetname/editStreetNamemodal.cshtml'
            });

        }

        if (abp.auth.hasPermission('Pages.Masters.PostalType')) {
            $stateProvider.state('host.postaltype', {
                url: '/postaltype',
                templateUrl: '~/App/host/views/postaltype/index.cshtml'
            });

            $stateProvider.state('createPostalType', {
                url: '/postaltype/createModel',
                templateUrl: '~/App/host/views/postaltype/createModal.cshtml'
            });

            $stateProvider.state('editPostalType', {
                url: '/postaltype/editModal/:Id',
                templateUrl: '~/App/host/views/postaltype/editModal.cshtml'
            });
        }
        if (abp.auth.hasPermission('Pages.Masters.PostCodes')) {
            $stateProvider.state('host.postcodes', {
                url: '/postcodes',
                templateUrl: '~/App/host/views/postcodes/index.cshtml'
            });

            $stateProvider.state('createPostCodes', {
                url: '/postcodes/createModel',
                templateUrl: '~/App/host/views/postcodes/createModal.cshtml'
            });

            $stateProvider.state('editPostCodes', {
                url: '/postcodes/editModal/:Id',
                templateUrl: '~/App/host/views/postcodes/editModal.cshtml'
            });

        }
        if (abp.auth.hasPermission('Pages.Masters.Inverter')) {
            $stateProvider.state('host.inverter', {
                url: '/inverter',
                templateUrl: '~/App/host/views/inverter/index.cshtml'
            });

            $stateProvider.state('createInverter', {
                url: '/inverter/createModel',
                templateUrl: '~/App/host/views/inverter/createModel.cshtml'
            });

            $stateProvider.state('editInverter', {
                url: '/inverter/editModal/:Id',
                templateUrl: '~/App/host/views/inverter/editModal.cshtml'
            });

        }
        if (abp.auth.hasPermission('Pages.Masters.Panel')) {
            $stateProvider.state('host.panel', {
                url: '/panel',
                templateUrl: '~/App/host/views/panel/index.cshtml'
            });

            $stateProvider.state('createPanel', {
                url: '/panel/createModel',
                templateUrl: '~/App/host/views/panel/createPanel.cshtml'
            });

            $stateProvider.state('editPanel', {
                url: '/panel/editModal/:Id',
                templateUrl: '~/App/host/views/panel/editPanel.cshtml'
            });

        }
        if (abp.auth.hasPermission('Pages.JobsReport')) {
            $stateProvider.state('host.jobsReport', {
                url: '/jobsReport',
                templateUrl: '~/App/host/views/jobsReport/index.cshtml'
            });
        }
        if (abp.auth.hasPermission('Pages.newInvitation')) {
            $stateProvider.state('host.newInvitation', {
                url: '/newInvitation',
                templateUrl: '~/App/host/views/newInvitation/index.cshtml'
            });
            $stateProvider.state('newInvitationDetail', {
                url: '/newInvitation/detailInstallerModel/:InstallerId',
                templateUrl: '~/App/host/views/newInvitation/detailInstallerModel.cshtml'
            });
        }
        if (abp.auth.hasPermission('Pages.AllInstallers')) {
            $stateProvider.state('host.allInstallers', {
                url: '/allInstallers',
                templateUrl: '~/App/host/views/allInstallers/index.cshtml'
            });
            $stateProvider.state('allInstallersDetail', {
                url: '/allInstallers/detailInstallerModel/:InstallerId',
                templateUrl: '~/App/host/views/allInstallers/detailInstallerModel.cshtml'
            });
        }
        if (abp.auth.hasPermission('Pages.AdminInvitation')) {
            $stateProvider.state('host.adminInvitation', {
                url: '/adminInvitation',
                templateUrl: '~/App/host/views/adminInvitation/index.cshtml'
            });
            $stateProvider.state('adminInvitationDetail', {
                url: '/adminInvitation/detailInstallerModel/:InstallerId',
                templateUrl: '~/App/host/views/adminInvitation/detailInstallerModel.cshtml'
            });
        }
        if (abp.auth.hasPermission('Pages.AllInstallerRequests')) {
            $stateProvider.state('host.allInstallerRequests', {
                url: '/allInstallerRequests',
                templateUrl: '~/App/host/views/allInstallerRequests/index.cshtml'
            });
            $stateProvider.state('allInstReqDetail', {
                url: '/allInstallerRequests/detailInstallerModel/:InstallerId',
                templateUrl: '~/App/host/views/allInstallerRequests/detailInstallerModel.cshtml'
            });
        }
        //if (abp.auth.hasPermission('Pages.InstallerInvitation')) {
        //    $stateProvider.state('tenant.installerInvitation', {
        //        url: '/installerInvitation',
        //        templateUrl: '~/App/tenant/views/installerInvitation/index.cshtml'
        //    });
        //}
        if (abp.auth.hasPermission('Pages.Editions')) {
            $stateProvider.state('host.editions', {
                url: '/editions',
                templateUrl: '~/App/host/views/editions/index.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Host.Maintenance')) {
            $stateProvider.state('host.maintenance', {
                url: '/maintenance',
                templateUrl: '~/App/host/views/maintenance/index.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Host.Settings')) {
            $stateProvider.state('host.settings', {
                url: '/settings',
                templateUrl: '~/App/host/views/settings/index.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Video')) {
            $stateProvider.state('host.video', {
                url: '/video',
                templateUrl: '~/App/host/views/video/index.cshtml'
            });

            $stateProvider.state('createVideo', {
                url: '/video/createVideo',
                templateUrl: '~/App/host/views/video/createModel.cshtml'
            });

            $stateProvider.state('editVideo', {
                url: '/video/editVideo/:Id',
                templateUrl: '~/App/host/views/video/editModel.cshtml'
            });

        }


        //TENANT routes

        $stateProvider.state('tenant', {
            'abstract': true,
            url: '/tenant',
            template: '<div ui-view class="fade-in-up"></div>'
        });

        if (abp.auth.hasPermission('Pages.Tenant.Dashboard')) {
            $urlRouterProvider.otherwise("/tenant/dashboard"); //Entrance page for a tenant
            $stateProvider.state('tenant.dashboard', {
                url: '/dashboard',
                templateUrl: '~/App/tenant/views/dashboard/index.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Host.Dashboard')) {
            $urlRouterProvider.otherwise("/host/dashboard"); //Entrance page for a tenant
            $stateProvider.state('host.dashboard', {
                url: '/dashboard',
                templateUrl: '~/App/host/views/dashboard/index.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Installer')) {
            $stateProvider.state('tenant.installer', {
                url: '/installer',
                templateUrl: '~/App/tenant/views/installer/index.cshtml'
            });
            $stateProvider.state('createInstaller', {
                url: '/installer/createModel',
                templateUrl: '~/App/tenant/views/installer/createModel.cshtml'
            });
            $stateProvider.state('editInstaller', {
                url: '/installer/editModel/:installerId',
                templateUrl: '~/App/tenant/views/installer/editModel.cshtml'
            });
            $stateProvider.state('detailInstaller', {
                url: '/installer/detailModel/:installerId',
                templateUrl: '~/App/tenant/views/installer/detailModel.cshtml'
            });

            $stateProvider.state('installerEmployee', {
                url: '/installer/employeeList/:InstallerId',
                templateUrl: '~/App/tenant/views/installer/employeeList.cshtml'
            });
            $stateProvider.state('createEmpInstaller', {
                url: '/installer/createEmpModel/:installerId',
                templateUrl: '~/App/tenant/views/installer/createEmpModel.cshtml'
            });
            $stateProvider.state('editEmpInstaller', {
                url: '/installer/editEmpModel/:InstallerId/:empId',
                templateUrl: '~/App/tenant/views/installer/editEmpModel.cshtml'
            });
            $stateProvider.state('detailEmpInstaller', {
                url: '/installer/detailEmpModel/:InstallerId/:empId',
                templateUrl: '~/App/tenant/views/installer/detailEmpModel.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.EnergyRetailers')) {
            $stateProvider.state('tenant.energyRetailers', {
                url: '/energyRetailers',
                templateUrl: '~/App/tenant/views/energyRetailers/index.cshtml'
            });
            $stateProvider.state('createEnergyRetailers', {
                url: '/energyRetailers/createModel',
                templateUrl: '~/App/tenant/views/energyRetailers/createModel.cshtml'
            });
            $stateProvider.state('editEnergyRetailers', {
                url: '/energyRetailers/editModel/:Id',
                templateUrl: '~/App/tenant/views/energyRetailers/editModel.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.EnergyDestributars')) {
            $stateProvider.state('tenant.energyDestributars', {
                url: '/energyDestributars',
                templateUrl: '~/App/tenant/views/energyDestributars/index.cshtml'
            });
            $stateProvider.state('createEnergyDestributars', {
                url: '/energyDestributars/createModel',
                templateUrl: '~/App/tenant/views/energyDestributars/createModel.cshtml'
            });
            $stateProvider.state('editEnergyDestributars', {
                url: '/energyDestributars/editModel/:Id',
                templateUrl: '~/App/tenant/views/energyDestributars/editModel.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Administration.Tenant.Settings')) {
            $stateProvider.state('tenant.settings', {
                url: '/settings',
                templateUrl: '~/App/tenant/views/settings/index.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.Tenant.Calendar')) {
            $stateProvider.state('tenant.calendar', {
                url: '/calendar',
                templateUrl: '~/App/tenant/views/calendar/index.cshtml'
            });
        }

        if (abp.auth.hasPermission('Pages.STC')) {
            $stateProvider.state('host.stc', {
                url: '/stc',
                templateUrl: '~/App/host/views/stc/index.cshtml'
            });

            $stateProvider.state('detailstcJob', {
                url: '/stc/detailModal/:jobId',
                templateUrl: '~/App/host/views/stc/detailModal.cshtml'
            });
        }

        //$qProvider settings
        $qProvider.errorOnUnhandledRejections(false);
    }
]);

appModule.run(["$rootScope", "settings", "$state", 'i18nService', '$uibModalStack', function ($rootScope, settings, $state, i18nService, $uibModalStack) {
    $rootScope.$state = $state;
    $rootScope.$settings = settings;

    $rootScope.$on('$stateChangeSuccess', function () {
        $uibModalStack.dismissAll();
    });

    //Set Ui-Grid language
    if (i18nService.get(abp.localization.currentCulture.name)) {
        i18nService.setCurrentLang(abp.localization.currentCulture.name);
    } else {
        i18nService.setCurrentLang("en");
    }

    $rootScope.safeApply = function (fn) {
        var phase = this.$root.$$phase;
        if (phase == '$apply' || phase == '$digest') {
            if (fn && (typeof (fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };
}]);

