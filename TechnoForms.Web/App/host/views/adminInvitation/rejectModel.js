﻿(function () {
    appModule.controller('host.views.adminInvitation.rejectModel', [
        '$scope', '$uibModalInstance', 'abp.services.app.adminInvitation', 'invitationId',
        function ($scope, $uibModalInstance, adminInvitationService, invitationId) {
            var vm = this;

            vm.saving = false;
            vm.invitation = {};

            vm.save = function () {
                vm.saving = true;
                adminInvitationService.rejectAdminInvitation(vm.invitation).then(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $uibModalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                adminInvitationService.getAdminInvitationForReject({
                    id: invitationId
                }).then(function (result) {
                    vm.invitation = result.data;
                });
            }
            init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();