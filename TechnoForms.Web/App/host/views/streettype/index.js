﻿(function () {
    appModule.controller('host.views.streettype.index', [
        '$scope', '$state', '$uibModal', '$http', 'uiGridConstants', 'abp.services.app.streetType',
        function ($scope, $state, $uibModal, $http, uiGridConstants,  streettypeService) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.gridHeight = 0;
            vm.loading = false;

            vm.permissions = {
                createstreettype: abp.auth.hasPermission('Pages.Masters.StreetType.Create'),
                deletestreettype: abp.auth.hasPermission('Pages.Masters.StreetType.Delete'),
                editStreetType: abp.auth.hasPermission('Pages.Masters.StreetType.Edit')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            $scope.uploadFile = function (file) {
                console.log('file is ');
                var file = $('#filetoupload')[0].files[0];
                console.dir($('#filetoupload')[0].files[0]);
                console.dir(file);
                var uploadUrl = "../File/ImportData";
                var fd = new FormData();
                fd.append('file', file);

                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).then(function (response) {
                    console.log(response);
                });
            };

            console.log(app.consts.grid.defaultPageSizes);
            console.log(app.consts.grid.defaultPageSize);
            vm.streetTypeGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                enableColumnResizing: true,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        enableColumnMenu: false,
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',
                        width: 80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.editStreetType" ng-click="grid.appScope.editStreetType(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.deletestreettype" ng-click="grid.appScope.deleteStreetType(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('StreetTypeName'),
                        enableColumnMenu: false,
                        field: 'streetTypeName',
                        enableColumnResizing: false,
                        minWidth:150,
                       
                    },
                    {
                        name: app.localize('StreetTypeCode'),
                        enableColumnMenu: false,
                        field: 'streetTypeCode',
                        width: 220,
                    },
                    {
                        name: app.localize('CreationTime'),
                        enableColumnMenu: false,
                        field: 'creationTime',
                        width: 170,
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getStreetType();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getStreetType();
                    });
                },
                data: []
            };

            if (!vm.permissions.editStreetType && !vm.permissions.deletestreettype) {
                vm.streetTypeGridOptions.columnDefs.shift();
            }

            vm.getStreetType = function () {
                vm.loading = true;
                streettypeService.getStreetType({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).then(function (result) {
                    vm.streetTypeGridOptions.totalItems = result.data.totalCount;
                    vm.streetTypeGridOptions.data = result.data.items;
                    console.log(result.data.items);
                    vm.gridHeight = "height:" + (vm.streetTypeGridOptions.data.length * 37 + 35 + 61) + "px";
                    //$('.ui-grid-viewport').css('height', 'auto');
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createStreetType = function () {
                //$state.go('createStreetType', {
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/streettype/createModal.cshtml',
                    controller: 'host.views.streettype.createModal as vm',
                    backdrop: 'static'
                });
                modalInstance.result.then(function (result) {
                    vm.getStreetType();
                });
            };

            vm.deleteStreetType = function (streettype) {
                abp.message.confirm(
                    app.localize('StreetTypeDeleteWarningMessage', streettype.streetTypeName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            streettypeService.deleteStreetType({
                                id: streettype.id
                            }).then(function () {
                                vm.getStreetType();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.editStreetType = function (streettype) {
                //$state.go('editstreettype', {
                //    Id: streettype.id
                //});

                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/streettype/editModal.cshtml',
                    controller: 'host.views.streettype.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        Id: function () {
                            return streettype.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getStreetType();
                });
            };
                        
                    
            vm.getStreetType();
        }
    ]);
})();