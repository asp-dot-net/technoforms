﻿(function () {
    appModule.controller('host.views.streettype.editModal', [
        '$scope', '$state', '$stateParams','$uibModalInstance', 'abp.services.app.streetType','Id',
        function ($scope, $state, $stateParams, $uibModalInstance, streettypeService,Id) {
            var vm = this;

            vm.saving = false;
            vm.streettype = null;
            vm.editions = [];
            //vm.streettypeId = $stateParams.Id;
            vm.exist = false;

            vm.save = function () {
                vm.saving = true;

                streettypeService.checkStreetTypeExists({ StreetTypeName: vm.streettype.streetTypeName, StreetTypeCode: vm.streettype.streetTypeCode, Id: Id }).then(function (result) {
                    if (result.data.status == true) {
                        streettypeService.updateStreetType(vm.streettype)
                            .then(function () {
                                //$state.go('host.streettype', {
                                //});
                                abp.notify.info(app.localize('SavedSuccessfully'));
                                $uibModalInstance.close();
                            }).finally(function () {
                                vm.saving = false;
                            });
                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        //vm.message = result.data.message;
                        //vm.exist = true;
                        vm.saving = false;
                    }
                });


              
            };

            vm.cancel = function () {
                //$state.go('host.streettype', {
                //});
                $uibModalInstance.dismiss();
            };

            function init() {
                streettypeService.getStreetTypeForEdit({
                    id: Id //vm.streettypeId
                }).then(function (result) {
                    vm.streettype = result.data;
                });
            }
            init();
        }
    ]);
})();