﻿(function () {
    appModule.controller('host.views.streettype.createModal', [
        '$scope', '$state','$uibModalInstance', 'abp.services.app.streetType',
        function ($scope, $state, $uibModalInstance, streettypeService) {
            var vm = this;

            vm.saving = false;
            vm.streetType = {};
            vm.exist = false;

            vm.save = function () {
                vm.saving = true;
                streettypeService.checkStreetTypeExists({ StreetTypeName: vm.streetType.streetTypeName, StreetTypeCode: vm.streetType.streetTypeCode }).then(function (result) {
                    if (result.data.status == true) {

                        streettypeService.createStreetType(vm.streetType).then(function () {
                            //$state.go('host.streettype', {
                            //});
                            abp.notify.info(app.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }).finally(function () {
                            vm.saving = false;
                        });

                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        //vm.message = result.data.message;
                        //vm.exist = true;
                        vm.saving = false;
                    }
                });
            };

            vm.cancel = function () {
                //$state.go('host.streettype', {
                //});
                 $uibModalInstance.dismiss();
            };
        }
    ]);
})();