﻿(function () {
    appModule.controller('host.views.pendinginvoice.invoiceGenerateDetail', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.invoice', 'uiGridConstants',
        function ($scope, $state, $stateParams, $uibModal, invoiceService, uiGridConstants) {
            var vm = this;
            vm.norecord = false;
            vm.saving = false;
            vm.invoiceNumber = null;
            vm.isValidinvoiceNumber = true;
            vm.save = function () {
            };

            vm.cancel = function () {
                $state.go('host.pendinginvoice');
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.tenantId = $stateParams.TenantId;
            vm.jobsGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('JobId'),
                        field: 'jobId',
                        minWidth:100,
                        width:100,
                    },
                    {
                        name: app.localize('ProjectNumber'),
                        field: 'projectNumber',
                        
                    },
                    {
                        name: app.localize('TotalSTC'),
                        field: 'totalSTC',
                        width:150,
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getJobs();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getJobs();
                    });
                },
                data: []
            };

            vm.getJobs = function () {
                vm.loading = true;
                invoiceService.getJobByTenantId({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    tenantId: vm.tenantId
                }).then(function (result) {
                    vm.jobsGridOptions.totalItems = result.data.totalCount;
                    vm.jobsGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.generateInvoice = function (tenantId) {
                console.log(vm.invoiceNumber);
                if (vm.invoiceNumber == null) {
                    vm.isValidinvoiceNumber = false;
                    //abp.notify.info(app.localize('PleaseenterInvoiceNumber'));
                }
                else {
                    vm.isValidinvoiceNumber = true;
                    abp.message.confirm(
                        app.localize('InvoiceGenerateConfirmMessage', tenantId),
                        function (isConfirmed) {
                            if (isConfirmed) {
                                invoiceService.generateInvoice({
                                    tenantId: tenantId, invoiceNumber: vm.invoiceNumber
                                }).then(function () {
                                    $state.go('InvoiceGenerated');
                                    abp.notify.success(app.localize('InvoiceSuccessfullyGenerated'));
                                });
                            }
                        }
                    );
                }
                //abp.message.confirm(
                //    app.localize('InvoiceGenerateConfirmMessage', tenantId),
                //    function (isConfirmed) {
                //        if (isConfirmed) {
                //            invoiceService.generateInvoice({
                //                tenantId: tenantId, invoiceNumber: vm.invoiceNumber
                //            }).then(function () {
                //                $state.go('InvoiceGenerated');
                //                abp.notify.success(app.localize('InvoiceSuccessfullyGenerated'));
                //            });
                //        }
                //    }
                //);

                // if (vm.invoiceNumber != null) {

                //}
                //else {
                //    vm.isValidinvoiceNumber = false;
                //    abp.notify.info(app.localize('PleaseenterInvoiceNumber'));
                //}
            };


            function init() {
                invoiceService.getInvoiceDetailByTenantId({
                    tenantId: vm.tenantId
                }).then(function (result) {
                    vm.customername = result.data.tenantName;
                    vm.totalSTC = result.data.totalSTC;
                    vm.strRate = result.data.strRate;
                    vm.totalInvoiceAmount = result.data.totalInvoiceAmount;
                }).finally(function () {
                    vm.loading = false;
                });

                vm.getJobs();
            }

            init();
        }
    ]);
})();