﻿(function () {
    appModule.controller('host.views.pendinginvoice.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.invoice',
        function ($scope, $state, $uibModal, uiGridConstants, invoiceService) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            //start from here...
            vm.loading = false;
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };
            vm.dateRangeOptions = app.createDateRangePickerOptions();
            vm.dateRangeModel = {
                startDate: moment().startOf('month'),
                endDate: moment().endOf('month')
            };
            vm.jobsGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('GenerateInvoice'),
                        enableColumnMenu: false, 
                        headerCellClass:'centeralign',
                        cellClass:'centeralign', 
                        enableSorting: false,
                        width:180,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                            '<a class="btn btn-xs btn-primary blue" ng-click="grid.appScope.InvoiceDetailPage(row.entity)">' + app.localize('GenerateInvoice') + '</a>'
                            + '</div>'
                    },
                    {
                        name: app.localize('Name'),
                        enableColumnMenu: false,                    
                        minWidth:150,
                    },
                    {
                        name: app.localize('TotalSTC'),
                        field: 'totalSTC',
                        enableColumnMenu: false,      
                        cellClass:'rightalign',
                        width:150
                    },
                    {
                        name: app.localize('STCRate'),
                        field: 'strRate',
                        enableColumnMenu: false,      
                        cellClass:'rightalign',
                        width:150
                    },
                    {
                        name: app.localize('TotalInvoiceAmount'),
                        field: 'totalInvoiceAmount',
                        cellClass:'rightalign',
                        enableColumnMenu: false,      
                        width:200
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getJobs();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getJobs();
                    });
                },
                data: []
            };

            vm.getJobs = function () {
                vm.loading = true;
                invoiceService.getGenerateInvoice({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    startDate: vm.dateRangeModel.startDate,
                    endDate: vm.dateRangeModel.endDate
                }).then(function (result) {
                    vm.jobsGridOptions.totalItems = result.data.totalCount;
                    vm.jobsGridOptions.data = result.data.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };
            vm.InvoiceDetailPage = function (Tenant) {
                $state.go('InvoiceGenerateDetail', {
                    TenantId: Tenant.id
                });
            };

            
            vm.getJobs();
        }
    ]);
})();