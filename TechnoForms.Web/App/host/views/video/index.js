﻿(function () {
	appModule.controller('host.views.video.index', [
		'$scope', '$state', '$uibModal', 'abp.services.app.video',
		function ($scope, $state, $uibModal, videoService) {
			var vm = this;
			vm.norecord = false;
			vm.videos = [];
			vm.createvideos = true;

			vm.permissions = {
				createVideo: abp.auth.hasPermission('Pages.Video.Create'),
				editVideo: abp.auth.hasPermission('Pages.Video.Edit'),
				deleteVideo: abp.auth.hasPermission('Pages.Video.Delete')
			};

			vm.creatingNewvideo = function () {
				//$state.go('createVideo');
				var modalInstance = $uibModal.open({
					templateUrl: '/App/host/views/video/createModel.cshtml',
					controller: 'host.views.video.createModel as vm',
					backdrop: 'static'
				});

				modalInstance.result.then(function (result) {
					vm.getVideos();
				});
			};

			vm.getVideos = function () {
				vm.loading = true;
				videoService.getVideos().then(function (result) {
					vm.videos = result.data.items;
					if (result.data.items.length >= 6) {
						vm.createvideos = false;
					}
					if (result.data.totalCount == 0) {
						//vm.norecord = true;
						abp.notify.info(app.localize('NoRecordFound'));
					}
					else { vm.norecord = false; }

				}).finally(function () {
					vm.loading = false;
				});
			};

			vm.editvideo = function (id) {
				//$state.go('editVideo', { Id: id });
				var modalInstance = $uibModal.open({
					templateUrl: '/App/host/views/video/editModel.cshtml',
					controller: 'host.views.video.editModel as vm',
					backdrop: 'static',
					resolve: {
						videoId: function () {
							return id;
						}
					}
				});

				modalInstance.result.then(function (result) {
					vm.getVideos();
				});
			};
			$scope.$on('$viewContentLoaded', function () {
				App.initAjax();
			});
			vm.getVideos();
			//start from here...
		}
	]);
})();