﻿(function () {
	appModule.controller('host.views.video.editModel', [
		'$scope', '$state', '$uibModalInstance', '$stateParams', 'FileUploader', 'abp.services.app.video','videoId',
		function ($scope, $state, $uibModalInstance, $stateParams, fileUploader, videoService, videoId) {
			var vm = this;

			vm.docFIleName = null;
			vm.saving = false;
			vm.video = {};
			vm.videoId = $stateParams.Id;
			vm.logouploader = new fileUploader({
				url: abp.appPath + 'File/UploadVideo',
				headers: {
					"X-XSRF-TOKEN": abp.security.antiForgery.getToken()
				},
				queueLimit: 1,
				autoUpload: true,
				removeAfterUpload: true,
				filters: [{
					name: 'imageFilter',
					fn: function (item, options) {
						//File type check
						var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
						//if ('|jpg|jpeg|png|gif|'.indexOf(type) === -1) {
						//    abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
						//    return false;
						//}

						//File size check
						//if (item.size > 1048576) //1MB
						//{
						//    abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
						//    return false;
						//}

						return true;
					}
				}]
			});

			vm.logouploader.onSuccessItem = function (fileItem, response, status, headers) {
				if (response.success) {
					var $LogoResize = $('#Logophoto');
					var profileFilePath = abp.appPath + 'Temp/Video/' + response.result.fileName + '?v=' + new Date().valueOf();
					vm.docFIleName = response.result.fileName;
					$LogoResize.attr('src', profileFilePath);

				} else {
					abp.message.error(response.error.message);
				}
			};


			vm.save = function () {
				if (!vm.docFIleName) {
					return;
				}
				vm.video.fileName = vm.docFIleName;
				vm.video.id = videoId; //vm.videoId;
				vm.saving = true;
				videoService.updateVideo(vm.video).then(function () {
					//$state.go('host.video', {
					//});
					abp.notify.info(app.localize('SavedSuccessfully'));
					$uibModalInstance.close();
				}).finally(function () {
					vm.saving = false;
				});
			};

			vm.cancel = function () {
				//$state.go('host.video', {
				//});
				$uibModalInstance.dismiss();
			};

			function init() {
				videoService.getVideoForEdit({
					id: videoId //vm.videoId
				}).then(function (result) {
					vm.video = result.data;
					console.log(vm.video);
				});
			}
			init();
		}
	]);
})();