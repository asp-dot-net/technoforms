﻿(function () {
    appModule.controller('host.views.postaltype.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.postalType',
        function ($scope, $state, $uibModal, uiGridConstants, postaltypeService) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;
            //vm.filterText = $stateParams.filterText || null;

            vm.permissions = {
                createpostaltype: abp.auth.hasPermission('Pages.Masters.PostalType.Create'),
                deletepostaltype: abp.auth.hasPermission('Pages.Masters.PostalType.Delete'),
                editpostaltype: abp.auth.hasPermission('Pages.Masters.PostalType.Edit')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.postalTypeGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        enableColumnMenu: false,
                        headerCellClass:'centeralign',
                        cellClass:'centeralign',
                        width: 80,
                        cellTemplate:
                        '<div class=\"ui-grid-cell-contents\">' +
                        '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                        '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                        '    <ul uib-dropdown-menu>' +
                        '      <li><a ng-if="grid.appScope.permissions.editpostaltype" ng-click="grid.appScope.editPostalType(row.entity)">' + app.localize('Edit') + '</a></li>' +
                        '      <li><a ng-if="grid.appScope.permissions.deletepostaltype" ng-click="grid.appScope.deletePostalType(row.entity)">' + app.localize('Delete') + '</a></li>' +
                        '    </ul>' +
                        '  </div>' +
                        '</div>'
                    },
                    {
                        name: app.localize('PostalTypeName'),
                        enableColumnMenu: false,                      
                        field: 'postalTypeName',
                        minWidth:200,
                    },
                    {
                        name: app.localize('PostalTypeCode'),
                        enableColumnMenu: false,                    
                        field: 'postalTypeCode',
                        width: 220
                    },
                    {
                        name: app.localize('CreationTime'),
                        enableColumnMenu: false,     
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',               
                        field: 'creationTime',
                        width: 170,                      
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getPostalType();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getPostalType();
                    });
                },
                data: []
            };

            if (!vm.permissions.editpostaltype && !vm.permissions.deletepostaltype) {
                vm.postalTypeGridOptions.columnDefs.shift();
            }
            
            vm.getPostalType = function () {
                vm.loading = true;
                postaltypeService.getPostalType({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).then(function (result) {
                    vm.postalTypeGridOptions.totalItems = result.data.totalCount;
                    vm.postalTypeGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createPostalType = function () {
                //$state.go('createPostalType', {
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/postaltype/createModal.cshtml',
                    controller: 'host.views.postaltype.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.getPostalType();
                });
            };

            vm.deletePostalType = function (postaltype) {
                abp.message.confirm(
                    app.localize('postalTypeDeleteWarningMessage', postaltype.postalTypeName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            postaltypeService.deletePostalType({
                                id: postaltype.id
                            }).then(function () {
                                vm.getPostalType();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.editPostalType = function (postaltype) {
                //$state.go('editPostalType', {
                //    Id: postaltype.id
                //});

                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/postaltype/editModal.cshtml',
                    controller: 'host.views.postaltype.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        Id: function () {
                            return postaltype.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getPostalType();
                });
            };

            vm.getPostalType();
        }
    ]);
})();