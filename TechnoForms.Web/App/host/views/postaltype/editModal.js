﻿(function () {
    appModule.controller('host.views.postaltype.editModal', [
        '$scope', '$state', '$stateParams','$uibModalInstance', 'abp.services.app.postalType','Id',
        function ($scope, $state, $stateParams, $uibModalInstance, postaltypeService,Id) {
            var vm = this;

            vm.saving = false;
            vm.postaltype = null;
            vm.editions = [];
            vm.postaltypeId = $stateParams.Id;
            vm.exist = false;

            vm.save = function () {
                vm.saving = true;

                postaltypeService.checkPostalTypeExists({ PostalTypeName: vm.postaltype.postalTypeName, PostalTypeCode: vm.postaltype.postalTypeCode, Id: Id }).then(function (result) {
                    if (result.data.status == true) {
                        postaltypeService.updatePostalType(vm.postaltype)
                            .then(function () {
                                //$state.go('host.postaltype', {
                                //});
                                abp.notify.info(app.localize('SavedSuccessfully'));
                                 $uibModalInstance.close();
                            }).finally(function () {
                                vm.saving = false;
                            });
                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        //vm.message = result.data.message;
                        //vm.exist = true;
                        vm.saving = false;
                    }
                });


            };

            vm.cancel = function () {
                //$state.go('host.postaltype', {
                //});
                $uibModalInstance.dismiss();
            };

            function init() {
                postaltypeService.getPostalTypeForEdit({
                    id: Id
                }).then(function (result) {
                    vm.postaltype = result.data;
                });
            }
            init();
        }
    ]);
})();