﻿(function () {
    appModule.controller('host.views.postaltype.createModal', [
        '$scope', '$state','$uibModalInstance', 'abp.services.app.postalType',
        function ($scope, $state, $uibModalInstance, postaltypeService) {
            var vm = this;

            vm.saving = false;
            vm.postalType = {};
            vm.exist = false;

            vm.save = function () {
                vm.saving = true;


                postaltypeService.checkPostalTypeExists({ PostalTypeName: vm.postalType.postalTypeName, PostalTypeCode: vm.postalType.postalTypeCode }).then(function (result) {
                    if (result.data.status == true) {

                        postaltypeService.createPostalType(vm.postalType).then(function () {
                            //$state.go('host.postaltype', {
                            //});
                            abp.notify.info(app.localize('SavedSuccessfully'));
                             $uibModalInstance.close();
                        }).finally(function () {
                            vm.saving = false;
                        });

                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        //vm.message = result.data.message;
                        //vm.exist = true;
                        vm.saving = false;
                    }
                });

              
            };

            vm.cancel = function () {
                //$state.go('host.postaltype', {
                //});

                 $uibModalInstance.dismiss();
            };
        }
    ]);
})();