﻿(function () {
    appModule.controller('host.views.postcodes.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.postCodes', 'abp.services.app.state',
        function ($scope, $state, $uibModal, uiGridConstants, postcodesService, stateService) {
            var vm = this;
            vm.norecord = false;
            vm.stateNames = [];
            vm.classname = "";
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });
            vm.loading = false;
            //start from here...
           

            vm.permissions = {
                createpostcodes: abp.auth.hasPermission('Pages.Masters.PostalType.Create'),
                deletepostcodes: abp.auth.hasPermission('Pages.Masters.PostalType.Delete'),
                editpostcodes: abp.auth.hasPermission('Pages.Masters.PostalType.Edit')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.postcodesGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        enableColumnMenu: false,
                        headerCellClass:'centeralign',
                        cellClass:'centeralign',
                        width: 80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.editpostcodes" ng-click="grid.appScope.editPostCodes(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.deletepostcodes" ng-click="grid.appScope.deletePostCodes(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('PostCodes'),
                        enableColumnMenu: false,
                        headerCellClass:'centeralign',
                        cellClass:'centeralign',
                        field: 'postcode',
                        width: 130
                    },
                    {
                        name: app.localize('Suburb'),
                        enableColumnMenu: false,                       
                        field: 'suburb',
                        minWidth:150,
                    },
                    {
                        name: app.localize('State'),
                        enableColumnMenu: false,   
                        field: 'state',
                        minWidth:150,
                    },
                    {
                        name: app.localize('POBoxes'),
                        enableColumnMenu: false,                       
                        field: 'poBoxes',
                        width: 150,
                    },
                    {
                        name: app.localize('Area'),
                        enableColumnMenu: false,                       
                        field: 'area',
                        minWidth:150,
                    },
                    {
                        name: app.localize('CreationTime'),
                        enableColumnMenu: false,      
                        headerCellClass:'centeralign',
                        cellClass:'centeralign',                 
                        field: 'creationTime',
                        width: 170,
                        cellClass: 'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getPostCodes();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getPostCodes();
                    });
                },
                data: []
            };

            if (!vm.permissions.editpostcodes && !vm.permissions.deletepostcodes) {
                vm.postcodesGridOptions.columnDefs.shift();
            }

            vm.getPostCodes = function () {
                vm.loading = true;
                postcodesService.getPostCodes({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    postcode: vm.filterpostcode,
                    suburb: vm.filtersuburb,
                    state: vm.filterstate
                }).then(function (result) {
                    vm.postcodesGridOptions.totalItems = result.data.totalCount;
                    vm.postcodesGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createPostCodes = function () {
                //$state.go('createPostCodes', {
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/postcodes/createModal.cshtml',
                    controller: 'host.views.postcodes.createModal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function (result) {
                    vm.getPostCodes();
                });
            };

            vm.deletePostCodes = function (postcodes) {
                abp.message.confirm(
                    app.localize('PostCodeDeleteWarningMessage', postcodes.postcode),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            postcodesService.deletePostCodes({
                                id: postcodes.id
                            }).then(function () {
                                vm.getPostCodes();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.editPostCodes = function (postcodes) {
                //$state.go('editPostCodes', {
                //    Id: postcodes.id
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/postcodes/editModal.cshtml',
                    controller: 'host.views.postcodes.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        Id: function () {
                            return postcodes.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getPostCodes();
                });
            };

            vm.ClearAll = function(){
                vm.filterpostcode = null;
                vm.filtersuburb = null;
                vm.filterstate = null;
                setTimeout(function () {
                    $('#stateSelectionCombobox').selectpicker('refresh');
                }, 0);

                vm.getPostCodes();
            };

            function init() {
                stateService.getState().then(function (result) {
                    vm.stateNames = result.data.items;
                    vm.flags = result.data.flags;
                    setTimeout(function () {
                        $('#stateSelectionCombobox').selectpicker('refresh');
                        $('#stateSelectionCombobox').selectpicker('refresh');
                    }, 0);
                    vm.loading = false;
                });
            }
            init();
            
            vm.getPostCodes();
        }
    ]);
})();