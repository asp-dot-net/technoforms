﻿(function () {
    appModule.controller('host.views.postcodes.createModal', [
        '$scope', '$state','$uibModalInstance', 'abp.services.app.postCodes', 'abp.services.app.state',
        function ($scope, $state, $uibModalInstance, postcodesService, stateService) {
            var vm = this;

            vm.saving = false;
            vm.postcodes = {};
            vm.stateNames = [];
            vm.exist = false;
            $scope.CheckNumber = function () {
                if (isNaN(event.key) || event.key === ' ' || event.key === '') {
                    event.returnValue = '';
                }
            };
            vm.save = function () {
                vm.saving = true;


                angular.forEach(vm.stateNames, function (value, key) {
                    if (value.id === vm.postcodes.state) {
                        vm.postcodes.state = value;
                    }
                });

                postcodesService.checkPostCodesExists({ Postcode: vm.postcodes.postCode, Suburb: vm.postcodes.suburb }).then(function (result) {
                    if (result.data.status == true) {

                        postcodesService.createPostCodes(vm.postcodes).then(function () {
                            //$state.go('host.postcodes');
                            abp.notify.info(app.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }).finally(function () {
                            vm.saving = false;
                        });

                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        //vm.message = result.data.message;
                        //vm.exist = true;
                        vm.saving = false;
                    }
                });


            };

            vm.cancel = function () {
                //$state.go('host.postcodes');
                $uibModalInstance.dismiss();
            };

            function init() {
                stateService.getState().then(function (result) {
                    //vm.state = result.data.items;
                    vm.stateNames = result.data.items;
                    vm.flags = result.data.flags;
                    setTimeout(function () {
                        $('#stateSelectionCombobox').selectpicker('refresh');
                        $('#stateSelectionCombobox').selectpicker('refresh');
                    }, 0);
                    vm.loading = false;
                });
            }

            init();
        }
    ]);
})();