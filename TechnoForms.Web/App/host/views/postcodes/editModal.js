﻿(function () {
    appModule.controller('host.views.postcodes.editModal', [
        '$scope', '$state', '$stateParams','$uibModalInstance', 'abp.services.app.postCodes', 'abp.services.app.state','Id',
        function ($scope, $state, $stateParams, $uibModalInstance, postcodesService, stateService,Id) {
            var vm = this;

            vm.saving = false;
            vm.state = null;
            vm.postcodes = null;
            vm.editions = [];
            vm.exist = false;
            //vm.state = null;
            //vm.stateNames = [];
            vm.postcodesId = $stateParams.Id;
            $scope.CheckNumber = function () {
                if (isNaN(event.key) || event.key === ' ' || event.key === '') {
                    event.returnValue = '';
                }
            };
            vm.save = function () {
                vm.saving = true;

                postcodesService.checkPostCodesExists({ Postcode: vm.postcodes.postcode, Suburb: vm.postcodes.suburb, Id: Id }).then(function (result) {
                    if (result.data.status == true) {
                        postcodesService.updatePostCodes(vm.postcodes)
                            .then(function () {
                                abp.notify.info(app.localize('SavedSuccessfully'));
                                //$state.go('host.postcodes');
                                $uibModalInstance.close();
                            }).finally(function () {
                                vm.saving = false;
                            });
                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        //vm.message = result.data.message;
                        //vm.exist = true;
                        vm.saving = false;
                    }
                });

               
            };

            vm.cancel = function () {
                //$state.go('host.postcodes');
                $uibModalInstance.dismiss();
            };

            function init() {
                postcodesService.getPostCodesForEdit({
                    id: Id
                }).then(function (result) {

                    vm.postcodes = result.data;
                    stateService.getState().then(function (result) {
                        //vm.state = result.data.items;
                        vm.stateNames = result.data.items;
                        setTimeout(function () {
                            $('#stateSelectionCombobox').selectpicker('refresh');
                            $('#stateSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;

                        angular.forEach(vm.stateNames, function (value, key) {
                            if (value.id === vm.postcodes.stateId) {
                                vm.state = value;
                            }
                        });

                        //vm.state = vm.stateNames[1];

                    });


                });

            }
            init();
        }
    ]);
})();