﻿(function () {
	appModule.controller('host.views.tenants.featuresModal', [
		'$scope', '$state','$uibModalInstance', '$stateParams', 'abp.services.app.tenant', 'tenant',
		function ($scope, $state, $uibModalInstance, $stateParams, tenantService, tenant) {
			//'$scope', '$state', '$uibModalInstance', 'abp.services.app.tenant', 'tenant',
			//function ($scope, $state, $uibModalInstance, tenantService, tenant) {
			var vm = this;

			vm.saving = false;
			vm.resettingFeatures = false;
			//vm.tenant = tenant;
			vm.tenant = $stateParams.tenantId;
			vm.featureEditData = null;

			vm.tenantId = $stateParams.tenantId;
			vm.resetFeatures = function () {
				vm.resettingFeatures = true;
				tenantService.resetTenantSpecificFeatures({
					id: tenant
				}).then(function () {
					abp.notify.info(app.localize('ResetSuccessfully'));
					loadFeatures();
				}).finally(function () {
					vm.resettingFeatures = false;
				});
			};

			vm.save = function () {
				if (!vm.featureEditData.isValid()) {
					abp.message.warn(app.localize('InvalidFeaturesWarning'));
					return;
				}

				vm.saving = true;
				tenantService.updateTenantFeatures({
					id: tenant,
					featureValues: vm.featureEditData.featureValues
				}).then(function () {
					//$state.go('host.tenants');
					abp.notify.info(app.localize('SavedSuccessfully'));
					$uibModalInstance.close();
				}).finally(function () {
					vm.saving = false;
				});
			};

			vm.cancel = function () {
				//$state.go('host.tenants');
				$uibModalInstance.dismiss();
			};

			function loadFeatures() {
				tenantService.getTenantFeaturesForEdit({
					id: tenant
				}).then(function (result) {
					console.log(result.data);
					vm.featureEditData = result.data;
				});

				tenantService.getTenantForEdit({
					id: tenant
				}).then(function (result) {
					vm.tenant = result.data;
				});
			}



			loadFeatures();
		}
	]);
})();