﻿(function () {
	appModule.controller('host.views.tenants.createModal', [
		'$scope', '$state', 'abp.services.app.tenant', 'abp.services.app.commonLookup',
		function ($scope, $state, tenantService, commonLookupService) {
			var vm = this;

			vm.saving = false;
			vm.setRandomPassword = true;
			vm.useHostDb = true;
			vm.unitTypes = [];
			vm.streetNames = [];
			vm.streetTypes = [];
			vm.editions = [];
			vm.suburb = "";
			vm.IsGoogleAddress = true;

			vm.tenant = {
				isActive: true,
				shouldChangePasswordOnNextLogin: true,
				sendActivationEmail: true,
				editionId: 0
			};
			vm.emailFormat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			vm.phonepattern = /^(07|03|08|04|13|18)[\d]{8}/;
			vm.CheckNumber = function () {
				if (isNaN(event.key) || event.key === ' ' || event.key === '') {
					event.returnValue = '';
				}
			};
			vm.CheckFloatNumber = function () {
				if ((isNaN(event.key) || event.key === ' ' || event.key === '') && event.key != '.') {
					event.returnValue = '';
				}
			};

			vm.getEditionValue = function (item) {
				return parseInt(item.value);
			};

			//vm.getPostCodes = function (streetCity) {
			//	if (streetCity != null) {
			//                 tenantService.getPostCodesListBySuburb({ id: streetCity }).then(function (result) {
			//                     vm.tenant.suburb = result.data.items[0].suburb;
			//			vm.postCodes = result.data.items;
			//			setTimeout(function () {
			//				$('#postCodeSelectionCombobox').selectpicker('refresh');
			//				$('#postCodeSelectionCombobox').selectpicker('refresh');
			//			}, 0);
			//			vm.stateNames = result.data.items;
			//			setTimeout(function () {
			//				$('#stateSelectionCombobox').selectpicker('refresh');
			//				$('#stateSelectionCombobox').selectpicker('refresh');
			//			}, 0);

			//			vm.tenant.state = result.data.items[0].stateName;
			//			vm.tenant.postCode = result.data.items[0].postcode;

			//			vm.loading = false;
			//		});
			//	}
			//};

			//vm.getStreetAddress = function () {

			//	var unittype = "";
			//	var unitNo = "";
			//	var streetNo = "";
			//	var streetName = "";
			//	var streetType = "";
			//	if (vm.tenant.unitType != null) {
			//		unittype = vm.tenant.unitType;
			//	}
			//	if (vm.tenant.unitNo != null) {
			//		unitNo = vm.tenant.unitNo;
			//	}
			//	if (vm.tenant.streetNumber != null) {
			//		streetNo = vm.tenant.streetNumber;
			//	}
			//	if (vm.tenant.streetName != null) {
			//		streetName = vm.tenant.streetName;
			//	}
			//	if (vm.tenant.streetType != null) {
			//		streetType = vm.tenant.streetType;
			//	}
			//	vm.tenant.streetAddress = unittype + " " + unitNo + " " + streetNo + " " + streetName + " " + streetType;
			//};



			vm.save = function () {
				vm.saving = true;
				vm.tenant.streetNumber = $("#street_number").val();
				vm.tenant.streetName = $("#routeName").val();
				vm.tenant.streetType = $("#routeType").val();
				vm.tenant.suburb = $("#locality").val();
				var value = vm.tenant.suburb.split("|");
				vm.tenant.suburb = value[0].trim();
				vm.tenant.state = $("#administrative_area_level_1").val();
				vm.tenant.postCode = $("#postal_code").val();

				var unittype = "";
				var unitNo = "";
				var streetNo = "";
				var streetName = "";
				var streetType = "";
				if (vm.tenant.unitType != null) {
					unittype = vm.tenant.unitType;
				}
				if (vm.tenant.unitNo != null) {
					unitNo = vm.tenant.unitNo;
				}
				if (vm.tenant.streetNumber != null) {
					streetNo = vm.tenant.streetNumber;
				}
				if (vm.tenant.streetName != null) {
					streetName = vm.tenant.streetName;
				}
				if (vm.tenant.streetType != null) {
					streetType = vm.tenant.streetName;
				}
				vm.tenant.streetAddress = unittype + " " + unitNo + " " + streetNo + " " + streetName + " " + streetType;
				if (vm.setRandomPassword) {
					vm.tenant.adminPassword = null;
				}

				if (vm.tenant.editionId == 0) {
					vm.tenant.editionId = null;
				}


				tenantService.createTenant(vm.tenant)
					.then(function () {
						$state.go('host.tenants');
						abp.notify.info(app.localize('SavedSuccessfully'));
						//$uibModalInstance.close();
					}).finally(function () {
						vm.saving = false;
					});
			};

			vm.cancel = function () {
				$state.go('host.tenants');
				//$uibModalInstance.dismiss();
			};

			commonLookupService.getEditionsForCombobox({}).then(function (result) {
				vm.editions = result.data.items;
				vm.editions.unshift({ value: "0", displayText: app.localize('NotAssigned') });

				commonLookupService.getDefaultEditionName({}).then(function (result) {
					var defaultEdition = _.where(vm.editions, { displayText: result.data });
					if (defaultEdition && defaultEdition.length) {
						vm.tenant.editionId = parseInt(_.where(vm.editions, { displayText: 'Standard' })[0].value);
					}
				});
			});

			vm.test = function () {
				if (vm.tenant.tenancyName != null)
					vm.tenant.connectionString = "Server=108.60.219.120; Database=" + vm.tenant.tenancyName + ";Trusted_Connection=True;";
				else
					vm.tenant.connectionString = "Server=108.60.219.120; Database=;Trusted_Connection=True;";
			};
			vm.test();

			function init() {
				//tenantService.getUnitType().then(function (result) {
				//	vm.unitTypes = result.data.items;
				//	vm.flags = result.data.flags;
				//	setTimeout(function () {
				//		$('#unitTypeSelectionCombobox').selectpicker('refresh');
				//		$('#unitTypeSelectionCombobox').selectpicker('refresh');
				//	}, 0);
				//	vm.loading = false;
				//});

				//tenantService.getStreetName().then(function (result) {
				//	vm.streetNames = result.data.items;
				//	vm.flags = result.data.flags;
				//	setTimeout(function () {
				//		$('#streetNameSelectionCombobox').selectpicker('refresh');
				//		$('#streetNameSelectionCombobox').selectpicker('refresh');
				//	}, 0);
				//	vm.loading = false;
				//});

				//tenantService.getStreetType().then(function (result) {
				//	vm.streetTypes = result.data.items;
				//	vm.flags = result.data.flags;
				//	setTimeout(function () {
				//		$('#streetTypeSelectionCombobox').selectpicker('refresh');
				//		$('#streetTypeSelectionCombobox').selectpicker('refresh');
				//	}, 0);
				//	vm.loading = false;
				//});

				//tenantService.getSuburb().then(function (result) {
				//	vm.suburbs = result.data.items;
				//	vm.flags = result.data.flags;
				//	setTimeout(function () {
				//		$('#suburbSelectionCombobox').selectpicker('refresh');
				//		$('#suburbSelectionCombobox').selectpicker('refresh');
				//	}, 0);
				//	vm.loading = false;
				//});
			}

			init();

			//vm.getSuburb = function () {
			//	if (vm.tenant.suburb.length > 1) {
			//		tenantService.autoCompleteSuburb(vm.tenant.suburb).then(function (result) {
			//			vm.postCodes = result.data.items;
			//			vm.loading = false;
			//		});
			//	}
			//};

			vm.getUnitTypeList = function () {
				tenantService.getUnitType({
					text: vm.tenant.unitType
				}).then(function (result) {
					$(".UnitTypetypeahead").typeahead({ source: result.data });
				});
			};
			vm.getStreetNameList = function () {
				tenantService.getStreetNames({
					text: vm.tenant.streetName
				}).then(function (result) {
					$(".StreetNametypeahead").typeahead({ source: result.data });
				});
			};
			vm.getStreetTypeList = function () {
				tenantService.getStreetType({
					text: vm.tenant.streetType
				}).then(function (result) {
					$(".StreetTypetypeahead").typeahead({ source: result.data });
				});
			};
			vm.getSuburbList = function () {
				tenantService.getAllPostCodes({
					text: vm.suburb
				}).then(function (result) {
					$(".Fnametypeahead").typeahead({ source: result.data, onSelect: displayStatePostcode });
				});
			};
			function displayStatePostcode(item) {
				var value = item.text.split("|");
				vm.tenant.suburb = value[0].trim();
				vm.tenant.Suburb_Id = item.value;
				vm.tenant.state = value[1].trim();
				vm.tenant.postCode = value[2].trim();
			}
			vm.google = function () {
				vm.IsGoogleAddress = true;
			};
			vm.manual = function () {
				vm.IsGoogleAddress = false;
			};
			vm.getStreetNameList();
			vm.getSuburbList();
			vm.getUnitTypeList();
			vm.getStreetTypeList();
		}
	]);
})();