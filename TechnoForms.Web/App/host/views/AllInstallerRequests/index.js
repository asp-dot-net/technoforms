﻿(function () {
    appModule.controller('host.views.allInstallerRequests.index', [
        '$scope', '$uibModal', '$state','uiGridConstants', 'abp.services.app.adminInvitation',
        function ($scope, $uibModal, $state, uiGridConstants, adminInvitationService) {
            var vm = this;
            vm.norecord = false;
            vm.invitationlist = [];
            vm.editingInvitation = null;
            vm.invitationedit = [];
            vm.pageStatus = 1;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.admininvitationGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        enableColumnMenu: false,               
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',
                        width: 80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-click="grid.appScope.detailInvitation(row.entity)">' + app.localize('Detail') + '</a></li>' +
                            
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Installer'),
                        field: 'fullName',
                        enableColumnMenu: false,                                      
                        minWidth: 150
                    },
                    {
                        name: app.localize('Customer'),
                        field: 'custName',
                        enableColumnMenu: false,                                     
                        minWidth: 160
                    },
                    {
                        name: app.localize('ApprovedByAdmin'),
                        enableSorting: false,
                        enableColumnMenu: false,               
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',
                        width: 130,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.request_status == 0" class="label label-default">' + app.localize('Pending') + '</span>' +
                            '  <span ng-show="row.entity.request_status == 1" class="label label-success">' + app.localize('Approved') + '</span>' +
                            '  <span ng-show="row.entity.request_status == 2" tooltip-placement="left" uib-tooltip="{{row.entity.rejectReason}}" class="label label-danger">' + app.localize('Rejected') + '</span>' +
                            '</div>',
                        cellClass: 'centeralign'
                    },
                    {
                        name: app.localize('ApprovedByInstaller'),
                        enableSorting: false,
                        enableColumnMenu: false,               
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',
                        width: 140,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <span ng-show="row.entity.inst_Request_status == 0" class="label label-default">' + app.localize('Pending') + '</span>' +
                            '  <span ng-show="row.entity.inst_Request_status == 1" class="label label-success">' + app.localize('Approved') + '</span>' +
                            '  <span ng-show="row.entity.inst_Request_status == 2" tooltip-placement="left" uib-tooltip="{{row.entity.inst_RejectReason}}" class="label label-danger">' + app.localize('Rejected') + '</span>' +
                            '</div>',
                        cellClass: 'centeralign'
                    },
                    {
                        name: app.localize('InstallerExpiryCEC'),
                        field: 'installerExpiry',
                        enableColumnMenu: false,               
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',
                        width: 270,
                        enableSorting: false
                    },
                    {
                        name: app.localize('ExpiryDate'),
                        field: 'insAccreExDate',
                        enableColumnMenu: false,               
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',                      
                        enableSorting: false,
                        width: 120,
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        enableColumnMenu: false,               
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',                     
                        width:140,
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getinvitationList();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getinvitationList();
                    });
                },
                data: []
            };

            vm.getinvitationList = function () {

                vm.loading = true;
                adminInvitationService.getAdminInvitation({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    pageStatus: vm.pageStatus,
                    adminStatus: vm.adminStatus,
                    installerStatus: vm.installerStatus,
                    instName: vm.instName,
                    custName: vm.custName
                }).then(function (result) {
                    vm.admininvitationGridOptions.totalItems = result.data.totalCount;
                    vm.admininvitationGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };
            
            vm.detailInvitation = function (adminInvitation) {
                $state.go('allInstReqDetail', {
                    InstallerId: adminInvitation.id
                });
                //var modalInstance = $uibModal.open({
                //    templateUrl: '/App/host/views/adminInvitation/detailInstallerModel.cshtml',
                //    controller: 'host.views.adminInvitation.detailInstallerModel as vm',
                //    backdrop: 'static',
                //    resolve: {
                //        invitationId: function () {
                //            return adminInvitation.id;                            
                //        }
                //    }
                //});
                //modalInstance.result.then(function (result) {
                //    vm.getinvitationList();
                //});
            };
            vm.approveInvitation = function (adminInvitation) {
                abp.message.confirm(
                    app.localize('AreYouSureToApproveinvitation', adminInvitation.fullName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            adminInvitationService.approveAdminInvitation({
                                id: adminInvitation.id
                            }).then(function () {
                                abp.notify.success(app.localize('InstallerApprovedSuccessfully'));
                                vm.getinvitationList();
                            });
                        }
                    }
                );
            };

            vm.rejectInvitation = function (adminInvitation) {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/adminInvitation/rejectModel.cshtml',
                    controller: 'host.views.adminInvitation.rejectModel as vm',
                    backdrop: 'static',
                    resolve: {
                        invitationId: function () {
                            return adminInvitation.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getinvitationList();
                });
            };

            function Init() {
                adminInvitationService.getInvitationStatus().then(function (result) {
                    vm.statusNames = result.data.items;
                    vm.flags = result.data.flags;
                    setTimeout(function () {
                        $('#inststatusSelectionCombobox').selectpicker('refresh');
                        $('#inststatusSelectionCombobox').selectpicker('refresh');
                    }, 0);
                    vm.loading = false;
                });
                adminInvitationService.getInvitationStatus().then(function (result) {
                    vm.statusNames = result.data.items;
                    vm.flags = result.data.flags;
                    setTimeout(function () {
                        $('#adminstatusSelectionCombobox').selectpicker('refresh');
                        $('#adminstatusSelectionCombobox').selectpicker('refresh');
                    }, 0);
                    vm.loading = false;
                });
            }

            Init();
            vm.getinvitationList();

            vm.ClearAll = function () {
                vm.instName = null;
                vm.custName = null;
                vm.adminStatus = null;
                vm.installerStatus = null;

                setTimeout(function () {
                    $('#adminstatusSelectionCombobox').selectpicker('refresh');
                    $('#inststatusSelectionCombobox').selectpicker('refresh');
                }, 0);

                vm.getinvitationList();
            };

        }
    ]);
})();