﻿(function () {
    appModule.controller('host.views.dashboard.index', [
        '$scope', 'uiGridConstants', 'abp.services.app.adminDashboard',
        function ($scope, uiGridConstants, adminDashboardService) {
            var vm = this;
            vm.jobList = [];

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            init();
            function init() {
               
            }
        }
    ]);
})();