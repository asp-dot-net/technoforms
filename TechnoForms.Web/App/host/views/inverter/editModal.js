﻿(function () {
	appModule.controller('host.views.inverter.editModal', [
		'$scope', '$state', '$stateParams', '$uibModalInstance', 'FileUploader', 'abp.services.app.inverter', 'Id',
		function ($scope, $state, $stateParams, $uibModalInstance, fileUploader, inverterService, Id) {
			var vm = this;

			vm.saving = false;
			vm.inverter = null;
			vm.editions = [];
			vm.inverterId = $stateParams.Id;

			//DatePIcker Code Start
			$scope.today = function () {
				$scope.dt = new Date();
			};
			//$scope.today();

			$scope.clear = function () {
				$scope.dt = null;
			};

			$scope.inlineOptions = {
				customClass: getDayClass,
				minDate: new Date(),
				showWeeks: true
			};
			//disable past dates
			$scope.dateOptions = {
				//dateDisabled: disabled,
				formatYear: 'yy',
				//maxDate: new Date(2020, 5, 22),
				minDate: new Date(),
				startingDay: 1
			};
			//Enable all dates
			$scope.dateOptionsApproval = {
				//dateDisabled: disabled,
				formatYear: 'yy',
				startingDay: 1
			};
			// Disable weekend selection
			function disabled(data) {
				var date = data.date,
					mode = data.mode;
				return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
			}

			//$scope.toggleMin = function () {
			//    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
			//    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
			//};

			//$scope.toggleMin();

			$scope.open1 = function () {
				$scope.popup1.opened = true;
			};

			$scope.open2 = function () {
				$scope.popup2.opened = true;
			};

			$scope.setDate = function (year, month, day) {
				$scope.dt = new Date(year, month, day);
			};

			$scope.formats = ['d/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			$scope.format = $scope.formats[0];
			$scope.altInputFormats = ['M!/d!/yyyy'];

			$scope.popup1 = {
				opened: false
			};

			$scope.popup2 = {
				opened: false
			};

			var tomorrow = new Date();
			tomorrow.setDate(tomorrow.getDate() + 1);
			var afterTomorrow = new Date();
			afterTomorrow.setDate(tomorrow.getDate() + 1);
			$scope.events = [
				{
					date: tomorrow,
					status: 'full'
				},
				{
					date: afterTomorrow,
					status: 'partially'
				}
			];

			function getDayClass(data) {
				var date = data.date,
					mode = data.mode;
				if (mode === 'day') {
					var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

					for (var i = 0; i < $scope.events.length; i++) {
						var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

						if (dayToCheck === currentDay) {
							return $scope.events[i].status;
						}
					}
				}

				return '';
			}
			//DatePIcker Code End

			vm.openNewTab = function (test) {
				window.open(abp.appPath + 'Temp/InverterLogo/' + test + '?v=' + new Date().valueOf(), '_blank');
			};

			vm.logouploader = new fileUploader({
				url: abp.appPath + 'File/InverterLogo',
				headers: {
					"X-XSRF-TOKEN": abp.security.antiForgery.getToken()
				},
				queueLimit: 1,
				autoUpload: true,
				removeAfterUpload: true,
				filters: [{
					name: 'imageFilter',
					fn: function (item, options) {
						//File type check
						var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
						if ('|jpg|jpeg|png|gif|'.indexOf(type) === -1) {
							abp.message.warn(app.localize('ProfilePicture_Warn_FileType'));
							return false;
						}

						//File size check
						if (item.size > 1048576) //1MB
						{
							abp.message.warn(app.localize('ProfilePicture_Warn_SizeLimit'));
							return false;
						}

						return true;
					}
				}]
			});

			vm.logouploader.onSuccessItem = function (fileItem, response, status, headers) {
				if (response.success) {
					var profileFilePath = abp.appPath + 'Temp/Downloads/' + response.result.fileName + '?v=' + new Date().valueOf();
					vm.logoFileName = response.result.fileName;

				} else {
					abp.message.error(response.error.message);
				}
			};

			vm.save = function () {
				vm.saving = true;
				vm.inverter.inverterLogo = vm.logoFileName;
				inverterService.checkInverterExists({ ModelNo: vm.inverter.modelNumber, Id: Id }).then(function (result) {
					if (result.data.status == true) {
						inverterService.updateInverter(vm.inverter)
							.then(function () {
								//$state.go('host.inverter');
								abp.notify.info(app.localize('SavedSuccessfully'));
								$uibModalInstance.close();
							}).finally(function () {
								vm.saving = false;
							});
					}
					else {
						abp.notify.info(app.localize('RecordAlreadyExists'));
						//vm.message = result.data.message;
						//vm.exist = true;
						vm.saving = false;
					}
				});
			};

			vm.cancel = function () {
				//$state.go('host.inverter');
				$uibModalInstance.dismiss();
			};

			function init() {
				inverterService.getInverterForEdit({
					id: Id
				}).then(function (result) {
					vm.inverter = result.data;
					vm.logoFileName = result.data.inverterLogo;
					vm.inverter.approvalDate = new Date(vm.inverter.approvalDate);
					vm.inverter.expiryDate = new Date(vm.inverter.expiryDate);
				});
			}
			init();
		}
	]);
})();