﻿(function () {
    appModule.controller('host.views.inverter.inverterBulkUpload', [
        '$scope', '$state', '$http', '$rootScope', '$uibModalInstance', 'abp.services.app.inverter',
        function ($scope, $state, $http, $rootScope, $uibModalInstance, inverterService) {
            var vm = this;

            vm.saving = false;
            vm.inverter = null;
            
            vm.uploadFile = function (file) {
                vm.saving = true;
                var files = $('#filetoupload')[0].files[0];
                var uploadUrl = "../BulkDataUpload/InverterData";
                var fd = new FormData();
                fd.append('file', files);
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).then(function () {
                    $scope.$parent.$emit('eventDataImported', null);
                    abp.notify.success(app.localize('SavedSuccessfully'));
                    $uibModalInstance.dismiss();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {
                inverterService.getInverterBulkUpload({
                }).then(function (result) {
                    vm.inverter = result.data;
                });
            }
            init();

        }
    ]);
})();