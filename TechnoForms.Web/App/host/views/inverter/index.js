﻿(function () {
    appModule.controller('host.views.inverter.index', [
        '$scope', '$state', '$http', '$rootScope','$uibModal', 'uiGridConstants', 'abp.services.app.inverter',
        function ($scope, $state, $http, $rootScope, $uibModal, uiGridConstants, inverterService) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;

            vm.permissions = {
                createInverter: abp.auth.hasPermission('Pages.Masters.Inverter.Create'),
                editInverter: abp.auth.hasPermission('Pages.Masters.Inverter.Edit'),
                deleteInverter: abp.auth.hasPermission('Pages.Masters.Inverter.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


              //DatePIcker Code Start
              $scope.today = function () {
                $scope.dt = new Date();
            };
            //$scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            ////Disable past dates
            //$scope.dateOptions = {
            //    //dateDisabled: disabled,
            //    formatYear: 'yy',
            //    maxDate: new Date(2020, 5, 22),
            //    minDate: new Date(),
            //    startingDay: 1
            //};

            // Disable weekend selection
           
            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['d/MM/yyyy','dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.altInputFormats = ['M!/d!/yyyy'];

            $scope.popup2 = {
                opened: false
            };

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }
            //DatePIcker Code End
            

            vm.inverterGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        enableColumnMenu: false,               
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',
                        width:80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.editInverter" ng-click="grid.appScope.editInverter(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.deleteInverter" ng-click="grid.appScope.deleteInverter(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('Manufacturer'),
                        field: 'manufacturarName',
                        enableColumnMenu: false,                                      
                        minWidth:200
                    },
                    {
                        name: app.localize('Series'),
                        enableColumnMenu: false,             
                        field: 'series',
                        minWidth:200
                                    
                    },
                    {
                        name: app.localize('ModelNumber'),
                        enableColumnMenu: false,             
                        field: 'modelNumber',
                        headerCellClass:'centeralign',
                        cellClass:'centeralign',
                        minWidth:200
                    },
                    {
                        name: app.localize('acPower'),
                        enableColumnMenu: false,             
                        field: 'acPower',
                        cellClass:'centeralign',
                        width: 110
                    },
                    {
                        name: app.localize('ApprovalDate'),
                        enableColumnMenu: false,  
                        field: 'approvalDate',
                        width: 140,
                        headerCellClass:'centeralign',
                        cellClass:'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    },
                    {
                        name: app.localize('ExpiryDate'),
                        enableColumnMenu: false,  
                        field: 'expiryDate',
                        width: 120,
                        headerCellClass:'centeralign',
                        cellClass: 'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getInverter();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getInverter();
                    });
                },
                data: []
            };

            if (!vm.permissions.editInverter && !vm.permissions.deleteInverter) {
                vm.inverterGridOptions.columnDefs.shift();
            }

            vm.getInverter = function () {
                vm.loading = true;
                inverterService.getInverter({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    manufacturarName: vm.manufacturarText,
                    ModelNumber: vm.modelNumber,
                    ExpiryDate: vm.expiryDate
                }).then(function (result) {
                    vm.inverterGridOptions.totalItems = result.data.totalCount;
                    vm.inverterGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createInverter = function () {
                //$state.go('createInverter', {
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/inverter/createModel.cshtml',
                    controller: 'host.views.inverter.createModel as vm',
                    backdrop: 'static'
                });
                modalInstance.result.then(function () {
                    vm.getInverter();
                });
            };

            vm.uploadRec = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/inverter/inverterBulkUpload.cshtml',
                    controller: 'host.views.inverter.inverterBulkUpload as vm',
                    scope: $scope,
                    backdrop: 'static'
                });

                $scope.$on('eventDataImported', function (event, data) {
                    vm.getInverter();
                });
            };

            vm.deleteInverter = function (inverter) {
                abp.message.confirm(
                    app.localize('InverterDeleteWarningMessage', inverter.modelNumber),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            inverterService.deleteInverter({
                                id: inverter.id
                            }).then(function () {
                                vm.getInverter();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.editInverter = function (inverter) {
                //$state.go('editInverter', {
                //    Id: inverter.id
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/inverter/editModal.cshtml',
                    controller: 'host.views.inverter.editModal as vm',
                    backdrop: 'static',
                    resolve: {
                        Id: function () {
                            return inverter.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getInverter();
                });
            };

            vm.ClearAll = function () {
                vm.manufacturarText = null;
                vm.modelNumber = null;
                vm.expiryDate = null;
                vm.getInverter();
            };
            vm.getInverter();
        }
    ]);
})();