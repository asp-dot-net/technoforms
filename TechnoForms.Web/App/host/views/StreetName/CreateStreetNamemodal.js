﻿(function () {
    appModule.controller('host.views.streetname.createStreetNamemodal', [
        '$scope', '$state','$uibModalInstance', 'abp.services.app.streetName',
        function ($scope, $state, $uibModalInstance, streetNameService) {
            var vm = this;

            vm.saving = false;
            vm.streetNamelist = {};
            vm.exist = false;

            vm.save = function () {
                vm.saving = true;

                streetNameService.checkStreetNameExists({ StreetName: vm.streetNamelist.StreetName }).then(function (result) {
                    if (result.data.status == true) {
                        streetNameService.createStreetName(vm.streetNamelist).then(function () {
                            //$state.go('host.streetname', {});
                            abp.notify.info(app.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }).finally(function () {
                            vm.saving = false;
                        });
                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        //vm.message = result.data.message;
                        //vm.exist = true;
                        vm.saving = false;
                    }
                });
            };

            vm.cancel = function () {
                //$state.go('host.streetname', {});
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();
