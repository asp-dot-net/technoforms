﻿(function () {
    appModule.controller('host.views.streetname.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.streetName',
        function ($scope, $state, $uibModal, uiGridConstants, streetNameService) {
            var vm = this;
            vm.norecord = false;
            vm.streetNamelist = [];
            vm.editingStreetName = null;
            vm.streetNameedit = [];
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.permissions = {
                createstreetname: abp.auth.hasPermission('Pages.Masters.StreetName.Create'),
                deleteStreetName: abp.auth.hasPermission('Pages.Masters.StreetName.Delete'),
                editStreetName: abp.auth.hasPermission('Pages.Masters.StreetName.Edit')
            };

            vm.streetNameGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        enableColumnMenu: false,
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',
                        width: 80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.editStreetName" ng-click="grid.appScope.editStreetName(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.deleteStreetName" ng-click="grid.appScope.deleteStreetName(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('streetName'),
                        enableColumnMenu: false,
                        field: 'streetName',
                        minWidth:150,
                    },
                    {
                        name: app.localize('CreationTime'),
                        enableColumnMenu: false,
                        cellClass:'centeralign',
                        headerCellClass:'centeralign',
                        field: 'creationTime',
                        width: 170,
                        cellClass: 'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getStreet();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getStreet();
                    });
                },
                data: []
            };

            vm.getStreet = function () {
                vm.loading = true;
                streetNameService.getStreetName({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).then(function (result) {
                    vm.streetNameGridOptions.totalItems = result.data.totalCount;
                    vm.streetNameGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            //vm.getStreet = function () {
            //    streetNameService.getStreetName().then(function (result) {
            //        vm.streetNamelist = result.data.items;
            //    });
            //};
            if (!vm.permissions.editStreetName && !vm.permissions.deleteStreetName) {
                vm.streetNameGridOptions.columnDefs.shift();
            }

            //start from here...
            vm.openCreateStreetNameModal = function () {
                //$state.go('createStreetName', {
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '~/app/host/views/streetname/createStreetNamemodal.cshtml',
                    controller: 'host.views.streetname.createStreetNamemodal as vm',
                    backdrop: 'static'
                });

                modalInstance.result.then(function () {
                    vm.getStreet();
                });
            };

            vm.editStreetName = function (streetname) {
                //$state.go('editStreetName', {
                //    Id: streetname.id
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/streetname/editStreetNamemodal.cshtml',
                    controller: 'host.views.streetname.editStreetNamemodal as vm',
                    backdrop: 'static',
                    resolve: {
                        Id: function () {
                            return streetname.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getStreet();
                });
            };

            vm.deleteStreetName = function (streetName) {
                abp.message.confirm(
                    app.localize('AreYouSureToDeleteStreetName', streetName.streetName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            streetNameService.deleteStreetName({
                                id: streetName.id
                            }).then(function () {
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                                vm.getStreet();
                            });
                        }
                    }
                );
            };

            vm.getStreet();

        }
    ]);
})();