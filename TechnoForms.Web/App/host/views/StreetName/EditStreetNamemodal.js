﻿(function () {
    appModule.controller('host.views.streetname.editStreetNamemodal', [
        '$scope', '$state', '$stateParams','$uibModalInstance', 'abp.services.app.streetName','Id',
        function ($scope, $state, $stateParams, $uibModalInstance, streetNameService,Id) {
            var vm = this;

            vm.saving = false;
            vm.streetname = null;
            vm.editions = [];
            vm.streetnameId = $stateParams.Id;
            vm.exist = false;

            vm.save = function () {
                vm.saving = true;
                streetNameService.checkStreetNameExists({ StreetName: vm.streetname.streetName, Id: Id }).then(function (result) {
                    if (result.data.status == true) {
                        streetNameService.updateStreetName(vm.streetname)
                            .then(function () {
                                //$state.go('host.streetname', {});
                                abp.notify.info(app.localize('SavedSuccessfully'));
                                $uibModalInstance.close();
                            }).finally(function () {
                                vm.saving = false;
                            });
                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        //vm.message = result.data.message;
                        //vm.exist = true;
                        vm.saving = false;
                    }
                });
            };

            vm.cancel = function () {
                //$state.go('host.streetname', {});
                $uibModalInstance.dismiss();
            };

            function init() {
                streetNameService.getStreetNameForEdit({
                    id: Id
                }).then(function (result) {
                    vm.streetname = result.data;
                });
            }
            init();
        }
    ]);
})();