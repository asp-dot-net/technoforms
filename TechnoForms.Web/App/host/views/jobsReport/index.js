﻿(function () {
    appModule.controller('host.views.jobsReport.index', [
        '$scope', '$uibModal', 'uiGridConstants', 'abp.services.app.jobs',
        function ($scope, $uibModal, uiGridConstants, jobsService) {
            var vm = this;

            var vm = this;

            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            //start from here...
            vm.loading = false;
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.jobsGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('FirstName'),
                        field: 'firstName'
                    },
                    {
                        name: app.localize('LastName'),
                        field: 'lastName'
                    },
                    {
                        name: app.localize('CreationTime'),
                        field: 'creationTime',
                        width:130,
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getJobs();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getJobs();
                    });
                },
                data: []
            };

            vm.getJobs = function () {
                vm.loading = true;
                jobsService.getAllJobs({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filter: vm.filterText
                }).then(function (result) {
                    vm.jobsGridOptions.totalItems = result.data.totalCount;
                    vm.jobsGridOptions.data = result.data.items;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            //vm.detailJobs = function (jobs) {
            //    var modalInstance = $uibModal.open({
            //        templateUrl: '/App/tenant/views/jobs/detailModal.cshtml',
            //        controller: 'tenant.views.jobs.detailModal as vm',
            //        backdrop: 'static',
            //        resolve: {
            //            jobId: function () {
            //                return jobs.id;
            //            }
            //        }
            //    });
            //    modalInstance.result.then(function (result) {
            //        vm.getJobs();
            //    });
            //};

            vm.getJobs();

        }
    ]);
})();