﻿(function () {
    appModule.controller('host.views.stc.index', [
        '$scope', '$state', '$uibModal', 'uiGridConstants', 'abp.services.app.sTC', 'abp.services.app.state',
        function ($scope, $state, $uibModal, uiGridConstants, stcService, stateService) {
            var vm = this;
            vm.norecord = false;
            vm.streetStateList = [];
            vm.installerList = [];
            vm.systemTypeList = [];
            vm.stcapplied = [];
            vm.pvdstatus = [];
            vm.tenantlist = [];
            vm.totalstc = 0;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.stcGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Detail'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        cellClass: 'centeralign',
                        headerCellClass: 'centeralign',
                        width: 100,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '<a class="btn btn-xs btn-primary blue" ng-click="grid.appScope.detailJobs(row.entity)">' + app.localize('View') + '</a>'
                            + '</div>'
                    },
                    {
                        name: app.localize('jobid'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'formId',
                        minWidth: 100
                    },
                    {
                        name: app.localize('PVDNumber'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'pvdNumber',
                        minWidth: 130
                    },
                    {
                        name: app.localize('PVDStatus'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'pvdStatus',
                        minWidth: 130
                    },
                    {
                        name: app.localize('RECCreateDate'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'stcAppliedDate',
                        cellClass: 'centeralign',
                        headerCellClass: 'centeralign',
                        width: 170,
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    },
                    {
                        name: app.localize('ProjectNumber'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'projectNumber',
                        minWidth: 150
                    },
                    {
                        name: app.localize('STCPage'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'totalSTC',
                        minWidth: 50
                    },
                    {
                        name: app.localize('Owner'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'owner',
                        minWidth: 150
                    },
                    {
                        name: app.localize('State'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'streetState',
                        minWidth: 80
                    },                    
                    {
                        name: app.localize('Installer'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'installerName',
                        minWidth: 150
                    },
                    {
                        name: app.localize('InstallDate'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'expectedInstallDate',
                        cellClass: 'centeralign',
                        headerCellClass: 'centeralign',
                        width: 170,
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    },
                    {
                        name: app.localize('Phone'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        field: 'phone',
                        minWidth: 120
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getstc();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getstc();
                    });
                },
                data: []
            };

            vm.detailJobs = function (jobs) {
                $state.go('detailstcJob', {
                    jobId: jobs.id
                });
            };

            vm.getstc = function () {
                vm.loading = true;
                stcService.getSTCList({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    filterText: vm.filterText,
                    filterFormId: vm.filterFormId,
                    filterOwnerName: vm.filterOwnerName,
                    filterState: vm.filterState,
                    filterInstallerId: vm.filterInstallerId,
                    filterSystemtypeId: vm.filterSystemtypeId,
                    filterStartDate: vm.filterStartDate,
                    filterEndDate: vm.filterEndDate,
                    filterDateypeId: vm.filterDateypeId,
                    filterPVDNumber: vm.filterPVDNumber,
                    filterStcApplied: vm.filterStcApplied,
                    filterPVDNumberval: vm.filterPVDNumberval,
                    filterPVDstatusval: vm.filterPVDstatusval,
                    filterTenantvalId: vm.filterTenantvalId
                }).then(function (result) {
                    vm.totalstc = 0;
                    for (i = 0; i < result.data.items.length; i++) {
                        vm.totalstc += result.data.items[i].totalSTC;
                    }
                    vm.stcGridOptions.totalItems = result.data.totalCount;
                    vm.stcGridOptions.data = result.data.items;
                    if (result.data.items.length == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            stateService.getState().then(function (result) {
                vm.streetStateList = result.data.items;
                setTimeout(function () {
                    $('#streetStateSelectionCombobox').selectpicker('refresh');
                    $('#streetStateSelectionCombobox').selectpicker('refresh');
                }, 0);
                vm.jobs.streetState = result.data.items[0].stateName;
                vm.loading = false;
            });

            stcService.getSystemType().then(function (result) {
                vm.systemTypeList = result.data.items;
                setTimeout(function () {
                    $('#systemTypeSelectionCombobox').selectpicker('refresh');
                    $('#systemTypeSelectionCombobox').selectpicker('refresh');
                }, 0);
                vm.loading = false;
            });

            stcService.getTenantList().then(function (result) {
                vm.tenantlist = result.data.items;
                setTimeout(function () {
                    $('#tenantSelectionCombobox').selectpicker('refresh');
                    $('#tenantSelectionCombobox').selectpicker('refresh');
                }, 0);
                vm.loading = false;
            });
            vm.getInstaller = function () {
                if (vm.filterTenantvalId != null && vm.filterTenantvalId != 0) {
                    stcService.getAllInstaller({ Id: vm.filterTenantvalId }).then(function (result) {
                        vm.installerList = result.data.items;
                        setTimeout(function () {
                            $('#createforSelectionCombobox').selectpicker('refresh');
                            $('#createforSelectionCombobox').selectpicker('refresh');
                        }, 0);
                        vm.loading = false;
                    });
                }
            };
            vm.datetype = [
                {
                    id: '1',
                    name: 'Install Date'
                }, {
                    id: '2',
                    name: 'STC Created Date'
                }];
            vm.stcapplied = [{
                id: 'Yes',
                name: 'Yes'
            }, {
                id: 'No',
                name: 'No'
            }];

            vm.pvdstatus = [{
                id: 'Approved',
                name: 'Approved'
            }, {
                id: 'Failed',
                name: 'Failed'
            }, {
                id: 'New',
                name: 'New'
            }, {
                id: 'Pending',
                name: 'Pending'
            }];

            vm.ClearAll = function () {
                vm.filterFormId = null;
                vm.filterOwnerName = null;
                vm.filterState = null;
                vm.filterEndDate = "";
                vm.filterStartDate = "";
                vm.filterPVDNumber = null;
                vm.filterPVDstatusval = null;

                vm.filterInstallerId = null;
                vm.filterSystemtypeId = null;
                vm.filterStcApplied = null;
                vm.filterPVDNumberval = null;
                vm.filterDateypeId = null;
                vm.filterTenantvalId = null;
                vm.filterPVDstatusval = null;

                //vm.filterInstallerId = 0;
                //vm.filterSystemtypeId = 0;
                //vm.filterStcApplied = 0;
                //vm.filterPVDNumberval = 0;
                //vm.filterDateypeId = 0;
                //vm.filterTenantvalId = 0;

                setTimeout(function () {
                    $('#createforSelectionCombobox').selectpicker('refresh');
                    $('#streetStateSelectionCombobox').selectpicker('refresh');
                    $('#systemTypeSelectionCombobox').selectpicker('refresh');
                    $('#systemstcappliedSelectionCombobox').selectpicker('refresh');
                    $('#systempvdnumberSelectionCombobox').selectpicker('refresh');
                    $('#systemDateTypeSelectionCombobox').selectpicker('refresh');
                    $('#tenantSelectionCombobox').selectpicker('refresh');
                    $('#systempvdstatusSelectionCombobox').selectpicker('refresh');
                    
                }, 0);

                vm.getstc();
            };
            vm.getstc();
        }
    ]);
})();