﻿(function () {
    appModule.controller('host.views.collection.index', [
        '$scope', 'uiGridConstants', 'abp.services.app.collection',
        function ($scope, uiGridConstants, collectionService) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            //start from here...
            vm.loading = false;
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.collectionGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('CustomerName'),
                        enableColumnMenu: false,   
                        field: 'customerName',
                        minWidth:150,
                    },
                    {
                        name: app.localize('InvoiceNumber'),                       
                        field: 'invoiceNumber',
                        headerCellClass:'centeralign',
                        cellClass: 'centeralign',
                        minWidth:150,
                    },
                    {
                        name: app.localize('amount'),
                        field: 'amount',
                        enableColumnMenu: false,   
                        cellClass: 'rightalign',
                        width: 110
                    },
                    {
                        name: app.localize('ModeofpaymentName'),
                        enableColumnMenu: false,   
                        field: 'modeofpaymentName',
                        cellClass: 'centeralign',
                        width: 225
                    },
                    {
                        name: app.localize('InstrumentDetail'),
                        enableColumnMenu: false,   
                        field: 'instrumentDetail',
                        width: 175
                    },
                    {
                        name: app.localize('date'),
                        enableColumnMenu: false,   
                        field: 'date',
                        cellFilter: 'date:\'dd-MM-yyyy\'',
						//cellFilter: 'momentFormat: \'L\'',
                        width: 110,
                        headerCellClass:'centeralign',
                        cellClass: 'centeralign',
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getCollection();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getCollection();
                    });
                },
                data: []
            };

            vm.getCollection = function () {
                vm.loading = true;
                collectionService.getCollectionList({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                }).then(function (result) {
                    vm.collectionGridOptions.totalItems = result.data.totalCount;
                    vm.collectionGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getCollection();
        }
    ]);
})();