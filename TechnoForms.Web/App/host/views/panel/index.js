﻿(function () {
    appModule.controller('host.views.panel.index', [
        '$scope', '$state', '$rootScope', '$uibModal', 'uiGridConstants', 'abp.services.app.panel',
        function ($scope, $state, $rootScope, $uibModal, uiGridConstants, panelService) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });



            vm.loading = false;

            //$scope.uploadFile = function (file) {
            //    vm.loading = true;
            //    console.log('file is ');
            //    var file = $('#filetoupload')[0].files[0];
            //    console.dir($('#filetoupload')[0].files[0]);
            //    console.dir(file);
            //    var uploadUrl = "../BulkDataUpload/PanelData";
            //    var fd = new FormData();
            //    fd.append('file', file);

            //    $http.post(uploadUrl, fd, {
            //        transformRequest: angular.identity,
            //        headers: { 'Content-Type': undefined }
            //    }).then(function () {
            //        vm.getPanel();
            //        abp.notify.success(app.localize('SavedSuccessfully'));
            //    }).finally(function () {
            //        vm.loading = false;
            //    });
            //};

            vm.permissions = {
                createPanel: abp.auth.hasPermission('Pages.Masters.Panel.Create'),
                editPanel: abp.auth.hasPermission('Pages.Masters.Panel.Edit'),
                deletePanel: abp.auth.hasPermission('Pages.Masters.Panel.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };


            //DatePIcker Code Start
            $scope.today = function () {
                $scope.dt = new Date();
            };
            //$scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                //dateDisabled: disabled,
                formatYear: 'yy',
                //maxDate: new Date(2020, 5, 22),
                //minDate: new Date(),
                startingDay: 1
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            //$scope.toggleMin = function () {
            //    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            //    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            //};

            //$scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.open2 = function () {
                $scope.popup2.opened = true;
            };

            $scope.setDate = function (year, month, day) {
                $scope.dt = new Date(year, month, day);
            };

            $scope.formats = ['d/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.altInputFormats = ['M!/d!/yyyy'];

            $scope.popup1 = {
                opened: false
            };

            $scope.popup2 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }
            //DatePIcker Code End

            vm.panelGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableSorting: false,
                        enableColumnMenu: false,
                        headerCellClass: 'centeralign',
                        cellClass: 'centeralign',
                        width: 80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.editPanel" ng-click="grid.appScope.editPanel(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.deletePanel" ng-click="grid.appScope.deletePanel(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('LicenceHolder'),
                        field: 'licenceOrCertiHolder',
                        enableColumnMenu: false,
                        minWidth: 250
                    },
                    {
                        name: app.localize('ModelNumber'),
                        field: 'modelNo',
                        enableColumnMenu: false,
                        headerCellClass: 'centeralign',
                        cellClass: 'centeralign',
                        width: 200
                    },
                    {
                        name: app.localize('CECApprovedDate'),
                        field: 'cecApprovedDate',
                        enableColumnMenu: false,
                        width: 146,
                        headerCellClass: 'centeralign',
                        cellClass: 'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    },
                    {
                        name: app.localize('ExpiryDate'),
                        enableColumnMenu: false,
                        field: 'expiryDate',
                        width: 125,
                        headerCellClass: 'centeralign',
                        cellClass: 'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    },
                    {
                        name: app.localize('Size'),
                        field: 'size',
                        headerCellClass: 'centeralign',
                        cellClass: 'centeralign',
                        enableColumnMenu: false,
                        width: 70
                    },
                    {
                        name: app.localize('FireTested'),
                        enableColumnMenu: false,
                        field: 'fireTested',
                        width: 120
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getPanel();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getPanel();
                    });
                },
                data: []
            };

            if (!vm.permissions.editPanel && !vm.permissions.deletePanel) {
                vm.panelGridOptions.columnDefs.shift();
            }

            vm.getPanel = function () {
                vm.loading = true;
                panelService.getPanel({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    licenceOrCertiHolder: vm.licenceOrCertiHolder,
                    modelNo: vm.modelNo,
                    expiryDate: vm.expiryDate
                }).then(function (result) {
                    vm.panelGridOptions.totalItems = result.data.totalCount;
                    vm.panelGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createPanel = function () {
                //$state.go('createPanel', {
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/panel/createPanel.cshtml',
                    controller: 'host.views.panel.createPanel as vm',
                    backdrop: 'static'
                });
                modalInstance.result.then(function () {
                    vm.getPanel();
                });
            };

            vm.uploadRec = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/panel/panelBulkUpload.cshtml',
                    controller: 'host.views.panel.panelBulkUpload as vm',
                    scope: $scope,
                    backdrop: 'static'
                });
                $scope.$on('eventPanelDataImported', function (event, data) {
                    vm.getPanel();
                });
            };

            vm.deletePanel = function (panel) {
                abp.message.confirm(
                    app.localize('PanelDeleteWarningMessage', panel.modelNo),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            panelService.deletePanel({
                                id: panel.id
                            }).then(function () {
                                vm.getPanel();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.editPanel = function (panel) {
                //$state.go('editPanel', {
                //    Id: panel.id
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/panel/editPanel.cshtml',
                    controller: 'host.views.panel.editPanel as vm',
                    backdrop: 'static',
                    resolve: {
                        Id: function () {
                            return panel.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getPanel();
                });
            };

            vm.ClearAll = function () {
                vm.licenceOrCertiHolder = null;
                vm.modelNo = null;
                vm.expiryDate = null;
                vm.getPanel();
            };

            vm.getPanel();
        }
    ]);
})();