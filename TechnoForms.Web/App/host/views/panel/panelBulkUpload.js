﻿(function () {
    appModule.controller('host.views.panel.panelBulkUpload', [
        '$scope', '$state', '$http', '$rootScope', '$uibModalInstance', 'abp.services.app.panel',
        function ($scope, $state, $http, $rootScope, $uibModalInstance, panelService) {
            var vm = this;
            
            vm.saving = false;
            vm.panel = null;

            vm.uploadFile = function (file) {
                vm.saving = true;
                var files = $('#filetoupload')[0].files[0];
                var uploadUrl = "../BulkDataUpload/PanelData";
                var fd = new FormData();
                fd.append('file', files);
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).then(function () {
                    $scope.$parent.$emit('eventPanelDataImported', null);
                    abp.notify.success(app.localize('SavedSuccessfully'));
                    $uibModalInstance.dismiss();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.updatePanelSize = function (file) {
                vm.saving = true;
                var files = $('#filetoupload')[0].files[0];
                var uploadUrl = "../BulkDataUpload/UpdatePanelSize";
                var fd = new FormData();
                fd.append('file', files);
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).then(function () {
                    $scope.$parent.$emit('eventPanelDataImported', null);
                    abp.notify.success(app.localize('SavedSuccessfully'));
                    $uibModalInstance.dismiss();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            vm.updateData = function (file) {
                vm.saving = true;
                var files = $('#filetoupload')[0].files[0];
                var uploadUrl = "../BulkDataUpload/PanelDataUpdate";
                var fd = new FormData();
                fd.append('file', files);
                $http.post(uploadUrl, fd, {
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
                }).then(function () {
                    $scope.$parent.$emit('eventPanelDataImported', null);
                    abp.notify.success(app.localize('SavedSuccessfully'));
                    $uibModalInstance.dismiss();
                }).finally(function () {
                    vm.saving = false;
                });
            };
            
            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };

            function init() {
                panelService.getPanelBulkUpload({}).then(function (result) {
                    vm.panel = result.data;
                });
            }
            init();
        }
    ]);
})();