﻿(function () {
    appModule.controller('host.views.unittype.createModel', [
        '$scope', '$state','$uibModalInstance', 'abp.services.app.unitType',
        function ($scope, $state, $uibModalInstance, unitTypeService) {
            var vm = this;
            
            vm.saving = false;
            vm.exist = false;
            vm.message = "";
            vm.unitType = {};

            vm.save = function () {
                vm.saving = true;
                unitTypeService.checkUnitTypeExists({ UnitTypeName: vm.unitType.UnitTypeName }).then(function (result) {
                    if (result.data.status == true) {
                        unitTypeService.createUnitType(vm.unitType).then(function () {
                            //$state.go('host.unittype', {
                            //});
                            abp.notify.info(app.localize('SavedSuccessfully'));
                            $uibModalInstance.close();
                        }).finally(function () {
                            vm.saving = false;
                            $uibModalInstance.close();
                        });
                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        //vm.message = result.data.message;
                        //vm.exist = true;
                        vm.saving = false;
                    }
                });

            };

            vm.cancel = function () {
                //$state.go('host.unittype', {
                //});
                $uibModalInstance.dismiss({});
            };
        }
    ]);
})();