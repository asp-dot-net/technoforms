﻿(function () {
    appModule.controller('host.views.unittype.index', [
        '$scope', '$state', '$uibModal', '$http', 'uiGridConstants', 'abp.services.app.unitType',

        function ($scope, $state, $uibModal, $http, uiGridConstants, unitTypeService) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            vm.loading = false;

            vm.permissions = {
                createUnitType: abp.auth.hasPermission('Pages.Masters.UnitType.Create'),
                editUnitType: abp.auth.hasPermission('Pages.Masters.UnitType.Edit'),
                deleteUnitType: abp.auth.hasPermission('Pages.Masters.UnitType.Delete')
            };

            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.unitTypeGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('Actions'),
                        enableColumnMenu: false,
                        enableSorting: false,
                        cellClass: 'centeralign',
                        headerCellClass: 'centeralign',
                        width: 80,
                        cellTemplate:
                            '<div class=\"ui-grid-cell-contents\">' +
                            '  <div class="btn-group dropdown" uib-dropdown="" dropdown-append-to-body>' +
                            '    <button class="btn btn-xs btn-primary blue" uib-dropdown-toggle="" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> ' + app.localize('Actions') + ' <span class="caret"></span></button>' +
                            '    <ul uib-dropdown-menu>' +
                            '      <li><a ng-if="grid.appScope.permissions.editUnitType" ng-click="grid.appScope.editUnitType(row.entity)">' + app.localize('Edit') + '</a></li>' +
                            '      <li><a ng-if="grid.appScope.permissions.deleteUnitType" ng-click="grid.appScope.deleteUnitType(row.entity)">' + app.localize('Delete') + '</a></li>' +
                            '    </ul>' +
                            '  </div>' +
                            '</div>'
                    },
                    {
                        name: app.localize('UnitType'),
                        enableColumnMenu: false,
                        field: 'unitTypeName',
                        minWidth: 150,
                    },
                    {
                        name: app.localize('CreationTime'),
                        enableColumnMenu: false,
                        field: 'creationTime',
                        headerCellClass: 'centeralign',
                        width: 170,
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getUnitType();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getUnitType();
                    });
                },
                data: []
            };

            if (!vm.permissions.editUnitType && !vm.permissions.deleteUnitType) {
                vm.unitTypeGridOptions.columnDefs.shift();
            }

            vm.getUnitType = function () {
                vm.loading = true;
                unitTypeService.getUnitType({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    UnitTypeName: vm.filterText
                }).then(function (result) {
                    vm.unitTypeGridOptions.totalItems = result.data.totalCount;
                    vm.unitTypeGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.createUnitType = function () {
                //$state.go('createUnitType', {
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/unittype/createModel.cshtml',
                    controller: 'host.views.unittype.createModel as vm',
                    backdrop: 'static'
                });
                modalInstance.result.then(function () {
                    vm.getUnitType();
                });
            };

            vm.deleteUnitType = function (unit) {
                abp.message.confirm(
                    app.localize('UnitTypeDeleteWarningMessage', unit.unitTypeName),
                    function (isConfirmed) {
                        if (isConfirmed) {
                            unitTypeService.deleteUnitType({
                                id: unit.id
                            }).then(function () {
                                vm.getUnitType();
                                abp.notify.success(app.localize('SuccessfullyDeleted'));
                            });
                        }
                    }
                );
            };

            vm.editUnitType = function (unitType) {
                //$state.go('editUnitType', {
                //    Id: unitType.id
                //});
                var modalInstance = $uibModal.open({
                    templateUrl: '/App/host/views/unittype/editModel.cshtml',
                    controller: 'host.views.unittype.editModel as vm',
                    backdrop: 'static',
                    resolve: {
                        Id: function () {
                            return unitType.id;
                        }
                    }
                });

                modalInstance.result.then(function (result) {
                    vm.getUnitType();
                });
            };



            vm.getUnitType();
        }
    ]);
})();