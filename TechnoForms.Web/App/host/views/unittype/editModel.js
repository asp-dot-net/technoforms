﻿(function () {
    appModule.controller('host.views.unittype.editModel', [
        '$scope', '$state', '$stateParams', '$uibModalInstance', 'abp.services.app.unitType','Id',
        function ($scope, $state, $stateParams, $uibModalInstance, unitTypeService, Id) {
            var vm = this;

            vm.saving = false;
            vm.unittype = null;
            vm.editions = [];
            //vm.unitTypeId = $stateParams.Id;

            vm.exist = false;
            vm.message = "";

            vm.save = function () {
                vm.saving = true;
                unitTypeService.checkUnitTypeExists({ UnitTypeName: vm.unittype.unitTypeName, Id: Id }).then(function (result) {
                    if (result.data.status == true) {
                        unitTypeService.updateUnitType(vm.unittype)
                            .then(function () {
                                //$state.go('host.unittype', {
                                //});
                                abp.notify.info(app.localize('SavedSuccessfully'));
                                $uibModalInstance.close();
                            }).finally(function () {
                                vm.saving = false;
                            });
                    }
                    else {
                        abp.notify.info(app.localize('RecordAlreadyExists'));
                        //vm.message = result.data.message;
                        //vm.exist = true;
                        vm.saving = false;
                    }
                });
            };

            vm.cancel = function () {
                //$state.go('host.unittype', {
                //});
                $uibModalInstance.dismiss();
            };

            function init() {
                unitTypeService.getUnitTypeForEdit({
                    id: Id //vm.unitTypeId
                }).then(function (result) {
                    vm.unittype = result.data;
                });
            }
            init();
        }
    ]);
})();