﻿(function () {
    appModule.controller('host.views.newInvitation.rejectModel', [
        '$scope', '$uibModalInstance', 'abp.services.app.newInvitation', 'invitationId',
        function ($scope, $uibModalInstance, newInvitationService, invitationId) {
            var vm = this;

            vm.saving = false;
            vm.invitation = {};

            vm.save = function () {
                vm.saving = true;
                newInvitationService.rejectInvitation(vm.invitation).then(function () {
                    abp.notify.info(app.localize('SavedSuccessfully'));
                    $uibModalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                newInvitationService.getInvitationForReject({
                    id: invitationId
                }).then(function (result) {
                    vm.invitation = result.data;
                });
            }
            init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();