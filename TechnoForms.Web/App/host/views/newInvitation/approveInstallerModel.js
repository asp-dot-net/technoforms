﻿(function () {
    appModule.controller('host.views.newInvitation.approveInstallerModel', [
        '$scope', '$uibModalInstance', 'abp.services.app.newInvitation', 'invitationId',
        function ($scope, $uibModalInstance, newInvitationService, invitationId) {
            var vm = this;

            vm.saving = false;
            vm.users = [];

            vm.invitation = {};

            vm.save = function () {
                vm.saving = true;
                newInvitationService.approveInvitation({ id: vm.invitation.id, UserId: vm.invitation.userId }).then(function () {
                    abp.notify.info(app.localize('InstallerVerifiedSuccessfully'));
                    $uibModalInstance.close();
                }).finally(function () {
                    vm.saving = false;
                });
            };

            function init() {
                newInvitationService.getInvitationForReject({
                    id: invitationId
                }).then(function (result) {
                    vm.invitation = result.data;
                });

                newInvitationService.getUsers().then(function (result) {
                    vm.users = result.data.items;
                    vm.flags = result.data.flags;
                    setTimeout(function () {
                        $('#userSelectionCombobox').selectpicker('refresh');
                        $('#userSelectionCombobox').selectpicker('refresh');
                    }, 0);
                    vm.loading = false;
                });
            }
            init();

            vm.cancel = function () {
                $uibModalInstance.dismiss();
            };
        }
    ]);
})();