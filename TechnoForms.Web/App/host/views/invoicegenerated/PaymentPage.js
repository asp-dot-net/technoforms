﻿(function () {
    appModule.controller('host.views.invoicegenerated.paymentPage', [
        '$scope', '$state', '$stateParams', '$uibModal', 'abp.services.app.invoice', 'abp.services.app.payment', 'uiGridConstants',
        function ($scope, $state, $stateParams, $uibModal, invoiceService, paymentService, uiGridConstants) {
            var vm = this;
            vm.norecord = false;
            vm.saving = false;
            vm.payments = {};
            vm.modeofpayment = [];
            vm.amountvalid = true;
            vm.save = function () {
            };

            vm.cancel = function () {
                $state.go('host.invoicegenerated');
            };

            vm.CheckFloatNumber = function () {
                if ((isNaN(event.key) || event.key === ' ' || event.key === '') && event.key != '.') {
                    event.returnValue = '';
                }
            };
            //DatePIcker Code Start
            $scope.today = function () {
                $scope.dt = new Date();
            };
            //$scope.today();

            $scope.clear = function () {
                $scope.dt = null;
            };

            $scope.inlineOptions = {
                customClass: getDayClass,
                minDate: new Date(),
                showWeeks: true
            };

            $scope.dateOptions = {
                //dateDisabled: disabled,
                formatYear: 'yy',
                //maxDate: new Date(2020, 5, 22),
                //minDate: new Date(),
                startingDay: 1
            };

            // Disable weekend selection
            function disabled(data) {
                var date = data.date,
                    mode = data.mode;
                return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            }

            //$scope.toggleMin = function () {
            //    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
            //    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
            //};

            //$scope.toggleMin();

            $scope.open1 = function () {
                $scope.popup1.opened = true;
            };

            $scope.formats = ['d/MM/yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.altInputFormats = ['M!/d!/yyyy'];

            $scope.popup1 = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];

            function getDayClass(data) {
                var date = data.date,
                    mode = data.mode;
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            }
//DatePIcker Code End


            vm.invoiceId = $stateParams.InvoiceId;
            vm.tenantId = $stateParams.TenantId;
            vm.checkamount = function (amount, invoiceamount) {
                vm.validateamount = true;
                if (amount > invoiceamount)
                    vm.validateamount = false;
            }

            vm.save = function () {
               // vm.saving = true;
                vm.amountvalid = true;
                vm.payments.invoice_ID = $stateParams.InvoiceId;
                vm.payments.tenantId = $stateParams.TenantId;
                if (vm.payments.amount <= vm.outStandingAmount) {
                    paymentService.createPayments(vm.payments).then(function () {
                        vm.saving = false;
                        vm.getPayment();
                        vm.getInvoiceDetail();
                        vm.payments = {};
                        vm.bindModeOfPaymment();
                        abp.notify.info(app.localize('PaymentAddedSuccessfully'));
                    }).finally(function () {
                        vm.saving = false;
                    });
                }
                else {
                    abp.notify.info(app.localize('Pleaseentervalidamount'));
                    vm.saving = false;
                    vm.amountvalid = false;
                }

            };
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.paymentGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('date'),
                        enableColumnMenu: false,   
                        headerCellClass:'centeralign',
                        field: 'date',
                        width: '150',
                        cellClass:'centeralign',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    },
                    {
                        name: app.localize('ModeofpaymentName'),
                        field: 'modeofpaymentName',
                        enableColumnMenu: false,   
                        minWidth:200
                    },
                    {
                        name: app.localize('amount'),
                        enableColumnMenu: false,   
                        field: 'amount',
                        width:'150',
                        cellClass:'rightalign'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getPayment(); 
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getPayment();
                    });
                },
                data: []
            };

            vm.getPayment = function () {
                vm.loading = true;
                paymentService.getPaymentList({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                    InvoiceId: vm.invoiceId
                }).then(function (result) {
                    vm.paymentGridOptions.totalItems = result.data.totalCount;
                    vm.paymentGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.getInvoiceDetail = function () {
                invoiceService.getInvoiceDetailByInvoiceId({
                    invoiceId: vm.invoiceId,
                    tenantId: vm.tenantId
                }).then(function (result) {
                    vm.customername = result.data.customerName;
                    vm.invoiceNumber = result.data.invoiceNumber;
                    vm.invoiceAmount = result.data.invoiceAmount;
                    vm.paidAmount = result.data.paidAmount;
                    vm.outStandingAmount = result.data.outStandingAmount;
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.bindModeOfPaymment = function () {
                paymentService.getModeOfPayment().then(function (result) {
                    vm.modeofpayment = result.data.items;
                    setTimeout(function () {
                        $('#modeOfPaymentCombobox').selectpicker('refresh');
                        $('#modeOfPaymentCombobox').selectpicker('refresh');
                    }, 0);
                    vm.loading = false;
                });
            };
            function init() {
                vm.bindModeOfPaymment();
                vm.getInvoiceDetail();
                vm.getPayment();
            }


            init();
        }
    ]);
})();