﻿(function () {
    appModule.controller('host.views.invoicegenerated.index', [
        '$scope', '$state', 'uiGridConstants', 'abp.services.app.invoice',
        function ($scope, $state, uiGridConstants, invoiceservice) {
            var vm = this;
            vm.norecord = false;
            $scope.$on('$viewContentLoaded', function () {
                App.initAjax();
            });

            //start from here...
            vm.loading = false;
            var requestParams = {
                skipCount: 0,
                maxResultCount: app.consts.grid.defaultPageSize,
                sorting: null
            };

            vm.jobsGridOptions = {
                enableHorizontalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                enableVerticalScrollbar: uiGridConstants.scrollbars.WHEN_NEEDED,
                paginationPageSizes: app.consts.grid.defaultPageSizes,
                paginationPageSize: app.consts.grid.defaultPageSize,
                useExternalPagination: true,
                useExternalSorting: true,
                appScopeProvider: vm,
                rowTemplate: '<div ng-repeat="(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name" class="ui-grid-cell" ng-class="{ \'ui-grid-row-header-cell\': col.isRowHeader, \'text-muted\': !row.entity.isActive }"  ui-grid-cell></div>',
                columnDefs: [
                    {
                        name: app.localize('CustomerName'),
                        enableColumnMenu: false,         
                        field: 'customerName',
                        minWidth:200,
                    },
                    {
                        name: app.localize('InvoiceNumber'),
                        width:'200',
                        headerCellClass:'centeralign',
                        enableColumnMenu: false,      
                        cellClass:'centeralign',
                        field: 'invoiceNumber'
                    },

                    {
                        name: app.localize('InvoiceAmount'),
                        enableColumnMenu: false,         
                        field: 'invoiceAmount',
                        width:'200',
                        cellClass:'rightalign',
                    },
                    {
                        name: app.localize('CreationTime'),
                        headerCellClass:'centeralign',
                        enableColumnMenu: false,         
                        width:'180',
                        cellClass:'centeralign',
                        field: 'creationTime',
                        cellFilter: 'date:\'dd-MM-yyyy\''
                    },
                    {
                        name: app.localize('AddPayment'),
                        headerCellClass:'centeralign',
                        enableColumnMenu: false,         
                        enableSorting: false,
                        width:'180',
                        cellClass:'centeralign',
                        cellTemplate:
                            '<a ng-click="grid.appScope.PaymentPage(row.entity)" class="btn btn-primary buttoncenter buttonaddpayment">' + app.localize('AddPayment') + '</a>'
                    }
                ],
                onRegisterApi: function (gridApi) {
                    $scope.gridApi = gridApi;
                    $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                        if (!sortColumns.length || !sortColumns[0].field) {
                            requestParams.sorting = null;
                        } else {
                            requestParams.sorting = sortColumns[0].field + ' ' + sortColumns[0].sort.direction;
                        }

                        vm.getInvoice();
                    });
                    gridApi.pagination.on.paginationChanged($scope, function (pageNumber, pageSize) {
                        requestParams.skipCount = (pageNumber - 1) * pageSize;
                        requestParams.maxResultCount = pageSize;

                        vm.getInvoice();
                    });
                },
                data: []
            };

            vm.getInvoice = function () {
                vm.loading = true;
                invoiceservice.getGeneratedInvoice({
                    skipCount: requestParams.skipCount,
                    maxResultCount: requestParams.maxResultCount,
                    sorting: requestParams.sorting,
                }).then(function (result) {
                    vm.jobsGridOptions.totalItems = result.data.totalCount;
                    vm.jobsGridOptions.data = result.data.items;
                    if (result.data.totalCount == 0) {
                        //vm.norecord = true;
                        abp.notify.info(app.localize('NoRecordFound'));
                    }
                    else { vm.norecord = false; }
                }).finally(function () {
                    vm.loading = false;
                });
            };

            vm.PaymentPage = function (Invoice) {
                $state.go('PaymentPage', {
                    InvoiceId: Invoice.id,
                    TenantId: Invoice.tenantId
                });
            };

            vm.getInvoice();
        }
    ]);
})();