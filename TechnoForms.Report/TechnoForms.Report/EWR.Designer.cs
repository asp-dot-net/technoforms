namespace TechnoForms.Report
{
	partial class EWR
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EWR));
			Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup68 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup69 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup70 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup71 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup72 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup73 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup74 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup75 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup76 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup77 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup78 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup79 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup80 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup81 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup82 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup60 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup61 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup62 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup63 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup64 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup65 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup66 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup67 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup85 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup86 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup87 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup88 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup89 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup90 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup91 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup92 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup93 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup94 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup95 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup96 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup97 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup98 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup99 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup100 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup101 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup102 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup83 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup84 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup141 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup142 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup143 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup144 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup145 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup146 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup147 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup148 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup149 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup150 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup151 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup152 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup153 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup154 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup155 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup156 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup157 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup158 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup159 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup160 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup106 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup107 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup108 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup109 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup103 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup104 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup105 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup114 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup115 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup116 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup117 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup118 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup119 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup110 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup111 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup112 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup113 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup123 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup124 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup125 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup126 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup127 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup120 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup121 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup122 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup128 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup129 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup130 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup131 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup135 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup136 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup137 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup138 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup139 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup140 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup132 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup133 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup134 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup161 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup162 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup163 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup164 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup165 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup166 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup167 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup168 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup169 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup177 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup178 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup179 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup180 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup181 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup182 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup183 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup184 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup185 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup186 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup170 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup171 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup172 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup173 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup174 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup175 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup176 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup194 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup195 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup196 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup197 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup198 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup199 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup200 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup201 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup187 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup188 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup189 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup190 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup191 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup192 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup193 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup202 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup203 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup204 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup205 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup206 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup207 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup208 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup209 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup213 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup214 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup215 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup216 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup217 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup218 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup219 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup220 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup221 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup222 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup223 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup224 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup225 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup226 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup210 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup211 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup212 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup231 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup232 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup233 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup234 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup235 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup236 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup237 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup238 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup239 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup227 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup228 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup229 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup230 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup240 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup241 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup242 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup243 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup244 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup245 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup246 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup247 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup248 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup249 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup250 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup251 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup252 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup253 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox26 = new Telerik.Reporting.TextBox();
			this.textBox32 = new Telerik.Reporting.TextBox();
			this.textBox28 = new Telerik.Reporting.TextBox();
			this.textBox34 = new Telerik.Reporting.TextBox();
			this.textBox30 = new Telerik.Reporting.TextBox();
			this.textBox41 = new Telerik.Reporting.TextBox();
			this.textBox42 = new Telerik.Reporting.TextBox();
			this.textBox43 = new Telerik.Reporting.TextBox();
			this.textBox44 = new Telerik.Reporting.TextBox();
			this.textBox45 = new Telerik.Reporting.TextBox();
			this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
			this.textBox48 = new Telerik.Reporting.TextBox();
			this.textBox60 = new Telerik.Reporting.TextBox();
			this.textBox66 = new Telerik.Reporting.TextBox();
			this.textBox83 = new Telerik.Reporting.TextBox();
			this.textBox85 = new Telerik.Reporting.TextBox();
			this.textBox71 = new Telerik.Reporting.TextBox();
			this.textBox112 = new Telerik.Reporting.TextBox();
			this.textBox107 = new Telerik.Reporting.TextBox();
			this.textBox115 = new Telerik.Reporting.TextBox();
			this.textBox116 = new Telerik.Reporting.TextBox();
			this.textBox121 = new Telerik.Reporting.TextBox();
			this.textBox124 = new Telerik.Reporting.TextBox();
			this.textBox136 = new Telerik.Reporting.TextBox();
			this.textBox142 = new Telerik.Reporting.TextBox();
			this.checkBox55 = new Telerik.Reporting.CheckBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.pictureBox2 = new Telerik.Reporting.PictureBox();
			this.table1 = new Telerik.Reporting.Table();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.table2 = new Telerik.Reporting.Table();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.table3 = new Telerik.Reporting.Table();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.table44 = new Telerik.Reporting.Table();
			this.checkBox76 = new Telerik.Reporting.CheckBox();
			this.checkBox80 = new Telerik.Reporting.CheckBox();
			this.checkBox81 = new Telerik.Reporting.CheckBox();
			this.checkBox82 = new Telerik.Reporting.CheckBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.textBox25 = new Telerik.Reporting.TextBox();
			this.table4 = new Telerik.Reporting.Table();
			this.textBox27 = new Telerik.Reporting.TextBox();
			this.textBox29 = new Telerik.Reporting.TextBox();
			this.textBox31 = new Telerik.Reporting.TextBox();
			this.textBox33 = new Telerik.Reporting.TextBox();
			this.textBox35 = new Telerik.Reporting.TextBox();
			this.table5 = new Telerik.Reporting.Table();
			this.textBox36 = new Telerik.Reporting.TextBox();
			this.textBox37 = new Telerik.Reporting.TextBox();
			this.textBox38 = new Telerik.Reporting.TextBox();
			this.textBox39 = new Telerik.Reporting.TextBox();
			this.textBox40 = new Telerik.Reporting.TextBox();
			this.table6 = new Telerik.Reporting.Table();
			this.textBox47 = new Telerik.Reporting.TextBox();
			this.textBox51 = new Telerik.Reporting.TextBox();
			this.textBox53 = new Telerik.Reporting.TextBox();
			this.textBox46 = new Telerik.Reporting.TextBox();
			this.table18 = new Telerik.Reporting.Table();
			this.checkBox27 = new Telerik.Reporting.CheckBox();
			this.checkBox28 = new Telerik.Reporting.CheckBox();
			this.table7 = new Telerik.Reporting.Table();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.textBox52 = new Telerik.Reporting.TextBox();
			this.textBox55 = new Telerik.Reporting.TextBox();
			this.table8 = new Telerik.Reporting.Table();
			this.textBox62 = new Telerik.Reporting.TextBox();
			this.textBox63 = new Telerik.Reporting.TextBox();
			this.textBox64 = new Telerik.Reporting.TextBox();
			this.textBox50 = new Telerik.Reporting.TextBox();
			this.textBox59 = new Telerik.Reporting.TextBox();
			this.panel1 = new Telerik.Reporting.Panel();
			this.textBox56 = new Telerik.Reporting.TextBox();
			this.table9 = new Telerik.Reporting.Table();
			this.textBox70 = new Telerik.Reporting.TextBox();
			this.textBox72 = new Telerik.Reporting.TextBox();
			this.textBox74 = new Telerik.Reporting.TextBox();
			this.checkBox1 = new Telerik.Reporting.CheckBox();
			this.checkBox2 = new Telerik.Reporting.CheckBox();
			this.checkBox3 = new Telerik.Reporting.CheckBox();
			this.checkBox4 = new Telerik.Reporting.CheckBox();
			this.checkBox5 = new Telerik.Reporting.CheckBox();
			this.table13 = new Telerik.Reporting.Table();
			this.checkBox22 = new Telerik.Reporting.CheckBox();
			this.checkBox23 = new Telerik.Reporting.CheckBox();
			this.table10 = new Telerik.Reporting.Table();
			this.checkBox6 = new Telerik.Reporting.CheckBox();
			this.checkBox7 = new Telerik.Reporting.CheckBox();
			this.table11 = new Telerik.Reporting.Table();
			this.checkBox8 = new Telerik.Reporting.CheckBox();
			this.checkBox9 = new Telerik.Reporting.CheckBox();
			this.table12 = new Telerik.Reporting.Table();
			this.textBox61 = new Telerik.Reporting.TextBox();
			this.textBox65 = new Telerik.Reporting.TextBox();
			this.textBox73 = new Telerik.Reporting.TextBox();
			this.checkBox10 = new Telerik.Reporting.CheckBox();
			this.checkBox11 = new Telerik.Reporting.CheckBox();
			this.checkBox12 = new Telerik.Reporting.CheckBox();
			this.checkBox13 = new Telerik.Reporting.CheckBox();
			this.textBox67 = new Telerik.Reporting.TextBox();
			this.textBox68 = new Telerik.Reporting.TextBox();
			this.textBox69 = new Telerik.Reporting.TextBox();
			this.textBox54 = new Telerik.Reporting.TextBox();
			this.textBox58 = new Telerik.Reporting.TextBox();
			this.table14 = new Telerik.Reporting.Table();
			this.textBox75 = new Telerik.Reporting.TextBox();
			this.panel2 = new Telerik.Reporting.Panel();
			this.table15 = new Telerik.Reporting.Table();
			this.checkBox14 = new Telerik.Reporting.CheckBox();
			this.checkBox15 = new Telerik.Reporting.CheckBox();
			this.checkBox16 = new Telerik.Reporting.CheckBox();
			this.checkBox17 = new Telerik.Reporting.CheckBox();
			this.textBox81 = new Telerik.Reporting.TextBox();
			this.textBox86 = new Telerik.Reporting.TextBox();
			this.textBox87 = new Telerik.Reporting.TextBox();
			this.textBox94 = new Telerik.Reporting.TextBox();
			this.textBox96 = new Telerik.Reporting.TextBox();
			this.textBox97 = new Telerik.Reporting.TextBox();
			this.textBox104 = new Telerik.Reporting.TextBox();
			this.textBox76 = new Telerik.Reporting.TextBox();
			this.textBox78 = new Telerik.Reporting.TextBox();
			this.textBox109 = new Telerik.Reporting.TextBox();
			this.checkBox18 = new Telerik.Reporting.CheckBox();
			this.table20 = new Telerik.Reporting.Table();
			this.textBox79 = new Telerik.Reporting.TextBox();
			this.table21 = new Telerik.Reporting.Table();
			this.checkBox24 = new Telerik.Reporting.CheckBox();
			this.checkBox25 = new Telerik.Reporting.CheckBox();
			this.checkBox26 = new Telerik.Reporting.CheckBox();
			this.textBox99 = new Telerik.Reporting.TextBox();
			this.table19 = new Telerik.Reporting.Table();
			this.textBox57 = new Telerik.Reporting.TextBox();
			this.table17 = new Telerik.Reporting.Table();
			this.checkBox19 = new Telerik.Reporting.CheckBox();
			this.checkBox20 = new Telerik.Reporting.CheckBox();
			this.checkBox29 = new Telerik.Reporting.CheckBox();
			this.textBox84 = new Telerik.Reporting.TextBox();
			this.textBox89 = new Telerik.Reporting.TextBox();
			this.textBox90 = new Telerik.Reporting.TextBox();
			this.table22 = new Telerik.Reporting.Table();
			this.textBox88 = new Telerik.Reporting.TextBox();
			this.table23 = new Telerik.Reporting.Table();
			this.checkBox21 = new Telerik.Reporting.CheckBox();
			this.checkBox30 = new Telerik.Reporting.CheckBox();
			this.textBox91 = new Telerik.Reporting.TextBox();
			this.textBox98 = new Telerik.Reporting.TextBox();
			this.table24 = new Telerik.Reporting.Table();
			this.textBox77 = new Telerik.Reporting.TextBox();
			this.textBox92 = new Telerik.Reporting.TextBox();
			this.textBox80 = new Telerik.Reporting.TextBox();
			this.table16 = new Telerik.Reporting.Table();
			this.textBox93 = new Telerik.Reporting.TextBox();
			this.textBox100 = new Telerik.Reporting.TextBox();
			this.table25 = new Telerik.Reporting.Table();
			this.checkBox31 = new Telerik.Reporting.CheckBox();
			this.checkBox32 = new Telerik.Reporting.CheckBox();
			this.textBox82 = new Telerik.Reporting.TextBox();
			this.textBox95 = new Telerik.Reporting.TextBox();
			this.table26 = new Telerik.Reporting.Table();
			this.checkBox33 = new Telerik.Reporting.CheckBox();
			this.checkBox34 = new Telerik.Reporting.CheckBox();
			this.textBox105 = new Telerik.Reporting.TextBox();
			this.textBox106 = new Telerik.Reporting.TextBox();
			this.textBox110 = new Telerik.Reporting.TextBox();
			this.table27 = new Telerik.Reporting.Table();
			this.textBox103 = new Telerik.Reporting.TextBox();
			this.textBox111 = new Telerik.Reporting.TextBox();
			this.textBox117 = new Telerik.Reporting.TextBox();
			this.textBox108 = new Telerik.Reporting.TextBox();
			this.table28 = new Telerik.Reporting.Table();
			this.checkBox35 = new Telerik.Reporting.CheckBox();
			this.checkBox36 = new Telerik.Reporting.CheckBox();
			this.checkBox37 = new Telerik.Reporting.CheckBox();
			this.table29 = new Telerik.Reporting.Table();
			this.checkBox38 = new Telerik.Reporting.CheckBox();
			this.checkBox39 = new Telerik.Reporting.CheckBox();
			this.checkBox40 = new Telerik.Reporting.CheckBox();
			this.table30 = new Telerik.Reporting.Table();
			this.textBox114 = new Telerik.Reporting.TextBox();
			this.table31 = new Telerik.Reporting.Table();
			this.checkBox43 = new Telerik.Reporting.CheckBox();
			this.checkBox44 = new Telerik.Reporting.CheckBox();
			this.table32 = new Telerik.Reporting.Table();
			this.checkBox41 = new Telerik.Reporting.CheckBox();
			this.checkBox45 = new Telerik.Reporting.CheckBox();
			this.checkBox46 = new Telerik.Reporting.CheckBox();
			this.table33 = new Telerik.Reporting.Table();
			this.textBox118 = new Telerik.Reporting.TextBox();
			this.checkBox42 = new Telerik.Reporting.CheckBox();
			this.checkBox47 = new Telerik.Reporting.CheckBox();
			this.textBox101 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.table34 = new Telerik.Reporting.Table();
			this.textBox113 = new Telerik.Reporting.TextBox();
			this.textBox120 = new Telerik.Reporting.TextBox();
			this.textBox122 = new Telerik.Reporting.TextBox();
			this.textBox128 = new Telerik.Reporting.TextBox();
			this.table35 = new Telerik.Reporting.Table();
			this.checkBox48 = new Telerik.Reporting.CheckBox();
			this.checkBox49 = new Telerik.Reporting.CheckBox();
			this.textBox123 = new Telerik.Reporting.TextBox();
			this.textBox126 = new Telerik.Reporting.TextBox();
			this.textBox119 = new Telerik.Reporting.TextBox();
			this.checkBox50 = new Telerik.Reporting.CheckBox();
			this.checkBox51 = new Telerik.Reporting.CheckBox();
			this.checkBox52 = new Telerik.Reporting.CheckBox();
			this.table36 = new Telerik.Reporting.Table();
			this.textBox129 = new Telerik.Reporting.TextBox();
			this.textBox131 = new Telerik.Reporting.TextBox();
			this.checkBox53 = new Telerik.Reporting.CheckBox();
			this.checkBox54 = new Telerik.Reporting.CheckBox();
			this.table37 = new Telerik.Reporting.Table();
			this.textBox125 = new Telerik.Reporting.TextBox();
			this.textBox130 = new Telerik.Reporting.TextBox();
			this.textBox132 = new Telerik.Reporting.TextBox();
			this.textBox133 = new Telerik.Reporting.TextBox();
			this.textBox134 = new Telerik.Reporting.TextBox();
			this.textBox135 = new Telerik.Reporting.TextBox();
			this.table38 = new Telerik.Reporting.Table();
			this.textBox127 = new Telerik.Reporting.TextBox();
			this.table39 = new Telerik.Reporting.Table();
			this.textBox141 = new Telerik.Reporting.TextBox();
			this.textBox143 = new Telerik.Reporting.TextBox();
			this.textBox145 = new Telerik.Reporting.TextBox();
			this.textBox137 = new Telerik.Reporting.TextBox();
			this.textBox138 = new Telerik.Reporting.TextBox();
			this.textBox144 = new Telerik.Reporting.TextBox();
			this.textBox146 = new Telerik.Reporting.TextBox();
			this.textBox139 = new Telerik.Reporting.TextBox();
			this.textBox149 = new Telerik.Reporting.TextBox();
			this.textBox150 = new Telerik.Reporting.TextBox();
			this.textBox140 = new Telerik.Reporting.TextBox();
			this.textBox151 = new Telerik.Reporting.TextBox();
			this.textBox152 = new Telerik.Reporting.TextBox();
			this.textBox153 = new Telerik.Reporting.TextBox();
			this.textBox147 = new Telerik.Reporting.TextBox();
			this.textBox148 = new Telerik.Reporting.TextBox();
			this.textBox154 = new Telerik.Reporting.TextBox();
			this.textBox155 = new Telerik.Reporting.TextBox();
			this.textBox156 = new Telerik.Reporting.TextBox();
			this.textBox157 = new Telerik.Reporting.TextBox();
			this.textBox158 = new Telerik.Reporting.TextBox();
			this.textBox159 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// textBox1
			// 
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.021D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
			this.textBox1.Style.Font.Name = "Arial";
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox1.Value = "Retailer:";
			// 
			// textBox5
			// 
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.479D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
			this.textBox5.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
			this.textBox5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox5.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox5.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			// 
			// textBox10
			// 
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.395D), Telerik.Reporting.Drawing.Unit.Inch(0.208D));
			this.textBox10.Style.Color = System.Drawing.Color.IndianRed;
			this.textBox10.Style.Font.Name = "Arial";
			this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox10.Value = "Send completed form by email or mail to Retailer:";
			// 
			// textBox12
			// 
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.207D), Telerik.Reporting.Drawing.Unit.Inch(0.208D));
			this.textBox12.Style.BorderColor.Bottom = System.Drawing.Color.IndianRed;
			this.textBox12.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox12.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox12.Style.Color = System.Drawing.Color.IndianRed;
			this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			// 
			// textBox14
			// 
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.792D), Telerik.Reporting.Drawing.Unit.Inch(0.208D));
			this.textBox14.Style.Color = System.Drawing.Color.IndianRed;
			this.textBox14.Style.Font.Name = "Arial";
			this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox14.Value = "or Distributor:";
			// 
			// textBox16
			// 
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.207D), Telerik.Reporting.Drawing.Unit.Inch(0.208D));
			this.textBox16.Style.BorderColor.Bottom = System.Drawing.Color.IndianRed;
			this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox16.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox16.Style.Color = System.Drawing.Color.IndianRed;
			this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox16.StyleName = "";
			// 
			// textBox13
			// 
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.643D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox13.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox13.Style.Font.Bold = true;
			this.textBox13.Style.Font.Name = "Calibri";
			this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.2D);
			this.textBox13.Value = "Work site address";
			// 
			// textBox20
			// 
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.176D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			// 
			// textBox22
			// 
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.78D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox22.StyleName = "";
			// 
			// textBox26
			// 
			this.textBox26.Name = "textBox26";
			this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.179D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox26.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox26.Value = "Unit / Floor / Shop";
			// 
			// textBox32
			// 
			this.textBox32.Name = "textBox32";
			this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.108D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox32.StyleName = "";
			// 
			// textBox28
			// 
			this.textBox28.Name = "textBox28";
			this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.273D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox28.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox28.Value = "Street number";
			// 
			// textBox34
			// 
			this.textBox34.Name = "textBox34";
			this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.108D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox34.StyleName = "";
			// 
			// textBox30
			// 
			this.textBox30.Name = "textBox30";
			this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.932D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox30.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox30.Value = "Street Name";
			// 
			// textBox41
			// 
			this.textBox41.Name = "textBox41";
			this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.592D), Telerik.Reporting.Drawing.Unit.Inch(0.188D));
			this.textBox41.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox41.Value = "Lot";
			// 
			// textBox42
			// 
			this.textBox42.Name = "textBox42";
			this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.108D), Telerik.Reporting.Drawing.Unit.Inch(0.188D));
			this.textBox42.StyleName = "";
			// 
			// textBox43
			// 
			this.textBox43.Name = "textBox43";
			this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.2D), Telerik.Reporting.Drawing.Unit.Inch(0.188D));
			this.textBox43.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox43.Value = "Suburb";
			// 
			// textBox44
			// 
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.108D), Telerik.Reporting.Drawing.Unit.Inch(0.188D));
			this.textBox44.StyleName = "";
			// 
			// textBox45
			// 
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.592D), Telerik.Reporting.Drawing.Unit.Inch(0.188D));
			this.textBox45.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox45.Value = "Existing meter number";
			// 
			// htmlTextBox1
			// 
			this.htmlTextBox1.Name = "htmlTextBox1";
			this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(0.201D));
			this.htmlTextBox1.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.htmlTextBox1.StyleName = "";
			this.htmlTextBox1.Value = "<span style=\"color: #003f87\"><strong><span style=\"font-size: 12pt\">Description of" +
    " All Works</span>&nbsp; </strong><strong>(including coincidental works taking pl" +
    "ace)</strong> (PLEASE PRINT)</span>";
			// 
			// textBox48
			// 
			this.textBox48.Name = "textBox48";
			this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.187D));
			this.textBox48.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox48.Style.Font.Bold = true;
			this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox48.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox48.Value = "Premise type";
			// 
			// textBox60
			// 
			this.textBox60.Name = "textBox60";
			this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.218D), Telerik.Reporting.Drawing.Unit.Inch(0.188D));
			this.textBox60.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox60.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox60.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox60.Style.Font.Bold = true;
			this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox60.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox60.Value = "Connection type";
			// 
			// textBox66
			// 
			this.textBox66.Name = "textBox66";
			this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.281D), Telerik.Reporting.Drawing.Unit.Inch(0.188D));
			this.textBox66.Style.Font.Bold = true;
			this.textBox66.StyleName = "";
			// 
			// textBox83
			// 
			this.textBox83.Name = "textBox83";
			this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.176D), Telerik.Reporting.Drawing.Unit.Inch(0.19D));
			this.textBox83.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox83.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox83.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox83.Style.Font.Bold = true;
			this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox83.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox83.Value = "Supply Required";
			// 
			// textBox85
			// 
			this.textBox85.Name = "textBox85";
			this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.729D), Telerik.Reporting.Drawing.Unit.Inch(0.19D));
			this.textBox85.Style.Font.Bold = true;
			this.textBox85.StyleName = "";
			// 
			// textBox71
			// 
			this.textBox71.Name = "textBox71";
			this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.292D), Telerik.Reporting.Drawing.Unit.Inch(0.19D));
			this.textBox71.Style.Font.Bold = true;
			this.textBox71.StyleName = "";
			// 
			// textBox112
			// 
			this.textBox112.Name = "textBox112";
			this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.193D));
			this.textBox112.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox112.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox112.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox112.Style.Font.Bold = true;
			this.textBox112.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox112.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox112.Value = "Number of premises";
			// 
			// textBox107
			// 
			this.textBox107.Name = "textBox107";
			this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.144D), Telerik.Reporting.Drawing.Unit.Inch(0.188D));
			this.textBox107.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox107.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox107.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox107.Style.Font.Bold = true;
			this.textBox107.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox107.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox107.Value = "Metering requirements";
			// 
			// textBox115
			// 
			this.textBox115.Name = "textBox115";
			this.textBox115.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.193D));
			this.textBox115.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox115.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox115.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox115.Style.Font.Bold = true;
			this.textBox115.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox115.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox115.Value = "Termination:";
			// 
			// textBox116
			// 
			this.textBox116.Name = "textBox116";
			this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.144D), Telerik.Reporting.Drawing.Unit.Inch(0.188D));
			this.textBox116.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox116.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox116.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox116.Style.Font.Bold = true;
			this.textBox116.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox116.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox116.Value = "Embedded Network";
			// 
			// textBox121
			// 
			this.textBox121.Name = "textBox121";
			this.textBox121.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.087D), Telerik.Reporting.Drawing.Unit.Inch(0.218D));
			this.textBox121.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox121.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox121.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox121.Style.Font.Bold = true;
			this.textBox121.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox121.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox121.Value = "Truck appointment: ";
			// 
			// textBox124
			// 
			this.textBox124.Name = "textBox124";
			this.textBox124.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.475D), Telerik.Reporting.Drawing.Unit.Inch(0.218D));
			this.textBox124.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox124.Style.Font.Bold = true;
			this.textBox124.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox124.StyleName = "";
			// 
			// textBox136
			// 
			this.textBox136.Name = "textBox136";
			this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.488D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox136.Style.Font.Name = "Arial";
			this.textBox136.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox136.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox136.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox136.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox136.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox136.Value = "THE WORK WILL BE SAFE TO CONNECT ON: DATE:";
			// 
			// textBox142
			// 
			this.textBox142.Name = "textBox142";
			this.textBox142.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.332D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox142.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox142.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox142.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox142.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox142.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox142.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox142.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox142.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox142.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox142.Style.Font.Name = "Arial";
			this.textBox142.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox142.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox142.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox142.StyleName = "";
			// 
			// checkBox55
			// 
			this.checkBox55.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox55.CheckedImage = ((object)(resources.GetObject("checkBox55.CheckedImage")));
			this.checkBox55.IndeterminateImage = ((object)(resources.GetObject("checkBox55.IndeterminateImage")));
			this.checkBox55.Name = "checkBox55";
			this.checkBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.78D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.checkBox55.Style.Font.Name = "Arial";
			this.checkBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.checkBox55.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox55.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox55.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(1.5D);
			this.checkBox55.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.checkBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox55.StyleName = "";
			this.checkBox55.Text = " OR at Completion of Truck Appointments";
			this.checkBox55.UncheckedImage = ((object)(resources.GetObject("checkBox55.UncheckedImage")));
			this.checkBox55.Value = "";
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(290.96D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox2,
            this.table1,
            this.textBox9,
            this.table2,
            this.table3,
            this.table4,
            this.table5,
            this.table6,
            this.table7,
            this.table8,
            this.panel1,
            this.table39});
			this.detail.Name = "detail";
			this.detail.Style.Font.Name = "Calibri";
			// 
			// pictureBox2
			// 
			this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.3D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.pictureBox2.MimeType = "image/png";
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.6D), Telerik.Reporting.Drawing.Unit.Inch(0.9D));
			this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
			// 
			// table1
			// 
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.021D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.479D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.148D)));
			this.table1.Body.SetCellContent(0, 0, this.textBox2);
			this.table1.Body.SetCellContent(0, 1, this.textBox6);
			this.table1.Body.SetCellContent(1, 0, this.textBox3);
			this.table1.Body.SetCellContent(1, 1, this.textBox4);
			this.table1.Body.SetCellContent(2, 0, this.textBox7);
			this.table1.Body.SetCellContent(2, 1, this.textBox8);
			tableGroup1.Name = "tableGroup";
			tableGroup1.ReportItem = this.textBox1;
			tableGroup2.Name = "tableGroup2";
			tableGroup2.ReportItem = this.textBox5;
			this.table1.ColumnGroups.Add(tableGroup1);
			this.table1.ColumnGroups.Add(tableGroup2);
			this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.textBox6,
            this.textBox3,
            this.textBox4,
            this.textBox7,
            this.textBox8,
            this.textBox1,
            this.textBox5});
			this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
			this.table1.Name = "table1";
			tableGroup4.Name = "group";
			tableGroup5.Name = "group1";
			tableGroup6.Name = "group2";
			tableGroup3.ChildGroups.Add(tableGroup4);
			tableGroup3.ChildGroups.Add(tableGroup5);
			tableGroup3.ChildGroups.Add(tableGroup6);
			tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup3.Name = "detailTableGroup";
			this.table1.RowGroups.Add(tableGroup3);
			this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.5D), Telerik.Reporting.Drawing.Unit.Inch(0.828D));
			this.table1.Style.BorderColor.Default = System.Drawing.Color.DimGray;
			this.table1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table1.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table1.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
			// 
			// textBox2
			// 
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.021D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
			this.textBox2.Style.Font.Name = "Arial";
			this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox2.Value = "Service Order:#";
			// 
			// textBox6
			// 
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.479D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
			this.textBox6.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
			this.textBox6.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox6.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox6.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			// 
			// textBox3
			// 
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.021D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.textBox3.Style.Font.Name = "Arial";
			this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox3.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox3.StyleName = "";
			this.textBox3.Value = "NMI:";
			// 
			// textBox4
			// 
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.479D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.textBox4.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
			this.textBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox4.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox4.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox4.StyleName = "";
			// 
			// textBox7
			// 
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.021D), Telerik.Reporting.Drawing.Unit.Inch(0.148D));
			this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox7.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox7.StyleName = "";
			// 
			// textBox8
			// 
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.479D), Telerik.Reporting.Drawing.Unit.Inch(0.148D));
			this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox8.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox8.StyleName = "";
			this.textBox8.Value = "Retailer Use Only";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4D), Telerik.Reporting.Drawing.Unit.Inch(1.4D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.5D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.textBox9.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox9.Style.Font.Bold = true;
			this.textBox9.Style.Font.Name = "Arial";
			this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12.2D);
			this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox9.Value = "Application for service � Electrical Works Request (EWR) for new and existing ins" +
    "tallations";
			// 
			// table2
			// 
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.395D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.207D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.792D)));
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.207D)));
			this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.598D)));
			this.table2.Body.SetCellContent(0, 0, this.textBox11, 1, 4);
			tableGroup7.Name = "tableGroup1";
			tableGroup7.ReportItem = this.textBox10;
			tableGroup8.Name = "tableGroup3";
			tableGroup8.ReportItem = this.textBox12;
			tableGroup9.Name = "tableGroup4";
			tableGroup9.ReportItem = this.textBox14;
			tableGroup10.Name = "group3";
			tableGroup10.ReportItem = this.textBox16;
			this.table2.ColumnGroups.Add(tableGroup7);
			this.table2.ColumnGroups.Add(tableGroup8);
			this.table2.ColumnGroups.Add(tableGroup9);
			this.table2.ColumnGroups.Add(tableGroup10);
			this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox11,
            this.textBox10,
            this.textBox12,
            this.textBox14,
            this.textBox16});
			this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4D), Telerik.Reporting.Drawing.Unit.Inch(1.7D));
			this.table2.Name = "table2";
			tableGroup11.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup11.Name = "detailTableGroup1";
			this.table2.RowGroups.Add(tableGroup11);
			this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.601D), Telerik.Reporting.Drawing.Unit.Inch(0.806D));
			// 
			// textBox11
			// 
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.601D), Telerik.Reporting.Drawing.Unit.Inch(0.598D));
			this.textBox11.Style.Color = System.Drawing.Color.Black;
			this.textBox11.Style.Font.Name = "Arial";
			this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.textBox11.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox11.Value = resources.GetString("textBox11.Value");
			// 
			// table3
			// 
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.718D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.925D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.176D)));
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.78D)));
			this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.198D)));
			this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.179D)));
			this.table3.Body.SetCellContent(0, 0, this.textBox15);
			this.table3.Body.SetCellContent(0, 1, this.textBox19);
			this.table3.Body.SetCellContent(0, 2, this.textBox21);
			this.table3.Body.SetCellContent(0, 3, this.textBox23);
			this.table3.Body.SetCellContent(1, 0, this.textBox17);
			this.table3.Body.SetCellContent(1, 1, this.table44, 1, 3);
			tableGroup20.Name = "group5";
			tableGroup21.Name = "tableGroup6";
			tableGroup19.ChildGroups.Add(tableGroup20);
			tableGroup19.ChildGroups.Add(tableGroup21);
			tableGroup19.Name = "tableGroup5";
			tableGroup19.ReportItem = this.textBox13;
			tableGroup22.Name = "tableGroup7";
			tableGroup22.ReportItem = this.textBox20;
			tableGroup23.Name = "group4";
			tableGroup23.ReportItem = this.textBox22;
			this.table3.ColumnGroups.Add(tableGroup19);
			this.table3.ColumnGroups.Add(tableGroup22);
			this.table3.ColumnGroups.Add(tableGroup23);
			this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox15,
            this.textBox19,
            this.textBox21,
            this.textBox23,
            this.textBox17,
            this.table44,
            this.textBox13,
            this.textBox20,
            this.textBox22});
			this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4D), Telerik.Reporting.Drawing.Unit.Inch(2.6D));
			this.table3.Name = "table3";
			tableGroup25.Name = "group6";
			tableGroup26.Name = "group7";
			tableGroup24.ChildGroups.Add(tableGroup25);
			tableGroup24.ChildGroups.Add(tableGroup26);
			tableGroup24.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup24.Name = "detailTableGroup2";
			this.table3.RowGroups.Add(tableGroup24);
			this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.599D), Telerik.Reporting.Drawing.Unit.Inch(0.554D));
			this.table3.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(100)))), ((int)(((byte)(150)))));
			// 
			// textBox15
			// 
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.718D), Telerik.Reporting.Drawing.Unit.Inch(0.198D));
			this.textBox15.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox15.Value = "Customer or business name";
			// 
			// textBox19
			// 
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.925D), Telerik.Reporting.Drawing.Unit.Inch(0.198D));
			this.textBox19.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox19.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox19.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox19.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox19.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox19.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			// 
			// textBox21
			// 
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.176D), Telerik.Reporting.Drawing.Unit.Inch(0.198D));
			this.textBox21.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox21.Value = "Ph # / Mb #:";
			// 
			// textBox23
			// 
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.78D), Telerik.Reporting.Drawing.Unit.Inch(0.198D));
			this.textBox23.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox23.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox23.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox23.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox23.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox23.StyleName = "";
			// 
			// textBox17
			// 
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.718D), Telerik.Reporting.Drawing.Unit.Inch(0.179D));
			this.textBox17.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox17.Style.Font.Bold = true;
			this.textBox17.Style.Font.Name = "Calibri";
			this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.2D);
			this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox17.StyleName = "";
			this.textBox17.Value = "Site Type:";
			// 
			// table44
			// 
			this.table44.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.839D)));
			this.table44.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.73D)));
			this.table44.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.939D)));
			this.table44.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.679D)));
			this.table44.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.023D)));
			this.table44.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.672D)));
			this.table44.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.179D)));
			this.table44.Body.SetCellContent(0, 0, this.checkBox76);
			this.table44.Body.SetCellContent(0, 1, this.checkBox80);
			this.table44.Body.SetCellContent(0, 3, this.checkBox81);
			this.table44.Body.SetCellContent(0, 2, this.checkBox82);
			this.table44.Body.SetCellContent(0, 4, this.textBox24);
			this.table44.Body.SetCellContent(0, 5, this.textBox25);
			tableGroup12.Name = "tableGroup7";
			tableGroup13.Name = "tableGroup9";
			tableGroup14.Name = "group26";
			tableGroup15.Name = "group25";
			tableGroup16.Name = "group8";
			tableGroup17.Name = "group9";
			this.table44.ColumnGroups.Add(tableGroup12);
			this.table44.ColumnGroups.Add(tableGroup13);
			this.table44.ColumnGroups.Add(tableGroup14);
			this.table44.ColumnGroups.Add(tableGroup15);
			this.table44.ColumnGroups.Add(tableGroup16);
			this.table44.ColumnGroups.Add(tableGroup17);
			this.table44.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox76,
            this.checkBox80,
            this.checkBox82,
            this.checkBox81,
            this.textBox24,
            this.textBox25});
			this.table44.Name = "table44";
			tableGroup18.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup18.Name = "detailTableGroup3";
			this.table44.RowGroups.Add(tableGroup18);
			this.table44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.882D), Telerik.Reporting.Drawing.Unit.Inch(0.179D));
			this.table44.Style.BorderColor.Default = System.Drawing.Color.Silver;
			this.table44.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table44.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
			this.table44.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(7)))), ((int)(((byte)(106)))), ((int)(((byte)(183)))));
			this.table44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.table44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			// 
			// checkBox76
			// 
			this.checkBox76.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox76.CheckedImage = ((object)(resources.GetObject("checkBox76.CheckedImage")));
			this.checkBox76.IndeterminateImage = ((object)(resources.GetObject("checkBox76.IndeterminateImage")));
			this.checkBox76.Name = "checkBox76";
			this.checkBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.839D), Telerik.Reporting.Drawing.Unit.Inch(0.179D));
			this.checkBox76.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.checkBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.checkBox76.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.checkBox76.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.2D);
			this.checkBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox76.StyleName = "";
			this.checkBox76.Text = "Factory";
			this.checkBox76.UncheckedImage = ((object)(resources.GetObject("checkBox76.UncheckedImage")));
			this.checkBox76.Value = "";
			// 
			// checkBox80
			// 
			this.checkBox80.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox80.CheckedImage = ((object)(resources.GetObject("checkBox80.CheckedImage")));
			this.checkBox80.IndeterminateImage = ((object)(resources.GetObject("checkBox80.IndeterminateImage")));
			this.checkBox80.Name = "checkBox80";
			this.checkBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.73D), Telerik.Reporting.Drawing.Unit.Inch(0.179D));
			this.checkBox80.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.checkBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.checkBox80.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.checkBox80.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.2D);
			this.checkBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox80.StyleName = "";
			this.checkBox80.Text = "Suite";
			this.checkBox80.UncheckedImage = ((object)(resources.GetObject("checkBox80.UncheckedImage")));
			this.checkBox80.Value = "";
			// 
			// checkBox81
			// 
			this.checkBox81.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox81.CheckedImage = ((object)(resources.GetObject("checkBox81.CheckedImage")));
			this.checkBox81.IndeterminateImage = ((object)(resources.GetObject("checkBox81.IndeterminateImage")));
			this.checkBox81.Name = "checkBox81";
			this.checkBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.679D), Telerik.Reporting.Drawing.Unit.Inch(0.179D));
			this.checkBox81.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.checkBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.checkBox81.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.checkBox81.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.2D);
			this.checkBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox81.StyleName = "";
			this.checkBox81.Text = "Shop";
			this.checkBox81.UncheckedImage = ((object)(resources.GetObject("checkBox81.UncheckedImage")));
			this.checkBox81.Value = "";
			// 
			// checkBox82
			// 
			this.checkBox82.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox82.CheckedImage = ((object)(resources.GetObject("checkBox82.CheckedImage")));
			this.checkBox82.IndeterminateImage = ((object)(resources.GetObject("checkBox82.IndeterminateImage")));
			this.checkBox82.Name = "checkBox82";
			this.checkBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.939D), Telerik.Reporting.Drawing.Unit.Inch(0.179D));
			this.checkBox82.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.checkBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.checkBox82.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.checkBox82.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.2D);
			this.checkBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox82.StyleName = "";
			this.checkBox82.Text = " Tenancy";
			this.checkBox82.UncheckedImage = ((object)(resources.GetObject("checkBox82.UncheckedImage")));
			this.checkBox82.Value = "";
			// 
			// textBox24
			// 
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.023D), Telerik.Reporting.Drawing.Unit.Inch(0.179D));
			this.textBox24.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox24.StyleName = "";
			this.textBox24.Value = "Other";
			// 
			// textBox25
			// 
			this.textBox25.Name = "textBox25";
			this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.672D), Telerik.Reporting.Drawing.Unit.Inch(0.179D));
			this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
			this.textBox25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox25.StyleName = "";
			// 
			// table4
			// 
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.179D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.108D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.273D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.108D)));
			this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(4.932D)));
			this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.177D)));
			this.table4.Body.SetCellContent(0, 0, this.textBox27);
			this.table4.Body.SetCellContent(0, 2, this.textBox29);
			this.table4.Body.SetCellContent(0, 4, this.textBox31);
			this.table4.Body.SetCellContent(0, 1, this.textBox33);
			this.table4.Body.SetCellContent(0, 3, this.textBox35);
			tableGroup27.Name = "tableGroup8";
			tableGroup27.ReportItem = this.textBox26;
			tableGroup28.Name = "group10";
			tableGroup28.ReportItem = this.textBox32;
			tableGroup29.Name = "tableGroup10";
			tableGroup29.ReportItem = this.textBox28;
			tableGroup30.Name = "group11";
			tableGroup30.ReportItem = this.textBox34;
			tableGroup31.Name = "tableGroup11";
			tableGroup31.ReportItem = this.textBox30;
			this.table4.ColumnGroups.Add(tableGroup27);
			this.table4.ColumnGroups.Add(tableGroup28);
			this.table4.ColumnGroups.Add(tableGroup29);
			this.table4.ColumnGroups.Add(tableGroup30);
			this.table4.ColumnGroups.Add(tableGroup31);
			this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox27,
            this.textBox33,
            this.textBox29,
            this.textBox35,
            this.textBox31,
            this.textBox26,
            this.textBox32,
            this.textBox28,
            this.textBox34,
            this.textBox30});
			this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4D), Telerik.Reporting.Drawing.Unit.Inch(3.2D));
			this.table4.Name = "table4";
			tableGroup32.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup32.Name = "detailTableGroup4";
			this.table4.RowGroups.Add(tableGroup32);
			this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(0.354D));
			this.table4.Style.Color = System.Drawing.Color.MidnightBlue;
			// 
			// textBox27
			// 
			this.textBox27.Name = "textBox27";
			this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.179D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox27.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox27.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox27.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox27.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox27.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox27.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox27.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(170)))));
			// 
			// textBox29
			// 
			this.textBox29.Name = "textBox29";
			this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.273D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox29.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox29.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox29.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox29.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox29.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox29.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			// 
			// textBox31
			// 
			this.textBox31.Name = "textBox31";
			this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.932D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox31.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox31.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox31.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox31.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox31.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox31.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			// 
			// textBox33
			// 
			this.textBox33.Name = "textBox33";
			this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.108D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox33.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox33.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox33.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox33.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox33.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox33.StyleName = "";
			// 
			// textBox35
			// 
			this.textBox35.Name = "textBox35";
			this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.108D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox35.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox35.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox35.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox35.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox35.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox35.StyleName = "";
			// 
			// table5
			// 
			this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.592D)));
			this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.108D)));
			this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(4.2D)));
			this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.108D)));
			this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.592D)));
			this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.177D)));
			this.table5.Body.SetCellContent(0, 0, this.textBox36);
			this.table5.Body.SetCellContent(0, 2, this.textBox37);
			this.table5.Body.SetCellContent(0, 4, this.textBox38);
			this.table5.Body.SetCellContent(0, 1, this.textBox39);
			this.table5.Body.SetCellContent(0, 3, this.textBox40);
			tableGroup33.Name = "tableGroup8";
			tableGroup33.ReportItem = this.textBox41;
			tableGroup34.Name = "group10";
			tableGroup34.ReportItem = this.textBox42;
			tableGroup35.Name = "tableGroup10";
			tableGroup35.ReportItem = this.textBox43;
			tableGroup36.Name = "group11";
			tableGroup36.ReportItem = this.textBox44;
			tableGroup37.Name = "tableGroup11";
			tableGroup37.ReportItem = this.textBox45;
			this.table5.ColumnGroups.Add(tableGroup33);
			this.table5.ColumnGroups.Add(tableGroup34);
			this.table5.ColumnGroups.Add(tableGroup35);
			this.table5.ColumnGroups.Add(tableGroup36);
			this.table5.ColumnGroups.Add(tableGroup37);
			this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox36,
            this.textBox39,
            this.textBox37,
            this.textBox40,
            this.textBox38,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45});
			this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.402D), Telerik.Reporting.Drawing.Unit.Inch(3.6D));
			this.table5.Name = "table5";
			tableGroup38.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup38.Name = "detailTableGroup4";
			this.table5.RowGroups.Add(tableGroup38);
			this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(0.365D));
			this.table5.Style.Color = System.Drawing.Color.MidnightBlue;
			// 
			// textBox36
			// 
			this.textBox36.Name = "textBox36";
			this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.592D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox36.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox36.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox36.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox36.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox36.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			// 
			// textBox37
			// 
			this.textBox37.Name = "textBox37";
			this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.2D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox37.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox37.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox37.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox37.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox37.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox37.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			// 
			// textBox38
			// 
			this.textBox38.Name = "textBox38";
			this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.592D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox38.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox38.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox38.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox38.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox38.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox38.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			// 
			// textBox39
			// 
			this.textBox39.Name = "textBox39";
			this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.108D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox39.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox39.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox39.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox39.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox39.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox39.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox39.StyleName = "";
			// 
			// textBox40
			// 
			this.textBox40.Name = "textBox40";
			this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.108D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox40.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox40.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox40.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox40.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox40.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox40.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox40.StyleName = "";
			// 
			// table6
			// 
			this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.762D)));
			this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.054D)));
			this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.893D)));
			this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.109D)));
			this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.781D)));
			this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.177D)));
			this.table6.Body.SetCellContent(0, 0, this.textBox47);
			this.table6.Body.SetCellContent(0, 4, this.textBox51);
			this.table6.Body.SetCellContent(0, 2, this.textBox53);
			this.table6.Body.SetCellContent(0, 3, this.textBox46);
			this.table6.Body.SetCellContent(0, 1, this.table18);
			tableGroup42.Name = "tableGroup12";
			tableGroup43.Name = "tableGroup13";
			tableGroup44.Name = "group12";
			tableGroup45.Name = "group13";
			tableGroup46.Name = "tableGroup14";
			this.table6.ColumnGroups.Add(tableGroup42);
			this.table6.ColumnGroups.Add(tableGroup43);
			this.table6.ColumnGroups.Add(tableGroup44);
			this.table6.ColumnGroups.Add(tableGroup45);
			this.table6.ColumnGroups.Add(tableGroup46);
			this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox47,
            this.table18,
            this.textBox53,
            this.textBox46,
            this.textBox51});
			this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4D), Telerik.Reporting.Drawing.Unit.Inch(4.03D));
			this.table6.Name = "table6";
			tableGroup47.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup47.Name = "detailTableGroup5";
			this.table6.RowGroups.Add(tableGroup47);
			this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.599D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			// 
			// textBox47
			// 
			this.textBox47.Name = "textBox47";
			this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.762D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox47.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox47.Value = "Has contact been made with Projects Group?";
			// 
			// textBox51
			// 
			this.textBox51.Name = "textBox51";
			this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.781D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox51.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox51.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox51.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox51.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox51.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox51.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			// 
			// textBox53
			// 
			this.textBox53.Name = "textBox53";
			this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.893D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox53.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox53.StyleName = "";
			this.textBox53.Value = "Project number";
			// 
			// textBox46
			// 
			this.textBox46.Name = "textBox46";
			this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.109D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox46.StyleName = "";
			// 
			// table18
			// 
			this.table18.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.492D)));
			this.table18.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.478D)));
			this.table18.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.177D)));
			this.table18.Body.SetCellContent(0, 0, this.checkBox27);
			this.table18.Body.SetCellContent(0, 1, this.checkBox28);
			tableGroup39.Name = "tableGroup7";
			tableGroup40.Name = "tableGroup9";
			this.table18.ColumnGroups.Add(tableGroup39);
			this.table18.ColumnGroups.Add(tableGroup40);
			this.table18.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox27,
            this.checkBox28});
			this.table18.Name = "table18";
			tableGroup41.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup41.Name = "detailTableGroup3";
			this.table18.RowGroups.Add(tableGroup41);
			this.table18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.053D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.table18.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table18.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table18.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table18.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table18.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table18.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox27
			// 
			this.checkBox27.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox27.CheckedImage = ((object)(resources.GetObject("checkBox27.CheckedImage")));
			this.checkBox27.IndeterminateImage = ((object)(resources.GetObject("checkBox27.IndeterminateImage")));
			this.checkBox27.Name = "checkBox27";
			this.checkBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.492D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.checkBox27.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.checkBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox27.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox27.StyleName = "";
			this.checkBox27.Text = " Yes";
			this.checkBox27.UncheckedImage = ((object)(resources.GetObject("checkBox27.UncheckedImage")));
			this.checkBox27.Value = "";
			// 
			// checkBox28
			// 
			this.checkBox28.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox28.CheckedImage = ((object)(resources.GetObject("checkBox28.CheckedImage")));
			this.checkBox28.IndeterminateImage = ((object)(resources.GetObject("checkBox28.IndeterminateImage")));
			this.checkBox28.Name = "checkBox28";
			this.checkBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.478D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.checkBox28.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.checkBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox28.StyleName = "";
			this.checkBox28.Text = " No";
			this.checkBox28.UncheckedImage = ((object)(resources.GetObject("checkBox28.UncheckedImage")));
			this.checkBox28.Value = "";
			// 
			// table7
			// 
			this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.358D)));
			this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.14D)));
			this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(4.102D)));
			this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.177D)));
			this.table7.Body.SetCellContent(0, 0, this.textBox49);
			this.table7.Body.SetCellContent(0, 1, this.textBox52);
			this.table7.Body.SetCellContent(0, 2, this.textBox55);
			tableGroup48.Name = "tableGroup15";
			tableGroup49.Name = "tableGroup16";
			tableGroup50.Name = "tableGroup17";
			this.table7.ColumnGroups.Add(tableGroup48);
			this.table7.ColumnGroups.Add(tableGroup49);
			this.table7.ColumnGroups.Add(tableGroup50);
			this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox49,
            this.textBox52,
            this.textBox55});
			this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4D), Telerik.Reporting.Drawing.Unit.Inch(4.27D));
			this.table7.Name = "table7";
			tableGroup51.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup51.Name = "detailTableGroup6";
			this.table7.RowGroups.Add(tableGroup51);
			this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			// 
			// textBox49
			// 
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.358D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox49.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox49.Value = "Name of person at Distributors Projects Group to contact";
			// 
			// textBox52
			// 
			this.textBox52.Name = "textBox52";
			this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.14D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			// 
			// textBox55
			// 
			this.textBox55.Name = "textBox55";
			this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.102D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.textBox55.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox55.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox55.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox55.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox55.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox55.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			// 
			// table8
			// 
			this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(5.158D)));
			this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1D)));
			this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.442D)));
			this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.201D)));
			this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.201D)));
			this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.201D)));
			this.table8.Body.SetCellContent(2, 0, this.textBox62);
			this.table8.Body.SetCellContent(2, 1, this.textBox63);
			this.table8.Body.SetCellContent(2, 2, this.textBox64);
			this.table8.Body.SetCellContent(0, 0, this.textBox50, 1, 3);
			this.table8.Body.SetCellContent(1, 0, this.textBox59, 1, 3);
			tableGroup53.Name = "group16";
			tableGroup54.Name = "tableGroup19";
			tableGroup55.Name = "tableGroup20";
			tableGroup52.ChildGroups.Add(tableGroup53);
			tableGroup52.ChildGroups.Add(tableGroup54);
			tableGroup52.ChildGroups.Add(tableGroup55);
			tableGroup52.Name = "tableGroup18";
			tableGroup52.ReportItem = this.htmlTextBox1;
			this.table8.ColumnGroups.Add(tableGroup52);
			this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox50,
            this.textBox59,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.htmlTextBox1});
			this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4D), Telerik.Reporting.Drawing.Unit.Inch(4.52D));
			this.table8.Name = "table8";
			tableGroup56.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup56.Name = "detailTableGroup7";
			tableGroup57.Name = "group14";
			tableGroup58.Name = "group15";
			this.table8.RowGroups.Add(tableGroup56);
			this.table8.RowGroups.Add(tableGroup57);
			this.table8.RowGroups.Add(tableGroup58);
			this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(0.804D));
			// 
			// textBox62
			// 
			this.textBox62.Name = "textBox62";
			this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.158D), Telerik.Reporting.Drawing.Unit.Inch(0.201D));
			this.textBox62.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox62.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox62.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
			this.textBox62.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox62.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox62.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox62.StyleName = "";
			// 
			// textBox63
			// 
			this.textBox63.Name = "textBox63";
			this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.201D));
			this.textBox63.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox63.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox63.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox63.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox63.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox63.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox63.StyleName = "";
			this.textBox63.Value = "CES No";
			// 
			// textBox64
			// 
			this.textBox64.Name = "textBox64";
			this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.442D), Telerik.Reporting.Drawing.Unit.Inch(0.201D));
			this.textBox64.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox64.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox64.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox64.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox64.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox64.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox64.StyleName = "";
			// 
			// textBox50
			// 
			this.textBox50.Name = "textBox50";
			this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(0.201D));
			this.textBox50.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox50.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox50.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
			this.textBox50.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox50.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox50.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			// 
			// textBox59
			// 
			this.textBox59.Name = "textBox59";
			this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(0.201D));
			this.textBox59.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox59.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox59.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
			this.textBox59.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox59.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox59.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox59.StyleName = "";
			// 
			// panel1
			// 
			this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox56,
            this.table9,
            this.table12,
            this.panel2,
            this.table26,
            this.table27,
            this.table30,
            this.table33,
            this.table34,
            this.table36});
			this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4D), Telerik.Reporting.Drawing.Unit.Inch(5.4D));
			this.panel1.Name = "panel1";
			this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(4.304D));
			this.panel1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			// 
			// textBox56
			// 
			this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.textBox56.Name = "textBox56";
			this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.8D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.textBox56.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox56.Style.Font.Bold = true;
			this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.2D);
			this.textBox56.Value = "Work requirements";
			// 
			// table9
			// 
			this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.084D)));
			this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.627D)));
			this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.177D)));
			this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.158D)));
			this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
			this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.169D)));
			this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.315D)));
			this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.315D)));
			this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.148D)));
			this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.158D)));
			this.table9.Body.SetCellContent(5, 0, this.textBox70);
			this.table9.Body.SetCellContent(6, 0, this.textBox72);
			this.table9.Body.SetCellContent(7, 0, this.textBox74);
			this.table9.Body.SetCellContent(0, 0, this.checkBox1, 1, 2);
			this.table9.Body.SetCellContent(1, 0, this.checkBox2, 1, 2);
			this.table9.Body.SetCellContent(2, 0, this.checkBox3, 1, 2);
			this.table9.Body.SetCellContent(3, 0, this.checkBox4, 1, 2);
			this.table9.Body.SetCellContent(4, 0, this.checkBox5, 1, 2);
			this.table9.Body.SetCellContent(5, 1, this.table13);
			this.table9.Body.SetCellContent(6, 1, this.table10);
			this.table9.Body.SetCellContent(7, 1, this.table11);
			tableGroup69.Name = "group17";
			tableGroup70.Name = "tableGroup23";
			tableGroup68.ChildGroups.Add(tableGroup69);
			tableGroup68.ChildGroups.Add(tableGroup70);
			tableGroup68.Name = "tableGroup21";
			tableGroup68.ReportItem = this.textBox48;
			this.table9.ColumnGroups.Add(tableGroup68);
			this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox1,
            this.checkBox2,
            this.checkBox3,
            this.checkBox4,
            this.checkBox5,
            this.textBox70,
            this.table13,
            this.textBox72,
            this.table10,
            this.textBox74,
            this.table11,
            this.textBox48});
			this.table9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.table9.Name = "table9";
			tableGroup72.Name = "group18";
			tableGroup73.Name = "group19";
			tableGroup74.Name = "group20";
			tableGroup75.Name = "group21";
			tableGroup76.Name = "group22";
			tableGroup71.ChildGroups.Add(tableGroup72);
			tableGroup71.ChildGroups.Add(tableGroup73);
			tableGroup71.ChildGroups.Add(tableGroup74);
			tableGroup71.ChildGroups.Add(tableGroup75);
			tableGroup71.ChildGroups.Add(tableGroup76);
			tableGroup71.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup71.Name = "detailTableGroup8";
			tableGroup78.Name = "group24";
			tableGroup77.ChildGroups.Add(tableGroup78);
			tableGroup77.Name = "group23";
			tableGroup80.Name = "group28";
			tableGroup79.ChildGroups.Add(tableGroup80);
			tableGroup79.Name = "group27";
			tableGroup82.Name = "group30";
			tableGroup81.ChildGroups.Add(tableGroup82);
			tableGroup81.Name = "group29";
			this.table9.RowGroups.Add(tableGroup71);
			this.table9.RowGroups.Add(tableGroup77);
			this.table9.RowGroups.Add(tableGroup79);
			this.table9.RowGroups.Add(tableGroup81);
			this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(1.827D));
			this.table9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table9.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			// 
			// textBox70
			// 
			this.textBox70.Name = "textBox70";
			this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.084D), Telerik.Reporting.Drawing.Unit.Inch(0.315D));
			this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox70.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox70.StyleName = "";
			this.textBox70.Value = "Private Cables on\r\nPublic Land";
			// 
			// textBox72
			// 
			this.textBox72.Name = "textBox72";
			this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.084D), Telerik.Reporting.Drawing.Unit.Inch(0.148D));
			this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox72.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox72.StyleName = "";
			this.textBox72.Value = "Is EPV required?";
			// 
			// textBox74
			// 
			this.textBox74.Name = "textBox74";
			this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.084D), Telerik.Reporting.Drawing.Unit.Inch(0.158D));
			this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox74.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox74.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox74.StyleName = "";
			this.textBox74.Value = "Is Traffic Control req?";
			// 
			// checkBox1
			// 
			this.checkBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox1.CheckedImage = ((object)(resources.GetObject("checkBox1.CheckedImage")));
			this.checkBox1.IndeterminateImage = ((object)(resources.GetObject("checkBox1.IndeterminateImage")));
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.177D));
			this.checkBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox1.StyleName = "";
			this.checkBox1.Text = "Commercial / Industria";
			this.checkBox1.UncheckedImage = ((object)(resources.GetObject("checkBox1.UncheckedImage")));
			this.checkBox1.Value = "";
			// 
			// checkBox2
			// 
			this.checkBox2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox2.CheckedImage = ((object)(resources.GetObject("checkBox2.CheckedImage")));
			this.checkBox2.IndeterminateImage = ((object)(resources.GetObject("checkBox2.IndeterminateImage")));
			this.checkBox2.Name = "checkBox2";
			this.checkBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.158D));
			this.checkBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox2.StyleName = "";
			this.checkBox2.Text = "Residential";
			this.checkBox2.UncheckedImage = ((object)(resources.GetObject("checkBox2.UncheckedImage")));
			this.checkBox2.Value = "";
			// 
			// checkBox3
			// 
			this.checkBox3.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox3.CheckedImage = ((object)(resources.GetObject("checkBox3.CheckedImage")));
			this.checkBox3.IndeterminateImage = ((object)(resources.GetObject("checkBox3.IndeterminateImage")));
			this.checkBox3.Name = "checkBox3";
			this.checkBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.checkBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox3.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox3.StyleName = "";
			this.checkBox3.Text = "Unmetered Supply";
			this.checkBox3.UncheckedImage = ((object)(resources.GetObject("checkBox3.UncheckedImage")));
			this.checkBox3.Value = "";
			// 
			// checkBox4
			// 
			this.checkBox4.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox4.CheckedImage = ((object)(resources.GetObject("checkBox4.CheckedImage")));
			this.checkBox4.IndeterminateImage = ((object)(resources.GetObject("checkBox4.IndeterminateImage")));
			this.checkBox4.Name = "checkBox4";
			this.checkBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.169D));
			this.checkBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox4.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox4.StyleName = "";
			this.checkBox4.Text = "Builders Supply Pole (BSP)";
			this.checkBox4.UncheckedImage = ((object)(resources.GetObject("checkBox4.UncheckedImage")));
			this.checkBox4.Value = "";
			// 
			// checkBox5
			// 
			this.checkBox5.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox5.CheckedImage = ((object)(resources.GetObject("checkBox5.CheckedImage")));
			this.checkBox5.IndeterminateImage = ((object)(resources.GetObject("checkBox5.IndeterminateImage")));
			this.checkBox5.Name = "checkBox5";
			this.checkBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.315D));
			this.checkBox5.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.checkBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox5.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox5.StyleName = "";
			this.checkBox5.Text = "Builders Supply in the Permanent Position";
			this.checkBox5.UncheckedImage = ((object)(resources.GetObject("checkBox5.UncheckedImage")));
			this.checkBox5.Value = "";
			// 
			// table13
			// 
			this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.268D)));
			this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.275D)));
			this.table13.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.315D)));
			this.table13.Body.SetCellContent(0, 0, this.checkBox22);
			this.table13.Body.SetCellContent(0, 1, this.checkBox23);
			tableGroup59.Name = "tableGroup7";
			tableGroup60.Name = "tableGroup9";
			this.table13.ColumnGroups.Add(tableGroup59);
			this.table13.ColumnGroups.Add(tableGroup60);
			this.table13.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox22,
            this.checkBox23});
			this.table13.Name = "table13";
			tableGroup61.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup61.Name = "detailTableGroup3";
			this.table13.RowGroups.Add(tableGroup61);
			this.table13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.626D), Telerik.Reporting.Drawing.Unit.Inch(0.315D));
			this.table13.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table13.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table13.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table13.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table13.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table13.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table13.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox22
			// 
			this.checkBox22.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox22.CheckedImage = ((object)(resources.GetObject("checkBox22.CheckedImage")));
			this.checkBox22.IndeterminateImage = ((object)(resources.GetObject("checkBox22.IndeterminateImage")));
			this.checkBox22.Name = "checkBox22";
			this.checkBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.268D), Telerik.Reporting.Drawing.Unit.Inch(0.315D));
			this.checkBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.checkBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox22.StyleName = "";
			this.checkBox22.Text = " Y";
			this.checkBox22.UncheckedImage = ((object)(resources.GetObject("checkBox22.UncheckedImage")));
			this.checkBox22.Value = "";
			// 
			// checkBox23
			// 
			this.checkBox23.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox23.CheckedImage = ((object)(resources.GetObject("checkBox23.CheckedImage")));
			this.checkBox23.IndeterminateImage = ((object)(resources.GetObject("checkBox23.IndeterminateImage")));
			this.checkBox23.Name = "checkBox23";
			this.checkBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.275D), Telerik.Reporting.Drawing.Unit.Inch(0.315D));
			this.checkBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.checkBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox23.StyleName = "";
			this.checkBox23.Text = " N";
			this.checkBox23.UncheckedImage = ((object)(resources.GetObject("checkBox23.UncheckedImage")));
			this.checkBox23.Value = "";
			// 
			// table10
			// 
			this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.268D)));
			this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.276D)));
			this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.148D)));
			this.table10.Body.SetCellContent(0, 0, this.checkBox6);
			this.table10.Body.SetCellContent(0, 1, this.checkBox7);
			tableGroup62.Name = "tableGroup7";
			tableGroup63.Name = "tableGroup9";
			this.table10.ColumnGroups.Add(tableGroup62);
			this.table10.ColumnGroups.Add(tableGroup63);
			this.table10.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox6,
            this.checkBox7});
			this.table10.Name = "table10";
			tableGroup64.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup64.Name = "detailTableGroup3";
			this.table10.RowGroups.Add(tableGroup64);
			this.table10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.627D), Telerik.Reporting.Drawing.Unit.Inch(0.148D));
			this.table10.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table10.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table10.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table10.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table10.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table10.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table10.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox6
			// 
			this.checkBox6.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox6.CheckedImage = ((object)(resources.GetObject("checkBox6.CheckedImage")));
			this.checkBox6.IndeterminateImage = ((object)(resources.GetObject("checkBox6.IndeterminateImage")));
			this.checkBox6.Name = "checkBox6";
			this.checkBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.268D), Telerik.Reporting.Drawing.Unit.Inch(0.148D));
			this.checkBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.checkBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox6.StyleName = "";
			this.checkBox6.Text = " Y";
			this.checkBox6.UncheckedImage = ((object)(resources.GetObject("checkBox6.UncheckedImage")));
			this.checkBox6.Value = "";
			// 
			// checkBox7
			// 
			this.checkBox7.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox7.CheckedImage = ((object)(resources.GetObject("checkBox7.CheckedImage")));
			this.checkBox7.IndeterminateImage = ((object)(resources.GetObject("checkBox7.IndeterminateImage")));
			this.checkBox7.Name = "checkBox7";
			this.checkBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.276D), Telerik.Reporting.Drawing.Unit.Inch(0.148D));
			this.checkBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.checkBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox7.StyleName = "";
			this.checkBox7.Text = " N";
			this.checkBox7.UncheckedImage = ((object)(resources.GetObject("checkBox7.UncheckedImage")));
			this.checkBox7.Value = "";
			// 
			// table11
			// 
			this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.268D)));
			this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.276D)));
			this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.158D)));
			this.table11.Body.SetCellContent(0, 0, this.checkBox8);
			this.table11.Body.SetCellContent(0, 1, this.checkBox9);
			tableGroup65.Name = "tableGroup7";
			tableGroup66.Name = "tableGroup9";
			this.table11.ColumnGroups.Add(tableGroup65);
			this.table11.ColumnGroups.Add(tableGroup66);
			this.table11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox8,
            this.checkBox9});
			this.table11.Name = "table11";
			tableGroup67.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup67.Name = "detailTableGroup3";
			this.table11.RowGroups.Add(tableGroup67);
			this.table11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.627D), Telerik.Reporting.Drawing.Unit.Inch(0.158D));
			this.table11.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table11.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table11.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table11.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table11.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox8
			// 
			this.checkBox8.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox8.CheckedImage = ((object)(resources.GetObject("checkBox8.CheckedImage")));
			this.checkBox8.IndeterminateImage = ((object)(resources.GetObject("checkBox8.IndeterminateImage")));
			this.checkBox8.Name = "checkBox8";
			this.checkBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.268D), Telerik.Reporting.Drawing.Unit.Inch(0.158D));
			this.checkBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.checkBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox8.StyleName = "";
			this.checkBox8.Text = " Y";
			this.checkBox8.UncheckedImage = ((object)(resources.GetObject("checkBox8.UncheckedImage")));
			this.checkBox8.Value = "";
			// 
			// checkBox9
			// 
			this.checkBox9.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox9.CheckedImage = ((object)(resources.GetObject("checkBox9.CheckedImage")));
			this.checkBox9.IndeterminateImage = ((object)(resources.GetObject("checkBox9.IndeterminateImage")));
			this.checkBox9.Name = "checkBox9";
			this.checkBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.276D), Telerik.Reporting.Drawing.Unit.Inch(0.158D));
			this.checkBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.checkBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox9.StyleName = "";
			this.checkBox9.Text = " N";
			this.checkBox9.UncheckedImage = ((object)(resources.GetObject("checkBox9.UncheckedImage")));
			this.checkBox9.Value = "";
			// 
			// table12
			// 
			this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.666D)));
			this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.552D)));
			this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.281D)));
			this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.178D)));
			this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.159D)));
			this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.201D)));
			this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17D)));
			this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17D)));
			this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17D)));
			this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18D)));
			this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.253D)));
			this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.157D)));
			this.table12.Body.SetCellContent(4, 0, this.textBox61);
			this.table12.Body.SetCellContent(4, 1, this.textBox65);
			this.table12.Body.SetCellContent(4, 2, this.textBox73);
			this.table12.Body.SetCellContent(0, 0, this.checkBox10, 1, 3);
			this.table12.Body.SetCellContent(1, 0, this.checkBox11, 1, 3);
			this.table12.Body.SetCellContent(2, 0, this.checkBox12, 1, 3);
			this.table12.Body.SetCellContent(3, 0, this.checkBox13, 1, 3);
			this.table12.Body.SetCellContent(5, 0, this.textBox67);
			this.table12.Body.SetCellContent(5, 1, this.textBox68);
			this.table12.Body.SetCellContent(5, 2, this.textBox69);
			this.table12.Body.SetCellContent(6, 0, this.textBox54, 1, 3);
			this.table12.Body.SetCellContent(8, 0, this.textBox58, 1, 3);
			this.table12.Body.SetCellContent(7, 0, this.table14, 1, 3);
			tableGroup86.Name = "group17";
			tableGroup87.Name = "tableGroup23";
			tableGroup85.ChildGroups.Add(tableGroup86);
			tableGroup85.ChildGroups.Add(tableGroup87);
			tableGroup85.Name = "tableGroup21";
			tableGroup85.ReportItem = this.textBox60;
			tableGroup89.Name = "group32";
			tableGroup88.ChildGroups.Add(tableGroup89);
			tableGroup88.Name = "group31";
			tableGroup88.ReportItem = this.textBox66;
			this.table12.ColumnGroups.Add(tableGroup85);
			this.table12.ColumnGroups.Add(tableGroup88);
			this.table12.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox10,
            this.checkBox11,
            this.checkBox12,
            this.checkBox13,
            this.textBox61,
            this.textBox65,
            this.textBox73,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox54,
            this.table14,
            this.textBox58,
            this.textBox60,
            this.textBox66});
			this.table12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.8D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.table12.Name = "table12";
			tableGroup91.Name = "group18";
			tableGroup92.Name = "group19";
			tableGroup93.Name = "group20";
			tableGroup94.Name = "group21";
			tableGroup95.Name = "group22";
			tableGroup96.Name = "group33";
			tableGroup90.ChildGroups.Add(tableGroup91);
			tableGroup90.ChildGroups.Add(tableGroup92);
			tableGroup90.ChildGroups.Add(tableGroup93);
			tableGroup90.ChildGroups.Add(tableGroup94);
			tableGroup90.ChildGroups.Add(tableGroup95);
			tableGroup90.ChildGroups.Add(tableGroup96);
			tableGroup90.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup90.Name = "detailTableGroup8";
			tableGroup98.Name = "group24";
			tableGroup97.ChildGroups.Add(tableGroup98);
			tableGroup97.Name = "group23";
			tableGroup100.Name = "group28";
			tableGroup99.ChildGroups.Add(tableGroup100);
			tableGroup99.Name = "group27";
			tableGroup102.Name = "group30";
			tableGroup101.ChildGroups.Add(tableGroup102);
			tableGroup101.Name = "group29";
			this.table12.RowGroups.Add(tableGroup90);
			this.table12.RowGroups.Add(tableGroup97);
			this.table12.RowGroups.Add(tableGroup99);
			this.table12.RowGroups.Add(tableGroup101);
			this.table12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.499D), Telerik.Reporting.Drawing.Unit.Inch(1.826D));
			this.table12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table12.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			// 
			// textBox61
			// 
			this.textBox61.Name = "textBox61";
			this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.666D), Telerik.Reporting.Drawing.Unit.Inch(0.17D));
			this.textBox61.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox61.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox61.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox61.StyleName = "";
			this.textBox61.Value = "Solar Panel";
			// 
			// textBox65
			// 
			this.textBox65.Name = "textBox65";
			this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.552D), Telerik.Reporting.Drawing.Unit.Inch(0.17D));
			this.textBox65.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox65.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox65.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox65.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox65.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox65.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox65.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox65.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox65.StyleName = "";
			// 
			// textBox73
			// 
			this.textBox73.Name = "textBox73";
			this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.281D), Telerik.Reporting.Drawing.Unit.Inch(0.17D));
			this.textBox73.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox73.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox73.StyleName = "";
			this.textBox73.Value = "kW";
			// 
			// checkBox10
			// 
			this.checkBox10.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox10.CheckedImage = ((object)(resources.GetObject("checkBox10.CheckedImage")));
			this.checkBox10.IndeterminateImage = ((object)(resources.GetObject("checkBox10.IndeterminateImage")));
			this.checkBox10.Name = "checkBox10";
			this.checkBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.499D), Telerik.Reporting.Drawing.Unit.Inch(0.178D));
			this.checkBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox10.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox10.StyleName = "";
			this.checkBox10.Text = "New";
			this.checkBox10.UncheckedImage = ((object)(resources.GetObject("checkBox10.UncheckedImage")));
			this.checkBox10.Value = "";
			// 
			// checkBox11
			// 
			this.checkBox11.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox11.CheckedImage = ((object)(resources.GetObject("checkBox11.CheckedImage")));
			this.checkBox11.IndeterminateImage = ((object)(resources.GetObject("checkBox11.IndeterminateImage")));
			this.checkBox11.Name = "checkBox11";
			this.checkBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.499D), Telerik.Reporting.Drawing.Unit.Inch(0.159D));
			this.checkBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox11.StyleName = "";
			this.checkBox11.Text = "Alteration";
			this.checkBox11.UncheckedImage = ((object)(resources.GetObject("checkBox11.UncheckedImage")));
			this.checkBox11.Value = "";
			// 
			// checkBox12
			// 
			this.checkBox12.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox12.CheckedImage = ((object)(resources.GetObject("checkBox12.CheckedImage")));
			this.checkBox12.IndeterminateImage = ((object)(resources.GetObject("checkBox12.IndeterminateImage")));
			this.checkBox12.Name = "checkBox12";
			this.checkBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.499D), Telerik.Reporting.Drawing.Unit.Inch(0.201D));
			this.checkBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox12.StyleName = "";
			this.checkBox12.Text = "Private Overhead";
			this.checkBox12.UncheckedImage = ((object)(resources.GetObject("checkBox12.UncheckedImage")));
			this.checkBox12.Value = "";
			// 
			// checkBox13
			// 
			this.checkBox13.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox13.CheckedImage = ((object)(resources.GetObject("checkBox13.CheckedImage")));
			this.checkBox13.IndeterminateImage = ((object)(resources.GetObject("checkBox13.IndeterminateImage")));
			this.checkBox13.Name = "checkBox13";
			this.checkBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.499D), Telerik.Reporting.Drawing.Unit.Inch(0.17D));
			this.checkBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox13.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox13.StyleName = "";
			this.checkBox13.Text = "Embedded Network";
			this.checkBox13.UncheckedImage = ((object)(resources.GetObject("checkBox13.UncheckedImage")));
			this.checkBox13.Value = "";
			// 
			// textBox67
			// 
			this.textBox67.Name = "textBox67";
			this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.666D), Telerik.Reporting.Drawing.Unit.Inch(0.17D));
			this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox67.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox67.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox67.StyleName = "";
			this.textBox67.Value = "Inverter";
			// 
			// textBox68
			// 
			this.textBox68.Name = "textBox68";
			this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.552D), Telerik.Reporting.Drawing.Unit.Inch(0.17D));
			this.textBox68.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox68.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox68.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox68.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox68.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox68.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox68.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox68.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox68.StyleName = "";
			// 
			// textBox69
			// 
			this.textBox69.Name = "textBox69";
			this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.281D), Telerik.Reporting.Drawing.Unit.Inch(0.17D));
			this.textBox69.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox69.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox69.StyleName = "";
			this.textBox69.Value = "kW";
			// 
			// textBox54
			// 
			this.textBox54.Name = "textBox54";
			this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.499D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
			this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox54.StyleName = "";
			this.textBox54.Value = "Alternative supply";
			// 
			// textBox58
			// 
			this.textBox58.Name = "textBox58";
			this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.499D), Telerik.Reporting.Drawing.Unit.Inch(0.157D));
			this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox58.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox58.StyleName = "";
			// 
			// table14
			// 
			this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.36D)));
			this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.253D)));
			this.table14.Body.SetCellContent(0, 0, this.textBox75);
			tableGroup83.Name = "tableGroup22";
			this.table14.ColumnGroups.Add(tableGroup83);
			this.table14.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox75});
			this.table14.Name = "table14";
			tableGroup84.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup84.Name = "detailTableGroup9";
			this.table14.RowGroups.Add(tableGroup84);
			this.table14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.499D), Telerik.Reporting.Drawing.Unit.Inch(0.253D));
			this.table14.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.table14.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table14.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
			this.table14.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table14.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table14.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table14.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table14.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.table14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.table14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table14.StyleName = "";
			// 
			// textBox75
			// 
			this.textBox75.Name = "textBox75";
			this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.36D), Telerik.Reporting.Drawing.Unit.Inch(0.253D));
			this.textBox75.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox75.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			// 
			// panel2
			// 
			this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table15});
			this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.4D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.panel2.Name = "panel2";
			this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.2D), Telerik.Reporting.Drawing.Unit.Inch(1.826D));
			// 
			// table15
			// 
			this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.926D)));
			this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.25D)));
			this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.729D)));
			this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.292D)));
			this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18D)));
			this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.161D)));
			this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.203D)));
			this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.172D)));
			this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.172D)));
			this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.172D)));
			this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.182D)));
			this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.203D)));
			this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19D)));
			this.table15.Body.SetCellContent(0, 0, this.checkBox14);
			this.table15.Body.SetCellContent(1, 0, this.checkBox15);
			this.table15.Body.SetCellContent(2, 0, this.checkBox16);
			this.table15.Body.SetCellContent(3, 0, this.checkBox17);
			this.table15.Body.SetCellContent(8, 0, this.textBox81);
			this.table15.Body.SetCellContent(0, 2, this.textBox86);
			this.table15.Body.SetCellContent(1, 2, this.textBox87);
			this.table15.Body.SetCellContent(8, 2, this.textBox94);
			this.table15.Body.SetCellContent(0, 1, this.textBox96);
			this.table15.Body.SetCellContent(1, 1, this.textBox97);
			this.table15.Body.SetCellContent(8, 1, this.textBox104);
			this.table15.Body.SetCellContent(0, 3, this.textBox76);
			this.table15.Body.SetCellContent(1, 3, this.textBox78);
			this.table15.Body.SetCellContent(8, 3, this.textBox109);
			this.table15.Body.SetCellContent(4, 0, this.checkBox18);
			this.table15.Body.SetCellContent(2, 1, this.table20, 1, 3);
			this.table15.Body.SetCellContent(3, 1, this.textBox99, 1, 3);
			this.table15.Body.SetCellContent(4, 1, this.table19, 1, 3);
			this.table15.Body.SetCellContent(5, 0, this.table22, 1, 4);
			this.table15.Body.SetCellContent(6, 0, this.table24, 1, 4);
			this.table15.Body.SetCellContent(7, 0, this.table16, 1, 4);
			tableGroup142.Name = "group17";
			tableGroup143.Name = "group37";
			tableGroup141.ChildGroups.Add(tableGroup142);
			tableGroup141.ChildGroups.Add(tableGroup143);
			tableGroup141.Name = "tableGroup21";
			tableGroup141.ReportItem = this.textBox83;
			tableGroup145.Name = "group35";
			tableGroup144.ChildGroups.Add(tableGroup145);
			tableGroup144.Name = "group34";
			tableGroup144.ReportItem = this.textBox85;
			tableGroup147.Name = "group39";
			tableGroup146.ChildGroups.Add(tableGroup147);
			tableGroup146.Name = "group38";
			tableGroup146.ReportItem = this.textBox71;
			this.table15.ColumnGroups.Add(tableGroup141);
			this.table15.ColumnGroups.Add(tableGroup144);
			this.table15.ColumnGroups.Add(tableGroup146);
			this.table15.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox14,
            this.textBox96,
            this.textBox86,
            this.textBox76,
            this.checkBox15,
            this.textBox97,
            this.textBox87,
            this.textBox78,
            this.checkBox16,
            this.table20,
            this.checkBox17,
            this.textBox99,
            this.checkBox18,
            this.table19,
            this.table22,
            this.table24,
            this.table16,
            this.textBox81,
            this.textBox104,
            this.textBox94,
            this.textBox109,
            this.textBox83,
            this.textBox85,
            this.textBox71});
			this.table15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
			this.table15.Name = "table15";
			tableGroup149.Name = "group18";
			tableGroup150.Name = "group19";
			tableGroup151.Name = "group20";
			tableGroup152.Name = "group21";
			tableGroup153.Name = "group22";
			tableGroup154.Name = "group33";
			tableGroup148.ChildGroups.Add(tableGroup149);
			tableGroup148.ChildGroups.Add(tableGroup150);
			tableGroup148.ChildGroups.Add(tableGroup151);
			tableGroup148.ChildGroups.Add(tableGroup152);
			tableGroup148.ChildGroups.Add(tableGroup153);
			tableGroup148.ChildGroups.Add(tableGroup154);
			tableGroup148.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup148.Name = "detailTableGroup8";
			tableGroup156.Name = "group24";
			tableGroup155.ChildGroups.Add(tableGroup156);
			tableGroup155.Name = "group23";
			tableGroup158.Name = "group28";
			tableGroup157.ChildGroups.Add(tableGroup158);
			tableGroup157.Name = "group27";
			tableGroup160.Name = "group30";
			tableGroup159.ChildGroups.Add(tableGroup160);
			tableGroup159.Name = "group29";
			this.table15.RowGroups.Add(tableGroup148);
			this.table15.RowGroups.Add(tableGroup155);
			this.table15.RowGroups.Add(tableGroup157);
			this.table15.RowGroups.Add(tableGroup159);
			this.table15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.197D), Telerik.Reporting.Drawing.Unit.Inch(1.825D));
			this.table15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table15.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table15.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			// 
			// checkBox14
			// 
			this.checkBox14.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox14.CheckedImage = ((object)(resources.GetObject("checkBox14.CheckedImage")));
			this.checkBox14.IndeterminateImage = ((object)(resources.GetObject("checkBox14.IndeterminateImage")));
			this.checkBox14.Name = "checkBox14";
			this.checkBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.926D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
			this.checkBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox14.StyleName = "";
			this.checkBox14.Text = "Overhead";
			this.checkBox14.UncheckedImage = ((object)(resources.GetObject("checkBox14.UncheckedImage")));
			this.checkBox14.Value = "";
			// 
			// checkBox15
			// 
			this.checkBox15.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox15.CheckedImage = ((object)(resources.GetObject("checkBox15.CheckedImage")));
			this.checkBox15.IndeterminateImage = ((object)(resources.GetObject("checkBox15.IndeterminateImage")));
			this.checkBox15.Name = "checkBox15";
			this.checkBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.926D), Telerik.Reporting.Drawing.Unit.Inch(0.161D));
			this.checkBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox15.StyleName = "";
			this.checkBox15.Text = "Underground";
			this.checkBox15.UncheckedImage = ((object)(resources.GetObject("checkBox15.UncheckedImage")));
			this.checkBox15.Value = "";
			// 
			// checkBox16
			// 
			this.checkBox16.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox16.CheckedImage = ((object)(resources.GetObject("checkBox16.CheckedImage")));
			this.checkBox16.IndeterminateImage = ((object)(resources.GetObject("checkBox16.IndeterminateImage")));
			this.checkBox16.Name = "checkBox16";
			this.checkBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.926D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.checkBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox16.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox16.StyleName = "";
			this.checkBox16.Text = "Pole to pit";
			this.checkBox16.UncheckedImage = ((object)(resources.GetObject("checkBox16.UncheckedImage")));
			this.checkBox16.Value = "";
			// 
			// checkBox17
			// 
			this.checkBox17.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox17.CheckedImage = ((object)(resources.GetObject("checkBox17.CheckedImage")));
			this.checkBox17.IndeterminateImage = ((object)(resources.GetObject("checkBox17.IndeterminateImage")));
			this.checkBox17.Name = "checkBox17";
			this.checkBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.926D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.checkBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox17.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox17.StyleName = "";
			this.checkBox17.Text = "URD";
			this.checkBox17.UncheckedImage = ((object)(resources.GetObject("checkBox17.UncheckedImage")));
			this.checkBox17.Value = "";
			// 
			// textBox81
			// 
			this.textBox81.Name = "textBox81";
			this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.926D), Telerik.Reporting.Drawing.Unit.Inch(0.19D));
			this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox81.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox81.StyleName = "";
			// 
			// textBox86
			// 
			this.textBox86.Name = "textBox86";
			this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.729D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
			this.textBox86.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox86.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox86.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox86.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox86.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox86.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox86.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox86.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox86.StyleName = "";
			// 
			// textBox87
			// 
			this.textBox87.Name = "textBox87";
			this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.729D), Telerik.Reporting.Drawing.Unit.Inch(0.161D));
			this.textBox87.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox87.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox87.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox87.StyleName = "";
			// 
			// textBox94
			// 
			this.textBox94.Name = "textBox94";
			this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.729D), Telerik.Reporting.Drawing.Unit.Inch(0.19D));
			this.textBox94.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox94.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox94.StyleName = "";
			// 
			// textBox96
			// 
			this.textBox96.Name = "textBox96";
			this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.25D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
			this.textBox96.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox96.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox96.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox96.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox96.StyleName = "";
			this.textBox96.Value = "Length of Overhead Cable";
			// 
			// textBox97
			// 
			this.textBox97.Name = "textBox97";
			this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.25D), Telerik.Reporting.Drawing.Unit.Inch(0.161D));
			this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox97.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox97.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox97.StyleName = "";
			// 
			// textBox104
			// 
			this.textBox104.Name = "textBox104";
			this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.25D), Telerik.Reporting.Drawing.Unit.Inch(0.19D));
			this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox104.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox104.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox104.StyleName = "";
			// 
			// textBox76
			// 
			this.textBox76.Name = "textBox76";
			this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.292D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
			this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox76.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox76.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox76.StyleName = "";
			this.textBox76.Value = "Approximate meters";
			// 
			// textBox78
			// 
			this.textBox78.Name = "textBox78";
			this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.292D), Telerik.Reporting.Drawing.Unit.Inch(0.161D));
			this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox78.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox78.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox78.StyleName = "";
			// 
			// textBox109
			// 
			this.textBox109.Name = "textBox109";
			this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.292D), Telerik.Reporting.Drawing.Unit.Inch(0.19D));
			this.textBox109.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox109.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox109.StyleName = "";
			// 
			// checkBox18
			// 
			this.checkBox18.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox18.CheckedImage = ((object)(resources.GetObject("checkBox18.CheckedImage")));
			this.checkBox18.IndeterminateImage = ((object)(resources.GetObject("checkBox18.IndeterminateImage")));
			this.checkBox18.Name = "checkBox18";
			this.checkBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.926D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.checkBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox18.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox18.StyleName = "";
			this.checkBox18.Text = "Substation";
			this.checkBox18.UncheckedImage = ((object)(resources.GetObject("checkBox18.UncheckedImage")));
			this.checkBox18.Value = "";
			// 
			// table20
			// 
			this.table20.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.204D)));
			this.table20.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.757D)));
			this.table20.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.31D)));
			this.table20.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.203D)));
			this.table20.Body.SetCellContent(0, 0, this.textBox79);
			this.table20.Body.SetCellContent(0, 1, this.table21);
			this.table20.Body.SetCellContent(0, 2, this.checkBox26);
			tableGroup106.Name = "tableGroup24";
			tableGroup107.Name = "tableGroup25";
			tableGroup108.Name = "tableGroup26";
			this.table20.ColumnGroups.Add(tableGroup106);
			this.table20.ColumnGroups.Add(tableGroup107);
			this.table20.ColumnGroups.Add(tableGroup108);
			this.table20.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox79,
            this.table21,
            this.checkBox26});
			this.table20.Name = "table20";
			tableGroup109.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup109.Name = "detailTableGroup10";
			this.table20.RowGroups.Add(tableGroup109);
			this.table20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.271D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			// 
			// textBox79
			// 
			this.textBox79.Name = "textBox79";
			this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.204D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.textBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox79.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox79.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox79.StyleName = "";
			this.textBox79.Value = "as pit been installed?";
			// 
			// table21
			// 
			this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.332D)));
			this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.341D)));
			this.table21.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.203D)));
			this.table21.Body.SetCellContent(0, 0, this.checkBox24);
			this.table21.Body.SetCellContent(0, 1, this.checkBox25);
			tableGroup103.Name = "tableGroup7";
			tableGroup104.Name = "tableGroup9";
			this.table21.ColumnGroups.Add(tableGroup103);
			this.table21.ColumnGroups.Add(tableGroup104);
			this.table21.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox24,
            this.checkBox25});
			this.table21.Name = "table21";
			tableGroup105.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup105.Name = "detailTableGroup3";
			this.table21.RowGroups.Add(tableGroup105);
			this.table21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.756D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.table21.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table21.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table21.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table21.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table21.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table21.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table21.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table21.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox24
			// 
			this.checkBox24.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox24.CheckedImage = ((object)(resources.GetObject("checkBox24.CheckedImage")));
			this.checkBox24.IndeterminateImage = ((object)(resources.GetObject("checkBox24.IndeterminateImage")));
			this.checkBox24.Name = "checkBox24";
			this.checkBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.332D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.checkBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox24.StyleName = "";
			this.checkBox24.Text = " Y";
			this.checkBox24.UncheckedImage = ((object)(resources.GetObject("checkBox24.UncheckedImage")));
			this.checkBox24.Value = "";
			// 
			// checkBox25
			// 
			this.checkBox25.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox25.CheckedImage = ((object)(resources.GetObject("checkBox25.CheckedImage")));
			this.checkBox25.IndeterminateImage = ((object)(resources.GetObject("checkBox25.IndeterminateImage")));
			this.checkBox25.Name = "checkBox25";
			this.checkBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.341D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.checkBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox25.StyleName = "";
			this.checkBox25.Text = " N";
			this.checkBox25.UncheckedImage = ((object)(resources.GetObject("checkBox25.UncheckedImage")));
			this.checkBox25.Value = "";
			// 
			// checkBox26
			// 
			this.checkBox26.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox26.CheckedImage = ((object)(resources.GetObject("checkBox26.CheckedImage")));
			this.checkBox26.IndeterminateImage = ((object)(resources.GetObject("checkBox26.IndeterminateImage")));
			this.checkBox26.Name = "checkBox26";
			this.checkBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.31D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.checkBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.checkBox26.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox26.StyleName = "";
			this.checkBox26.Text = "O/H to U/G Conversion";
			this.checkBox26.UncheckedImage = ((object)(resources.GetObject("checkBox26.UncheckedImage")));
			this.checkBox26.Value = "";
			// 
			// textBox99
			// 
			this.textBox99.Name = "textBox99";
			this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.271D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox99.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox99.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox99.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox99.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox99.StyleName = "";
			this.textBox99.Value = "(if pit has not been installed please contact the Distribution Company)";
			// 
			// table19
			// 
			this.table19.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.209D)));
			this.table19.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.898D)));
			this.table19.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.274D)));
			this.table19.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.615D)));
			this.table19.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.219D)));
			this.table19.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.172D)));
			this.table19.Body.SetCellContent(0, 0, this.textBox57);
			this.table19.Body.SetCellContent(0, 1, this.table17);
			this.table19.Body.SetCellContent(0, 2, this.textBox84);
			this.table19.Body.SetCellContent(0, 4, this.textBox89);
			this.table19.Body.SetCellContent(0, 3, this.textBox90);
			tableGroup114.Name = "tableGroup24";
			tableGroup115.Name = "tableGroup25";
			tableGroup116.Name = "tableGroup26";
			tableGroup117.Name = "group40";
			tableGroup118.Name = "group41";
			this.table19.ColumnGroups.Add(tableGroup114);
			this.table19.ColumnGroups.Add(tableGroup115);
			this.table19.ColumnGroups.Add(tableGroup116);
			this.table19.ColumnGroups.Add(tableGroup117);
			this.table19.ColumnGroups.Add(tableGroup118);
			this.table19.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox57,
            this.table17,
            this.textBox84,
            this.textBox90,
            this.textBox89});
			this.table19.Name = "table19";
			tableGroup119.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup119.Name = "detailTableGroup10";
			this.table19.RowGroups.Add(tableGroup119);
			this.table19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.271D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.table19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.table19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.table19.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.table19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.table19.StyleName = "";
			// 
			// textBox57
			// 
			this.textBox57.Name = "textBox57";
			this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.209D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox57.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox57.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox57.StyleName = "";
			this.textBox57.Value = "NEW MAINS: NO. PHASES ";
			// 
			// table17
			// 
			this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.272D)));
			this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.303D)));
			this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.303D)));
			this.table17.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.172D)));
			this.table17.Body.SetCellContent(0, 0, this.checkBox19);
			this.table17.Body.SetCellContent(0, 1, this.checkBox20);
			this.table17.Body.SetCellContent(0, 2, this.checkBox29);
			tableGroup110.Name = "tableGroup7";
			tableGroup111.Name = "tableGroup9";
			tableGroup112.Name = "group36";
			this.table17.ColumnGroups.Add(tableGroup110);
			this.table17.ColumnGroups.Add(tableGroup111);
			this.table17.ColumnGroups.Add(tableGroup112);
			this.table17.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox19,
            this.checkBox20,
            this.checkBox29});
			this.table17.Name = "table17";
			tableGroup113.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup113.Name = "detailTableGroup3";
			this.table17.RowGroups.Add(tableGroup113);
			this.table17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.961D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.table17.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table17.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table17.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table17.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table17.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table17.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.table17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table17.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox19
			// 
			this.checkBox19.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox19.CheckedImage = ((object)(resources.GetObject("checkBox19.CheckedImage")));
			this.checkBox19.IndeterminateImage = ((object)(resources.GetObject("checkBox19.IndeterminateImage")));
			this.checkBox19.Name = "checkBox19";
			this.checkBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.272D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.checkBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox19.StyleName = "";
			this.checkBox19.Text = " 1";
			this.checkBox19.UncheckedImage = ((object)(resources.GetObject("checkBox19.UncheckedImage")));
			this.checkBox19.Value = "";
			// 
			// checkBox20
			// 
			this.checkBox20.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox20.CheckedImage = ((object)(resources.GetObject("checkBox20.CheckedImage")));
			this.checkBox20.IndeterminateImage = ((object)(resources.GetObject("checkBox20.IndeterminateImage")));
			this.checkBox20.Name = "checkBox20";
			this.checkBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.303D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.checkBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox20.StyleName = "";
			this.checkBox20.Text = " 2";
			this.checkBox20.UncheckedImage = ((object)(resources.GetObject("checkBox20.UncheckedImage")));
			this.checkBox20.Value = "";
			// 
			// checkBox29
			// 
			this.checkBox29.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox29.CheckedImage = ((object)(resources.GetObject("checkBox29.CheckedImage")));
			this.checkBox29.IndeterminateImage = ((object)(resources.GetObject("checkBox29.IndeterminateImage")));
			this.checkBox29.Name = "checkBox29";
			this.checkBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.303D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.checkBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox29.StyleName = "";
			this.checkBox29.Text = " 3";
			this.checkBox29.UncheckedImage = ((object)(resources.GetObject("checkBox29.UncheckedImage")));
			this.checkBox29.Value = "";
			// 
			// textBox84
			// 
			this.textBox84.Name = "textBox84";
			this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.274D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox84.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox84.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox84.StyleName = "";
			this.textBox84.Value = "SIZE";
			// 
			// textBox89
			// 
			this.textBox89.Name = "textBox89";
			this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.219D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox89.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox89.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox89.StyleName = "";
			this.textBox89.Value = "mm";
			// 
			// textBox90
			// 
			this.textBox90.Name = "textBox90";
			this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.615D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.textBox90.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox90.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox90.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox90.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox90.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox90.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox90.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox90.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox90.StyleName = "";
			// 
			// table22
			// 
			this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.245D)));
			this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.347D)));
			this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.758D)));
			this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.791D)));
			this.table22.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.172D)));
			this.table22.Body.SetCellContent(0, 0, this.textBox88);
			this.table22.Body.SetCellContent(0, 1, this.table23);
			this.table22.Body.SetCellContent(0, 2, this.textBox91);
			this.table22.Body.SetCellContent(0, 3, this.textBox98);
			tableGroup123.Name = "tableGroup24";
			tableGroup124.Name = "tableGroup25";
			tableGroup125.Name = "tableGroup26";
			tableGroup126.Name = "group40";
			this.table22.ColumnGroups.Add(tableGroup123);
			this.table22.ColumnGroups.Add(tableGroup124);
			this.table22.ColumnGroups.Add(tableGroup125);
			this.table22.ColumnGroups.Add(tableGroup126);
			this.table22.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox88,
            this.table23,
            this.textBox91,
            this.textBox98});
			this.table22.Name = "table22";
			tableGroup127.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup127.Name = "detailTableGroup10";
			this.table22.RowGroups.Add(tableGroup127);
			this.table22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.197D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.table22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.table22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.table22.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.table22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.table22.StyleName = "";
			// 
			// textBox88
			// 
			this.textBox88.Name = "textBox88";
			this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.245D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.textBox88.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox88.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox88.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox88.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox88.StyleName = "";
			this.textBox88.Value = "MAX DEMAND Installation";
			// 
			// table23
			// 
			this.table23.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.688D)));
			this.table23.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.576D)));
			this.table23.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.172D)));
			this.table23.Body.SetCellContent(0, 0, this.checkBox21);
			this.table23.Body.SetCellContent(0, 1, this.checkBox30);
			tableGroup120.Name = "tableGroup7";
			tableGroup121.Name = "tableGroup9";
			this.table23.ColumnGroups.Add(tableGroup120);
			this.table23.ColumnGroups.Add(tableGroup121);
			this.table23.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox21,
            this.checkBox30});
			this.table23.Name = "table23";
			tableGroup122.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup122.Name = "detailTableGroup3";
			this.table23.RowGroups.Add(tableGroup122);
			this.table23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.347D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.table23.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table23.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table23.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table23.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table23.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table23.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.table23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table23.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox21
			// 
			this.checkBox21.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox21.CheckedImage = ((object)(resources.GetObject("checkBox21.CheckedImage")));
			this.checkBox21.IndeterminateImage = ((object)(resources.GetObject("checkBox21.IndeterminateImage")));
			this.checkBox21.Name = "checkBox21";
			this.checkBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.688D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.checkBox21.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.checkBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox21.StyleName = "";
			this.checkBox21.Text = "Existing";
			this.checkBox21.UncheckedImage = ((object)(resources.GetObject("checkBox21.UncheckedImage")));
			this.checkBox21.Value = "";
			// 
			// checkBox30
			// 
			this.checkBox30.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox30.CheckedImage = ((object)(resources.GetObject("checkBox30.CheckedImage")));
			this.checkBox30.IndeterminateImage = ((object)(resources.GetObject("checkBox30.IndeterminateImage")));
			this.checkBox30.Name = "checkBox30";
			this.checkBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.576D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.checkBox30.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.checkBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox30.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox30.StyleName = "";
			this.checkBox30.Text = "New";
			this.checkBox30.UncheckedImage = ((object)(resources.GetObject("checkBox30.UncheckedImage")));
			this.checkBox30.Value = "";
			// 
			// textBox91
			// 
			this.textBox91.Name = "textBox91";
			this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.758D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.textBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox91.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox91.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox91.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox91.StyleName = "";
			this.textBox91.Value = "Amps per phase ";
			// 
			// textBox98
			// 
			this.textBox98.Name = "textBox98";
			this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.791D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
			this.textBox98.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox98.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox98.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox98.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox98.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox98.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox98.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox98.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox98.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox98.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox98.StyleName = "";
			// 
			// table24
			// 
			this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.893D)));
			this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.217D)));
			this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.031D)));
			this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.182D)));
			this.table24.Body.SetCellContent(0, 0, this.textBox77);
			this.table24.Body.SetCellContent(0, 2, this.textBox92);
			this.table24.Body.SetCellContent(0, 1, this.textBox80);
			tableGroup128.Name = "tableGroup24";
			tableGroup129.Name = "tableGroup25";
			tableGroup130.Name = "tableGroup26";
			this.table24.ColumnGroups.Add(tableGroup128);
			this.table24.ColumnGroups.Add(tableGroup129);
			this.table24.ColumnGroups.Add(tableGroup130);
			this.table24.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox77,
            this.textBox80,
            this.textBox92});
			this.table24.Name = "table24";
			tableGroup131.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup131.Name = "detailTableGroup10";
			this.table24.RowGroups.Add(tableGroup131);
			this.table24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.197D), Telerik.Reporting.Drawing.Unit.Inch(0.182D));
			this.table24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.table24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.table24.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.table24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.table24.StyleName = "";
			// 
			// textBox77
			// 
			this.textBox77.Name = "textBox77";
			this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.893D), Telerik.Reporting.Drawing.Unit.Inch(0.182D));
			this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox77.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox77.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox77.StyleName = "";
			this.textBox77.Value = "MAX DEMAND of TOTAL SITE";
			// 
			// textBox92
			// 
			this.textBox92.Name = "textBox92";
			this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.031D), Telerik.Reporting.Drawing.Unit.Inch(0.182D));
			this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox92.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox92.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox92.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox92.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox92.StyleName = "";
			this.textBox92.Value = "Amps per phase ";
			// 
			// textBox80
			// 
			this.textBox80.Name = "textBox80";
			this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.217D), Telerik.Reporting.Drawing.Unit.Inch(0.182D));
			this.textBox80.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox80.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox80.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox80.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox80.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox80.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox80.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox80.StyleName = "";
			this.textBox80.Value = "";
			// 
			// table16
			// 
			this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.848D)));
			this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.896D)));
			this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.499D)));
			this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.937D)));
			this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.961D)));
			this.table16.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.203D)));
			this.table16.Body.SetCellContent(0, 0, this.textBox93);
			this.table16.Body.SetCellContent(0, 3, this.textBox100);
			this.table16.Body.SetCellContent(0, 1, this.table25);
			this.table16.Body.SetCellContent(0, 4, this.textBox82);
			this.table16.Body.SetCellContent(0, 2, this.textBox95);
			tableGroup135.Name = "tableGroup24";
			tableGroup136.Name = "tableGroup25";
			tableGroup137.Name = "group43";
			tableGroup138.Name = "group40";
			tableGroup139.Name = "group42";
			this.table16.ColumnGroups.Add(tableGroup135);
			this.table16.ColumnGroups.Add(tableGroup136);
			this.table16.ColumnGroups.Add(tableGroup137);
			this.table16.ColumnGroups.Add(tableGroup138);
			this.table16.ColumnGroups.Add(tableGroup139);
			this.table16.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox93,
            this.table25,
            this.textBox95,
            this.textBox100,
            this.textBox82});
			this.table16.Name = "table16";
			tableGroup140.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup140.Name = "detailTableGroup10";
			this.table16.RowGroups.Add(tableGroup140);
			this.table16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.197D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.table16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.table16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.table16.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.table16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.table16.StyleName = "";
			// 
			// textBox93
			// 
			this.textBox93.Name = "textBox93";
			this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.848D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox93.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox93.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox93.StyleName = "";
			this.textBox93.Value = "Is SCCD Installed";
			// 
			// textBox100
			// 
			this.textBox100.Name = "textBox100";
			this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.937D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.textBox100.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox100.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox100.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox100.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox100.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox100.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox100.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox100.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox100.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox100.StyleName = "";
			// 
			// table25
			// 
			this.table25.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.436D)));
			this.table25.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.376D)));
			this.table25.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.203D)));
			this.table25.Body.SetCellContent(0, 0, this.checkBox31);
			this.table25.Body.SetCellContent(0, 1, this.checkBox32);
			tableGroup132.Name = "tableGroup7";
			tableGroup133.Name = "tableGroup9";
			this.table25.ColumnGroups.Add(tableGroup132);
			this.table25.ColumnGroups.Add(tableGroup133);
			this.table25.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox31,
            this.checkBox32});
			this.table25.Name = "table25";
			tableGroup134.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup134.Name = "detailTableGroup3";
			this.table25.RowGroups.Add(tableGroup134);
			this.table25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.895D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.table25.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table25.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table25.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table25.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table25.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table25.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table25.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table25.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox31
			// 
			this.checkBox31.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox31.CheckedImage = ((object)(resources.GetObject("checkBox31.CheckedImage")));
			this.checkBox31.IndeterminateImage = ((object)(resources.GetObject("checkBox31.IndeterminateImage")));
			this.checkBox31.Name = "checkBox31";
			this.checkBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.436D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.checkBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox31.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox31.StyleName = "";
			this.checkBox31.Text = " Y";
			this.checkBox31.UncheckedImage = ((object)(resources.GetObject("checkBox31.UncheckedImage")));
			this.checkBox31.Value = "";
			// 
			// checkBox32
			// 
			this.checkBox32.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox32.CheckedImage = ((object)(resources.GetObject("checkBox32.CheckedImage")));
			this.checkBox32.IndeterminateImage = ((object)(resources.GetObject("checkBox32.IndeterminateImage")));
			this.checkBox32.Name = "checkBox32";
			this.checkBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.376D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.checkBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox32.StyleName = "";
			this.checkBox32.Text = " N";
			this.checkBox32.UncheckedImage = ((object)(resources.GetObject("checkBox32.UncheckedImage")));
			this.checkBox32.Value = "";
			// 
			// textBox82
			// 
			this.textBox82.Name = "textBox82";
			this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.961D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.textBox82.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox82.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox82.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox82.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox82.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox82.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox82.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox82.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox82.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox82.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox82.StyleName = "";
			this.textBox82.Value = "Amps";
			// 
			// textBox95
			// 
			this.textBox95.Name = "textBox95";
			this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.499D), Telerik.Reporting.Drawing.Unit.Inch(0.203D));
			this.textBox95.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox95.Style.Color = System.Drawing.Color.MidnightBlue;
			this.textBox95.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.textBox95.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox95.StyleName = "";
			// 
			// table26
			// 
			this.table26.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.92D)));
			this.table26.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.791D)));
			this.table26.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.204D)));
			this.table26.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.174D)));
			this.table26.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.174D)));
			this.table26.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.107D)));
			this.table26.Body.SetCellContent(0, 0, this.checkBox33, 1, 2);
			this.table26.Body.SetCellContent(1, 0, this.checkBox34, 1, 2);
			this.table26.Body.SetCellContent(2, 0, this.textBox105);
			this.table26.Body.SetCellContent(2, 1, this.textBox106);
			this.table26.Body.SetCellContent(3, 0, this.textBox110, 1, 2);
			tableGroup162.Name = "group17";
			tableGroup163.Name = "tableGroup23";
			tableGroup161.ChildGroups.Add(tableGroup162);
			tableGroup161.ChildGroups.Add(tableGroup163);
			tableGroup161.Name = "tableGroup21";
			tableGroup161.ReportItem = this.textBox112;
			this.table26.ColumnGroups.Add(tableGroup161);
			this.table26.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox33,
            this.checkBox34,
            this.textBox105,
            this.textBox106,
            this.textBox110,
            this.textBox112});
			this.table26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(2.12D));
			this.table26.Name = "table26";
			tableGroup165.Name = "group18";
			tableGroup166.Name = "group19";
			tableGroup167.Name = "group33";
			tableGroup164.ChildGroups.Add(tableGroup165);
			tableGroup164.ChildGroups.Add(tableGroup166);
			tableGroup164.ChildGroups.Add(tableGroup167);
			tableGroup164.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup164.Name = "detailTableGroup8";
			tableGroup169.Name = "group30";
			tableGroup168.ChildGroups.Add(tableGroup169);
			tableGroup168.Name = "group29";
			this.table26.RowGroups.Add(tableGroup164);
			this.table26.RowGroups.Add(tableGroup168);
			this.table26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.852D));
			this.table26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table26.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table26.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			// 
			// checkBox33
			// 
			this.checkBox33.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox33.CheckedImage = ((object)(resources.GetObject("checkBox33.CheckedImage")));
			this.checkBox33.IndeterminateImage = ((object)(resources.GetObject("checkBox33.IndeterminateImage")));
			this.checkBox33.Name = "checkBox33";
			this.checkBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.checkBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox33.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox33.StyleName = "";
			this.checkBox33.Text = "Single premise";
			this.checkBox33.UncheckedImage = ((object)(resources.GetObject("checkBox33.UncheckedImage")));
			this.checkBox33.Value = "";
			// 
			// checkBox34
			// 
			this.checkBox34.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox34.CheckedImage = ((object)(resources.GetObject("checkBox34.CheckedImage")));
			this.checkBox34.IndeterminateImage = ((object)(resources.GetObject("checkBox34.IndeterminateImage")));
			this.checkBox34.Name = "checkBox34";
			this.checkBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.174D));
			this.checkBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox34.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox34.StyleName = "";
			this.checkBox34.Text = "Multiple premises";
			this.checkBox34.UncheckedImage = ((object)(resources.GetObject("checkBox34.UncheckedImage")));
			this.checkBox34.Value = "";
			// 
			// textBox105
			// 
			this.textBox105.Name = "textBox105";
			this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.92D), Telerik.Reporting.Drawing.Unit.Inch(0.174D));
			this.textBox105.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox105.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox105.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox105.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox105.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox105.StyleName = "";
			this.textBox105.Value = "Number of units";
			// 
			// textBox106
			// 
			this.textBox106.Name = "textBox106";
			this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.791D), Telerik.Reporting.Drawing.Unit.Inch(0.174D));
			this.textBox106.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox106.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox106.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox106.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox106.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox106.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox106.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox106.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox106.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox106.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox106.StyleName = "";
			// 
			// textBox110
			// 
			this.textBox110.Name = "textBox110";
			this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.107D));
			this.textBox110.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox110.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox110.StyleName = "";
			// 
			// table27
			// 
			this.table27.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.499D)));
			this.table27.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.479D)));
			this.table27.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.166D)));
			this.table27.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.178D)));
			this.table27.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.159D)));
			this.table27.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17D)));
			this.table27.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.157D)));
			this.table27.Body.SetCellContent(3, 0, this.textBox103, 1, 2);
			this.table27.Body.SetCellContent(1, 0, this.textBox111);
			this.table27.Body.SetCellContent(3, 2, this.textBox117);
			this.table27.Body.SetCellContent(0, 0, this.textBox108, 1, 2);
			this.table27.Body.SetCellContent(0, 2, this.table28);
			this.table27.Body.SetCellContent(1, 1, this.table29, 1, 2);
			this.table27.Body.SetCellContent(2, 0, this.checkBox40, 1, 3);
			tableGroup178.Name = "group17";
			tableGroup179.Name = "tableGroup23";
			tableGroup180.Name = "group45";
			tableGroup177.ChildGroups.Add(tableGroup178);
			tableGroup177.ChildGroups.Add(tableGroup179);
			tableGroup177.ChildGroups.Add(tableGroup180);
			tableGroup177.Name = "tableGroup21";
			tableGroup177.ReportItem = this.textBox107;
			this.table27.ColumnGroups.Add(tableGroup177);
			this.table27.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox108,
            this.table28,
            this.textBox111,
            this.table29,
            this.checkBox40,
            this.textBox103,
            this.textBox117,
            this.textBox107});
			this.table27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.8D), Telerik.Reporting.Drawing.Unit.Inch(2.12D));
			this.table27.Name = "table27";
			tableGroup182.Name = "group18";
			tableGroup183.Name = "group19";
			tableGroup184.Name = "group33";
			tableGroup181.ChildGroups.Add(tableGroup182);
			tableGroup181.ChildGroups.Add(tableGroup183);
			tableGroup181.ChildGroups.Add(tableGroup184);
			tableGroup181.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup181.Name = "detailTableGroup8";
			tableGroup186.Name = "group30";
			tableGroup185.ChildGroups.Add(tableGroup186);
			tableGroup185.Name = "group29";
			this.table27.RowGroups.Add(tableGroup181);
			this.table27.RowGroups.Add(tableGroup185);
			this.table27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.144D), Telerik.Reporting.Drawing.Unit.Inch(0.852D));
			this.table27.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table27.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table27.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			// 
			// textBox103
			// 
			this.textBox103.Name = "textBox103";
			this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.978D), Telerik.Reporting.Drawing.Unit.Inch(0.157D));
			this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox103.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox103.StyleName = "";
			this.textBox103.Value = "Other (specify)";
			// 
			// textBox111
			// 
			this.textBox111.Name = "textBox111";
			this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.499D), Telerik.Reporting.Drawing.Unit.Inch(0.159D));
			this.textBox111.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox111.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox111.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox111.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox111.StyleName = "";
			// 
			// textBox117
			// 
			this.textBox117.Name = "textBox117";
			this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.166D), Telerik.Reporting.Drawing.Unit.Inch(0.157D));
			this.textBox117.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox117.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox117.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox117.StyleName = "";
			// 
			// textBox108
			// 
			this.textBox108.Name = "textBox108";
			this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.978D), Telerik.Reporting.Drawing.Unit.Inch(0.178D));
			this.textBox108.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox108.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox108.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox108.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox108.StyleName = "";
			this.textBox108.Value = "Meter Phases:";
			// 
			// table28
			// 
			this.table28.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.361D)));
			this.table28.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.361D)));
			this.table28.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.361D)));
			this.table28.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.178D)));
			this.table28.Body.SetCellContent(0, 0, this.checkBox35);
			this.table28.Body.SetCellContent(0, 1, this.checkBox36);
			this.table28.Body.SetCellContent(0, 2, this.checkBox37);
			tableGroup170.Name = "tableGroup7";
			tableGroup171.Name = "tableGroup9";
			tableGroup172.Name = "group36";
			this.table28.ColumnGroups.Add(tableGroup170);
			this.table28.ColumnGroups.Add(tableGroup171);
			this.table28.ColumnGroups.Add(tableGroup172);
			this.table28.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox35,
            this.checkBox36,
            this.checkBox37});
			this.table28.Name = "table28";
			tableGroup173.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup173.Name = "detailTableGroup3";
			this.table28.RowGroups.Add(tableGroup173);
			this.table28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.166D), Telerik.Reporting.Drawing.Unit.Inch(0.178D));
			this.table28.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table28.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table28.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table28.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table28.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table28.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table28.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table28.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.table28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table28.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox35
			// 
			this.checkBox35.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox35.CheckedImage = ((object)(resources.GetObject("checkBox35.CheckedImage")));
			this.checkBox35.IndeterminateImage = ((object)(resources.GetObject("checkBox35.IndeterminateImage")));
			this.checkBox35.Name = "checkBox35";
			this.checkBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.361D), Telerik.Reporting.Drawing.Unit.Inch(0.178D));
			this.checkBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox35.StyleName = "";
			this.checkBox35.Text = " 1";
			this.checkBox35.UncheckedImage = ((object)(resources.GetObject("checkBox35.UncheckedImage")));
			this.checkBox35.Value = "";
			// 
			// checkBox36
			// 
			this.checkBox36.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox36.CheckedImage = ((object)(resources.GetObject("checkBox36.CheckedImage")));
			this.checkBox36.IndeterminateImage = ((object)(resources.GetObject("checkBox36.IndeterminateImage")));
			this.checkBox36.Name = "checkBox36";
			this.checkBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.361D), Telerik.Reporting.Drawing.Unit.Inch(0.178D));
			this.checkBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox36.StyleName = "";
			this.checkBox36.Text = " 2";
			this.checkBox36.UncheckedImage = ((object)(resources.GetObject("checkBox36.UncheckedImage")));
			this.checkBox36.Value = "";
			// 
			// checkBox37
			// 
			this.checkBox37.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox37.CheckedImage = ((object)(resources.GetObject("checkBox37.CheckedImage")));
			this.checkBox37.IndeterminateImage = ((object)(resources.GetObject("checkBox37.IndeterminateImage")));
			this.checkBox37.Name = "checkBox37";
			this.checkBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.361D), Telerik.Reporting.Drawing.Unit.Inch(0.178D));
			this.checkBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox37.StyleName = "";
			this.checkBox37.Text = " 3";
			this.checkBox37.UncheckedImage = ((object)(resources.GetObject("checkBox37.UncheckedImage")));
			this.checkBox37.Value = "";
			// 
			// table29
			// 
			this.table29.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.742D)));
			this.table29.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.861D)));
			this.table29.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.159D)));
			this.table29.Body.SetCellContent(0, 0, this.checkBox38);
			this.table29.Body.SetCellContent(0, 1, this.checkBox39);
			tableGroup174.Name = "tableGroup7";
			tableGroup175.Name = "tableGroup9";
			this.table29.ColumnGroups.Add(tableGroup174);
			this.table29.ColumnGroups.Add(tableGroup175);
			this.table29.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox38,
            this.checkBox39});
			this.table29.Name = "table29";
			tableGroup176.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup176.Name = "detailTableGroup3";
			this.table29.RowGroups.Add(tableGroup176);
			this.table29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.645D), Telerik.Reporting.Drawing.Unit.Inch(0.159D));
			this.table29.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table29.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table29.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table29.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table29.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table29.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table29.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table29.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table29.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.table29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox38
			// 
			this.checkBox38.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox38.CheckedImage = ((object)(resources.GetObject("checkBox38.CheckedImage")));
			this.checkBox38.IndeterminateImage = ((object)(resources.GetObject("checkBox38.IndeterminateImage")));
			this.checkBox38.Name = "checkBox38";
			this.checkBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.742D), Telerik.Reporting.Drawing.Unit.Inch(0.159D));
			this.checkBox38.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.checkBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox38.StyleName = "";
			this.checkBox38.Text = "Hot Water";
			this.checkBox38.UncheckedImage = ((object)(resources.GetObject("checkBox38.UncheckedImage")));
			this.checkBox38.Value = "";
			// 
			// checkBox39
			// 
			this.checkBox39.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox39.CheckedImage = ((object)(resources.GetObject("checkBox39.CheckedImage")));
			this.checkBox39.IndeterminateImage = ((object)(resources.GetObject("checkBox39.IndeterminateImage")));
			this.checkBox39.Name = "checkBox39";
			this.checkBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.861D), Telerik.Reporting.Drawing.Unit.Inch(0.159D));
			this.checkBox39.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.checkBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.checkBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox39.StyleName = "";
			this.checkBox39.Text = " Floor Heating";
			this.checkBox39.UncheckedImage = ((object)(resources.GetObject("checkBox39.UncheckedImage")));
			this.checkBox39.Value = "";
			// 
			// checkBox40
			// 
			this.checkBox40.CheckAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.checkBox40.CheckedImage = ((object)(resources.GetObject("checkBox40.CheckedImage")));
			this.checkBox40.IndeterminateImage = ((object)(resources.GetObject("checkBox40.IndeterminateImage")));
			this.checkBox40.Name = "checkBox40";
			this.checkBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.144D), Telerik.Reporting.Drawing.Unit.Inch(0.17D));
			this.checkBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.checkBox40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox40.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox40.StyleName = "";
			this.checkBox40.Text = "CT metering";
			this.checkBox40.UncheckedImage = ((object)(resources.GetObject("checkBox40.UncheckedImage")));
			this.checkBox40.Value = "";
			// 
			// table30
			// 
			this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.92D)));
			this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.791D)));
			this.table30.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.204D)));
			this.table30.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.174D)));
			this.table30.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.107D)));
			this.table30.Body.SetCellContent(2, 0, this.textBox114, 1, 2);
			this.table30.Body.SetCellContent(0, 0, this.table31, 1, 2);
			this.table30.Body.SetCellContent(1, 0, this.table32, 1, 2);
			tableGroup195.Name = "group17";
			tableGroup196.Name = "tableGroup23";
			tableGroup194.ChildGroups.Add(tableGroup195);
			tableGroup194.ChildGroups.Add(tableGroup196);
			tableGroup194.Name = "tableGroup21";
			tableGroup194.ReportItem = this.textBox115;
			this.table30.ColumnGroups.Add(tableGroup194);
			this.table30.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table31,
            this.table32,
            this.textBox114,
            this.textBox115});
			this.table30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.05D));
			this.table30.Name = "table30";
			tableGroup198.Name = "group18";
			tableGroup199.Name = "group19";
			tableGroup197.ChildGroups.Add(tableGroup198);
			tableGroup197.ChildGroups.Add(tableGroup199);
			tableGroup197.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup197.Name = "detailTableGroup8";
			tableGroup201.Name = "group30";
			tableGroup200.ChildGroups.Add(tableGroup201);
			tableGroup200.Name = "group29";
			this.table30.RowGroups.Add(tableGroup197);
			this.table30.RowGroups.Add(tableGroup200);
			this.table30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.678D));
			this.table30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table30.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table30.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			// 
			// textBox114
			// 
			this.textBox114.Name = "textBox114";
			this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.107D));
			this.textBox114.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox114.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox114.StyleName = "";
			// 
			// table31
			// 
			this.table31.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.027D)));
			this.table31.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.684D)));
			this.table31.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.204D)));
			this.table31.Body.SetCellContent(0, 0, this.checkBox43);
			this.table31.Body.SetCellContent(0, 1, this.checkBox44);
			tableGroup187.Name = "tableGroup7";
			tableGroup188.Name = "tableGroup9";
			this.table31.ColumnGroups.Add(tableGroup187);
			this.table31.ColumnGroups.Add(tableGroup188);
			this.table31.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox43,
            this.checkBox44});
			this.table31.Name = "table31";
			tableGroup189.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup189.Name = "detailTableGroup3";
			this.table31.RowGroups.Add(tableGroup189);
			this.table31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.711D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.table31.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table31.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table31.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table31.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table31.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table31.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table31.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table31.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.table31.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.table31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox43
			// 
			this.checkBox43.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox43.CheckedImage = ((object)(resources.GetObject("checkBox43.CheckedImage")));
			this.checkBox43.IndeterminateImage = ((object)(resources.GetObject("checkBox43.IndeterminateImage")));
			this.checkBox43.Name = "checkBox43";
			this.checkBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.027D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.checkBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox43.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.checkBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox43.StyleName = "";
			this.checkBox43.Text = "Group Metering";
			this.checkBox43.UncheckedImage = ((object)(resources.GetObject("checkBox43.UncheckedImage")));
			this.checkBox43.Value = "";
			// 
			// checkBox44
			// 
			this.checkBox44.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox44.CheckedImage = ((object)(resources.GetObject("checkBox44.CheckedImage")));
			this.checkBox44.IndeterminateImage = ((object)(resources.GetObject("checkBox44.IndeterminateImage")));
			this.checkBox44.Name = "checkBox44";
			this.checkBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.684D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.checkBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox44.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox44.StyleName = "";
			this.checkBox44.Text = "Pillar";
			this.checkBox44.UncheckedImage = ((object)(resources.GetObject("checkBox44.UncheckedImage")));
			this.checkBox44.Value = "";
			// 
			// table32
			// 
			this.table32.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.769D)));
			this.table32.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.613D)));
			this.table32.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.316D)));
			this.table32.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.174D)));
			this.table32.Body.SetCellContent(0, 0, this.checkBox41);
			this.table32.Body.SetCellContent(0, 1, this.checkBox45);
			this.table32.Body.SetCellContent(0, 2, this.checkBox46);
			tableGroup190.Name = "tableGroup7";
			tableGroup191.Name = "tableGroup9";
			tableGroup192.Name = "group36";
			this.table32.ColumnGroups.Add(tableGroup190);
			this.table32.ColumnGroups.Add(tableGroup191);
			this.table32.ColumnGroups.Add(tableGroup192);
			this.table32.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox41,
            this.checkBox45,
            this.checkBox46});
			this.table32.Name = "table32";
			tableGroup193.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup193.Name = "detailTableGroup3";
			this.table32.RowGroups.Add(tableGroup193);
			this.table32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.712D), Telerik.Reporting.Drawing.Unit.Inch(0.174D));
			this.table32.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table32.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table32.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table32.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table32.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table32.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table32.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.table32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table32.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.table32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox41
			// 
			this.checkBox41.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox41.CheckedImage = ((object)(resources.GetObject("checkBox41.CheckedImage")));
			this.checkBox41.IndeterminateImage = ((object)(resources.GetObject("checkBox41.IndeterminateImage")));
			this.checkBox41.Name = "checkBox41";
			this.checkBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.769D), Telerik.Reporting.Drawing.Unit.Inch(0.174D));
			this.checkBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.checkBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.checkBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox41.StyleName = "";
			this.checkBox41.Text = " Substation";
			this.checkBox41.UncheckedImage = ((object)(resources.GetObject("checkBox41.UncheckedImage")));
			this.checkBox41.Value = "";
			// 
			// checkBox45
			// 
			this.checkBox45.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox45.CheckedImage = ((object)(resources.GetObject("checkBox45.CheckedImage")));
			this.checkBox45.IndeterminateImage = ((object)(resources.GetObject("checkBox45.IndeterminateImage")));
			this.checkBox45.Name = "checkBox45";
			this.checkBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.613D), Telerik.Reporting.Drawing.Unit.Inch(0.174D));
			this.checkBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox45.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox45.StyleName = "";
			this.checkBox45.Text = " FOLCB";
			this.checkBox45.UncheckedImage = ((object)(resources.GetObject("checkBox45.UncheckedImage")));
			this.checkBox45.Value = "";
			// 
			// checkBox46
			// 
			this.checkBox46.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox46.CheckedImage = ((object)(resources.GetObject("checkBox46.CheckedImage")));
			this.checkBox46.IndeterminateImage = ((object)(resources.GetObject("checkBox46.IndeterminateImage")));
			this.checkBox46.Name = "checkBox46";
			this.checkBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.316D), Telerik.Reporting.Drawing.Unit.Inch(0.174D));
			this.checkBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox46.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.checkBox46.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.checkBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.checkBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox46.StyleName = "";
			this.checkBox46.Text = " Pit";
			this.checkBox46.UncheckedImage = ((object)(resources.GetObject("checkBox46.UncheckedImage")));
			this.checkBox46.Value = "";
			// 
			// table33
			// 
			this.table33.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.118D)));
			this.table33.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.026D)));
			this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.159D)));
			this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.17D)));
			this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.157D)));
			this.table33.Body.SetCellContent(1, 1, this.textBox118);
			this.table33.Body.SetCellContent(0, 0, this.checkBox42);
			this.table33.Body.SetCellContent(0, 1, this.checkBox47);
			this.table33.Body.SetCellContent(1, 0, this.textBox101);
			this.table33.Body.SetCellContent(2, 0, this.textBox18, 1, 2);
			tableGroup203.Name = "group17";
			tableGroup204.Name = "group46";
			tableGroup202.ChildGroups.Add(tableGroup203);
			tableGroup202.ChildGroups.Add(tableGroup204);
			tableGroup202.Name = "tableGroup21";
			tableGroup202.ReportItem = this.textBox116;
			this.table33.ColumnGroups.Add(tableGroup202);
			this.table33.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox42,
            this.checkBox47,
            this.textBox101,
            this.textBox118,
            this.textBox18,
            this.textBox116});
			this.table33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.8D), Telerik.Reporting.Drawing.Unit.Inch(3.05D));
			this.table33.Name = "table33";
			tableGroup206.Name = "group19";
			tableGroup207.Name = "group33";
			tableGroup205.ChildGroups.Add(tableGroup206);
			tableGroup205.ChildGroups.Add(tableGroup207);
			tableGroup205.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup205.Name = "detailTableGroup8";
			tableGroup209.Name = "group30";
			tableGroup208.ChildGroups.Add(tableGroup209);
			tableGroup208.Name = "group29";
			this.table33.RowGroups.Add(tableGroup205);
			this.table33.RowGroups.Add(tableGroup208);
			this.table33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.144D), Telerik.Reporting.Drawing.Unit.Inch(0.674D));
			this.table33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table33.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table33.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			// 
			// textBox118
			// 
			this.textBox118.Name = "textBox118";
			this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.026D), Telerik.Reporting.Drawing.Unit.Inch(0.17D));
			this.textBox118.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox118.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox118.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox118.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox118.StyleName = "";
			// 
			// checkBox42
			// 
			this.checkBox42.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox42.CheckedImage = ((object)(resources.GetObject("checkBox42.CheckedImage")));
			this.checkBox42.IndeterminateImage = ((object)(resources.GetObject("checkBox42.IndeterminateImage")));
			this.checkBox42.Name = "checkBox42";
			this.checkBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.118D), Telerik.Reporting.Drawing.Unit.Inch(0.159D));
			this.checkBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.checkBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox42.StyleName = "";
			this.checkBox42.Text = "Parent";
			this.checkBox42.UncheckedImage = ((object)(resources.GetObject("checkBox42.UncheckedImage")));
			this.checkBox42.Value = "";
			// 
			// checkBox47
			// 
			this.checkBox47.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox47.CheckedImage = ((object)(resources.GetObject("checkBox47.CheckedImage")));
			this.checkBox47.IndeterminateImage = ((object)(resources.GetObject("checkBox47.IndeterminateImage")));
			this.checkBox47.Name = "checkBox47";
			this.checkBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.026D), Telerik.Reporting.Drawing.Unit.Inch(0.159D));
			this.checkBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox47.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox47.StyleName = "";
			this.checkBox47.Text = "Child";
			this.checkBox47.UncheckedImage = ((object)(resources.GetObject("checkBox47.UncheckedImage")));
			this.checkBox47.Value = "";
			// 
			// textBox101
			// 
			this.textBox101.Name = "textBox101";
			this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.118D), Telerik.Reporting.Drawing.Unit.Inch(0.17D));
			this.textBox101.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox101.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox101.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox101.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox101.StyleName = "";
			this.textBox101.Value = "Additional Notes";
			// 
			// textBox18
			// 
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.144D), Telerik.Reporting.Drawing.Unit.Inch(0.157D));
			this.textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox18.StyleName = "";
			// 
			// table34
			// 
			this.table34.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.971D)));
			this.table34.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.116D)));
			this.table34.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.475D)));
			this.table34.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.231D)));
			this.table34.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.37D)));
			this.table34.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
			this.table34.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.197D)));
			this.table34.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.163D)));
			this.table34.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
			this.table34.Body.SetCellContent(3, 0, this.textBox113);
			this.table34.Body.SetCellContent(4, 0, this.textBox120, 1, 2);
			this.table34.Body.SetCellContent(0, 0, this.textBox122, 1, 2);
			this.table34.Body.SetCellContent(4, 2, this.textBox128);
			this.table34.Body.SetCellContent(0, 2, this.table35);
			this.table34.Body.SetCellContent(1, 0, this.textBox123, 1, 3);
			this.table34.Body.SetCellContent(2, 0, this.textBox126, 1, 3);
			this.table34.Body.SetCellContent(3, 1, this.textBox119, 1, 2);
			this.table34.Body.SetCellContent(5, 0, this.checkBox50);
			this.table34.Body.SetCellContent(5, 1, this.checkBox51);
			this.table34.Body.SetCellContent(5, 2, this.checkBox52);
			tableGroup214.Name = "group17";
			tableGroup215.Name = "tableGroup23";
			tableGroup213.ChildGroups.Add(tableGroup214);
			tableGroup213.ChildGroups.Add(tableGroup215);
			tableGroup213.Name = "tableGroup21";
			tableGroup213.ReportItem = this.textBox121;
			tableGroup217.Name = "group48";
			tableGroup216.ChildGroups.Add(tableGroup217);
			tableGroup216.Name = "group47";
			tableGroup216.ReportItem = this.textBox124;
			this.table34.ColumnGroups.Add(tableGroup213);
			this.table34.ColumnGroups.Add(tableGroup216);
			this.table34.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox122,
            this.table35,
            this.textBox123,
            this.textBox126,
            this.textBox113,
            this.textBox119,
            this.textBox120,
            this.textBox128,
            this.checkBox50,
            this.checkBox51,
            this.checkBox52,
            this.textBox121,
            this.textBox124});
			this.table34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.031D), Telerik.Reporting.Drawing.Unit.Inch(2.12D));
			this.table34.Name = "table34";
			tableGroup219.Name = "group18";
			tableGroup220.Name = "group19";
			tableGroup221.Name = "group49";
			tableGroup222.Name = "group33";
			tableGroup218.ChildGroups.Add(tableGroup219);
			tableGroup218.ChildGroups.Add(tableGroup220);
			tableGroup218.ChildGroups.Add(tableGroup221);
			tableGroup218.ChildGroups.Add(tableGroup222);
			tableGroup218.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup218.Name = "detailTableGroup8";
			tableGroup224.Name = "group30";
			tableGroup223.ChildGroups.Add(tableGroup224);
			tableGroup223.Name = "group29";
			tableGroup226.Name = "group51";
			tableGroup225.ChildGroups.Add(tableGroup226);
			tableGroup225.Name = "group50";
			this.table34.RowGroups.Add(tableGroup218);
			this.table34.RowGroups.Add(tableGroup223);
			this.table34.RowGroups.Add(tableGroup225);
			this.table34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.562D), Telerik.Reporting.Drawing.Unit.Inch(1.579D));
			this.table34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table34.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table34.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			// 
			// textBox113
			// 
			this.textBox113.Name = "textBox113";
			this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.971D), Telerik.Reporting.Drawing.Unit.Inch(0.197D));
			this.textBox113.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox113.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox113.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox113.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox113.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox113.StyleName = "";
			this.textBox113.Value = "LEI Name";
			// 
			// textBox120
			// 
			this.textBox120.Name = "textBox120";
			this.textBox120.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.087D), Telerik.Reporting.Drawing.Unit.Inch(0.163D));
			this.textBox120.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox120.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox120.StyleName = "";
			this.textBox120.Value = "Acceptance of charges for the truck appt:";
			// 
			// textBox122
			// 
			this.textBox122.Name = "textBox122";
			this.textBox122.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.087D), Telerik.Reporting.Drawing.Unit.Inch(0.231D));
			this.textBox122.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox122.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox122.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox122.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox122.StyleName = "";
			this.textBox122.Value = "Do you require a truck appointment: ";
			// 
			// textBox128
			// 
			this.textBox128.Name = "textBox128";
			this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.475D), Telerik.Reporting.Drawing.Unit.Inch(0.163D));
			this.textBox128.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox128.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox128.StyleName = "";
			// 
			// table35
			// 
			this.table35.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.648D)));
			this.table35.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.744D)));
			this.table35.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.231D)));
			this.table35.Body.SetCellContent(0, 0, this.checkBox48);
			this.table35.Body.SetCellContent(0, 1, this.checkBox49);
			tableGroup210.Name = "tableGroup7";
			tableGroup211.Name = "tableGroup9";
			this.table35.ColumnGroups.Add(tableGroup210);
			this.table35.ColumnGroups.Add(tableGroup211);
			this.table35.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox48,
            this.checkBox49});
			this.table35.Name = "table35";
			tableGroup212.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup212.Name = "detailTableGroup3";
			this.table35.RowGroups.Add(tableGroup212);
			this.table35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.475D), Telerik.Reporting.Drawing.Unit.Inch(0.231D));
			this.table35.Style.BorderColor.Default = System.Drawing.Color.Black;
			this.table35.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.table35.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table35.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table35.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table35.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table35.Style.Color = System.Drawing.Color.MidnightBlue;
			this.table35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
			this.table35.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			// 
			// checkBox48
			// 
			this.checkBox48.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox48.CheckedImage = ((object)(resources.GetObject("checkBox48.CheckedImage")));
			this.checkBox48.IndeterminateImage = ((object)(resources.GetObject("checkBox48.IndeterminateImage")));
			this.checkBox48.Name = "checkBox48";
			this.checkBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.648D), Telerik.Reporting.Drawing.Unit.Inch(0.231D));
			this.checkBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox48.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.checkBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox48.StyleName = "";
			this.checkBox48.Text = " Y";
			this.checkBox48.UncheckedImage = ((object)(resources.GetObject("checkBox48.UncheckedImage")));
			this.checkBox48.Value = "";
			// 
			// checkBox49
			// 
			this.checkBox49.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox49.CheckedImage = ((object)(resources.GetObject("checkBox49.CheckedImage")));
			this.checkBox49.IndeterminateImage = ((object)(resources.GetObject("checkBox49.IndeterminateImage")));
			this.checkBox49.Name = "checkBox49";
			this.checkBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.744D), Telerik.Reporting.Drawing.Unit.Inch(0.231D));
			this.checkBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox49.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.checkBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox49.StyleName = "";
			this.checkBox49.Text = " N";
			this.checkBox49.UncheckedImage = ((object)(resources.GetObject("checkBox49.UncheckedImage")));
			this.checkBox49.Value = "";
			// 
			// textBox123
			// 
			this.textBox123.Name = "textBox123";
			this.textBox123.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.562D), Telerik.Reporting.Drawing.Unit.Inch(0.37D));
			this.textBox123.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox123.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox123.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox123.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox123.StyleName = "";
			this.textBox123.Value = "Appointments are not necessarily supplied for all requests, unless you are requir" +
    "ed to complete work in conjunction with the Distribution Company. ";
			// 
			// textBox126
			// 
			this.textBox126.Name = "textBox126";
			this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.562D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.textBox126.Style.Font.Italic = true;
			this.textBox126.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox126.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox126.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox126.StyleName = "";
			this.textBox126.Value = "Note: CES needs to be supplied at time of appointment";
			// 
			// textBox119
			// 
			this.textBox119.Name = "textBox119";
			this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.591D), Telerik.Reporting.Drawing.Unit.Inch(0.197D));
			this.textBox119.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox119.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox119.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox119.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox119.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox119.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox119.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox119.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox119.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox119.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox119.StyleName = "";
			// 
			// checkBox50
			// 
			this.checkBox50.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox50.CheckedImage = ((object)(resources.GetObject("checkBox50.CheckedImage")));
			this.checkBox50.IndeterminateImage = ((object)(resources.GetObject("checkBox50.IndeterminateImage")));
			this.checkBox50.Name = "checkBox50";
			this.checkBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.971D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.checkBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.checkBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox50.StyleName = "";
			this.checkBox50.Text = "Retailer";
			this.checkBox50.UncheckedImage = ((object)(resources.GetObject("checkBox50.UncheckedImage")));
			this.checkBox50.Value = "";
			// 
			// checkBox51
			// 
			this.checkBox51.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox51.CheckedImage = ((object)(resources.GetObject("checkBox51.CheckedImage")));
			this.checkBox51.IndeterminateImage = ((object)(resources.GetObject("checkBox51.IndeterminateImage")));
			this.checkBox51.Name = "checkBox51";
			this.checkBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.116D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.checkBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox51.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox51.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.checkBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox51.StyleName = "";
			this.checkBox51.Text = "Field Works Order";
			this.checkBox51.UncheckedImage = ((object)(resources.GetObject("checkBox51.UncheckedImage")));
			this.checkBox51.Value = "";
			// 
			// checkBox52
			// 
			this.checkBox52.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox52.CheckedImage = ((object)(resources.GetObject("checkBox52.CheckedImage")));
			this.checkBox52.IndeterminateImage = ((object)(resources.GetObject("checkBox52.IndeterminateImage")));
			this.checkBox52.Name = "checkBox52";
			this.checkBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.475D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
			this.checkBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox52.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.checkBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox52.StyleName = "";
			this.checkBox52.Text = "REC";
			this.checkBox52.UncheckedImage = ((object)(resources.GetObject("checkBox52.UncheckedImage")));
			this.checkBox52.Value = "";
			// 
			// table36
			// 
			this.table36.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.177D)));
			this.table36.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.771D)));
			this.table36.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1D)));
			this.table36.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1D)));
			this.table36.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.646D)));
			this.table36.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.209D)));
			this.table36.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19D)));
			this.table36.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.104D)));
			this.table36.Body.SetCellContent(0, 3, this.textBox129);
			this.table36.Body.SetCellContent(0, 0, this.textBox131);
			this.table36.Body.SetCellContent(0, 1, this.checkBox53);
			this.table36.Body.SetCellContent(0, 2, this.checkBox54);
			this.table36.Body.SetCellContent(0, 4, this.table37);
			this.table36.Body.SetCellContent(2, 0, this.textBox130);
			this.table36.Body.SetCellContent(2, 1, this.textBox132);
			this.table36.Body.SetCellContent(2, 2, this.textBox133);
			this.table36.Body.SetCellContent(2, 3, this.textBox134);
			this.table36.Body.SetCellContent(2, 4, this.textBox135);
			this.table36.Body.SetCellContent(1, 0, this.table38, 1, 5);
			tableGroup231.Name = "tableGroup27";
			tableGroup232.Name = "tableGroup28";
			tableGroup233.Name = "group52";
			tableGroup234.Name = "group53";
			tableGroup235.Name = "tableGroup29";
			this.table36.ColumnGroups.Add(tableGroup231);
			this.table36.ColumnGroups.Add(tableGroup232);
			this.table36.ColumnGroups.Add(tableGroup233);
			this.table36.ColumnGroups.Add(tableGroup234);
			this.table36.ColumnGroups.Add(tableGroup235);
			this.table36.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox131,
            this.checkBox53,
            this.checkBox54,
            this.textBox129,
            this.table37,
            this.table38,
            this.textBox130,
            this.textBox132,
            this.textBox133,
            this.textBox134,
            this.textBox135});
			this.table36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(3.8D));
			this.table36.Name = "table36";
			tableGroup237.Name = "group54";
			tableGroup238.Name = "group55";
			tableGroup239.Name = "group56";
			tableGroup236.ChildGroups.Add(tableGroup237);
			tableGroup236.ChildGroups.Add(tableGroup238);
			tableGroup236.ChildGroups.Add(tableGroup239);
			tableGroup236.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup236.Name = "detailTableGroup11";
			this.table36.RowGroups.Add(tableGroup236);
			this.table36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.594D), Telerik.Reporting.Drawing.Unit.Inch(0.503D));
			this.table36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table36.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table36.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			// 
			// textBox129
			// 
			this.textBox129.Name = "textBox129";
			this.textBox129.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.209D));
			this.textBox129.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox129.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox129.StyleName = "";
			this.textBox129.Value = "Access Notes:";
			// 
			// textBox131
			// 
			this.textBox131.Name = "textBox131";
			this.textBox131.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.177D), Telerik.Reporting.Drawing.Unit.Inch(0.209D));
			this.textBox131.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox131.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox131.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(63)))), ((int)(((byte)(135)))));
			this.textBox131.Style.Font.Bold = true;
			this.textBox131.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox131.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox131.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox131.Value = "Access:";
			// 
			// checkBox53
			// 
			this.checkBox53.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox53.CheckedImage = ((object)(resources.GetObject("checkBox53.CheckedImage")));
			this.checkBox53.IndeterminateImage = ((object)(resources.GetObject("checkBox53.IndeterminateImage")));
			this.checkBox53.Name = "checkBox53";
			this.checkBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.771D), Telerik.Reporting.Drawing.Unit.Inch(0.209D));
			this.checkBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.checkBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox53.StyleName = "";
			this.checkBox53.Text = "To meter position & switchboard";
			this.checkBox53.UncheckedImage = ((object)(resources.GetObject("checkBox53.UncheckedImage")));
			this.checkBox53.Value = "";
			// 
			// checkBox54
			// 
			this.checkBox54.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.checkBox54.CheckedImage = ((object)(resources.GetObject("checkBox54.CheckedImage")));
			this.checkBox54.IndeterminateImage = ((object)(resources.GetObject("checkBox54.IndeterminateImage")));
			this.checkBox54.Name = "checkBox54";
			this.checkBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.209D));
			this.checkBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.checkBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.checkBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.checkBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.checkBox54.StyleName = "";
			this.checkBox54.Text = "VPI Lock";
			this.checkBox54.UncheckedImage = ((object)(resources.GetObject("checkBox54.UncheckedImage")));
			this.checkBox54.Value = "";
			// 
			// table37
			// 
			this.table37.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.507D)));
			this.table37.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.209D)));
			this.table37.Body.SetCellContent(0, 0, this.textBox125);
			tableGroup227.Name = "tableGroup22";
			this.table37.ColumnGroups.Add(tableGroup227);
			this.table37.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox125});
			this.table37.Name = "table37";
			tableGroup228.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup228.Name = "detailTableGroup9";
			this.table37.RowGroups.Add(tableGroup228);
			this.table37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.646D), Telerik.Reporting.Drawing.Unit.Inch(0.209D));
			this.table37.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.table37.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table37.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
			this.table37.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table37.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table37.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table37.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table37.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.table37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.table37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table37.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.table37.StyleName = "";
			// 
			// textBox125
			// 
			this.textBox125.Name = "textBox125";
			this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.507D), Telerik.Reporting.Drawing.Unit.Inch(0.209D));
			this.textBox125.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox125.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			// 
			// textBox130
			// 
			this.textBox130.Name = "textBox130";
			this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.177D), Telerik.Reporting.Drawing.Unit.Inch(0.104D));
			this.textBox130.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox130.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox130.Style.Font.Bold = true;
			this.textBox130.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox130.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox130.StyleName = "";
			// 
			// textBox132
			// 
			this.textBox132.Name = "textBox132";
			this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.771D), Telerik.Reporting.Drawing.Unit.Inch(0.104D));
			this.textBox132.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox132.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox132.Style.Font.Bold = true;
			this.textBox132.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox132.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox132.StyleName = "";
			// 
			// textBox133
			// 
			this.textBox133.Name = "textBox133";
			this.textBox133.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.104D));
			this.textBox133.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox133.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox133.Style.Font.Bold = true;
			this.textBox133.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox133.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox133.StyleName = "";
			// 
			// textBox134
			// 
			this.textBox134.Name = "textBox134";
			this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1D), Telerik.Reporting.Drawing.Unit.Inch(0.104D));
			this.textBox134.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox134.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox134.Style.Font.Bold = true;
			this.textBox134.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox134.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox134.StyleName = "";
			// 
			// textBox135
			// 
			this.textBox135.Name = "textBox135";
			this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.646D), Telerik.Reporting.Drawing.Unit.Inch(0.104D));
			this.textBox135.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox135.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox135.Style.Font.Bold = true;
			this.textBox135.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox135.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox135.StyleName = "";
			// 
			// table38
			// 
			this.table38.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(7.455D)));
			this.table38.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.19D)));
			this.table38.Body.SetCellContent(0, 0, this.textBox127);
			tableGroup229.Name = "tableGroup22";
			this.table38.ColumnGroups.Add(tableGroup229);
			this.table38.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox127});
			this.table38.Name = "table38";
			tableGroup230.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup230.Name = "detailTableGroup9";
			this.table38.RowGroups.Add(tableGroup230);
			this.table38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.594D), Telerik.Reporting.Drawing.Unit.Inch(0.19D));
			this.table38.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.table38.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
			this.table38.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
			this.table38.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
			this.table38.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table38.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table38.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table38.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.table38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.table38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table38.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table38.StyleName = "";
			// 
			// textBox127
			// 
			this.textBox127.Name = "textBox127";
			this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.455D), Telerik.Reporting.Drawing.Unit.Inch(0.19D));
			this.textBox127.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox127.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			// 
			// table39
			// 
			this.table39.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.301D)));
			this.table39.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.187D)));
			this.table39.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.332D)));
			this.table39.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.01D)));
			this.table39.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.77D)));
			this.table39.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.204D)));
			this.table39.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.204D)));
			this.table39.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.204D)));
			this.table39.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.204D)));
			this.table39.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.204D)));
			this.table39.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.273D)));
			this.table39.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.158D)));
			this.table39.Body.SetCellContent(0, 4, this.textBox141);
			this.table39.Body.SetCellContent(0, 2, this.textBox143);
			this.table39.Body.SetCellContent(0, 3, this.textBox145);
			this.table39.Body.SetCellContent(0, 0, this.textBox137, 1, 2);
			this.table39.Body.SetCellContent(1, 0, this.textBox138);
			this.table39.Body.SetCellContent(1, 3, this.textBox144);
			this.table39.Body.SetCellContent(1, 4, this.textBox146);
			this.table39.Body.SetCellContent(1, 1, this.textBox139, 1, 2);
			this.table39.Body.SetCellContent(2, 3, this.textBox149);
			this.table39.Body.SetCellContent(2, 4, this.textBox150);
			this.table39.Body.SetCellContent(2, 0, this.textBox140, 1, 3);
			this.table39.Body.SetCellContent(3, 2, this.textBox151);
			this.table39.Body.SetCellContent(3, 3, this.textBox152);
			this.table39.Body.SetCellContent(3, 4, this.textBox153);
			this.table39.Body.SetCellContent(3, 0, this.textBox147, 1, 2);
			this.table39.Body.SetCellContent(4, 0, this.textBox148);
			this.table39.Body.SetCellContent(4, 1, this.textBox154);
			this.table39.Body.SetCellContent(4, 2, this.textBox155);
			this.table39.Body.SetCellContent(4, 3, this.textBox156);
			this.table39.Body.SetCellContent(4, 4, this.textBox157);
			this.table39.Body.SetCellContent(5, 0, this.textBox158, 1, 5);
			this.table39.Body.SetCellContent(6, 0, this.textBox159, 1, 5);
			tableGroup241.Name = "group59";
			tableGroup242.Name = "tableGroup31";
			tableGroup240.ChildGroups.Add(tableGroup241);
			tableGroup240.ChildGroups.Add(tableGroup242);
			tableGroup240.Name = "tableGroup30";
			tableGroup240.ReportItem = this.textBox136;
			tableGroup243.Name = "group57";
			tableGroup243.ReportItem = this.textBox142;
			tableGroup245.Name = "group60";
			tableGroup246.Name = "tableGroup32";
			tableGroup244.ChildGroups.Add(tableGroup245);
			tableGroup244.ChildGroups.Add(tableGroup246);
			tableGroup244.Name = "group58";
			tableGroup244.ReportItem = this.checkBox55;
			this.table39.ColumnGroups.Add(tableGroup240);
			this.table39.ColumnGroups.Add(tableGroup243);
			this.table39.ColumnGroups.Add(tableGroup244);
			this.table39.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox137,
            this.textBox143,
            this.textBox145,
            this.textBox141,
            this.textBox138,
            this.textBox139,
            this.textBox144,
            this.textBox146,
            this.textBox140,
            this.textBox149,
            this.textBox150,
            this.textBox147,
            this.textBox151,
            this.textBox152,
            this.textBox153,
            this.textBox148,
            this.textBox154,
            this.textBox155,
            this.textBox156,
            this.textBox157,
            this.textBox158,
            this.textBox159,
            this.textBox136,
            this.textBox142,
            this.checkBox55});
			this.table39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.4D), Telerik.Reporting.Drawing.Unit.Inch(9.8D));
			this.table39.Name = "table39";
			tableGroup247.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup247.Name = "detailTableGroup12";
			tableGroup248.Name = "group61";
			tableGroup249.Name = "group62";
			tableGroup250.Name = "group63";
			tableGroup251.Name = "group64";
			tableGroup252.Name = "group65";
			tableGroup253.Name = "group66";
			this.table39.RowGroups.Add(tableGroup247);
			this.table39.RowGroups.Add(tableGroup248);
			this.table39.RowGroups.Add(tableGroup249);
			this.table39.RowGroups.Add(tableGroup250);
			this.table39.RowGroups.Add(tableGroup251);
			this.table39.RowGroups.Add(tableGroup252);
			this.table39.RowGroups.Add(tableGroup253);
			this.table39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(1.655D));
			this.table39.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.table39.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.table39.Style.Color = System.Drawing.Color.Black;
			this.table39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
			this.table39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			// 
			// textBox141
			// 
			this.textBox141.Name = "textBox141";
			this.textBox141.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.77D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox141.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox141.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox141.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox141.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox141.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox141.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox141.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox141.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox141.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox141.Style.Font.Name = "Arial";
			this.textBox141.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox141.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			// 
			// textBox143
			// 
			this.textBox143.Name = "textBox143";
			this.textBox143.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.332D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox143.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox143.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox143.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox143.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox143.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox143.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox143.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox143.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox143.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox143.Style.Font.Name = "Arial";
			this.textBox143.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox143.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox143.StyleName = "";
			// 
			// textBox145
			// 
			this.textBox145.Name = "textBox145";
			this.textBox145.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.01D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox145.Style.Font.Name = "Arial";
			this.textBox145.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox145.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox145.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox145.StyleName = "";
			this.textBox145.Value = "REC No:";
			// 
			// textBox137
			// 
			this.textBox137.Name = "textBox137";
			this.textBox137.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.488D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox137.Style.Font.Name = "Arial";
			this.textBox137.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox137.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox137.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox137.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox137.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox137.Value = "REGISTERED ELECTRICAL CONTRACTOR";
			// 
			// textBox138
			// 
			this.textBox138.Name = "textBox138";
			this.textBox138.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.301D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox138.Style.Font.Name = "Arial";
			this.textBox138.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox138.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox138.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox138.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox138.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox138.StyleName = "";
			this.textBox138.Value = "REGISTERED ADDRESS";
			// 
			// textBox144
			// 
			this.textBox144.Name = "textBox144";
			this.textBox144.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.01D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox144.Style.Font.Name = "Arial";
			this.textBox144.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox144.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox144.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox144.StyleName = "";
			this.textBox144.Value = "PHONE No:";
			// 
			// textBox146
			// 
			this.textBox146.Name = "textBox146";
			this.textBox146.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.77D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox146.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox146.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox146.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox146.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox146.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox146.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox146.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox146.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox146.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox146.Style.Font.Name = "Arial";
			this.textBox146.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox146.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox146.StyleName = "";
			// 
			// textBox139
			// 
			this.textBox139.Name = "textBox139";
			this.textBox139.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.519D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox139.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox139.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox139.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox139.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox139.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox139.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox139.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox139.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox139.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox139.Style.Font.Name = "Arial";
			this.textBox139.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox139.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox139.StyleName = "";
			// 
			// textBox149
			// 
			this.textBox149.Name = "textBox149";
			this.textBox149.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.01D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox149.Style.Font.Name = "Arial";
			this.textBox149.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox149.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox149.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox149.StyleName = "";
			this.textBox149.Value = "MOBILE No:";
			// 
			// textBox150
			// 
			this.textBox150.Name = "textBox150";
			this.textBox150.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.77D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox150.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox150.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox150.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox150.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox150.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox150.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox150.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox150.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox150.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox150.Style.Font.Name = "Arial";
			this.textBox150.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox150.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox150.StyleName = "";
			// 
			// textBox140
			// 
			this.textBox140.Name = "textBox140";
			this.textBox140.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.82D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox140.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox140.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Inset;
			this.textBox140.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox140.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox140.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox140.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox140.Style.Font.Name = "Arial";
			this.textBox140.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox140.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox140.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox140.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox140.StyleName = "";
			// 
			// textBox151
			// 
			this.textBox151.Name = "textBox151";
			this.textBox151.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.332D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox151.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox151.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox151.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox151.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox151.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox151.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox151.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox151.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox151.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox151.Style.Font.Name = "Arial";
			this.textBox151.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox151.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox151.StyleName = "";
			// 
			// textBox152
			// 
			this.textBox152.Name = "textBox152";
			this.textBox152.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.01D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox152.Style.Font.Name = "Arial";
			this.textBox152.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox152.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox152.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox152.StyleName = "";
			this.textBox152.Value = "EMAIL:";
			// 
			// textBox153
			// 
			this.textBox153.Name = "textBox153";
			this.textBox153.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.77D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox153.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox153.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox153.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox153.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox153.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox153.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox153.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox153.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox153.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox153.Style.Font.Name = "Arial";
			this.textBox153.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox153.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox153.StyleName = "";
			// 
			// textBox147
			// 
			this.textBox147.Name = "textBox147";
			this.textBox147.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.488D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox147.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox147.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox147.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox147.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox147.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox147.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox147.Style.Font.Name = "Arial";
			this.textBox147.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox147.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox147.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox147.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox147.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox147.StyleName = "";
			this.textBox147.Value = "RESPONSIBLE PERSON: (Please Print)";
			// 
			// textBox148
			// 
			this.textBox148.Name = "textBox148";
			this.textBox148.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.301D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox148.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox148.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox148.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox148.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox148.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox148.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox148.Style.Font.Name = "Arial";
			this.textBox148.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox148.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox148.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox148.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox148.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox148.StyleName = "";
			this.textBox148.Value = "SIGNATURE:";
			// 
			// textBox154
			// 
			this.textBox154.Name = "textBox154";
			this.textBox154.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.187D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox154.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox154.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox154.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox154.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox154.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox154.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox154.Style.Font.Name = "Arial";
			this.textBox154.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox154.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox154.StyleName = "";
			// 
			// textBox155
			// 
			this.textBox155.Name = "textBox155";
			this.textBox155.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.332D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox155.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox155.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox155.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox155.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox155.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox155.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox155.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox155.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox155.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox155.Style.Font.Name = "Arial";
			this.textBox155.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox155.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox155.StyleName = "";
			// 
			// textBox156
			// 
			this.textBox156.Name = "textBox156";
			this.textBox156.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.01D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox156.Style.Font.Name = "Arial";
			this.textBox156.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox156.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox156.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox156.StyleName = "";
			this.textBox156.Value = " DATE:";
			// 
			// textBox157
			// 
			this.textBox157.Name = "textBox157";
			this.textBox157.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.77D), Telerik.Reporting.Drawing.Unit.Inch(0.204D));
			this.textBox157.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox157.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
			this.textBox157.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox157.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Dotted;
			this.textBox157.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
			this.textBox157.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox157.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox157.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox157.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox157.Style.Font.Name = "Arial";
			this.textBox157.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox157.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox157.StyleName = "";
			// 
			// textBox158
			// 
			this.textBox158.Name = "textBox158";
			this.textBox158.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(0.273D));
			this.textBox158.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox158.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox158.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox158.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox158.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox158.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox158.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox158.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox158.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox158.StyleName = "";
			this.textBox158.Value = resources.GetString("textBox158.Value");
			// 
			// textBox159
			// 
			this.textBox159.Name = "textBox159";
			this.textBox159.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.6D), Telerik.Reporting.Drawing.Unit.Inch(0.158D));
			this.textBox159.Style.BorderColor.Default = System.Drawing.Color.MidnightBlue;
			this.textBox159.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox159.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox159.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
			this.textBox159.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox159.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
			this.textBox159.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox159.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox159.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(1.5D);
			this.textBox159.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox159.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox159.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox159.StyleName = "";
			this.textBox159.Value = "AusNet Services � 1300 360 795 Citipower Pty � 1300 132 894 Jemena � 1300 131 871" +
    " Powercor Australia Ltd � 1300 360 410 United Energy � 1300 131 689";
			// 
			// EWR
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
			this.Name = "EWR";
			this.PageSettings.ContinuousPaper = false;
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
			this.Style.BorderColor.Default = System.Drawing.Color.DimGray;
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Mm(208.28D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private Telerik.Reporting.DetailSection detail;
		private Telerik.Reporting.PictureBox pictureBox2;
		private Telerik.Reporting.Table table1;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox6;
		private Telerik.Reporting.TextBox textBox3;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox7;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.TextBox textBox5;
		private Telerik.Reporting.TextBox textBox8;
		private Telerik.Reporting.TextBox textBox9;
		private Telerik.Reporting.Table table2;
		private Telerik.Reporting.TextBox textBox11;
		private Telerik.Reporting.TextBox textBox10;
		private Telerik.Reporting.TextBox textBox12;
		private Telerik.Reporting.TextBox textBox14;
		private Telerik.Reporting.TextBox textBox16;
		private Telerik.Reporting.Table table3;
		private Telerik.Reporting.TextBox textBox15;
		private Telerik.Reporting.TextBox textBox19;
		private Telerik.Reporting.TextBox textBox21;
		private Telerik.Reporting.TextBox textBox23;
		private Telerik.Reporting.TextBox textBox13;
		private Telerik.Reporting.TextBox textBox20;
		private Telerik.Reporting.TextBox textBox22;
		private Telerik.Reporting.TextBox textBox17;
		private Telerik.Reporting.Table table44;
		private Telerik.Reporting.CheckBox checkBox76;
		private Telerik.Reporting.CheckBox checkBox80;
		private Telerik.Reporting.CheckBox checkBox81;
		private Telerik.Reporting.CheckBox checkBox82;
		private Telerik.Reporting.TextBox textBox24;
		private Telerik.Reporting.TextBox textBox25;
		private Telerik.Reporting.Table table4;
		private Telerik.Reporting.TextBox textBox27;
		private Telerik.Reporting.TextBox textBox29;
		private Telerik.Reporting.TextBox textBox31;
		private Telerik.Reporting.TextBox textBox26;
		private Telerik.Reporting.TextBox textBox28;
		private Telerik.Reporting.TextBox textBox30;
		private Telerik.Reporting.TextBox textBox33;
		private Telerik.Reporting.TextBox textBox35;
		private Telerik.Reporting.TextBox textBox32;
		private Telerik.Reporting.TextBox textBox34;
		private Telerik.Reporting.Table table5;
		private Telerik.Reporting.TextBox textBox36;
		private Telerik.Reporting.TextBox textBox37;
		private Telerik.Reporting.TextBox textBox38;
		private Telerik.Reporting.TextBox textBox39;
		private Telerik.Reporting.TextBox textBox40;
		private Telerik.Reporting.TextBox textBox41;
		private Telerik.Reporting.TextBox textBox42;
		private Telerik.Reporting.TextBox textBox43;
		private Telerik.Reporting.TextBox textBox44;
		private Telerik.Reporting.TextBox textBox45;
		private Telerik.Reporting.Table table6;
		private Telerik.Reporting.TextBox textBox47;
		private Telerik.Reporting.TextBox textBox51;
		private Telerik.Reporting.TextBox textBox53;
		private Telerik.Reporting.TextBox textBox46;
		private Telerik.Reporting.Table table18;
		private Telerik.Reporting.CheckBox checkBox27;
		private Telerik.Reporting.CheckBox checkBox28;
		private Telerik.Reporting.Table table7;
		private Telerik.Reporting.TextBox textBox49;
		private Telerik.Reporting.TextBox textBox52;
		private Telerik.Reporting.TextBox textBox55;
		private Telerik.Reporting.Table table8;
		private Telerik.Reporting.TextBox textBox62;
		private Telerik.Reporting.TextBox textBox63;
		private Telerik.Reporting.TextBox textBox64;
		private Telerik.Reporting.TextBox textBox50;
		private Telerik.Reporting.TextBox textBox59;
		private Telerik.Reporting.HtmlTextBox htmlTextBox1;
		private Telerik.Reporting.Panel panel1;
		private Telerik.Reporting.TextBox textBox56;
		private Telerik.Reporting.Table table9;
		private Telerik.Reporting.TextBox textBox70;
		private Telerik.Reporting.TextBox textBox72;
		private Telerik.Reporting.TextBox textBox74;
		private Telerik.Reporting.TextBox textBox48;
		private Telerik.Reporting.CheckBox checkBox1;
		private Telerik.Reporting.CheckBox checkBox2;
		private Telerik.Reporting.CheckBox checkBox3;
		private Telerik.Reporting.CheckBox checkBox4;
		private Telerik.Reporting.CheckBox checkBox5;
		private Telerik.Reporting.Table table13;
		private Telerik.Reporting.CheckBox checkBox22;
		private Telerik.Reporting.CheckBox checkBox23;
		private Telerik.Reporting.Table table10;
		private Telerik.Reporting.CheckBox checkBox6;
		private Telerik.Reporting.CheckBox checkBox7;
		private Telerik.Reporting.Table table11;
		private Telerik.Reporting.CheckBox checkBox8;
		private Telerik.Reporting.CheckBox checkBox9;
		private Telerik.Reporting.Table table12;
		private Telerik.Reporting.TextBox textBox54;
		private Telerik.Reporting.TextBox textBox58;
		private Telerik.Reporting.CheckBox checkBox10;
		private Telerik.Reporting.CheckBox checkBox11;
		private Telerik.Reporting.CheckBox checkBox12;
		private Telerik.Reporting.CheckBox checkBox13;
		private Telerik.Reporting.TextBox textBox60;
		private Telerik.Reporting.TextBox textBox61;
		private Telerik.Reporting.TextBox textBox65;
		private Telerik.Reporting.TextBox textBox73;
		private Telerik.Reporting.TextBox textBox66;
		private Telerik.Reporting.TextBox textBox67;
		private Telerik.Reporting.TextBox textBox68;
		private Telerik.Reporting.TextBox textBox69;
		private Telerik.Reporting.Table table14;
		private Telerik.Reporting.TextBox textBox75;
		private Telerik.Reporting.Panel panel2;
		private Telerik.Reporting.Table table15;
		private Telerik.Reporting.CheckBox checkBox14;
		private Telerik.Reporting.CheckBox checkBox15;
		private Telerik.Reporting.CheckBox checkBox16;
		private Telerik.Reporting.CheckBox checkBox17;
		private Telerik.Reporting.TextBox textBox81;
		private Telerik.Reporting.TextBox textBox86;
		private Telerik.Reporting.TextBox textBox87;
		private Telerik.Reporting.TextBox textBox94;
		private Telerik.Reporting.TextBox textBox96;
		private Telerik.Reporting.TextBox textBox97;
		private Telerik.Reporting.TextBox textBox99;
		private Telerik.Reporting.TextBox textBox104;
		private Telerik.Reporting.TextBox textBox83;
		private Telerik.Reporting.TextBox textBox85;
		private Telerik.Reporting.TextBox textBox76;
		private Telerik.Reporting.TextBox textBox78;
		private Telerik.Reporting.TextBox textBox109;
		private Telerik.Reporting.TextBox textBox71;
		private Telerik.Reporting.CheckBox checkBox18;
		private Telerik.Reporting.Table table20;
		private Telerik.Reporting.TextBox textBox79;
		private Telerik.Reporting.Table table21;
		private Telerik.Reporting.CheckBox checkBox24;
		private Telerik.Reporting.CheckBox checkBox25;
		private Telerik.Reporting.CheckBox checkBox26;
		private Telerik.Reporting.Table table19;
		private Telerik.Reporting.TextBox textBox57;
		private Telerik.Reporting.Table table17;
		private Telerik.Reporting.CheckBox checkBox19;
		private Telerik.Reporting.CheckBox checkBox20;
		private Telerik.Reporting.CheckBox checkBox29;
		private Telerik.Reporting.TextBox textBox84;
		private Telerik.Reporting.TextBox textBox89;
		private Telerik.Reporting.TextBox textBox90;
		private Telerik.Reporting.Table table22;
		private Telerik.Reporting.TextBox textBox88;
		private Telerik.Reporting.Table table23;
		private Telerik.Reporting.CheckBox checkBox21;
		private Telerik.Reporting.CheckBox checkBox30;
		private Telerik.Reporting.TextBox textBox91;
		private Telerik.Reporting.TextBox textBox98;
		private Telerik.Reporting.Table table24;
		private Telerik.Reporting.TextBox textBox77;
		private Telerik.Reporting.TextBox textBox92;
		private Telerik.Reporting.TextBox textBox80;
		private Telerik.Reporting.Table table16;
		private Telerik.Reporting.TextBox textBox93;
		private Telerik.Reporting.TextBox textBox100;
		private Telerik.Reporting.Table table25;
		private Telerik.Reporting.CheckBox checkBox31;
		private Telerik.Reporting.CheckBox checkBox32;
		private Telerik.Reporting.TextBox textBox82;
		private Telerik.Reporting.TextBox textBox95;
		private Telerik.Reporting.Table table26;
		private Telerik.Reporting.CheckBox checkBox33;
		private Telerik.Reporting.CheckBox checkBox34;
		private Telerik.Reporting.TextBox textBox105;
		private Telerik.Reporting.TextBox textBox106;
		private Telerik.Reporting.TextBox textBox110;
		private Telerik.Reporting.TextBox textBox112;
		private Telerik.Reporting.Table table27;
		private Telerik.Reporting.TextBox textBox107;
		private Telerik.Reporting.TextBox textBox103;
		private Telerik.Reporting.TextBox textBox108;
		private Telerik.Reporting.TextBox textBox111;
		private Telerik.Reporting.TextBox textBox117;
		private Telerik.Reporting.Table table28;
		private Telerik.Reporting.CheckBox checkBox35;
		private Telerik.Reporting.CheckBox checkBox36;
		private Telerik.Reporting.CheckBox checkBox37;
		private Telerik.Reporting.Table table29;
		private Telerik.Reporting.CheckBox checkBox38;
		private Telerik.Reporting.CheckBox checkBox39;
		private Telerik.Reporting.CheckBox checkBox40;
		private Telerik.Reporting.Table table30;
		private Telerik.Reporting.TextBox textBox114;
		private Telerik.Reporting.TextBox textBox115;
		private Telerik.Reporting.Table table31;
		private Telerik.Reporting.CheckBox checkBox43;
		private Telerik.Reporting.CheckBox checkBox44;
		private Telerik.Reporting.Table table32;
		private Telerik.Reporting.CheckBox checkBox41;
		private Telerik.Reporting.CheckBox checkBox45;
		private Telerik.Reporting.CheckBox checkBox46;
		private Telerik.Reporting.Table table33;
		private Telerik.Reporting.TextBox textBox18;
		private Telerik.Reporting.TextBox textBox116;
		private Telerik.Reporting.TextBox textBox118;
		private Telerik.Reporting.CheckBox checkBox42;
		private Telerik.Reporting.CheckBox checkBox47;
		private Telerik.Reporting.TextBox textBox101;
		private Telerik.Reporting.Table table34;
		private Telerik.Reporting.TextBox textBox113;
		private Telerik.Reporting.TextBox textBox119;
		private Telerik.Reporting.TextBox textBox120;
		private Telerik.Reporting.TextBox textBox122;
		private Telerik.Reporting.TextBox textBox123;
		private Telerik.Reporting.TextBox textBox128;
		private Telerik.Reporting.TextBox textBox121;
		private Telerik.Reporting.TextBox textBox124;
		private Telerik.Reporting.Table table35;
		private Telerik.Reporting.CheckBox checkBox48;
		private Telerik.Reporting.CheckBox checkBox49;
		private Telerik.Reporting.TextBox textBox126;
		private Telerik.Reporting.CheckBox checkBox50;
		private Telerik.Reporting.CheckBox checkBox51;
		private Telerik.Reporting.CheckBox checkBox52;
		private Telerik.Reporting.Table table36;
		private Telerik.Reporting.TextBox textBox129;
		private Telerik.Reporting.TextBox textBox131;
		private Telerik.Reporting.CheckBox checkBox53;
		private Telerik.Reporting.CheckBox checkBox54;
		private Telerik.Reporting.Table table37;
		private Telerik.Reporting.TextBox textBox125;
		private Telerik.Reporting.TextBox textBox130;
		private Telerik.Reporting.TextBox textBox132;
		private Telerik.Reporting.TextBox textBox133;
		private Telerik.Reporting.TextBox textBox134;
		private Telerik.Reporting.TextBox textBox135;
		private Telerik.Reporting.Table table38;
		private Telerik.Reporting.TextBox textBox127;
		private Telerik.Reporting.Table table39;
		private Telerik.Reporting.TextBox textBox137;
		private Telerik.Reporting.TextBox textBox141;
		private Telerik.Reporting.TextBox textBox143;
		private Telerik.Reporting.TextBox textBox145;
		private Telerik.Reporting.TextBox textBox136;
		private Telerik.Reporting.TextBox textBox142;
		private Telerik.Reporting.CheckBox checkBox55;
		private Telerik.Reporting.TextBox textBox138;
		private Telerik.Reporting.TextBox textBox144;
		private Telerik.Reporting.TextBox textBox146;
		private Telerik.Reporting.TextBox textBox139;
		private Telerik.Reporting.TextBox textBox149;
		private Telerik.Reporting.TextBox textBox150;
		private Telerik.Reporting.TextBox textBox140;
		private Telerik.Reporting.TextBox textBox151;
		private Telerik.Reporting.TextBox textBox152;
		private Telerik.Reporting.TextBox textBox153;
		private Telerik.Reporting.TextBox textBox147;
		private Telerik.Reporting.TextBox textBox148;
		private Telerik.Reporting.TextBox textBox154;
		private Telerik.Reporting.TextBox textBox155;
		private Telerik.Reporting.TextBox textBox156;
		private Telerik.Reporting.TextBox textBox157;
		private Telerik.Reporting.TextBox textBox158;
		private Telerik.Reporting.TextBox textBox159;
	}
}