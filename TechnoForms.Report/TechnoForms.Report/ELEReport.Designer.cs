namespace TechnoForms.Report
{
    partial class ELEReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ELEReport));
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.detail = new Telerik.Reporting.DetailSection();
			this.pictureBox1 = new Telerik.Reporting.PictureBox();
			this.textBox102 = new Telerik.Reporting.TextBox();
			this.textBox99 = new Telerik.Reporting.TextBox();
			this.textBox97 = new Telerik.Reporting.TextBox();
			this.textBox94 = new Telerik.Reporting.TextBox();
			this.textBox92 = new Telerik.Reporting.TextBox();
			this.textBox91 = new Telerik.Reporting.TextBox();
			this.textBox90 = new Telerik.Reporting.TextBox();
			this.textBox87 = new Telerik.Reporting.TextBox();
			this.textBox84 = new Telerik.Reporting.TextBox();
			this.textBox82 = new Telerik.Reporting.TextBox();
			this.textBox81 = new Telerik.Reporting.TextBox();
			this.textBox78 = new Telerik.Reporting.TextBox();
			this.pictureBox17 = new Telerik.Reporting.PictureBox();
			this.pictureBox18 = new Telerik.Reporting.PictureBox();
			this.pictureBox2 = new Telerik.Reporting.PictureBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.textBox102,
            this.textBox99,
            this.textBox97,
            this.textBox94,
            this.textBox92,
            this.textBox91,
            this.textBox90,
            this.textBox87,
            this.textBox84,
            this.textBox82,
            this.textBox81,
            this.textBox78,
            this.pictureBox17,
            this.pictureBox18,
            this.pictureBox2,
            this.textBox1,
            this.textBox2,
            this.textBox3});
			this.detail.Name = "detail";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
			this.pictureBox1.MimeType = "image/jpeg";
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
			this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
			// 
			// textBox102
			// 
			this.textBox102.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.559D), Telerik.Reporting.Drawing.Unit.Inch(3.858D));
			this.textBox102.Name = "textBox102";
			this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.008D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
			this.textBox102.Style.Color = System.Drawing.Color.White;
			this.textBox102.Style.Font.Name = "Calibri";
			this.textBox102.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox102.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox102.Value = "";
			// 
			// textBox99
			// 
			this.textBox99.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.743D), Telerik.Reporting.Drawing.Unit.Inch(3.858D));
			this.textBox99.Name = "textBox99";
			this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.029D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
			this.textBox99.Style.Color = System.Drawing.Color.White;
			this.textBox99.Style.Font.Name = "Calibri";
			this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox99.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox99.Value = "";
			// 
			// textBox97
			// 
			this.textBox97.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.993D), Telerik.Reporting.Drawing.Unit.Inch(4.906D));
			this.textBox97.Name = "textBox97";
			this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.723D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
			this.textBox97.Style.Color = System.Drawing.Color.White;
			this.textBox97.Style.Font.Name = "Calibri";
			this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox97.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox97.Value = "";
			// 
			// textBox94
			// 
			this.textBox94.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.993D), Telerik.Reporting.Drawing.Unit.Inch(5.866D));
			this.textBox94.Name = "textBox94";
			this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.889D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
			this.textBox94.Style.Color = System.Drawing.Color.White;
			this.textBox94.Style.Font.Name = "Calibri";
			this.textBox94.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox94.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox94.Value = "";
			// 
			// textBox92
			// 
			this.textBox92.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.134D), Telerik.Reporting.Drawing.Unit.Inch(5.866D));
			this.textBox92.Name = "textBox92";
			this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.917D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
			this.textBox92.Style.Color = System.Drawing.Color.White;
			this.textBox92.Style.Font.Name = "Calibri";
			this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox92.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox92.Value = "";
			// 
			// textBox91
			// 
			this.textBox91.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(1.218D), Telerik.Reporting.Drawing.Unit.Inch(9.685D));
			this.textBox91.Name = "textBox91";
			this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.452D), Telerik.Reporting.Drawing.Unit.Inch(0.351D));
			this.textBox91.Style.Font.Name = "Microsoft Sans Serif";
			this.textBox91.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox91.Value = "";
			// 
			// textBox90
			// 
			this.textBox90.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.067D), Telerik.Reporting.Drawing.Unit.Inch(7.52D));
			this.textBox90.Name = "textBox90";
			this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.358D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
			this.textBox90.Style.Font.Name = "Microsoft Sans Serif";
			this.textBox90.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox90.Value = "";
			// 
			// textBox87
			// 
			this.textBox87.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.067D), Telerik.Reporting.Drawing.Unit.Inch(7.858D));
			this.textBox87.Name = "textBox87";
			this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.358D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
			this.textBox87.Style.Font.Name = "Microsoft Sans Serif";
			this.textBox87.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox87.Value = "";
			// 
			// textBox84
			// 
			this.textBox84.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(5.512D), Telerik.Reporting.Drawing.Unit.Inch(7.067D));
			this.textBox84.Name = "textBox84";
			this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.317D), Telerik.Reporting.Drawing.Unit.Inch(0.23D));
			this.textBox84.Style.Font.Name = "Microsoft Sans Serif";
			this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox84.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox84.Value = "";
			// 
			// textBox82
			// 
			this.textBox82.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.067D), Telerik.Reporting.Drawing.Unit.Inch(7.087D));
			this.textBox82.Name = "textBox82";
			this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.317D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
			this.textBox82.Style.Font.Name = "Microsoft Sans Serif";
			this.textBox82.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox82.Value = "";
			// 
			// textBox81
			// 
			this.textBox81.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.504D), Telerik.Reporting.Drawing.Unit.Inch(7.087D));
			this.textBox81.Name = "textBox81";
			this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.33D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
			this.textBox81.Style.Font.Name = "Microsoft Sans Serif";
			this.textBox81.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox81.Value = "";
			// 
			// textBox78
			// 
			this.textBox78.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(3.931D), Telerik.Reporting.Drawing.Unit.Inch(7.087D));
			this.textBox78.Name = "textBox78";
			this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.441D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
			this.textBox78.Style.Font.Name = "Microsoft Sans Serif";
			this.textBox78.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox78.Value = "";
			// 
			// pictureBox17
			// 
			this.pictureBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Mm(73D), Telerik.Reporting.Drawing.Unit.Mm(47D));
			this.pictureBox17.MimeType = "image/jpeg";
			this.pictureBox17.Name = "pictureBox17";
			this.pictureBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(6.8D), Telerik.Reporting.Drawing.Unit.Mm(6.8D));
			this.pictureBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.pictureBox17.Value = ((object)(resources.GetObject("pictureBox17.Value")));
			// 
			// pictureBox18
			// 
			this.pictureBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.874D), Telerik.Reporting.Drawing.Unit.Inch(2.756D));
			this.pictureBox18.MimeType = "image/jpeg";
			this.pictureBox18.Name = "pictureBox18";
			this.pictureBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.266D), Telerik.Reporting.Drawing.Unit.Inch(0.268D));
			this.pictureBox18.Value = ((object)(resources.GetObject("pictureBox18.Value")));
			// 
			// pictureBox2
			// 
			this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(26.3D));
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.306D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.981D), Telerik.Reporting.Drawing.Unit.Cm(27.064D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox1.Style.Font.Name = "Microsoft Sans Serif";
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox1.Value = "";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.2D), Telerik.Reporting.Drawing.Unit.Cm(27.064D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox2.Style.Font.Name = "Microsoft Sans Serif";
			this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox2.Value = "";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.2D), Telerik.Reporting.Drawing.Unit.Cm(27.064D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox3.Style.Font.Name = "Microsoft Sans Serif";
			this.textBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox3.Value = "";
			// 
			// ELEReport
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
			this.Name = "ELEReport";
			this.PageSettings.ContinuousPaper = false;
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.PictureBox pictureBox17;
        private Telerik.Reporting.PictureBox pictureBox18;
		private Telerik.Reporting.PictureBox pictureBox2;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox3;
	}
}