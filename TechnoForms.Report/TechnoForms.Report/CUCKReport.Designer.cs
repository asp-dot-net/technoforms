namespace TechnoForms.Report
{
    partial class CUCKReport
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CUCKReport));
			Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.detail = new Telerik.Reporting.DetailSection();
			this.pictureBox16 = new Telerik.Reporting.PictureBox();
			this.textBox172 = new Telerik.Reporting.TextBox();
			this.textBox171 = new Telerik.Reporting.TextBox();
			this.textBox168 = new Telerik.Reporting.TextBox();
			this.textBox167 = new Telerik.Reporting.TextBox();
			this.textBox165 = new Telerik.Reporting.TextBox();
			this.textBox163 = new Telerik.Reporting.TextBox();
			this.textBox160 = new Telerik.Reporting.TextBox();
			this.textBox103 = new Telerik.Reporting.TextBox();
			this.textBox77 = new Telerik.Reporting.TextBox();
			this.textBox76 = new Telerik.Reporting.TextBox();
			this.textBox72 = new Telerik.Reporting.TextBox();
			this.pictureBox15 = new Telerik.Reporting.PictureBox();
			this.pictureBox3 = new Telerik.Reporting.PictureBox();
			this.pictureBox5 = new Telerik.Reporting.PictureBox();
			this.pictureBox6 = new Telerik.Reporting.PictureBox();
			this.pictureBox7 = new Telerik.Reporting.PictureBox();
			this.pictureBox8 = new Telerik.Reporting.PictureBox();
			this.pictureBox11 = new Telerik.Reporting.PictureBox();
			this.pictureBox12 = new Telerik.Reporting.PictureBox();
			this.pictureBox13 = new Telerik.Reporting.PictureBox();
			this.pictureBox14 = new Telerik.Reporting.PictureBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.table1 = new Telerik.Reporting.Table();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.textBox25 = new Telerik.Reporting.TextBox();
			this.textBox26 = new Telerik.Reporting.TextBox();
			this.textBox27 = new Telerik.Reporting.TextBox();
			this.textBox28 = new Telerik.Reporting.TextBox();
			this.textBox29 = new Telerik.Reporting.TextBox();
			this.textBox30 = new Telerik.Reporting.TextBox();
			this.textBox31 = new Telerik.Reporting.TextBox();
			this.textBox32 = new Telerik.Reporting.TextBox();
			this.textBox33 = new Telerik.Reporting.TextBox();
			this.textBox34 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox35 = new Telerik.Reporting.TextBox();
			this.textBox36 = new Telerik.Reporting.TextBox();
			this.textBox37 = new Telerik.Reporting.TextBox();
			this.textBox38 = new Telerik.Reporting.TextBox();
			this.textBox39 = new Telerik.Reporting.TextBox();
			this.textBox40 = new Telerik.Reporting.TextBox();
			this.textBox41 = new Telerik.Reporting.TextBox();
			this.textBox42 = new Telerik.Reporting.TextBox();
			this.textBox43 = new Telerik.Reporting.TextBox();
			this.textBox44 = new Telerik.Reporting.TextBox();
			this.textBox45 = new Telerik.Reporting.TextBox();
			this.textBox46 = new Telerik.Reporting.TextBox();
			this.textBox47 = new Telerik.Reporting.TextBox();
			this.textBox48 = new Telerik.Reporting.TextBox();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.textBox50 = new Telerik.Reporting.TextBox();
			this.textBox51 = new Telerik.Reporting.TextBox();
			this.textBox52 = new Telerik.Reporting.TextBox();
			this.textBox53 = new Telerik.Reporting.TextBox();
			this.textBox54 = new Telerik.Reporting.TextBox();
			this.textBox55 = new Telerik.Reporting.TextBox();
			this.textBox56 = new Telerik.Reporting.TextBox();
			this.textBox57 = new Telerik.Reporting.TextBox();
			this.textBox58 = new Telerik.Reporting.TextBox();
			this.textBox59 = new Telerik.Reporting.TextBox();
			this.pictureBox1 = new Telerik.Reporting.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox16,
            this.textBox172,
            this.textBox171,
            this.textBox168,
            this.textBox167,
            this.textBox165,
            this.textBox163,
            this.textBox160,
            this.textBox103,
            this.textBox77,
            this.textBox76,
            this.textBox72,
            this.pictureBox15,
            this.pictureBox3,
            this.pictureBox5,
            this.pictureBox6,
            this.pictureBox7,
            this.pictureBox8,
            this.pictureBox11,
            this.pictureBox12,
            this.pictureBox13,
            this.pictureBox14,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.table1,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.pictureBox1});
			this.detail.Name = "detail";
			// 
			// pictureBox16
			// 
			this.pictureBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
			this.pictureBox16.MimeType = "image/jpeg";
			this.pictureBox16.Name = "pictureBox16";
			this.pictureBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
			this.pictureBox16.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.pictureBox16.Value = ((object)(resources.GetObject("pictureBox16.Value")));
			// 
			// textBox172
			// 
			this.textBox172.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.659D), Telerik.Reporting.Drawing.Unit.Cm(4.421D));
			this.textBox172.Name = "textBox172";
			this.textBox172.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.497D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox172.Style.Font.Name = "Calibri";
			this.textBox172.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox172.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox172.Value = "I have received a copy of my invoice & warranty Documentation (contained Within t" +
    "he user Mnual).";
			// 
			// textBox171
			// 
			this.textBox171.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.659D), Telerik.Reporting.Drawing.Unit.Cm(4.9D));
			this.textBox171.Name = "textBox171";
			this.textBox171.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.497D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox171.Style.Font.Name = "Calibri";
			this.textBox171.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox171.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox171.Value = "I am Satisfied with the condition that the installer/s have left the property aft" +
    "er completing the installation.";
			// 
			// textBox168
			// 
			this.textBox168.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(5.461D));
			this.textBox168.Name = "textBox168";
			this.textBox168.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.674D), Telerik.Reporting.Drawing.Unit.Cm(0.339D));
			this.textBox168.Style.Color = System.Drawing.Color.DarkBlue;
			this.textBox168.Style.Font.Bold = true;
			this.textBox168.Style.Font.Name = "Calibri";
			this.textBox168.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox168.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox168.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox168.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox168.Value = "I also acknowledge that....";
			// 
			// textBox167
			// 
			this.textBox167.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.659D), Telerik.Reporting.Drawing.Unit.Cm(6.061D));
			this.textBox167.Name = "textBox167";
			this.textBox167.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.497D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
			this.textBox167.Style.Font.Name = "Calibri";
			this.textBox167.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox167.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox167.Value = "I am aware that claims relating to property or roof damage caused by the installa" +
    "tion of my system must be made within five [5] business\r\ndays of when I become a" +
    "ware of the damage";
			// 
			// textBox165
			// 
			this.textBox165.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.659D), Telerik.Reporting.Drawing.Unit.Cm(7.021D));
			this.textBox165.Name = "textBox165";
			this.textBox165.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.497D), Telerik.Reporting.Drawing.Unit.Cm(0.649D));
			this.textBox165.Style.Font.Name = "Calibri";
			this.textBox165.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox165.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox165.Value = "I am aware of the requirement for all new Solar PV Systems to undergo an independ" +
    "ent inspection & sign-off shortly after installation;\r\n& that I consent to this " +
    "inspection";
			// 
			// textBox163
			// 
			this.textBox163.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.659D), Telerik.Reporting.Drawing.Unit.Cm(7.919D));
			this.textBox163.Name = "textBox163";
			this.textBox163.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.497D), Telerik.Reporting.Drawing.Unit.Cm(1.023D));
			this.textBox163.Style.Font.Name = "Calibri";
			this.textBox163.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox163.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox163.Value = resources.GetString("textBox163.Value");
			// 
			// textBox160
			// 
			this.textBox160.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.659D), Telerik.Reporting.Drawing.Unit.Cm(9.1D));
			this.textBox160.Name = "textBox160";
			this.textBox160.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.497D), Telerik.Reporting.Drawing.Unit.Cm(0.608D));
			this.textBox160.Style.Font.Name = "Calibri";
			this.textBox160.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox160.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox160.Value = "I am aware of the potential damage which may be caused by turning the system on b" +
    "efore the meter installation/reconfiguration; &\r\nthat this damages not covered u" +
    "nder warranty.\r\n";
			// 
			// textBox103
			// 
			this.textBox103.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.659D), Telerik.Reporting.Drawing.Unit.Cm(11.744D));
			this.textBox103.Name = "textBox103";
			this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.497D), Telerik.Reporting.Drawing.Unit.Cm(0.356D));
			this.textBox103.Style.Font.Name = "Calibri";
			this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox103.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox103.Value = "I am happy to commence my repayments to Certegy; ";
			// 
			// textBox77
			// 
			this.textBox77.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.656D), Telerik.Reporting.Drawing.Unit.Cm(12.269D));
			this.textBox77.Name = "textBox77";
			this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.5D), Telerik.Reporting.Drawing.Unit.Cm(0.331D));
			this.textBox77.Style.Font.Name = "Calibri";
			this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox77.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox77.Value = "I am the owner of the property or dwelling at which the system is being installed" +
    "; & ";
			// 
			// textBox76
			// 
			this.textBox76.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.656D), Telerik.Reporting.Drawing.Unit.Cm(12.794D));
			this.textBox76.Name = "textBox76";
			this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.5D), Telerik.Reporting.Drawing.Unit.Cm(0.29D));
			this.textBox76.Style.Font.Name = "Calibri";
			this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox76.Value = "The provision of goods/services by the merchant has been completed to my satisfac" +
    "tion";
			// 
			// textBox72
			// 
			this.textBox72.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.656D), Telerik.Reporting.Drawing.Unit.Cm(13.275D));
			this.textBox72.Name = "textBox72";
			this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.5D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
			this.textBox72.Style.Font.Name = "Calibri";
			this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox72.Value = resources.GetString("textBox72.Value");
			// 
			// pictureBox15
			// 
			this.pictureBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(4.411D));
			this.pictureBox15.MimeType = "image/png";
			this.pictureBox15.Name = "pictureBox15";
			this.pictureBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.2D), Telerik.Reporting.Drawing.Unit.Cm(0.2D));
			this.pictureBox15.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox15.Style.BackgroundColor = System.Drawing.Color.Transparent;
			this.pictureBox15.Value = ((object)(resources.GetObject("pictureBox15.Value")));
			// 
			// pictureBox3
			// 
			this.pictureBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(4.912D));
			this.pictureBox3.MimeType = "image/png";
			this.pictureBox3.Name = "pictureBox3";
			this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.2D), Telerik.Reporting.Drawing.Unit.Cm(0.2D));
			this.pictureBox3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox3.Value = ((object)(resources.GetObject("pictureBox3.Value")));
			// 
			// pictureBox5
			// 
			this.pictureBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(6.141D));
			this.pictureBox5.MimeType = "image/png";
			this.pictureBox5.Name = "pictureBox5";
			this.pictureBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.2D), Telerik.Reporting.Drawing.Unit.Cm(0.2D));
			this.pictureBox5.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox5.Value = ((object)(resources.GetObject("pictureBox5.Value")));
			// 
			// pictureBox6
			// 
			this.pictureBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(7.111D));
			this.pictureBox6.MimeType = "image/png";
			this.pictureBox6.Name = "pictureBox6";
			this.pictureBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.2D), Telerik.Reporting.Drawing.Unit.Cm(0.2D));
			this.pictureBox6.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox6.Value = ((object)(resources.GetObject("pictureBox6.Value")));
			// 
			// pictureBox7
			// 
			this.pictureBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(8.011D));
			this.pictureBox7.MimeType = "image/png";
			this.pictureBox7.Name = "pictureBox7";
			this.pictureBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.2D), Telerik.Reporting.Drawing.Unit.Cm(0.2D));
			this.pictureBox7.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox7.Value = ((object)(resources.GetObject("pictureBox7.Value")));
			// 
			// pictureBox8
			// 
			this.pictureBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(9.202D));
			this.pictureBox8.MimeType = "image/png";
			this.pictureBox8.Name = "pictureBox8";
			this.pictureBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.2D), Telerik.Reporting.Drawing.Unit.Cm(0.2D));
			this.pictureBox8.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox8.Value = ((object)(resources.GetObject("pictureBox8.Value")));
			// 
			// pictureBox11
			// 
			this.pictureBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.319D), Telerik.Reporting.Drawing.Unit.Cm(11.755D));
			this.pictureBox11.MimeType = "image/png";
			this.pictureBox11.Name = "pictureBox11";
			this.pictureBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.2D), Telerik.Reporting.Drawing.Unit.Cm(0.2D));
			this.pictureBox11.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox11.Value = ((object)(resources.GetObject("pictureBox11.Value")));
			// 
			// pictureBox12
			// 
			this.pictureBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.319D), Telerik.Reporting.Drawing.Unit.Cm(12.26D));
			this.pictureBox12.MimeType = "image/png";
			this.pictureBox12.Name = "pictureBox12";
			this.pictureBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.2D), Telerik.Reporting.Drawing.Unit.Cm(0.2D));
			this.pictureBox12.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox12.Value = ((object)(resources.GetObject("pictureBox12.Value")));
			// 
			// pictureBox13
			// 
			this.pictureBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.319D), Telerik.Reporting.Drawing.Unit.Cm(12.84D));
			this.pictureBox13.MimeType = "image/png";
			this.pictureBox13.Name = "pictureBox13";
			this.pictureBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.2D), Telerik.Reporting.Drawing.Unit.Cm(0.2D));
			this.pictureBox13.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox13.Value = ((object)(resources.GetObject("pictureBox13.Value")));
			// 
			// pictureBox14
			// 
			this.pictureBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.319D), Telerik.Reporting.Drawing.Unit.Cm(13.365D));
			this.pictureBox14.MimeType = "image/png";
			this.pictureBox14.Name = "pictureBox14";
			this.pictureBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.2D), Telerik.Reporting.Drawing.Unit.Cm(0.2D));
			this.pictureBox14.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox14.Value = ((object)(resources.GetObject("pictureBox14.Value")));
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
			this.textBox1.Style.Color = System.Drawing.Color.White;
			this.textBox1.Style.Font.Bold = true;
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(30D);
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox1.Value = "Customer Acknowledgement";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(1.4D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
			this.textBox2.Style.Color = System.Drawing.Color.White;
			this.textBox2.Style.Font.Bold = true;
			this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox2.Value = "SECTION 1 - DECLARATION";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.69D), Telerik.Reporting.Drawing.Unit.Cm(1.73D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox3.Style.Color = System.Drawing.Color.DarkBlue;
			this.textBox3.Style.Font.Bold = true;
			this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox3.Value = "Customer Number :";
			// 
			// textBox4
			// 
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.189D), Telerik.Reporting.Drawing.Unit.Cm(1.73D));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox4.Style.Color = System.Drawing.Color.DarkBlue;
			this.textBox4.Style.Font.Bold = true;
			this.textBox4.Style.Font.Name = "Bahnschrift";
			this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox4.Value = "Project Number :";
			// 
			// textBox5
			// 
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.319D), Telerik.Reporting.Drawing.Unit.Cm(3D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.27D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox5.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox5.Value = "I,";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.4D), Telerik.Reporting.Drawing.Unit.Cm(3D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox6.Value = "being the owner or otherwise authorized Individual for the property at";
			// 
			// textBox7
			// 
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.656D), Telerik.Reporting.Drawing.Unit.Cm(3.661D));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.744D), Telerik.Reporting.Drawing.Unit.Cm(0.439D));
			this.textBox7.Style.Color = System.Drawing.Color.DarkBlue;
			this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox7.Value = "hereby acknowledge that�";
			// 
			// textBox8
			// 
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.85D), Telerik.Reporting.Drawing.Unit.Cm(9.96D));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.306D), Telerik.Reporting.Drawing.Unit.Cm(0.94D));
			this.textBox8.Style.Color = System.Drawing.Color.White;
			this.textBox8.Style.Font.Bold = true;
			this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox8.Value = "SECTION 2 - Only CERTEGY EZI-PAY CUSTOMERS";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.85D), Telerik.Reporting.Drawing.Unit.Cm(14.25D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.306D), Telerik.Reporting.Drawing.Unit.Cm(0.94D));
			this.textBox9.Style.Color = System.Drawing.Color.White;
			this.textBox9.Style.Font.Bold = true;
			this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox9.Value = "SECTION 3 - QUALITY\r\n";
			// 
			// textBox10
			// 
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.85D), Telerik.Reporting.Drawing.Unit.Cm(10.9D));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.65D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox10.Style.Color = System.Drawing.Color.DarkBlue;
			this.textBox10.Style.Font.Bold = true;
			this.textBox10.Style.Font.Name = "Calibri";
			this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox10.Value = "I declare/confirm that....\r\n";
			// 
			// textBox11
			// 
			this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.85D), Telerik.Reporting.Drawing.Unit.Cm(15.35D));
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5D), Telerik.Reporting.Drawing.Unit.Cm(0.55D));
			this.textBox11.Style.Font.Name = "Calibri";
			this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox11.Value = "Installation Satisfactorily Completed on (date):";
			// 
			// textBox12
			// 
			this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.85D), Telerik.Reporting.Drawing.Unit.Cm(19.769D));
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.306D), Telerik.Reporting.Drawing.Unit.Cm(0.79D));
			this.textBox12.Style.Color = System.Drawing.Color.White;
			this.textBox12.Style.Font.Bold = true;
			this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox12.Value = "SECTION 3 - INSTALLATION SKETCH\r\n";
			// 
			// textBox13
			// 
			this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.85D), Telerik.Reporting.Drawing.Unit.Cm(26.369D));
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.306D), Telerik.Reporting.Drawing.Unit.Cm(0.93D));
			this.textBox13.Style.Color = System.Drawing.Color.White;
			this.textBox13.Style.Font.Bold = true;
			this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox13.Value = "SECTION 4 - SIGNATURE\r\n";
			// 
			// textBox14
			// 
			this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.4D), Telerik.Reporting.Drawing.Unit.Cm(27.789D));
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox14.Style.Font.Name = "Calibri";
			this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox14.Value = "Signature";
			// 
			// textBox15
			// 
			this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.85D), Telerik.Reporting.Drawing.Unit.Cm(28.619D));
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.55D), Telerik.Reporting.Drawing.Unit.Cm(0.481D));
			this.textBox15.Style.Font.Name = "Calibri";
			this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox15.Value = "Date :";
			// 
			// textBox16
			// 
			this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.85D), Telerik.Reporting.Drawing.Unit.Cm(27.789D));
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.55D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox16.Style.Font.Name = "Calibri";
			this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox16.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox16.Value = "Name :";
			// 
			// table1
			// 
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.636D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.531D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.619D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.564D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.582D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.547D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.623D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.48D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.48D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.48D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.48D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.48D)));
			this.table1.Body.SetCellContent(0, 0, this.textBox17);
			this.table1.Body.SetCellContent(0, 1, this.textBox19);
			this.table1.Body.SetCellContent(0, 2, this.textBox21);
			this.table1.Body.SetCellContent(1, 0, this.textBox23);
			this.table1.Body.SetCellContent(1, 1, this.textBox24);
			this.table1.Body.SetCellContent(1, 2, this.textBox25);
			this.table1.Body.SetCellContent(2, 0, this.textBox26);
			this.table1.Body.SetCellContent(2, 1, this.textBox27);
			this.table1.Body.SetCellContent(2, 2, this.textBox28);
			this.table1.Body.SetCellContent(3, 0, this.textBox29);
			this.table1.Body.SetCellContent(3, 1, this.textBox30);
			this.table1.Body.SetCellContent(3, 2, this.textBox31);
			this.table1.Body.SetCellContent(4, 0, this.textBox32);
			this.table1.Body.SetCellContent(4, 1, this.textBox33);
			this.table1.Body.SetCellContent(4, 2, this.textBox34);
			this.table1.Body.SetCellContent(5, 0, this.textBox18);
			this.table1.Body.SetCellContent(5, 1, this.textBox20);
			this.table1.Body.SetCellContent(5, 2, this.textBox22);
			this.table1.Body.SetCellContent(0, 3, this.textBox35);
			this.table1.Body.SetCellContent(1, 3, this.textBox36);
			this.table1.Body.SetCellContent(2, 3, this.textBox37);
			this.table1.Body.SetCellContent(3, 3, this.textBox38);
			this.table1.Body.SetCellContent(4, 3, this.textBox39);
			this.table1.Body.SetCellContent(5, 3, this.textBox40);
			this.table1.Body.SetCellContent(0, 4, this.textBox41);
			this.table1.Body.SetCellContent(1, 4, this.textBox42);
			this.table1.Body.SetCellContent(2, 4, this.textBox43);
			this.table1.Body.SetCellContent(3, 4, this.textBox44);
			this.table1.Body.SetCellContent(4, 4, this.textBox45);
			this.table1.Body.SetCellContent(5, 4, this.textBox46);
			this.table1.Body.SetCellContent(0, 5, this.textBox47);
			this.table1.Body.SetCellContent(1, 5, this.textBox48);
			this.table1.Body.SetCellContent(2, 5, this.textBox49);
			this.table1.Body.SetCellContent(3, 5, this.textBox50);
			this.table1.Body.SetCellContent(4, 5, this.textBox51);
			this.table1.Body.SetCellContent(5, 5, this.textBox52);
			tableGroup1.Name = "tableGroup";
			tableGroup2.Name = "tableGroup1";
			tableGroup3.Name = "tableGroup2";
			tableGroup4.Name = "group6";
			tableGroup5.Name = "group7";
			tableGroup6.Name = "group8";
			this.table1.ColumnGroups.Add(tableGroup1);
			this.table1.ColumnGroups.Add(tableGroup2);
			this.table1.ColumnGroups.Add(tableGroup3);
			this.table1.ColumnGroups.Add(tableGroup4);
			this.table1.ColumnGroups.Add(tableGroup5);
			this.table1.ColumnGroups.Add(tableGroup6);
			this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17,
            this.textBox19,
            this.textBox21,
            this.textBox35,
            this.textBox41,
            this.textBox47,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox36,
            this.textBox42,
            this.textBox48,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox37,
            this.textBox43,
            this.textBox49,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox38,
            this.textBox44,
            this.textBox50,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox39,
            this.textBox45,
            this.textBox51,
            this.textBox18,
            this.textBox20,
            this.textBox22,
            this.textBox40,
            this.textBox46,
            this.textBox52});
			this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(16.55D));
			this.table1.Name = "table1";
			tableGroup8.Name = "group1";
			tableGroup9.Name = "group2";
			tableGroup10.Name = "group3";
			tableGroup11.Name = "group4";
			tableGroup12.Name = "group5";
			tableGroup13.Name = "group";
			tableGroup7.ChildGroups.Add(tableGroup8);
			tableGroup7.ChildGroups.Add(tableGroup9);
			tableGroup7.ChildGroups.Add(tableGroup10);
			tableGroup7.ChildGroups.Add(tableGroup11);
			tableGroup7.ChildGroups.Add(tableGroup12);
			tableGroup7.ChildGroups.Add(tableGroup13);
			tableGroup7.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup7.Name = "detailTableGroup";
			this.table1.RowGroups.Add(tableGroup7);
			this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.479D), Telerik.Reporting.Drawing.Unit.Cm(3.023D));
			// 
			// textBox17
			// 
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.636D), Telerik.Reporting.Drawing.Unit.Cm(0.623D));
			this.textBox17.Style.Color = System.Drawing.Color.DarkBlue;
			this.textBox17.Style.Font.Bold = true;
			this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox17.StyleName = "";
			this.textBox17.Value = "QUALITY OF SERVICE";
			// 
			// textBox19
			// 
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.531D), Telerik.Reporting.Drawing.Unit.Cm(0.623D));
			this.textBox19.Style.Color = System.Drawing.Color.White;
			this.textBox19.Style.Font.Bold = true;
			this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox19.StyleName = "";
			this.textBox19.Value = " 1 - Very Poor ";
			// 
			// textBox21
			// 
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.619D), Telerik.Reporting.Drawing.Unit.Cm(0.623D));
			this.textBox21.Style.Color = System.Drawing.Color.White;
			this.textBox21.Style.Font.Bold = true;
			this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox21.StyleName = "";
			this.textBox21.Value = " 2 - Poor ";
			// 
			// textBox23
			// 
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.636D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox23.Style.Color = System.Drawing.Color.White;
			this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox23.StyleName = "";
			this.textBox23.Value = "Overall Impression:";
			// 
			// textBox24
			// 
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.531D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox24.StyleName = "";
			// 
			// textBox25
			// 
			this.textBox25.Name = "textBox25";
			this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.619D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox25.StyleName = "";
			// 
			// textBox26
			// 
			this.textBox26.Name = "textBox26";
			this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.636D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox26.Style.Color = System.Drawing.Color.White;
			this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox26.StyleName = "";
			this.textBox26.Value = "Personal View: ";
			// 
			// textBox27
			// 
			this.textBox27.Name = "textBox27";
			this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.531D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox27.StyleName = "";
			// 
			// textBox28
			// 
			this.textBox28.Name = "textBox28";
			this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.619D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox28.StyleName = "";
			// 
			// textBox29
			// 
			this.textBox29.Name = "textBox29";
			this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.636D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox29.Style.Color = System.Drawing.Color.White;
			this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox29.StyleName = "";
			this.textBox29.Value = "The Installation";
			// 
			// textBox30
			// 
			this.textBox30.Name = "textBox30";
			this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.531D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox30.StyleName = "";
			// 
			// textBox31
			// 
			this.textBox31.Name = "textBox31";
			this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.619D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox31.StyleName = "";
			// 
			// textBox32
			// 
			this.textBox32.Name = "textBox32";
			this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.636D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox32.Style.Color = System.Drawing.Color.White;
			this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox32.StyleName = "";
			this.textBox32.Value = "Your Experience: ";
			// 
			// textBox33
			// 
			this.textBox33.Name = "textBox33";
			this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.531D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox33.StyleName = "";
			// 
			// textBox34
			// 
			this.textBox34.Name = "textBox34";
			this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.619D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox34.StyleName = "";
			// 
			// textBox18
			// 
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.636D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox18.Style.Color = System.Drawing.Color.White;
			this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox18.StyleName = "";
			this.textBox18.Value = "Services & Technical Support:";
			// 
			// textBox20
			// 
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.531D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox20.StyleName = "";
			// 
			// textBox22
			// 
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.619D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox22.StyleName = "";
			// 
			// textBox35
			// 
			this.textBox35.Name = "textBox35";
			this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.564D), Telerik.Reporting.Drawing.Unit.Cm(0.623D));
			this.textBox35.Style.Color = System.Drawing.Color.White;
			this.textBox35.Style.Font.Bold = true;
			this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox35.StyleName = "";
			this.textBox35.Value = "3 - Average ";
			// 
			// textBox36
			// 
			this.textBox36.Name = "textBox36";
			this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.564D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox36.StyleName = "";
			// 
			// textBox37
			// 
			this.textBox37.Name = "textBox37";
			this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.564D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox37.StyleName = "";
			// 
			// textBox38
			// 
			this.textBox38.Name = "textBox38";
			this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.564D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox38.StyleName = "";
			// 
			// textBox39
			// 
			this.textBox39.Name = "textBox39";
			this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.564D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox39.StyleName = "";
			// 
			// textBox40
			// 
			this.textBox40.Name = "textBox40";
			this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.564D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox40.StyleName = "";
			// 
			// textBox41
			// 
			this.textBox41.Name = "textBox41";
			this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.582D), Telerik.Reporting.Drawing.Unit.Cm(0.623D));
			this.textBox41.Style.Color = System.Drawing.Color.White;
			this.textBox41.Style.Font.Bold = true;
			this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox41.StyleName = "";
			this.textBox41.Value = " 4 - Good ";
			// 
			// textBox42
			// 
			this.textBox42.Name = "textBox42";
			this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.582D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox42.StyleName = "";
			// 
			// textBox43
			// 
			this.textBox43.Name = "textBox43";
			this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.582D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox43.StyleName = "";
			// 
			// textBox44
			// 
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.582D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox44.StyleName = "";
			// 
			// textBox45
			// 
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.582D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox45.StyleName = "";
			// 
			// textBox46
			// 
			this.textBox46.Name = "textBox46";
			this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.582D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox46.StyleName = "";
			// 
			// textBox47
			// 
			this.textBox47.Name = "textBox47";
			this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.547D), Telerik.Reporting.Drawing.Unit.Cm(0.623D));
			this.textBox47.Style.Color = System.Drawing.Color.White;
			this.textBox47.Style.Font.Bold = true;
			this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox47.StyleName = "";
			this.textBox47.Value = " 5 - Very Good";
			// 
			// textBox48
			// 
			this.textBox48.Name = "textBox48";
			this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.547D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox48.StyleName = "";
			// 
			// textBox49
			// 
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.547D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox49.StyleName = "";
			// 
			// textBox50
			// 
			this.textBox50.Name = "textBox50";
			this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.547D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox50.StyleName = "";
			// 
			// textBox51
			// 
			this.textBox51.Name = "textBox51";
			this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.547D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox51.StyleName = "";
			// 
			// textBox52
			// 
			this.textBox52.Name = "textBox52";
			this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.547D), Telerik.Reporting.Drawing.Unit.Cm(0.48D));
			this.textBox52.StyleName = "";
			// 
			// textBox53
			// 
			this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.659D), Telerik.Reporting.Drawing.Unit.Cm(3D));
			this.textBox53.Name = "textBox53";
			this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.741D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox53.Value = "";
			// 
			// textBox54
			// 
			this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.6D), Telerik.Reporting.Drawing.Unit.Cm(3D));
			this.textBox54.Name = "textBox54";
			this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.129D), Telerik.Reporting.Drawing.Unit.Cm(0.95D));
			this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
			this.textBox54.Value = "";
			// 
			// textBox55
			// 
			this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.35D), Telerik.Reporting.Drawing.Unit.Cm(15.35D));
			this.textBox55.Name = "textBox55";
			this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.45D), Telerik.Reporting.Drawing.Unit.Cm(0.55D));
			this.textBox55.Style.Font.Name = "Calibri";
			this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox55.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox55.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox55.Value = "";
			// 
			// textBox56
			// 
			this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(27.789D));
			this.textBox56.Name = "textBox56";
			this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox56.Style.Font.Name = "Calibri";
			this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox56.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox56.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox56.Value = "";
			// 
			// textBox57
			// 
			this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.49D), Telerik.Reporting.Drawing.Unit.Cm(1.73D));
			this.textBox57.Name = "textBox57";
			this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.51D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox57.Style.Color = System.Drawing.Color.DarkBlue;
			this.textBox57.Style.Font.Bold = true;
			this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox57.Value = "";
			// 
			// textBox58
			// 
			this.textBox58.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.689D), Telerik.Reporting.Drawing.Unit.Cm(1.73D));
			this.textBox58.Name = "textBox58";
			this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.467D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox58.Style.Color = System.Drawing.Color.DarkBlue;
			this.textBox58.Style.Font.Bold = true;
			this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox58.Value = "";
			// 
			// textBox59
			// 
			this.textBox59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(28.619D));
			this.textBox59.Name = "textBox59";
			this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.481D));
			this.textBox59.Style.Font.Name = "Calibri";
			this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox59.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox59.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox59.Value = "";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.1D), Telerik.Reporting.Drawing.Unit.Cm(27.789D));
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5D), Telerik.Reporting.Drawing.Unit.Cm(1.311D));
			// 
			// CUCKReport
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
			this.Name = "CUCKReport";
			this.PageSettings.ContinuousPaper = false;
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox16;
        private Telerik.Reporting.TextBox textBox172;
        private Telerik.Reporting.TextBox textBox171;
        private Telerik.Reporting.TextBox textBox168;
        private Telerik.Reporting.TextBox textBox167;
        private Telerik.Reporting.TextBox textBox165;
        private Telerik.Reporting.TextBox textBox163;
        private Telerik.Reporting.TextBox textBox160;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.PictureBox pictureBox15;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.PictureBox pictureBox5;
        private Telerik.Reporting.PictureBox pictureBox6;
        private Telerik.Reporting.PictureBox pictureBox7;
        private Telerik.Reporting.PictureBox pictureBox8;
        private Telerik.Reporting.PictureBox pictureBox11;
        private Telerik.Reporting.PictureBox pictureBox12;
        private Telerik.Reporting.PictureBox pictureBox13;
        private Telerik.Reporting.PictureBox pictureBox14;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
		private Telerik.Reporting.TextBox textBox53;
		private Telerik.Reporting.TextBox textBox54;
		private Telerik.Reporting.TextBox textBox55;
		private Telerik.Reporting.TextBox textBox56;
		private Telerik.Reporting.TextBox textBox57;
		private Telerik.Reporting.TextBox textBox58;
		private Telerik.Reporting.TextBox textBox59;
		private Telerik.Reporting.PictureBox pictureBox1;
	}
}