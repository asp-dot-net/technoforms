namespace TechnoForms.Report
{
    partial class UnitedEnergyInverter
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnitedEnergyInverter));
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup100 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup101 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup102 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup103 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup104 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup105 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup106 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup107 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup108 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup109 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup110 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup60 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup61 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup62 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup63 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup64 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup65 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup66 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup67 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup68 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup69 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup70 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup71 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup72 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup73 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup74 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup75 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup76 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup77 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup78 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup79 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup80 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup81 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup82 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup83 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup84 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup85 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup86 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup87 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup88 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup89 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup90 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup91 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup92 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup93 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup94 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup95 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup96 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup97 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup98 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup99 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup191 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup192 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup193 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup194 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup195 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup196 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup197 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup198 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup199 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup200 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup201 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup202 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup111 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup112 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup113 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup114 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup115 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup116 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup117 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup118 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup119 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup120 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup121 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup122 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup123 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup124 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup125 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup126 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup127 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup128 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup129 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup130 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup131 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup132 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup133 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup134 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup135 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup136 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup137 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup138 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup139 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup140 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup141 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup142 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup143 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup144 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup145 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup146 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup147 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup148 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup149 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup150 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup151 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup152 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup153 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup154 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup155 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup156 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup157 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup158 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup159 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup160 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup161 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup162 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup163 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup164 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup165 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup166 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup167 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup168 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup169 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup170 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup171 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup172 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup173 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup174 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup175 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup176 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup177 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup178 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup179 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup180 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup181 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup182 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup183 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup184 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup185 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup186 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup187 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup188 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup189 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup190 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.NavigateToUrlAction navigateToUrlAction1 = new Telerik.Reporting.NavigateToUrlAction();
            Telerik.Reporting.TableGroup tableGroup203 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup204 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup205 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup206 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup207 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup208 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup209 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup210 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup211 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup212 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup213 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup214 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup215 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup216 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup217 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup218 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup219 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup220 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup221 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup222 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup223 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup224 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup225 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup226 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup227 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup228 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup229 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup230 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup231 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup232 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup233 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup234 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup235 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup236 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup237 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup238 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup239 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup240 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup241 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup242 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup257 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup258 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup259 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup260 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup261 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup262 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup263 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup264 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup265 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup266 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup243 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup244 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup245 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup246 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup247 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup248 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup249 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup250 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup251 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup252 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup253 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup254 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup255 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup256 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup267 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup268 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup269 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup270 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup271 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup272 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup273 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup274 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup275 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup276 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup277 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup278 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup279 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup280 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup281 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup282 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup283 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup284 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup285 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup286 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup287 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup288 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup289 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup290 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup291 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup292 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup293 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup294 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup295 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup296 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup297 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup298 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup299 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup300 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup301 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup302 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup303 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup304 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup305 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup306 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup307 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup308 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup309 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup310 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup311 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup342 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup343 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup344 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup345 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup346 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup347 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup348 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup349 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup350 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup351 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup352 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup353 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup354 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup355 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup356 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup357 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup358 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup359 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup360 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup361 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup362 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup363 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup364 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup365 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup366 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup367 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup368 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup369 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup370 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup312 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup313 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup314 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup315 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup316 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup317 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup318 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup319 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup320 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup321 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup322 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup323 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup324 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup325 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup326 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup327 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup328 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup329 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup330 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup331 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup332 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup333 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup334 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup335 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup336 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup337 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup338 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup339 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup340 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup341 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.htmlTextBox5 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox12 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox17 = new Telerik.Reporting.HtmlTextBox();
            this.textBox333 = new Telerik.Reporting.TextBox();
            this.textBox341 = new Telerik.Reporting.TextBox();
            this.textBox161 = new Telerik.Reporting.TextBox();
            this.textBox156 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.page2 = new Telerik.Reporting.Panel();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.checkBox7 = new Telerik.Reporting.CheckBox();
            this.checkBox8 = new Telerik.Reporting.CheckBox();
            this.checkBox9 = new Telerik.Reporting.CheckBox();
            this.table6 = new Telerik.Reporting.Table();
            this.checkBox10 = new Telerik.Reporting.CheckBox();
            this.checkBox12 = new Telerik.Reporting.CheckBox();
            this.checkBox13 = new Telerik.Reporting.CheckBox();
            this.table7 = new Telerik.Reporting.Table();
            this.checkBox14 = new Telerik.Reporting.CheckBox();
            this.checkBox15 = new Telerik.Reporting.CheckBox();
            this.checkBox16 = new Telerik.Reporting.CheckBox();
            this.table8 = new Telerik.Reporting.Table();
            this.checkBox17 = new Telerik.Reporting.CheckBox();
            this.checkBox18 = new Telerik.Reporting.CheckBox();
            this.checkBox19 = new Telerik.Reporting.CheckBox();
            this.table10 = new Telerik.Reporting.Table();
            this.checkBox22 = new Telerik.Reporting.CheckBox();
            this.checkBox23 = new Telerik.Reporting.CheckBox();
            this.table11 = new Telerik.Reporting.Table();
            this.checkBox24 = new Telerik.Reporting.CheckBox();
            this.checkBox25 = new Telerik.Reporting.CheckBox();
            this.table12 = new Telerik.Reporting.Table();
            this.checkBox26 = new Telerik.Reporting.CheckBox();
            this.checkBox27 = new Telerik.Reporting.CheckBox();
            this.table13 = new Telerik.Reporting.Table();
            this.checkBox28 = new Telerik.Reporting.CheckBox();
            this.checkBox29 = new Telerik.Reporting.CheckBox();
            this.table14 = new Telerik.Reporting.Table();
            this.checkBox30 = new Telerik.Reporting.CheckBox();
            this.checkBox31 = new Telerik.Reporting.CheckBox();
            this.table15 = new Telerik.Reporting.Table();
            this.checkBox32 = new Telerik.Reporting.CheckBox();
            this.checkBox33 = new Telerik.Reporting.CheckBox();
            this.table16 = new Telerik.Reporting.Table();
            this.checkBox34 = new Telerik.Reporting.CheckBox();
            this.checkBox35 = new Telerik.Reporting.CheckBox();
            this.checkBox1 = new Telerik.Reporting.CheckBox();
            this.checkBox36 = new Telerik.Reporting.CheckBox();
            this.checkBox2 = new Telerik.Reporting.CheckBox();
            this.checkBox3 = new Telerik.Reporting.CheckBox();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.table20 = new Telerik.Reporting.Table();
            this.checkBox43 = new Telerik.Reporting.CheckBox();
            this.checkBox44 = new Telerik.Reporting.CheckBox();
            this.checkBox45 = new Telerik.Reporting.CheckBox();
            this.checkBox46 = new Telerik.Reporting.CheckBox();
            this.table21 = new Telerik.Reporting.Table();
            this.checkBox47 = new Telerik.Reporting.CheckBox();
            this.checkBox48 = new Telerik.Reporting.CheckBox();
            this.checkBox49 = new Telerik.Reporting.CheckBox();
            this.checkBox50 = new Telerik.Reporting.CheckBox();
            this.table22 = new Telerik.Reporting.Table();
            this.checkBox51 = new Telerik.Reporting.CheckBox();
            this.checkBox52 = new Telerik.Reporting.CheckBox();
            this.checkBox53 = new Telerik.Reporting.CheckBox();
            this.checkBox54 = new Telerik.Reporting.CheckBox();
            this.table24 = new Telerik.Reporting.Table();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.table25 = new Telerik.Reporting.Table();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.table26 = new Telerik.Reporting.Table();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.table28 = new Telerik.Reporting.Table();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.table41 = new Telerik.Reporting.Table();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.table42 = new Telerik.Reporting.Table();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.table53 = new Telerik.Reporting.Table();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.table54 = new Telerik.Reporting.Table();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.table55 = new Telerik.Reporting.Table();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.table29 = new Telerik.Reporting.Table();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.table38 = new Telerik.Reporting.Table();
            this.checkBox70 = new Telerik.Reporting.CheckBox();
            this.checkBox71 = new Telerik.Reporting.CheckBox();
            this.table39 = new Telerik.Reporting.Table();
            this.checkBox72 = new Telerik.Reporting.CheckBox();
            this.checkBox73 = new Telerik.Reporting.CheckBox();
            this.table40 = new Telerik.Reporting.Table();
            this.checkBox74 = new Telerik.Reporting.CheckBox();
            this.checkBox75 = new Telerik.Reporting.CheckBox();
            this.table44 = new Telerik.Reporting.Table();
            this.checkBox76 = new Telerik.Reporting.CheckBox();
            this.checkBox80 = new Telerik.Reporting.CheckBox();
            this.checkBox81 = new Telerik.Reporting.CheckBox();
            this.checkBox82 = new Telerik.Reporting.CheckBox();
            this.table45 = new Telerik.Reporting.Table();
            this.checkBox77 = new Telerik.Reporting.CheckBox();
            this.checkBox83 = new Telerik.Reporting.CheckBox();
            this.checkBox84 = new Telerik.Reporting.CheckBox();
            this.checkBox85 = new Telerik.Reporting.CheckBox();
            this.table46 = new Telerik.Reporting.Table();
            this.checkBox78 = new Telerik.Reporting.CheckBox();
            this.checkBox86 = new Telerik.Reporting.CheckBox();
            this.checkBox87 = new Telerik.Reporting.CheckBox();
            this.checkBox88 = new Telerik.Reporting.CheckBox();
            this.table47 = new Telerik.Reporting.Table();
            this.checkBox79 = new Telerik.Reporting.CheckBox();
            this.checkBox89 = new Telerik.Reporting.CheckBox();
            this.checkBox90 = new Telerik.Reporting.CheckBox();
            this.checkBox91 = new Telerik.Reporting.CheckBox();
            this.table30 = new Telerik.Reporting.Table();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.table31 = new Telerik.Reporting.Table();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.table32 = new Telerik.Reporting.Table();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.table33 = new Telerik.Reporting.Table();
            this.textBox106 = new Telerik.Reporting.TextBox();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.table34 = new Telerik.Reporting.Table();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.table35 = new Telerik.Reporting.Table();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.table36 = new Telerik.Reporting.Table();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.textBox114 = new Telerik.Reporting.TextBox();
            this.table37 = new Telerik.Reporting.Table();
            this.textBox115 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.table48 = new Telerik.Reporting.Table();
            this.textBox121 = new Telerik.Reporting.TextBox();
            this.textBox122 = new Telerik.Reporting.TextBox();
            this.textBox123 = new Telerik.Reporting.TextBox();
            this.textBox124 = new Telerik.Reporting.TextBox();
            this.table49 = new Telerik.Reporting.Table();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.textBox125 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.table50 = new Telerik.Reporting.Table();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.textBox128 = new Telerik.Reporting.TextBox();
            this.textBox129 = new Telerik.Reporting.TextBox();
            this.textBox130 = new Telerik.Reporting.TextBox();
            this.table51 = new Telerik.Reporting.Table();
            this.textBox119 = new Telerik.Reporting.TextBox();
            this.textBox131 = new Telerik.Reporting.TextBox();
            this.textBox132 = new Telerik.Reporting.TextBox();
            this.textBox133 = new Telerik.Reporting.TextBox();
            this.table52 = new Telerik.Reporting.Table();
            this.checkBox4 = new Telerik.Reporting.CheckBox();
            this.checkBox5 = new Telerik.Reporting.CheckBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.htmlTextBox2 = new Telerik.Reporting.HtmlTextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.panel4 = new Telerik.Reporting.Panel();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.page6 = new Telerik.Reporting.Panel();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBox317 = new Telerik.Reporting.TextBox();
            this.textBox318 = new Telerik.Reporting.TextBox();
            this.table61 = new Telerik.Reporting.Table();
            this.htmlTextBox7 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox9 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox13 = new Telerik.Reporting.HtmlTextBox();
            this.table62 = new Telerik.Reporting.Table();
            this.htmlTextBox6 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox8 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox10 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox11 = new Telerik.Reporting.HtmlTextBox();
            this.table63 = new Telerik.Reporting.Table();
            this.textBox320 = new Telerik.Reporting.TextBox();
            this.textBox322 = new Telerik.Reporting.TextBox();
            this.textBox324 = new Telerik.Reporting.TextBox();
            this.textBox326 = new Telerik.Reporting.TextBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.textBox321 = new Telerik.Reporting.TextBox();
            this.textBox319 = new Telerik.Reporting.TextBox();
            this.table64 = new Telerik.Reporting.Table();
            this.htmlTextBox14 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox15 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox16 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox18 = new Telerik.Reporting.HtmlTextBox();
            this.textBox332 = new Telerik.Reporting.TextBox();
            this.table66 = new Telerik.Reporting.Table();
            this.textBox334 = new Telerik.Reporting.TextBox();
            this.textBox335 = new Telerik.Reporting.TextBox();
            this.textBox336 = new Telerik.Reporting.TextBox();
            this.textBox337 = new Telerik.Reporting.TextBox();
            this.textBox338 = new Telerik.Reporting.TextBox();
            this.textBox339 = new Telerik.Reporting.TextBox();
            this.htmlTextBox19 = new Telerik.Reporting.HtmlTextBox();
            this.table65 = new Telerik.Reporting.Table();
            this.textBox323 = new Telerik.Reporting.TextBox();
            this.textBox325 = new Telerik.Reporting.TextBox();
            this.textBox327 = new Telerik.Reporting.TextBox();
            this.textBox328 = new Telerik.Reporting.TextBox();
            this.textBox329 = new Telerik.Reporting.TextBox();
            this.textBox330 = new Telerik.Reporting.TextBox();
            this.textBox331 = new Telerik.Reporting.TextBox();
            this.table67 = new Telerik.Reporting.Table();
            this.textBox340 = new Telerik.Reporting.TextBox();
            this.textBox342 = new Telerik.Reporting.TextBox();
            this.textBox343 = new Telerik.Reporting.TextBox();
            this.page5 = new Telerik.Reporting.Panel();
            this.textBox314 = new Telerik.Reporting.TextBox();
            this.textBox154 = new Telerik.Reporting.TextBox();
            this.table60 = new Telerik.Reporting.Table();
            this.textBox157 = new Telerik.Reporting.TextBox();
            this.textBox160 = new Telerik.Reporting.TextBox();
            this.textBox158 = new Telerik.Reporting.TextBox();
            this.textBox162 = new Telerik.Reporting.TextBox();
            this.textBox221 = new Telerik.Reporting.TextBox();
            this.textBox222 = new Telerik.Reporting.TextBox();
            this.textBox223 = new Telerik.Reporting.TextBox();
            this.textBox224 = new Telerik.Reporting.TextBox();
            this.textBox225 = new Telerik.Reporting.TextBox();
            this.textBox226 = new Telerik.Reporting.TextBox();
            this.textBox227 = new Telerik.Reporting.TextBox();
            this.textBox228 = new Telerik.Reporting.TextBox();
            this.textBox229 = new Telerik.Reporting.TextBox();
            this.textBox230 = new Telerik.Reporting.TextBox();
            this.textBox231 = new Telerik.Reporting.TextBox();
            this.textBox232 = new Telerik.Reporting.TextBox();
            this.textBox233 = new Telerik.Reporting.TextBox();
            this.textBox234 = new Telerik.Reporting.TextBox();
            this.textBox235 = new Telerik.Reporting.TextBox();
            this.textBox293 = new Telerik.Reporting.TextBox();
            this.textBox294 = new Telerik.Reporting.TextBox();
            this.textBox295 = new Telerik.Reporting.TextBox();
            this.textBox296 = new Telerik.Reporting.TextBox();
            this.textBox297 = new Telerik.Reporting.TextBox();
            this.textBox298 = new Telerik.Reporting.TextBox();
            this.textBox299 = new Telerik.Reporting.TextBox();
            this.textBox300 = new Telerik.Reporting.TextBox();
            this.textBox301 = new Telerik.Reporting.TextBox();
            this.textBox302 = new Telerik.Reporting.TextBox();
            this.textBox303 = new Telerik.Reporting.TextBox();
            this.textBox304 = new Telerik.Reporting.TextBox();
            this.textBox305 = new Telerik.Reporting.TextBox();
            this.textBox306 = new Telerik.Reporting.TextBox();
            this.textBox307 = new Telerik.Reporting.TextBox();
            this.textBox308 = new Telerik.Reporting.TextBox();
            this.textBox309 = new Telerik.Reporting.TextBox();
            this.textBox310 = new Telerik.Reporting.TextBox();
            this.textBox311 = new Telerik.Reporting.TextBox();
            this.textBox312 = new Telerik.Reporting.TextBox();
            this.textBox313 = new Telerik.Reporting.TextBox();
            this.htmlTextBox3 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox4 = new Telerik.Reporting.HtmlTextBox();
            this.textBox153 = new Telerik.Reporting.TextBox();
            this.textBox315 = new Telerik.Reporting.TextBox();
            this.textBox316 = new Telerik.Reporting.TextBox();
            this.page4 = new Telerik.Reporting.Panel();
            this.textBox152 = new Telerik.Reporting.TextBox();
            this.table59 = new Telerik.Reporting.Table();
            this.textBox236 = new Telerik.Reporting.TextBox();
            this.textBox237 = new Telerik.Reporting.TextBox();
            this.textBox238 = new Telerik.Reporting.TextBox();
            this.textBox239 = new Telerik.Reporting.TextBox();
            this.textBox240 = new Telerik.Reporting.TextBox();
            this.textBox241 = new Telerik.Reporting.TextBox();
            this.textBox242 = new Telerik.Reporting.TextBox();
            this.textBox243 = new Telerik.Reporting.TextBox();
            this.textBox244 = new Telerik.Reporting.TextBox();
            this.textBox245 = new Telerik.Reporting.TextBox();
            this.textBox246 = new Telerik.Reporting.TextBox();
            this.textBox247 = new Telerik.Reporting.TextBox();
            this.textBox248 = new Telerik.Reporting.TextBox();
            this.textBox249 = new Telerik.Reporting.TextBox();
            this.textBox250 = new Telerik.Reporting.TextBox();
            this.textBox251 = new Telerik.Reporting.TextBox();
            this.textBox252 = new Telerik.Reporting.TextBox();
            this.textBox253 = new Telerik.Reporting.TextBox();
            this.textBox254 = new Telerik.Reporting.TextBox();
            this.textBox255 = new Telerik.Reporting.TextBox();
            this.textBox256 = new Telerik.Reporting.TextBox();
            this.textBox257 = new Telerik.Reporting.TextBox();
            this.textBox258 = new Telerik.Reporting.TextBox();
            this.textBox259 = new Telerik.Reporting.TextBox();
            this.textBox260 = new Telerik.Reporting.TextBox();
            this.textBox261 = new Telerik.Reporting.TextBox();
            this.textBox262 = new Telerik.Reporting.TextBox();
            this.textBox263 = new Telerik.Reporting.TextBox();
            this.textBox264 = new Telerik.Reporting.TextBox();
            this.textBox265 = new Telerik.Reporting.TextBox();
            this.textBox266 = new Telerik.Reporting.TextBox();
            this.textBox267 = new Telerik.Reporting.TextBox();
            this.textBox268 = new Telerik.Reporting.TextBox();
            this.textBox269 = new Telerik.Reporting.TextBox();
            this.textBox270 = new Telerik.Reporting.TextBox();
            this.textBox271 = new Telerik.Reporting.TextBox();
            this.textBox272 = new Telerik.Reporting.TextBox();
            this.textBox273 = new Telerik.Reporting.TextBox();
            this.textBox274 = new Telerik.Reporting.TextBox();
            this.textBox275 = new Telerik.Reporting.TextBox();
            this.textBox276 = new Telerik.Reporting.TextBox();
            this.textBox277 = new Telerik.Reporting.TextBox();
            this.textBox278 = new Telerik.Reporting.TextBox();
            this.textBox279 = new Telerik.Reporting.TextBox();
            this.textBox280 = new Telerik.Reporting.TextBox();
            this.textBox281 = new Telerik.Reporting.TextBox();
            this.textBox282 = new Telerik.Reporting.TextBox();
            this.textBox283 = new Telerik.Reporting.TextBox();
            this.textBox284 = new Telerik.Reporting.TextBox();
            this.textBox285 = new Telerik.Reporting.TextBox();
            this.textBox286 = new Telerik.Reporting.TextBox();
            this.textBox287 = new Telerik.Reporting.TextBox();
            this.textBox288 = new Telerik.Reporting.TextBox();
            this.textBox289 = new Telerik.Reporting.TextBox();
            this.textBox290 = new Telerik.Reporting.TextBox();
            this.textBox291 = new Telerik.Reporting.TextBox();
            this.textBox292 = new Telerik.Reporting.TextBox();
            this.page3 = new Telerik.Reporting.Panel();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox134 = new Telerik.Reporting.TextBox();
            this.textBox135 = new Telerik.Reporting.TextBox();
            this.textBox136 = new Telerik.Reporting.TextBox();
            this.textBox137 = new Telerik.Reporting.TextBox();
            this.textBox138 = new Telerik.Reporting.TextBox();
            this.textBox139 = new Telerik.Reporting.TextBox();
            this.textBox140 = new Telerik.Reporting.TextBox();
            this.textBox141 = new Telerik.Reporting.TextBox();
            this.textBox142 = new Telerik.Reporting.TextBox();
            this.textBox143 = new Telerik.Reporting.TextBox();
            this.textBox144 = new Telerik.Reporting.TextBox();
            this.textBox145 = new Telerik.Reporting.TextBox();
            this.textBox146 = new Telerik.Reporting.TextBox();
            this.textBox147 = new Telerik.Reporting.TextBox();
            this.textBox148 = new Telerik.Reporting.TextBox();
            this.textBox149 = new Telerik.Reporting.TextBox();
            this.textBox150 = new Telerik.Reporting.TextBox();
            this.textBox151 = new Telerik.Reporting.TextBox();
            this.textBox155 = new Telerik.Reporting.TextBox();
            this.textBox159 = new Telerik.Reporting.TextBox();
            this.textBox167 = new Telerik.Reporting.TextBox();
            this.textBox168 = new Telerik.Reporting.TextBox();
            this.textBox169 = new Telerik.Reporting.TextBox();
            this.textBox170 = new Telerik.Reporting.TextBox();
            this.textBox163 = new Telerik.Reporting.TextBox();
            this.textBox164 = new Telerik.Reporting.TextBox();
            this.textBox165 = new Telerik.Reporting.TextBox();
            this.textBox166 = new Telerik.Reporting.TextBox();
            this.textBox171 = new Telerik.Reporting.TextBox();
            this.textBox172 = new Telerik.Reporting.TextBox();
            this.textBox173 = new Telerik.Reporting.TextBox();
            this.textBox174 = new Telerik.Reporting.TextBox();
            this.textBox175 = new Telerik.Reporting.TextBox();
            this.textBox176 = new Telerik.Reporting.TextBox();
            this.textBox177 = new Telerik.Reporting.TextBox();
            this.textBox178 = new Telerik.Reporting.TextBox();
            this.textBox179 = new Telerik.Reporting.TextBox();
            this.textBox180 = new Telerik.Reporting.TextBox();
            this.textBox181 = new Telerik.Reporting.TextBox();
            this.textBox182 = new Telerik.Reporting.TextBox();
            this.textBox183 = new Telerik.Reporting.TextBox();
            this.textBox185 = new Telerik.Reporting.TextBox();
            this.textBox186 = new Telerik.Reporting.TextBox();
            this.textBox187 = new Telerik.Reporting.TextBox();
            this.textBox188 = new Telerik.Reporting.TextBox();
            this.textBox192 = new Telerik.Reporting.TextBox();
            this.textBox193 = new Telerik.Reporting.TextBox();
            this.textBox194 = new Telerik.Reporting.TextBox();
            this.textBox195 = new Telerik.Reporting.TextBox();
            this.textBox196 = new Telerik.Reporting.TextBox();
            this.textBox197 = new Telerik.Reporting.TextBox();
            this.textBox198 = new Telerik.Reporting.TextBox();
            this.textBox199 = new Telerik.Reporting.TextBox();
            this.textBox201 = new Telerik.Reporting.TextBox();
            this.textBox202 = new Telerik.Reporting.TextBox();
            this.textBox203 = new Telerik.Reporting.TextBox();
            this.textBox205 = new Telerik.Reporting.TextBox();
            this.textBox206 = new Telerik.Reporting.TextBox();
            this.textBox207 = new Telerik.Reporting.TextBox();
            this.textBox209 = new Telerik.Reporting.TextBox();
            this.textBox210 = new Telerik.Reporting.TextBox();
            this.textBox211 = new Telerik.Reporting.TextBox();
            this.textBox213 = new Telerik.Reporting.TextBox();
            this.textBox214 = new Telerik.Reporting.TextBox();
            this.textBox215 = new Telerik.Reporting.TextBox();
            this.textBox217 = new Telerik.Reporting.TextBox();
            this.textBox218 = new Telerik.Reporting.TextBox();
            this.textBox219 = new Telerik.Reporting.TextBox();
            this.textBox220 = new Telerik.Reporting.TextBox();
            this.textBox184 = new Telerik.Reporting.TextBox();
            this.textBox216 = new Telerik.Reporting.TextBox();
            this.textBox212 = new Telerik.Reporting.TextBox();
            this.textBox208 = new Telerik.Reporting.TextBox();
            this.textBox204 = new Telerik.Reporting.TextBox();
            this.textBox200 = new Telerik.Reporting.TextBox();
            this.textBox189 = new Telerik.Reporting.TextBox();
            this.textBox190 = new Telerik.Reporting.TextBox();
            this.textBox191 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox120 = new Telerik.Reporting.TextBox();
            this.table17 = new Telerik.Reporting.Table();
            this.checkBox6 = new Telerik.Reporting.CheckBox();
            this.checkBox11 = new Telerik.Reporting.CheckBox();
            this.checkBox20 = new Telerik.Reporting.CheckBox();
            this.table18 = new Telerik.Reporting.Table();
            this.checkBox21 = new Telerik.Reporting.CheckBox();
            this.checkBox37 = new Telerik.Reporting.CheckBox();
            this.checkBox38 = new Telerik.Reporting.CheckBox();
            this.table19 = new Telerik.Reporting.Table();
            this.checkBox39 = new Telerik.Reporting.CheckBox();
            this.checkBox40 = new Telerik.Reporting.CheckBox();
            this.checkBox41 = new Telerik.Reporting.CheckBox();
            this.table23 = new Telerik.Reporting.Table();
            this.checkBox42 = new Telerik.Reporting.CheckBox();
            this.checkBox55 = new Telerik.Reporting.CheckBox();
            this.table27 = new Telerik.Reporting.Table();
            this.checkBox56 = new Telerik.Reporting.CheckBox();
            this.checkBox57 = new Telerik.Reporting.CheckBox();
            this.table43 = new Telerik.Reporting.Table();
            this.checkBox58 = new Telerik.Reporting.CheckBox();
            this.checkBox59 = new Telerik.Reporting.CheckBox();
            this.table56 = new Telerik.Reporting.Table();
            this.checkBox60 = new Telerik.Reporting.CheckBox();
            this.checkBox61 = new Telerik.Reporting.CheckBox();
            this.table57 = new Telerik.Reporting.Table();
            this.checkBox62 = new Telerik.Reporting.CheckBox();
            this.checkBox63 = new Telerik.Reporting.CheckBox();
            this.table58 = new Telerik.Reporting.Table();
            this.checkBox64 = new Telerik.Reporting.CheckBox();
            this.checkBox65 = new Telerik.Reporting.CheckBox();
            this.pageHeaderSection1 = new Telerik.Reporting.PageHeaderSection();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.06D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox40.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox40.Style.Color = System.Drawing.Color.White;
            this.textBox40.Style.Font.Bold = true;
            this.textBox40.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.Value = "GENERATION & STORAGE SYSTEM DETAILS";
            // 
            // textBox94
            // 
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.06D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox94.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox94.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox94.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox94.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox94.Style.Color = System.Drawing.Color.White;
            this.textBox94.Style.Font.Bold = true;
            this.textBox94.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox94.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox94.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox94.Value = "EXISTING: GENERATION & STORAGE SYSTEM DETAILS";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.06D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox57.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox57.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox57.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox57.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox57.Style.Color = System.Drawing.Color.White;
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox57.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.Value = "FINAL: GENERATION & STORAGE SYSTEM DETAILS";
            // 
            // htmlTextBox5
            // 
            this.htmlTextBox5.Name = "htmlTextBox5";
            this.htmlTextBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.895D), Telerik.Reporting.Drawing.Unit.Inch(0.885D));
            this.htmlTextBox5.StyleName = "";
            this.htmlTextBox5.Value = resources.GetString("htmlTextBox5.Value");
            // 
            // htmlTextBox12
            // 
            this.htmlTextBox12.Name = "htmlTextBox12";
            this.htmlTextBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.801D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.htmlTextBox12.StyleName = "";
            this.htmlTextBox12.Value = resources.GetString("htmlTextBox12.Value");
            // 
            // htmlTextBox17
            // 
            this.htmlTextBox17.Name = "htmlTextBox17";
            this.htmlTextBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.617D), Telerik.Reporting.Drawing.Unit.Inch(0.369D));
            this.htmlTextBox17.StyleName = "";
            this.htmlTextBox17.Value = resources.GetString("htmlTextBox17.Value");
            // 
            // textBox333
            // 
            this.textBox333.Name = "textBox333";
            this.textBox333.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.03D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox333.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox333.Value = "Mail:";
            // 
            // textBox341
            // 
            this.textBox341.Name = "textBox341";
            this.textBox341.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.644D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox341.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox341.Value = "PO BOX 449, Mount Waverley, VIC 3149";
            // 
            // textBox161
            // 
            this.textBox161.Name = "textBox161";
            this.textBox161.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox161.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox161.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox161.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox161.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox161.Style.Font.Bold = true;
            this.textBox161.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox161.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox161.StyleName = "";
            this.textBox161.Value = "INVERTER NUMBER";
            // 
            // textBox156
            // 
            this.textBox156.Name = "textBox156";
            this.textBox156.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox156.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox156.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox156.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox156.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox156.Style.Font.Bold = true;
            this.textBox156.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox156.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox156.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox156.StyleName = "";
            this.textBox156.Value = "INVERTER SERIAL NUMBER";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.059D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox26.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox26.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox26.Style.Color = System.Drawing.Color.White;
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.Value = "INVERTER DETAILS: MANDATORY FOR NEW / MODIFY / ADD / DECOMMISSION INSTALLATIONS";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(1548.39D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.page2,
            this.panel4,
            this.page6,
            this.page5,
            this.page4,
            this.page3});
            this.detail.Name = "detail";
            this.detail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // page2
            // 
            this.page2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table3,
            this.table4,
            this.table29,
            this.htmlTextBox2,
            this.textBox66});
            this.page2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(10.16D));
            this.page2.Name = "page2";
            this.page2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.193D), Telerik.Reporting.Drawing.Unit.Inch(10.15D));
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.7D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox41);
            this.table3.Body.SetCellContent(3, 0, this.textBox42);
            this.table3.Body.SetCellContent(3, 4, this.textBox52);
            this.table3.Body.SetCellContent(1, 0, this.textBox53);
            this.table3.Body.SetCellContent(1, 1, this.table5);
            this.table3.Body.SetCellContent(1, 2, this.table6);
            this.table3.Body.SetCellContent(1, 3, this.table7);
            this.table3.Body.SetCellContent(1, 4, this.table8);
            this.table3.Body.SetCellContent(2, 1, this.table10);
            this.table3.Body.SetCellContent(2, 2, this.table11);
            this.table3.Body.SetCellContent(2, 3, this.table12);
            this.table3.Body.SetCellContent(2, 4, this.table13);
            this.table3.Body.SetCellContent(3, 1, this.table14);
            this.table3.Body.SetCellContent(3, 2, this.table15);
            this.table3.Body.SetCellContent(3, 3, this.table16);
            this.table3.Body.SetCellContent(0, 1, this.checkBox1);
            this.table3.Body.SetCellContent(0, 2, this.checkBox36);
            this.table3.Body.SetCellContent(0, 3, this.checkBox2);
            this.table3.Body.SetCellContent(0, 4, this.checkBox3);
            this.table3.Body.SetCellContent(2, 0, this.htmlTextBox1);
            tableGroup39.Name = "group20";
            tableGroup40.Name = "group19";
            tableGroup41.Name = "group18";
            tableGroup42.Name = "tableGroup5";
            tableGroup43.Name = "tableGroup6";
            tableGroup38.ChildGroups.Add(tableGroup39);
            tableGroup38.ChildGroups.Add(tableGroup40);
            tableGroup38.ChildGroups.Add(tableGroup41);
            tableGroup38.ChildGroups.Add(tableGroup42);
            tableGroup38.ChildGroups.Add(tableGroup43);
            tableGroup38.Name = "tableGroup4";
            tableGroup38.ReportItem = this.textBox40;
            this.table3.ColumnGroups.Add(tableGroup38);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox41,
            this.checkBox1,
            this.checkBox36,
            this.checkBox2,
            this.checkBox3,
            this.textBox53,
            this.table5,
            this.table6,
            this.table7,
            this.table8,
            this.htmlTextBox1,
            this.table10,
            this.table11,
            this.table12,
            this.table13,
            this.textBox42,
            this.table14,
            this.table15,
            this.table16,
            this.textBox52,
            this.textBox40});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.093D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.table3.Name = "table3";
            tableGroup45.Name = "group21";
            tableGroup46.Name = "group23";
            tableGroup47.Name = "group24";
            tableGroup48.Name = "group22";
            tableGroup44.ChildGroups.Add(tableGroup45);
            tableGroup44.ChildGroups.Add(tableGroup46);
            tableGroup44.ChildGroups.Add(tableGroup47);
            tableGroup44.ChildGroups.Add(tableGroup48);
            tableGroup44.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup44.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup44);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.06D), Telerik.Reporting.Drawing.Unit.Inch(1.08D));
            this.table3.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox41.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox41.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox41.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox41.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox41.Style.Color = System.Drawing.Color.White;
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.1D);
            this.textBox41.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.Value = "Status of the Generation System";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox42.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox42.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox42.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox42.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox42.Style.Color = System.Drawing.Color.White;
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.1D);
            this.textBox42.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.StyleName = "";
            this.textBox42.Value = "Eligible for Victorian Gov. Rebate";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox52.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox52.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox52.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox52.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox52.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox52.StyleName = "";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox53.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox53.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox53.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox53.Style.Color = System.Drawing.Color.White;
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.1D);
            this.textBox53.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.StyleName = "";
            this.textBox53.Value = "No of Phase(s) Available at the Site";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.465D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.465D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.452D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table5.Body.SetCellContent(0, 0, this.checkBox7);
            this.table5.Body.SetCellContent(0, 1, this.checkBox8);
            this.table5.Body.SetCellContent(0, 2, this.checkBox9);
            tableGroup1.Name = "tableGroup7";
            tableGroup2.Name = "tableGroup9";
            tableGroup3.Name = "group25";
            this.table5.ColumnGroups.Add(tableGroup1);
            this.table5.ColumnGroups.Add(tableGroup2);
            this.table5.ColumnGroups.Add(tableGroup3);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox7,
            this.checkBox8,
            this.checkBox9});
            this.table5.Name = "table5";
            tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup4.Name = "detailTableGroup3";
            this.table5.RowGroups.Add(tableGroup4);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table5.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table5.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.table5.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // checkBox7
            // 
            this.checkBox7.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox7.CheckedImage = ((object)(resources.GetObject("checkBox7.CheckedImage")));
            this.checkBox7.IndeterminateImage = ((object)(resources.GetObject("checkBox7.IndeterminateImage")));
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.465D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox7.StyleName = "";
            this.checkBox7.Text = " 1";
            this.checkBox7.UncheckedImage = ((object)(resources.GetObject("checkBox7.UncheckedImage")));
            this.checkBox7.Value = "";
            // 
            // checkBox8
            // 
            this.checkBox8.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox8.CheckedImage = ((object)(resources.GetObject("checkBox8.CheckedImage")));
            this.checkBox8.IndeterminateImage = ((object)(resources.GetObject("checkBox8.IndeterminateImage")));
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.465D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox8.StyleName = "";
            this.checkBox8.Text = " 2";
            this.checkBox8.UncheckedImage = ((object)(resources.GetObject("checkBox8.UncheckedImage")));
            this.checkBox8.Value = "";
            // 
            // checkBox9
            // 
            this.checkBox9.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox9.CheckedImage = ((object)(resources.GetObject("checkBox9.CheckedImage")));
            this.checkBox9.IndeterminateImage = ((object)(resources.GetObject("checkBox9.IndeterminateImage")));
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.452D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox9.StyleName = "";
            this.checkBox9.Text = " 3";
            this.checkBox9.UncheckedImage = ((object)(resources.GetObject("checkBox9.UncheckedImage")));
            this.checkBox9.Value = "";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.535D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.535D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.52D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table6.Body.SetCellContent(0, 0, this.checkBox10);
            this.table6.Body.SetCellContent(0, 1, this.checkBox12);
            this.table6.Body.SetCellContent(0, 2, this.checkBox13);
            tableGroup5.Name = "tableGroup7";
            tableGroup6.Name = "tableGroup9";
            tableGroup7.Name = "group25";
            this.table6.ColumnGroups.Add(tableGroup5);
            this.table6.ColumnGroups.Add(tableGroup6);
            this.table6.ColumnGroups.Add(tableGroup7);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox10,
            this.checkBox12,
            this.checkBox13});
            this.table6.Name = "table6";
            tableGroup8.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup8.Name = "detailTableGroup3";
            this.table6.RowGroups.Add(tableGroup8);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table6.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table6.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // checkBox10
            // 
            this.checkBox10.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox10.CheckedImage = ((object)(resources.GetObject("checkBox10.CheckedImage")));
            this.checkBox10.IndeterminateImage = ((object)(resources.GetObject("checkBox10.IndeterminateImage")));
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.535D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox10.StyleName = "";
            this.checkBox10.Text = " 1";
            this.checkBox10.UncheckedImage = ((object)(resources.GetObject("checkBox10.UncheckedImage")));
            this.checkBox10.Value = "";
            // 
            // checkBox12
            // 
            this.checkBox12.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox12.CheckedImage = ((object)(resources.GetObject("checkBox12.CheckedImage")));
            this.checkBox12.IndeterminateImage = ((object)(resources.GetObject("checkBox12.IndeterminateImage")));
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.535D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox12.StyleName = "";
            this.checkBox12.Text = " 2";
            this.checkBox12.UncheckedImage = ((object)(resources.GetObject("checkBox12.UncheckedImage")));
            this.checkBox12.Value = "";
            // 
            // checkBox13
            // 
            this.checkBox13.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox13.CheckedImage = ((object)(resources.GetObject("checkBox13.CheckedImage")));
            this.checkBox13.IndeterminateImage = ((object)(resources.GetObject("checkBox13.IndeterminateImage")));
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.52D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox13.StyleName = "";
            this.checkBox13.Text = " 3";
            this.checkBox13.UncheckedImage = ((object)(resources.GetObject("checkBox13.UncheckedImage")));
            this.checkBox13.Value = "";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.483D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.483D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.499D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table7.Body.SetCellContent(0, 0, this.checkBox14);
            this.table7.Body.SetCellContent(0, 1, this.checkBox15);
            this.table7.Body.SetCellContent(0, 2, this.checkBox16);
            tableGroup9.Name = "tableGroup7";
            tableGroup10.Name = "tableGroup9";
            tableGroup11.Name = "group25";
            this.table7.ColumnGroups.Add(tableGroup9);
            this.table7.ColumnGroups.Add(tableGroup10);
            this.table7.ColumnGroups.Add(tableGroup11);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox14,
            this.checkBox15,
            this.checkBox16});
            this.table7.Name = "table7";
            tableGroup12.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup12.Name = "detailTableGroup3";
            this.table7.RowGroups.Add(tableGroup12);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.465D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table7.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table7.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // checkBox14
            // 
            this.checkBox14.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox14.CheckedImage = ((object)(resources.GetObject("checkBox14.CheckedImage")));
            this.checkBox14.IndeterminateImage = ((object)(resources.GetObject("checkBox14.IndeterminateImage")));
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.483D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox14.StyleName = "";
            this.checkBox14.Text = " 1";
            this.checkBox14.UncheckedImage = ((object)(resources.GetObject("checkBox14.UncheckedImage")));
            this.checkBox14.Value = "";
            // 
            // checkBox15
            // 
            this.checkBox15.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox15.CheckedImage = ((object)(resources.GetObject("checkBox15.CheckedImage")));
            this.checkBox15.IndeterminateImage = ((object)(resources.GetObject("checkBox15.IndeterminateImage")));
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.483D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox15.StyleName = "";
            this.checkBox15.Text = " 2";
            this.checkBox15.UncheckedImage = ((object)(resources.GetObject("checkBox15.UncheckedImage")));
            this.checkBox15.Value = "";
            // 
            // checkBox16
            // 
            this.checkBox16.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox16.CheckedImage = ((object)(resources.GetObject("checkBox16.CheckedImage")));
            this.checkBox16.IndeterminateImage = ((object)(resources.GetObject("checkBox16.IndeterminateImage")));
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.499D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox16.StyleName = "";
            this.checkBox16.Text = " 3";
            this.checkBox16.UncheckedImage = ((object)(resources.GetObject("checkBox16.UncheckedImage")));
            this.checkBox16.Value = "";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.484D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.484D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.425D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table8.Body.SetCellContent(0, 0, this.checkBox17);
            this.table8.Body.SetCellContent(0, 1, this.checkBox18);
            this.table8.Body.SetCellContent(0, 2, this.checkBox19);
            tableGroup13.Name = "tableGroup7";
            tableGroup14.Name = "tableGroup9";
            tableGroup15.Name = "group25";
            this.table8.ColumnGroups.Add(tableGroup13);
            this.table8.ColumnGroups.Add(tableGroup14);
            this.table8.ColumnGroups.Add(tableGroup15);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox17,
            this.checkBox18,
            this.checkBox19});
            this.table8.Name = "table8";
            tableGroup16.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup16.Name = "detailTableGroup3";
            this.table8.RowGroups.Add(tableGroup16);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.393D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table8.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table8.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // checkBox17
            // 
            this.checkBox17.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox17.CheckedImage = ((object)(resources.GetObject("checkBox17.CheckedImage")));
            this.checkBox17.IndeterminateImage = ((object)(resources.GetObject("checkBox17.IndeterminateImage")));
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.484D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox17.StyleName = "";
            this.checkBox17.Text = " 1";
            this.checkBox17.UncheckedImage = ((object)(resources.GetObject("checkBox17.UncheckedImage")));
            this.checkBox17.Value = "";
            // 
            // checkBox18
            // 
            this.checkBox18.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox18.CheckedImage = ((object)(resources.GetObject("checkBox18.CheckedImage")));
            this.checkBox18.IndeterminateImage = ((object)(resources.GetObject("checkBox18.IndeterminateImage")));
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.484D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox18.StyleName = "";
            this.checkBox18.Text = " 2";
            this.checkBox18.UncheckedImage = ((object)(resources.GetObject("checkBox18.UncheckedImage")));
            this.checkBox18.Value = "";
            // 
            // checkBox19
            // 
            this.checkBox19.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox19.CheckedImage = ((object)(resources.GetObject("checkBox19.CheckedImage")));
            this.checkBox19.IndeterminateImage = ((object)(resources.GetObject("checkBox19.IndeterminateImage")));
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.425D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox19.StyleName = "";
            this.checkBox19.Text = " 3";
            this.checkBox19.UncheckedImage = ((object)(resources.GetObject("checkBox19.UncheckedImage")));
            this.checkBox19.Value = "";
            // 
            // table10
            // 
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.653D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.572D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table10.Body.SetCellContent(0, 0, this.checkBox22);
            this.table10.Body.SetCellContent(0, 1, this.checkBox23);
            tableGroup17.Name = "tableGroup7";
            tableGroup18.Name = "tableGroup9";
            this.table10.ColumnGroups.Add(tableGroup17);
            this.table10.ColumnGroups.Add(tableGroup18);
            this.table10.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox22,
            this.checkBox23});
            this.table10.Name = "table10";
            tableGroup19.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup19.Name = "detailTableGroup3";
            this.table10.RowGroups.Add(tableGroup19);
            this.table10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.433D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table10.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table10.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.table10.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox22
            // 
            this.checkBox22.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox22.CheckedImage = ((object)(resources.GetObject("checkBox22.CheckedImage")));
            this.checkBox22.IndeterminateImage = ((object)(resources.GetObject("checkBox22.IndeterminateImage")));
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.653D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox22.StyleName = "";
            this.checkBox22.Text = " Yes";
            this.checkBox22.UncheckedImage = ((object)(resources.GetObject("checkBox22.UncheckedImage")));
            this.checkBox22.Value = "";
            // 
            // checkBox23
            // 
            this.checkBox23.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox23.CheckedImage = ((object)(resources.GetObject("checkBox23.CheckedImage")));
            this.checkBox23.IndeterminateImage = ((object)(resources.GetObject("checkBox23.IndeterminateImage")));
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.572D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox23.StyleName = "";
            this.checkBox23.Text = " No";
            this.checkBox23.UncheckedImage = ((object)(resources.GetObject("checkBox23.UncheckedImage")));
            this.checkBox23.Value = "";
            // 
            // table11
            // 
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.674D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.562D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table11.Body.SetCellContent(0, 0, this.checkBox24);
            this.table11.Body.SetCellContent(0, 1, this.checkBox25);
            tableGroup20.Name = "tableGroup7";
            tableGroup21.Name = "tableGroup9";
            this.table11.ColumnGroups.Add(tableGroup20);
            this.table11.ColumnGroups.Add(tableGroup21);
            this.table11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox24,
            this.checkBox25});
            this.table11.Name = "table11";
            tableGroup22.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup22.Name = "detailTableGroup3";
            this.table11.RowGroups.Add(tableGroup22);
            this.table11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.444D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table11.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table11.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.table11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox24
            // 
            this.checkBox24.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox24.CheckedImage = ((object)(resources.GetObject("checkBox24.CheckedImage")));
            this.checkBox24.IndeterminateImage = ((object)(resources.GetObject("checkBox24.IndeterminateImage")));
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.674D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox24.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox24.StyleName = "";
            this.checkBox24.Text = " Yes";
            this.checkBox24.UncheckedImage = ((object)(resources.GetObject("checkBox24.UncheckedImage")));
            this.checkBox24.Value = "";
            // 
            // checkBox25
            // 
            this.checkBox25.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox25.CheckedImage = ((object)(resources.GetObject("checkBox25.CheckedImage")));
            this.checkBox25.IndeterminateImage = ((object)(resources.GetObject("checkBox25.IndeterminateImage")));
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.562D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox25.StyleName = "";
            this.checkBox25.Text = " No";
            this.checkBox25.UncheckedImage = ((object)(resources.GetObject("checkBox25.UncheckedImage")));
            this.checkBox25.Value = "";
            // 
            // table12
            // 
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.674D)));
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.541D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table12.Body.SetCellContent(0, 0, this.checkBox26);
            this.table12.Body.SetCellContent(0, 1, this.checkBox27);
            tableGroup23.Name = "tableGroup7";
            tableGroup24.Name = "tableGroup9";
            this.table12.ColumnGroups.Add(tableGroup23);
            this.table12.ColumnGroups.Add(tableGroup24);
            this.table12.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox26,
            this.checkBox27});
            this.table12.Name = "table12";
            tableGroup25.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup25.Name = "detailTableGroup3";
            this.table12.RowGroups.Add(tableGroup25);
            this.table12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.423D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table12.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table12.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.table12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox26
            // 
            this.checkBox26.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox26.CheckedImage = ((object)(resources.GetObject("checkBox26.CheckedImage")));
            this.checkBox26.IndeterminateImage = ((object)(resources.GetObject("checkBox26.IndeterminateImage")));
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.674D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox26.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox26.StyleName = "";
            this.checkBox26.Text = " Yes";
            this.checkBox26.UncheckedImage = ((object)(resources.GetObject("checkBox26.UncheckedImage")));
            this.checkBox26.Value = "";
            // 
            // checkBox27
            // 
            this.checkBox27.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox27.CheckedImage = ((object)(resources.GetObject("checkBox27.CheckedImage")));
            this.checkBox27.IndeterminateImage = ((object)(resources.GetObject("checkBox27.IndeterminateImage")));
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.541D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox27.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox27.StyleName = "";
            this.checkBox27.Text = " No";
            this.checkBox27.UncheckedImage = ((object)(resources.GetObject("checkBox27.UncheckedImage")));
            this.checkBox27.Value = "";
            // 
            // table13
            // 
            this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.622D)));
            this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.551D)));
            this.table13.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table13.Body.SetCellContent(0, 0, this.checkBox28);
            this.table13.Body.SetCellContent(0, 1, this.checkBox29);
            tableGroup26.Name = "tableGroup7";
            tableGroup27.Name = "tableGroup9";
            this.table13.ColumnGroups.Add(tableGroup26);
            this.table13.ColumnGroups.Add(tableGroup27);
            this.table13.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox28,
            this.checkBox29});
            this.table13.Name = "table13";
            tableGroup28.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup28.Name = "detailTableGroup3";
            this.table13.RowGroups.Add(tableGroup28);
            this.table13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.381D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table13.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table13.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table13.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.table13.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox28
            // 
            this.checkBox28.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox28.CheckedImage = ((object)(resources.GetObject("checkBox28.CheckedImage")));
            this.checkBox28.IndeterminateImage = ((object)(resources.GetObject("checkBox28.IndeterminateImage")));
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.622D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox28.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox28.StyleName = "";
            this.checkBox28.Text = " Yes";
            this.checkBox28.UncheckedImage = ((object)(resources.GetObject("checkBox28.UncheckedImage")));
            this.checkBox28.Value = "";
            // 
            // checkBox29
            // 
            this.checkBox29.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox29.CheckedImage = ((object)(resources.GetObject("checkBox29.CheckedImage")));
            this.checkBox29.IndeterminateImage = ((object)(resources.GetObject("checkBox29.IndeterminateImage")));
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.551D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox29.StyleName = "";
            this.checkBox29.Text = " No";
            this.checkBox29.UncheckedImage = ((object)(resources.GetObject("checkBox29.UncheckedImage")));
            this.checkBox29.Value = "";
            // 
            // table14
            // 
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.685D)));
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.551D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table14.Body.SetCellContent(0, 0, this.checkBox30);
            this.table14.Body.SetCellContent(0, 1, this.checkBox31);
            tableGroup29.Name = "tableGroup7";
            tableGroup30.Name = "tableGroup9";
            this.table14.ColumnGroups.Add(tableGroup29);
            this.table14.ColumnGroups.Add(tableGroup30);
            this.table14.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox30,
            this.checkBox31});
            this.table14.Name = "table14";
            tableGroup31.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup31.Name = "detailTableGroup3";
            this.table14.RowGroups.Add(tableGroup31);
            this.table14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.444D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table14.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table14.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table14.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.table14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox30
            // 
            this.checkBox30.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox30.CheckedImage = ((object)(resources.GetObject("checkBox30.CheckedImage")));
            this.checkBox30.IndeterminateImage = ((object)(resources.GetObject("checkBox30.IndeterminateImage")));
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.685D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox30.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox30.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox30.StyleName = "";
            this.checkBox30.Text = " Yes";
            this.checkBox30.UncheckedImage = ((object)(resources.GetObject("checkBox30.UncheckedImage")));
            this.checkBox30.Value = "";
            // 
            // checkBox31
            // 
            this.checkBox31.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox31.CheckedImage = ((object)(resources.GetObject("checkBox31.CheckedImage")));
            this.checkBox31.IndeterminateImage = ((object)(resources.GetObject("checkBox31.IndeterminateImage")));
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.551D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox31.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox31.StyleName = "";
            this.checkBox31.Text = " No";
            this.checkBox31.UncheckedImage = ((object)(resources.GetObject("checkBox31.UncheckedImage")));
            this.checkBox31.Value = "";
            // 
            // table15
            // 
            this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.674D)));
            this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.551D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table15.Body.SetCellContent(0, 0, this.checkBox32);
            this.table15.Body.SetCellContent(0, 1, this.checkBox33);
            tableGroup32.Name = "tableGroup7";
            tableGroup33.Name = "tableGroup9";
            this.table15.ColumnGroups.Add(tableGroup32);
            this.table15.ColumnGroups.Add(tableGroup33);
            this.table15.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox32,
            this.checkBox33});
            this.table15.Name = "table15";
            tableGroup34.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup34.Name = "detailTableGroup3";
            this.table15.RowGroups.Add(tableGroup34);
            this.table15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.433D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table15.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table15.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table15.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.table15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox32
            // 
            this.checkBox32.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox32.CheckedImage = ((object)(resources.GetObject("checkBox32.CheckedImage")));
            this.checkBox32.IndeterminateImage = ((object)(resources.GetObject("checkBox32.IndeterminateImage")));
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.674D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox32.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox32.StyleName = "";
            this.checkBox32.Text = " Yes";
            this.checkBox32.UncheckedImage = ((object)(resources.GetObject("checkBox32.UncheckedImage")));
            this.checkBox32.Value = "";
            // 
            // checkBox33
            // 
            this.checkBox33.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox33.CheckedImage = ((object)(resources.GetObject("checkBox33.CheckedImage")));
            this.checkBox33.IndeterminateImage = ((object)(resources.GetObject("checkBox33.IndeterminateImage")));
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.551D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox33.StyleName = "";
            this.checkBox33.Text = " No";
            this.checkBox33.UncheckedImage = ((object)(resources.GetObject("checkBox33.UncheckedImage")));
            this.checkBox33.Value = "";
            // 
            // table16
            // 
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.674D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.489D)));
            this.table16.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table16.Body.SetCellContent(0, 0, this.checkBox34);
            this.table16.Body.SetCellContent(0, 1, this.checkBox35);
            tableGroup35.Name = "tableGroup7";
            tableGroup36.Name = "tableGroup9";
            this.table16.ColumnGroups.Add(tableGroup35);
            this.table16.ColumnGroups.Add(tableGroup36);
            this.table16.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox34,
            this.checkBox35});
            this.table16.Name = "table16";
            tableGroup37.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup37.Name = "detailTableGroup3";
            this.table16.RowGroups.Add(tableGroup37);
            this.table16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.371D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table16.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table16.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table16.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.table16.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox34
            // 
            this.checkBox34.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox34.CheckedImage = ((object)(resources.GetObject("checkBox34.CheckedImage")));
            this.checkBox34.IndeterminateImage = ((object)(resources.GetObject("checkBox34.IndeterminateImage")));
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.674D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox34.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox34.StyleName = "";
            this.checkBox34.Text = " Yes";
            this.checkBox34.UncheckedImage = ((object)(resources.GetObject("checkBox34.UncheckedImage")));
            this.checkBox34.Value = "";
            // 
            // checkBox35
            // 
            this.checkBox35.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox35.CheckedImage = ((object)(resources.GetObject("checkBox35.CheckedImage")));
            this.checkBox35.IndeterminateImage = ((object)(resources.GetObject("checkBox35.IndeterminateImage")));
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.489D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox35.StyleName = "";
            this.checkBox35.Text = " No";
            this.checkBox35.UncheckedImage = ((object)(resources.GetObject("checkBox35.UncheckedImage")));
            this.checkBox35.Value = "";
            // 
            // checkBox1
            // 
            this.checkBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox1.CheckedImage = ((object)(resources.GetObject("checkBox1.CheckedImage")));
            this.checkBox1.IndeterminateImage = ((object)(resources.GetObject("checkBox1.IndeterminateImage")));
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox1.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.checkBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.checkBox1.Style.Color = System.Drawing.Color.MidnightBlue;
            this.checkBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox1.StyleName = "";
            this.checkBox1.Text = "New Installation";
            this.checkBox1.UncheckedImage = ((object)(resources.GetObject("checkBox1.UncheckedImage")));
            this.checkBox1.Value = "";
            // 
            // checkBox36
            // 
            this.checkBox36.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox36.CheckedImage = ((object)(resources.GetObject("checkBox36.CheckedImage")));
            this.checkBox36.IndeterminateImage = ((object)(resources.GetObject("checkBox36.IndeterminateImage")));
            this.checkBox36.Name = "checkBox36";
            this.checkBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox36.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.checkBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox36.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.checkBox36.Style.Color = System.Drawing.Color.MidnightBlue;
            this.checkBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox36.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox36.StyleName = "";
            this.checkBox36.Text = "Modify Installtion";
            this.checkBox36.UncheckedImage = ((object)(resources.GetObject("checkBox36.UncheckedImage")));
            this.checkBox36.Value = "";
            // 
            // checkBox2
            // 
            this.checkBox2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox2.CheckedImage = ((object)(resources.GetObject("checkBox2.CheckedImage")));
            this.checkBox2.IndeterminateImage = ((object)(resources.GetObject("checkBox2.IndeterminateImage")));
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox2.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.checkBox2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.checkBox2.Style.Color = System.Drawing.Color.MidnightBlue;
            this.checkBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox2.StyleName = "";
            this.checkBox2.Text = "Add Installation";
            this.checkBox2.UncheckedImage = ((object)(resources.GetObject("checkBox2.UncheckedImage")));
            this.checkBox2.Value = "";
            // 
            // checkBox3
            // 
            this.checkBox3.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox3.CheckedImage = ((object)(resources.GetObject("checkBox3.CheckedImage")));
            this.checkBox3.IndeterminateImage = ((object)(resources.GetObject("checkBox3.IndeterminateImage")));
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox3.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.checkBox3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.checkBox3.Style.Color = System.Drawing.Color.MidnightBlue;
            this.checkBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox3.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox3.StyleName = "";
            this.checkBox3.Text = "Decommission/Upgrade";
            this.checkBox3.UncheckedImage = ((object)(resources.GetObject("checkBox3.UncheckedImage")));
            this.checkBox3.Value = "";
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.htmlTextBox1.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.htmlTextBox1.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.htmlTextBox1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.htmlTextBox1.Style.Color = System.Drawing.Color.White;
            this.htmlTextBox1.Style.Font.Bold = true;
            this.htmlTextBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.1D);
            this.htmlTextBox1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.htmlTextBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox1.StyleName = "";
            this.htmlTextBox1.Value = "<p style=\"text-align: justify\">Multi-mode (Hybrid) Inverter <sup>1</sup></p>";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.7D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table4.Body.SetCellContent(0, 2, this.table20);
            this.table4.Body.SetCellContent(0, 3, this.table21);
            this.table4.Body.SetCellContent(0, 4, this.table22);
            this.table4.Body.SetCellContent(1, 2, this.table24);
            this.table4.Body.SetCellContent(1, 3, this.table25);
            this.table4.Body.SetCellContent(1, 4, this.table26);
            this.table4.Body.SetCellContent(2, 2, this.table28);
            this.table4.Body.SetCellContent(2, 3, this.table41);
            this.table4.Body.SetCellContent(2, 4, this.table42);
            this.table4.Body.SetCellContent(3, 2, this.table53);
            this.table4.Body.SetCellContent(3, 3, this.table54);
            this.table4.Body.SetCellContent(3, 4, this.table55);
            this.table4.Body.SetCellContent(0, 0, this.textBox90);
            this.table4.Body.SetCellContent(1, 0, this.textBox91);
            this.table4.Body.SetCellContent(3, 0, this.textBox92);
            this.table4.Body.SetCellContent(2, 0, this.textBox93);
            this.table4.Body.SetCellContent(0, 1, this.textBox43);
            this.table4.Body.SetCellContent(1, 1, this.textBox46);
            this.table4.Body.SetCellContent(2, 1, this.textBox48);
            this.table4.Body.SetCellContent(3, 1, this.textBox49);
            tableGroup101.Name = "group20";
            tableGroup102.Name = "group19";
            tableGroup103.Name = "group18";
            tableGroup104.Name = "tableGroup5";
            tableGroup105.Name = "tableGroup6";
            tableGroup100.ChildGroups.Add(tableGroup101);
            tableGroup100.ChildGroups.Add(tableGroup102);
            tableGroup100.ChildGroups.Add(tableGroup103);
            tableGroup100.ChildGroups.Add(tableGroup104);
            tableGroup100.ChildGroups.Add(tableGroup105);
            tableGroup100.Name = "tableGroup4";
            tableGroup100.ReportItem = this.textBox94;
            this.table4.ColumnGroups.Add(tableGroup100);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox90,
            this.textBox43,
            this.table20,
            this.table21,
            this.table22,
            this.textBox91,
            this.textBox46,
            this.table24,
            this.table25,
            this.table26,
            this.textBox93,
            this.textBox48,
            this.table28,
            this.table41,
            this.table42,
            this.textBox92,
            this.textBox49,
            this.table53,
            this.table54,
            this.table55,
            this.textBox94});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.093D), Telerik.Reporting.Drawing.Unit.Inch(1.1D));
            this.table4.Name = "table4";
            tableGroup107.Name = "group21";
            tableGroup108.Name = "group23";
            tableGroup109.Name = "group24";
            tableGroup110.Name = "group29";
            tableGroup106.ChildGroups.Add(tableGroup107);
            tableGroup106.ChildGroups.Add(tableGroup108);
            tableGroup106.ChildGroups.Add(tableGroup109);
            tableGroup106.ChildGroups.Add(tableGroup110);
            tableGroup106.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup106.Name = "detailTableGroup2";
            this.table4.RowGroups.Add(tableGroup106);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.06D), Telerik.Reporting.Drawing.Unit.Inch(1.08D));
            this.table4.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table4.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table4.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // table20
            // 
            this.table20.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.372D)));
            this.table20.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.457D)));
            this.table20.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table20.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.389D)));
            this.table20.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table20.Body.SetCellContent(0, 0, this.checkBox43);
            this.table20.Body.SetCellContent(0, 1, this.checkBox44);
            this.table20.Body.SetCellContent(0, 3, this.checkBox45);
            this.table20.Body.SetCellContent(0, 2, this.checkBox46);
            tableGroup49.Name = "tableGroup7";
            tableGroup50.Name = "tableGroup9";
            tableGroup51.Name = "group26";
            tableGroup52.Name = "group25";
            this.table20.ColumnGroups.Add(tableGroup49);
            this.table20.ColumnGroups.Add(tableGroup50);
            this.table20.ColumnGroups.Add(tableGroup51);
            this.table20.ColumnGroups.Add(tableGroup52);
            this.table20.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox43,
            this.checkBox44,
            this.checkBox46,
            this.checkBox45});
            this.table20.Name = "table20";
            tableGroup53.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup53.Name = "detailTableGroup3";
            this.table20.RowGroups.Add(tableGroup53);
            this.table20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.591D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.table20.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table20.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // checkBox43
            // 
            this.checkBox43.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox43.CheckedImage = ((object)(resources.GetObject("checkBox43.CheckedImage")));
            this.checkBox43.IndeterminateImage = ((object)(resources.GetObject("checkBox43.IndeterminateImage")));
            this.checkBox43.Name = "checkBox43";
            this.checkBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.372D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox43.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox43.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox43.StyleName = "";
            this.checkBox43.Text = "Solar";
            this.checkBox43.UncheckedImage = ((object)(resources.GetObject("checkBox43.UncheckedImage")));
            this.checkBox43.Value = "";
            // 
            // checkBox44
            // 
            this.checkBox44.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox44.CheckedImage = ((object)(resources.GetObject("checkBox44.CheckedImage")));
            this.checkBox44.IndeterminateImage = ((object)(resources.GetObject("checkBox44.IndeterminateImage")));
            this.checkBox44.Name = "checkBox44";
            this.checkBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.457D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox44.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox44.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox44.StyleName = "";
            this.checkBox44.Text = " Battery";
            this.checkBox44.UncheckedImage = ((object)(resources.GetObject("checkBox44.UncheckedImage")));
            this.checkBox44.Value = "";
            // 
            // checkBox45
            // 
            this.checkBox45.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox45.CheckedImage = ((object)(resources.GetObject("checkBox45.CheckedImage")));
            this.checkBox45.IndeterminateImage = ((object)(resources.GetObject("checkBox45.IndeterminateImage")));
            this.checkBox45.Name = "checkBox45";
            this.checkBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.389D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox45.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox45.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox45.StyleName = "";
            this.checkBox45.Text = " Other";
            this.checkBox45.UncheckedImage = ((object)(resources.GetObject("checkBox45.UncheckedImage")));
            this.checkBox45.Value = "";
            // 
            // checkBox46
            // 
            this.checkBox46.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox46.CheckedImage = ((object)(resources.GetObject("checkBox46.CheckedImage")));
            this.checkBox46.IndeterminateImage = ((object)(resources.GetObject("checkBox46.IndeterminateImage")));
            this.checkBox46.Name = "checkBox46";
            this.checkBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox46.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox46.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox46.StyleName = "";
            this.checkBox46.Text = " Wind";
            this.checkBox46.UncheckedImage = ((object)(resources.GetObject("checkBox46.UncheckedImage")));
            this.checkBox46.Value = "";
            // 
            // table21
            // 
            this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.372D)));
            this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.457D)));
            this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.389D)));
            this.table21.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table21.Body.SetCellContent(0, 0, this.checkBox47);
            this.table21.Body.SetCellContent(0, 1, this.checkBox48);
            this.table21.Body.SetCellContent(0, 3, this.checkBox49);
            this.table21.Body.SetCellContent(0, 2, this.checkBox50);
            tableGroup54.Name = "tableGroup7";
            tableGroup55.Name = "tableGroup9";
            tableGroup56.Name = "group26";
            tableGroup57.Name = "group25";
            this.table21.ColumnGroups.Add(tableGroup54);
            this.table21.ColumnGroups.Add(tableGroup55);
            this.table21.ColumnGroups.Add(tableGroup56);
            this.table21.ColumnGroups.Add(tableGroup57);
            this.table21.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox47,
            this.checkBox48,
            this.checkBox50,
            this.checkBox49});
            this.table21.Name = "table21";
            tableGroup58.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup58.Name = "detailTableGroup3";
            this.table21.RowGroups.Add(tableGroup58);
            this.table21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.591D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.table21.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table21.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // checkBox47
            // 
            this.checkBox47.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox47.CheckedImage = ((object)(resources.GetObject("checkBox47.CheckedImage")));
            this.checkBox47.IndeterminateImage = ((object)(resources.GetObject("checkBox47.IndeterminateImage")));
            this.checkBox47.Name = "checkBox47";
            this.checkBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.372D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox47.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox47.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox47.StyleName = "";
            this.checkBox47.Text = "Solar";
            this.checkBox47.UncheckedImage = ((object)(resources.GetObject("checkBox47.UncheckedImage")));
            this.checkBox47.Value = "";
            // 
            // checkBox48
            // 
            this.checkBox48.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox48.CheckedImage = ((object)(resources.GetObject("checkBox48.CheckedImage")));
            this.checkBox48.IndeterminateImage = ((object)(resources.GetObject("checkBox48.IndeterminateImage")));
            this.checkBox48.Name = "checkBox48";
            this.checkBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.457D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox48.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox48.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox48.StyleName = "";
            this.checkBox48.Text = " Battery";
            this.checkBox48.UncheckedImage = ((object)(resources.GetObject("checkBox48.UncheckedImage")));
            this.checkBox48.Value = "";
            // 
            // checkBox49
            // 
            this.checkBox49.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox49.CheckedImage = ((object)(resources.GetObject("checkBox49.CheckedImage")));
            this.checkBox49.IndeterminateImage = ((object)(resources.GetObject("checkBox49.IndeterminateImage")));
            this.checkBox49.Name = "checkBox49";
            this.checkBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.389D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox49.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox49.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox49.StyleName = "";
            this.checkBox49.Text = " Other";
            this.checkBox49.UncheckedImage = ((object)(resources.GetObject("checkBox49.UncheckedImage")));
            this.checkBox49.Value = "";
            // 
            // checkBox50
            // 
            this.checkBox50.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox50.CheckedImage = ((object)(resources.GetObject("checkBox50.CheckedImage")));
            this.checkBox50.IndeterminateImage = ((object)(resources.GetObject("checkBox50.IndeterminateImage")));
            this.checkBox50.Name = "checkBox50";
            this.checkBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox50.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox50.StyleName = "";
            this.checkBox50.Text = " Wind";
            this.checkBox50.UncheckedImage = ((object)(resources.GetObject("checkBox50.UncheckedImage")));
            this.checkBox50.Value = "";
            // 
            // table22
            // 
            this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.372D)));
            this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.457D)));
            this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.388D)));
            this.table22.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table22.Body.SetCellContent(0, 0, this.checkBox51);
            this.table22.Body.SetCellContent(0, 1, this.checkBox52);
            this.table22.Body.SetCellContent(0, 3, this.checkBox53);
            this.table22.Body.SetCellContent(0, 2, this.checkBox54);
            tableGroup59.Name = "tableGroup7";
            tableGroup60.Name = "tableGroup9";
            tableGroup61.Name = "group26";
            tableGroup62.Name = "group25";
            this.table22.ColumnGroups.Add(tableGroup59);
            this.table22.ColumnGroups.Add(tableGroup60);
            this.table22.ColumnGroups.Add(tableGroup61);
            this.table22.ColumnGroups.Add(tableGroup62);
            this.table22.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox51,
            this.checkBox52,
            this.checkBox54,
            this.checkBox53});
            this.table22.Name = "table22";
            tableGroup63.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup63.Name = "detailTableGroup3";
            this.table22.RowGroups.Add(tableGroup63);
            this.table22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.table22.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table22.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // checkBox51
            // 
            this.checkBox51.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox51.CheckedImage = ((object)(resources.GetObject("checkBox51.CheckedImage")));
            this.checkBox51.IndeterminateImage = ((object)(resources.GetObject("checkBox51.IndeterminateImage")));
            this.checkBox51.Name = "checkBox51";
            this.checkBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.372D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox51.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox51.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox51.StyleName = "";
            this.checkBox51.Text = "Solar";
            this.checkBox51.UncheckedImage = ((object)(resources.GetObject("checkBox51.UncheckedImage")));
            this.checkBox51.Value = "";
            // 
            // checkBox52
            // 
            this.checkBox52.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox52.CheckedImage = ((object)(resources.GetObject("checkBox52.CheckedImage")));
            this.checkBox52.IndeterminateImage = ((object)(resources.GetObject("checkBox52.IndeterminateImage")));
            this.checkBox52.Name = "checkBox52";
            this.checkBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.457D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox52.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox52.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox52.StyleName = "";
            this.checkBox52.Text = " Battery";
            this.checkBox52.UncheckedImage = ((object)(resources.GetObject("checkBox52.UncheckedImage")));
            this.checkBox52.Value = "";
            // 
            // checkBox53
            // 
            this.checkBox53.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox53.CheckedImage = ((object)(resources.GetObject("checkBox53.CheckedImage")));
            this.checkBox53.IndeterminateImage = ((object)(resources.GetObject("checkBox53.IndeterminateImage")));
            this.checkBox53.Name = "checkBox53";
            this.checkBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.388D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox53.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox53.StyleName = "";
            this.checkBox53.Text = " Other";
            this.checkBox53.UncheckedImage = ((object)(resources.GetObject("checkBox53.UncheckedImage")));
            this.checkBox53.Value = "";
            // 
            // checkBox54
            // 
            this.checkBox54.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox54.CheckedImage = ((object)(resources.GetObject("checkBox54.CheckedImage")));
            this.checkBox54.IndeterminateImage = ((object)(resources.GetObject("checkBox54.IndeterminateImage")));
            this.checkBox54.Name = "checkBox54";
            this.checkBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox54.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox54.StyleName = "";
            this.checkBox54.Text = " Wind";
            this.checkBox54.UncheckedImage = ((object)(resources.GetObject("checkBox54.UncheckedImage")));
            this.checkBox54.Value = "";
            // 
            // table24
            // 
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table24.Body.SetCellContent(0, 0, this.textBox50);
            this.table24.Body.SetCellContent(0, 1, this.textBox56);
            this.table24.Body.SetCellContent(0, 2, this.textBox58);
            tableGroup64.Name = "tableGroup7";
            tableGroup65.Name = "tableGroup9";
            tableGroup66.Name = "group26";
            this.table24.ColumnGroups.Add(tableGroup64);
            this.table24.ColumnGroups.Add(tableGroup65);
            this.table24.ColumnGroups.Add(tableGroup66);
            this.table24.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox50,
            this.textBox56,
            this.textBox58});
            this.table24.Name = "table24";
            tableGroup67.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup67.Name = "detailTableGroup3";
            this.table24.RowGroups.Add(tableGroup67);
            this.table24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table24.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table24.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table24.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.table24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.StyleName = "";
            this.textBox50.Value = "Solar:";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox56.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox56.StyleName = "";
            this.textBox56.Value = "Wind:";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox58.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox58.StyleName = "";
            this.textBox58.Value = "Other:";
            // 
            // table25
            // 
            this.table25.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table25.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table25.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table25.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table25.Body.SetCellContent(0, 0, this.textBox60);
            this.table25.Body.SetCellContent(0, 1, this.textBox61);
            this.table25.Body.SetCellContent(0, 2, this.textBox62);
            tableGroup68.Name = "tableGroup7";
            tableGroup69.Name = "tableGroup9";
            tableGroup70.Name = "group26";
            this.table25.ColumnGroups.Add(tableGroup68);
            this.table25.ColumnGroups.Add(tableGroup69);
            this.table25.ColumnGroups.Add(tableGroup70);
            this.table25.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox60,
            this.textBox61,
            this.textBox62});
            this.table25.Name = "table25";
            tableGroup71.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup71.Name = "detailTableGroup3";
            this.table25.RowGroups.Add(tableGroup71);
            this.table25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table25.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table25.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.table25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox60.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox60.StyleName = "";
            this.textBox60.Value = "Solar:";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox61.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.StyleName = "";
            this.textBox61.Value = "Wind:";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox62.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.StyleName = "";
            this.textBox62.Value = "Other:";
            // 
            // table26
            // 
            this.table26.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table26.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table26.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table26.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table26.Body.SetCellContent(0, 0, this.textBox63);
            this.table26.Body.SetCellContent(0, 1, this.textBox64);
            this.table26.Body.SetCellContent(0, 2, this.textBox65);
            tableGroup72.Name = "tableGroup7";
            tableGroup73.Name = "tableGroup9";
            tableGroup74.Name = "group26";
            this.table26.ColumnGroups.Add(tableGroup72);
            this.table26.ColumnGroups.Add(tableGroup73);
            this.table26.ColumnGroups.Add(tableGroup74);
            this.table26.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox63,
            this.textBox64,
            this.textBox65});
            this.table26.Name = "table26";
            tableGroup75.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup75.Name = "detailTableGroup3";
            this.table26.RowGroups.Add(tableGroup75);
            this.table26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table26.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table26.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table26.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.table26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox63.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox63.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.StyleName = "";
            this.textBox63.Value = "Solar:";
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox64.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.StyleName = "";
            this.textBox64.Value = "Wind:";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox65.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.StyleName = "";
            this.textBox65.Value = "Other:";
            // 
            // table28
            // 
            this.table28.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.776D)));
            this.table28.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.814D)));
            this.table28.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table28.Body.SetCellContent(0, 0, this.textBox68);
            this.table28.Body.SetCellContent(0, 1, this.textBox69);
            tableGroup76.Name = "tableGroup7";
            tableGroup77.Name = "tableGroup9";
            this.table28.ColumnGroups.Add(tableGroup76);
            this.table28.ColumnGroups.Add(tableGroup77);
            this.table28.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox68,
            this.textBox69});
            this.table28.Name = "table28";
            tableGroup78.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup78.Name = "detailTableGroup3";
            this.table28.RowGroups.Add(tableGroup78);
            this.table28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table28.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table28.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table28.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.table28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.table28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.776D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox68.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox68.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.StyleName = "";
            this.textBox68.Value = "Battery:";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.814D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox69.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.StyleName = "";
            this.textBox69.Value = "Other:";
            // 
            // table41
            // 
            this.table41.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.776D)));
            this.table41.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.814D)));
            this.table41.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table41.Body.SetCellContent(0, 0, this.textBox70);
            this.table41.Body.SetCellContent(0, 1, this.textBox71);
            tableGroup79.Name = "tableGroup7";
            tableGroup80.Name = "tableGroup9";
            this.table41.ColumnGroups.Add(tableGroup79);
            this.table41.ColumnGroups.Add(tableGroup80);
            this.table41.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox70,
            this.textBox71});
            this.table41.Name = "table41";
            tableGroup81.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup81.Name = "detailTableGroup3";
            this.table41.RowGroups.Add(tableGroup81);
            this.table41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table41.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table41.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table41.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.table41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.table41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.776D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox70.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox70.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox70.StyleName = "";
            this.textBox70.Value = "Battery:";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.814D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox71.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.StyleName = "";
            this.textBox71.Value = "Other:";
            // 
            // table42
            // 
            this.table42.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.776D)));
            this.table42.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.814D)));
            this.table42.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table42.Body.SetCellContent(0, 0, this.textBox72);
            this.table42.Body.SetCellContent(0, 1, this.textBox73);
            tableGroup82.Name = "tableGroup7";
            tableGroup83.Name = "tableGroup9";
            this.table42.ColumnGroups.Add(tableGroup82);
            this.table42.ColumnGroups.Add(tableGroup83);
            this.table42.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox72,
            this.textBox73});
            this.table42.Name = "table42";
            tableGroup84.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup84.Name = "detailTableGroup3";
            this.table42.RowGroups.Add(tableGroup84);
            this.table42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table42.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table42.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table42.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.table42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.table42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.776D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox72.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.StyleName = "";
            this.textBox72.Value = "Battery:";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.814D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox73.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox73.StyleName = "";
            this.textBox73.Value = "Other:";
            // 
            // table53
            // 
            this.table53.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table53.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table53.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.413D)));
            this.table53.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table53.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table53.Body.SetCellContent(0, 0, this.textBox78);
            this.table53.Body.SetCellContent(0, 1, this.textBox79);
            this.table53.Body.SetCellContent(0, 3, this.textBox80);
            this.table53.Body.SetCellContent(0, 2, this.textBox81);
            tableGroup85.Name = "tableGroup7";
            tableGroup86.Name = "tableGroup9";
            tableGroup87.Name = "group28";
            tableGroup88.Name = "group26";
            this.table53.ColumnGroups.Add(tableGroup85);
            this.table53.ColumnGroups.Add(tableGroup86);
            this.table53.ColumnGroups.Add(tableGroup87);
            this.table53.ColumnGroups.Add(tableGroup88);
            this.table53.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox78,
            this.textBox79,
            this.textBox81,
            this.textBox80});
            this.table53.Name = "table53";
            tableGroup89.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup89.Name = "detailTableGroup3";
            this.table53.RowGroups.Add(tableGroup89);
            this.table53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.589D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table53.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table53.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table53.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox78.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.StyleName = "";
            this.textBox78.Value = "Hybrid:";
            // 
            // textBox79
            // 
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox79.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox79.StyleName = "";
            this.textBox79.Value = "Solar:";
            // 
            // textBox80
            // 
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox80.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox80.StyleName = "";
            this.textBox80.Value = "Wind:";
            // 
            // textBox81
            // 
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.413D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox81.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox81.StyleName = "";
            this.textBox81.Value = "Battery:";
            // 
            // table54
            // 
            this.table54.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table54.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table54.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.413D)));
            this.table54.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table54.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table54.Body.SetCellContent(0, 0, this.textBox82);
            this.table54.Body.SetCellContent(0, 1, this.textBox83);
            this.table54.Body.SetCellContent(0, 3, this.textBox84);
            this.table54.Body.SetCellContent(0, 2, this.textBox85);
            tableGroup90.Name = "tableGroup7";
            tableGroup91.Name = "tableGroup9";
            tableGroup92.Name = "group28";
            tableGroup93.Name = "group26";
            this.table54.ColumnGroups.Add(tableGroup90);
            this.table54.ColumnGroups.Add(tableGroup91);
            this.table54.ColumnGroups.Add(tableGroup92);
            this.table54.ColumnGroups.Add(tableGroup93);
            this.table54.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox82,
            this.textBox83,
            this.textBox85,
            this.textBox84});
            this.table54.Name = "table54";
            tableGroup94.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup94.Name = "detailTableGroup3";
            this.table54.RowGroups.Add(tableGroup94);
            this.table54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.589D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table54.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table54.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table54.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox82.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox82.StyleName = "";
            this.textBox82.Value = "Hybrid:";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox83.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox83.StyleName = "";
            this.textBox83.Value = "Solar:";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox84.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.StyleName = "";
            this.textBox84.Value = "Wind:";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.413D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox85.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox85.StyleName = "";
            this.textBox85.Value = "Battery:";
            // 
            // table55
            // 
            this.table55.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table55.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table55.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.413D)));
            this.table55.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table55.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table55.Body.SetCellContent(0, 0, this.textBox86);
            this.table55.Body.SetCellContent(0, 1, this.textBox87);
            this.table55.Body.SetCellContent(0, 3, this.textBox88);
            this.table55.Body.SetCellContent(0, 2, this.textBox89);
            tableGroup95.Name = "tableGroup7";
            tableGroup96.Name = "tableGroup9";
            tableGroup97.Name = "group28";
            tableGroup98.Name = "group26";
            this.table55.ColumnGroups.Add(tableGroup95);
            this.table55.ColumnGroups.Add(tableGroup96);
            this.table55.ColumnGroups.Add(tableGroup97);
            this.table55.ColumnGroups.Add(tableGroup98);
            this.table55.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox86,
            this.textBox87,
            this.textBox89,
            this.textBox88});
            this.table55.Name = "table55";
            tableGroup99.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup99.Name = "detailTableGroup3";
            this.table55.RowGroups.Add(tableGroup99);
            this.table55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.589D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table55.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table55.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table55.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table55.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox86.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox86.StyleName = "";
            this.textBox86.Value = "Hybrid:";
            // 
            // textBox87
            // 
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox87.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox87.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox87.StyleName = "";
            this.textBox87.Value = "Solar:";
            // 
            // textBox88
            // 
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox88.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox88.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox88.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox88.StyleName = "";
            this.textBox88.Value = "Wind:";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.413D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox89.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox89.StyleName = "";
            this.textBox89.Value = "Battery:";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox90.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox90.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox90.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox90.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox90.Style.Color = System.Drawing.Color.White;
            this.textBox90.Style.Font.Bold = true;
            this.textBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox90.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox90.StyleName = "";
            this.textBox90.Value = "Type of Generation and Storage";
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox91.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox91.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox91.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox91.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox91.Style.Color = System.Drawing.Color.White;
            this.textBox91.Style.Font.Bold = false;
            this.textBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox91.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox91.StyleName = "";
            this.textBox91.Value = "Total Generation Capacity (kW)";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox92.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox92.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox92.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox92.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox92.Style.Color = System.Drawing.Color.White;
            this.textBox92.Style.Font.Bold = false;
            this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox92.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox92.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox92.StyleName = "";
            this.textBox92.Value = "Total Inverter Capacity (kVA)";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox93.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox93.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox93.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox93.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox93.Style.Color = System.Drawing.Color.White;
            this.textBox93.Style.Font.Bold = false;
            this.textBox93.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox93.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox93.StyleName = "";
            this.textBox93.Value = "Total Storage Capacity (kWh)";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox43.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox43.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox43.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox43.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox43.StyleName = "";
            this.textBox43.Value = "";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox46.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox46.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox46.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox46.StyleName = "";
            this.textBox46.Value = "";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox48.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox48.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox48.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox48.StyleName = "";
            this.textBox48.Value = "";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox49.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox49.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox49.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox49.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox49.StyleName = "";
            this.textBox49.Value = "";
            // 
            // table29
            // 
            this.table29.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.7D)));
            this.table29.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table29.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table29.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table29.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.59D)));
            this.table29.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table29.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table29.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table29.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table29.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.637D)));
            this.table29.Body.SetCellContent(4, 0, this.textBox51);
            this.table29.Body.SetCellContent(4, 1, this.table38);
            this.table29.Body.SetCellContent(4, 2, this.table39);
            this.table29.Body.SetCellContent(4, 3, this.table40);
            this.table29.Body.SetCellContent(0, 1, this.table44);
            this.table29.Body.SetCellContent(0, 2, this.table45);
            this.table29.Body.SetCellContent(0, 3, this.table46);
            this.table29.Body.SetCellContent(0, 4, this.table47);
            this.table29.Body.SetCellContent(1, 1, this.table30);
            this.table29.Body.SetCellContent(1, 2, this.table31);
            this.table29.Body.SetCellContent(1, 3, this.table32);
            this.table29.Body.SetCellContent(1, 4, this.table33);
            this.table29.Body.SetCellContent(2, 1, this.table34);
            this.table29.Body.SetCellContent(2, 2, this.table35);
            this.table29.Body.SetCellContent(2, 3, this.table36);
            this.table29.Body.SetCellContent(2, 4, this.table37);
            this.table29.Body.SetCellContent(3, 1, this.table48);
            this.table29.Body.SetCellContent(3, 2, this.table49);
            this.table29.Body.SetCellContent(3, 3, this.table50);
            this.table29.Body.SetCellContent(3, 4, this.table51);
            this.table29.Body.SetCellContent(4, 4, this.table52);
            this.table29.Body.SetCellContent(0, 0, this.textBox54);
            this.table29.Body.SetCellContent(1, 0, this.textBox47);
            this.table29.Body.SetCellContent(3, 0, this.textBox55);
            this.table29.Body.SetCellContent(2, 0, this.textBox59);
            tableGroup192.Name = "group20";
            tableGroup193.Name = "group19";
            tableGroup194.Name = "group18";
            tableGroup195.Name = "tableGroup5";
            tableGroup196.Name = "tableGroup6";
            tableGroup191.ChildGroups.Add(tableGroup192);
            tableGroup191.ChildGroups.Add(tableGroup193);
            tableGroup191.ChildGroups.Add(tableGroup194);
            tableGroup191.ChildGroups.Add(tableGroup195);
            tableGroup191.ChildGroups.Add(tableGroup196);
            tableGroup191.Name = "tableGroup4";
            tableGroup191.ReportItem = this.textBox57;
            this.table29.ColumnGroups.Add(tableGroup191);
            this.table29.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox54,
            this.table44,
            this.table45,
            this.table46,
            this.table47,
            this.textBox47,
            this.table30,
            this.table31,
            this.table32,
            this.table33,
            this.textBox59,
            this.table34,
            this.table35,
            this.table36,
            this.table37,
            this.textBox55,
            this.table48,
            this.table49,
            this.table50,
            this.table51,
            this.textBox51,
            this.table38,
            this.table39,
            this.table40,
            this.table52,
            this.textBox57});
            this.table29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.093D), Telerik.Reporting.Drawing.Unit.Inch(2.2D));
            this.table29.Name = "table29";
            tableGroup198.Name = "group21";
            tableGroup199.Name = "group23";
            tableGroup200.Name = "group24";
            tableGroup201.Name = "group29";
            tableGroup202.Name = "group22";
            tableGroup197.ChildGroups.Add(tableGroup198);
            tableGroup197.ChildGroups.Add(tableGroup199);
            tableGroup197.ChildGroups.Add(tableGroup200);
            tableGroup197.ChildGroups.Add(tableGroup201);
            tableGroup197.ChildGroups.Add(tableGroup202);
            tableGroup197.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup197.Name = "detailTableGroup2";
            this.table29.RowGroups.Add(tableGroup197);
            this.table29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.06D), Telerik.Reporting.Drawing.Unit.Inch(1.717D));
            this.table29.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table29.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table29.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.637D));
            this.textBox51.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox51.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox51.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox51.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox51.Style.Color = System.Drawing.Color.White;
            this.textBox51.Style.Font.Bold = false;
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.9D);
            this.textBox51.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox51.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.StyleName = "";
            this.textBox51.Value = "Where Total Inverter Capacity i s more than 10kVA per Phase, the Generation and S" +
    "torage System is tested to ensure it does not export more than 10kW per Phase";
            // 
            // table38
            // 
            this.table38.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.776D)));
            this.table38.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.814D)));
            this.table38.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.537D)));
            this.table38.Body.SetCellContent(0, 0, this.checkBox70);
            this.table38.Body.SetCellContent(0, 1, this.checkBox71);
            tableGroup111.Name = "tableGroup7";
            tableGroup112.Name = "tableGroup9";
            this.table38.ColumnGroups.Add(tableGroup111);
            this.table38.ColumnGroups.Add(tableGroup112);
            this.table38.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox70,
            this.checkBox71});
            this.table38.Name = "table38";
            tableGroup113.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup113.Name = "detailTableGroup3";
            this.table38.RowGroups.Add(tableGroup113);
            this.table38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.637D));
            this.table38.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table38.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table38.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table38.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table38.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.table38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // checkBox70
            // 
            this.checkBox70.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.checkBox70.CheckedImage = ((object)(resources.GetObject("checkBox70.CheckedImage")));
            this.checkBox70.IndeterminateImage = ((object)(resources.GetObject("checkBox70.IndeterminateImage")));
            this.checkBox70.Name = "checkBox70";
            this.checkBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.776D), Telerik.Reporting.Drawing.Unit.Inch(0.537D));
            this.checkBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox70.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.checkBox70.StyleName = "";
            this.checkBox70.Text = " Yes";
            this.checkBox70.UncheckedImage = ((object)(resources.GetObject("checkBox70.UncheckedImage")));
            this.checkBox70.Value = "";
            // 
            // checkBox71
            // 
            this.checkBox71.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.checkBox71.CheckedImage = ((object)(resources.GetObject("checkBox71.CheckedImage")));
            this.checkBox71.IndeterminateImage = ((object)(resources.GetObject("checkBox71.IndeterminateImage")));
            this.checkBox71.Name = "checkBox71";
            this.checkBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.814D), Telerik.Reporting.Drawing.Unit.Inch(0.537D));
            this.checkBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox71.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.checkBox71.StyleName = "";
            this.checkBox71.Text = " No";
            this.checkBox71.UncheckedImage = ((object)(resources.GetObject("checkBox71.UncheckedImage")));
            this.checkBox71.Value = "";
            // 
            // table39
            // 
            this.table39.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.776D)));
            this.table39.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.814D)));
            this.table39.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.537D)));
            this.table39.Body.SetCellContent(0, 0, this.checkBox72);
            this.table39.Body.SetCellContent(0, 1, this.checkBox73);
            tableGroup114.Name = "tableGroup7";
            tableGroup115.Name = "tableGroup9";
            this.table39.ColumnGroups.Add(tableGroup114);
            this.table39.ColumnGroups.Add(tableGroup115);
            this.table39.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox72,
            this.checkBox73});
            this.table39.Name = "table39";
            tableGroup116.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup116.Name = "detailTableGroup3";
            this.table39.RowGroups.Add(tableGroup116);
            this.table39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.637D));
            this.table39.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table39.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table39.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table39.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table39.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.table39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // checkBox72
            // 
            this.checkBox72.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.checkBox72.CheckedImage = ((object)(resources.GetObject("checkBox72.CheckedImage")));
            this.checkBox72.IndeterminateImage = ((object)(resources.GetObject("checkBox72.IndeterminateImage")));
            this.checkBox72.Name = "checkBox72";
            this.checkBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.776D), Telerik.Reporting.Drawing.Unit.Inch(0.537D));
            this.checkBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox72.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox72.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox72.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.checkBox72.StyleName = "";
            this.checkBox72.Text = " Yes";
            this.checkBox72.UncheckedImage = ((object)(resources.GetObject("checkBox72.UncheckedImage")));
            this.checkBox72.Value = "";
            // 
            // checkBox73
            // 
            this.checkBox73.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.checkBox73.CheckedImage = ((object)(resources.GetObject("checkBox73.CheckedImage")));
            this.checkBox73.IndeterminateImage = ((object)(resources.GetObject("checkBox73.IndeterminateImage")));
            this.checkBox73.Name = "checkBox73";
            this.checkBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.814D), Telerik.Reporting.Drawing.Unit.Inch(0.537D));
            this.checkBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox73.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.checkBox73.StyleName = "";
            this.checkBox73.Text = " No";
            this.checkBox73.UncheckedImage = ((object)(resources.GetObject("checkBox73.UncheckedImage")));
            this.checkBox73.Value = "";
            // 
            // table40
            // 
            this.table40.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.776D)));
            this.table40.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.814D)));
            this.table40.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.537D)));
            this.table40.Body.SetCellContent(0, 0, this.checkBox74);
            this.table40.Body.SetCellContent(0, 1, this.checkBox75);
            tableGroup117.Name = "tableGroup7";
            tableGroup118.Name = "tableGroup9";
            this.table40.ColumnGroups.Add(tableGroup117);
            this.table40.ColumnGroups.Add(tableGroup118);
            this.table40.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox74,
            this.checkBox75});
            this.table40.Name = "table40";
            tableGroup119.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup119.Name = "detailTableGroup3";
            this.table40.RowGroups.Add(tableGroup119);
            this.table40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.637D));
            this.table40.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table40.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table40.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table40.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table40.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.table40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // checkBox74
            // 
            this.checkBox74.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.checkBox74.CheckedImage = ((object)(resources.GetObject("checkBox74.CheckedImage")));
            this.checkBox74.IndeterminateImage = ((object)(resources.GetObject("checkBox74.IndeterminateImage")));
            this.checkBox74.Name = "checkBox74";
            this.checkBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.776D), Telerik.Reporting.Drawing.Unit.Inch(0.537D));
            this.checkBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox74.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox74.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox74.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.checkBox74.StyleName = "";
            this.checkBox74.Text = " Yes";
            this.checkBox74.UncheckedImage = ((object)(resources.GetObject("checkBox74.UncheckedImage")));
            this.checkBox74.Value = "";
            // 
            // checkBox75
            // 
            this.checkBox75.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.checkBox75.CheckedImage = ((object)(resources.GetObject("checkBox75.CheckedImage")));
            this.checkBox75.IndeterminateImage = ((object)(resources.GetObject("checkBox75.IndeterminateImage")));
            this.checkBox75.Name = "checkBox75";
            this.checkBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.814D), Telerik.Reporting.Drawing.Unit.Inch(0.537D));
            this.checkBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox75.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox75.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.checkBox75.StyleName = "";
            this.checkBox75.Text = " No";
            this.checkBox75.UncheckedImage = ((object)(resources.GetObject("checkBox75.UncheckedImage")));
            this.checkBox75.Value = "";
            // 
            // table44
            // 
            this.table44.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.372D)));
            this.table44.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.457D)));
            this.table44.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table44.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.389D)));
            this.table44.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table44.Body.SetCellContent(0, 0, this.checkBox76);
            this.table44.Body.SetCellContent(0, 1, this.checkBox80);
            this.table44.Body.SetCellContent(0, 3, this.checkBox81);
            this.table44.Body.SetCellContent(0, 2, this.checkBox82);
            tableGroup120.Name = "tableGroup7";
            tableGroup121.Name = "tableGroup9";
            tableGroup122.Name = "group26";
            tableGroup123.Name = "group25";
            this.table44.ColumnGroups.Add(tableGroup120);
            this.table44.ColumnGroups.Add(tableGroup121);
            this.table44.ColumnGroups.Add(tableGroup122);
            this.table44.ColumnGroups.Add(tableGroup123);
            this.table44.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox76,
            this.checkBox80,
            this.checkBox82,
            this.checkBox81});
            this.table44.Name = "table44";
            tableGroup124.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup124.Name = "detailTableGroup3";
            this.table44.RowGroups.Add(tableGroup124);
            this.table44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.591D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.table44.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table44.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table44.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            // 
            // checkBox76
            // 
            this.checkBox76.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox76.CheckedImage = ((object)(resources.GetObject("checkBox76.CheckedImage")));
            this.checkBox76.IndeterminateImage = ((object)(resources.GetObject("checkBox76.IndeterminateImage")));
            this.checkBox76.Name = "checkBox76";
            this.checkBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.372D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox76.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox76.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox76.StyleName = "";
            this.checkBox76.Text = "Solar";
            this.checkBox76.UncheckedImage = ((object)(resources.GetObject("checkBox76.UncheckedImage")));
            this.checkBox76.Value = "";
            // 
            // checkBox80
            // 
            this.checkBox80.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox80.CheckedImage = ((object)(resources.GetObject("checkBox80.CheckedImage")));
            this.checkBox80.IndeterminateImage = ((object)(resources.GetObject("checkBox80.IndeterminateImage")));
            this.checkBox80.Name = "checkBox80";
            this.checkBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.457D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox80.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox80.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox80.StyleName = "";
            this.checkBox80.Text = " Battery";
            this.checkBox80.UncheckedImage = ((object)(resources.GetObject("checkBox80.UncheckedImage")));
            this.checkBox80.Value = "";
            // 
            // checkBox81
            // 
            this.checkBox81.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox81.CheckedImage = ((object)(resources.GetObject("checkBox81.CheckedImage")));
            this.checkBox81.IndeterminateImage = ((object)(resources.GetObject("checkBox81.IndeterminateImage")));
            this.checkBox81.Name = "checkBox81";
            this.checkBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.389D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox81.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox81.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox81.StyleName = "";
            this.checkBox81.Text = " Other";
            this.checkBox81.UncheckedImage = ((object)(resources.GetObject("checkBox81.UncheckedImage")));
            this.checkBox81.Value = "";
            // 
            // checkBox82
            // 
            this.checkBox82.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox82.CheckedImage = ((object)(resources.GetObject("checkBox82.CheckedImage")));
            this.checkBox82.IndeterminateImage = ((object)(resources.GetObject("checkBox82.IndeterminateImage")));
            this.checkBox82.Name = "checkBox82";
            this.checkBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox82.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox82.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox82.StyleName = "";
            this.checkBox82.Text = " Wind";
            this.checkBox82.UncheckedImage = ((object)(resources.GetObject("checkBox82.UncheckedImage")));
            this.checkBox82.Value = "";
            // 
            // table45
            // 
            this.table45.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.372D)));
            this.table45.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.457D)));
            this.table45.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table45.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.389D)));
            this.table45.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table45.Body.SetCellContent(0, 0, this.checkBox77);
            this.table45.Body.SetCellContent(0, 1, this.checkBox83);
            this.table45.Body.SetCellContent(0, 3, this.checkBox84);
            this.table45.Body.SetCellContent(0, 2, this.checkBox85);
            tableGroup125.Name = "tableGroup7";
            tableGroup126.Name = "tableGroup9";
            tableGroup127.Name = "group26";
            tableGroup128.Name = "group25";
            this.table45.ColumnGroups.Add(tableGroup125);
            this.table45.ColumnGroups.Add(tableGroup126);
            this.table45.ColumnGroups.Add(tableGroup127);
            this.table45.ColumnGroups.Add(tableGroup128);
            this.table45.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox77,
            this.checkBox83,
            this.checkBox85,
            this.checkBox84});
            this.table45.Name = "table45";
            tableGroup129.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup129.Name = "detailTableGroup3";
            this.table45.RowGroups.Add(tableGroup129);
            this.table45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.591D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.table45.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table45.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table45.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            // 
            // checkBox77
            // 
            this.checkBox77.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox77.CheckedImage = ((object)(resources.GetObject("checkBox77.CheckedImage")));
            this.checkBox77.IndeterminateImage = ((object)(resources.GetObject("checkBox77.IndeterminateImage")));
            this.checkBox77.Name = "checkBox77";
            this.checkBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.372D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox77.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox77.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox77.StyleName = "";
            this.checkBox77.Text = "Solar";
            this.checkBox77.UncheckedImage = ((object)(resources.GetObject("checkBox77.UncheckedImage")));
            this.checkBox77.Value = "";
            // 
            // checkBox83
            // 
            this.checkBox83.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox83.CheckedImage = ((object)(resources.GetObject("checkBox83.CheckedImage")));
            this.checkBox83.IndeterminateImage = ((object)(resources.GetObject("checkBox83.IndeterminateImage")));
            this.checkBox83.Name = "checkBox83";
            this.checkBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.457D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox83.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox83.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox83.StyleName = "";
            this.checkBox83.Text = " Battery";
            this.checkBox83.UncheckedImage = ((object)(resources.GetObject("checkBox83.UncheckedImage")));
            this.checkBox83.Value = "";
            // 
            // checkBox84
            // 
            this.checkBox84.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox84.CheckedImage = ((object)(resources.GetObject("checkBox84.CheckedImage")));
            this.checkBox84.IndeterminateImage = ((object)(resources.GetObject("checkBox84.IndeterminateImage")));
            this.checkBox84.Name = "checkBox84";
            this.checkBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.389D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox84.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox84.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox84.StyleName = "";
            this.checkBox84.Text = " Other";
            this.checkBox84.UncheckedImage = ((object)(resources.GetObject("checkBox84.UncheckedImage")));
            this.checkBox84.Value = "";
            // 
            // checkBox85
            // 
            this.checkBox85.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox85.CheckedImage = ((object)(resources.GetObject("checkBox85.CheckedImage")));
            this.checkBox85.IndeterminateImage = ((object)(resources.GetObject("checkBox85.IndeterminateImage")));
            this.checkBox85.Name = "checkBox85";
            this.checkBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox85.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox85.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox85.StyleName = "";
            this.checkBox85.Text = " Wind";
            this.checkBox85.UncheckedImage = ((object)(resources.GetObject("checkBox85.UncheckedImage")));
            this.checkBox85.Value = "";
            // 
            // table46
            // 
            this.table46.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.372D)));
            this.table46.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.457D)));
            this.table46.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table46.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.389D)));
            this.table46.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table46.Body.SetCellContent(0, 0, this.checkBox78);
            this.table46.Body.SetCellContent(0, 1, this.checkBox86);
            this.table46.Body.SetCellContent(0, 3, this.checkBox87);
            this.table46.Body.SetCellContent(0, 2, this.checkBox88);
            tableGroup130.Name = "tableGroup7";
            tableGroup131.Name = "tableGroup9";
            tableGroup132.Name = "group26";
            tableGroup133.Name = "group25";
            this.table46.ColumnGroups.Add(tableGroup130);
            this.table46.ColumnGroups.Add(tableGroup131);
            this.table46.ColumnGroups.Add(tableGroup132);
            this.table46.ColumnGroups.Add(tableGroup133);
            this.table46.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox78,
            this.checkBox86,
            this.checkBox88,
            this.checkBox87});
            this.table46.Name = "table46";
            tableGroup134.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup134.Name = "detailTableGroup3";
            this.table46.RowGroups.Add(tableGroup134);
            this.table46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.591D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.table46.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table46.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table46.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            // 
            // checkBox78
            // 
            this.checkBox78.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox78.CheckedImage = ((object)(resources.GetObject("checkBox78.CheckedImage")));
            this.checkBox78.IndeterminateImage = ((object)(resources.GetObject("checkBox78.IndeterminateImage")));
            this.checkBox78.Name = "checkBox78";
            this.checkBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.372D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox78.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox78.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox78.StyleName = "";
            this.checkBox78.Text = "Solar";
            this.checkBox78.UncheckedImage = ((object)(resources.GetObject("checkBox78.UncheckedImage")));
            this.checkBox78.Value = "";
            // 
            // checkBox86
            // 
            this.checkBox86.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox86.CheckedImage = ((object)(resources.GetObject("checkBox86.CheckedImage")));
            this.checkBox86.IndeterminateImage = ((object)(resources.GetObject("checkBox86.IndeterminateImage")));
            this.checkBox86.Name = "checkBox86";
            this.checkBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.457D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox86.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox86.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox86.StyleName = "";
            this.checkBox86.Text = " Battery";
            this.checkBox86.UncheckedImage = ((object)(resources.GetObject("checkBox86.UncheckedImage")));
            this.checkBox86.Value = "";
            // 
            // checkBox87
            // 
            this.checkBox87.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox87.CheckedImage = ((object)(resources.GetObject("checkBox87.CheckedImage")));
            this.checkBox87.IndeterminateImage = ((object)(resources.GetObject("checkBox87.IndeterminateImage")));
            this.checkBox87.Name = "checkBox87";
            this.checkBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.389D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox87.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox87.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox87.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox87.StyleName = "";
            this.checkBox87.Text = " Other";
            this.checkBox87.UncheckedImage = ((object)(resources.GetObject("checkBox87.UncheckedImage")));
            this.checkBox87.Value = "";
            // 
            // checkBox88
            // 
            this.checkBox88.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox88.CheckedImage = ((object)(resources.GetObject("checkBox88.CheckedImage")));
            this.checkBox88.IndeterminateImage = ((object)(resources.GetObject("checkBox88.IndeterminateImage")));
            this.checkBox88.Name = "checkBox88";
            this.checkBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox88.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox88.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox88.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox88.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox88.StyleName = "";
            this.checkBox88.Text = " Wind";
            this.checkBox88.UncheckedImage = ((object)(resources.GetObject("checkBox88.UncheckedImage")));
            this.checkBox88.Value = "";
            // 
            // table47
            // 
            this.table47.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.372D)));
            this.table47.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.457D)));
            this.table47.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table47.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.388D)));
            this.table47.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table47.Body.SetCellContent(0, 0, this.checkBox79);
            this.table47.Body.SetCellContent(0, 1, this.checkBox89);
            this.table47.Body.SetCellContent(0, 3, this.checkBox90);
            this.table47.Body.SetCellContent(0, 2, this.checkBox91);
            tableGroup135.Name = "tableGroup7";
            tableGroup136.Name = "tableGroup9";
            tableGroup137.Name = "group26";
            tableGroup138.Name = "group25";
            this.table47.ColumnGroups.Add(tableGroup135);
            this.table47.ColumnGroups.Add(tableGroup136);
            this.table47.ColumnGroups.Add(tableGroup137);
            this.table47.ColumnGroups.Add(tableGroup138);
            this.table47.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox79,
            this.checkBox89,
            this.checkBox91,
            this.checkBox90});
            this.table47.Name = "table47";
            tableGroup139.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup139.Name = "detailTableGroup3";
            this.table47.RowGroups.Add(tableGroup139);
            this.table47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.table47.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table47.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table47.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            // 
            // checkBox79
            // 
            this.checkBox79.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox79.CheckedImage = ((object)(resources.GetObject("checkBox79.CheckedImage")));
            this.checkBox79.IndeterminateImage = ((object)(resources.GetObject("checkBox79.IndeterminateImage")));
            this.checkBox79.Name = "checkBox79";
            this.checkBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.372D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox79.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox79.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox79.StyleName = "";
            this.checkBox79.Text = "Solar";
            this.checkBox79.UncheckedImage = ((object)(resources.GetObject("checkBox79.UncheckedImage")));
            this.checkBox79.Value = "";
            // 
            // checkBox89
            // 
            this.checkBox89.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox89.CheckedImage = ((object)(resources.GetObject("checkBox89.CheckedImage")));
            this.checkBox89.IndeterminateImage = ((object)(resources.GetObject("checkBox89.IndeterminateImage")));
            this.checkBox89.Name = "checkBox89";
            this.checkBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.457D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox89.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox89.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox89.StyleName = "";
            this.checkBox89.Text = " Battery";
            this.checkBox89.UncheckedImage = ((object)(resources.GetObject("checkBox89.UncheckedImage")));
            this.checkBox89.Value = "";
            // 
            // checkBox90
            // 
            this.checkBox90.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox90.CheckedImage = ((object)(resources.GetObject("checkBox90.CheckedImage")));
            this.checkBox90.IndeterminateImage = ((object)(resources.GetObject("checkBox90.IndeterminateImage")));
            this.checkBox90.Name = "checkBox90";
            this.checkBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.388D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox90.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox90.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox90.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox90.StyleName = "";
            this.checkBox90.Text = " Other";
            this.checkBox90.UncheckedImage = ((object)(resources.GetObject("checkBox90.UncheckedImage")));
            this.checkBox90.Value = "";
            // 
            // checkBox91
            // 
            this.checkBox91.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox91.CheckedImage = ((object)(resources.GetObject("checkBox91.CheckedImage")));
            this.checkBox91.IndeterminateImage = ((object)(resources.GetObject("checkBox91.IndeterminateImage")));
            this.checkBox91.Name = "checkBox91";
            this.checkBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.checkBox91.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox91.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox91.StyleName = "";
            this.checkBox91.Text = " Wind";
            this.checkBox91.UncheckedImage = ((object)(resources.GetObject("checkBox91.UncheckedImage")));
            this.checkBox91.Value = "";
            // 
            // table30
            // 
            this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table30.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table30.Body.SetCellContent(0, 0, this.textBox97);
            this.table30.Body.SetCellContent(0, 1, this.textBox98);
            this.table30.Body.SetCellContent(0, 2, this.textBox99);
            tableGroup140.Name = "tableGroup7";
            tableGroup141.Name = "tableGroup9";
            tableGroup142.Name = "group26";
            this.table30.ColumnGroups.Add(tableGroup140);
            this.table30.ColumnGroups.Add(tableGroup141);
            this.table30.ColumnGroups.Add(tableGroup142);
            this.table30.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox97,
            this.textBox98,
            this.textBox99});
            this.table30.Name = "table30";
            tableGroup143.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup143.Name = "detailTableGroup3";
            this.table30.RowGroups.Add(tableGroup143);
            this.table30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table30.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table30.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table30.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox97.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox97.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox97.StyleName = "";
            this.textBox97.Value = "Solar:";
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox98.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox98.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox98.StyleName = "";
            this.textBox98.Value = "Wind:";
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox99.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox99.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox99.StyleName = "";
            this.textBox99.Value = "Other:";
            // 
            // table31
            // 
            this.table31.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table31.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table31.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table31.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table31.Body.SetCellContent(0, 0, this.textBox100);
            this.table31.Body.SetCellContent(0, 1, this.textBox101);
            this.table31.Body.SetCellContent(0, 2, this.textBox102);
            tableGroup144.Name = "tableGroup7";
            tableGroup145.Name = "tableGroup9";
            tableGroup146.Name = "group26";
            this.table31.ColumnGroups.Add(tableGroup144);
            this.table31.ColumnGroups.Add(tableGroup145);
            this.table31.ColumnGroups.Add(tableGroup146);
            this.table31.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox100,
            this.textBox101,
            this.textBox102});
            this.table31.Name = "table31";
            tableGroup147.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup147.Name = "detailTableGroup3";
            this.table31.RowGroups.Add(tableGroup147);
            this.table31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table31.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table31.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table31.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox100.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox100.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox100.StyleName = "";
            this.textBox100.Value = "Solar:";
            // 
            // textBox101
            // 
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox101.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox101.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox101.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox101.StyleName = "";
            this.textBox101.Value = "Wind:";
            // 
            // textBox102
            // 
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox102.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox102.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox102.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox102.StyleName = "";
            this.textBox102.Value = "Other:";
            // 
            // table32
            // 
            this.table32.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table32.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table32.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table32.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table32.Body.SetCellContent(0, 0, this.textBox103);
            this.table32.Body.SetCellContent(0, 1, this.textBox104);
            this.table32.Body.SetCellContent(0, 2, this.textBox105);
            tableGroup148.Name = "tableGroup7";
            tableGroup149.Name = "tableGroup9";
            tableGroup150.Name = "group26";
            this.table32.ColumnGroups.Add(tableGroup148);
            this.table32.ColumnGroups.Add(tableGroup149);
            this.table32.ColumnGroups.Add(tableGroup150);
            this.table32.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox103,
            this.textBox104,
            this.textBox105});
            this.table32.Name = "table32";
            tableGroup151.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup151.Name = "detailTableGroup3";
            this.table32.RowGroups.Add(tableGroup151);
            this.table32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table32.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table32.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox103
            // 
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox103.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox103.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox103.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox103.StyleName = "";
            this.textBox103.Value = "Solar:";
            // 
            // textBox104
            // 
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox104.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox104.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox104.StyleName = "";
            this.textBox104.Value = "Wind:";
            // 
            // textBox105
            // 
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox105.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox105.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox105.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox105.StyleName = "";
            this.textBox105.Value = "Other:";
            // 
            // table33
            // 
            this.table33.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table33.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table33.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.53D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table33.Body.SetCellContent(0, 0, this.textBox106);
            this.table33.Body.SetCellContent(0, 1, this.textBox107);
            this.table33.Body.SetCellContent(0, 2, this.textBox108);
            tableGroup152.Name = "tableGroup7";
            tableGroup153.Name = "tableGroup9";
            tableGroup154.Name = "group26";
            this.table33.ColumnGroups.Add(tableGroup152);
            this.table33.ColumnGroups.Add(tableGroup153);
            this.table33.ColumnGroups.Add(tableGroup154);
            this.table33.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox106,
            this.textBox107,
            this.textBox108});
            this.table33.Name = "table33";
            tableGroup155.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup155.Name = "detailTableGroup3";
            this.table33.RowGroups.Add(tableGroup155);
            this.table33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table33.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table33.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox106
            // 
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox106.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox106.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox106.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox106.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox106.StyleName = "";
            this.textBox106.Value = "Solar:";
            // 
            // textBox107
            // 
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox107.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox107.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox107.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox107.StyleName = "";
            this.textBox107.Value = "Wind:";
            // 
            // textBox108
            // 
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.53D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox108.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox108.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox108.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox108.StyleName = "";
            this.textBox108.Value = "Other:";
            // 
            // table34
            // 
            this.table34.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.776D)));
            this.table34.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.814D)));
            this.table34.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table34.Body.SetCellContent(0, 0, this.textBox109);
            this.table34.Body.SetCellContent(0, 1, this.textBox110);
            tableGroup156.Name = "tableGroup7";
            tableGroup157.Name = "tableGroup9";
            this.table34.ColumnGroups.Add(tableGroup156);
            this.table34.ColumnGroups.Add(tableGroup157);
            this.table34.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox109,
            this.textBox110});
            this.table34.Name = "table34";
            tableGroup158.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup158.Name = "detailTableGroup3";
            this.table34.RowGroups.Add(tableGroup158);
            this.table34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table34.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table34.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.table34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox109
            // 
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.776D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox109.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox109.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox109.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox109.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox109.StyleName = "";
            this.textBox109.Value = "Battery:";
            // 
            // textBox110
            // 
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.814D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox110.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox110.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox110.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox110.StyleName = "";
            this.textBox110.Value = "Other:";
            // 
            // table35
            // 
            this.table35.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.776D)));
            this.table35.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.814D)));
            this.table35.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table35.Body.SetCellContent(0, 0, this.textBox111);
            this.table35.Body.SetCellContent(0, 1, this.textBox112);
            tableGroup159.Name = "tableGroup7";
            tableGroup160.Name = "tableGroup9";
            this.table35.ColumnGroups.Add(tableGroup159);
            this.table35.ColumnGroups.Add(tableGroup160);
            this.table35.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox111,
            this.textBox112});
            this.table35.Name = "table35";
            tableGroup161.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup161.Name = "detailTableGroup3";
            this.table35.RowGroups.Add(tableGroup161);
            this.table35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table35.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table35.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.table35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox111
            // 
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.776D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox111.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox111.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox111.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox111.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox111.StyleName = "";
            this.textBox111.Value = "Battery:";
            // 
            // textBox112
            // 
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.814D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox112.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox112.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox112.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox112.StyleName = "";
            this.textBox112.Value = "Other:";
            // 
            // table36
            // 
            this.table36.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.776D)));
            this.table36.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.814D)));
            this.table36.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table36.Body.SetCellContent(0, 0, this.textBox113);
            this.table36.Body.SetCellContent(0, 1, this.textBox114);
            tableGroup162.Name = "tableGroup7";
            tableGroup163.Name = "tableGroup9";
            this.table36.ColumnGroups.Add(tableGroup162);
            this.table36.ColumnGroups.Add(tableGroup163);
            this.table36.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox113,
            this.textBox114});
            this.table36.Name = "table36";
            tableGroup164.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup164.Name = "detailTableGroup3";
            this.table36.RowGroups.Add(tableGroup164);
            this.table36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table36.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table36.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.table36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox113
            // 
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.776D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox113.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox113.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox113.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox113.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox113.StyleName = "";
            this.textBox113.Value = "Battery:";
            // 
            // textBox114
            // 
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.814D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox114.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox114.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox114.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox114.StyleName = "";
            this.textBox114.Value = "Other:";
            // 
            // table37
            // 
            this.table37.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.776D)));
            this.table37.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.814D)));
            this.table37.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table37.Body.SetCellContent(0, 0, this.textBox115);
            this.table37.Body.SetCellContent(0, 1, this.textBox116);
            tableGroup165.Name = "tableGroup7";
            tableGroup166.Name = "tableGroup9";
            this.table37.ColumnGroups.Add(tableGroup165);
            this.table37.ColumnGroups.Add(tableGroup166);
            this.table37.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox115,
            this.textBox116});
            this.table37.Name = "table37";
            tableGroup167.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup167.Name = "detailTableGroup3";
            this.table37.RowGroups.Add(tableGroup167);
            this.table37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table37.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table37.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table37.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.table37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            // 
            // textBox115
            // 
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.776D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox115.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox115.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox115.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox115.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox115.StyleName = "";
            this.textBox115.Value = "Battery:";
            // 
            // textBox116
            // 
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.814D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox116.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox116.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox116.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox116.StyleName = "";
            this.textBox116.Value = "Other:";
            // 
            // table48
            // 
            this.table48.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table48.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table48.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.413D)));
            this.table48.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table48.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table48.Body.SetCellContent(0, 0, this.textBox121);
            this.table48.Body.SetCellContent(0, 1, this.textBox122);
            this.table48.Body.SetCellContent(0, 3, this.textBox123);
            this.table48.Body.SetCellContent(0, 2, this.textBox124);
            tableGroup168.Name = "tableGroup7";
            tableGroup169.Name = "tableGroup9";
            tableGroup170.Name = "group28";
            tableGroup171.Name = "group26";
            this.table48.ColumnGroups.Add(tableGroup168);
            this.table48.ColumnGroups.Add(tableGroup169);
            this.table48.ColumnGroups.Add(tableGroup170);
            this.table48.ColumnGroups.Add(tableGroup171);
            this.table48.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox121,
            this.textBox122,
            this.textBox124,
            this.textBox123});
            this.table48.Name = "table48";
            tableGroup172.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup172.Name = "detailTableGroup3";
            this.table48.RowGroups.Add(tableGroup172);
            this.table48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.589D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table48.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table48.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table48.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // textBox121
            // 
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox121.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox121.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox121.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox121.StyleName = "";
            this.textBox121.Value = "Hybrid:";
            // 
            // textBox122
            // 
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox122.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox122.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox122.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox122.StyleName = "";
            this.textBox122.Value = "Solar:";
            // 
            // textBox123
            // 
            this.textBox123.Name = "textBox123";
            this.textBox123.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox123.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox123.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox123.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox123.StyleName = "";
            this.textBox123.Value = "Wind:";
            // 
            // textBox124
            // 
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.413D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox124.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox124.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox124.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox124.StyleName = "";
            this.textBox124.Value = "Battery:";
            // 
            // table49
            // 
            this.table49.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table49.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table49.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.413D)));
            this.table49.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table49.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table49.Body.SetCellContent(0, 0, this.textBox117);
            this.table49.Body.SetCellContent(0, 1, this.textBox125);
            this.table49.Body.SetCellContent(0, 3, this.textBox126);
            this.table49.Body.SetCellContent(0, 2, this.textBox127);
            tableGroup173.Name = "tableGroup7";
            tableGroup174.Name = "tableGroup9";
            tableGroup175.Name = "group28";
            tableGroup176.Name = "group26";
            this.table49.ColumnGroups.Add(tableGroup173);
            this.table49.ColumnGroups.Add(tableGroup174);
            this.table49.ColumnGroups.Add(tableGroup175);
            this.table49.ColumnGroups.Add(tableGroup176);
            this.table49.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox117,
            this.textBox125,
            this.textBox127,
            this.textBox126});
            this.table49.Name = "table49";
            tableGroup177.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup177.Name = "detailTableGroup3";
            this.table49.RowGroups.Add(tableGroup177);
            this.table49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.589D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table49.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table49.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table49.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // textBox117
            // 
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox117.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox117.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox117.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox117.StyleName = "";
            this.textBox117.Value = "Hybrid:";
            // 
            // textBox125
            // 
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox125.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox125.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox125.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox125.StyleName = "";
            this.textBox125.Value = "Solar:";
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox126.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox126.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox126.StyleName = "";
            this.textBox126.Value = "Wind:";
            // 
            // textBox127
            // 
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.413D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox127.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox127.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox127.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox127.StyleName = "";
            this.textBox127.Value = "Battery:";
            // 
            // table50
            // 
            this.table50.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table50.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table50.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.413D)));
            this.table50.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table50.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table50.Body.SetCellContent(0, 0, this.textBox118);
            this.table50.Body.SetCellContent(0, 1, this.textBox128);
            this.table50.Body.SetCellContent(0, 3, this.textBox129);
            this.table50.Body.SetCellContent(0, 2, this.textBox130);
            tableGroup178.Name = "tableGroup7";
            tableGroup179.Name = "tableGroup9";
            tableGroup180.Name = "group28";
            tableGroup181.Name = "group26";
            this.table50.ColumnGroups.Add(tableGroup178);
            this.table50.ColumnGroups.Add(tableGroup179);
            this.table50.ColumnGroups.Add(tableGroup180);
            this.table50.ColumnGroups.Add(tableGroup181);
            this.table50.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox118,
            this.textBox128,
            this.textBox130,
            this.textBox129});
            this.table50.Name = "table50";
            tableGroup182.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup182.Name = "detailTableGroup3";
            this.table50.RowGroups.Add(tableGroup182);
            this.table50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.589D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table50.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table50.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table50.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // textBox118
            // 
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox118.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox118.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox118.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox118.StyleName = "";
            this.textBox118.Value = "Hybrid:";
            // 
            // textBox128
            // 
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox128.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox128.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox128.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox128.StyleName = "";
            this.textBox128.Value = "Solar:";
            // 
            // textBox129
            // 
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox129.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox129.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox129.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox129.StyleName = "";
            this.textBox129.Value = "Wind:";
            // 
            // textBox130
            // 
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.413D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox130.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox130.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox130.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox130.StyleName = "";
            this.textBox130.Value = "Battery:";
            // 
            // table51
            // 
            this.table51.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table51.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table51.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.413D)));
            this.table51.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.392D)));
            this.table51.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table51.Body.SetCellContent(0, 0, this.textBox119);
            this.table51.Body.SetCellContent(0, 1, this.textBox131);
            this.table51.Body.SetCellContent(0, 3, this.textBox132);
            this.table51.Body.SetCellContent(0, 2, this.textBox133);
            tableGroup183.Name = "tableGroup7";
            tableGroup184.Name = "tableGroup9";
            tableGroup185.Name = "group28";
            tableGroup186.Name = "group26";
            this.table51.ColumnGroups.Add(tableGroup183);
            this.table51.ColumnGroups.Add(tableGroup184);
            this.table51.ColumnGroups.Add(tableGroup185);
            this.table51.ColumnGroups.Add(tableGroup186);
            this.table51.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox119,
            this.textBox131,
            this.textBox133,
            this.textBox132});
            this.table51.Name = "table51";
            tableGroup187.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup187.Name = "detailTableGroup3";
            this.table51.RowGroups.Add(tableGroup187);
            this.table51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.589D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table51.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table51.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table51.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // textBox119
            // 
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox119.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox119.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox119.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox119.StyleName = "";
            this.textBox119.Value = "Hybrid:";
            // 
            // textBox131
            // 
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox131.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox131.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox131.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox131.StyleName = "";
            this.textBox131.Value = "Solar:";
            // 
            // textBox132
            // 
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.392D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox132.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox132.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox132.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox132.StyleName = "";
            this.textBox132.Value = "Wind:";
            // 
            // textBox133
            // 
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.413D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox133.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.textBox133.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox133.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox133.StyleName = "";
            this.textBox133.Value = "Battery:";
            // 
            // table52
            // 
            this.table52.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.776D)));
            this.table52.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.814D)));
            this.table52.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.537D)));
            this.table52.Body.SetCellContent(0, 0, this.checkBox4);
            this.table52.Body.SetCellContent(0, 1, this.checkBox5);
            tableGroup188.Name = "tableGroup7";
            tableGroup189.Name = "tableGroup9";
            this.table52.ColumnGroups.Add(tableGroup188);
            this.table52.ColumnGroups.Add(tableGroup189);
            this.table52.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox4,
            this.checkBox5});
            this.table52.Name = "table52";
            tableGroup190.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup190.Name = "detailTableGroup3";
            this.table52.RowGroups.Add(tableGroup190);
            this.table52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.59D), Telerik.Reporting.Drawing.Unit.Inch(0.637D));
            this.table52.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table52.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table52.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.table52.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table52.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.table52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            // 
            // checkBox4
            // 
            this.checkBox4.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.checkBox4.CheckedImage = ((object)(resources.GetObject("checkBox4.CheckedImage")));
            this.checkBox4.IndeterminateImage = ((object)(resources.GetObject("checkBox4.IndeterminateImage")));
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.776D), Telerik.Reporting.Drawing.Unit.Inch(0.537D));
            this.checkBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox4.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Inch(0D);
            this.checkBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.checkBox4.StyleName = "";
            this.checkBox4.Text = " Yes";
            this.checkBox4.UncheckedImage = ((object)(resources.GetObject("checkBox4.UncheckedImage")));
            this.checkBox4.Value = "";
            // 
            // checkBox5
            // 
            this.checkBox5.CheckAlign = System.Drawing.ContentAlignment.TopRight;
            this.checkBox5.CheckedImage = ((object)(resources.GetObject("checkBox5.CheckedImage")));
            this.checkBox5.IndeterminateImage = ((object)(resources.GetObject("checkBox5.IndeterminateImage")));
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.814D), Telerik.Reporting.Drawing.Unit.Inch(0.537D));
            this.checkBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.checkBox5.StyleName = "";
            this.checkBox5.Text = " No";
            this.checkBox5.UncheckedImage = ((object)(resources.GetObject("checkBox5.UncheckedImage")));
            this.checkBox5.Value = "";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox54.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox54.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox54.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox54.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox54.Style.Color = System.Drawing.Color.White;
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox54.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.StyleName = "";
            this.textBox54.Value = "Type of Generation and Storage";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox47.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox47.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox47.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox47.Style.Color = System.Drawing.Color.White;
            this.textBox47.Style.Font.Bold = false;
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox47.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.StyleName = "";
            this.textBox47.Value = "Total Generation Capacity (kW)";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox55.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox55.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox55.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox55.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox55.Style.Color = System.Drawing.Color.White;
            this.textBox55.Style.Font.Bold = false;
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox55.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.StyleName = "";
            this.textBox55.Value = "Total Inverter Capacity (kVA)";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.7D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox59.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox59.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox59.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox59.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox59.Style.Color = System.Drawing.Color.White;
            this.textBox59.Style.Font.Bold = false;
            this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox59.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.StyleName = "";
            this.textBox59.Value = "Total Storage Capacity (kWh)";
            // 
            // htmlTextBox2
            // 
            this.htmlTextBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.2D), Telerik.Reporting.Drawing.Unit.Inch(9.74D));
            this.htmlTextBox2.Name = "htmlTextBox2";
            this.htmlTextBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.203D), Telerik.Reporting.Drawing.Unit.Inch(0.21D));
            this.htmlTextBox2.Value = "<span style=\"font-size: 10px\"><sup>1</sup>A multi-mode (hybrid) inverter integrat" +
    "es solar and battery inverter technologies into one unit. </span>";
            // 
            // textBox66
            // 
            this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.2D), Telerik.Reporting.Drawing.Unit.Inch(9.95D));
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox66.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox66.Value = "Page 2 of 6";
            // 
            // panel4
            // 
            this.panel4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox8,
            this.textBox5,
            this.table1,
            this.table2,
            this.textBox1});
            this.panel4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.panel4.Name = "panel4";
            this.panel4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.193D), Telerik.Reporting.Drawing.Unit.Inch(10.15D));
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.159D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.749D), Telerik.Reporting.Drawing.Unit.Cm(0.529D));
            this.textBox2.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox2.Value = "SECTION 1: INSTALLATION DETAILS (to be completed and signed by the installer) ";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.159D), Telerik.Reporting.Drawing.Unit.Cm(0.7D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(18.859D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.5D);
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox3.Value = "This application form applies to AS4777 compliant Inverter Generation & Storage S" +
    "ystems exporting up to 10kW per ";
            // 
            // textBox4
            // 
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.159D), Telerik.Reporting.Drawing.Unit.Cm(1.2D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.749D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox4.Style.Font.Bold = true;
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.5D);
            this.textBox4.Value = "phase per premises with a total inverter capacity not exceed 30kVA.   ";
            // 
            // textBox8
            // 
            navigateToUrlAction1.Url = "https://www.unitedenergy.com.au/industry/solar-energy/";
            this.textBox8.Action = navigateToUrlAction1;
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.029D), Telerik.Reporting.Drawing.Unit.Cm(1.87D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.377D), Telerik.Reporting.Drawing.Unit.Cm(0.429D));
            this.textBox8.Style.Color = System.Drawing.SystemColors.Highlight;
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox8.Style.Font.Underline = true;
            this.textBox8.Value = "https://www.unitedenergy.com.au/industry/solar-energy/ ";
            // 
            // textBox5
            // 
            this.textBox5.Action = null;
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.159D), Telerik.Reporting.Drawing.Unit.Cm(1.87D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.869D), Telerik.Reporting.Drawing.Unit.Cm(0.429D));
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox5.Value = "For all other systems, please visit";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.758D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.221D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(9.986D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.823D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.765D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.794D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.794D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.794D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.823D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.676D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.676D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.911D)));
            this.table1.Body.SetCellContent(1, 0, this.textBox7);
            this.table1.Body.SetCellContent(2, 0, this.textBox10);
            this.table1.Body.SetCellContent(3, 0, this.textBox12);
            this.table1.Body.SetCellContent(4, 0, this.textBox14);
            this.table1.Body.SetCellContent(5, 0, this.textBox16);
            this.table1.Body.SetCellContent(6, 0, this.textBox18);
            this.table1.Body.SetCellContent(6, 1, this.textBox19);
            this.table1.Body.SetCellContent(7, 0, this.textBox20);
            this.table1.Body.SetCellContent(7, 1, this.textBox21);
            this.table1.Body.SetCellContent(8, 0, this.textBox22);
            this.table1.Body.SetCellContent(6, 2, this.textBox44);
            this.table1.Body.SetCellContent(7, 2, this.textBox45);
            this.table1.Body.SetCellContent(1, 1, this.textBox9, 1, 2);
            this.table1.Body.SetCellContent(0, 0, this.textBox6, 1, 3);
            this.table1.Body.SetCellContent(2, 1, this.textBox11, 1, 2);
            this.table1.Body.SetCellContent(3, 1, this.textBox13, 1, 2);
            this.table1.Body.SetCellContent(4, 1, this.textBox15, 1, 2);
            this.table1.Body.SetCellContent(5, 1, this.textBox17, 1, 2);
            this.table1.Body.SetCellContent(8, 1, this.textBox23, 1, 2);
            tableGroup203.Name = "tableGroup";
            tableGroup204.Name = "tableGroup2";
            tableGroup205.Name = "group17";
            this.table1.ColumnGroups.Add(tableGroup203);
            this.table1.ColumnGroups.Add(tableGroup204);
            this.table1.ColumnGroups.Add(tableGroup205);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox6,
            this.textBox7,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox44,
            this.textBox20,
            this.textBox21,
            this.textBox45,
            this.textBox22,
            this.textBox23});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.15D), Telerik.Reporting.Drawing.Unit.Cm(2.573D));
            this.table1.Name = "table1";
            tableGroup207.Name = "group1";
            tableGroup208.Name = "group";
            tableGroup209.Name = "group2";
            tableGroup210.Name = "group3";
            tableGroup211.Name = "group4";
            tableGroup212.Name = "group5";
            tableGroup213.Name = "group6";
            tableGroup214.Name = "group7";
            tableGroup215.Name = "group8";
            tableGroup206.ChildGroups.Add(tableGroup207);
            tableGroup206.ChildGroups.Add(tableGroup208);
            tableGroup206.ChildGroups.Add(tableGroup209);
            tableGroup206.ChildGroups.Add(tableGroup210);
            tableGroup206.ChildGroups.Add(tableGroup211);
            tableGroup206.ChildGroups.Add(tableGroup212);
            tableGroup206.ChildGroups.Add(tableGroup213);
            tableGroup206.ChildGroups.Add(tableGroup214);
            tableGroup206.ChildGroups.Add(tableGroup215);
            tableGroup206.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup206.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup206);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.86D), Telerik.Reporting.Drawing.Unit.Cm(8.056D));
            this.table1.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.table1.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table1.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table1.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.758D), Telerik.Reporting.Drawing.Unit.Cm(0.765D));
            this.textBox7.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox7.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox7.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox7.Style.Color = System.Drawing.Color.White;
            this.textBox7.Style.Font.Bold = true;
            this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.StyleName = "";
            this.textBox7.Value = "NMI (Refer to your electricity bill) ";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.758D), Telerik.Reporting.Drawing.Unit.Cm(0.794D));
            this.textBox10.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox10.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox10.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox10.Style.Color = System.Drawing.Color.White;
            this.textBox10.Style.Font.Bold = true;
            this.textBox10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.StyleName = "";
            this.textBox10.Value = "Meter Number ";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.758D), Telerik.Reporting.Drawing.Unit.Cm(0.794D));
            this.textBox12.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox12.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox12.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox12.Style.Color = System.Drawing.Color.White;
            this.textBox12.Style.Font.Bold = true;
            this.textBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.StyleName = "";
            this.textBox12.Value = "Generator Owner Name ";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.758D), Telerik.Reporting.Drawing.Unit.Cm(0.794D));
            this.textBox14.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox14.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox14.Style.Color = System.Drawing.Color.White;
            this.textBox14.Style.Font.Bold = true;
            this.textBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "Site Address ";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.758D), Telerik.Reporting.Drawing.Unit.Cm(0.823D));
            this.textBox16.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox16.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox16.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox16.Style.Color = System.Drawing.Color.White;
            this.textBox16.Style.Font.Bold = true;
            this.textBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "Email Address ";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.758D), Telerik.Reporting.Drawing.Unit.Cm(0.676D));
            this.textBox18.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox18.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox18.Style.Color = System.Drawing.Color.White;
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "Telephone Number ";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.221D), Telerik.Reporting.Drawing.Unit.Cm(0.676D));
            this.textBox19.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox19.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox19.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox19.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            this.textBox19.Value = "Business Hours: ";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.758D), Telerik.Reporting.Drawing.Unit.Cm(0.676D));
            this.textBox20.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox20.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox20.Style.Color = System.Drawing.Color.White;
            this.textBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.221D), Telerik.Reporting.Drawing.Unit.Cm(0.676D));
            this.textBox21.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox21.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox21.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox21.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.StyleName = "";
            this.textBox21.Value = "After Hours: ";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.758D), Telerik.Reporting.Drawing.Unit.Cm(1.911D));
            this.textBox22.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox22.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox22.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox22.Style.Color = System.Drawing.Color.White;
            this.textBox22.Style.Font.Bold = true;
            this.textBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "Mailing Address ";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.986D), Telerik.Reporting.Drawing.Unit.Cm(0.676D));
            this.textBox44.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox44.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox44.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox44.StyleName = "";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.986D), Telerik.Reporting.Drawing.Unit.Cm(0.676D));
            this.textBox45.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox45.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox45.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox45.StyleName = "";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.206D), Telerik.Reporting.Drawing.Unit.Cm(0.765D));
            this.textBox9.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox9.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox9.StyleName = "";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.964D), Telerik.Reporting.Drawing.Unit.Cm(0.823D));
            this.textBox6.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox6.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox6.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox6.Style.Color = System.Drawing.Color.White;
            this.textBox6.Style.Font.Bold = true;
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.StyleName = "";
            this.textBox6.Value = "GENERATION SYSTEM OWNER DETAILS ";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.206D), Telerik.Reporting.Drawing.Unit.Cm(0.794D));
            this.textBox11.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox11.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox11.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox11.StyleName = "";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.206D), Telerik.Reporting.Drawing.Unit.Cm(0.794D));
            this.textBox13.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox13.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox13.StyleName = "";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.206D), Telerik.Reporting.Drawing.Unit.Cm(0.794D));
            this.textBox15.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox15.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox15.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox15.StyleName = "";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.206D), Telerik.Reporting.Drawing.Unit.Cm(0.823D));
            this.textBox17.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox17.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox17.StyleName = "";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.206D), Telerik.Reporting.Drawing.Unit.Cm(1.911D));
            this.textBox23.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox23.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox23.StyleName = "";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.14D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(11.824D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.794D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.823D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.765D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.941D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.735D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.823D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.794D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.788D)));
            this.table2.Body.SetCellContent(1, 0, this.textBox25);
            this.table2.Body.SetCellContent(1, 1, this.textBox27);
            this.table2.Body.SetCellContent(2, 0, this.textBox28);
            this.table2.Body.SetCellContent(2, 1, this.textBox29);
            this.table2.Body.SetCellContent(3, 0, this.textBox30);
            this.table2.Body.SetCellContent(3, 1, this.textBox31);
            this.table2.Body.SetCellContent(4, 0, this.textBox32);
            this.table2.Body.SetCellContent(4, 1, this.textBox33);
            this.table2.Body.SetCellContent(5, 0, this.textBox34);
            this.table2.Body.SetCellContent(5, 1, this.textBox35);
            this.table2.Body.SetCellContent(6, 0, this.textBox36);
            this.table2.Body.SetCellContent(6, 1, this.textBox37);
            this.table2.Body.SetCellContent(7, 0, this.textBox38);
            this.table2.Body.SetCellContent(7, 1, this.textBox39);
            this.table2.Body.SetCellContent(0, 0, this.textBox24, 1, 2);
            tableGroup216.Name = "tableGroup1";
            tableGroup217.Name = "tableGroup3";
            this.table2.ColumnGroups.Add(tableGroup216);
            this.table2.ColumnGroups.Add(tableGroup217);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox24,
            this.textBox25,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.15D), Telerik.Reporting.Drawing.Unit.Cm(10.922D));
            this.table2.Name = "table2";
            tableGroup219.Name = "group10";
            tableGroup220.Name = "group9";
            tableGroup221.Name = "group11";
            tableGroup222.Name = "group12";
            tableGroup223.Name = "group13";
            tableGroup224.Name = "group14";
            tableGroup225.Name = "group15";
            tableGroup226.Name = "group16";
            tableGroup218.ChildGroups.Add(tableGroup219);
            tableGroup218.ChildGroups.Add(tableGroup220);
            tableGroup218.ChildGroups.Add(tableGroup221);
            tableGroup218.ChildGroups.Add(tableGroup222);
            tableGroup218.ChildGroups.Add(tableGroup223);
            tableGroup218.ChildGroups.Add(tableGroup224);
            tableGroup218.ChildGroups.Add(tableGroup225);
            tableGroup218.ChildGroups.Add(tableGroup226);
            tableGroup218.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup218.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup218);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.86D), Telerik.Reporting.Drawing.Unit.Cm(7.463D));
            this.table2.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.table2.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table2.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.14D), Telerik.Reporting.Drawing.Unit.Cm(0.823D));
            this.textBox25.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox25.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox25.Style.Color = System.Drawing.Color.White;
            this.textBox25.Style.Font.Bold = true;
            this.textBox25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.StyleName = "";
            this.textBox25.Value = "Installer Name";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.824D), Telerik.Reporting.Drawing.Unit.Cm(0.823D));
            this.textBox27.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox27.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox27.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox27.StyleName = "";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.14D), Telerik.Reporting.Drawing.Unit.Cm(0.765D));
            this.textBox28.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox28.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox28.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox28.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox28.Style.Color = System.Drawing.Color.White;
            this.textBox28.Style.Font.Bold = true;
            this.textBox28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.StyleName = "";
            this.textBox28.Value = "Company ";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.824D), Telerik.Reporting.Drawing.Unit.Cm(0.765D));
            this.textBox29.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox29.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox29.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox29.StyleName = "";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.14D), Telerik.Reporting.Drawing.Unit.Cm(1.941D));
            this.textBox30.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox30.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox30.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox30.Style.Color = System.Drawing.Color.White;
            this.textBox30.Style.Font.Bold = true;
            this.textBox30.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.StyleName = "";
            this.textBox30.Value = "Company Address ";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.824D), Telerik.Reporting.Drawing.Unit.Cm(1.941D));
            this.textBox31.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox31.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox31.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox31.StyleName = "";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.14D), Telerik.Reporting.Drawing.Unit.Cm(0.735D));
            this.textBox32.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox32.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox32.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox32.Style.Color = System.Drawing.Color.White;
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.StyleName = "";
            this.textBox32.Value = "License No (REC No) ";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.824D), Telerik.Reporting.Drawing.Unit.Cm(0.735D));
            this.textBox33.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox33.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox33.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox33.StyleName = "";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.14D), Telerik.Reporting.Drawing.Unit.Cm(0.823D));
            this.textBox34.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox34.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox34.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox34.Style.Color = System.Drawing.Color.White;
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.StyleName = "";
            this.textBox34.Value = "CEC Accreditation No ";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.824D), Telerik.Reporting.Drawing.Unit.Cm(0.823D));
            this.textBox35.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox35.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox35.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox35.StyleName = "";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.14D), Telerik.Reporting.Drawing.Unit.Cm(0.794D));
            this.textBox36.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox36.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox36.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox36.Style.Color = System.Drawing.Color.White;
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.StyleName = "";
            this.textBox36.Value = "Email Address ";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.824D), Telerik.Reporting.Drawing.Unit.Cm(0.794D));
            this.textBox37.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox37.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox37.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox37.StyleName = "";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.14D), Telerik.Reporting.Drawing.Unit.Cm(0.788D));
            this.textBox38.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox38.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox38.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox38.Style.Color = System.Drawing.Color.White;
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            this.textBox38.Value = "Mobile Number ";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.824D), Telerik.Reporting.Drawing.Unit.Cm(0.788D));
            this.textBox39.Style.BackgroundColor = System.Drawing.Color.White;
            this.textBox39.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox39.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox39.StyleName = "";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.964D), Telerik.Reporting.Drawing.Unit.Cm(0.794D));
            this.textBox24.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox24.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox24.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox24.Style.Color = System.Drawing.Color.White;
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "INSTALLER DETAILS ";
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.2D), Telerik.Reporting.Drawing.Unit.Inch(9.86D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox1.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox1.Value = "Page 1 of 6";
            // 
            // page6
            // 
            this.page6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel1,
            this.panel2,
            this.textBox343});
            this.page6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(50.81D));
            this.page6.Name = "page6";
            this.page6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.19D), Telerik.Reporting.Drawing.Unit.Inch(10.15D));
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox317,
            this.textBox318,
            this.table61,
            this.table62,
            this.table63});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.095D), Telerik.Reporting.Drawing.Unit.Inch(4.2D));
            // 
            // textBox317
            // 
            this.textBox317.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.998D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.textBox317.Name = "textBox317";
            this.textBox317.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.204D), Telerik.Reporting.Drawing.Unit.Cm(0.508D));
            this.textBox317.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox317.Style.Font.Bold = true;
            this.textBox317.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox317.Value = "SECTION 2: INSTALLATION COMPLIANCE (to be completed and signed by the Registered " +
    "Electrical Contractor)";
            // 
            // textBox318
            // 
            this.textBox318.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.393D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
            this.textBox318.Name = "textBox318";
            this.textBox318.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.488D), Telerik.Reporting.Drawing.Unit.Inch(0.6D));
            this.textBox318.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox318.Style.Font.Bold = true;
            this.textBox318.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox318.Value = resources.GetString("textBox318.Value");
            // 
            // table61
            // 
            this.table61.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.895D)));
            this.table61.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.615D)));
            this.table61.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.46D)));
            this.table61.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.481D)));
            this.table61.Body.SetCellContent(0, 0, this.htmlTextBox7);
            this.table61.Body.SetCellContent(1, 0, this.htmlTextBox9);
            this.table61.Body.SetCellContent(2, 0, this.htmlTextBox13);
            tableGroup227.Name = "tableGroup14";
            tableGroup227.ReportItem = this.htmlTextBox5;
            this.table61.ColumnGroups.Add(tableGroup227);
            this.table61.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox7,
            this.htmlTextBox9,
            this.htmlTextBox13,
            this.htmlTextBox5});
            this.table61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.207D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.table61.Name = "table61";
            tableGroup229.Name = "group74";
            tableGroup230.Name = "group75";
            tableGroup231.Name = "group76";
            tableGroup228.ChildGroups.Add(tableGroup229);
            tableGroup228.ChildGroups.Add(tableGroup230);
            tableGroup228.ChildGroups.Add(tableGroup231);
            tableGroup228.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup228.Name = "detailTableGroup6";
            this.table61.RowGroups.Add(tableGroup228);
            this.table61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.895D), Telerik.Reporting.Drawing.Unit.Inch(2.441D));
            // 
            // htmlTextBox7
            // 
            this.htmlTextBox7.Name = "htmlTextBox7";
            this.htmlTextBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.895D), Telerik.Reporting.Drawing.Unit.Inch(0.615D));
            this.htmlTextBox7.StyleName = "";
            this.htmlTextBox7.Value = resources.GetString("htmlTextBox7.Value");
            // 
            // htmlTextBox9
            // 
            this.htmlTextBox9.Name = "htmlTextBox9";
            this.htmlTextBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.895D), Telerik.Reporting.Drawing.Unit.Inch(0.46D));
            this.htmlTextBox9.StyleName = "";
            this.htmlTextBox9.Value = resources.GetString("htmlTextBox9.Value");
            // 
            // htmlTextBox13
            // 
            this.htmlTextBox13.Name = "htmlTextBox13";
            this.htmlTextBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.895D), Telerik.Reporting.Drawing.Unit.Inch(0.481D));
            this.htmlTextBox13.StyleName = "";
            this.htmlTextBox13.Value = resources.GetString("htmlTextBox13.Value");
            // 
            // table62
            // 
            this.table62.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.801D)));
            this.table62.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.355D)));
            this.table62.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.335D)));
            this.table62.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.596D)));
            this.table62.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.637D)));
            this.table62.Body.SetCellContent(0, 0, this.htmlTextBox6);
            this.table62.Body.SetCellContent(1, 0, this.htmlTextBox8);
            this.table62.Body.SetCellContent(2, 0, this.htmlTextBox10);
            this.table62.Body.SetCellContent(3, 0, this.htmlTextBox11);
            tableGroup232.Name = "tableGroup14";
            tableGroup232.ReportItem = this.htmlTextBox12;
            this.table62.ColumnGroups.Add(tableGroup232);
            this.table62.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox6,
            this.htmlTextBox8,
            this.htmlTextBox10,
            this.htmlTextBox11,
            this.htmlTextBox12});
            this.table62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(4.1D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.table62.Name = "table62";
            tableGroup234.Name = "group74";
            tableGroup235.Name = "group75";
            tableGroup236.Name = "group76";
            tableGroup237.Name = "group77";
            tableGroup233.ChildGroups.Add(tableGroup234);
            tableGroup233.ChildGroups.Add(tableGroup235);
            tableGroup233.ChildGroups.Add(tableGroup236);
            tableGroup233.ChildGroups.Add(tableGroup237);
            tableGroup233.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup233.Name = "detailTableGroup6";
            this.table62.RowGroups.Add(tableGroup233);
            this.table62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.801D), Telerik.Reporting.Drawing.Unit.Inch(2.423D));
            // 
            // htmlTextBox6
            // 
            this.htmlTextBox6.Name = "htmlTextBox6";
            this.htmlTextBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.801D), Telerik.Reporting.Drawing.Unit.Inch(0.355D));
            this.htmlTextBox6.StyleName = "";
            this.htmlTextBox6.Value = resources.GetString("htmlTextBox6.Value");
            // 
            // htmlTextBox8
            // 
            this.htmlTextBox8.Name = "htmlTextBox8";
            this.htmlTextBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.801D), Telerik.Reporting.Drawing.Unit.Inch(0.335D));
            this.htmlTextBox8.StyleName = "";
            this.htmlTextBox8.Value = resources.GetString("htmlTextBox8.Value");
            // 
            // htmlTextBox10
            // 
            this.htmlTextBox10.Name = "htmlTextBox10";
            this.htmlTextBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.801D), Telerik.Reporting.Drawing.Unit.Inch(0.596D));
            this.htmlTextBox10.StyleName = "";
            this.htmlTextBox10.Value = resources.GetString("htmlTextBox10.Value");
            // 
            // htmlTextBox11
            // 
            this.htmlTextBox11.Name = "htmlTextBox11";
            this.htmlTextBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.801D), Telerik.Reporting.Drawing.Unit.Inch(0.637D));
            this.htmlTextBox11.StyleName = "";
            this.htmlTextBox11.Value = resources.GetString("htmlTextBox11.Value");
            // 
            // table63
            // 
            this.table63.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.771D)));
            this.table63.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.417D)));
            this.table63.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.052D)));
            this.table63.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.635D)));
            this.table63.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table63.Body.SetCellContent(0, 0, this.textBox320);
            this.table63.Body.SetCellContent(0, 1, this.textBox322);
            this.table63.Body.SetCellContent(0, 2, this.textBox324);
            this.table63.Body.SetCellContent(0, 3, this.textBox326);
            tableGroup238.Name = "tableGroup15";
            tableGroup239.Name = "tableGroup16";
            tableGroup240.Name = "tableGroup17";
            tableGroup241.Name = "group78";
            this.table63.ColumnGroups.Add(tableGroup238);
            this.table63.ColumnGroups.Add(tableGroup239);
            this.table63.ColumnGroups.Add(tableGroup240);
            this.table63.ColumnGroups.Add(tableGroup241);
            this.table63.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox320,
            this.textBox322,
            this.textBox324,
            this.textBox326});
            this.table63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.393D), Telerik.Reporting.Drawing.Unit.Inch(3.8D));
            this.table63.Name = "table63";
            tableGroup242.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup242.Name = "detailTableGroup7";
            this.table63.RowGroups.Add(tableGroup242);
            this.table63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.875D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            // 
            // textBox320
            // 
            this.textBox320.Name = "textBox320";
            this.textBox320.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.771D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox320.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox320.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox320.Value = "INSTALLER SIGNATURE";
            // 
            // textBox322
            // 
            this.textBox322.Name = "textBox322";
            this.textBox322.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.417D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox322.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.textBox322.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox322.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox322.Style.Font.Underline = false;
            // 
            // textBox324
            // 
            this.textBox324.Name = "textBox324";
            this.textBox324.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.052D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox324.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox324.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox324.Value = "DATE";
            // 
            // textBox326
            // 
            this.textBox326.Name = "textBox326";
            this.textBox326.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.635D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox326.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.textBox326.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox326.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox326.Style.Font.Underline = true;
            this.textBox326.StyleName = "";
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox321,
            this.textBox319,
            this.table64,
            this.textBox331,
            this.table67});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(4.2D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.19D), Telerik.Reporting.Drawing.Unit.Inch(5.19D));
            // 
            // textBox321
            // 
            this.textBox321.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.998D), Telerik.Reporting.Drawing.Unit.Cm(0.254D));
            this.textBox321.Name = "textBox321";
            this.textBox321.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.804D), Telerik.Reporting.Drawing.Unit.Cm(0.508D));
            this.textBox321.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox321.Style.Font.Bold = true;
            this.textBox321.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox321.Value = "SECTION 3: CUSTOMER ACKNOWLEDGEMENT (to be completed and signed by the generation" +
    " system owner)";
            // 
            // textBox319
            // 
            this.textBox319.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.393D), Telerik.Reporting.Drawing.Unit.Inch(0.4D));
            this.textBox319.Name = "textBox319";
            this.textBox319.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.488D), Telerik.Reporting.Drawing.Unit.Inch(0.4D));
            this.textBox319.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox319.Style.Font.Bold = true;
            this.textBox319.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox319.Value = "By signing this form, you acknowledge and represent that you have read, understoo" +
    "d and agree to comply with the UE Basic Micro Embedded Generation Connection Mod" +
    "el Standing Offer (MSO), and that you:";
            // 
            // table64
            // 
            this.table64.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(7.617D)));
            this.table64.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.426D)));
            this.table64.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.259D)));
            this.table64.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.382D)));
            this.table64.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.215D)));
            this.table64.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.253D)));
            this.table64.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.215D)));
            this.table64.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.304D)));
            this.table64.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.215D)));
            this.table64.Body.SetCellContent(0, 0, this.htmlTextBox14);
            this.table64.Body.SetCellContent(1, 0, this.htmlTextBox15);
            this.table64.Body.SetCellContent(2, 0, this.htmlTextBox16);
            this.table64.Body.SetCellContent(3, 0, this.htmlTextBox18);
            this.table64.Body.SetCellContent(5, 0, this.textBox332);
            this.table64.Body.SetCellContent(4, 0, this.table66);
            this.table64.Body.SetCellContent(6, 0, this.htmlTextBox19);
            this.table64.Body.SetCellContent(7, 0, this.table65);
            tableGroup257.Name = "tableGroup14";
            tableGroup257.ReportItem = this.htmlTextBox17;
            this.table64.ColumnGroups.Add(tableGroup257);
            this.table64.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox14,
            this.htmlTextBox15,
            this.htmlTextBox16,
            this.htmlTextBox18,
            this.table66,
            this.textBox332,
            this.htmlTextBox19,
            this.table65,
            this.htmlTextBox17});
            this.table64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.293D), Telerik.Reporting.Drawing.Unit.Inch(0.9D));
            this.table64.Name = "table64";
            tableGroup259.Name = "group74";
            tableGroup260.Name = "group75";
            tableGroup261.Name = "group76";
            tableGroup262.Name = "group79";
            tableGroup263.Name = "group82";
            tableGroup264.Name = "group83";
            tableGroup265.Name = "group84";
            tableGroup266.Name = "group85";
            tableGroup258.ChildGroups.Add(tableGroup259);
            tableGroup258.ChildGroups.Add(tableGroup260);
            tableGroup258.ChildGroups.Add(tableGroup261);
            tableGroup258.ChildGroups.Add(tableGroup262);
            tableGroup258.ChildGroups.Add(tableGroup263);
            tableGroup258.ChildGroups.Add(tableGroup264);
            tableGroup258.ChildGroups.Add(tableGroup265);
            tableGroup258.ChildGroups.Add(tableGroup266);
            tableGroup258.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup258.Name = "detailTableGroup6";
            this.table64.RowGroups.Add(tableGroup258);
            this.table64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.617D), Telerik.Reporting.Drawing.Unit.Inch(2.638D));
            // 
            // htmlTextBox14
            // 
            this.htmlTextBox14.Name = "htmlTextBox14";
            this.htmlTextBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.617D), Telerik.Reporting.Drawing.Unit.Inch(0.426D));
            this.htmlTextBox14.StyleName = "";
            this.htmlTextBox14.Value = resources.GetString("htmlTextBox14.Value");
            // 
            // htmlTextBox15
            // 
            this.htmlTextBox15.Name = "htmlTextBox15";
            this.htmlTextBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.617D), Telerik.Reporting.Drawing.Unit.Inch(0.259D));
            this.htmlTextBox15.StyleName = "";
            this.htmlTextBox15.Value = resources.GetString("htmlTextBox15.Value");
            // 
            // htmlTextBox16
            // 
            this.htmlTextBox16.Name = "htmlTextBox16";
            this.htmlTextBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.617D), Telerik.Reporting.Drawing.Unit.Inch(0.382D));
            this.htmlTextBox16.StyleName = "";
            this.htmlTextBox16.Value = resources.GetString("htmlTextBox16.Value");
            // 
            // htmlTextBox18
            // 
            this.htmlTextBox18.Name = "htmlTextBox18";
            this.htmlTextBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.617D), Telerik.Reporting.Drawing.Unit.Inch(0.215D));
            this.htmlTextBox18.StyleName = "";
            this.htmlTextBox18.Value = "<ul><li><div style=\"text-align: justify\"><span style=\"font-size: 8.5pt; color: #0" +
    "00080\">Do not require a written offer.</span></div></li></ul>";
            // 
            // textBox332
            // 
            this.textBox332.Name = "textBox332";
            this.textBox332.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.617D), Telerik.Reporting.Drawing.Unit.Inch(0.215D));
            this.textBox332.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox332.Style.Font.Bold = true;
            this.textBox332.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox332.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox332.StyleName = "";
            this.textBox332.Value = "OR";
            // 
            // table66
            // 
            this.table66.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.275D)));
            this.table66.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.426D)));
            this.table66.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.607D)));
            this.table66.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.365D)));
            this.table66.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.632D)));
            this.table66.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.174D)));
            this.table66.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.253D)));
            this.table66.Body.SetCellContent(0, 2, this.textBox334);
            this.table66.Body.SetCellContent(0, 3, this.textBox335);
            this.table66.Body.SetCellContent(0, 4, this.textBox336);
            this.table66.Body.SetCellContent(0, 5, this.textBox337);
            this.table66.Body.SetCellContent(0, 1, this.textBox338);
            this.table66.Body.SetCellContent(0, 0, this.textBox339);
            tableGroup243.Name = "group81";
            tableGroup244.Name = "group80";
            tableGroup245.Name = "tableGroup15";
            tableGroup246.Name = "tableGroup16";
            tableGroup247.Name = "tableGroup17";
            tableGroup248.Name = "group78";
            this.table66.ColumnGroups.Add(tableGroup243);
            this.table66.ColumnGroups.Add(tableGroup244);
            this.table66.ColumnGroups.Add(tableGroup245);
            this.table66.ColumnGroups.Add(tableGroup246);
            this.table66.ColumnGroups.Add(tableGroup247);
            this.table66.ColumnGroups.Add(tableGroup248);
            this.table66.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox339,
            this.textBox338,
            this.textBox334,
            this.textBox335,
            this.textBox336,
            this.textBox337});
            this.table66.Name = "table66";
            tableGroup249.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup249.Name = "detailTableGroup7";
            this.table66.RowGroups.Add(tableGroup249);
            this.table66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.618D), Telerik.Reporting.Drawing.Unit.Inch(0.253D));
            this.table66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.table66.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // textBox334
            // 
            this.textBox334.Name = "textBox334";
            this.textBox334.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.607D), Telerik.Reporting.Drawing.Unit.Inch(0.253D));
            this.textBox334.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox334.Value = "CUSTOMER SIGNATURE";
            // 
            // textBox335
            // 
            this.textBox335.Name = "textBox335";
            this.textBox335.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.365D), Telerik.Reporting.Drawing.Unit.Inch(0.253D));
            this.textBox335.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.textBox335.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox335.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox335.Style.Font.Underline = false;
            // 
            // textBox336
            // 
            this.textBox336.Name = "textBox336";
            this.textBox336.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.632D), Telerik.Reporting.Drawing.Unit.Inch(0.253D));
            this.textBox336.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox336.Value = "DATE";
            // 
            // textBox337
            // 
            this.textBox337.Name = "textBox337";
            this.textBox337.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.174D), Telerik.Reporting.Drawing.Unit.Inch(0.253D));
            this.textBox337.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.textBox337.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox337.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox337.Style.Font.Underline = true;
            this.textBox337.StyleName = "";
            // 
            // textBox338
            // 
            this.textBox338.Name = "textBox338";
            this.textBox338.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.426D), Telerik.Reporting.Drawing.Unit.Inch(0.253D));
            this.textBox338.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.textBox338.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox338.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox338.StyleName = "";
            // 
            // textBox339
            // 
            this.textBox339.Name = "textBox339";
            this.textBox339.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.275D), Telerik.Reporting.Drawing.Unit.Inch(0.253D));
            this.textBox339.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox339.StyleName = "";
            this.textBox339.Value = "CUSTOMER NAME";
            // 
            // htmlTextBox19
            // 
            this.htmlTextBox19.Name = "htmlTextBox19";
            this.htmlTextBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.617D), Telerik.Reporting.Drawing.Unit.Inch(0.304D));
            this.htmlTextBox19.StyleName = "";
            this.htmlTextBox19.Value = "<ul><li><div style=\"text-align: justify\"><span style=\"font-size: 8.5pt; color: #0" +
    "00080\">Require a written offer.</span></div></li></ul>";
            // 
            // table65
            // 
            this.table65.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.278D)));
            this.table65.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.41D)));
            this.table65.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.611D)));
            this.table65.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.369D)));
            this.table65.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.634D)));
            this.table65.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.177D)));
            this.table65.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.215D)));
            this.table65.Body.SetCellContent(0, 2, this.textBox323);
            this.table65.Body.SetCellContent(0, 3, this.textBox325);
            this.table65.Body.SetCellContent(0, 4, this.textBox327);
            this.table65.Body.SetCellContent(0, 5, this.textBox328);
            this.table65.Body.SetCellContent(0, 1, this.textBox329);
            this.table65.Body.SetCellContent(0, 0, this.textBox330);
            tableGroup250.Name = "group81";
            tableGroup251.Name = "group80";
            tableGroup252.Name = "tableGroup15";
            tableGroup253.Name = "tableGroup16";
            tableGroup254.Name = "tableGroup17";
            tableGroup255.Name = "group78";
            this.table65.ColumnGroups.Add(tableGroup250);
            this.table65.ColumnGroups.Add(tableGroup251);
            this.table65.ColumnGroups.Add(tableGroup252);
            this.table65.ColumnGroups.Add(tableGroup253);
            this.table65.ColumnGroups.Add(tableGroup254);
            this.table65.ColumnGroups.Add(tableGroup255);
            this.table65.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox330,
            this.textBox329,
            this.textBox323,
            this.textBox325,
            this.textBox327,
            this.textBox328});
            this.table65.Name = "table65";
            tableGroup256.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup256.Name = "detailTableGroup7";
            this.table65.RowGroups.Add(tableGroup256);
            this.table65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.618D), Telerik.Reporting.Drawing.Unit.Inch(0.215D));
            this.table65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.table65.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // textBox323
            // 
            this.textBox323.Name = "textBox323";
            this.textBox323.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.611D), Telerik.Reporting.Drawing.Unit.Inch(0.215D));
            this.textBox323.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox323.Value = "CUSTOMER SIGNATURE";
            // 
            // textBox325
            // 
            this.textBox325.Name = "textBox325";
            this.textBox325.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.369D), Telerik.Reporting.Drawing.Unit.Inch(0.215D));
            this.textBox325.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.textBox325.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox325.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox325.Style.Font.Underline = false;
            // 
            // textBox327
            // 
            this.textBox327.Name = "textBox327";
            this.textBox327.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.634D), Telerik.Reporting.Drawing.Unit.Inch(0.215D));
            this.textBox327.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox327.Value = "DATE";
            // 
            // textBox328
            // 
            this.textBox328.Name = "textBox328";
            this.textBox328.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.177D), Telerik.Reporting.Drawing.Unit.Inch(0.215D));
            this.textBox328.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.textBox328.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox328.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox328.Style.Font.Underline = true;
            this.textBox328.StyleName = "";
            // 
            // textBox329
            // 
            this.textBox329.Name = "textBox329";
            this.textBox329.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.41D), Telerik.Reporting.Drawing.Unit.Inch(0.215D));
            this.textBox329.Style.BorderColor.Bottom = System.Drawing.Color.Gray;
            this.textBox329.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dashed;
            this.textBox329.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox329.StyleName = "";
            // 
            // textBox330
            // 
            this.textBox330.Name = "textBox330";
            this.textBox330.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.278D), Telerik.Reporting.Drawing.Unit.Inch(0.215D));
            this.textBox330.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox330.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox330.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox330.StyleName = "";
            this.textBox330.Value = "CUSTOMER NAME";
            // 
            // textBox331
            // 
            this.textBox331.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.456D), Telerik.Reporting.Drawing.Unit.Inch(3.8D));
            this.textBox331.Name = "textBox331";
            this.textBox331.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.497D), Telerik.Reporting.Drawing.Unit.Inch(0.4D));
            this.textBox331.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox331.Style.Font.Bold = true;
            this.textBox331.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10.5D);
            this.textBox331.Value = "Please return the completed and signed form (keeping a copy for your reference) t" +
    "o your Retailer, or to United Energy via:";
            // 
            // table67
            // 
            this.table67.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.03D)));
            this.table67.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.644D)));
            this.table67.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table67.Body.SetCellContent(0, 0, this.textBox340);
            this.table67.Body.SetCellContent(0, 1, this.textBox342);
            tableGroup267.Name = "tableGroup18";
            tableGroup267.ReportItem = this.textBox333;
            tableGroup268.Name = "tableGroup19";
            tableGroup268.ReportItem = this.textBox341;
            this.table67.ColumnGroups.Add(tableGroup267);
            this.table67.ColumnGroups.Add(tableGroup268);
            this.table67.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox340,
            this.textBox342,
            this.textBox333,
            this.textBox341});
            this.table67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.493D), Telerik.Reporting.Drawing.Unit.Inch(4.4D));
            this.table67.Name = "table67";
            tableGroup269.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup269.Name = "detailTableGroup8";
            this.table67.RowGroups.Add(tableGroup269);
            this.table67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.674D), Telerik.Reporting.Drawing.Unit.Inch(0.48D));
            this.table67.Style.Color = System.Drawing.Color.MidnightBlue;
            // 
            // textBox340
            // 
            this.textBox340.Name = "textBox340";
            this.textBox340.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.03D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox340.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox340.Value = "E-mail:";
            // 
            // textBox342
            // 
            this.textBox342.Name = "textBox342";
            this.textBox342.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.644D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox342.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox342.Value = "uesolarconnections@ue.com.au";
            // 
            // textBox343
            // 
            this.textBox343.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.2D), Telerik.Reporting.Drawing.Unit.Inch(9.89D));
            this.textBox343.Name = "textBox343";
            this.textBox343.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox343.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox343.Value = "Page 6 of 6";
            // 
            // page5
            // 
            this.page5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox314,
            this.textBox154,
            this.table60,
            this.htmlTextBox3,
            this.htmlTextBox4,
            this.textBox153,
            this.textBox315,
            this.textBox316});
            this.page5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.062D), Telerik.Reporting.Drawing.Unit.Inch(40.61D));
            this.page5.Name = "page5";
            this.page5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.142D), Telerik.Reporting.Drawing.Unit.Inch(10.15D));
            // 
            // textBox314
            // 
            this.textBox314.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.192D), Telerik.Reporting.Drawing.Unit.Inch(5.753D));
            this.textBox314.Name = "textBox314";
            this.textBox314.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.32D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox314.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox314.Style.Font.Bold = true;
            this.textBox314.Value = "TYPES OF SOLAR PV PANELS";
            // 
            // textBox154
            // 
            this.textBox154.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.192D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
            this.textBox154.Name = "textBox154";
            this.textBox154.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.95D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox154.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox154.Style.Font.Bold = true;
            this.textBox154.Value = "The Installer must fill this page if more than one inverter of the same Make and " +
    "Model is installed at a premises.";
            // 
            // table60
            // 
            this.table60.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.727D)));
            this.table60.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(6.05D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table60.Body.SetCellContent(0, 0, this.textBox157);
            this.table60.Body.SetCellContent(0, 1, this.textBox160);
            this.table60.Body.SetCellContent(13, 0, this.textBox158);
            this.table60.Body.SetCellContent(13, 1, this.textBox162);
            this.table60.Body.SetCellContent(12, 0, this.textBox221);
            this.table60.Body.SetCellContent(12, 1, this.textBox222);
            this.table60.Body.SetCellContent(11, 0, this.textBox223);
            this.table60.Body.SetCellContent(11, 1, this.textBox224);
            this.table60.Body.SetCellContent(10, 0, this.textBox225);
            this.table60.Body.SetCellContent(10, 1, this.textBox226);
            this.table60.Body.SetCellContent(9, 0, this.textBox227);
            this.table60.Body.SetCellContent(9, 1, this.textBox228);
            this.table60.Body.SetCellContent(8, 0, this.textBox229);
            this.table60.Body.SetCellContent(8, 1, this.textBox230);
            this.table60.Body.SetCellContent(7, 0, this.textBox231);
            this.table60.Body.SetCellContent(7, 1, this.textBox232);
            this.table60.Body.SetCellContent(6, 0, this.textBox233);
            this.table60.Body.SetCellContent(6, 1, this.textBox234);
            this.table60.Body.SetCellContent(3, 0, this.textBox235);
            this.table60.Body.SetCellContent(3, 1, this.textBox293);
            this.table60.Body.SetCellContent(5, 0, this.textBox294);
            this.table60.Body.SetCellContent(5, 1, this.textBox295);
            this.table60.Body.SetCellContent(4, 0, this.textBox296);
            this.table60.Body.SetCellContent(4, 1, this.textBox297);
            this.table60.Body.SetCellContent(1, 0, this.textBox298);
            this.table60.Body.SetCellContent(1, 1, this.textBox299);
            this.table60.Body.SetCellContent(2, 0, this.textBox300);
            this.table60.Body.SetCellContent(2, 1, this.textBox301);
            this.table60.Body.SetCellContent(14, 0, this.textBox302);
            this.table60.Body.SetCellContent(14, 1, this.textBox303);
            this.table60.Body.SetCellContent(15, 0, this.textBox304);
            this.table60.Body.SetCellContent(15, 1, this.textBox305);
            this.table60.Body.SetCellContent(16, 0, this.textBox306);
            this.table60.Body.SetCellContent(16, 1, this.textBox307);
            this.table60.Body.SetCellContent(17, 0, this.textBox308);
            this.table60.Body.SetCellContent(17, 1, this.textBox309);
            this.table60.Body.SetCellContent(18, 0, this.textBox310);
            this.table60.Body.SetCellContent(18, 1, this.textBox311);
            this.table60.Body.SetCellContent(19, 0, this.textBox312);
            this.table60.Body.SetCellContent(19, 1, this.textBox313);
            tableGroup270.Name = "tableGroup12";
            tableGroup270.ReportItem = this.textBox161;
            tableGroup271.Name = "tableGroup13";
            tableGroup271.ReportItem = this.textBox156;
            this.table60.ColumnGroups.Add(tableGroup270);
            this.table60.ColumnGroups.Add(tableGroup271);
            this.table60.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox157,
            this.textBox160,
            this.textBox298,
            this.textBox299,
            this.textBox300,
            this.textBox301,
            this.textBox235,
            this.textBox293,
            this.textBox296,
            this.textBox297,
            this.textBox294,
            this.textBox295,
            this.textBox233,
            this.textBox234,
            this.textBox231,
            this.textBox232,
            this.textBox229,
            this.textBox230,
            this.textBox227,
            this.textBox228,
            this.textBox225,
            this.textBox226,
            this.textBox223,
            this.textBox224,
            this.textBox221,
            this.textBox222,
            this.textBox158,
            this.textBox162,
            this.textBox302,
            this.textBox303,
            this.textBox304,
            this.textBox305,
            this.textBox306,
            this.textBox307,
            this.textBox308,
            this.textBox309,
            this.textBox310,
            this.textBox311,
            this.textBox312,
            this.textBox313,
            this.textBox161,
            this.textBox156});
            this.table60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.192D), Telerik.Reporting.Drawing.Unit.Inch(0.75D));
            this.table60.Name = "table60";
            tableGroup273.Name = "group54";
            tableGroup274.Name = "group66";
            tableGroup275.Name = "group67";
            tableGroup276.Name = "group63";
            tableGroup277.Name = "group65";
            tableGroup278.Name = "group64";
            tableGroup279.Name = "group62";
            tableGroup280.Name = "group61";
            tableGroup281.Name = "group60";
            tableGroup282.Name = "group59";
            tableGroup283.Name = "group58";
            tableGroup284.Name = "group57";
            tableGroup285.Name = "group56";
            tableGroup286.Name = "group55";
            tableGroup287.Name = "group68";
            tableGroup288.Name = "group69";
            tableGroup289.Name = "group70";
            tableGroup290.Name = "group71";
            tableGroup291.Name = "group72";
            tableGroup292.Name = "group73";
            tableGroup272.ChildGroups.Add(tableGroup273);
            tableGroup272.ChildGroups.Add(tableGroup274);
            tableGroup272.ChildGroups.Add(tableGroup275);
            tableGroup272.ChildGroups.Add(tableGroup276);
            tableGroup272.ChildGroups.Add(tableGroup277);
            tableGroup272.ChildGroups.Add(tableGroup278);
            tableGroup272.ChildGroups.Add(tableGroup279);
            tableGroup272.ChildGroups.Add(tableGroup280);
            tableGroup272.ChildGroups.Add(tableGroup281);
            tableGroup272.ChildGroups.Add(tableGroup282);
            tableGroup272.ChildGroups.Add(tableGroup283);
            tableGroup272.ChildGroups.Add(tableGroup284);
            tableGroup272.ChildGroups.Add(tableGroup285);
            tableGroup272.ChildGroups.Add(tableGroup286);
            tableGroup272.ChildGroups.Add(tableGroup287);
            tableGroup272.ChildGroups.Add(tableGroup288);
            tableGroup272.ChildGroups.Add(tableGroup289);
            tableGroup272.ChildGroups.Add(tableGroup290);
            tableGroup272.ChildGroups.Add(tableGroup291);
            tableGroup272.ChildGroups.Add(tableGroup292);
            tableGroup272.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup272.Name = "detailTableGroup5";
            this.table60.RowGroups.Add(tableGroup272);
            this.table60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.777D), Telerik.Reporting.Drawing.Unit.Inch(4.28D));
            // 
            // textBox157
            // 
            this.textBox157.Name = "textBox157";
            this.textBox157.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox157.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox157.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox157.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox157.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox157.Style.Font.Bold = true;
            this.textBox157.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox157.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox157.Value = " 1";
            // 
            // textBox160
            // 
            this.textBox160.Name = "textBox160";
            this.textBox160.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox160.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox160.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox160.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox160.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox160.Style.Font.Bold = true;
            this.textBox160.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox160.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox158
            // 
            this.textBox158.Name = "textBox158";
            this.textBox158.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox158.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox158.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox158.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox158.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox158.Style.Font.Bold = true;
            this.textBox158.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox158.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox158.StyleName = "";
            this.textBox158.Value = "14";
            // 
            // textBox162
            // 
            this.textBox162.Name = "textBox162";
            this.textBox162.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox162.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox162.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox162.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox162.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox162.Style.Font.Bold = true;
            this.textBox162.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox162.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox162.StyleName = "";
            // 
            // textBox221
            // 
            this.textBox221.Name = "textBox221";
            this.textBox221.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox221.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox221.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox221.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox221.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox221.Style.Font.Bold = true;
            this.textBox221.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox221.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox221.StyleName = "";
            this.textBox221.Value = "13";
            // 
            // textBox222
            // 
            this.textBox222.Name = "textBox222";
            this.textBox222.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox222.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox222.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox222.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox222.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox222.Style.Font.Bold = true;
            this.textBox222.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox222.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox222.StyleName = "";
            // 
            // textBox223
            // 
            this.textBox223.Name = "textBox223";
            this.textBox223.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox223.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox223.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox223.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox223.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox223.Style.Font.Bold = true;
            this.textBox223.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox223.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox223.StyleName = "";
            this.textBox223.Value = "12";
            // 
            // textBox224
            // 
            this.textBox224.Name = "textBox224";
            this.textBox224.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox224.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox224.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox224.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox224.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox224.Style.Font.Bold = true;
            this.textBox224.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox224.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox224.StyleName = "";
            // 
            // textBox225
            // 
            this.textBox225.Name = "textBox225";
            this.textBox225.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox225.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox225.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox225.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox225.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox225.Style.Font.Bold = true;
            this.textBox225.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox225.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox225.StyleName = "";
            this.textBox225.Value = "11";
            // 
            // textBox226
            // 
            this.textBox226.Name = "textBox226";
            this.textBox226.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox226.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox226.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox226.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox226.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox226.Style.Font.Bold = true;
            this.textBox226.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox226.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox226.StyleName = "";
            // 
            // textBox227
            // 
            this.textBox227.Name = "textBox227";
            this.textBox227.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox227.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox227.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox227.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox227.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox227.Style.Font.Bold = true;
            this.textBox227.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox227.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox227.StyleName = "";
            this.textBox227.Value = "10";
            // 
            // textBox228
            // 
            this.textBox228.Name = "textBox228";
            this.textBox228.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox228.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox228.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox228.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox228.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox228.Style.Font.Bold = true;
            this.textBox228.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox228.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox228.StyleName = "";
            // 
            // textBox229
            // 
            this.textBox229.Name = "textBox229";
            this.textBox229.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox229.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox229.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox229.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox229.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox229.Style.Font.Bold = true;
            this.textBox229.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox229.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox229.StyleName = "";
            this.textBox229.Value = "9";
            // 
            // textBox230
            // 
            this.textBox230.Name = "textBox230";
            this.textBox230.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox230.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox230.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox230.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox230.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox230.Style.Font.Bold = true;
            this.textBox230.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox230.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox230.StyleName = "";
            // 
            // textBox231
            // 
            this.textBox231.Name = "textBox231";
            this.textBox231.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox231.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox231.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox231.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox231.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox231.Style.Font.Bold = true;
            this.textBox231.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox231.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox231.StyleName = "";
            this.textBox231.Value = "8";
            // 
            // textBox232
            // 
            this.textBox232.Name = "textBox232";
            this.textBox232.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox232.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox232.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox232.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox232.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox232.Style.Font.Bold = true;
            this.textBox232.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox232.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox232.StyleName = "";
            // 
            // textBox233
            // 
            this.textBox233.Name = "textBox233";
            this.textBox233.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox233.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox233.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox233.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox233.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox233.Style.Font.Bold = true;
            this.textBox233.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox233.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox233.StyleName = "";
            this.textBox233.Value = "7";
            // 
            // textBox234
            // 
            this.textBox234.Name = "textBox234";
            this.textBox234.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox234.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox234.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox234.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox234.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox234.Style.Font.Bold = true;
            this.textBox234.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox234.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox234.StyleName = "";
            // 
            // textBox235
            // 
            this.textBox235.Name = "textBox235";
            this.textBox235.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox235.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox235.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox235.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox235.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox235.Style.Font.Bold = true;
            this.textBox235.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox235.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox235.StyleName = "";
            this.textBox235.Value = "4";
            // 
            // textBox293
            // 
            this.textBox293.Name = "textBox293";
            this.textBox293.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox293.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox293.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox293.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox293.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox293.Style.Font.Bold = true;
            this.textBox293.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox293.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox293.StyleName = "";
            // 
            // textBox294
            // 
            this.textBox294.Name = "textBox294";
            this.textBox294.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox294.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox294.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox294.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox294.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox294.Style.Font.Bold = true;
            this.textBox294.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox294.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox294.StyleName = "";
            this.textBox294.Value = "6";
            // 
            // textBox295
            // 
            this.textBox295.Name = "textBox295";
            this.textBox295.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox295.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox295.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox295.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox295.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox295.Style.Font.Bold = true;
            this.textBox295.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox295.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox295.StyleName = "";
            // 
            // textBox296
            // 
            this.textBox296.Name = "textBox296";
            this.textBox296.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox296.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox296.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox296.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox296.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox296.Style.Font.Bold = true;
            this.textBox296.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox296.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox296.StyleName = "";
            this.textBox296.Value = "5";
            // 
            // textBox297
            // 
            this.textBox297.Name = "textBox297";
            this.textBox297.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox297.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox297.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox297.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox297.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox297.Style.Font.Bold = true;
            this.textBox297.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox297.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox297.StyleName = "";
            // 
            // textBox298
            // 
            this.textBox298.Name = "textBox298";
            this.textBox298.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox298.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox298.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox298.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox298.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox298.Style.Font.Bold = true;
            this.textBox298.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox298.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox298.StyleName = "";
            this.textBox298.Value = "2";
            // 
            // textBox299
            // 
            this.textBox299.Name = "textBox299";
            this.textBox299.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox299.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox299.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox299.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox299.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox299.Style.Font.Bold = true;
            this.textBox299.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox299.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox299.StyleName = "";
            // 
            // textBox300
            // 
            this.textBox300.Name = "textBox300";
            this.textBox300.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox300.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox300.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox300.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox300.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox300.Style.Font.Bold = true;
            this.textBox300.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox300.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox300.StyleName = "";
            this.textBox300.Value = "3";
            // 
            // textBox301
            // 
            this.textBox301.Name = "textBox301";
            this.textBox301.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox301.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox301.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox301.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox301.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox301.Style.Font.Bold = true;
            this.textBox301.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox301.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox301.StyleName = "";
            // 
            // textBox302
            // 
            this.textBox302.Name = "textBox302";
            this.textBox302.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox302.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox302.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox302.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox302.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox302.Style.Font.Bold = true;
            this.textBox302.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox302.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox302.StyleName = "";
            this.textBox302.Value = "15";
            // 
            // textBox303
            // 
            this.textBox303.Name = "textBox303";
            this.textBox303.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox303.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox303.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox303.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox303.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox303.Style.Font.Bold = true;
            this.textBox303.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox303.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox303.StyleName = "";
            // 
            // textBox304
            // 
            this.textBox304.Name = "textBox304";
            this.textBox304.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox304.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox304.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox304.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox304.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox304.Style.Font.Bold = true;
            this.textBox304.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox304.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox304.StyleName = "";
            this.textBox304.Value = "16";
            // 
            // textBox305
            // 
            this.textBox305.Name = "textBox305";
            this.textBox305.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox305.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox305.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox305.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox305.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox305.Style.Font.Bold = true;
            this.textBox305.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox305.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox305.StyleName = "";
            // 
            // textBox306
            // 
            this.textBox306.Name = "textBox306";
            this.textBox306.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox306.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox306.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox306.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox306.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox306.Style.Font.Bold = true;
            this.textBox306.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox306.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox306.StyleName = "";
            this.textBox306.Value = "17";
            // 
            // textBox307
            // 
            this.textBox307.Name = "textBox307";
            this.textBox307.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox307.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox307.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox307.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox307.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox307.Style.Font.Bold = true;
            this.textBox307.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox307.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox307.StyleName = "";
            // 
            // textBox308
            // 
            this.textBox308.Name = "textBox308";
            this.textBox308.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox308.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox308.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox308.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox308.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox308.Style.Font.Bold = true;
            this.textBox308.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox308.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox308.StyleName = "";
            this.textBox308.Value = "18";
            // 
            // textBox309
            // 
            this.textBox309.Name = "textBox309";
            this.textBox309.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox309.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox309.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox309.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox309.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox309.Style.Font.Bold = true;
            this.textBox309.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox309.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox309.StyleName = "";
            // 
            // textBox310
            // 
            this.textBox310.Name = "textBox310";
            this.textBox310.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox310.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox310.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox310.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox310.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox310.Style.Font.Bold = true;
            this.textBox310.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox310.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox310.StyleName = "";
            this.textBox310.Value = "19";
            // 
            // textBox311
            // 
            this.textBox311.Name = "textBox311";
            this.textBox311.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox311.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox311.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox311.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox311.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox311.Style.Font.Bold = true;
            this.textBox311.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox311.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox311.StyleName = "";
            // 
            // textBox312
            // 
            this.textBox312.Name = "textBox312";
            this.textBox312.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.727D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox312.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox312.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox312.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox312.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox312.Style.Font.Bold = true;
            this.textBox312.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox312.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox312.StyleName = "";
            this.textBox312.Value = "20";
            // 
            // textBox313
            // 
            this.textBox313.Name = "textBox313";
            this.textBox313.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox313.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox313.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox313.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox313.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox313.Style.Font.Bold = true;
            this.textBox313.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox313.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox313.StyleName = "";
            // 
            // htmlTextBox3
            // 
            this.htmlTextBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.176D), Telerik.Reporting.Drawing.Unit.Inch(6D));
            this.htmlTextBox3.Name = "htmlTextBox3";
            this.htmlTextBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.971D), Telerik.Reporting.Drawing.Unit.Inch(1.2D));
            this.htmlTextBox3.Style.Color = System.Drawing.Color.MidnightBlue;
            this.htmlTextBox3.Value = resources.GetString("htmlTextBox3.Value");
            // 
            // htmlTextBox4
            // 
            this.htmlTextBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.218D), Telerik.Reporting.Drawing.Unit.Inch(7.65D));
            this.htmlTextBox4.Name = "htmlTextBox4";
            this.htmlTextBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.971D), Telerik.Reporting.Drawing.Unit.Inch(1.7D));
            this.htmlTextBox4.Style.Color = System.Drawing.Color.MidnightBlue;
            this.htmlTextBox4.Value = resources.GetString("htmlTextBox4.Value");
            // 
            // textBox153
            // 
            this.textBox153.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.192D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox153.Name = "textBox153";
            this.textBox153.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.268D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox153.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox153.Style.Font.Bold = true;
            this.textBox153.Value = "INVERTER SERIAL NUMBERS";
            // 
            // textBox315
            // 
            this.textBox315.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.218D), Telerik.Reporting.Drawing.Unit.Inch(7.35D));
            this.textBox315.Name = "textBox315";
            this.textBox315.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.978D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox315.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox315.Style.Font.Bold = true;
            this.textBox315.Value = "TYPES OF STORAGE DEVICES";
            // 
            // textBox316
            // 
            this.textBox316.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.2D), Telerik.Reporting.Drawing.Unit.Inch(9.9D));
            this.textBox316.Name = "textBox316";
            this.textBox316.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox316.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox316.Value = "Page 5 of 6";
            // 
            // page4
            // 
            this.page4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox152,
            this.table59});
            this.page4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(30.56D));
            this.page4.Name = "page4";
            this.page4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.261D), Telerik.Reporting.Drawing.Unit.Inch(10.05D));
            // 
            // textBox152
            // 
            this.textBox152.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.2D), Telerik.Reporting.Drawing.Unit.Inch(9.84D));
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox152.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox152.Value = "Page 4 of 6";
            // 
            // table59
            // 
            this.table59.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(4.735D)));
            this.table59.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.108D)));
            this.table59.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.108D)));
            this.table59.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.108D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table59.Body.SetCellContent(1, 0, this.textBox236);
            this.table59.Body.SetCellContent(1, 1, this.textBox237);
            this.table59.Body.SetCellContent(1, 2, this.textBox238);
            this.table59.Body.SetCellContent(1, 3, this.textBox239);
            this.table59.Body.SetCellContent(0, 0, this.textBox240, 1, 4);
            this.table59.Body.SetCellContent(2, 0, this.textBox241);
            this.table59.Body.SetCellContent(2, 1, this.textBox242);
            this.table59.Body.SetCellContent(2, 2, this.textBox243);
            this.table59.Body.SetCellContent(2, 3, this.textBox244);
            this.table59.Body.SetCellContent(3, 0, this.textBox245);
            this.table59.Body.SetCellContent(3, 1, this.textBox246);
            this.table59.Body.SetCellContent(3, 2, this.textBox247);
            this.table59.Body.SetCellContent(3, 3, this.textBox248);
            this.table59.Body.SetCellContent(5, 0, this.textBox249);
            this.table59.Body.SetCellContent(5, 1, this.textBox250);
            this.table59.Body.SetCellContent(5, 2, this.textBox251);
            this.table59.Body.SetCellContent(5, 3, this.textBox252);
            this.table59.Body.SetCellContent(7, 0, this.textBox253);
            this.table59.Body.SetCellContent(7, 1, this.textBox254);
            this.table59.Body.SetCellContent(7, 2, this.textBox255);
            this.table59.Body.SetCellContent(7, 3, this.textBox256);
            this.table59.Body.SetCellContent(9, 1, this.textBox257);
            this.table59.Body.SetCellContent(9, 2, this.textBox258);
            this.table59.Body.SetCellContent(9, 3, this.textBox259);
            this.table59.Body.SetCellContent(8, 0, this.textBox260);
            this.table59.Body.SetCellContent(6, 0, this.textBox261);
            this.table59.Body.SetCellContent(6, 1, this.textBox262);
            this.table59.Body.SetCellContent(6, 2, this.textBox263);
            this.table59.Body.SetCellContent(6, 3, this.textBox264);
            this.table59.Body.SetCellContent(4, 0, this.textBox265);
            this.table59.Body.SetCellContent(4, 1, this.textBox266);
            this.table59.Body.SetCellContent(4, 2, this.textBox267);
            this.table59.Body.SetCellContent(4, 3, this.textBox268);
            this.table59.Body.SetCellContent(14, 1, this.textBox269);
            this.table59.Body.SetCellContent(14, 2, this.textBox270);
            this.table59.Body.SetCellContent(14, 3, this.textBox271);
            this.table59.Body.SetCellContent(13, 1, this.textBox272);
            this.table59.Body.SetCellContent(13, 2, this.textBox273);
            this.table59.Body.SetCellContent(13, 3, this.textBox274);
            this.table59.Body.SetCellContent(12, 1, this.textBox275);
            this.table59.Body.SetCellContent(12, 2, this.textBox276);
            this.table59.Body.SetCellContent(12, 3, this.textBox277);
            this.table59.Body.SetCellContent(11, 1, this.textBox278);
            this.table59.Body.SetCellContent(11, 2, this.textBox279);
            this.table59.Body.SetCellContent(11, 3, this.textBox280);
            this.table59.Body.SetCellContent(10, 1, this.textBox281);
            this.table59.Body.SetCellContent(10, 2, this.textBox282);
            this.table59.Body.SetCellContent(10, 3, this.textBox283);
            this.table59.Body.SetCellContent(9, 0, this.textBox284);
            this.table59.Body.SetCellContent(10, 0, this.textBox285);
            this.table59.Body.SetCellContent(11, 0, this.textBox286);
            this.table59.Body.SetCellContent(12, 0, this.textBox287);
            this.table59.Body.SetCellContent(13, 0, this.textBox288);
            this.table59.Body.SetCellContent(14, 0, this.textBox289);
            this.table59.Body.SetCellContent(8, 1, this.textBox290);
            this.table59.Body.SetCellContent(8, 2, this.textBox291);
            this.table59.Body.SetCellContent(8, 3, this.textBox292);
            tableGroup293.Name = "group30";
            tableGroup294.Name = "tableGroup10";
            tableGroup295.Name = "group27";
            tableGroup296.Name = "tableGroup11";
            this.table59.ColumnGroups.Add(tableGroup293);
            this.table59.ColumnGroups.Add(tableGroup294);
            this.table59.ColumnGroups.Add(tableGroup295);
            this.table59.ColumnGroups.Add(tableGroup296);
            this.table59.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox240,
            this.textBox236,
            this.textBox237,
            this.textBox238,
            this.textBox239,
            this.textBox241,
            this.textBox242,
            this.textBox243,
            this.textBox244,
            this.textBox245,
            this.textBox246,
            this.textBox247,
            this.textBox248,
            this.textBox265,
            this.textBox266,
            this.textBox267,
            this.textBox268,
            this.textBox249,
            this.textBox250,
            this.textBox251,
            this.textBox252,
            this.textBox261,
            this.textBox262,
            this.textBox263,
            this.textBox264,
            this.textBox253,
            this.textBox254,
            this.textBox255,
            this.textBox256,
            this.textBox260,
            this.textBox290,
            this.textBox291,
            this.textBox292,
            this.textBox284,
            this.textBox257,
            this.textBox258,
            this.textBox259,
            this.textBox285,
            this.textBox281,
            this.textBox282,
            this.textBox283,
            this.textBox286,
            this.textBox278,
            this.textBox279,
            this.textBox280,
            this.textBox287,
            this.textBox275,
            this.textBox276,
            this.textBox277,
            this.textBox288,
            this.textBox272,
            this.textBox273,
            this.textBox274,
            this.textBox289,
            this.textBox269,
            this.textBox270,
            this.textBox271});
            this.table59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.093D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.table59.Name = "table59";
            tableGroup297.Name = "group39";
            tableGroup298.Name = "group40";
            tableGroup299.Name = "group41";
            tableGroup300.Name = "group42";
            tableGroup301.Name = "group48";
            tableGroup302.Name = "group43";
            tableGroup303.Name = "group47";
            tableGroup304.Name = "group44";
            tableGroup305.Name = "group46";
            tableGroup306.Name = "group45";
            tableGroup307.Name = "group53";
            tableGroup308.Name = "group52";
            tableGroup309.Name = "group51";
            tableGroup310.Name = "group50";
            tableGroup311.Name = "group49";
            this.table59.RowGroups.Add(tableGroup297);
            this.table59.RowGroups.Add(tableGroup298);
            this.table59.RowGroups.Add(tableGroup299);
            this.table59.RowGroups.Add(tableGroup300);
            this.table59.RowGroups.Add(tableGroup301);
            this.table59.RowGroups.Add(tableGroup302);
            this.table59.RowGroups.Add(tableGroup303);
            this.table59.RowGroups.Add(tableGroup304);
            this.table59.RowGroups.Add(tableGroup305);
            this.table59.RowGroups.Add(tableGroup306);
            this.table59.RowGroups.Add(tableGroup307);
            this.table59.RowGroups.Add(tableGroup308);
            this.table59.RowGroups.Add(tableGroup309);
            this.table59.RowGroups.Add(tableGroup310);
            this.table59.RowGroups.Add(tableGroup311);
            this.table59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.059D), Telerik.Reporting.Drawing.Unit.Inch(3D));
            this.table59.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table59.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox236
            // 
            this.textBox236.Name = "textBox236";
            this.textBox236.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox236.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox236.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox236.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox236.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox236.Style.Color = System.Drawing.Color.White;
            this.textBox236.Style.Font.Bold = true;
            this.textBox236.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox236.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox236.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox236.StyleName = "";
            this.textBox236.Value = "STORAGE 1";
            // 
            // textBox237
            // 
            this.textBox237.Name = "textBox237";
            this.textBox237.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox237.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox237.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox237.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox237.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox237.Style.Font.Bold = true;
            this.textBox237.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox237.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox237.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox237.StyleName = "";
            this.textBox237.Value = "INVERTER 1";
            // 
            // textBox238
            // 
            this.textBox238.Name = "textBox238";
            this.textBox238.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox238.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox238.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox238.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox238.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox238.Style.Font.Bold = true;
            this.textBox238.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox238.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox238.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox238.StyleName = "";
            this.textBox238.Value = "INVERTER 2";
            // 
            // textBox239
            // 
            this.textBox239.Name = "textBox239";
            this.textBox239.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox239.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox239.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox239.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox239.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox239.Style.Font.Bold = true;
            this.textBox239.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox239.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox239.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox239.StyleName = "";
            this.textBox239.Value = "INVERTER 3";
            // 
            // textBox240
            // 
            this.textBox240.Name = "textBox240";
            this.textBox240.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.059D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox240.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox240.Style.Color = System.Drawing.Color.White;
            this.textBox240.Style.Font.Bold = true;
            this.textBox240.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox240.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox240.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox240.StyleName = "";
            this.textBox240.Value = "STORAGE DETAILS: MANDATORY FOR NEW / MODIFY / ADD / DECOMMISSION INSTALLATIONS";
            // 
            // textBox241
            // 
            this.textBox241.Name = "textBox241";
            this.textBox241.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox241.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox241.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox241.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox241.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox241.Style.Color = System.Drawing.Color.White;
            this.textBox241.Style.Font.Bold = true;
            this.textBox241.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox241.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox241.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox241.StyleName = "";
            this.textBox241.Value = "Storage Type (Battery / Other)";
            // 
            // textBox242
            // 
            this.textBox242.Name = "textBox242";
            this.textBox242.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox242.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox242.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox242.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox242.StyleName = "";
            // 
            // textBox243
            // 
            this.textBox243.Name = "textBox243";
            this.textBox243.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox243.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox243.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox243.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox243.StyleName = "";
            // 
            // textBox244
            // 
            this.textBox244.Name = "textBox244";
            this.textBox244.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox244.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox244.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox244.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox244.StyleName = "";
            // 
            // textBox245
            // 
            this.textBox245.Name = "textBox245";
            this.textBox245.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox245.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox245.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox245.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox245.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox245.Style.Color = System.Drawing.Color.White;
            this.textBox245.Style.Font.Bold = true;
            this.textBox245.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox245.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox245.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox245.StyleName = "";
            this.textBox245.Value = "Quantity (No of Batteries / Fuel Cells etc.)";
            // 
            // textBox246
            // 
            this.textBox246.Name = "textBox246";
            this.textBox246.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox246.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox246.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox246.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox246.StyleName = "";
            // 
            // textBox247
            // 
            this.textBox247.Name = "textBox247";
            this.textBox247.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox247.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox247.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox247.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox247.StyleName = "";
            // 
            // textBox248
            // 
            this.textBox248.Name = "textBox248";
            this.textBox248.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox248.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox248.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox248.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox248.StyleName = "";
            // 
            // textBox249
            // 
            this.textBox249.Name = "textBox249";
            this.textBox249.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox249.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox249.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox249.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox249.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox249.Style.Color = System.Drawing.Color.White;
            this.textBox249.Style.Font.Bold = true;
            this.textBox249.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox249.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox249.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox249.StyleName = "";
            this.textBox249.Value = "Manufacturer";
            // 
            // textBox250
            // 
            this.textBox250.Name = "textBox250";
            this.textBox250.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox250.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox250.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox250.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox250.StyleName = "";
            // 
            // textBox251
            // 
            this.textBox251.Name = "textBox251";
            this.textBox251.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox251.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox251.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox251.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox251.StyleName = "";
            // 
            // textBox252
            // 
            this.textBox252.Name = "textBox252";
            this.textBox252.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox252.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox252.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox252.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox252.StyleName = "";
            // 
            // textBox253
            // 
            this.textBox253.Name = "textBox253";
            this.textBox253.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox253.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox253.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox253.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox253.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox253.Style.Color = System.Drawing.Color.White;
            this.textBox253.Style.Font.Bold = true;
            this.textBox253.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox253.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox253.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox253.StyleName = "";
            this.textBox253.Value = "Rated Storage Capacity per Device (kWh)";
            // 
            // textBox254
            // 
            this.textBox254.Name = "textBox254";
            this.textBox254.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox254.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox254.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox254.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox254.StyleName = "";
            // 
            // textBox255
            // 
            this.textBox255.Name = "textBox255";
            this.textBox255.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox255.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox255.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox255.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox255.StyleName = "";
            // 
            // textBox256
            // 
            this.textBox256.Name = "textBox256";
            this.textBox256.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox256.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox256.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox256.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox256.StyleName = "";
            // 
            // textBox257
            // 
            this.textBox257.Name = "textBox257";
            this.textBox257.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox257.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox257.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox257.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox257.StyleName = "";
            // 
            // textBox258
            // 
            this.textBox258.Name = "textBox258";
            this.textBox258.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox258.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox258.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox258.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox258.StyleName = "";
            // 
            // textBox259
            // 
            this.textBox259.Name = "textBox259";
            this.textBox259.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox259.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox259.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox259.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox259.StyleName = "";
            // 
            // textBox260
            // 
            this.textBox260.Name = "textBox260";
            this.textBox260.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox260.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
            this.textBox260.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox260.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox260.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox260.Style.Color = System.Drawing.Color.White;
            this.textBox260.Style.Font.Bold = true;
            this.textBox260.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox260.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox260.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox260.StyleName = "";
            this.textBox260.Value = "STORAGE 2";
            // 
            // textBox261
            // 
            this.textBox261.Name = "textBox261";
            this.textBox261.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox261.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox261.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox261.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox261.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox261.Style.Color = System.Drawing.Color.White;
            this.textBox261.Style.Font.Bold = true;
            this.textBox261.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox261.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox261.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox261.StyleName = "";
            this.textBox261.Value = "Model Number";
            // 
            // textBox262
            // 
            this.textBox262.Name = "textBox262";
            this.textBox262.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox262.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox262.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox262.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox262.StyleName = "";
            // 
            // textBox263
            // 
            this.textBox263.Name = "textBox263";
            this.textBox263.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox263.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox263.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox263.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox263.StyleName = "";
            // 
            // textBox264
            // 
            this.textBox264.Name = "textBox264";
            this.textBox264.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox264.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox264.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox264.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox264.StyleName = "";
            // 
            // textBox265
            // 
            this.textBox265.Name = "textBox265";
            this.textBox265.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox265.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox265.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox265.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox265.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox265.Style.Color = System.Drawing.Color.White;
            this.textBox265.Style.Font.Bold = true;
            this.textBox265.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox265.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox265.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox265.StyleName = "";
            this.textBox265.Value = "Type of Storage Device (Only applicable for Storage Devices � Refer to Page 5)";
            // 
            // textBox266
            // 
            this.textBox266.Name = "textBox266";
            this.textBox266.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox266.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox266.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox266.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox266.StyleName = "";
            // 
            // textBox267
            // 
            this.textBox267.Name = "textBox267";
            this.textBox267.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox267.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox267.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox267.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox267.StyleName = "";
            // 
            // textBox268
            // 
            this.textBox268.Name = "textBox268";
            this.textBox268.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox268.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox268.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox268.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox268.StyleName = "";
            // 
            // textBox269
            // 
            this.textBox269.Name = "textBox269";
            this.textBox269.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox269.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox269.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox269.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox269.StyleName = "";
            // 
            // textBox270
            // 
            this.textBox270.Name = "textBox270";
            this.textBox270.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox270.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox270.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox270.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox270.StyleName = "";
            // 
            // textBox271
            // 
            this.textBox271.Name = "textBox271";
            this.textBox271.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox271.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox271.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox271.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox271.StyleName = "";
            // 
            // textBox272
            // 
            this.textBox272.Name = "textBox272";
            this.textBox272.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox272.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox272.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox272.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox272.StyleName = "";
            // 
            // textBox273
            // 
            this.textBox273.Name = "textBox273";
            this.textBox273.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox273.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox273.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox273.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox273.StyleName = "";
            // 
            // textBox274
            // 
            this.textBox274.Name = "textBox274";
            this.textBox274.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox274.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox274.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox274.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox274.StyleName = "";
            // 
            // textBox275
            // 
            this.textBox275.Name = "textBox275";
            this.textBox275.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox275.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox275.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox275.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox275.StyleName = "";
            // 
            // textBox276
            // 
            this.textBox276.Name = "textBox276";
            this.textBox276.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox276.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox276.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox276.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox276.StyleName = "";
            // 
            // textBox277
            // 
            this.textBox277.Name = "textBox277";
            this.textBox277.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox277.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox277.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox277.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox277.StyleName = "";
            // 
            // textBox278
            // 
            this.textBox278.Name = "textBox278";
            this.textBox278.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox278.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox278.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox278.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox278.StyleName = "";
            // 
            // textBox279
            // 
            this.textBox279.Name = "textBox279";
            this.textBox279.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox279.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox279.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox279.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox279.StyleName = "";
            // 
            // textBox280
            // 
            this.textBox280.Name = "textBox280";
            this.textBox280.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox280.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox280.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox280.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox280.StyleName = "";
            // 
            // textBox281
            // 
            this.textBox281.Name = "textBox281";
            this.textBox281.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox281.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox281.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox281.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox281.StyleName = "";
            // 
            // textBox282
            // 
            this.textBox282.Name = "textBox282";
            this.textBox282.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox282.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox282.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox282.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox282.StyleName = "";
            // 
            // textBox283
            // 
            this.textBox283.Name = "textBox283";
            this.textBox283.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox283.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox283.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox283.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox283.StyleName = "";
            // 
            // textBox284
            // 
            this.textBox284.Name = "textBox284";
            this.textBox284.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox284.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
            this.textBox284.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox284.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox284.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox284.Style.Color = System.Drawing.Color.White;
            this.textBox284.Style.Font.Bold = true;
            this.textBox284.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox284.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox284.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox284.StyleName = "";
            this.textBox284.Value = "Storage Type (Battery / Other)";
            // 
            // textBox285
            // 
            this.textBox285.Name = "textBox285";
            this.textBox285.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox285.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
            this.textBox285.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox285.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox285.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox285.Style.Color = System.Drawing.Color.White;
            this.textBox285.Style.Font.Bold = true;
            this.textBox285.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox285.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox285.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox285.StyleName = "";
            this.textBox285.Value = "Quantity (No of Batteries / Fuel Cells etc.)";
            // 
            // textBox286
            // 
            this.textBox286.Name = "textBox286";
            this.textBox286.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox286.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
            this.textBox286.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox286.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox286.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox286.Style.Color = System.Drawing.Color.White;
            this.textBox286.Style.Font.Bold = true;
            this.textBox286.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox286.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox286.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox286.StyleName = "";
            this.textBox286.Value = "Type of Storage Device (Only applicable for Storage Devices � Refer to Page 5)";
            // 
            // textBox287
            // 
            this.textBox287.Name = "textBox287";
            this.textBox287.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox287.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
            this.textBox287.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox287.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox287.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox287.Style.Color = System.Drawing.Color.White;
            this.textBox287.Style.Font.Bold = true;
            this.textBox287.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox287.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox287.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox287.StyleName = "";
            this.textBox287.Value = "Manufacturer";
            // 
            // textBox288
            // 
            this.textBox288.Name = "textBox288";
            this.textBox288.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox288.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
            this.textBox288.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox288.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox288.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox288.Style.Color = System.Drawing.Color.White;
            this.textBox288.Style.Font.Bold = true;
            this.textBox288.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox288.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox288.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox288.StyleName = "";
            this.textBox288.Value = "Model Number";
            // 
            // textBox289
            // 
            this.textBox289.Name = "textBox289";
            this.textBox289.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox289.Style.BackgroundColor = System.Drawing.Color.CornflowerBlue;
            this.textBox289.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox289.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox289.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox289.Style.Color = System.Drawing.Color.White;
            this.textBox289.Style.Font.Bold = true;
            this.textBox289.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox289.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox289.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox289.StyleName = "";
            this.textBox289.Value = "Rated Storage Capacity per Device (kWh)";
            // 
            // textBox290
            // 
            this.textBox290.Name = "textBox290";
            this.textBox290.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox290.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox290.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox290.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox290.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox290.Style.Font.Bold = true;
            this.textBox290.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox290.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox290.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox290.StyleName = "";
            this.textBox290.Value = "INVERTER 1";
            // 
            // textBox291
            // 
            this.textBox291.Name = "textBox291";
            this.textBox291.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox291.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox291.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox291.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox291.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox291.Style.Font.Bold = true;
            this.textBox291.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox291.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox291.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox291.StyleName = "";
            this.textBox291.Value = "INVERTER 2";
            // 
            // textBox292
            // 
            this.textBox292.Name = "textBox292";
            this.textBox292.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox292.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox292.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox292.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox292.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox292.Style.Font.Bold = true;
            this.textBox292.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox292.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox292.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox292.StyleName = "";
            this.textBox292.Value = "INVERTER 3";
            // 
            // page3
            // 
            this.page3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox95,
            this.table9});
            this.page3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(20.36D));
            this.page3.Name = "page3";
            this.page3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.193D), Telerik.Reporting.Drawing.Unit.Inch(10.15D));
            // 
            // textBox95
            // 
            this.textBox95.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.2D), Telerik.Reporting.Drawing.Unit.Inch(9.94D));
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.2D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox95.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox95.Value = "Page 3 of 6";
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(4.735D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.108D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.108D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.108D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.325D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.492D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table9.Body.SetCellContent(0, 0, this.textBox74);
            this.table9.Body.SetCellContent(1, 0, this.textBox75);
            this.table9.Body.SetCellContent(1, 1, this.textBox77);
            this.table9.Body.SetCellContent(1, 2, this.textBox96);
            this.table9.Body.SetCellContent(1, 3, this.textBox134);
            this.table9.Body.SetCellContent(2, 0, this.textBox135);
            this.table9.Body.SetCellContent(2, 1, this.textBox136);
            this.table9.Body.SetCellContent(2, 2, this.textBox137);
            this.table9.Body.SetCellContent(2, 3, this.textBox138);
            this.table9.Body.SetCellContent(3, 0, this.textBox139);
            this.table9.Body.SetCellContent(3, 1, this.textBox140);
            this.table9.Body.SetCellContent(3, 2, this.textBox141);
            this.table9.Body.SetCellContent(3, 3, this.textBox142);
            this.table9.Body.SetCellContent(4, 0, this.textBox143);
            this.table9.Body.SetCellContent(4, 1, this.textBox144);
            this.table9.Body.SetCellContent(4, 2, this.textBox145);
            this.table9.Body.SetCellContent(4, 3, this.textBox146);
            this.table9.Body.SetCellContent(5, 0, this.textBox147);
            this.table9.Body.SetCellContent(5, 1, this.textBox148);
            this.table9.Body.SetCellContent(5, 2, this.textBox149);
            this.table9.Body.SetCellContent(5, 3, this.textBox150);
            this.table9.Body.SetCellContent(6, 0, this.textBox151);
            this.table9.Body.SetCellContent(7, 0, this.textBox155);
            this.table9.Body.SetCellContent(8, 0, this.textBox159);
            this.table9.Body.SetCellContent(10, 0, this.textBox167);
            this.table9.Body.SetCellContent(10, 1, this.textBox168);
            this.table9.Body.SetCellContent(10, 2, this.textBox169);
            this.table9.Body.SetCellContent(10, 3, this.textBox170);
            this.table9.Body.SetCellContent(9, 0, this.textBox163, 1, 4);
            this.table9.Body.SetCellContent(11, 0, this.textBox164);
            this.table9.Body.SetCellContent(11, 1, this.textBox165);
            this.table9.Body.SetCellContent(11, 2, this.textBox166);
            this.table9.Body.SetCellContent(11, 3, this.textBox171);
            this.table9.Body.SetCellContent(12, 0, this.textBox172);
            this.table9.Body.SetCellContent(12, 1, this.textBox173);
            this.table9.Body.SetCellContent(12, 2, this.textBox174);
            this.table9.Body.SetCellContent(12, 3, this.textBox175);
            this.table9.Body.SetCellContent(14, 0, this.textBox176);
            this.table9.Body.SetCellContent(14, 1, this.textBox177);
            this.table9.Body.SetCellContent(14, 2, this.textBox178);
            this.table9.Body.SetCellContent(14, 3, this.textBox179);
            this.table9.Body.SetCellContent(16, 0, this.textBox180);
            this.table9.Body.SetCellContent(16, 1, this.textBox181);
            this.table9.Body.SetCellContent(16, 2, this.textBox182);
            this.table9.Body.SetCellContent(16, 3, this.textBox183);
            this.table9.Body.SetCellContent(18, 1, this.textBox185);
            this.table9.Body.SetCellContent(18, 2, this.textBox186);
            this.table9.Body.SetCellContent(18, 3, this.textBox187);
            this.table9.Body.SetCellContent(17, 0, this.textBox188);
            this.table9.Body.SetCellContent(15, 0, this.textBox192);
            this.table9.Body.SetCellContent(15, 1, this.textBox193);
            this.table9.Body.SetCellContent(15, 2, this.textBox194);
            this.table9.Body.SetCellContent(15, 3, this.textBox195);
            this.table9.Body.SetCellContent(13, 0, this.textBox196);
            this.table9.Body.SetCellContent(13, 1, this.textBox197);
            this.table9.Body.SetCellContent(13, 2, this.textBox198);
            this.table9.Body.SetCellContent(13, 3, this.textBox199);
            this.table9.Body.SetCellContent(23, 1, this.textBox201);
            this.table9.Body.SetCellContent(23, 2, this.textBox202);
            this.table9.Body.SetCellContent(23, 3, this.textBox203);
            this.table9.Body.SetCellContent(22, 1, this.textBox205);
            this.table9.Body.SetCellContent(22, 2, this.textBox206);
            this.table9.Body.SetCellContent(22, 3, this.textBox207);
            this.table9.Body.SetCellContent(21, 1, this.textBox209);
            this.table9.Body.SetCellContent(21, 2, this.textBox210);
            this.table9.Body.SetCellContent(21, 3, this.textBox211);
            this.table9.Body.SetCellContent(20, 1, this.textBox213);
            this.table9.Body.SetCellContent(20, 2, this.textBox214);
            this.table9.Body.SetCellContent(20, 3, this.textBox215);
            this.table9.Body.SetCellContent(19, 1, this.textBox217);
            this.table9.Body.SetCellContent(19, 2, this.textBox218);
            this.table9.Body.SetCellContent(19, 3, this.textBox219);
            this.table9.Body.SetCellContent(18, 0, this.textBox220);
            this.table9.Body.SetCellContent(19, 0, this.textBox184);
            this.table9.Body.SetCellContent(20, 0, this.textBox216);
            this.table9.Body.SetCellContent(21, 0, this.textBox212);
            this.table9.Body.SetCellContent(22, 0, this.textBox208);
            this.table9.Body.SetCellContent(23, 0, this.textBox204);
            this.table9.Body.SetCellContent(17, 1, this.textBox200);
            this.table9.Body.SetCellContent(17, 2, this.textBox189);
            this.table9.Body.SetCellContent(17, 3, this.textBox190);
            this.table9.Body.SetCellContent(0, 1, this.textBox191);
            this.table9.Body.SetCellContent(0, 2, this.textBox76);
            this.table9.Body.SetCellContent(0, 3, this.textBox120);
            this.table9.Body.SetCellContent(6, 1, this.table17);
            this.table9.Body.SetCellContent(6, 2, this.table18);
            this.table9.Body.SetCellContent(6, 3, this.table19);
            this.table9.Body.SetCellContent(7, 1, this.table23);
            this.table9.Body.SetCellContent(7, 2, this.table27);
            this.table9.Body.SetCellContent(7, 3, this.table43);
            this.table9.Body.SetCellContent(8, 3, this.table56);
            this.table9.Body.SetCellContent(8, 2, this.table57);
            this.table9.Body.SetCellContent(8, 1, this.table58);
            tableGroup343.Name = "group30";
            tableGroup344.Name = "tableGroup10";
            tableGroup345.Name = "group27";
            tableGroup346.Name = "tableGroup11";
            tableGroup342.ChildGroups.Add(tableGroup343);
            tableGroup342.ChildGroups.Add(tableGroup344);
            tableGroup342.ChildGroups.Add(tableGroup345);
            tableGroup342.ChildGroups.Add(tableGroup346);
            tableGroup342.Name = "tableGroup8";
            tableGroup342.ReportItem = this.textBox26;
            this.table9.ColumnGroups.Add(tableGroup342);
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox74,
            this.textBox191,
            this.textBox76,
            this.textBox120,
            this.textBox75,
            this.textBox77,
            this.textBox96,
            this.textBox134,
            this.textBox135,
            this.textBox136,
            this.textBox137,
            this.textBox138,
            this.textBox139,
            this.textBox140,
            this.textBox141,
            this.textBox142,
            this.textBox143,
            this.textBox144,
            this.textBox145,
            this.textBox146,
            this.textBox147,
            this.textBox148,
            this.textBox149,
            this.textBox150,
            this.textBox151,
            this.table17,
            this.table18,
            this.table19,
            this.textBox155,
            this.table23,
            this.table27,
            this.table43,
            this.textBox159,
            this.table58,
            this.table57,
            this.table56,
            this.textBox163,
            this.textBox167,
            this.textBox168,
            this.textBox169,
            this.textBox170,
            this.textBox164,
            this.textBox165,
            this.textBox166,
            this.textBox171,
            this.textBox172,
            this.textBox173,
            this.textBox174,
            this.textBox175,
            this.textBox196,
            this.textBox197,
            this.textBox198,
            this.textBox199,
            this.textBox176,
            this.textBox177,
            this.textBox178,
            this.textBox179,
            this.textBox192,
            this.textBox193,
            this.textBox194,
            this.textBox195,
            this.textBox180,
            this.textBox181,
            this.textBox182,
            this.textBox183,
            this.textBox188,
            this.textBox200,
            this.textBox189,
            this.textBox190,
            this.textBox220,
            this.textBox185,
            this.textBox186,
            this.textBox187,
            this.textBox184,
            this.textBox217,
            this.textBox218,
            this.textBox219,
            this.textBox216,
            this.textBox213,
            this.textBox214,
            this.textBox215,
            this.textBox212,
            this.textBox209,
            this.textBox210,
            this.textBox211,
            this.textBox208,
            this.textBox205,
            this.textBox206,
            this.textBox207,
            this.textBox204,
            this.textBox201,
            this.textBox202,
            this.textBox203,
            this.textBox26});
            this.table9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.093D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.table9.Name = "table9";
            tableGroup347.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup347.Name = "detailTableGroup4";
            tableGroup348.Name = "group31";
            tableGroup349.Name = "group32";
            tableGroup350.Name = "group33";
            tableGroup351.Name = "group34";
            tableGroup352.Name = "group35";
            tableGroup353.Name = "group36";
            tableGroup354.Name = "group37";
            tableGroup355.Name = "group38";
            tableGroup356.Name = "group39";
            tableGroup357.Name = "group40";
            tableGroup358.Name = "group41";
            tableGroup359.Name = "group42";
            tableGroup360.Name = "group48";
            tableGroup361.Name = "group43";
            tableGroup362.Name = "group47";
            tableGroup363.Name = "group44";
            tableGroup364.Name = "group46";
            tableGroup365.Name = "group45";
            tableGroup366.Name = "group53";
            tableGroup367.Name = "group52";
            tableGroup368.Name = "group51";
            tableGroup369.Name = "group50";
            tableGroup370.Name = "group49";
            this.table9.RowGroups.Add(tableGroup347);
            this.table9.RowGroups.Add(tableGroup348);
            this.table9.RowGroups.Add(tableGroup349);
            this.table9.RowGroups.Add(tableGroup350);
            this.table9.RowGroups.Add(tableGroup351);
            this.table9.RowGroups.Add(tableGroup352);
            this.table9.RowGroups.Add(tableGroup353);
            this.table9.RowGroups.Add(tableGroup354);
            this.table9.RowGroups.Add(tableGroup355);
            this.table9.RowGroups.Add(tableGroup356);
            this.table9.RowGroups.Add(tableGroup357);
            this.table9.RowGroups.Add(tableGroup358);
            this.table9.RowGroups.Add(tableGroup359);
            this.table9.RowGroups.Add(tableGroup360);
            this.table9.RowGroups.Add(tableGroup361);
            this.table9.RowGroups.Add(tableGroup362);
            this.table9.RowGroups.Add(tableGroup363);
            this.table9.RowGroups.Add(tableGroup364);
            this.table9.RowGroups.Add(tableGroup365);
            this.table9.RowGroups.Add(tableGroup366);
            this.table9.RowGroups.Add(tableGroup367);
            this.table9.RowGroups.Add(tableGroup368);
            this.table9.RowGroups.Add(tableGroup369);
            this.table9.RowGroups.Add(tableGroup370);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.059D), Telerik.Reporting.Drawing.Unit.Inch(5.497D));
            this.table9.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table9.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox74.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox74.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox74.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox74.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox74.Style.Color = System.Drawing.Color.White;
            this.textBox74.Style.Font.Bold = true;
            this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox74.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox74.Value = "Inverter Details";
            // 
            // textBox75
            // 
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox75.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox75.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox75.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox75.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox75.Style.Color = System.Drawing.Color.White;
            this.textBox75.Style.Font.Bold = true;
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox75.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox75.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox75.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox75.StyleName = "";
            this.textBox75.Value = "Manufacturer";
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox77.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox77.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox77.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox77.StyleName = "";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox96.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox96.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox96.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox96.StyleName = "";
            // 
            // textBox134
            // 
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox134.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox134.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox134.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox134.StyleName = "";
            // 
            // textBox135
            // 
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox135.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox135.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox135.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox135.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox135.Style.Color = System.Drawing.Color.White;
            this.textBox135.Style.Font.Bold = true;
            this.textBox135.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox135.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox135.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox135.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox135.StyleName = "";
            this.textBox135.Value = "Model Number";
            // 
            // textBox136
            // 
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox136.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox136.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox136.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox136.StyleName = "";
            // 
            // textBox137
            // 
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox137.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox137.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox137.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox137.StyleName = "";
            // 
            // textBox138
            // 
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox138.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox138.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox138.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox138.StyleName = "";
            // 
            // textBox139
            // 
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox139.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox139.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox139.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox139.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox139.Style.Color = System.Drawing.Color.White;
            this.textBox139.Style.Font.Bold = true;
            this.textBox139.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox139.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox139.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox139.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox139.StyleName = "";
            this.textBox139.Value = "Quantity (No of Inverters)";
            // 
            // textBox140
            // 
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox140.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox140.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox140.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox140.StyleName = "";
            // 
            // textBox141
            // 
            this.textBox141.Name = "textBox141";
            this.textBox141.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox141.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox141.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox141.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox141.StyleName = "";
            // 
            // textBox142
            // 
            this.textBox142.Name = "textBox142";
            this.textBox142.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox142.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox142.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox142.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox142.StyleName = "";
            // 
            // textBox143
            // 
            this.textBox143.Name = "textBox143";
            this.textBox143.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.325D));
            this.textBox143.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox143.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox143.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox143.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox143.Style.Color = System.Drawing.Color.White;
            this.textBox143.Style.Font.Bold = true;
            this.textBox143.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox143.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox143.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox143.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox143.StyleName = "";
            this.textBox143.Value = "Serial Number (if multiple inverters of same Make & Model, then provide individua" +
    "l serial numbers on page 5)";
            // 
            // textBox144
            // 
            this.textBox144.Name = "textBox144";
            this.textBox144.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.325D));
            this.textBox144.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox144.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox144.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox144.StyleName = "";
            // 
            // textBox145
            // 
            this.textBox145.Name = "textBox145";
            this.textBox145.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.325D));
            this.textBox145.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox145.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox145.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox145.StyleName = "";
            // 
            // textBox146
            // 
            this.textBox146.Name = "textBox146";
            this.textBox146.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.325D));
            this.textBox146.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox146.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox146.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox146.StyleName = "";
            // 
            // textBox147
            // 
            this.textBox147.Name = "textBox147";
            this.textBox147.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox147.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox147.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox147.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox147.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox147.Style.Color = System.Drawing.Color.White;
            this.textBox147.Style.Font.Bold = true;
            this.textBox147.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox147.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox147.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox147.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox147.StyleName = "";
            this.textBox147.Value = "Inverter Capacity (kVA)";
            // 
            // textBox148
            // 
            this.textBox148.Name = "textBox148";
            this.textBox148.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox148.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox148.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox148.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox148.StyleName = "";
            // 
            // textBox149
            // 
            this.textBox149.Name = "textBox149";
            this.textBox149.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox149.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox149.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox149.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox149.StyleName = "";
            // 
            // textBox150
            // 
            this.textBox150.Name = "textBox150";
            this.textBox150.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox150.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox150.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox150.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox150.StyleName = "";
            // 
            // textBox151
            // 
            this.textBox151.Name = "textBox151";
            this.textBox151.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox151.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox151.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox151.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox151.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox151.Style.Color = System.Drawing.Color.White;
            this.textBox151.Style.Font.Bold = true;
            this.textBox151.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox151.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox151.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox151.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox151.StyleName = "";
            this.textBox151.Value = "Number of Phase(s) Inverter Connected to:";
            // 
            // textBox155
            // 
            this.textBox155.Name = "textBox155";
            this.textBox155.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.492D));
            this.textBox155.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox155.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox155.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox155.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox155.Style.Color = System.Drawing.Color.White;
            this.textBox155.Style.Font.Bold = true;
            this.textBox155.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox155.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox155.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox155.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox155.StyleName = "";
            this.textBox155.Value = "Volt-Var and Volt-Watt applied with UE Settings & Sustained Operation Over-Voltag" +
    "e Limit set to 258V? (Only applicable for New Inverters and Existing Inverters w" +
    "ith Volt-Var & Volt-Watt capability)";
            // 
            // textBox159
            // 
            this.textBox159.Name = "textBox159";
            this.textBox159.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox159.Style.BackgroundColor = System.Drawing.Color.MidnightBlue;
            this.textBox159.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox159.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox159.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox159.Style.Color = System.Drawing.Color.White;
            this.textBox159.Style.Font.Bold = true;
            this.textBox159.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox159.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox159.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox159.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox159.StyleName = "";
            this.textBox159.Value = "DRED Interaction Enabled?";
            // 
            // textBox167
            // 
            this.textBox167.Name = "textBox167";
            this.textBox167.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox167.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox167.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox167.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox167.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox167.Style.Color = System.Drawing.Color.White;
            this.textBox167.Style.Font.Bold = true;
            this.textBox167.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox167.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox167.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox167.StyleName = "";
            this.textBox167.Value = "GENERATION 1";
            // 
            // textBox168
            // 
            this.textBox168.Name = "textBox168";
            this.textBox168.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox168.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox168.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox168.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox168.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox168.Style.Font.Bold = true;
            this.textBox168.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox168.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox168.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox168.StyleName = "";
            this.textBox168.Value = "INVERTER 1";
            // 
            // textBox169
            // 
            this.textBox169.Name = "textBox169";
            this.textBox169.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox169.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox169.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox169.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox169.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox169.Style.Font.Bold = true;
            this.textBox169.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox169.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox169.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox169.StyleName = "";
            this.textBox169.Value = "INVERTER 2";
            // 
            // textBox170
            // 
            this.textBox170.Name = "textBox170";
            this.textBox170.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox170.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox170.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox170.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox170.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox170.Style.Font.Bold = true;
            this.textBox170.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox170.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox170.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox170.StyleName = "";
            this.textBox170.Value = "INVERTER 3";
            // 
            // textBox163
            // 
            this.textBox163.Name = "textBox163";
            this.textBox163.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(8.059D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox163.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox163.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox163.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox163.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox163.Style.Color = System.Drawing.Color.White;
            this.textBox163.Style.Font.Bold = true;
            this.textBox163.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox163.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox163.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox163.StyleName = "";
            this.textBox163.Value = "GENERATION DETAILS: MANDATORY FOR NEW / MODIFY / ADD / DECOMMISSION INSTALLATIONS" +
    "";
            // 
            // textBox164
            // 
            this.textBox164.Name = "textBox164";
            this.textBox164.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox164.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox164.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox164.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox164.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox164.Style.Color = System.Drawing.Color.White;
            this.textBox164.Style.Font.Bold = true;
            this.textBox164.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox164.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox164.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox164.StyleName = "";
            this.textBox164.Value = "Generation Type (Solar PV / Wind / Other)";
            // 
            // textBox165
            // 
            this.textBox165.Name = "textBox165";
            this.textBox165.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox165.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox165.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox165.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox165.StyleName = "";
            // 
            // textBox166
            // 
            this.textBox166.Name = "textBox166";
            this.textBox166.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox166.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox166.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox166.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox166.StyleName = "";
            // 
            // textBox171
            // 
            this.textBox171.Name = "textBox171";
            this.textBox171.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox171.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox171.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox171.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox171.StyleName = "";
            // 
            // textBox172
            // 
            this.textBox172.Name = "textBox172";
            this.textBox172.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox172.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox172.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox172.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox172.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox172.Style.Color = System.Drawing.Color.White;
            this.textBox172.Style.Font.Bold = true;
            this.textBox172.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox172.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox172.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox172.StyleName = "";
            this.textBox172.Value = "Quantity (No of Panels / Turbines etc.)";
            // 
            // textBox173
            // 
            this.textBox173.Name = "textBox173";
            this.textBox173.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox173.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox173.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox173.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox173.StyleName = "";
            // 
            // textBox174
            // 
            this.textBox174.Name = "textBox174";
            this.textBox174.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox174.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox174.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox174.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox174.StyleName = "";
            // 
            // textBox175
            // 
            this.textBox175.Name = "textBox175";
            this.textBox175.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox175.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox175.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox175.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox175.StyleName = "";
            // 
            // textBox176
            // 
            this.textBox176.Name = "textBox176";
            this.textBox176.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox176.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox176.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox176.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox176.Style.Color = System.Drawing.Color.White;
            this.textBox176.Style.Font.Bold = true;
            this.textBox176.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox176.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox176.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox176.StyleName = "";
            this.textBox176.Value = "Manufacturer";
            // 
            // textBox177
            // 
            this.textBox177.Name = "textBox177";
            this.textBox177.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox177.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox177.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox177.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox177.StyleName = "";
            // 
            // textBox178
            // 
            this.textBox178.Name = "textBox178";
            this.textBox178.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox178.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox178.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox178.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox178.StyleName = "";
            // 
            // textBox179
            // 
            this.textBox179.Name = "textBox179";
            this.textBox179.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox179.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox179.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox179.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox179.StyleName = "";
            // 
            // textBox180
            // 
            this.textBox180.Name = "textBox180";
            this.textBox180.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox180.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox180.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox180.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox180.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox180.Style.Color = System.Drawing.Color.White;
            this.textBox180.Style.Font.Bold = true;
            this.textBox180.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox180.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox180.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox180.StyleName = "";
            this.textBox180.Value = "Rated Capacity per Device (kW)";
            // 
            // textBox181
            // 
            this.textBox181.Name = "textBox181";
            this.textBox181.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox181.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox181.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox181.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox181.StyleName = "";
            // 
            // textBox182
            // 
            this.textBox182.Name = "textBox182";
            this.textBox182.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox182.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox182.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox182.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox182.StyleName = "";
            // 
            // textBox183
            // 
            this.textBox183.Name = "textBox183";
            this.textBox183.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox183.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox183.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox183.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox183.StyleName = "";
            // 
            // textBox185
            // 
            this.textBox185.Name = "textBox185";
            this.textBox185.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox185.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox185.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox185.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox185.StyleName = "";
            // 
            // textBox186
            // 
            this.textBox186.Name = "textBox186";
            this.textBox186.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox186.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox186.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox186.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox186.StyleName = "";
            // 
            // textBox187
            // 
            this.textBox187.Name = "textBox187";
            this.textBox187.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox187.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox187.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox187.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox187.StyleName = "";
            // 
            // textBox188
            // 
            this.textBox188.Name = "textBox188";
            this.textBox188.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox188.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(210)))), ((int)(((byte)(190)))));
            this.textBox188.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox188.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox188.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox188.Style.Color = System.Drawing.Color.White;
            this.textBox188.Style.Font.Bold = true;
            this.textBox188.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox188.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox188.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox188.StyleName = "";
            this.textBox188.Value = "GENERATION 2";
            // 
            // textBox192
            // 
            this.textBox192.Name = "textBox192";
            this.textBox192.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox192.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox192.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox192.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox192.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox192.Style.Color = System.Drawing.Color.White;
            this.textBox192.Style.Font.Bold = true;
            this.textBox192.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox192.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox192.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox192.StyleName = "";
            this.textBox192.Value = "Model Number";
            // 
            // textBox193
            // 
            this.textBox193.Name = "textBox193";
            this.textBox193.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox193.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox193.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox193.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox193.StyleName = "";
            // 
            // textBox194
            // 
            this.textBox194.Name = "textBox194";
            this.textBox194.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox194.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox194.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox194.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox194.StyleName = "";
            // 
            // textBox195
            // 
            this.textBox195.Name = "textBox195";
            this.textBox195.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox195.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox195.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox195.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox195.StyleName = "";
            // 
            // textBox196
            // 
            this.textBox196.Name = "textBox196";
            this.textBox196.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox196.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(128)))));
            this.textBox196.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox196.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox196.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox196.Style.Color = System.Drawing.Color.White;
            this.textBox196.Style.Font.Bold = true;
            this.textBox196.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox196.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox196.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox196.StyleName = "";
            this.textBox196.Value = "Type of Solar PV Panel (Only applicable for Solar PV Panels � Refer to Page 5)";
            // 
            // textBox197
            // 
            this.textBox197.Name = "textBox197";
            this.textBox197.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox197.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox197.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox197.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox197.StyleName = "";
            // 
            // textBox198
            // 
            this.textBox198.Name = "textBox198";
            this.textBox198.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox198.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox198.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox198.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox198.StyleName = "";
            // 
            // textBox199
            // 
            this.textBox199.Name = "textBox199";
            this.textBox199.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox199.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox199.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox199.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox199.StyleName = "";
            // 
            // textBox201
            // 
            this.textBox201.Name = "textBox201";
            this.textBox201.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox201.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox201.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox201.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox201.StyleName = "";
            // 
            // textBox202
            // 
            this.textBox202.Name = "textBox202";
            this.textBox202.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox202.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox202.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox202.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox202.StyleName = "";
            // 
            // textBox203
            // 
            this.textBox203.Name = "textBox203";
            this.textBox203.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox203.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox203.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox203.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox203.StyleName = "";
            // 
            // textBox205
            // 
            this.textBox205.Name = "textBox205";
            this.textBox205.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox205.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox205.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox205.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox205.StyleName = "";
            // 
            // textBox206
            // 
            this.textBox206.Name = "textBox206";
            this.textBox206.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox206.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox206.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox206.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox206.StyleName = "";
            // 
            // textBox207
            // 
            this.textBox207.Name = "textBox207";
            this.textBox207.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox207.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox207.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox207.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox207.StyleName = "";
            // 
            // textBox209
            // 
            this.textBox209.Name = "textBox209";
            this.textBox209.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox209.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox209.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox209.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox209.StyleName = "";
            // 
            // textBox210
            // 
            this.textBox210.Name = "textBox210";
            this.textBox210.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox210.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox210.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox210.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox210.StyleName = "";
            // 
            // textBox211
            // 
            this.textBox211.Name = "textBox211";
            this.textBox211.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox211.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox211.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox211.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox211.StyleName = "";
            // 
            // textBox213
            // 
            this.textBox213.Name = "textBox213";
            this.textBox213.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox213.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox213.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox213.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox213.StyleName = "";
            // 
            // textBox214
            // 
            this.textBox214.Name = "textBox214";
            this.textBox214.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox214.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox214.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox214.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox214.StyleName = "";
            // 
            // textBox215
            // 
            this.textBox215.Name = "textBox215";
            this.textBox215.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox215.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox215.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox215.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox215.StyleName = "";
            // 
            // textBox217
            // 
            this.textBox217.Name = "textBox217";
            this.textBox217.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox217.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox217.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox217.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox217.StyleName = "";
            // 
            // textBox218
            // 
            this.textBox218.Name = "textBox218";
            this.textBox218.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox218.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox218.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox218.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox218.StyleName = "";
            // 
            // textBox219
            // 
            this.textBox219.Name = "textBox219";
            this.textBox219.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox219.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox219.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox219.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox219.StyleName = "";
            // 
            // textBox220
            // 
            this.textBox220.Name = "textBox220";
            this.textBox220.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox220.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(210)))), ((int)(((byte)(190)))));
            this.textBox220.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox220.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox220.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox220.Style.Color = System.Drawing.Color.White;
            this.textBox220.Style.Font.Bold = true;
            this.textBox220.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox220.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox220.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox220.StyleName = "";
            this.textBox220.Value = "Generation Type (Solar PV / Wind / Other)";
            // 
            // textBox184
            // 
            this.textBox184.Name = "textBox184";
            this.textBox184.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox184.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(210)))), ((int)(((byte)(190)))));
            this.textBox184.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox184.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox184.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox184.Style.Color = System.Drawing.Color.White;
            this.textBox184.Style.Font.Bold = true;
            this.textBox184.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox184.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox184.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox184.StyleName = "";
            this.textBox184.Value = "Quantity (No of Panels / Turbines etc.)";
            // 
            // textBox216
            // 
            this.textBox216.Name = "textBox216";
            this.textBox216.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox216.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(210)))), ((int)(((byte)(190)))));
            this.textBox216.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox216.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox216.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox216.Style.Color = System.Drawing.Color.White;
            this.textBox216.Style.Font.Bold = true;
            this.textBox216.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox216.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox216.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox216.StyleName = "";
            this.textBox216.Value = "Type of Solar PV Panel (Only applicable for Solar PV Panels � Refer to Page 5)";
            // 
            // textBox212
            // 
            this.textBox212.Name = "textBox212";
            this.textBox212.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox212.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(210)))), ((int)(((byte)(190)))));
            this.textBox212.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox212.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox212.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox212.Style.Color = System.Drawing.Color.White;
            this.textBox212.Style.Font.Bold = true;
            this.textBox212.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox212.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox212.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox212.StyleName = "";
            this.textBox212.Value = "Manufacturer";
            // 
            // textBox208
            // 
            this.textBox208.Name = "textBox208";
            this.textBox208.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox208.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(210)))), ((int)(((byte)(190)))));
            this.textBox208.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox208.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox208.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox208.Style.Color = System.Drawing.Color.White;
            this.textBox208.Style.Font.Bold = true;
            this.textBox208.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox208.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox208.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox208.StyleName = "";
            this.textBox208.Value = "Model Number";
            // 
            // textBox204
            // 
            this.textBox204.Name = "textBox204";
            this.textBox204.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(4.735D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox204.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(210)))), ((int)(((byte)(190)))));
            this.textBox204.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox204.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox204.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox204.Style.Color = System.Drawing.Color.White;
            this.textBox204.Style.Font.Bold = true;
            this.textBox204.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox204.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox204.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox204.StyleName = "";
            this.textBox204.Value = "Rated Capacity per Device (kW)";
            // 
            // textBox200
            // 
            this.textBox200.Name = "textBox200";
            this.textBox200.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox200.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox200.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox200.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox200.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox200.Style.Font.Bold = true;
            this.textBox200.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox200.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox200.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox200.StyleName = "";
            this.textBox200.Value = "INVERTER 1";
            // 
            // textBox189
            // 
            this.textBox189.Name = "textBox189";
            this.textBox189.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox189.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox189.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox189.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox189.Style.Font.Bold = true;
            this.textBox189.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox189.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox189.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox189.StyleName = "";
            this.textBox189.Value = "INVERTER 2";
            // 
            // textBox190
            // 
            this.textBox190.Name = "textBox190";
            this.textBox190.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox190.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox190.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox190.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox190.Style.Font.Bold = true;
            this.textBox190.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox190.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox190.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox190.StyleName = "";
            this.textBox190.Value = "INVERTER 3";
            // 
            // textBox191
            // 
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox191.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox191.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox191.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox191.Style.Font.Bold = true;
            this.textBox191.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox191.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox191.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox191.StyleName = "";
            this.textBox191.Value = "INVERTER 1";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox76.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox76.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox76.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox76.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox76.Style.Font.Bold = true;
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox76.StyleName = "";
            this.textBox76.Value = "INVERTER 2";
            // 
            // textBox120
            // 
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.textBox120.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.textBox120.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox120.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox120.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox120.Style.Font.Bold = true;
            this.textBox120.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox120.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox120.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox120.StyleName = "";
            this.textBox120.Value = "INVERTER 3";
            // 
            // table17
            // 
            this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.363D)));
            this.table17.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table17.Body.SetCellContent(0, 0, this.checkBox6);
            this.table17.Body.SetCellContent(0, 1, this.checkBox11);
            this.table17.Body.SetCellContent(0, 2, this.checkBox20);
            tableGroup312.Name = "tableGroup7";
            tableGroup313.Name = "tableGroup9";
            tableGroup314.Name = "group25";
            this.table17.ColumnGroups.Add(tableGroup312);
            this.table17.ColumnGroups.Add(tableGroup313);
            this.table17.ColumnGroups.Add(tableGroup314);
            this.table17.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox6,
            this.checkBox11,
            this.checkBox20});
            this.table17.Name = "table17";
            tableGroup315.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup315.Name = "detailTableGroup3";
            this.table17.RowGroups.Add(tableGroup315);
            this.table17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.109D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table17.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table17.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table17.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table17.Style.Color = System.Drawing.Color.MidnightBlue;
            // 
            // checkBox6
            // 
            this.checkBox6.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox6.CheckedImage = ((object)(resources.GetObject("checkBox6.CheckedImage")));
            this.checkBox6.IndeterminateImage = ((object)(resources.GetObject("checkBox6.IndeterminateImage")));
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox6.StyleName = "";
            this.checkBox6.Text = " 1";
            this.checkBox6.UncheckedImage = ((object)(resources.GetObject("checkBox6.UncheckedImage")));
            this.checkBox6.Value = "";
            // 
            // checkBox11
            // 
            this.checkBox11.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox11.CheckedImage = ((object)(resources.GetObject("checkBox11.CheckedImage")));
            this.checkBox11.IndeterminateImage = ((object)(resources.GetObject("checkBox11.IndeterminateImage")));
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox11.StyleName = "";
            this.checkBox11.Text = " 2";
            this.checkBox11.UncheckedImage = ((object)(resources.GetObject("checkBox11.UncheckedImage")));
            this.checkBox11.Value = "";
            // 
            // checkBox20
            // 
            this.checkBox20.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox20.CheckedImage = ((object)(resources.GetObject("checkBox20.CheckedImage")));
            this.checkBox20.IndeterminateImage = ((object)(resources.GetObject("checkBox20.IndeterminateImage")));
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.363D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox20.StyleName = "";
            this.checkBox20.Text = " 3";
            this.checkBox20.UncheckedImage = ((object)(resources.GetObject("checkBox20.UncheckedImage")));
            this.checkBox20.Value = "";
            // 
            // table18
            // 
            this.table18.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table18.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table18.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.363D)));
            this.table18.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table18.Body.SetCellContent(0, 0, this.checkBox21);
            this.table18.Body.SetCellContent(0, 1, this.checkBox37);
            this.table18.Body.SetCellContent(0, 2, this.checkBox38);
            tableGroup316.Name = "tableGroup7";
            tableGroup317.Name = "tableGroup9";
            tableGroup318.Name = "group25";
            this.table18.ColumnGroups.Add(tableGroup316);
            this.table18.ColumnGroups.Add(tableGroup317);
            this.table18.ColumnGroups.Add(tableGroup318);
            this.table18.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox21,
            this.checkBox37,
            this.checkBox38});
            this.table18.Name = "table18";
            tableGroup319.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup319.Name = "detailTableGroup3";
            this.table18.RowGroups.Add(tableGroup319);
            this.table18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.109D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table18.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table18.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table18.Style.Color = System.Drawing.Color.MidnightBlue;
            // 
            // checkBox21
            // 
            this.checkBox21.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox21.CheckedImage = ((object)(resources.GetObject("checkBox21.CheckedImage")));
            this.checkBox21.IndeterminateImage = ((object)(resources.GetObject("checkBox21.IndeterminateImage")));
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox21.StyleName = "";
            this.checkBox21.Text = " 1";
            this.checkBox21.UncheckedImage = ((object)(resources.GetObject("checkBox21.UncheckedImage")));
            this.checkBox21.Value = "";
            // 
            // checkBox37
            // 
            this.checkBox37.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox37.CheckedImage = ((object)(resources.GetObject("checkBox37.CheckedImage")));
            this.checkBox37.IndeterminateImage = ((object)(resources.GetObject("checkBox37.IndeterminateImage")));
            this.checkBox37.Name = "checkBox37";
            this.checkBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox37.StyleName = "";
            this.checkBox37.Text = " 2";
            this.checkBox37.UncheckedImage = ((object)(resources.GetObject("checkBox37.UncheckedImage")));
            this.checkBox37.Value = "";
            // 
            // checkBox38
            // 
            this.checkBox38.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox38.CheckedImage = ((object)(resources.GetObject("checkBox38.CheckedImage")));
            this.checkBox38.IndeterminateImage = ((object)(resources.GetObject("checkBox38.IndeterminateImage")));
            this.checkBox38.Name = "checkBox38";
            this.checkBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.363D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox38.StyleName = "";
            this.checkBox38.Text = " 3";
            this.checkBox38.UncheckedImage = ((object)(resources.GetObject("checkBox38.UncheckedImage")));
            this.checkBox38.Value = "";
            // 
            // table19
            // 
            this.table19.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table19.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.373D)));
            this.table19.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.363D)));
            this.table19.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table19.Body.SetCellContent(0, 0, this.checkBox39);
            this.table19.Body.SetCellContent(0, 1, this.checkBox40);
            this.table19.Body.SetCellContent(0, 2, this.checkBox41);
            tableGroup320.Name = "tableGroup7";
            tableGroup321.Name = "tableGroup9";
            tableGroup322.Name = "group25";
            this.table19.ColumnGroups.Add(tableGroup320);
            this.table19.ColumnGroups.Add(tableGroup321);
            this.table19.ColumnGroups.Add(tableGroup322);
            this.table19.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox39,
            this.checkBox40,
            this.checkBox41});
            this.table19.Name = "table19";
            tableGroup323.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup323.Name = "detailTableGroup3";
            this.table19.RowGroups.Add(tableGroup323);
            this.table19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.109D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table19.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table19.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table19.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table19.Style.Color = System.Drawing.Color.MidnightBlue;
            // 
            // checkBox39
            // 
            this.checkBox39.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox39.CheckedImage = ((object)(resources.GetObject("checkBox39.CheckedImage")));
            this.checkBox39.IndeterminateImage = ((object)(resources.GetObject("checkBox39.IndeterminateImage")));
            this.checkBox39.Name = "checkBox39";
            this.checkBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox39.StyleName = "";
            this.checkBox39.Text = " 1";
            this.checkBox39.UncheckedImage = ((object)(resources.GetObject("checkBox39.UncheckedImage")));
            this.checkBox39.Value = "";
            // 
            // checkBox40
            // 
            this.checkBox40.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox40.CheckedImage = ((object)(resources.GetObject("checkBox40.CheckedImage")));
            this.checkBox40.IndeterminateImage = ((object)(resources.GetObject("checkBox40.IndeterminateImage")));
            this.checkBox40.Name = "checkBox40";
            this.checkBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.373D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox40.StyleName = "";
            this.checkBox40.Text = " 2";
            this.checkBox40.UncheckedImage = ((object)(resources.GetObject("checkBox40.UncheckedImage")));
            this.checkBox40.Value = "";
            // 
            // checkBox41
            // 
            this.checkBox41.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox41.CheckedImage = ((object)(resources.GetObject("checkBox41.CheckedImage")));
            this.checkBox41.IndeterminateImage = ((object)(resources.GetObject("checkBox41.IndeterminateImage")));
            this.checkBox41.Name = "checkBox41";
            this.checkBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.363D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox41.StyleName = "";
            this.checkBox41.Text = " 3";
            this.checkBox41.UncheckedImage = ((object)(resources.GetObject("checkBox41.UncheckedImage")));
            this.checkBox41.Value = "";
            // 
            // table23
            // 
            this.table23.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.541D)));
            this.table23.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.567D)));
            this.table23.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.492D)));
            this.table23.Body.SetCellContent(0, 0, this.checkBox42);
            this.table23.Body.SetCellContent(0, 1, this.checkBox55);
            tableGroup324.Name = "tableGroup7";
            tableGroup325.Name = "tableGroup9";
            this.table23.ColumnGroups.Add(tableGroup324);
            this.table23.ColumnGroups.Add(tableGroup325);
            this.table23.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox42,
            this.checkBox55});
            this.table23.Name = "table23";
            tableGroup326.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup326.Name = "detailTableGroup3";
            this.table23.RowGroups.Add(tableGroup326);
            this.table23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.492D));
            this.table23.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table23.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table23.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table23.Style.Color = System.Drawing.Color.MidnightBlue;
            // 
            // checkBox42
            // 
            this.checkBox42.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox42.CheckedImage = ((object)(resources.GetObject("checkBox42.CheckedImage")));
            this.checkBox42.IndeterminateImage = ((object)(resources.GetObject("checkBox42.IndeterminateImage")));
            this.checkBox42.Name = "checkBox42";
            this.checkBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.541D), Telerik.Reporting.Drawing.Unit.Inch(0.492D));
            this.checkBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox42.StyleName = "";
            this.checkBox42.Text = " Yes";
            this.checkBox42.UncheckedImage = ((object)(resources.GetObject("checkBox42.UncheckedImage")));
            this.checkBox42.Value = "";
            // 
            // checkBox55
            // 
            this.checkBox55.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox55.CheckedImage = ((object)(resources.GetObject("checkBox55.CheckedImage")));
            this.checkBox55.IndeterminateImage = ((object)(resources.GetObject("checkBox55.IndeterminateImage")));
            this.checkBox55.Name = "checkBox55";
            this.checkBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.567D), Telerik.Reporting.Drawing.Unit.Inch(0.492D));
            this.checkBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox55.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox55.StyleName = "";
            this.checkBox55.Text = " No";
            this.checkBox55.UncheckedImage = ((object)(resources.GetObject("checkBox55.UncheckedImage")));
            this.checkBox55.Value = "";
            // 
            // table27
            // 
            this.table27.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.541D)));
            this.table27.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.567D)));
            this.table27.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.492D)));
            this.table27.Body.SetCellContent(0, 0, this.checkBox56);
            this.table27.Body.SetCellContent(0, 1, this.checkBox57);
            tableGroup327.Name = "tableGroup7";
            tableGroup328.Name = "tableGroup9";
            this.table27.ColumnGroups.Add(tableGroup327);
            this.table27.ColumnGroups.Add(tableGroup328);
            this.table27.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox56,
            this.checkBox57});
            this.table27.Name = "table27";
            tableGroup329.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup329.Name = "detailTableGroup3";
            this.table27.RowGroups.Add(tableGroup329);
            this.table27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.492D));
            this.table27.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table27.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table27.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table27.Style.Color = System.Drawing.Color.MidnightBlue;
            // 
            // checkBox56
            // 
            this.checkBox56.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox56.CheckedImage = ((object)(resources.GetObject("checkBox56.CheckedImage")));
            this.checkBox56.IndeterminateImage = ((object)(resources.GetObject("checkBox56.IndeterminateImage")));
            this.checkBox56.Name = "checkBox56";
            this.checkBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.541D), Telerik.Reporting.Drawing.Unit.Inch(0.492D));
            this.checkBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox56.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox56.StyleName = "";
            this.checkBox56.Text = " Yes";
            this.checkBox56.UncheckedImage = ((object)(resources.GetObject("checkBox56.UncheckedImage")));
            this.checkBox56.Value = "";
            // 
            // checkBox57
            // 
            this.checkBox57.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox57.CheckedImage = ((object)(resources.GetObject("checkBox57.CheckedImage")));
            this.checkBox57.IndeterminateImage = ((object)(resources.GetObject("checkBox57.IndeterminateImage")));
            this.checkBox57.Name = "checkBox57";
            this.checkBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.567D), Telerik.Reporting.Drawing.Unit.Inch(0.492D));
            this.checkBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox57.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox57.StyleName = "";
            this.checkBox57.Text = " No";
            this.checkBox57.UncheckedImage = ((object)(resources.GetObject("checkBox57.UncheckedImage")));
            this.checkBox57.Value = "";
            // 
            // table43
            // 
            this.table43.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.541D)));
            this.table43.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.567D)));
            this.table43.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.492D)));
            this.table43.Body.SetCellContent(0, 0, this.checkBox58);
            this.table43.Body.SetCellContent(0, 1, this.checkBox59);
            tableGroup330.Name = "tableGroup7";
            tableGroup331.Name = "tableGroup9";
            this.table43.ColumnGroups.Add(tableGroup330);
            this.table43.ColumnGroups.Add(tableGroup331);
            this.table43.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox58,
            this.checkBox59});
            this.table43.Name = "table43";
            tableGroup332.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup332.Name = "detailTableGroup3";
            this.table43.RowGroups.Add(tableGroup332);
            this.table43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.492D));
            this.table43.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table43.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table43.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table43.Style.Color = System.Drawing.Color.MidnightBlue;
            // 
            // checkBox58
            // 
            this.checkBox58.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox58.CheckedImage = ((object)(resources.GetObject("checkBox58.CheckedImage")));
            this.checkBox58.IndeterminateImage = ((object)(resources.GetObject("checkBox58.IndeterminateImage")));
            this.checkBox58.Name = "checkBox58";
            this.checkBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.541D), Telerik.Reporting.Drawing.Unit.Inch(0.492D));
            this.checkBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox58.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox58.StyleName = "";
            this.checkBox58.Text = " Yes";
            this.checkBox58.UncheckedImage = ((object)(resources.GetObject("checkBox58.UncheckedImage")));
            this.checkBox58.Value = "";
            // 
            // checkBox59
            // 
            this.checkBox59.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox59.CheckedImage = ((object)(resources.GetObject("checkBox59.CheckedImage")));
            this.checkBox59.IndeterminateImage = ((object)(resources.GetObject("checkBox59.IndeterminateImage")));
            this.checkBox59.Name = "checkBox59";
            this.checkBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.567D), Telerik.Reporting.Drawing.Unit.Inch(0.492D));
            this.checkBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox59.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox59.StyleName = "";
            this.checkBox59.Text = " No";
            this.checkBox59.UncheckedImage = ((object)(resources.GetObject("checkBox59.UncheckedImage")));
            this.checkBox59.Value = "";
            // 
            // table56
            // 
            this.table56.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.541D)));
            this.table56.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.567D)));
            this.table56.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table56.Body.SetCellContent(0, 0, this.checkBox60);
            this.table56.Body.SetCellContent(0, 1, this.checkBox61);
            tableGroup333.Name = "tableGroup7";
            tableGroup334.Name = "tableGroup9";
            this.table56.ColumnGroups.Add(tableGroup333);
            this.table56.ColumnGroups.Add(tableGroup334);
            this.table56.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox60,
            this.checkBox61});
            this.table56.Name = "table56";
            tableGroup335.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup335.Name = "detailTableGroup3";
            this.table56.RowGroups.Add(tableGroup335);
            this.table56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table56.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table56.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table56.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table56.Style.Color = System.Drawing.Color.MidnightBlue;
            // 
            // checkBox60
            // 
            this.checkBox60.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox60.CheckedImage = ((object)(resources.GetObject("checkBox60.CheckedImage")));
            this.checkBox60.IndeterminateImage = ((object)(resources.GetObject("checkBox60.IndeterminateImage")));
            this.checkBox60.Name = "checkBox60";
            this.checkBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.541D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox60.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox60.StyleName = "";
            this.checkBox60.Text = " Yes";
            this.checkBox60.UncheckedImage = ((object)(resources.GetObject("checkBox60.UncheckedImage")));
            this.checkBox60.Value = "";
            // 
            // checkBox61
            // 
            this.checkBox61.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox61.CheckedImage = ((object)(resources.GetObject("checkBox61.CheckedImage")));
            this.checkBox61.IndeterminateImage = ((object)(resources.GetObject("checkBox61.IndeterminateImage")));
            this.checkBox61.Name = "checkBox61";
            this.checkBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.567D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox61.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox61.StyleName = "";
            this.checkBox61.Text = " No";
            this.checkBox61.UncheckedImage = ((object)(resources.GetObject("checkBox61.UncheckedImage")));
            this.checkBox61.Value = "";
            // 
            // table57
            // 
            this.table57.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.541D)));
            this.table57.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.567D)));
            this.table57.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table57.Body.SetCellContent(0, 0, this.checkBox62);
            this.table57.Body.SetCellContent(0, 1, this.checkBox63);
            tableGroup336.Name = "tableGroup7";
            tableGroup337.Name = "tableGroup9";
            this.table57.ColumnGroups.Add(tableGroup336);
            this.table57.ColumnGroups.Add(tableGroup337);
            this.table57.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox62,
            this.checkBox63});
            this.table57.Name = "table57";
            tableGroup338.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup338.Name = "detailTableGroup3";
            this.table57.RowGroups.Add(tableGroup338);
            this.table57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table57.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table57.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table57.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table57.Style.Color = System.Drawing.Color.MidnightBlue;
            // 
            // checkBox62
            // 
            this.checkBox62.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox62.CheckedImage = ((object)(resources.GetObject("checkBox62.CheckedImage")));
            this.checkBox62.IndeterminateImage = ((object)(resources.GetObject("checkBox62.IndeterminateImage")));
            this.checkBox62.Name = "checkBox62";
            this.checkBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.541D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox62.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox62.StyleName = "";
            this.checkBox62.Text = " Yes";
            this.checkBox62.UncheckedImage = ((object)(resources.GetObject("checkBox62.UncheckedImage")));
            this.checkBox62.Value = "";
            // 
            // checkBox63
            // 
            this.checkBox63.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox63.CheckedImage = ((object)(resources.GetObject("checkBox63.CheckedImage")));
            this.checkBox63.IndeterminateImage = ((object)(resources.GetObject("checkBox63.IndeterminateImage")));
            this.checkBox63.Name = "checkBox63";
            this.checkBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.567D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox63.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox63.StyleName = "";
            this.checkBox63.Text = " No";
            this.checkBox63.UncheckedImage = ((object)(resources.GetObject("checkBox63.UncheckedImage")));
            this.checkBox63.Value = "";
            // 
            // table58
            // 
            this.table58.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.541D)));
            this.table58.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.567D)));
            this.table58.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table58.Body.SetCellContent(0, 0, this.checkBox64);
            this.table58.Body.SetCellContent(0, 1, this.checkBox65);
            tableGroup339.Name = "tableGroup7";
            tableGroup340.Name = "tableGroup9";
            this.table58.ColumnGroups.Add(tableGroup339);
            this.table58.ColumnGroups.Add(tableGroup340);
            this.table58.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox64,
            this.checkBox65});
            this.table58.Name = "table58";
            tableGroup341.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup341.Name = "detailTableGroup3";
            this.table58.RowGroups.Add(tableGroup341);
            this.table58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.108D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table58.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table58.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table58.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table58.Style.Color = System.Drawing.Color.MidnightBlue;
            // 
            // checkBox64
            // 
            this.checkBox64.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox64.CheckedImage = ((object)(resources.GetObject("checkBox64.CheckedImage")));
            this.checkBox64.IndeterminateImage = ((object)(resources.GetObject("checkBox64.IndeterminateImage")));
            this.checkBox64.Name = "checkBox64";
            this.checkBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.541D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox64.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox64.StyleName = "";
            this.checkBox64.Text = " Yes";
            this.checkBox64.UncheckedImage = ((object)(resources.GetObject("checkBox64.UncheckedImage")));
            this.checkBox64.Value = "";
            // 
            // checkBox65
            // 
            this.checkBox65.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox65.CheckedImage = ((object)(resources.GetObject("checkBox65.CheckedImage")));
            this.checkBox65.IndeterminateImage = ((object)(resources.GetObject("checkBox65.IndeterminateImage")));
            this.checkBox65.Name = "checkBox65";
            this.checkBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.567D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox65.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox65.StyleName = "";
            this.checkBox65.Text = " No";
            this.checkBox65.UncheckedImage = ((object)(resources.GetObject("checkBox65.UncheckedImage")));
            this.checkBox65.Value = "";
            // 
            // pageHeaderSection1
            // 
            this.pageHeaderSection1.Height = Telerik.Reporting.Drawing.Unit.Inch(1.2D);
            this.pageHeaderSection1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox2,
            this.textBox67});
            this.pageHeaderSection1.Name = "pageHeaderSection1";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.3D), Telerik.Reporting.Drawing.Unit.Inch(0.1D));
            this.pictureBox2.MimeType = "image/jpeg";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.657D), Telerik.Reporting.Drawing.Unit.Inch(1D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // textBox67
            // 
            this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.762D), Telerik.Reporting.Drawing.Unit.Cm(1.27D));
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.922D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
            this.textBox67.Style.Color = System.Drawing.Color.MidnightBlue;
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11.5D);
            this.textBox67.Value = "United Energy Inverter - Basic Micro Embedded Generator Connection Form";
            // 
            // UnitedEnergyInverter_new
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail,
            this.pageHeaderSection1});
            this.Name = "UnitedEnergyInverter_new";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.CheckBox checkBox7;
        private Telerik.Reporting.CheckBox checkBox8;
        private Telerik.Reporting.CheckBox checkBox9;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.CheckBox checkBox10;
        private Telerik.Reporting.CheckBox checkBox12;
        private Telerik.Reporting.CheckBox checkBox13;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.CheckBox checkBox14;
        private Telerik.Reporting.CheckBox checkBox15;
        private Telerik.Reporting.CheckBox checkBox16;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.CheckBox checkBox17;
        private Telerik.Reporting.CheckBox checkBox18;
        private Telerik.Reporting.CheckBox checkBox19;
        private Telerik.Reporting.Table table10;
        private Telerik.Reporting.CheckBox checkBox22;
        private Telerik.Reporting.CheckBox checkBox23;
        private Telerik.Reporting.Table table11;
        private Telerik.Reporting.CheckBox checkBox24;
        private Telerik.Reporting.CheckBox checkBox25;
        private Telerik.Reporting.Table table12;
        private Telerik.Reporting.CheckBox checkBox26;
        private Telerik.Reporting.CheckBox checkBox27;
        private Telerik.Reporting.Table table13;
        private Telerik.Reporting.CheckBox checkBox28;
        private Telerik.Reporting.CheckBox checkBox29;
        private Telerik.Reporting.Table table14;
        private Telerik.Reporting.CheckBox checkBox30;
        private Telerik.Reporting.CheckBox checkBox31;
        private Telerik.Reporting.Table table15;
        private Telerik.Reporting.CheckBox checkBox32;
        private Telerik.Reporting.CheckBox checkBox33;
        private Telerik.Reporting.Table table16;
        private Telerik.Reporting.CheckBox checkBox34;
        private Telerik.Reporting.CheckBox checkBox35;
        private Telerik.Reporting.CheckBox checkBox1;
        private Telerik.Reporting.CheckBox checkBox36;
        private Telerik.Reporting.CheckBox checkBox2;
        private Telerik.Reporting.CheckBox checkBox3;
        private Telerik.Reporting.Table table29;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.Table table38;
        private Telerik.Reporting.CheckBox checkBox70;
        private Telerik.Reporting.CheckBox checkBox71;
        private Telerik.Reporting.Table table39;
        private Telerik.Reporting.CheckBox checkBox72;
        private Telerik.Reporting.CheckBox checkBox73;
        private Telerik.Reporting.Table table40;
        private Telerik.Reporting.CheckBox checkBox74;
        private Telerik.Reporting.CheckBox checkBox75;
        private Telerik.Reporting.Table table44;
        private Telerik.Reporting.CheckBox checkBox76;
        private Telerik.Reporting.CheckBox checkBox80;
        private Telerik.Reporting.CheckBox checkBox81;
        private Telerik.Reporting.CheckBox checkBox82;
        private Telerik.Reporting.Table table45;
        private Telerik.Reporting.CheckBox checkBox77;
        private Telerik.Reporting.CheckBox checkBox83;
        private Telerik.Reporting.CheckBox checkBox84;
        private Telerik.Reporting.CheckBox checkBox85;
        private Telerik.Reporting.Table table46;
        private Telerik.Reporting.CheckBox checkBox78;
        private Telerik.Reporting.CheckBox checkBox86;
        private Telerik.Reporting.CheckBox checkBox87;
        private Telerik.Reporting.CheckBox checkBox88;
        private Telerik.Reporting.Table table47;
        private Telerik.Reporting.CheckBox checkBox79;
        private Telerik.Reporting.CheckBox checkBox89;
        private Telerik.Reporting.CheckBox checkBox90;
        private Telerik.Reporting.CheckBox checkBox91;
        private Telerik.Reporting.Table table30;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.Table table31;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.Table table32;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.Table table33;
        private Telerik.Reporting.TextBox textBox106;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.Table table34;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.Table table35;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.TextBox textBox112;
        private Telerik.Reporting.Table table36;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.TextBox textBox114;
        private Telerik.Reporting.Table table37;
        private Telerik.Reporting.TextBox textBox115;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.Table table48;
        private Telerik.Reporting.TextBox textBox121;
        private Telerik.Reporting.TextBox textBox122;
        private Telerik.Reporting.TextBox textBox123;
        private Telerik.Reporting.TextBox textBox124;
        private Telerik.Reporting.Table table49;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox125;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.Table table50;
        private Telerik.Reporting.TextBox textBox118;
        private Telerik.Reporting.TextBox textBox128;
        private Telerik.Reporting.TextBox textBox129;
        private Telerik.Reporting.TextBox textBox130;
        private Telerik.Reporting.Table table51;
        private Telerik.Reporting.TextBox textBox119;
        private Telerik.Reporting.TextBox textBox131;
        private Telerik.Reporting.TextBox textBox132;
        private Telerik.Reporting.TextBox textBox133;
        private Telerik.Reporting.Table table52;
        private Telerik.Reporting.CheckBox checkBox4;
        private Telerik.Reporting.CheckBox checkBox5;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.Table table20;
        private Telerik.Reporting.CheckBox checkBox43;
        private Telerik.Reporting.CheckBox checkBox44;
        private Telerik.Reporting.CheckBox checkBox45;
        private Telerik.Reporting.CheckBox checkBox46;
        private Telerik.Reporting.Table table21;
        private Telerik.Reporting.CheckBox checkBox47;
        private Telerik.Reporting.CheckBox checkBox48;
        private Telerik.Reporting.CheckBox checkBox49;
        private Telerik.Reporting.CheckBox checkBox50;
        private Telerik.Reporting.Table table22;
        private Telerik.Reporting.CheckBox checkBox51;
        private Telerik.Reporting.CheckBox checkBox52;
        private Telerik.Reporting.CheckBox checkBox53;
        private Telerik.Reporting.CheckBox checkBox54;
        private Telerik.Reporting.Table table24;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.Table table25;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.Table table26;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.Table table28;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.Table table41;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.Table table42;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.Table table53;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.Table table54;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.Table table55;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.HtmlTextBox htmlTextBox2;
        private Telerik.Reporting.Table table9;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.PageHeaderSection pageHeaderSection1;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox134;
        private Telerik.Reporting.TextBox textBox135;
        private Telerik.Reporting.TextBox textBox136;
        private Telerik.Reporting.TextBox textBox137;
        private Telerik.Reporting.TextBox textBox138;
        private Telerik.Reporting.TextBox textBox139;
        private Telerik.Reporting.TextBox textBox140;
        private Telerik.Reporting.TextBox textBox141;
        private Telerik.Reporting.TextBox textBox142;
        private Telerik.Reporting.TextBox textBox143;
        private Telerik.Reporting.TextBox textBox144;
        private Telerik.Reporting.TextBox textBox145;
        private Telerik.Reporting.TextBox textBox146;
        private Telerik.Reporting.TextBox textBox147;
        private Telerik.Reporting.TextBox textBox148;
        private Telerik.Reporting.TextBox textBox149;
        private Telerik.Reporting.TextBox textBox150;
        private Telerik.Reporting.TextBox textBox151;
        private Telerik.Reporting.TextBox textBox155;
        private Telerik.Reporting.TextBox textBox159;
        private Telerik.Reporting.TextBox textBox167;
        private Telerik.Reporting.TextBox textBox168;
        private Telerik.Reporting.TextBox textBox169;
        private Telerik.Reporting.TextBox textBox170;
        private Telerik.Reporting.TextBox textBox163;
        private Telerik.Reporting.TextBox textBox164;
        private Telerik.Reporting.TextBox textBox165;
        private Telerik.Reporting.TextBox textBox166;
        private Telerik.Reporting.TextBox textBox171;
        private Telerik.Reporting.TextBox textBox172;
        private Telerik.Reporting.TextBox textBox173;
        private Telerik.Reporting.TextBox textBox174;
        private Telerik.Reporting.TextBox textBox175;
        private Telerik.Reporting.TextBox textBox176;
        private Telerik.Reporting.TextBox textBox177;
        private Telerik.Reporting.TextBox textBox178;
        private Telerik.Reporting.TextBox textBox179;
        private Telerik.Reporting.TextBox textBox180;
        private Telerik.Reporting.TextBox textBox181;
        private Telerik.Reporting.TextBox textBox182;
        private Telerik.Reporting.TextBox textBox183;
        private Telerik.Reporting.TextBox textBox185;
        private Telerik.Reporting.TextBox textBox186;
        private Telerik.Reporting.TextBox textBox187;
        private Telerik.Reporting.TextBox textBox188;
        private Telerik.Reporting.TextBox textBox192;
        private Telerik.Reporting.TextBox textBox193;
        private Telerik.Reporting.TextBox textBox194;
        private Telerik.Reporting.TextBox textBox195;
        private Telerik.Reporting.TextBox textBox196;
        private Telerik.Reporting.TextBox textBox197;
        private Telerik.Reporting.TextBox textBox198;
        private Telerik.Reporting.TextBox textBox199;
        private Telerik.Reporting.TextBox textBox201;
        private Telerik.Reporting.TextBox textBox202;
        private Telerik.Reporting.TextBox textBox203;
        private Telerik.Reporting.TextBox textBox205;
        private Telerik.Reporting.TextBox textBox206;
        private Telerik.Reporting.TextBox textBox207;
        private Telerik.Reporting.TextBox textBox209;
        private Telerik.Reporting.TextBox textBox210;
        private Telerik.Reporting.TextBox textBox211;
        private Telerik.Reporting.TextBox textBox213;
        private Telerik.Reporting.TextBox textBox214;
        private Telerik.Reporting.TextBox textBox215;
        private Telerik.Reporting.TextBox textBox217;
        private Telerik.Reporting.TextBox textBox218;
        private Telerik.Reporting.TextBox textBox219;
        private Telerik.Reporting.TextBox textBox220;
        private Telerik.Reporting.TextBox textBox184;
        private Telerik.Reporting.TextBox textBox216;
        private Telerik.Reporting.TextBox textBox212;
        private Telerik.Reporting.TextBox textBox208;
        private Telerik.Reporting.TextBox textBox204;
        private Telerik.Reporting.TextBox textBox200;
        private Telerik.Reporting.TextBox textBox189;
        private Telerik.Reporting.TextBox textBox190;
        private Telerik.Reporting.TextBox textBox191;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox120;
        private Telerik.Reporting.Table table17;
        private Telerik.Reporting.CheckBox checkBox6;
        private Telerik.Reporting.CheckBox checkBox11;
        private Telerik.Reporting.CheckBox checkBox20;
        private Telerik.Reporting.Table table18;
        private Telerik.Reporting.CheckBox checkBox21;
        private Telerik.Reporting.CheckBox checkBox37;
        private Telerik.Reporting.CheckBox checkBox38;
        private Telerik.Reporting.Table table19;
        private Telerik.Reporting.CheckBox checkBox39;
        private Telerik.Reporting.CheckBox checkBox40;
        private Telerik.Reporting.CheckBox checkBox41;
        private Telerik.Reporting.Table table23;
        private Telerik.Reporting.CheckBox checkBox42;
        private Telerik.Reporting.CheckBox checkBox55;
        private Telerik.Reporting.Table table27;
        private Telerik.Reporting.CheckBox checkBox56;
        private Telerik.Reporting.CheckBox checkBox57;
        private Telerik.Reporting.Table table43;
        private Telerik.Reporting.CheckBox checkBox58;
        private Telerik.Reporting.CheckBox checkBox59;
        private Telerik.Reporting.Table table56;
        private Telerik.Reporting.CheckBox checkBox60;
        private Telerik.Reporting.CheckBox checkBox61;
        private Telerik.Reporting.Table table57;
        private Telerik.Reporting.CheckBox checkBox62;
        private Telerik.Reporting.CheckBox checkBox63;
        private Telerik.Reporting.Table table58;
        private Telerik.Reporting.CheckBox checkBox64;
        private Telerik.Reporting.CheckBox checkBox65;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.Table table59;
        private Telerik.Reporting.TextBox textBox236;
        private Telerik.Reporting.TextBox textBox237;
        private Telerik.Reporting.TextBox textBox238;
        private Telerik.Reporting.TextBox textBox239;
        private Telerik.Reporting.TextBox textBox240;
        private Telerik.Reporting.TextBox textBox241;
        private Telerik.Reporting.TextBox textBox242;
        private Telerik.Reporting.TextBox textBox243;
        private Telerik.Reporting.TextBox textBox244;
        private Telerik.Reporting.TextBox textBox245;
        private Telerik.Reporting.TextBox textBox246;
        private Telerik.Reporting.TextBox textBox247;
        private Telerik.Reporting.TextBox textBox248;
        private Telerik.Reporting.TextBox textBox249;
        private Telerik.Reporting.TextBox textBox250;
        private Telerik.Reporting.TextBox textBox251;
        private Telerik.Reporting.TextBox textBox252;
        private Telerik.Reporting.TextBox textBox253;
        private Telerik.Reporting.TextBox textBox254;
        private Telerik.Reporting.TextBox textBox255;
        private Telerik.Reporting.TextBox textBox256;
        private Telerik.Reporting.TextBox textBox257;
        private Telerik.Reporting.TextBox textBox258;
        private Telerik.Reporting.TextBox textBox259;
        private Telerik.Reporting.TextBox textBox260;
        private Telerik.Reporting.TextBox textBox261;
        private Telerik.Reporting.TextBox textBox262;
        private Telerik.Reporting.TextBox textBox263;
        private Telerik.Reporting.TextBox textBox264;
        private Telerik.Reporting.TextBox textBox265;
        private Telerik.Reporting.TextBox textBox266;
        private Telerik.Reporting.TextBox textBox267;
        private Telerik.Reporting.TextBox textBox268;
        private Telerik.Reporting.TextBox textBox269;
        private Telerik.Reporting.TextBox textBox270;
        private Telerik.Reporting.TextBox textBox271;
        private Telerik.Reporting.TextBox textBox272;
        private Telerik.Reporting.TextBox textBox273;
        private Telerik.Reporting.TextBox textBox274;
        private Telerik.Reporting.TextBox textBox275;
        private Telerik.Reporting.TextBox textBox276;
        private Telerik.Reporting.TextBox textBox277;
        private Telerik.Reporting.TextBox textBox278;
        private Telerik.Reporting.TextBox textBox279;
        private Telerik.Reporting.TextBox textBox280;
        private Telerik.Reporting.TextBox textBox281;
        private Telerik.Reporting.TextBox textBox282;
        private Telerik.Reporting.TextBox textBox283;
        private Telerik.Reporting.TextBox textBox284;
        private Telerik.Reporting.TextBox textBox285;
        private Telerik.Reporting.TextBox textBox286;
        private Telerik.Reporting.TextBox textBox287;
        private Telerik.Reporting.TextBox textBox288;
        private Telerik.Reporting.TextBox textBox289;
        private Telerik.Reporting.TextBox textBox290;
        private Telerik.Reporting.TextBox textBox291;
        private Telerik.Reporting.TextBox textBox292;
        private Telerik.Reporting.TextBox textBox152;
        private Telerik.Reporting.TextBox textBox153;
        private Telerik.Reporting.TextBox textBox154;
        private Telerik.Reporting.Table table60;
        private Telerik.Reporting.TextBox textBox157;
        private Telerik.Reporting.TextBox textBox160;
        private Telerik.Reporting.TextBox textBox161;
        private Telerik.Reporting.TextBox textBox156;
        private Telerik.Reporting.TextBox textBox158;
        private Telerik.Reporting.TextBox textBox162;
        private Telerik.Reporting.TextBox textBox221;
        private Telerik.Reporting.TextBox textBox222;
        private Telerik.Reporting.TextBox textBox223;
        private Telerik.Reporting.TextBox textBox224;
        private Telerik.Reporting.TextBox textBox225;
        private Telerik.Reporting.TextBox textBox226;
        private Telerik.Reporting.TextBox textBox227;
        private Telerik.Reporting.TextBox textBox228;
        private Telerik.Reporting.TextBox textBox229;
        private Telerik.Reporting.TextBox textBox230;
        private Telerik.Reporting.TextBox textBox231;
        private Telerik.Reporting.TextBox textBox232;
        private Telerik.Reporting.TextBox textBox233;
        private Telerik.Reporting.TextBox textBox234;
        private Telerik.Reporting.TextBox textBox235;
        private Telerik.Reporting.TextBox textBox293;
        private Telerik.Reporting.TextBox textBox294;
        private Telerik.Reporting.TextBox textBox295;
        private Telerik.Reporting.TextBox textBox296;
        private Telerik.Reporting.TextBox textBox297;
        private Telerik.Reporting.TextBox textBox298;
        private Telerik.Reporting.TextBox textBox299;
        private Telerik.Reporting.TextBox textBox300;
        private Telerik.Reporting.TextBox textBox301;
        private Telerik.Reporting.TextBox textBox302;
        private Telerik.Reporting.TextBox textBox303;
        private Telerik.Reporting.TextBox textBox304;
        private Telerik.Reporting.TextBox textBox305;
        private Telerik.Reporting.TextBox textBox306;
        private Telerik.Reporting.TextBox textBox307;
        private Telerik.Reporting.TextBox textBox308;
        private Telerik.Reporting.TextBox textBox309;
        private Telerik.Reporting.TextBox textBox310;
        private Telerik.Reporting.TextBox textBox311;
        private Telerik.Reporting.TextBox textBox312;
        private Telerik.Reporting.TextBox textBox313;
        private Telerik.Reporting.HtmlTextBox htmlTextBox3;
        private Telerik.Reporting.HtmlTextBox htmlTextBox4;
        private Telerik.Reporting.TextBox textBox314;
        private Telerik.Reporting.TextBox textBox315;
        private Telerik.Reporting.TextBox textBox316;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.TextBox textBox317;
        private Telerik.Reporting.TextBox textBox318;
        private Telerik.Reporting.Table table61;
        private Telerik.Reporting.HtmlTextBox htmlTextBox7;
        private Telerik.Reporting.HtmlTextBox htmlTextBox9;
        private Telerik.Reporting.HtmlTextBox htmlTextBox5;
        private Telerik.Reporting.Table table62;
        private Telerik.Reporting.HtmlTextBox htmlTextBox6;
        private Telerik.Reporting.HtmlTextBox htmlTextBox8;
        private Telerik.Reporting.HtmlTextBox htmlTextBox10;
        private Telerik.Reporting.HtmlTextBox htmlTextBox11;
        private Telerik.Reporting.HtmlTextBox htmlTextBox12;
        private Telerik.Reporting.Table table63;
        private Telerik.Reporting.TextBox textBox320;
        private Telerik.Reporting.TextBox textBox322;
        private Telerik.Reporting.TextBox textBox324;
        private Telerik.Reporting.TextBox textBox326;
        private Telerik.Reporting.HtmlTextBox htmlTextBox13;
        private Telerik.Reporting.Panel panel2;
        private Telerik.Reporting.TextBox textBox321;
        private Telerik.Reporting.TextBox textBox319;
        private Telerik.Reporting.Table table64;
        private Telerik.Reporting.HtmlTextBox htmlTextBox14;
        private Telerik.Reporting.HtmlTextBox htmlTextBox15;
        private Telerik.Reporting.HtmlTextBox htmlTextBox16;
        private Telerik.Reporting.HtmlTextBox htmlTextBox18;
        private Telerik.Reporting.TextBox textBox332;
        private Telerik.Reporting.Table table66;
        private Telerik.Reporting.TextBox textBox334;
        private Telerik.Reporting.TextBox textBox335;
        private Telerik.Reporting.TextBox textBox336;
        private Telerik.Reporting.TextBox textBox337;
        private Telerik.Reporting.TextBox textBox338;
        private Telerik.Reporting.TextBox textBox339;
        private Telerik.Reporting.HtmlTextBox htmlTextBox17;
        private Telerik.Reporting.HtmlTextBox htmlTextBox19;
        private Telerik.Reporting.Table table65;
        private Telerik.Reporting.TextBox textBox323;
        private Telerik.Reporting.TextBox textBox325;
        private Telerik.Reporting.TextBox textBox327;
        private Telerik.Reporting.TextBox textBox328;
        private Telerik.Reporting.TextBox textBox329;
        private Telerik.Reporting.TextBox textBox330;
        private Telerik.Reporting.TextBox textBox331;
        private Telerik.Reporting.Table table67;
        private Telerik.Reporting.TextBox textBox340;
        private Telerik.Reporting.TextBox textBox342;
        private Telerik.Reporting.TextBox textBox333;
        private Telerik.Reporting.TextBox textBox341;
        private Telerik.Reporting.TextBox textBox343;
        private Telerik.Reporting.Panel page2;
        private Telerik.Reporting.Panel panel4;
        private Telerik.Reporting.Panel page6;
        private Telerik.Reporting.Panel page5;
        private Telerik.Reporting.Panel page4;
        private Telerik.Reporting.Panel page3;
    }
}