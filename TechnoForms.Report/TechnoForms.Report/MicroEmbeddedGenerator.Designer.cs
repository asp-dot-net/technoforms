namespace TechnoForms.Report
{
    partial class MicroEmbeddedGenerator
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MicroEmbeddedGenerator));
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup60 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup61 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup62 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup63 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup64 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup65 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup66 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup67 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup68 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup69 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup70 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup71 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup72 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup73 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup74 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup75 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup76 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup77 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup78 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup79 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup80 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup81 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup82 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup83 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup84 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup85 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup86 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup87 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup88 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup89 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.piclogo = new Telerik.Reporting.PictureBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.checkBox1 = new Telerik.Reporting.CheckBox();
            this.checkBox2 = new Telerik.Reporting.CheckBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.checkBox3 = new Telerik.Reporting.CheckBox();
            this.checkBox4 = new Telerik.Reporting.CheckBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.checkBox5 = new Telerik.Reporting.CheckBox();
            this.checkBox6 = new Telerik.Reporting.CheckBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox155 = new Telerik.Reporting.TextBox();
            this.textBox156 = new Telerik.Reporting.TextBox();
            this.textBox162 = new Telerik.Reporting.TextBox();
            this.textBox168 = new Telerik.Reporting.TextBox();
            this.textBox174 = new Telerik.Reporting.TextBox();
            this.textBox180 = new Telerik.Reporting.TextBox();
            this.textBox186 = new Telerik.Reporting.TextBox();
            this.textBox192 = new Telerik.Reporting.TextBox();
            this.textBox193 = new Telerik.Reporting.TextBox();
            this.textBox194 = new Telerik.Reporting.TextBox();
            this.textBox195 = new Telerik.Reporting.TextBox();
            this.textBox196 = new Telerik.Reporting.TextBox();
            this.textBox197 = new Telerik.Reporting.TextBox();
            this.textBox198 = new Telerik.Reporting.TextBox();
            this.textBox199 = new Telerik.Reporting.TextBox();
            this.textBox200 = new Telerik.Reporting.TextBox();
            this.textBox201 = new Telerik.Reporting.TextBox();
            this.textBox202 = new Telerik.Reporting.TextBox();
            this.textBox203 = new Telerik.Reporting.TextBox();
            this.textBox204 = new Telerik.Reporting.TextBox();
            this.textBox205 = new Telerik.Reporting.TextBox();
            this.textBox206 = new Telerik.Reporting.TextBox();
            this.textBox207 = new Telerik.Reporting.TextBox();
            this.textBox208 = new Telerik.Reporting.TextBox();
            this.textBox331 = new Telerik.Reporting.TextBox();
            this.textBox341 = new Telerik.Reporting.TextBox();
            this.textBox342 = new Telerik.Reporting.TextBox();
            this.textBox343 = new Telerik.Reporting.TextBox();
            this.textBox344 = new Telerik.Reporting.TextBox();
            this.textBox345 = new Telerik.Reporting.TextBox();
            this.textBox346 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.textBox114 = new Telerik.Reporting.TextBox();
            this.textBox115 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.textBox106 = new Telerik.Reporting.TextBox();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.textBox119 = new Telerik.Reporting.TextBox();
            this.textBox120 = new Telerik.Reporting.TextBox();
            this.textBox121 = new Telerik.Reporting.TextBox();
            this.textBox122 = new Telerik.Reporting.TextBox();
            this.textBox123 = new Telerik.Reporting.TextBox();
            this.textBox124 = new Telerik.Reporting.TextBox();
            this.textBox125 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.textBox128 = new Telerik.Reporting.TextBox();
            this.textBox129 = new Telerik.Reporting.TextBox();
            this.textBox130 = new Telerik.Reporting.TextBox();
            this.textBox131 = new Telerik.Reporting.TextBox();
            this.textBox132 = new Telerik.Reporting.TextBox();
            this.textBox133 = new Telerik.Reporting.TextBox();
            this.textBox134 = new Telerik.Reporting.TextBox();
            this.textBox135 = new Telerik.Reporting.TextBox();
            this.textBox136 = new Telerik.Reporting.TextBox();
            this.textBox137 = new Telerik.Reporting.TextBox();
            this.textBox138 = new Telerik.Reporting.TextBox();
            this.textBox139 = new Telerik.Reporting.TextBox();
            this.textBox140 = new Telerik.Reporting.TextBox();
            this.textBox141 = new Telerik.Reporting.TextBox();
            this.textBox142 = new Telerik.Reporting.TextBox();
            this.textBox143 = new Telerik.Reporting.TextBox();
            this.textBox144 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox145 = new Telerik.Reporting.TextBox();
            this.textBox147 = new Telerik.Reporting.TextBox();
            this.textBox149 = new Telerik.Reporting.TextBox();
            this.textBox151 = new Telerik.Reporting.TextBox();
            this.textBox152 = new Telerik.Reporting.TextBox();
            this.textBox153 = new Telerik.Reporting.TextBox();
            this.textBox146 = new Telerik.Reporting.TextBox();
            this.textBox148 = new Telerik.Reporting.TextBox();
            this.textBox154 = new Telerik.Reporting.TextBox();
            this.textBox157 = new Telerik.Reporting.TextBox();
            this.textBox158 = new Telerik.Reporting.TextBox();
            this.textBox159 = new Telerik.Reporting.TextBox();
            this.textBox160 = new Telerik.Reporting.TextBox();
            this.textBox161 = new Telerik.Reporting.TextBox();
            this.textBox163 = new Telerik.Reporting.TextBox();
            this.textBox164 = new Telerik.Reporting.TextBox();
            this.textBox165 = new Telerik.Reporting.TextBox();
            this.textBox166 = new Telerik.Reporting.TextBox();
            this.textBox167 = new Telerik.Reporting.TextBox();
            this.textBox169 = new Telerik.Reporting.TextBox();
            this.textBox170 = new Telerik.Reporting.TextBox();
            this.textBox171 = new Telerik.Reporting.TextBox();
            this.textBox172 = new Telerik.Reporting.TextBox();
            this.textBox173 = new Telerik.Reporting.TextBox();
            this.textBox175 = new Telerik.Reporting.TextBox();
            this.textBox176 = new Telerik.Reporting.TextBox();
            this.textBox177 = new Telerik.Reporting.TextBox();
            this.textBox178 = new Telerik.Reporting.TextBox();
            this.textBox179 = new Telerik.Reporting.TextBox();
            this.textBox181 = new Telerik.Reporting.TextBox();
            this.textBox182 = new Telerik.Reporting.TextBox();
            this.textBox183 = new Telerik.Reporting.TextBox();
            this.textBox184 = new Telerik.Reporting.TextBox();
            this.textBox185 = new Telerik.Reporting.TextBox();
            this.textBox187 = new Telerik.Reporting.TextBox();
            this.textBox188 = new Telerik.Reporting.TextBox();
            this.textBox189 = new Telerik.Reporting.TextBox();
            this.textBox190 = new Telerik.Reporting.TextBox();
            this.textBox191 = new Telerik.Reporting.TextBox();
            this.textBox209 = new Telerik.Reporting.TextBox();
            this.textBox210 = new Telerik.Reporting.TextBox();
            this.textBox211 = new Telerik.Reporting.TextBox();
            this.textBox212 = new Telerik.Reporting.TextBox();
            this.textBox213 = new Telerik.Reporting.TextBox();
            this.textBox214 = new Telerik.Reporting.TextBox();
            this.textBox215 = new Telerik.Reporting.TextBox();
            this.textBox216 = new Telerik.Reporting.TextBox();
            this.textBox217 = new Telerik.Reporting.TextBox();
            this.textBox218 = new Telerik.Reporting.TextBox();
            this.textBox219 = new Telerik.Reporting.TextBox();
            this.textBox220 = new Telerik.Reporting.TextBox();
            this.textBox221 = new Telerik.Reporting.TextBox();
            this.textBox222 = new Telerik.Reporting.TextBox();
            this.textBox223 = new Telerik.Reporting.TextBox();
            this.textBox224 = new Telerik.Reporting.TextBox();
            this.textBox225 = new Telerik.Reporting.TextBox();
            this.textBox226 = new Telerik.Reporting.TextBox();
            this.textBox227 = new Telerik.Reporting.TextBox();
            this.textBox228 = new Telerik.Reporting.TextBox();
            this.textBox229 = new Telerik.Reporting.TextBox();
            this.textBox230 = new Telerik.Reporting.TextBox();
            this.textBox231 = new Telerik.Reporting.TextBox();
            this.textBox232 = new Telerik.Reporting.TextBox();
            this.textBox233 = new Telerik.Reporting.TextBox();
            this.textBox234 = new Telerik.Reporting.TextBox();
            this.textBox235 = new Telerik.Reporting.TextBox();
            this.textBox236 = new Telerik.Reporting.TextBox();
            this.textBox237 = new Telerik.Reporting.TextBox();
            this.textBox238 = new Telerik.Reporting.TextBox();
            this.textBox239 = new Telerik.Reporting.TextBox();
            this.textBox240 = new Telerik.Reporting.TextBox();
            this.textBox241 = new Telerik.Reporting.TextBox();
            this.textBox242 = new Telerik.Reporting.TextBox();
            this.textBox243 = new Telerik.Reporting.TextBox();
            this.textBox244 = new Telerik.Reporting.TextBox();
            this.textBox245 = new Telerik.Reporting.TextBox();
            this.textBox246 = new Telerik.Reporting.TextBox();
            this.textBox247 = new Telerik.Reporting.TextBox();
            this.textBox248 = new Telerik.Reporting.TextBox();
            this.textBox249 = new Telerik.Reporting.TextBox();
            this.textBox250 = new Telerik.Reporting.TextBox();
            this.textBox251 = new Telerik.Reporting.TextBox();
            this.textBox252 = new Telerik.Reporting.TextBox();
            this.textBox253 = new Telerik.Reporting.TextBox();
            this.textBox254 = new Telerik.Reporting.TextBox();
            this.textBox255 = new Telerik.Reporting.TextBox();
            this.textBox256 = new Telerik.Reporting.TextBox();
            this.textBox259 = new Telerik.Reporting.TextBox();
            this.textBox260 = new Telerik.Reporting.TextBox();
            this.textBox261 = new Telerik.Reporting.TextBox();
            this.textBox262 = new Telerik.Reporting.TextBox();
            this.textBox263 = new Telerik.Reporting.TextBox();
            this.textBox264 = new Telerik.Reporting.TextBox();
            this.textBox265 = new Telerik.Reporting.TextBox();
            this.textBox266 = new Telerik.Reporting.TextBox();
            this.textBox267 = new Telerik.Reporting.TextBox();
            this.textBox268 = new Telerik.Reporting.TextBox();
            this.textBox269 = new Telerik.Reporting.TextBox();
            this.textBox270 = new Telerik.Reporting.TextBox();
            this.textBox271 = new Telerik.Reporting.TextBox();
            this.textBox272 = new Telerik.Reporting.TextBox();
            this.textBox273 = new Telerik.Reporting.TextBox();
            this.textBox275 = new Telerik.Reporting.TextBox();
            this.textBox279 = new Telerik.Reporting.TextBox();
            this.checkBox7 = new Telerik.Reporting.CheckBox();
            this.textBox276 = new Telerik.Reporting.TextBox();
            this.checkBox8 = new Telerik.Reporting.CheckBox();
            this.textBox332 = new Telerik.Reporting.TextBox();
            this.textBox333 = new Telerik.Reporting.TextBox();
            this.textBox334 = new Telerik.Reporting.TextBox();
            this.textBox335 = new Telerik.Reporting.TextBox();
            this.textBox336 = new Telerik.Reporting.TextBox();
            this.textBox337 = new Telerik.Reporting.TextBox();
            this.textBox338 = new Telerik.Reporting.TextBox();
            this.textBox257 = new Telerik.Reporting.TextBox();
            this.textBox347 = new Telerik.Reporting.TextBox();
            this.textBox348 = new Telerik.Reporting.TextBox();
            this.textBox349 = new Telerik.Reporting.TextBox();
            this.textBox350 = new Telerik.Reporting.TextBox();
            this.textBox351 = new Telerik.Reporting.TextBox();
            this.textBox352 = new Telerik.Reporting.TextBox();
            this.textBox353 = new Telerik.Reporting.TextBox();
            this.textBox354 = new Telerik.Reporting.TextBox();
            this.textBox355 = new Telerik.Reporting.TextBox();
            this.textBox356 = new Telerik.Reporting.TextBox();
            this.textBox357 = new Telerik.Reporting.TextBox();
            this.textBox358 = new Telerik.Reporting.TextBox();
            this.textBox359 = new Telerik.Reporting.TextBox();
            this.textBox360 = new Telerik.Reporting.TextBox();
            this.textBox150 = new Telerik.Reporting.TextBox();
            this.textBox274 = new Telerik.Reporting.TextBox();
            this.textBox277 = new Telerik.Reporting.TextBox();
            this.textBox278 = new Telerik.Reporting.TextBox();
            this.textBox280 = new Telerik.Reporting.TextBox();
            this.textBox281 = new Telerik.Reporting.TextBox();
            this.textBox283 = new Telerik.Reporting.TextBox();
            this.textBox282 = new Telerik.Reporting.TextBox();
            this.textBox284 = new Telerik.Reporting.TextBox();
            this.textBox285 = new Telerik.Reporting.TextBox();
            this.textBox287 = new Telerik.Reporting.TextBox();
            this.textBox289 = new Telerik.Reporting.TextBox();
            this.textBox286 = new Telerik.Reporting.TextBox();
            this.checkBox9 = new Telerik.Reporting.CheckBox();
            this.textBox291 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox306 = new Telerik.Reporting.TextBox();
            this.textBox308 = new Telerik.Reporting.TextBox();
            this.textBox309 = new Telerik.Reporting.TextBox();
            this.textBox293 = new Telerik.Reporting.TextBox();
            this.textBox294 = new Telerik.Reporting.TextBox();
            this.htmlTextBox10 = new Telerik.Reporting.HtmlTextBox();
            this.textBox314 = new Telerik.Reporting.TextBox();
            this.textBox315 = new Telerik.Reporting.TextBox();
            this.textBox316 = new Telerik.Reporting.TextBox();
            this.textBox317 = new Telerik.Reporting.TextBox();
            this.textBox318 = new Telerik.Reporting.TextBox();
            this.textBox319 = new Telerik.Reporting.TextBox();
            this.textBox320 = new Telerik.Reporting.TextBox();
            this.textBox321 = new Telerik.Reporting.TextBox();
            this.textBox325 = new Telerik.Reporting.TextBox();
            this.textBox326 = new Telerik.Reporting.TextBox();
            this.textBox327 = new Telerik.Reporting.TextBox();
            this.textBox328 = new Telerik.Reporting.TextBox();
            this.textBox329 = new Telerik.Reporting.TextBox();
            this.textBox330 = new Telerik.Reporting.TextBox();
            this.htmlTextBox7 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox8 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox9 = new Telerik.Reporting.HtmlTextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox339 = new Telerik.Reporting.TextBox();
            this.textBox258 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox340 = new Telerik.Reporting.TextBox();
            this.textBox288 = new Telerik.Reporting.TextBox();
            this.htmlTextBox11 = new Telerik.Reporting.HtmlTextBox();
            this.textBox307 = new Telerik.Reporting.TextBox();
            this.textBox322 = new Telerik.Reporting.TextBox();
            this.textBox323 = new Telerik.Reporting.TextBox();
            this.textBox324 = new Telerik.Reporting.TextBox();
            this.textBox362 = new Telerik.Reporting.TextBox();
            this.textBox363 = new Telerik.Reporting.TextBox();
            this.textBox364 = new Telerik.Reporting.TextBox();
            this.textBox365 = new Telerik.Reporting.TextBox();
            this.textBox366 = new Telerik.Reporting.TextBox();
            this.textBox367 = new Telerik.Reporting.TextBox();
            this.textBox368 = new Telerik.Reporting.TextBox();
            this.textBox369 = new Telerik.Reporting.TextBox();
            this.textBox370 = new Telerik.Reporting.TextBox();
            this.textBox371 = new Telerik.Reporting.TextBox();
            this.textBox312 = new Telerik.Reporting.TextBox();
            this.textBox310 = new Telerik.Reporting.TextBox();
            this.textBox292 = new Telerik.Reporting.TextBox();
            this.htmlTextBox6 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox5 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox4 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox3 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox2 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.textBox304 = new Telerik.Reporting.TextBox();
            this.textBox300 = new Telerik.Reporting.TextBox();
            this.textBox298 = new Telerik.Reporting.TextBox();
            this.textBox301 = new Telerik.Reporting.TextBox();
            this.textBox296 = new Telerik.Reporting.TextBox();
            this.textBox295 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(866D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.piclogo,
            this.textBox1,
            this.textBox2,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox9,
            this.textBox8,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.table1,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.checkBox5,
            this.checkBox6,
            this.textBox57,
            this.table2,
            this.textBox83,
            this.textBox84,
            this.textBox100,
            this.textBox101,
            this.table3,
            this.textBox144,
            this.table4,
            this.textBox274,
            this.textBox277,
            this.textBox278,
            this.textBox280,
            this.textBox281,
            this.textBox283,
            this.textBox282,
            this.textBox284,
            this.textBox285,
            this.textBox287,
            this.textBox289,
            this.textBox286,
            this.checkBox9,
            this.textBox291,
            this.table5,
            this.textBox314,
            this.textBox315,
            this.textBox316,
            this.textBox317,
            this.textBox318,
            this.textBox319,
            this.textBox320,
            this.textBox321,
            this.textBox325,
            this.textBox326,
            this.textBox327,
            this.textBox328,
            this.textBox329,
            this.textBox330,
            this.htmlTextBox7,
            this.htmlTextBox8,
            this.htmlTextBox9,
            this.table6,
            this.htmlTextBox11});
            this.detail.Name = "detail";
            this.detail.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.detail.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            // 
            // piclogo
            // 
            this.piclogo.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.499900817871094D), Telerik.Reporting.Drawing.Unit.Cm(9.9921220680698752E-05D));
            this.piclogo.MimeType = "image/png";
            this.piclogo.Name = "piclogo";
            this.piclogo.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.4999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(2.9000000953674316D));
            this.piclogo.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.piclogo.Style.BorderColor.Default = System.Drawing.Color.Transparent;
            this.piclogo.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.piclogo.Style.LineColor = System.Drawing.Color.Transparent;
            this.piclogo.Style.LineWidth = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.piclogo.Value = ((object)(resources.GetObject("piclogo.Value")));
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.89999997615814209D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox1.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "ADDITION OR ALTERATION OF MICRO EMBEDDED GENERATOR CONNECTION";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(3.3999998569488525D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.999799728393555D), Telerik.Reporting.Drawing.Unit.Cm(1.6999999284744263D));
            this.textBox2.Style.Font.Name = "Arial";
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox2.Style.Font.Underline = false;
            this.textBox2.Value = resources.GetString("textBox2.Value");
            // 
            // textBox5
            // 
            this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00020024616969749332D), Telerik.Reporting.Drawing.Unit.Cm(9.09999942779541D));
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4997997283935547D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox5.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Italic = true;
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox5.Style.Font.Underline = false;
            this.textBox5.Value = "Customer details";
            // 
            // textBox6
            // 
            this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00020024616969749332D), Telerik.Reporting.Drawing.Unit.Cm(10.199999809265137D));
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9997999668121338D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "Name: ";
            // 
            // textBox7
            // 
            this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.0002002716064453D), Telerik.Reporting.Drawing.Unit.Cm(10.199999809265137D));
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.099596977233887D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox7.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox7.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox7.Style.Font.Underline = false;
            this.textBox7.Style.LineColor = System.Drawing.Color.Black;
            this.textBox7.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox7.Value = "Kemal Kajim";
            // 
            // textBox9
            // 
            this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.099998474121094D), Telerik.Reporting.Drawing.Unit.Cm(10.199999809265137D));
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5000019073486328D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "ABN (if Applicable) :";
            // 
            // textBox8
            // 
            this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.600200653076172D), Telerik.Reporting.Drawing.Unit.Cm(10.199999809265137D));
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3996982574462891D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox8.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox8.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox8.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox8.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox8.Style.Font.Underline = false;
            this.textBox8.Style.LineColor = System.Drawing.Color.Black;
            this.textBox8.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox8.Value = "";
            // 
            // textBox10
            // 
            this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00060073850909247994D), Telerik.Reporting.Drawing.Unit.Cm(11.199999809265137D));
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9997997283935547D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.Value = "Company Name :";
            // 
            // textBox11
            // 
            this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.0002000331878662D), Telerik.Reporting.Drawing.Unit.Cm(11.199999809265137D));
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.999700546264648D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox11.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox11.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox11.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox11.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox11.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox11.Style.Font.Underline = false;
            this.textBox11.Style.LineColor = System.Drawing.Color.Black;
            this.textBox11.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox11.Value = "";
            // 
            // textBox12
            // 
            this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(12.200000762939453D));
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox12.Value = "Address :";
            // 
            // textBox13
            // 
            this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.0002000331878662D), Telerik.Reporting.Drawing.Unit.Cm(12.200000762939453D));
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.999700546264648D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox13.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox13.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox13.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox13.Style.Font.Underline = false;
            this.textBox13.Style.LineColor = System.Drawing.Color.Black;
            this.textBox13.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.Value = "25 SUNBURY STREET ,PALLARA,QLD - 4110";
            // 
            // textBox14
            // 
            this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00020024616969749332D), Telerik.Reporting.Drawing.Unit.Cm(13.299999237060547D));
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9997990131378174D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.Value = "Email :";
            // 
            // textBox15
            // 
            this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.0002002716064453D), Telerik.Reporting.Drawing.Unit.Cm(13.299999237060547D));
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.09959888458252D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox15.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox15.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox15.Style.Font.Underline = false;
            this.textBox15.Style.LineColor = System.Drawing.Color.Black;
            this.textBox15.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = "sales@aceprint.com.au";
            // 
            // textBox16
            // 
            this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.099998474121094D), Telerik.Reporting.Drawing.Unit.Cm(13.299999237060547D));
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5000013113021851D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.Value = "Phone :";
            // 
            // textBox17
            // 
            this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.600200653076172D), Telerik.Reporting.Drawing.Unit.Cm(13.299999237060547D));
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3996987342834473D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox17.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox17.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox17.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox17.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox17.Style.Font.Underline = false;
            this.textBox17.Style.LineColor = System.Drawing.Color.Black;
            this.textBox17.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.Value = "0407190051";
            // 
            // textBox18
            // 
            this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(14.699999809265137D));
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.30000114440918D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox18.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.Font.Italic = true;
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox18.Style.Font.Underline = false;
            this.textBox18.Value = "Registered Electrical Contractor (REC) details";
            // 
            // textBox19
            // 
            this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D));
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9997999668121338D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.Value = "Name: ";
            // 
            // textBox20
            // 
            this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.0002007484436035D), Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D));
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.099596977233887D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox20.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox20.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox20.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox20.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox20.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox20.Style.Font.Underline = false;
            this.textBox20.Style.LineColor = System.Drawing.Color.Black;
            this.textBox20.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.Value = "Fourie Eksteen";
            // 
            // textBox21
            // 
            this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.099998474121094D), Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D));
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2000018358230591D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.Value = "ABN:";
            // 
            // textBox22
            // 
            this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.300199508666992D), Telerik.Reporting.Drawing.Unit.Cm(15.799999237060547D));
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.699699878692627D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox22.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox22.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox22.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox22.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox22.Style.Font.Underline = false;
            this.textBox22.Style.LineColor = System.Drawing.Color.Black;
            this.textBox22.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.Value = "0";
            // 
            // textBox23
            // 
            this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00020024616969749332D), Telerik.Reporting.Drawing.Unit.Cm(17D));
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9997999668121338D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.Value = "REC Number :";
            // 
            // textBox24
            // 
            this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.0002012252807617D), Telerik.Reporting.Drawing.Unit.Cm(17D));
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.299799919128418D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox24.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox24.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox24.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox24.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox24.Style.Font.Underline = false;
            this.textBox24.Style.LineColor = System.Drawing.Color.Black;
            this.textBox24.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.Value = "A1873042";
            // 
            // textBox25
            // 
            this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.300201416015625D), Telerik.Reporting.Drawing.Unit.Cm(17D));
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5996015071868896D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.Value = "CES Number :";
            // 
            // textBox26
            // 
            this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.90000057220459D), Telerik.Reporting.Drawing.Unit.Cm(17D));
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.1001996994018555D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox26.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox26.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox26.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox26.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox26.Style.Font.Underline = false;
            this.textBox26.Style.LineColor = System.Drawing.Color.Black;
            this.textBox26.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox26.Value = "";
            // 
            // textBox27
            // 
            this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00020024616969749332D), Telerik.Reporting.Drawing.Unit.Cm(18.399999618530273D));
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9997997283935547D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox27.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Font.Italic = true;
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(18D);
            this.textBox27.Style.Font.Underline = false;
            this.textBox27.Value = "Supply Address details";
            // 
            // textBox28
            // 
            this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00020024616969749332D), Telerik.Reporting.Drawing.Unit.Cm(19.899999618530273D));
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9999995231628418D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox28.Value = "Address :";
            // 
            // textBox29
            // 
            this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.0003998279571533D), Telerik.Reporting.Drawing.Unit.Cm(19.899999618530273D));
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.999500274658203D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox29.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox29.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox29.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox29.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox29.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox29.Style.Font.Underline = false;
            this.textBox29.Style.LineColor = System.Drawing.Color.Black;
            this.textBox29.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.Value = "5 NEWLANDS STREET";
            // 
            // textBox30
            // 
            this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.000399684882722795D), Telerik.Reporting.Drawing.Unit.Cm(21.099998474121094D));
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9997999668121338D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox30.Value = "Suburb :";
            // 
            // textBox31
            // 
            this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.0003998279571533D), Telerik.Reporting.Drawing.Unit.Cm(21.099998474121094D));
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2996001243591309D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox31.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox31.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox31.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox31.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox31.Style.Font.Underline = false;
            this.textBox31.Style.LineColor = System.Drawing.Color.Black;
            this.textBox31.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.Value = "REDLAND BAY";
            // 
            // textBox32
            // 
            this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.30019998550415D), Telerik.Reporting.Drawing.Unit.Cm(21.099998474121094D));
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3997999429702759D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.Value = "State:";
            // 
            // textBox33
            // 
            this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.700200080871582D), Telerik.Reporting.Drawing.Unit.Cm(21.099998474121094D));
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.3995976448059082D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox33.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox33.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox33.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox33.Style.Font.Underline = false;
            this.textBox33.Style.LineColor = System.Drawing.Color.Black;
            this.textBox33.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.Value = "QLD";
            // 
            // textBox34
            // 
            this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.099998474121094D), Telerik.Reporting.Drawing.Unit.Cm(21.099998474121094D));
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9998037815093994D), Telerik.Reporting.Drawing.Unit.Cm(0.599999725818634D));
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.Value = "PostCode:";
            // 
            // textBox35
            // 
            this.textBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.100002288818359D), Telerik.Reporting.Drawing.Unit.Cm(21.099998474121094D));
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.7318849563598633D), Telerik.Reporting.Drawing.Unit.Cm(0.60000050067901611D));
            this.textBox35.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox35.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.textBox35.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox35.Style.Font.Underline = false;
            this.textBox35.Style.LineColor = System.Drawing.Color.Black;
            this.textBox35.Style.LineStyle = Telerik.Reporting.Drawing.LineStyle.Solid;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.Value = "4165";
            // 
            // textBox36
            // 
            this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941D), Telerik.Reporting.Drawing.Unit.Cm(1.6000000238418579D));
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox36.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox36.Value = "A";
            // 
            // textBox37
            // 
            this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D), Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D));
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9997999668121338D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox37.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox37.Value = "DDITION";
            // 
            // textBox38
            // 
            this.textBox38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.6999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(1.9000002145767212D));
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.7999998927116394D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox38.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox38.Value = "OR";
            // 
            // textBox39
            // 
            this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.7000000476837158D), Telerik.Reporting.Drawing.Unit.Cm(1.6205599308013916D));
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox39.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox39.Value = "A";
            // 
            // textBox40
            // 
            this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(1.9000002145767212D));
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox40.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox40.Style.Font.Bold = true;
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox40.Value = "LTERATION";
            // 
            // textBox41
            // 
            this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.0999999046325684D), Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D));
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.80000025033950806D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox41.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox41.Style.Font.Bold = true;
            this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox41.Value = "OF";
            // 
            // textBox42
            // 
            this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.1000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(1.6411194801330566D));
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.79979932308197021D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox42.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox42.Value = "M";
            // 
            // textBox43
            // 
            this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.8000001907348633D), Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D));
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1997994184494019D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox43.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox43.Style.Font.Bold = true;
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox43.Value = "ICRO";
            // 
            // textBox44
            // 
            this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.199999809265137D), Telerik.Reporting.Drawing.Unit.Cm(1.6999999284744263D));
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox44.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox44.Style.Font.Bold = true;
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox44.Value = "E";
            // 
            // textBox45
            // 
            this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.699999809265137D), Telerik.Reporting.Drawing.Unit.Cm(1.9411197900772095D));
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3000004291534424D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox45.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox45.Value = "MBEDDED";
            // 
            // textBox46
            // 
            this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.300000190734863D), Telerik.Reporting.Drawing.Unit.Cm(1.6000000238418579D));
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59980267286300659D), Telerik.Reporting.Drawing.Unit.Cm(0.800000011920929D));
            this.textBox46.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.Value = "G";
            // 
            // textBox47
            // 
            this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.90000057220459D), Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D));
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8000006675720215D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox47.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(73)))), ((int)(((byte)(124)))));
            this.textBox47.Style.Font.Bold = true;
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.textBox47.Value = "ENERATOR";
            // 
            // textBox48
            // 
            this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(22.600000381469727D));
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.90000057220459D), Telerik.Reporting.Drawing.Unit.Cm(0.70000249147415161D));
            this.textBox48.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(194)))), ((int)(((byte)(225)))));
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox48.Value = "Jemena Electricity Networks Ltd ABN: 82 064 651 083\r\nAddition or Alteration of Mi" +
    "cro Embedded Generator Connection - 1 July 2016";
            // 
            // textBox49
            // 
            this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(24D));
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9997997283935547D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox49.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox49.Style.Font.Bold = true;
            this.textBox49.Style.Font.Italic = true;
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(17D);
            this.textBox49.Style.Font.Underline = false;
            this.textBox49.Value = "Service required";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.402916431427002D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.6139593124389648D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.2441701889038086D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.84395879507064819D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.7381247878074646D)));
            this.table1.Body.SetCellContent(0, 0, this.textBox50);
            this.table1.Body.SetCellContent(0, 1, this.checkBox1);
            this.table1.Body.SetCellContent(0, 2, this.checkBox2);
            this.table1.Body.SetCellContent(1, 0, this.textBox51);
            this.table1.Body.SetCellContent(1, 1, this.checkBox3);
            this.table1.Body.SetCellContent(1, 2, this.checkBox4);
            tableGroup1.Name = "tableGroup";
            tableGroup2.Name = "tableGroup1";
            tableGroup3.Name = "tableGroup2";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.ColumnGroups.Add(tableGroup3);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox50,
            this.checkBox1,
            this.checkBox2,
            this.textBox51,
            this.checkBox3,
            this.checkBox4});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941D), Telerik.Reporting.Drawing.Unit.Cm(25.100000381469727D));
            this.table1.Name = "table1";
            tableGroup5.Name = "group1";
            tableGroup6.Name = "group";
            tableGroup4.ChildGroups.Add(tableGroup5);
            tableGroup4.ChildGroups.Add(tableGroup6);
            tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup4.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup4);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.261045455932617D), Telerik.Reporting.Drawing.Unit.Cm(1.5820835828781128D));
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.4029154777526855D), Telerik.Reporting.Drawing.Unit.Cm(0.84395879507064819D));
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.StyleName = "";
            this.textBox50.Value = "Inverter/Embedded Generator";
            // 
            // checkBox1
            // 
            this.checkBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6139591932296753D), Telerik.Reporting.Drawing.Unit.Cm(0.84395879507064819D));
            this.checkBox1.Style.Font.Bold = true;
            this.checkBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.checkBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox1.StyleName = "";
            this.checkBox1.Text = "New";
            // 
            // checkBox2
            // 
            this.checkBox2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2441692352294922D), Telerik.Reporting.Drawing.Unit.Cm(0.84395879507064819D));
            this.checkBox2.Style.Font.Bold = true;
            this.checkBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.checkBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.checkBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox2.StyleName = "";
            this.checkBox2.Text = "Alteration";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.402916431427002D), Telerik.Reporting.Drawing.Unit.Cm(0.7381247878074646D));
            this.textBox51.Style.Font.Bold = true;
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.StyleName = "";
            this.textBox51.Value = "Inverter/Embedded Generator";
            // 
            // checkBox3
            // 
            this.checkBox3.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.6139593124389648D), Telerik.Reporting.Drawing.Unit.Cm(0.7381247878074646D));
            this.checkBox3.Style.Font.Bold = true;
            this.checkBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.checkBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox3.StyleName = "";
            this.checkBox3.Text = "New";
            // 
            // checkBox4
            // 
            this.checkBox4.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2441701889038086D), Telerik.Reporting.Drawing.Unit.Cm(0.7381247878074646D));
            this.checkBox4.Style.Font.Bold = true;
            this.checkBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.checkBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.checkBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox4.StyleName = "";
            this.checkBox4.Text = "Alteration";
            // 
            // textBox52
            // 
            this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.300199508666992D), Telerik.Reporting.Drawing.Unit.Cm(26D));
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.0996001958847046D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.Value = "NMI:";
            // 
            // textBox53
            // 
            this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.399999618530273D), Telerik.Reporting.Drawing.Unit.Cm(26D));
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5507485866546631D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox53.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.Value = "31203823579";
            // 
            // textBox54
            // 
            this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941D), Telerik.Reporting.Drawing.Unit.Cm(27.19999885559082D));
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox54.Value = "* An Electrical Works Request (EWR) must accompany this application if you are ad" +
    "ding or making changes to your electricity supply.";
            // 
            // textBox55
            // 
            this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(28.600000381469727D));
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.2999992370605469D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox55.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox55.Style.Font.Bold = true;
            this.textBox55.Style.Font.Italic = true;
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(17D);
            this.textBox55.Style.Font.Underline = false;
            this.textBox55.Value = "Inverter/Generator Installation Details";
            // 
            // textBox56
            // 
            this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.000399684882722795D), Telerik.Reporting.Drawing.Unit.Cm(29.700000762939453D));
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.499600410461426D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(12D);
            this.textBox56.Value = "Are/Will separate Inverter(s) be installed for the generator and energy storage?";
            // 
            // checkBox5
            // 
            this.checkBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.199997901916504D), Telerik.Reporting.Drawing.Unit.Cm(29.700000762939453D));
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1998014450073242D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.checkBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox5.Text = "Yes/Separate";
            // 
            // checkBox6
            // 
            this.checkBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.900197982788086D), Telerik.Reporting.Drawing.Unit.Cm(29.700000762939453D));
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1998014450073242D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.checkBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox6.Text = "No / Combined";
            // 
            // textBox57
            // 
            this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(30.799999237060547D));
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.2999992370605469D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox57.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox57.Style.Font.Bold = true;
            this.textBox57.Style.Font.Italic = true;
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(17D);
            this.textBox57.Style.Font.Underline = false;
            this.textBox57.Value = "Generator Installed Capacity";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.1396903991699219D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1621325016021729D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.5039716362953186D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8277058601379395D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.5039716362953186D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0058119297027588D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.52969640493392944D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.7030184268951416D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.1852083206176758D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60583323240280151D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34125003218650818D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.71166664361953735D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.65875023603439331D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.71166664361953735D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox58);
            this.table2.Body.SetCellContent(0, 1, this.textBox60);
            this.table2.Body.SetCellContent(1, 0, this.textBox59);
            this.table2.Body.SetCellContent(1, 1, this.textBox61);
            this.table2.Body.SetCellContent(1, 3, this.textBox63);
            this.table2.Body.SetCellContent(3, 0, this.textBox64);
            this.table2.Body.SetCellContent(3, 1, this.textBox65);
            this.table2.Body.SetCellContent(3, 3, this.textBox66);
            this.table2.Body.SetCellContent(0, 5, this.textBox67);
            this.table2.Body.SetCellContent(1, 5, this.textBox68);
            this.table2.Body.SetCellContent(3, 5, this.textBox69);
            this.table2.Body.SetCellContent(1, 7, this.textBox71);
            this.table2.Body.SetCellContent(3, 7, this.textBox72);
            this.table2.Body.SetCellContent(0, 3, this.textBox73);
            this.table2.Body.SetCellContent(5, 0, this.textBox70);
            this.table2.Body.SetCellContent(5, 1, this.textBox74);
            this.table2.Body.SetCellContent(5, 3, this.textBox75);
            this.table2.Body.SetCellContent(5, 5, this.textBox76);
            this.table2.Body.SetCellContent(5, 7, this.textBox77);
            this.table2.Body.SetCellContent(7, 0, this.textBox78);
            this.table2.Body.SetCellContent(7, 1, this.textBox79);
            this.table2.Body.SetCellContent(7, 3, this.textBox80);
            this.table2.Body.SetCellContent(7, 5, this.textBox81);
            this.table2.Body.SetCellContent(7, 7, this.textBox82);
            this.table2.Body.SetCellContent(0, 2, this.textBox85);
            this.table2.Body.SetCellContent(1, 2, this.textBox86);
            this.table2.Body.SetCellContent(3, 2, this.textBox87);
            this.table2.Body.SetCellContent(5, 2, this.textBox88);
            this.table2.Body.SetCellContent(7, 2, this.textBox89);
            this.table2.Body.SetCellContent(0, 4, this.textBox90);
            this.table2.Body.SetCellContent(1, 4, this.textBox91);
            this.table2.Body.SetCellContent(3, 4, this.textBox92);
            this.table2.Body.SetCellContent(5, 4, this.textBox93);
            this.table2.Body.SetCellContent(7, 4, this.textBox94);
            this.table2.Body.SetCellContent(0, 6, this.textBox95);
            this.table2.Body.SetCellContent(1, 6, this.textBox96);
            this.table2.Body.SetCellContent(3, 6, this.textBox97);
            this.table2.Body.SetCellContent(5, 6, this.textBox98);
            this.table2.Body.SetCellContent(7, 6, this.textBox99);
            this.table2.Body.SetCellContent(2, 0, this.textBox155);
            this.table2.Body.SetCellContent(2, 1, this.textBox156);
            this.table2.Body.SetCellContent(2, 2, this.textBox162);
            this.table2.Body.SetCellContent(2, 3, this.textBox168);
            this.table2.Body.SetCellContent(2, 4, this.textBox174);
            this.table2.Body.SetCellContent(2, 5, this.textBox180);
            this.table2.Body.SetCellContent(2, 6, this.textBox186);
            this.table2.Body.SetCellContent(2, 7, this.textBox192);
            this.table2.Body.SetCellContent(4, 0, this.textBox193);
            this.table2.Body.SetCellContent(4, 1, this.textBox194);
            this.table2.Body.SetCellContent(4, 2, this.textBox195);
            this.table2.Body.SetCellContent(4, 3, this.textBox196);
            this.table2.Body.SetCellContent(4, 4, this.textBox197);
            this.table2.Body.SetCellContent(4, 5, this.textBox198);
            this.table2.Body.SetCellContent(4, 6, this.textBox199);
            this.table2.Body.SetCellContent(4, 7, this.textBox200);
            this.table2.Body.SetCellContent(6, 0, this.textBox201);
            this.table2.Body.SetCellContent(6, 1, this.textBox202);
            this.table2.Body.SetCellContent(6, 2, this.textBox203);
            this.table2.Body.SetCellContent(6, 3, this.textBox204);
            this.table2.Body.SetCellContent(6, 4, this.textBox205);
            this.table2.Body.SetCellContent(6, 5, this.textBox206);
            this.table2.Body.SetCellContent(6, 6, this.textBox207);
            this.table2.Body.SetCellContent(6, 7, this.textBox208);
            this.table2.Body.SetCellContent(1, 8, this.textBox331);
            this.table2.Body.SetCellContent(2, 8, this.textBox341);
            this.table2.Body.SetCellContent(3, 8, this.textBox342);
            this.table2.Body.SetCellContent(4, 8, this.textBox343);
            this.table2.Body.SetCellContent(5, 8, this.textBox344);
            this.table2.Body.SetCellContent(6, 8, this.textBox345);
            this.table2.Body.SetCellContent(7, 8, this.textBox346);
            this.table2.Body.SetCellContent(0, 7, this.textBox62, 1, 2);
            tableGroup7.Name = "tableGroup3";
            tableGroup8.Name = "tableGroup4";
            tableGroup9.Name = "group9";
            tableGroup10.Name = "tableGroup5";
            tableGroup11.Name = "group10";
            tableGroup12.Name = "group5";
            tableGroup13.Name = "group11";
            tableGroup14.Name = "group6";
            tableGroup15.Name = "group64";
            this.table2.ColumnGroups.Add(tableGroup7);
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.ColumnGroups.Add(tableGroup10);
            this.table2.ColumnGroups.Add(tableGroup11);
            this.table2.ColumnGroups.Add(tableGroup12);
            this.table2.ColumnGroups.Add(tableGroup13);
            this.table2.ColumnGroups.Add(tableGroup14);
            this.table2.ColumnGroups.Add(tableGroup15);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox58,
            this.textBox60,
            this.textBox85,
            this.textBox73,
            this.textBox90,
            this.textBox67,
            this.textBox95,
            this.textBox62,
            this.textBox59,
            this.textBox61,
            this.textBox86,
            this.textBox63,
            this.textBox91,
            this.textBox68,
            this.textBox96,
            this.textBox71,
            this.textBox331,
            this.textBox155,
            this.textBox156,
            this.textBox162,
            this.textBox168,
            this.textBox174,
            this.textBox180,
            this.textBox186,
            this.textBox192,
            this.textBox341,
            this.textBox64,
            this.textBox65,
            this.textBox87,
            this.textBox66,
            this.textBox92,
            this.textBox69,
            this.textBox97,
            this.textBox72,
            this.textBox342,
            this.textBox193,
            this.textBox194,
            this.textBox195,
            this.textBox196,
            this.textBox197,
            this.textBox198,
            this.textBox199,
            this.textBox200,
            this.textBox343,
            this.textBox70,
            this.textBox74,
            this.textBox88,
            this.textBox75,
            this.textBox93,
            this.textBox76,
            this.textBox98,
            this.textBox77,
            this.textBox344,
            this.textBox201,
            this.textBox202,
            this.textBox203,
            this.textBox204,
            this.textBox205,
            this.textBox206,
            this.textBox207,
            this.textBox208,
            this.textBox345,
            this.textBox78,
            this.textBox79,
            this.textBox89,
            this.textBox80,
            this.textBox94,
            this.textBox81,
            this.textBox99,
            this.textBox82,
            this.textBox346});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.099999949336051941D), Telerik.Reporting.Drawing.Unit.Cm(32.099998474121094D));
            this.table2.Name = "table2";
            tableGroup17.Name = "group3";
            tableGroup18.Name = "group2";
            tableGroup19.Name = "group26";
            tableGroup20.Name = "group4";
            tableGroup21.Name = "group33";
            tableGroup22.Name = "group7";
            tableGroup23.Name = "group34";
            tableGroup24.Name = "group8";
            tableGroup16.ChildGroups.Add(tableGroup17);
            tableGroup16.ChildGroups.Add(tableGroup18);
            tableGroup16.ChildGroups.Add(tableGroup19);
            tableGroup16.ChildGroups.Add(tableGroup20);
            tableGroup16.ChildGroups.Add(tableGroup21);
            tableGroup16.ChildGroups.Add(tableGroup22);
            tableGroup16.ChildGroups.Add(tableGroup23);
            tableGroup16.ChildGroups.Add(tableGroup24);
            tableGroup16.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup16.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup16);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.561206817626953D), Telerik.Reporting.Drawing.Unit.Cm(4.2937502861022949D));
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1396923065185547D), Telerik.Reporting.Drawing.Unit.Cm(0.5000002384185791D));
            this.textBox58.StyleName = "";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1621320247650146D), Telerik.Reporting.Drawing.Unit.Cm(0.5000002384185791D));
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.StyleName = "";
            this.textBox60.Value = "Solar PV(kW)";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1396923065185547D), Telerik.Reporting.Drawing.Unit.Cm(0.60583323240280151D));
            this.textBox59.StyleName = "";
            this.textBox59.Value = "New";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1621320247650146D), Telerik.Reporting.Drawing.Unit.Cm(0.60583323240280151D));
            this.textBox61.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox61.StyleName = "";
            this.textBox61.Value = "6.61";
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8277029991149902D), Telerik.Reporting.Drawing.Unit.Cm(0.60583323240280151D));
            this.textBox63.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox63.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox63.StyleName = "";
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1396923065185547D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox64.StyleName = "";
            this.textBox64.Value = "Existing Retained";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1621320247650146D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox65.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox65.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox65.StyleName = "";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8277029991149902D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox66.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox66.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox66.StyleName = "";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0058085918426514D), Telerik.Reporting.Drawing.Unit.Cm(0.5000002384185791D));
            this.textBox67.Style.Font.Bold = true;
            this.textBox67.StyleName = "";
            this.textBox67.Value = "Other * (kVA)";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0058085918426514D), Telerik.Reporting.Drawing.Unit.Cm(0.60583323240280151D));
            this.textBox68.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox68.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox68.StyleName = "";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0058085918426514D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox69.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox69.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox69.StyleName = "";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7030184268951416D), Telerik.Reporting.Drawing.Unit.Cm(0.60583323240280151D));
            this.textBox71.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox71.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox71.StyleName = "";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7030184268951416D), Telerik.Reporting.Drawing.Unit.Cm(0.71166664361953735D));
            this.textBox72.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox72.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox72.StyleName = "";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8277029991149902D), Telerik.Reporting.Drawing.Unit.Cm(0.5000002384185791D));
            this.textBox73.Style.Font.Bold = true;
            this.textBox73.StyleName = "";
            this.textBox73.Value = "Wind(kVA)";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1396923065185547D), Telerik.Reporting.Drawing.Unit.Cm(0.65875011682510376D));
            this.textBox70.StyleName = "";
            this.textBox70.Value = "Existing Removed";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1621320247650146D), Telerik.Reporting.Drawing.Unit.Cm(0.65875011682510376D));
            this.textBox74.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox74.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox74.StyleName = "";
            // 
            // textBox75
            // 
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8277029991149902D), Telerik.Reporting.Drawing.Unit.Cm(0.65875011682510376D));
            this.textBox75.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox75.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox75.StyleName = "";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0058085918426514D), Telerik.Reporting.Drawing.Unit.Cm(0.65875011682510376D));
            this.textBox76.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox76.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox76.StyleName = "";
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7030184268951416D), Telerik.Reporting.Drawing.Unit.Cm(0.65875023603439331D));
            this.textBox77.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox77.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox77.StyleName = "";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1396923065185547D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox78.StyleName = "";
            this.textBox78.Value = "Total capacity";
            // 
            // textBox79
            // 
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1621320247650146D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox79.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox79.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox79.StyleName = "";
            this.textBox79.Value = "6.61";
            // 
            // textBox80
            // 
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8277029991149902D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox80.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox80.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox80.StyleName = "";
            // 
            // textBox81
            // 
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0058085918426514D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox81.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox81.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox81.StyleName = "";
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7030184268951416D), Telerik.Reporting.Drawing.Unit.Cm(0.71166664361953735D));
            this.textBox82.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox82.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox82.StyleName = "";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.5000002384185791D));
            this.textBox85.Style.Font.Bold = true;
            this.textBox85.StyleName = "";
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.60583323240280151D));
            this.textBox86.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox86.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox86.StyleName = "";
            // 
            // textBox87
            // 
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox87.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox87.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox87.StyleName = "";
            // 
            // textBox88
            // 
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.65875011682510376D));
            this.textBox88.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox88.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox88.StyleName = "";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox89.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox89.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox89.StyleName = "";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.5000002384185791D));
            this.textBox90.Style.Font.Bold = true;
            this.textBox90.StyleName = "";
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.60583323240280151D));
            this.textBox91.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox91.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox91.StyleName = "";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox92.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox92.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox92.StyleName = "";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.65875011682510376D));
            this.textBox93.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox93.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox93.StyleName = "";
            // 
            // textBox94
            // 
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox94.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox94.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox94.StyleName = "";
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.52969646453857422D), Telerik.Reporting.Drawing.Unit.Cm(0.5000002384185791D));
            this.textBox95.Style.Font.Bold = true;
            this.textBox95.StyleName = "";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.52969646453857422D), Telerik.Reporting.Drawing.Unit.Cm(0.60583323240280151D));
            this.textBox96.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox96.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox96.StyleName = "";
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.52969646453857422D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox97.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox97.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox97.StyleName = "";
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.52969646453857422D), Telerik.Reporting.Drawing.Unit.Cm(0.65875011682510376D));
            this.textBox98.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox98.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox98.StyleName = "";
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.52969646453857422D), Telerik.Reporting.Drawing.Unit.Cm(0.71166658401489258D));
            this.textBox99.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox99.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox99.StyleName = "";
            // 
            // textBox155
            // 
            this.textBox155.Name = "textBox155";
            this.textBox155.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1396923065185547D), Telerik.Reporting.Drawing.Unit.Cm(0.3412499725818634D));
            this.textBox155.StyleName = "";
            // 
            // textBox156
            // 
            this.textBox156.Name = "textBox156";
            this.textBox156.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1621320247650146D), Telerik.Reporting.Drawing.Unit.Cm(0.3412499725818634D));
            this.textBox156.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox156.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox156.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox156.StyleName = "";
            // 
            // textBox162
            // 
            this.textBox162.Name = "textBox162";
            this.textBox162.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.3412499725818634D));
            this.textBox162.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox162.StyleName = "";
            // 
            // textBox168
            // 
            this.textBox168.Name = "textBox168";
            this.textBox168.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8277029991149902D), Telerik.Reporting.Drawing.Unit.Cm(0.3412499725818634D));
            this.textBox168.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox168.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox168.StyleName = "";
            // 
            // textBox174
            // 
            this.textBox174.Name = "textBox174";
            this.textBox174.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.3412499725818634D));
            this.textBox174.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox174.StyleName = "";
            // 
            // textBox180
            // 
            this.textBox180.Name = "textBox180";
            this.textBox180.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0058085918426514D), Telerik.Reporting.Drawing.Unit.Cm(0.3412499725818634D));
            this.textBox180.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox180.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox180.StyleName = "";
            // 
            // textBox186
            // 
            this.textBox186.Name = "textBox186";
            this.textBox186.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.52969646453857422D), Telerik.Reporting.Drawing.Unit.Cm(0.3412499725818634D));
            this.textBox186.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox186.StyleName = "";
            // 
            // textBox192
            // 
            this.textBox192.Name = "textBox192";
            this.textBox192.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7030184268951416D), Telerik.Reporting.Drawing.Unit.Cm(0.34125003218650818D));
            this.textBox192.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox192.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox192.StyleName = "";
            // 
            // textBox193
            // 
            this.textBox193.Name = "textBox193";
            this.textBox193.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1396923065185547D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox193.StyleName = "";
            // 
            // textBox194
            // 
            this.textBox194.Name = "textBox194";
            this.textBox194.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1621320247650146D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox194.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox194.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox194.StyleName = "";
            // 
            // textBox195
            // 
            this.textBox195.Name = "textBox195";
            this.textBox195.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox195.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox195.StyleName = "";
            // 
            // textBox196
            // 
            this.textBox196.Name = "textBox196";
            this.textBox196.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8277029991149902D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox196.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox196.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox196.StyleName = "";
            // 
            // textBox197
            // 
            this.textBox197.Name = "textBox197";
            this.textBox197.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox197.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox197.StyleName = "";
            // 
            // textBox198
            // 
            this.textBox198.Name = "textBox198";
            this.textBox198.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0058085918426514D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox198.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox198.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox198.StyleName = "";
            // 
            // textBox199
            // 
            this.textBox199.Name = "textBox199";
            this.textBox199.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.52969646453857422D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox199.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox199.StyleName = "";
            // 
            // textBox200
            // 
            this.textBox200.Name = "textBox200";
            this.textBox200.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7030184268951416D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox200.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox200.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox200.StyleName = "";
            // 
            // textBox201
            // 
            this.textBox201.Name = "textBox201";
            this.textBox201.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1396923065185547D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox201.StyleName = "";
            // 
            // textBox202
            // 
            this.textBox202.Name = "textBox202";
            this.textBox202.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1621320247650146D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox202.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox202.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox202.StyleName = "";
            // 
            // textBox203
            // 
            this.textBox203.Name = "textBox203";
            this.textBox203.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox203.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox203.StyleName = "";
            // 
            // textBox204
            // 
            this.textBox204.Name = "textBox204";
            this.textBox204.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8277029991149902D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox204.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox204.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox204.StyleName = "";
            // 
            // textBox205
            // 
            this.textBox205.Name = "textBox205";
            this.textBox205.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.50397145748138428D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox205.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox205.StyleName = "";
            // 
            // textBox206
            // 
            this.textBox206.Name = "textBox206";
            this.textBox206.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0058085918426514D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox206.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox206.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox206.StyleName = "";
            // 
            // textBox207
            // 
            this.textBox207.Name = "textBox207";
            this.textBox207.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.52969646453857422D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox207.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox207.StyleName = "";
            // 
            // textBox208
            // 
            this.textBox208.Name = "textBox208";
            this.textBox208.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7030184268951416D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox208.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox208.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox208.StyleName = "";
            // 
            // textBox331
            // 
            this.textBox331.Name = "textBox331";
            this.textBox331.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1852085590362549D), Telerik.Reporting.Drawing.Unit.Cm(0.60583323240280151D));
            this.textBox331.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox331.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox331.StyleName = "";
            // 
            // textBox341
            // 
            this.textBox341.Name = "textBox341";
            this.textBox341.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1852085590362549D), Telerik.Reporting.Drawing.Unit.Cm(0.34125003218650818D));
            this.textBox341.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox341.StyleName = "";
            // 
            // textBox342
            // 
            this.textBox342.Name = "textBox342";
            this.textBox342.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1852085590362549D), Telerik.Reporting.Drawing.Unit.Cm(0.71166664361953735D));
            this.textBox342.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox342.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox342.StyleName = "";
            // 
            // textBox343
            // 
            this.textBox343.Name = "textBox343";
            this.textBox343.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1852085590362549D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox343.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox343.StyleName = "";
            // 
            // textBox344
            // 
            this.textBox344.Name = "textBox344";
            this.textBox344.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1852085590362549D), Telerik.Reporting.Drawing.Unit.Cm(0.65875023603439331D));
            this.textBox344.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox344.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox344.StyleName = "";
            // 
            // textBox345
            // 
            this.textBox345.Name = "textBox345";
            this.textBox345.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1852085590362549D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox345.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox345.StyleName = "";
            // 
            // textBox346
            // 
            this.textBox346.Name = "textBox346";
            this.textBox346.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1852085590362549D), Telerik.Reporting.Drawing.Unit.Cm(0.71166664361953735D));
            this.textBox346.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox346.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox346.StyleName = "";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.8882269859313965D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox62.Style.Font.Bold = true;
            this.textBox62.StyleName = "";
            this.textBox62.Value = "Energy Storage System (kWh)";
            // 
            // textBox83
            // 
            this.textBox83.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(37.099998474121094D));
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.37978458404541D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox83.Value = "* If �Other� please specify:";
            // 
            // textBox84
            // 
            this.textBox84.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.3799848556518555D), Telerik.Reporting.Drawing.Unit.Cm(37.099998474121094D));
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.0436210632324219D), Telerik.Reporting.Drawing.Unit.Cm(0.59999889135360718D));
            this.textBox84.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox84.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox84.Value = "";
            // 
            // textBox100
            // 
            this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(38.200000762939453D));
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox100.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox100.Style.Font.Bold = true;
            this.textBox100.Style.Font.Italic = true;
            this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(17D);
            this.textBox100.Style.Font.Underline = false;
            this.textBox100.Value = "Capacity per phase";
            // 
            // textBox101
            // 
            this.textBox101.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.4000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(38.200000762939453D));
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.399797439575195D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox101.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox101.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox101.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox101.Value = "(Only required for 2-phase and 3-phase generators and energy storage systems)";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.59353494644165D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1485373973846436D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.7564578652381897D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.5454158782958984D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.677082359790802D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.3758397102355957D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000405311584473D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox102);
            this.table3.Body.SetCellContent(2, 0, this.textBox103);
            this.table3.Body.SetCellContent(2, 1, this.textBox105);
            this.table3.Body.SetCellContent(2, 3, this.textBox107);
            this.table3.Body.SetCellContent(4, 0, this.textBox108);
            this.table3.Body.SetCellContent(4, 1, this.textBox109);
            this.table3.Body.SetCellContent(4, 3, this.textBox110);
            this.table3.Body.SetCellContent(2, 5, this.textBox112);
            this.table3.Body.SetCellContent(4, 5, this.textBox113);
            this.table3.Body.SetCellContent(6, 0, this.textBox114);
            this.table3.Body.SetCellContent(6, 1, this.textBox115);
            this.table3.Body.SetCellContent(6, 3, this.textBox116);
            this.table3.Body.SetCellContent(6, 5, this.textBox117);
            this.table3.Body.SetCellContent(0, 1, this.textBox118);
            this.table3.Body.SetCellContent(0, 3, this.textBox104);
            this.table3.Body.SetCellContent(0, 5, this.textBox106);
            this.table3.Body.SetCellContent(1, 0, this.textBox111);
            this.table3.Body.SetCellContent(1, 1, this.textBox119);
            this.table3.Body.SetCellContent(1, 3, this.textBox120);
            this.table3.Body.SetCellContent(1, 5, this.textBox121);
            this.table3.Body.SetCellContent(3, 0, this.textBox122);
            this.table3.Body.SetCellContent(3, 1, this.textBox123);
            this.table3.Body.SetCellContent(3, 3, this.textBox124);
            this.table3.Body.SetCellContent(3, 5, this.textBox125);
            this.table3.Body.SetCellContent(5, 0, this.textBox126);
            this.table3.Body.SetCellContent(5, 1, this.textBox127);
            this.table3.Body.SetCellContent(5, 3, this.textBox128);
            this.table3.Body.SetCellContent(5, 5, this.textBox129);
            this.table3.Body.SetCellContent(0, 2, this.textBox130);
            this.table3.Body.SetCellContent(1, 2, this.textBox131);
            this.table3.Body.SetCellContent(2, 2, this.textBox132);
            this.table3.Body.SetCellContent(3, 2, this.textBox133);
            this.table3.Body.SetCellContent(4, 2, this.textBox134);
            this.table3.Body.SetCellContent(5, 2, this.textBox135);
            this.table3.Body.SetCellContent(6, 2, this.textBox136);
            this.table3.Body.SetCellContent(0, 4, this.textBox137);
            this.table3.Body.SetCellContent(1, 4, this.textBox138);
            this.table3.Body.SetCellContent(2, 4, this.textBox139);
            this.table3.Body.SetCellContent(3, 4, this.textBox140);
            this.table3.Body.SetCellContent(4, 4, this.textBox141);
            this.table3.Body.SetCellContent(5, 4, this.textBox142);
            this.table3.Body.SetCellContent(6, 4, this.textBox143);
            tableGroup25.Name = "tableGroup6";
            tableGroup26.Name = "tableGroup7";
            tableGroup27.Name = "group20";
            tableGroup28.Name = "tableGroup8";
            tableGroup29.Name = "group21";
            tableGroup30.Name = "group15";
            this.table3.ColumnGroups.Add(tableGroup25);
            this.table3.ColumnGroups.Add(tableGroup26);
            this.table3.ColumnGroups.Add(tableGroup27);
            this.table3.ColumnGroups.Add(tableGroup28);
            this.table3.ColumnGroups.Add(tableGroup29);
            this.table3.ColumnGroups.Add(tableGroup30);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox102,
            this.textBox118,
            this.textBox130,
            this.textBox104,
            this.textBox137,
            this.textBox106,
            this.textBox111,
            this.textBox119,
            this.textBox131,
            this.textBox120,
            this.textBox138,
            this.textBox121,
            this.textBox103,
            this.textBox105,
            this.textBox132,
            this.textBox107,
            this.textBox139,
            this.textBox112,
            this.textBox122,
            this.textBox123,
            this.textBox133,
            this.textBox124,
            this.textBox140,
            this.textBox125,
            this.textBox108,
            this.textBox109,
            this.textBox134,
            this.textBox110,
            this.textBox141,
            this.textBox113,
            this.textBox126,
            this.textBox127,
            this.textBox135,
            this.textBox128,
            this.textBox142,
            this.textBox129,
            this.textBox114,
            this.textBox115,
            this.textBox136,
            this.textBox116,
            this.textBox143,
            this.textBox117});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(39.200000762939453D));
            this.table3.Name = "table3";
            tableGroup32.Name = "group13";
            tableGroup33.Name = "group17";
            tableGroup34.Name = "group12";
            tableGroup35.Name = "group18";
            tableGroup36.Name = "group14";
            tableGroup37.Name = "group19";
            tableGroup38.Name = "group16";
            tableGroup31.ChildGroups.Add(tableGroup32);
            tableGroup31.ChildGroups.Add(tableGroup33);
            tableGroup31.ChildGroups.Add(tableGroup34);
            tableGroup31.ChildGroups.Add(tableGroup35);
            tableGroup31.ChildGroups.Add(tableGroup36);
            tableGroup31.ChildGroups.Add(tableGroup37);
            tableGroup31.ChildGroups.Add(tableGroup38);
            tableGroup31.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup31.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup31);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.096868515014648D), Telerik.Reporting.Drawing.Unit.Cm(2.7937667369842529D));
            // 
            // textBox102
            // 
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5935382843017578D), Telerik.Reporting.Drawing.Unit.Cm(0.5000039935112D));
            this.textBox102.Style.Font.Bold = true;
            this.textBox102.StyleName = "";
            this.textBox102.Value = "Total Installed Capacity Per Phase";
            // 
            // textBox103
            // 
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5935382843017578D), Telerik.Reporting.Drawing.Unit.Cm(0.50000429153442383D));
            this.textBox103.StyleName = "";
            this.textBox103.Value = "2- or 3-phase generators (kVA)";
            // 
            // textBox105
            // 
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1485376358032227D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox105.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox105.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox105.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox105.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox105.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox105.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox105.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox105.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox105.StyleName = "";
            // 
            // textBox107
            // 
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5454168319702148D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox107.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox107.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox107.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox107.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox107.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox107.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox107.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox107.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox107.StyleName = "";
            // 
            // textBox108
            // 
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5935382843017578D), Telerik.Reporting.Drawing.Unit.Cm(0.50000417232513428D));
            this.textBox108.StyleName = "";
            this.textBox108.Value = "2- or 3-phase energy storage system (kWh)";
            // 
            // textBox109
            // 
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1485376358032227D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox109.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox109.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox109.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox109.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox109.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox109.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox109.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox109.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox109.StyleName = "";
            // 
            // textBox110
            // 
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5454168319702148D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox110.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox110.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox110.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox110.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox110.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox110.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox110.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox110.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox110.StyleName = "";
            // 
            // textBox112
            // 
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3758394718170166D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox112.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox112.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox112.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox112.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox112.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox112.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox112.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox112.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox112.StyleName = "";
            // 
            // textBox113
            // 
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3758394718170166D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox113.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox113.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox113.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox113.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox113.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox113.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox113.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox113.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox113.StyleName = "";
            // 
            // textBox114
            // 
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.5935382843017578D), Telerik.Reporting.Drawing.Unit.Cm(0.50000417232513428D));
            this.textBox114.Style.Font.Bold = true;
            this.textBox114.StyleName = "";
            this.textBox114.Value = "Total";
            // 
            // textBox115
            // 
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1485376358032227D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox115.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox115.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox115.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox115.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox115.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox115.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox115.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox115.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox115.StyleName = "";
            // 
            // textBox116
            // 
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5454168319702148D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox116.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox116.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox116.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox116.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox116.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox116.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox116.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox116.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox116.StyleName = "";
            // 
            // textBox117
            // 
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3758394718170166D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox117.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox117.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox117.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox117.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox117.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox117.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox117.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox117.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox117.StyleName = "";
            // 
            // textBox118
            // 
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1485376358032227D), Telerik.Reporting.Drawing.Unit.Cm(0.50000405311584473D));
            this.textBox118.Style.Font.Bold = true;
            this.textBox118.StyleName = "";
            this.textBox118.Value = "Red Phase";
            // 
            // textBox104
            // 
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5454168319702148D), Telerik.Reporting.Drawing.Unit.Cm(0.50000405311584473D));
            this.textBox104.Style.Font.Bold = true;
            this.textBox104.StyleName = "";
            this.textBox104.Value = "White Phase";
            // 
            // textBox106
            // 
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3758394718170166D), Telerik.Reporting.Drawing.Unit.Cm(0.50000405311584473D));
            this.textBox106.Style.Font.Bold = true;
            this.textBox106.StyleName = "";
            this.textBox106.Value = "Blue Phase";
            // 
            // textBox111
            // 
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.593538761138916D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox111.Style.Font.Bold = true;
            this.textBox111.StyleName = "";
            // 
            // textBox119
            // 
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1485376358032227D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox119.Style.Font.Bold = true;
            this.textBox119.StyleName = "";
            // 
            // textBox120
            // 
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5454168319702148D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox120.Style.Font.Bold = true;
            this.textBox120.StyleName = "";
            // 
            // textBox121
            // 
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3758394718170166D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox121.Style.Font.Bold = true;
            this.textBox121.StyleName = "";
            // 
            // textBox122
            // 
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.593538761138916D), Telerik.Reporting.Drawing.Unit.Cm(0.26458334922790527D));
            this.textBox122.StyleName = "";
            // 
            // textBox123
            // 
            this.textBox123.Name = "textBox123";
            this.textBox123.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1485376358032227D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox123.StyleName = "";
            // 
            // textBox124
            // 
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5454168319702148D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox124.StyleName = "";
            // 
            // textBox125
            // 
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3758394718170166D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox125.StyleName = "";
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.593538761138916D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox126.StyleName = "";
            // 
            // textBox127
            // 
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1485376358032227D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox127.StyleName = "";
            // 
            // textBox128
            // 
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5454168319702148D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox128.StyleName = "";
            // 
            // textBox129
            // 
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.3758394718170166D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox129.StyleName = "";
            // 
            // textBox130
            // 
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.75645822286605835D), Telerik.Reporting.Drawing.Unit.Cm(0.50000405311584473D));
            this.textBox130.Style.Font.Bold = true;
            this.textBox130.StyleName = "";
            // 
            // textBox131
            // 
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.75645822286605835D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox131.Style.Font.Bold = true;
            this.textBox131.StyleName = "";
            // 
            // textBox132
            // 
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.75645822286605835D), Telerik.Reporting.Drawing.Unit.Cm(0.50000429153442383D));
            this.textBox132.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox132.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox132.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox132.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox132.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox132.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox132.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox132.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox132.StyleName = "";
            // 
            // textBox133
            // 
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.75645822286605835D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox133.StyleName = "";
            // 
            // textBox134
            // 
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.75645822286605835D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox134.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox134.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox134.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox134.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox134.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox134.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox134.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox134.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox134.StyleName = "";
            // 
            // textBox135
            // 
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.75645822286605835D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox135.StyleName = "";
            // 
            // textBox136
            // 
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.75645822286605835D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox136.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox136.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox136.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox136.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox136.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox136.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox136.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox136.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox136.StyleName = "";
            // 
            // textBox137
            // 
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.67708325386047363D), Telerik.Reporting.Drawing.Unit.Cm(0.50000405311584473D));
            this.textBox137.Style.Font.Bold = true;
            this.textBox137.StyleName = "";
            // 
            // textBox138
            // 
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.67708325386047363D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox138.Style.Font.Bold = true;
            this.textBox138.StyleName = "";
            // 
            // textBox139
            // 
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.67708325386047363D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox139.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox139.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox139.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox139.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox139.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox139.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox139.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox139.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox139.StyleName = "";
            // 
            // textBox140
            // 
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.67708325386047363D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox140.StyleName = "";
            // 
            // textBox141
            // 
            this.textBox141.Name = "textBox141";
            this.textBox141.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.67708325386047363D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox141.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox141.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox141.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox141.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox141.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox141.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox141.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox141.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox141.StyleName = "";
            // 
            // textBox142
            // 
            this.textBox142.Name = "textBox142";
            this.textBox142.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.67708325386047363D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox142.StyleName = "";
            // 
            // textBox143
            // 
            this.textBox143.Name = "textBox143";
            this.textBox143.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.67708325386047363D), Telerik.Reporting.Drawing.Unit.Cm(0.500004231929779D));
            this.textBox143.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox143.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox143.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox143.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox143.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox143.Style.BorderWidth.Left = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox143.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox143.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox143.StyleName = "";
            // 
            // textBox144
            // 
            this.textBox144.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(42.5D));
            this.textBox144.Name = "textBox144";
            this.textBox144.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7795820236206055D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox144.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox144.Style.Font.Bold = true;
            this.textBox144.Style.Font.Italic = true;
            this.textBox144.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(17D);
            this.textBox144.Style.Font.Underline = false;
            this.textBox144.Value = "Inverter/Energy Storage Details";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9287035465240479D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.48062759637832642D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9816210269927979D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.55065363645553589D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.2816867828369141D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.59733796119689941D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.2591661214828491D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000065565109253D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.2645832896232605D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.36770844459533691D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.36770820617675781D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.42062550783157349D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.57937526702880859D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.55291670560836792D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.529166579246521D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.9497910737991333D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox145);
            this.table4.Body.SetCellContent(0, 1, this.textBox147);
            this.table4.Body.SetCellContent(0, 3, this.textBox149);
            this.table4.Body.SetCellContent(1, 0, this.textBox151);
            this.table4.Body.SetCellContent(1, 1, this.textBox152);
            this.table4.Body.SetCellContent(1, 3, this.textBox153);
            this.table4.Body.SetCellContent(0, 5, this.textBox146);
            this.table4.Body.SetCellContent(1, 5, this.textBox148);
            this.table4.Body.SetCellContent(1, 7, this.textBox154);
            this.table4.Body.SetCellContent(2, 0, this.textBox157);
            this.table4.Body.SetCellContent(2, 1, this.textBox158);
            this.table4.Body.SetCellContent(2, 3, this.textBox159);
            this.table4.Body.SetCellContent(2, 5, this.textBox160);
            this.table4.Body.SetCellContent(2, 7, this.textBox161);
            this.table4.Body.SetCellContent(3, 0, this.textBox163);
            this.table4.Body.SetCellContent(3, 1, this.textBox164);
            this.table4.Body.SetCellContent(3, 3, this.textBox165);
            this.table4.Body.SetCellContent(3, 5, this.textBox166);
            this.table4.Body.SetCellContent(3, 7, this.textBox167);
            this.table4.Body.SetCellContent(4, 0, this.textBox169);
            this.table4.Body.SetCellContent(4, 1, this.textBox170);
            this.table4.Body.SetCellContent(4, 3, this.textBox171);
            this.table4.Body.SetCellContent(4, 5, this.textBox172);
            this.table4.Body.SetCellContent(4, 7, this.textBox173);
            this.table4.Body.SetCellContent(5, 0, this.textBox175);
            this.table4.Body.SetCellContent(5, 1, this.textBox176);
            this.table4.Body.SetCellContent(5, 3, this.textBox177);
            this.table4.Body.SetCellContent(5, 5, this.textBox178);
            this.table4.Body.SetCellContent(5, 7, this.textBox179);
            this.table4.Body.SetCellContent(6, 0, this.textBox181);
            this.table4.Body.SetCellContent(6, 1, this.textBox182);
            this.table4.Body.SetCellContent(6, 3, this.textBox183);
            this.table4.Body.SetCellContent(6, 5, this.textBox184);
            this.table4.Body.SetCellContent(6, 7, this.textBox185);
            this.table4.Body.SetCellContent(7, 0, this.textBox187);
            this.table4.Body.SetCellContent(7, 1, this.textBox188);
            this.table4.Body.SetCellContent(7, 3, this.textBox189);
            this.table4.Body.SetCellContent(7, 5, this.textBox190);
            this.table4.Body.SetCellContent(7, 7, this.textBox191);
            this.table4.Body.SetCellContent(0, 2, this.textBox209);
            this.table4.Body.SetCellContent(1, 2, this.textBox210);
            this.table4.Body.SetCellContent(2, 2, this.textBox211);
            this.table4.Body.SetCellContent(3, 2, this.textBox212);
            this.table4.Body.SetCellContent(4, 2, this.textBox213);
            this.table4.Body.SetCellContent(5, 2, this.textBox214);
            this.table4.Body.SetCellContent(6, 2, this.textBox215);
            this.table4.Body.SetCellContent(7, 2, this.textBox216);
            this.table4.Body.SetCellContent(0, 4, this.textBox217);
            this.table4.Body.SetCellContent(1, 4, this.textBox218);
            this.table4.Body.SetCellContent(2, 4, this.textBox219);
            this.table4.Body.SetCellContent(3, 4, this.textBox220);
            this.table4.Body.SetCellContent(4, 4, this.textBox221);
            this.table4.Body.SetCellContent(5, 4, this.textBox222);
            this.table4.Body.SetCellContent(6, 4, this.textBox223);
            this.table4.Body.SetCellContent(7, 4, this.textBox224);
            this.table4.Body.SetCellContent(0, 6, this.textBox225);
            this.table4.Body.SetCellContent(1, 6, this.textBox226);
            this.table4.Body.SetCellContent(2, 6, this.textBox227);
            this.table4.Body.SetCellContent(3, 6, this.textBox228);
            this.table4.Body.SetCellContent(4, 6, this.textBox229);
            this.table4.Body.SetCellContent(5, 6, this.textBox230);
            this.table4.Body.SetCellContent(6, 6, this.textBox231);
            this.table4.Body.SetCellContent(7, 6, this.textBox232);
            this.table4.Body.SetCellContent(8, 0, this.textBox233);
            this.table4.Body.SetCellContent(8, 1, this.textBox234);
            this.table4.Body.SetCellContent(8, 2, this.textBox235);
            this.table4.Body.SetCellContent(8, 3, this.textBox236);
            this.table4.Body.SetCellContent(8, 4, this.textBox237);
            this.table4.Body.SetCellContent(8, 5, this.textBox238);
            this.table4.Body.SetCellContent(8, 6, this.textBox239);
            this.table4.Body.SetCellContent(8, 7, this.textBox240);
            this.table4.Body.SetCellContent(9, 0, this.textBox241);
            this.table4.Body.SetCellContent(9, 1, this.textBox242);
            this.table4.Body.SetCellContent(9, 2, this.textBox243);
            this.table4.Body.SetCellContent(9, 3, this.textBox244);
            this.table4.Body.SetCellContent(9, 4, this.textBox245);
            this.table4.Body.SetCellContent(9, 5, this.textBox246);
            this.table4.Body.SetCellContent(9, 6, this.textBox247);
            this.table4.Body.SetCellContent(9, 7, this.textBox248);
            this.table4.Body.SetCellContent(10, 0, this.textBox249);
            this.table4.Body.SetCellContent(10, 1, this.textBox250);
            this.table4.Body.SetCellContent(10, 2, this.textBox251);
            this.table4.Body.SetCellContent(10, 3, this.textBox252);
            this.table4.Body.SetCellContent(10, 4, this.textBox253);
            this.table4.Body.SetCellContent(10, 5, this.textBox254);
            this.table4.Body.SetCellContent(10, 6, this.textBox255);
            this.table4.Body.SetCellContent(10, 7, this.textBox256);
            this.table4.Body.SetCellContent(11, 2, this.textBox259);
            this.table4.Body.SetCellContent(11, 3, this.textBox260);
            this.table4.Body.SetCellContent(11, 4, this.textBox261);
            this.table4.Body.SetCellContent(11, 5, this.textBox262);
            this.table4.Body.SetCellContent(11, 6, this.textBox263);
            this.table4.Body.SetCellContent(11, 7, this.textBox264);
            this.table4.Body.SetCellContent(11, 1, this.textBox265);
            this.table4.Body.SetCellContent(13, 1, this.textBox266);
            this.table4.Body.SetCellContent(13, 2, this.textBox267);
            this.table4.Body.SetCellContent(13, 3, this.textBox268);
            this.table4.Body.SetCellContent(13, 4, this.textBox269);
            this.table4.Body.SetCellContent(13, 5, this.textBox270);
            this.table4.Body.SetCellContent(13, 6, this.textBox271);
            this.table4.Body.SetCellContent(13, 7, this.textBox272);
            this.table4.Body.SetCellContent(14, 0, this.textBox273);
            this.table4.Body.SetCellContent(14, 2, this.textBox275);
            this.table4.Body.SetCellContent(14, 6, this.textBox279);
            this.table4.Body.SetCellContent(14, 1, this.checkBox7);
            this.table4.Body.SetCellContent(14, 3, this.textBox276, 1, 3);
            this.table4.Body.SetCellContent(14, 7, this.checkBox8);
            this.table4.Body.SetCellContent(12, 1, this.textBox332);
            this.table4.Body.SetCellContent(12, 2, this.textBox333);
            this.table4.Body.SetCellContent(12, 3, this.textBox334);
            this.table4.Body.SetCellContent(12, 4, this.textBox335);
            this.table4.Body.SetCellContent(12, 5, this.textBox336);
            this.table4.Body.SetCellContent(12, 6, this.textBox337);
            this.table4.Body.SetCellContent(12, 7, this.textBox338);
            this.table4.Body.SetCellContent(11, 0, this.textBox257, 3, 1);
            this.table4.Body.SetCellContent(1, 8, this.textBox347);
            this.table4.Body.SetCellContent(2, 8, this.textBox348);
            this.table4.Body.SetCellContent(3, 8, this.textBox349);
            this.table4.Body.SetCellContent(4, 8, this.textBox350);
            this.table4.Body.SetCellContent(5, 8, this.textBox351);
            this.table4.Body.SetCellContent(6, 8, this.textBox352);
            this.table4.Body.SetCellContent(7, 8, this.textBox353);
            this.table4.Body.SetCellContent(8, 8, this.textBox354);
            this.table4.Body.SetCellContent(9, 8, this.textBox355);
            this.table4.Body.SetCellContent(10, 8, this.textBox356);
            this.table4.Body.SetCellContent(11, 8, this.textBox357);
            this.table4.Body.SetCellContent(12, 8, this.textBox358);
            this.table4.Body.SetCellContent(13, 8, this.textBox359);
            this.table4.Body.SetCellContent(14, 8, this.textBox360);
            this.table4.Body.SetCellContent(0, 7, this.textBox150, 1, 2);
            tableGroup39.Name = "tableGroup9";
            tableGroup40.Name = "tableGroup10";
            tableGroup41.Name = "group35";
            tableGroup42.Name = "tableGroup11";
            tableGroup43.Name = "group36";
            tableGroup44.Name = "group22";
            tableGroup45.Name = "group37";
            tableGroup46.Name = "group25";
            tableGroup47.Name = "group65";
            this.table4.ColumnGroups.Add(tableGroup39);
            this.table4.ColumnGroups.Add(tableGroup40);
            this.table4.ColumnGroups.Add(tableGroup41);
            this.table4.ColumnGroups.Add(tableGroup42);
            this.table4.ColumnGroups.Add(tableGroup43);
            this.table4.ColumnGroups.Add(tableGroup44);
            this.table4.ColumnGroups.Add(tableGroup45);
            this.table4.ColumnGroups.Add(tableGroup46);
            this.table4.ColumnGroups.Add(tableGroup47);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox145,
            this.textBox147,
            this.textBox209,
            this.textBox149,
            this.textBox217,
            this.textBox146,
            this.textBox225,
            this.textBox150,
            this.textBox151,
            this.textBox152,
            this.textBox210,
            this.textBox153,
            this.textBox218,
            this.textBox148,
            this.textBox226,
            this.textBox154,
            this.textBox347,
            this.textBox157,
            this.textBox158,
            this.textBox211,
            this.textBox159,
            this.textBox219,
            this.textBox160,
            this.textBox227,
            this.textBox161,
            this.textBox348,
            this.textBox163,
            this.textBox164,
            this.textBox212,
            this.textBox165,
            this.textBox220,
            this.textBox166,
            this.textBox228,
            this.textBox167,
            this.textBox349,
            this.textBox169,
            this.textBox170,
            this.textBox213,
            this.textBox171,
            this.textBox221,
            this.textBox172,
            this.textBox229,
            this.textBox173,
            this.textBox350,
            this.textBox175,
            this.textBox176,
            this.textBox214,
            this.textBox177,
            this.textBox222,
            this.textBox178,
            this.textBox230,
            this.textBox179,
            this.textBox351,
            this.textBox181,
            this.textBox182,
            this.textBox215,
            this.textBox183,
            this.textBox223,
            this.textBox184,
            this.textBox231,
            this.textBox185,
            this.textBox352,
            this.textBox187,
            this.textBox188,
            this.textBox216,
            this.textBox189,
            this.textBox224,
            this.textBox190,
            this.textBox232,
            this.textBox191,
            this.textBox353,
            this.textBox233,
            this.textBox234,
            this.textBox235,
            this.textBox236,
            this.textBox237,
            this.textBox238,
            this.textBox239,
            this.textBox240,
            this.textBox354,
            this.textBox241,
            this.textBox242,
            this.textBox243,
            this.textBox244,
            this.textBox245,
            this.textBox246,
            this.textBox247,
            this.textBox248,
            this.textBox355,
            this.textBox249,
            this.textBox250,
            this.textBox251,
            this.textBox252,
            this.textBox253,
            this.textBox254,
            this.textBox255,
            this.textBox256,
            this.textBox356,
            this.textBox257,
            this.textBox265,
            this.textBox259,
            this.textBox260,
            this.textBox261,
            this.textBox262,
            this.textBox263,
            this.textBox264,
            this.textBox357,
            this.textBox332,
            this.textBox333,
            this.textBox334,
            this.textBox335,
            this.textBox336,
            this.textBox337,
            this.textBox338,
            this.textBox358,
            this.textBox266,
            this.textBox267,
            this.textBox268,
            this.textBox269,
            this.textBox270,
            this.textBox271,
            this.textBox272,
            this.textBox359,
            this.textBox273,
            this.checkBox7,
            this.textBox275,
            this.textBox276,
            this.textBox279,
            this.checkBox8,
            this.textBox360});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(43.900001525878906D));
            this.table4.Name = "table4";
            tableGroup49.Name = "group23";
            tableGroup50.Name = "group24";
            tableGroup51.Name = "group27";
            tableGroup52.Name = "group28";
            tableGroup53.Name = "group29";
            tableGroup54.Name = "group30";
            tableGroup55.Name = "group31";
            tableGroup56.Name = "group32";
            tableGroup57.Name = "group38";
            tableGroup58.Name = "group39";
            tableGroup59.Name = "group40";
            tableGroup60.Name = "group41";
            tableGroup61.Name = "group59";
            tableGroup62.Name = "group42";
            tableGroup63.Name = "group43";
            tableGroup48.ChildGroups.Add(tableGroup49);
            tableGroup48.ChildGroups.Add(tableGroup50);
            tableGroup48.ChildGroups.Add(tableGroup51);
            tableGroup48.ChildGroups.Add(tableGroup52);
            tableGroup48.ChildGroups.Add(tableGroup53);
            tableGroup48.ChildGroups.Add(tableGroup54);
            tableGroup48.ChildGroups.Add(tableGroup55);
            tableGroup48.ChildGroups.Add(tableGroup56);
            tableGroup48.ChildGroups.Add(tableGroup57);
            tableGroup48.ChildGroups.Add(tableGroup58);
            tableGroup48.ChildGroups.Add(tableGroup59);
            tableGroup48.ChildGroups.Add(tableGroup60);
            tableGroup48.ChildGroups.Add(tableGroup61);
            tableGroup48.ChildGroups.Add(tableGroup62);
            tableGroup48.ChildGroups.Add(tableGroup63);
            tableGroup48.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup48.Name = "detailTableGroup3";
            this.table4.RowGroups.Add(tableGroup48);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.8685302734375D), Telerik.Reporting.Drawing.Unit.Cm(7.5318765640258789D));
            // 
            // textBox145
            // 
            this.textBox145.Name = "textBox145";
            this.textBox145.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox145.StyleName = "";
            // 
            // textBox147
            // 
            this.textBox147.Name = "textBox147";
            this.textBox147.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox147.Style.Font.Bold = true;
            this.textBox147.StyleName = "";
            this.textBox147.Value = "Inverter Model 1";
            // 
            // textBox149
            // 
            this.textBox149.Name = "textBox149";
            this.textBox149.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox149.Style.Font.Bold = true;
            this.textBox149.StyleName = "";
            this.textBox149.Value = "Inverter Model 2";
            // 
            // textBox151
            // 
            this.textBox151.Name = "textBox151";
            this.textBox151.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000065565109253D));
            this.textBox151.StyleName = "";
            this.textBox151.Value = "Manufacturer";
            // 
            // textBox152
            // 
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.50000077486038208D));
            this.textBox152.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox152.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox152.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox152.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox152.StyleName = "";
            this.textBox152.Value = "Fronius International GmbH";
            // 
            // textBox153
            // 
            this.textBox153.Name = "textBox153";
            this.textBox153.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.50000077486038208D));
            this.textBox153.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox153.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox153.StyleName = "";
            // 
            // textBox146
            // 
            this.textBox146.Name = "textBox146";
            this.textBox146.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816858291625977D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox146.Style.Font.Bold = true;
            this.textBox146.StyleName = "";
            this.textBox146.Value = "Inverter Model 3";
            // 
            // textBox148
            // 
            this.textBox148.Name = "textBox148";
            this.textBox148.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816858291625977D), Telerik.Reporting.Drawing.Unit.Cm(0.50000077486038208D));
            this.textBox148.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox148.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox148.StyleName = "";
            // 
            // textBox154
            // 
            this.textBox154.Name = "textBox154";
            this.textBox154.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.50000065565109253D));
            this.textBox154.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox154.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox154.StyleName = "";
            // 
            // textBox157
            // 
            this.textBox157.Name = "textBox157";
            this.textBox157.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.2645832896232605D));
            this.textBox157.StyleName = "";
            // 
            // textBox158
            // 
            this.textBox158.Name = "textBox158";
            this.textBox158.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox158.StyleName = "";
            // 
            // textBox159
            // 
            this.textBox159.Name = "textBox159";
            this.textBox159.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox159.StyleName = "";
            // 
            // textBox160
            // 
            this.textBox160.Name = "textBox160";
            this.textBox160.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816858291625977D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox160.StyleName = "";
            // 
            // textBox161
            // 
            this.textBox161.Name = "textBox161";
            this.textBox161.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.2645832896232605D));
            this.textBox161.StyleName = "";
            // 
            // textBox163
            // 
            this.textBox163.Name = "textBox163";
            this.textBox163.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox163.StyleName = "";
            this.textBox163.Value = "Model Name";
            // 
            // textBox164
            // 
            this.textBox164.Name = "textBox164";
            this.textBox164.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox164.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox164.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox164.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox164.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox164.StyleName = "";
            this.textBox164.Value = "Symo 5.0-3-M";
            // 
            // textBox165
            // 
            this.textBox165.Name = "textBox165";
            this.textBox165.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox165.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox165.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox165.StyleName = "";
            // 
            // textBox166
            // 
            this.textBox166.Name = "textBox166";
            this.textBox166.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816858291625977D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox166.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox166.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox166.StyleName = "";
            // 
            // textBox167
            // 
            this.textBox167.Name = "textBox167";
            this.textBox167.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox167.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox167.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox167.StyleName = "";
            // 
            // textBox169
            // 
            this.textBox169.Name = "textBox169";
            this.textBox169.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.36770844459533691D));
            this.textBox169.StyleName = "";
            // 
            // textBox170
            // 
            this.textBox170.Name = "textBox170";
            this.textBox170.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.36770865321159363D));
            this.textBox170.StyleName = "";
            // 
            // textBox171
            // 
            this.textBox171.Name = "textBox171";
            this.textBox171.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.36770865321159363D));
            this.textBox171.StyleName = "";
            // 
            // textBox172
            // 
            this.textBox172.Name = "textBox172";
            this.textBox172.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816858291625977D), Telerik.Reporting.Drawing.Unit.Cm(0.36770865321159363D));
            this.textBox172.StyleName = "";
            // 
            // textBox173
            // 
            this.textBox173.Name = "textBox173";
            this.textBox173.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.36770844459533691D));
            this.textBox173.StyleName = "";
            // 
            // textBox175
            // 
            this.textBox175.Name = "textBox175";
            this.textBox175.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox175.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox175.StyleName = "";
            this.textBox175.Value = "Rating of Inverter / Storage System";
            // 
            // textBox176
            // 
            this.textBox176.Name = "textBox176";
            this.textBox176.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox176.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox176.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox176.StyleName = "";
            this.textBox176.Value = "6.61";
            // 
            // textBox177
            // 
            this.textBox177.Name = "textBox177";
            this.textBox177.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox177.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox177.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox177.StyleName = "";
            // 
            // textBox178
            // 
            this.textBox178.Name = "textBox178";
            this.textBox178.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816858291625977D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox178.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox178.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox178.StyleName = "";
            // 
            // textBox179
            // 
            this.textBox179.Name = "textBox179";
            this.textBox179.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox179.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox179.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox179.StyleName = "";
            // 
            // textBox181
            // 
            this.textBox181.Name = "textBox181";
            this.textBox181.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox181.StyleName = "";
            // 
            // textBox182
            // 
            this.textBox182.Name = "textBox182";
            this.textBox182.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox182.StyleName = "";
            // 
            // textBox183
            // 
            this.textBox183.Name = "textBox183";
            this.textBox183.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox183.StyleName = "";
            // 
            // textBox184
            // 
            this.textBox184.Name = "textBox184";
            this.textBox184.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816858291625977D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox184.StyleName = "";
            // 
            // textBox185
            // 
            this.textBox185.Name = "textBox185";
            this.textBox185.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox185.StyleName = "";
            // 
            // textBox187
            // 
            this.textBox187.Name = "textBox187";
            this.textBox187.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox187.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox187.StyleName = "";
            this.textBox187.Value = "Number of Inverters / Storage Systems";
            // 
            // textBox188
            // 
            this.textBox188.Name = "textBox188";
            this.textBox188.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox188.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox188.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox188.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox188.StyleName = "";
            this.textBox188.Value = "1";
            // 
            // textBox189
            // 
            this.textBox189.Name = "textBox189";
            this.textBox189.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox189.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox189.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox189.StyleName = "";
            // 
            // textBox190
            // 
            this.textBox190.Name = "textBox190";
            this.textBox190.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816858291625977D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox190.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox190.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox190.StyleName = "";
            // 
            // textBox191
            // 
            this.textBox191.Name = "textBox191";
            this.textBox191.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox191.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox191.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox191.StyleName = "";
            // 
            // textBox209
            // 
            this.textBox209.Name = "textBox209";
            this.textBox209.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox209.StyleName = "";
            // 
            // textBox210
            // 
            this.textBox210.Name = "textBox210";
            this.textBox210.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.50000077486038208D));
            this.textBox210.StyleName = "";
            // 
            // textBox211
            // 
            this.textBox211.Name = "textBox211";
            this.textBox211.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox211.StyleName = "";
            // 
            // textBox212
            // 
            this.textBox212.Name = "textBox212";
            this.textBox212.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox212.StyleName = "";
            // 
            // textBox213
            // 
            this.textBox213.Name = "textBox213";
            this.textBox213.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.36770865321159363D));
            this.textBox213.StyleName = "";
            // 
            // textBox214
            // 
            this.textBox214.Name = "textBox214";
            this.textBox214.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox214.StyleName = "";
            // 
            // textBox215
            // 
            this.textBox215.Name = "textBox215";
            this.textBox215.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox215.StyleName = "";
            // 
            // textBox216
            // 
            this.textBox216.Name = "textBox216";
            this.textBox216.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox216.StyleName = "";
            // 
            // textBox217
            // 
            this.textBox217.Name = "textBox217";
            this.textBox217.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065369606018066D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox217.StyleName = "";
            // 
            // textBox218
            // 
            this.textBox218.Name = "textBox218";
            this.textBox218.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065369606018066D), Telerik.Reporting.Drawing.Unit.Cm(0.50000077486038208D));
            this.textBox218.StyleName = "";
            // 
            // textBox219
            // 
            this.textBox219.Name = "textBox219";
            this.textBox219.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065369606018066D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox219.StyleName = "";
            // 
            // textBox220
            // 
            this.textBox220.Name = "textBox220";
            this.textBox220.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065369606018066D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox220.StyleName = "";
            // 
            // textBox221
            // 
            this.textBox221.Name = "textBox221";
            this.textBox221.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065369606018066D), Telerik.Reporting.Drawing.Unit.Cm(0.36770865321159363D));
            this.textBox221.StyleName = "";
            // 
            // textBox222
            // 
            this.textBox222.Name = "textBox222";
            this.textBox222.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065369606018066D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox222.StyleName = "";
            // 
            // textBox223
            // 
            this.textBox223.Name = "textBox223";
            this.textBox223.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065369606018066D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox223.StyleName = "";
            // 
            // textBox224
            // 
            this.textBox224.Name = "textBox224";
            this.textBox224.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065369606018066D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox224.StyleName = "";
            // 
            // textBox225
            // 
            this.textBox225.Name = "textBox225";
            this.textBox225.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox225.StyleName = "";
            // 
            // textBox226
            // 
            this.textBox226.Name = "textBox226";
            this.textBox226.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.50000077486038208D));
            this.textBox226.StyleName = "";
            // 
            // textBox227
            // 
            this.textBox227.Name = "textBox227";
            this.textBox227.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.26458331942558289D));
            this.textBox227.StyleName = "";
            // 
            // textBox228
            // 
            this.textBox228.Name = "textBox228";
            this.textBox228.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox228.StyleName = "";
            // 
            // textBox229
            // 
            this.textBox229.Name = "textBox229";
            this.textBox229.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.36770865321159363D));
            this.textBox229.StyleName = "";
            // 
            // textBox230
            // 
            this.textBox230.Name = "textBox230";
            this.textBox230.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox230.StyleName = "";
            // 
            // textBox231
            // 
            this.textBox231.Name = "textBox231";
            this.textBox231.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox231.StyleName = "";
            // 
            // textBox232
            // 
            this.textBox232.Name = "textBox232";
            this.textBox232.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox232.StyleName = "";
            // 
            // textBox233
            // 
            this.textBox233.Name = "textBox233";
            this.textBox233.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.36770820617675781D));
            this.textBox233.StyleName = "";
            // 
            // textBox234
            // 
            this.textBox234.Name = "textBox234";
            this.textBox234.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.3677082359790802D));
            this.textBox234.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox234.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox234.StyleName = "";
            // 
            // textBox235
            // 
            this.textBox235.Name = "textBox235";
            this.textBox235.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.3677082359790802D));
            this.textBox235.StyleName = "";
            // 
            // textBox236
            // 
            this.textBox236.Name = "textBox236";
            this.textBox236.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.3677082359790802D));
            this.textBox236.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox236.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox236.StyleName = "";
            // 
            // textBox237
            // 
            this.textBox237.Name = "textBox237";
            this.textBox237.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065369606018066D), Telerik.Reporting.Drawing.Unit.Cm(0.3677082359790802D));
            this.textBox237.StyleName = "";
            // 
            // textBox238
            // 
            this.textBox238.Name = "textBox238";
            this.textBox238.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816858291625977D), Telerik.Reporting.Drawing.Unit.Cm(0.3677082359790802D));
            this.textBox238.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox238.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox238.StyleName = "";
            // 
            // textBox239
            // 
            this.textBox239.Name = "textBox239";
            this.textBox239.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.3677082359790802D));
            this.textBox239.StyleName = "";
            // 
            // textBox240
            // 
            this.textBox240.Name = "textBox240";
            this.textBox240.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.36770820617675781D));
            this.textBox240.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox240.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox240.StyleName = "";
            // 
            // textBox241
            // 
            this.textBox241.Name = "textBox241";
            this.textBox241.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox241.StyleName = "";
            this.textBox241.Value = "Action: (New / Retain / Remove)";
            // 
            // textBox242
            // 
            this.textBox242.Name = "textBox242";
            this.textBox242.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox242.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox242.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox242.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox242.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox242.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox242.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox242.StyleName = "";
            // 
            // textBox243
            // 
            this.textBox243.Name = "textBox243";
            this.textBox243.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox243.StyleName = "";
            // 
            // textBox244
            // 
            this.textBox244.Name = "textBox244";
            this.textBox244.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox244.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox244.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox244.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox244.StyleName = "";
            // 
            // textBox245
            // 
            this.textBox245.Name = "textBox245";
            this.textBox245.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065369606018066D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox245.StyleName = "";
            // 
            // textBox246
            // 
            this.textBox246.Name = "textBox246";
            this.textBox246.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816858291625977D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox246.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox246.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox246.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox246.StyleName = "";
            // 
            // textBox247
            // 
            this.textBox247.Name = "textBox247";
            this.textBox247.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D));
            this.textBox247.StyleName = "";
            // 
            // textBox248
            // 
            this.textBox248.Name = "textBox248";
            this.textBox248.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox248.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox248.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox248.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox248.StyleName = "";
            // 
            // textBox249
            // 
            this.textBox249.Name = "textBox249";
            this.textBox249.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.42062550783157349D));
            this.textBox249.StyleName = "";
            // 
            // textBox250
            // 
            this.textBox250.Name = "textBox250";
            this.textBox250.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.42062515020370483D));
            this.textBox250.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox250.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox250.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox250.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox250.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox250.StyleName = "";
            // 
            // textBox251
            // 
            this.textBox251.Name = "textBox251";
            this.textBox251.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.42062515020370483D));
            this.textBox251.StyleName = "";
            // 
            // textBox252
            // 
            this.textBox252.Name = "textBox252";
            this.textBox252.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.42062515020370483D));
            this.textBox252.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox252.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox252.StyleName = "";
            // 
            // textBox253
            // 
            this.textBox253.Name = "textBox253";
            this.textBox253.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065369606018066D), Telerik.Reporting.Drawing.Unit.Cm(0.42062515020370483D));
            this.textBox253.StyleName = "";
            // 
            // textBox254
            // 
            this.textBox254.Name = "textBox254";
            this.textBox254.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816858291625977D), Telerik.Reporting.Drawing.Unit.Cm(0.42062515020370483D));
            this.textBox254.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox254.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox254.StyleName = "";
            // 
            // textBox255
            // 
            this.textBox255.Name = "textBox255";
            this.textBox255.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.42062515020370483D));
            this.textBox255.StyleName = "";
            // 
            // textBox256
            // 
            this.textBox256.Name = "textBox256";
            this.textBox256.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.42062550783157349D));
            this.textBox256.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox256.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox256.StyleName = "";
            // 
            // textBox259
            // 
            this.textBox259.Name = "textBox259";
            this.textBox259.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062780499458313D), Telerik.Reporting.Drawing.Unit.Cm(0.57937490940093994D));
            this.textBox259.StyleName = "";
            // 
            // textBox260
            // 
            this.textBox260.Name = "textBox260";
            this.textBox260.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.57937490940093994D));
            this.textBox260.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox260.StyleName = "";
            // 
            // textBox261
            // 
            this.textBox261.Name = "textBox261";
            this.textBox261.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065375566482544D), Telerik.Reporting.Drawing.Unit.Cm(0.57937490940093994D));
            this.textBox261.StyleName = "";
            // 
            // textBox262
            // 
            this.textBox262.Name = "textBox262";
            this.textBox262.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816855907440186D), Telerik.Reporting.Drawing.Unit.Cm(0.57937490940093994D));
            this.textBox262.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox262.StyleName = "";
            // 
            // textBox263
            // 
            this.textBox263.Name = "textBox263";
            this.textBox263.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733837842941284D), Telerik.Reporting.Drawing.Unit.Cm(0.57937490940093994D));
            this.textBox263.StyleName = "";
            // 
            // textBox264
            // 
            this.textBox264.Name = "textBox264";
            this.textBox264.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.57937526702880859D));
            this.textBox264.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox264.StyleName = "";
            // 
            // textBox265
            // 
            this.textBox265.Name = "textBox265";
            this.textBox265.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.57937490940093994D));
            this.textBox265.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox265.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox265.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox265.StyleName = "";
            this.textBox265.Value = "";
            // 
            // textBox266
            // 
            this.textBox266.Name = "textBox266";
            this.textBox266.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.52916663885116577D));
            this.textBox266.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox266.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox266.StyleName = "";
            // 
            // textBox267
            // 
            this.textBox267.Name = "textBox267";
            this.textBox267.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.52916663885116577D));
            this.textBox267.StyleName = "";
            // 
            // textBox268
            // 
            this.textBox268.Name = "textBox268";
            this.textBox268.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.52916663885116577D));
            this.textBox268.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox268.StyleName = "";
            // 
            // textBox269
            // 
            this.textBox269.Name = "textBox269";
            this.textBox269.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065375566482544D), Telerik.Reporting.Drawing.Unit.Cm(0.52916663885116577D));
            this.textBox269.StyleName = "";
            // 
            // textBox270
            // 
            this.textBox270.Name = "textBox270";
            this.textBox270.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816853523254395D), Telerik.Reporting.Drawing.Unit.Cm(0.52916663885116577D));
            this.textBox270.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox270.StyleName = "";
            // 
            // textBox271
            // 
            this.textBox271.Name = "textBox271";
            this.textBox271.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733825922012329D), Telerik.Reporting.Drawing.Unit.Cm(0.52916663885116577D));
            this.textBox271.StyleName = "";
            // 
            // textBox272
            // 
            this.textBox272.Name = "textBox272";
            this.textBox272.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.529166579246521D));
            this.textBox272.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox272.StyleName = "";
            // 
            // textBox273
            // 
            this.textBox273.Name = "textBox273";
            this.textBox273.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(0.9497910737991333D));
            this.textBox273.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox273.StyleName = "";
            this.textBox273.Value = "Has the Operating Manual been provided to the customer?";
            // 
            // textBox275
            // 
            this.textBox275.Name = "textBox275";
            this.textBox275.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062777519226074D), Telerik.Reporting.Drawing.Unit.Cm(0.949791669845581D));
            this.textBox275.StyleName = "";
            // 
            // textBox279
            // 
            this.textBox279.Name = "textBox279";
            this.textBox279.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733843803405762D), Telerik.Reporting.Drawing.Unit.Cm(0.949791669845581D));
            this.textBox279.StyleName = "";
            // 
            // checkBox7
            // 
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.949791669845581D));
            this.checkBox7.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.checkBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox7.StyleName = "";
            this.checkBox7.Text = "Yes";
            // 
            // textBox276
            // 
            this.textBox276.Name = "textBox276";
            this.textBox276.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.8139610290527344D), Telerik.Reporting.Drawing.Unit.Cm(0.949791669845581D));
            this.textBox276.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox276.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox276.StyleName = "";
            this.textBox276.Value = "Has the customer been instructed in the operation of the system?";
            // 
            // checkBox8
            // 
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.9497910737991333D));
            this.checkBox8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.checkBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox8.StyleName = "";
            this.checkBox8.Text = "Yes";
            this.checkBox8.Value = "Yes";
            // 
            // textBox332
            // 
            this.textBox332.Name = "textBox332";
            this.textBox332.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9287052154541016D), Telerik.Reporting.Drawing.Unit.Cm(0.55291682481765747D));
            this.textBox332.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox332.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox332.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox332.StyleName = "";
            // 
            // textBox333
            // 
            this.textBox333.Name = "textBox333";
            this.textBox333.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.48062780499458313D), Telerik.Reporting.Drawing.Unit.Cm(0.55291682481765747D));
            this.textBox333.StyleName = "";
            // 
            // textBox334
            // 
            this.textBox334.Name = "textBox334";
            this.textBox334.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9816217422485352D), Telerik.Reporting.Drawing.Unit.Cm(0.55291682481765747D));
            this.textBox334.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox334.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox334.StyleName = "";
            // 
            // textBox335
            // 
            this.textBox335.Name = "textBox335";
            this.textBox335.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.55065375566482544D), Telerik.Reporting.Drawing.Unit.Cm(0.55291682481765747D));
            this.textBox335.StyleName = "";
            // 
            // textBox336
            // 
            this.textBox336.Name = "textBox336";
            this.textBox336.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2816855907440186D), Telerik.Reporting.Drawing.Unit.Cm(0.55291682481765747D));
            this.textBox336.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox336.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox336.StyleName = "";
            // 
            // textBox337
            // 
            this.textBox337.Name = "textBox337";
            this.textBox337.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.59733831882476807D), Telerik.Reporting.Drawing.Unit.Cm(0.55291682481765747D));
            this.textBox337.StyleName = "";
            // 
            // textBox338
            // 
            this.textBox338.Name = "textBox338";
            this.textBox338.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8722894191741943D), Telerik.Reporting.Drawing.Unit.Cm(0.55291670560836792D));
            this.textBox338.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox338.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox338.StyleName = "";
            // 
            // textBox257
            // 
            this.textBox257.Name = "textBox257";
            this.textBox257.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9164438247680664D), Telerik.Reporting.Drawing.Unit.Cm(1.6614586114883423D));
            this.textBox257.StyleName = "";
            this.textBox257.Value = "Certifying Authority Certificate Number (Available from Clean Energy Council webs" +
    "ite)";
            // 
            // textBox347
            // 
            this.textBox347.Name = "textBox347";
            this.textBox347.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.50000065565109253D));
            this.textBox347.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox347.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox347.StyleName = "";
            // 
            // textBox348
            // 
            this.textBox348.Name = "textBox348";
            this.textBox348.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.2645832896232605D));
            this.textBox348.StyleName = "";
            // 
            // textBox349
            // 
            this.textBox349.Name = "textBox349";
            this.textBox349.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox349.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox349.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox349.StyleName = "";
            // 
            // textBox350
            // 
            this.textBox350.Name = "textBox350";
            this.textBox350.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.36770844459533691D));
            this.textBox350.StyleName = "";
            // 
            // textBox351
            // 
            this.textBox351.Name = "textBox351";
            this.textBox351.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox351.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox351.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox351.StyleName = "";
            // 
            // textBox352
            // 
            this.textBox352.Name = "textBox352";
            this.textBox352.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox352.StyleName = "";
            // 
            // textBox353
            // 
            this.textBox353.Name = "textBox353";
            this.textBox353.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox353.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox353.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox353.StyleName = "";
            // 
            // textBox354
            // 
            this.textBox354.Name = "textBox354";
            this.textBox354.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.36770817637443542D));
            this.textBox354.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox354.StyleName = "";
            // 
            // textBox355
            // 
            this.textBox355.Name = "textBox355";
            this.textBox355.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox355.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox355.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox355.StyleName = "";
            // 
            // textBox356
            // 
            this.textBox356.Name = "textBox356";
            this.textBox356.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.4206254780292511D));
            this.textBox356.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox356.StyleName = "";
            // 
            // textBox357
            // 
            this.textBox357.Name = "textBox357";
            this.textBox357.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.57937520742416382D));
            this.textBox357.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox357.StyleName = "";
            // 
            // textBox358
            // 
            this.textBox358.Name = "textBox358";
            this.textBox358.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.55291664600372314D));
            this.textBox358.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox358.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox358.StyleName = "";
            // 
            // textBox359
            // 
            this.textBox359.Name = "textBox359";
            this.textBox359.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.529166579246521D));
            this.textBox359.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox359.StyleName = "";
            // 
            // textBox360
            // 
            this.textBox360.Name = "textBox360";
            this.textBox360.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2591664791107178D), Telerik.Reporting.Drawing.Unit.Cm(0.9497910737991333D));
            this.textBox360.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox360.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox360.StyleName = "";
            // 
            // textBox150
            // 
            this.textBox150.Name = "textBox150";
            this.textBox150.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1314558982849121D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox150.Style.Font.Bold = true;
            this.textBox150.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox150.StyleName = "";
            this.textBox150.Value = "Energy Storage System";
            // 
            // textBox274
            // 
            this.textBox274.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(52.099998474121094D));
            this.textBox274.Name = "textBox274";
            this.textBox274.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.460273742675781D), Telerik.Reporting.Drawing.Unit.Cm(0.89999669790267944D));
            this.textBox274.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(194)))), ((int)(((byte)(225)))));
            this.textBox274.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox274.Value = "Jemena Electricity Networks Ltd ABN: 82 064 651 083\r\nAddition or Alteration of Mi" +
    "cro Embedded Generator Connection - 1 July 2016";
            // 
            // textBox277
            // 
            this.textBox277.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(53.900001525878906D));
            this.textBox277.Name = "textBox277";
            this.textBox277.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.5D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox277.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox277.Style.Font.Bold = true;
            this.textBox277.Style.Font.Italic = true;
            this.textBox277.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(17D);
            this.textBox277.Style.Font.Underline = false;
            this.textBox277.Value = "Description of Inverter/Generator Alteration ";
            // 
            // textBox278
            // 
            this.textBox278.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.5D), Telerik.Reporting.Drawing.Unit.Cm(53.900001525878906D));
            this.textBox278.Name = "textBox278";
            this.textBox278.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3997979164123535D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox278.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox278.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox278.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox278.Value = "(Please provide details of your alteration here):";
            // 
            // textBox280
            // 
            this.textBox280.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(55D));
            this.textBox280.Name = "textBox280";
            this.textBox280.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.59999567270278931D));
            this.textBox280.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox280.Value = "Installed PV Grid";
            // 
            // textBox281
            // 
            this.textBox281.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(56.099998474121094D));
            this.textBox281.Name = "textBox281";
            this.textBox281.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.999900817871094D), Telerik.Reporting.Drawing.Unit.Cm(1.0999974012374878D));
            this.textBox281.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox281.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox281.Value = "";
            // 
            // textBox283
            // 
            this.textBox283.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(57.699996948242188D));
            this.textBox283.Name = "textBox283";
            this.textBox283.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.30182409286499D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox283.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox283.Style.Font.Bold = true;
            this.textBox283.Style.Font.Italic = true;
            this.textBox283.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(17D);
            this.textBox283.Style.Font.Underline = false;
            this.textBox283.Value = "Site Access / Special Instructions";
            // 
            // textBox282
            // 
            this.textBox282.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.3020243644714355D), Telerik.Reporting.Drawing.Unit.Cm(57.699996948242188D));
            this.textBox282.Name = "textBox282";
            this.textBox282.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.3997979164123535D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox282.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox282.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox282.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox282.Value = "(Please insert any special instructions below)";
            // 
            // textBox284
            // 
            this.textBox284.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(58.599998474121094D));
            this.textBox284.Name = "textBox284";
            this.textBox284.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.457258224487305D), Telerik.Reporting.Drawing.Unit.Cm(0.89999669790267944D));
            this.textBox284.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox284.Value = resources.GetString("textBox284.Value");
            // 
            // textBox285
            // 
            this.textBox285.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00040008861105889082D), Telerik.Reporting.Drawing.Unit.Cm(60D));
            this.textBox285.Name = "textBox285";
            this.textBox285.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(1.1000000238418579D));
            this.textBox285.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox285.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox285.Value = "";
            // 
            // textBox287
            // 
            this.textBox287.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(61.699996948242188D));
            this.textBox287.Name = "textBox287";
            this.textBox287.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.619949340820313D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox287.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox287.Style.Font.Bold = true;
            this.textBox287.Style.Font.Italic = true;
            this.textBox287.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(17D);
            this.textBox287.Style.Font.Underline = false;
            this.textBox287.Value = "Expedited Offer and Acceptance (Optional)";
            // 
            // textBox289
            // 
            this.textBox289.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(65.199996948242188D));
            this.textBox289.Name = "textBox289";
            this.textBox289.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.7576603889465332D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox289.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox289.Value = "Do you require an expedited connection?";
            // 
            // textBox286
            // 
            this.textBox286.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(66.200004577636719D));
            this.textBox286.Name = "textBox286";
            this.textBox286.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7820320129394531D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox286.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox286.Style.Font.Bold = true;
            this.textBox286.Style.Font.Italic = true;
            this.textBox286.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(17D);
            this.textBox286.Style.Font.Underline = false;
            this.textBox286.Value = "Energisation";
            // 
            // checkBox9
            // 
            this.checkBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.8999996185302734D), Telerik.Reporting.Drawing.Unit.Cm(65.199996948242188D));
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9999992847442627D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.checkBox9.Text = "Yes";
            // 
            // textBox291
            // 
            this.textBox291.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(68.5999984741211D));
            this.textBox291.Name = "textBox291";
            this.textBox291.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.8057956695556641D), Telerik.Reporting.Drawing.Unit.Cm(0.59999924898147583D));
            this.textBox291.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.textBox291.Style.Font.Bold = true;
            this.textBox291.Style.Font.Italic = true;
            this.textBox291.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(17D);
            this.textBox291.Style.Font.Underline = false;
            this.textBox291.Value = "Acknowledgement and authority";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.99135339260101318D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.53890722990036011D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(19.022012710571289D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999967217445374D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.52645838260650635D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.55291634798049927D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.65874940156936646D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.68520849943161011D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.52645838260650635D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.44708320498466492D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox295);
            this.table5.Body.SetCellContent(2, 0, this.textBox301);
            this.table5.Body.SetCellContent(3, 0, this.textBox298);
            this.table5.Body.SetCellContent(5, 0, this.textBox304);
            this.table5.Body.SetCellContent(7, 2, this.textBox306);
            this.table5.Body.SetCellContent(8, 2, this.textBox308);
            this.table5.Body.SetCellContent(9, 2, this.textBox309);
            this.table5.Body.SetCellContent(10, 2, this.textBox293);
            this.table5.Body.SetCellContent(11, 2, this.textBox294);
            this.table5.Body.SetCellContent(12, 0, this.textBox292);
            this.table5.Body.SetCellContent(13, 0, this.textBox310);
            this.table5.Body.SetCellContent(14, 0, this.textBox312);
            this.table5.Body.SetCellContent(6, 0, this.htmlTextBox1);
            this.table5.Body.SetCellContent(7, 0, this.htmlTextBox2);
            this.table5.Body.SetCellContent(8, 0, this.htmlTextBox3);
            this.table5.Body.SetCellContent(9, 0, this.htmlTextBox4);
            this.table5.Body.SetCellContent(10, 0, this.htmlTextBox5);
            this.table5.Body.SetCellContent(11, 0, this.htmlTextBox6);
            this.table5.Body.SetCellContent(6, 2, this.htmlTextBox10);
            this.table5.Body.SetCellContent(1, 0, this.textBox296);
            this.table5.Body.SetCellContent(4, 0, this.textBox300);
            this.table5.Body.SetCellContent(6, 1, this.textBox363);
            this.table5.Body.SetCellContent(7, 1, this.textBox364);
            this.table5.Body.SetCellContent(8, 1, this.textBox365);
            this.table5.Body.SetCellContent(9, 1, this.textBox366);
            this.table5.Body.SetCellContent(10, 1, this.textBox367);
            this.table5.Body.SetCellContent(11, 1, this.textBox368);
            this.table5.Body.SetCellContent(0, 1, this.textBox307, 1, 2);
            this.table5.Body.SetCellContent(1, 1, this.textBox322, 1, 2);
            this.table5.Body.SetCellContent(2, 1, this.textBox323, 1, 2);
            this.table5.Body.SetCellContent(5, 1, this.textBox362, 1, 2);
            this.table5.Body.SetCellContent(12, 1, this.textBox369, 1, 2);
            this.table5.Body.SetCellContent(13, 1, this.textBox370, 1, 2);
            this.table5.Body.SetCellContent(14, 1, this.textBox371, 1, 2);
            this.table5.Body.SetCellContent(3, 1, this.textBox324, 2, 2);
            tableGroup64.Name = "tableGroup12";
            tableGroup65.Name = "group66";
            tableGroup66.Name = "tableGroup13";
            this.table5.ColumnGroups.Add(tableGroup64);
            this.table5.ColumnGroups.Add(tableGroup65);
            this.table5.ColumnGroups.Add(tableGroup66);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox295,
            this.textBox307,
            this.textBox296,
            this.textBox322,
            this.textBox301,
            this.textBox323,
            this.textBox298,
            this.textBox324,
            this.textBox306,
            this.htmlTextBox10,
            this.textBox362,
            this.textBox308,
            this.textBox363,
            this.textBox309,
            this.textBox293,
            this.textBox364,
            this.textBox294,
            this.textBox300,
            this.textBox365,
            this.textBox304,
            this.htmlTextBox1,
            this.textBox366,
            this.htmlTextBox2,
            this.htmlTextBox3,
            this.textBox367,
            this.htmlTextBox4,
            this.htmlTextBox5,
            this.textBox368,
            this.htmlTextBox6,
            this.textBox292,
            this.textBox369,
            this.textBox310,
            this.textBox370,
            this.textBox312,
            this.textBox371});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.36916956305503845D), Telerik.Reporting.Drawing.Unit.Cm(69.5D));
            this.table5.Name = "table5";
            tableGroup68.Name = "group45";
            tableGroup69.Name = "group44";
            tableGroup70.Name = "group46";
            tableGroup71.Name = "group47";
            tableGroup72.Name = "group48";
            tableGroup73.Name = "group49";
            tableGroup74.Name = "group50";
            tableGroup75.Name = "group51";
            tableGroup76.Name = "group52";
            tableGroup77.Name = "group53";
            tableGroup78.Name = "group54";
            tableGroup79.Name = "group55";
            tableGroup80.Name = "group56";
            tableGroup81.Name = "group57";
            tableGroup82.Name = "group58";
            tableGroup67.ChildGroups.Add(tableGroup68);
            tableGroup67.ChildGroups.Add(tableGroup69);
            tableGroup67.ChildGroups.Add(tableGroup70);
            tableGroup67.ChildGroups.Add(tableGroup71);
            tableGroup67.ChildGroups.Add(tableGroup72);
            tableGroup67.ChildGroups.Add(tableGroup73);
            tableGroup67.ChildGroups.Add(tableGroup74);
            tableGroup67.ChildGroups.Add(tableGroup75);
            tableGroup67.ChildGroups.Add(tableGroup76);
            tableGroup67.ChildGroups.Add(tableGroup77);
            tableGroup67.ChildGroups.Add(tableGroup78);
            tableGroup67.ChildGroups.Add(tableGroup79);
            tableGroup67.ChildGroups.Add(tableGroup80);
            tableGroup67.ChildGroups.Add(tableGroup81);
            tableGroup67.ChildGroups.Add(tableGroup82);
            tableGroup67.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup67.Name = "detailTableGroup4";
            this.table5.RowGroups.Add(tableGroup67);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.552272796630859D), Telerik.Reporting.Drawing.Unit.Cm(7.89687442779541D));
            // 
            // textBox306
            // 
            this.textBox306.Name = "textBox306";
            this.textBox306.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.022014617919922D), Telerik.Reporting.Drawing.Unit.Cm(0.52645838260650635D));
            this.textBox306.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox306.StyleName = "";
            this.textBox306.Value = "commissioning tests as specified in the Service & Installation Rules have been co" +
    "mpleted and passed;";
            // 
            // textBox308
            // 
            this.textBox308.Name = "textBox308";
            this.textBox308.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.022014617919922D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox308.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox308.StyleName = "";
            this.textBox308.Value = "the generator is connected to a dedicated circuit complete with lockable isolatin" +
    "g switch at the switchboard;";
            // 
            // textBox309
            // 
            this.textBox309.Name = "textBox309";
            this.textBox309.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.022014617919922D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox309.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox309.StyleName = "";
            this.textBox309.Value = "the main switchboard, isolating fuse/switch/circuit breaker are labelled correctl" +
    "y;";
            // 
            // textBox293
            // 
            this.textBox293.Name = "textBox293";
            this.textBox293.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.022014617919922D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox293.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox293.StyleName = "";
            this.textBox293.Value = "alternative supply signage has been installed; and,";
            // 
            // textBox294
            // 
            this.textBox294.Name = "textBox294";
            this.textBox294.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.022014617919922D), Telerik.Reporting.Drawing.Unit.Cm(0.44708329439163208D));
            this.textBox294.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(9D);
            this.textBox294.StyleName = "";
            this.textBox294.Value = "the customer has been advised that the generator should remain switched off until" +
    " any metering upgrades are complete to avoid potential metering and billing issu" +
    "es.";
            // 
            // htmlTextBox10
            // 
            this.htmlTextBox10.Name = "htmlTextBox10";
            this.htmlTextBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.022014617919922D), Telerik.Reporting.Drawing.Unit.Cm(0.68520849943161011D));
            this.htmlTextBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(9D);
            this.htmlTextBox10.StyleName = "";
            this.htmlTextBox10.Value = resources.GetString("htmlTextBox10.Value");
            // 
            // textBox314
            // 
            this.textBox314.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(78.4000015258789D));
            this.textBox314.Name = "textBox314";
            this.textBox314.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0997998714447021D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox314.Value = "Signature of ";
            // 
            // textBox315
            // 
            this.textBox315.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.1000001430511475D), Telerik.Reporting.Drawing.Unit.Cm(78.4000015258789D));
            this.textBox315.Name = "textBox315";
            this.textBox315.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.0997998714447021D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox315.Style.Font.Italic = true;
            this.textBox315.Value = "applicant:";
            // 
            // textBox316
            // 
            this.textBox316.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.875838279724121D), Telerik.Reporting.Drawing.Unit.Cm(78.4000015258789D));
            this.textBox316.Name = "textBox316";
            this.textBox316.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2508974075317383D), Telerik.Reporting.Drawing.Unit.Cm(0.59999567270278931D));
            this.textBox316.Value = "Date:";
            // 
            // textBox317
            // 
            this.textBox317.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.126935005187988D), Telerik.Reporting.Drawing.Unit.Cm(78.4000015258789D));
            this.textBox317.Name = "textBox317";
            this.textBox317.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6995992660522461D), Telerik.Reporting.Drawing.Unit.Cm(0.59999567270278931D));
            this.textBox317.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox317.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox317.Value = "";
            // 
            // textBox318
            // 
            this.textBox318.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.826735496520996D), Telerik.Reporting.Drawing.Unit.Cm(78.4000015258789D));
            this.textBox318.Name = "textBox318";
            this.textBox318.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.30000096559524536D), Telerik.Reporting.Drawing.Unit.Cm(0.59999567270278931D));
            this.textBox318.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox318.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox318.Style.Font.Bold = true;
            this.textBox318.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox318.Value = "/";
            // 
            // textBox319
            // 
            this.textBox319.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.164164543151856D), Telerik.Reporting.Drawing.Unit.Cm(78.4000015258789D));
            this.textBox319.Name = "textBox319";
            this.textBox319.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6995992660522461D), Telerik.Reporting.Drawing.Unit.Cm(0.59999567270278931D));
            this.textBox319.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox319.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox319.Value = "";
            // 
            // textBox320
            // 
            this.textBox320.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.863964080810547D), Telerik.Reporting.Drawing.Unit.Cm(78.4000015258789D));
            this.textBox320.Name = "textBox320";
            this.textBox320.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.30000096559524536D), Telerik.Reporting.Drawing.Unit.Cm(0.59999567270278931D));
            this.textBox320.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox320.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox320.Style.Font.Bold = true;
            this.textBox320.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox320.Value = "/";
            // 
            // textBox321
            // 
            this.textBox321.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(18.164165496826172D), Telerik.Reporting.Drawing.Unit.Cm(78.4000015258789D));
            this.textBox321.Name = "textBox321";
            this.textBox321.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8356339931488037D), Telerik.Reporting.Drawing.Unit.Cm(0.59999567270278931D));
            this.textBox321.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox321.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Pixel(1D);
            this.textBox321.Value = "";
            // 
            // textBox325
            // 
            this.textBox325.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(80.5D));
            this.textBox325.Name = "textBox325";
            this.textBox325.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.5D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox325.Style.Font.Bold = true;
            this.textBox325.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox325.Value = "Network Connections";
            // 
            // textBox326
            // 
            this.textBox326.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(81.5D));
            this.textBox326.Name = "textBox326";
            this.textBox326.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2999997138977051D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox326.Style.Font.Bold = true;
            this.textBox326.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox326.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox326.Value = "Email:";
            // 
            // textBox327
            // 
            this.textBox327.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.9000000953674316D), Telerik.Reporting.Drawing.Unit.Cm(81.5D));
            this.textBox327.Name = "textBox327";
            this.textBox327.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2479357719421387D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox327.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox327.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox327.Value = "\tNetworkConnections@jemena.com.au";
            // 
            // textBox328
            // 
            this.textBox328.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.875837326049805D), Telerik.Reporting.Drawing.Unit.Cm(81.5D));
            this.textBox328.Name = "textBox328";
            this.textBox328.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8267747163772583D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox328.Style.Font.Bold = true;
            this.textBox328.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox328.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox328.Value = "Address:";
            // 
            // textBox329
            // 
            this.textBox329.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.702812194824219D), Telerik.Reporting.Drawing.Unit.Cm(81.5D));
            this.textBox329.Name = "textBox329";
            this.textBox329.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2479357719421387D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.textBox329.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox329.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox329.Value = "PO Box 16182, Melbourne VIC 3000";
            // 
            // textBox330
            // 
            this.textBox330.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(82.799995422363281D));
            this.textBox330.Name = "textBox330";
            this.textBox330.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.498531341552734D), Telerik.Reporting.Drawing.Unit.Cm(0.899993896484375D));
            this.textBox330.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(194)))), ((int)(((byte)(225)))));
            this.textBox330.Style.Font.Bold = true;
            this.textBox330.Style.Font.Italic = true;
            this.textBox330.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox330.Style.Font.Underline = false;
            this.textBox330.Value = "Jemena Electricity Networks Ltd ABN: 82 064 651 083\r\nAddition or Alteration of Mi" +
    "cro Embedded Generator Connection - 1 July 2016";
            // 
            // htmlTextBox7
            // 
            this.htmlTextBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.0200149267911911D), Telerik.Reporting.Drawing.Unit.Cm(5.4000000953674316D));
            this.htmlTextBox7.Name = "htmlTextBox7";
            this.htmlTextBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.979885101318359D), Telerik.Reporting.Drawing.Unit.Cm(1.7000002861022949D));
            this.htmlTextBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.htmlTextBox7.Value = resources.GetString("htmlTextBox7.Value");
            // 
            // htmlTextBox8
            // 
            this.htmlTextBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.0200149267911911D), Telerik.Reporting.Drawing.Unit.Cm(7.5D));
            this.htmlTextBox8.Name = "htmlTextBox8";
            this.htmlTextBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(13.481731414794922D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.htmlTextBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.htmlTextBox8.Value = "Please complete <u>  all </u> sections of this form or mark �Not Applicable� as r" +
    "equired.";
            // 
            // htmlTextBox9
            // 
            this.htmlTextBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(67.0999984741211D));
            this.htmlTextBox9.Name = "htmlTextBox9";
            this.htmlTextBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.831886291503906D), Telerik.Reporting.Drawing.Unit.Cm(1.0999974012374878D));
            this.htmlTextBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.htmlTextBox9.Value = resources.GetString("htmlTextBox9.Value");
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.78471481800079346D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(20.002790451049805D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.58208364248275757D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000065565109253D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.4735414981842041D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34125041961669922D)));
            this.table6.Body.SetCellContent(0, 0, this.textBox4);
            this.table6.Body.SetCellContent(2, 0, this.textBox339);
            this.table6.Body.SetCellContent(3, 0, this.textBox258);
            this.table6.Body.SetCellContent(1, 0, this.textBox3);
            this.table6.Body.SetCellContent(2, 1, this.textBox340, 2, 1);
            this.table6.Body.SetCellContent(0, 1, this.textBox288, 2, 1);
            tableGroup83.Name = "tableGroup14";
            tableGroup84.Name = "tableGroup15";
            this.table6.ColumnGroups.Add(tableGroup83);
            this.table6.ColumnGroups.Add(tableGroup84);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox4,
            this.textBox288,
            this.textBox3,
            this.textBox339,
            this.textBox340,
            this.textBox258});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.12021714448928833D), Telerik.Reporting.Drawing.Unit.Cm(62.600002288818359D));
            this.table6.Name = "table6";
            tableGroup86.Name = "group60";
            tableGroup87.Name = "group61";
            tableGroup88.Name = "group62";
            tableGroup89.Name = "group63";
            tableGroup85.ChildGroups.Add(tableGroup86);
            tableGroup85.ChildGroups.Add(tableGroup87);
            tableGroup85.ChildGroups.Add(tableGroup88);
            tableGroup85.ChildGroups.Add(tableGroup89);
            tableGroup85.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup85.Name = "detailTableGroup5";
            this.table6.RowGroups.Add(tableGroup85);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.787506103515625D), Telerik.Reporting.Drawing.Unit.Cm(1.8968762159347534D));
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.78471487760543823D), Telerik.Reporting.Drawing.Unit.Cm(0.58208382129669189D));
            this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox4.Value = "1.";
            // 
            // textBox339
            // 
            this.textBox339.Name = "textBox339";
            this.textBox339.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.78471487760543823D), Telerik.Reporting.Drawing.Unit.Cm(0.47354146838188171D));
            this.textBox339.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox339.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox339.StyleName = "";
            this.textBox339.Value = "2.";
            // 
            // textBox258
            // 
            this.textBox258.Name = "textBox258";
            this.textBox258.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.78471481800079346D), Telerik.Reporting.Drawing.Unit.Cm(0.34125036001205444D));
            this.textBox258.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox258.StyleName = "";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.78471481800079346D), Telerik.Reporting.Drawing.Unit.Cm(0.5000007152557373D));
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox3.StyleName = "";
            this.textBox3.Value = "";
            // 
            // textBox340
            // 
            this.textBox340.Name = "textBox340";
            this.textBox340.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.002790451049805D), Telerik.Reporting.Drawing.Unit.Cm(0.814791738986969D));
            this.textBox340.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(9D);
            this.textBox340.StyleName = "";
            this.textBox340.Value = resources.GetString("textBox340.Value");
            // 
            // textBox288
            // 
            this.textBox288.Name = "textBox288";
            this.textBox288.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.002790451049805D), Telerik.Reporting.Drawing.Unit.Cm(1.08208429813385D));
            this.textBox288.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox288.Value = resources.GetString("textBox288.Value");
            // 
            // htmlTextBox11
            // 
            this.htmlTextBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.0200149267911911D), Telerik.Reporting.Drawing.Unit.Cm(79.5999984741211D));
            this.htmlTextBox11.Name = "htmlTextBox11";
            this.htmlTextBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.641193389892578D), Telerik.Reporting.Drawing.Unit.Cm(0.600002110004425D));
            this.htmlTextBox11.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(1)))), ((int)(((byte)(114)))), ((int)(((byte)(192)))));
            this.htmlTextBox11.Style.Font.Bold = true;
            this.htmlTextBox11.Style.Font.Italic = true;
            this.htmlTextBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(15D);
            this.htmlTextBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.htmlTextBox11.Value = "Please return&nbsp;<span style=\"text-decoration: underline\">ALL 3 PAGES (2 sheets" +
    ")&nbsp;</span>&nbsp;of this completed form together with all required attachment" +
    "s to:";
            // 
            // textBox307
            // 
            this.textBox307.Name = "textBox307";
            this.textBox307.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.560922622680664D), Telerik.Reporting.Drawing.Unit.Cm(0.49999970197677612D));
            this.textBox307.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox307.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox307.StyleName = "";
            this.textBox307.Value = "You acknowledge that we will base our offer on the information provided in this a" +
    "pplication, confirm that the information provided in the";
            // 
            // textBox322
            // 
            this.textBox322.Name = "textBox322";
            this.textBox322.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.560922622680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox322.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox322.StyleName = "";
            this.textBox322.Value = "application is true and correct, and agree to notify us if any of the information" +
    " in the application changes.";
            // 
            // textBox323
            // 
            this.textBox323.Name = "textBox323";
            this.textBox323.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.560922622680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox323.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox323.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox323.StyleName = "";
            this.textBox323.Value = "You confirm that you are authorised to make this application on behalf of the cus" +
    "tomer at the supply address.";
            // 
            // textBox324
            // 
            this.textBox324.Name = "textBox324";
            this.textBox324.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.560920715332031D), Telerik.Reporting.Drawing.Unit.Cm(1.0793746709823608D));
            this.textBox324.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox324.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox324.StyleName = "";
            this.textBox324.Value = resources.GetString("textBox324.Value");
            // 
            // textBox362
            // 
            this.textBox362.Name = "textBox362";
            this.textBox362.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.560922622680664D), Telerik.Reporting.Drawing.Unit.Cm(0.65874940156936646D));
            this.textBox362.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox362.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox362.StyleName = "";
            this.textBox362.Value = "You confirm that the safety and technical requirements as defined in our model st" +
    "anding offer have been met. In particular:";
            // 
            // textBox363
            // 
            this.textBox363.Name = "textBox363";
            this.textBox363.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53890722990036011D), Telerik.Reporting.Drawing.Unit.Cm(0.68520849943161011D));
            this.textBox363.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.textBox363.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox363.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox363.StyleName = "";
            // 
            // textBox364
            // 
            this.textBox364.Name = "textBox364";
            this.textBox364.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53890722990036011D), Telerik.Reporting.Drawing.Unit.Cm(0.52645838260650635D));
            this.textBox364.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox364.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox364.StyleName = "";
            // 
            // textBox365
            // 
            this.textBox365.Name = "textBox365";
            this.textBox365.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53890722990036011D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox365.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.textBox365.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox365.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox365.StyleName = "";
            // 
            // textBox366
            // 
            this.textBox366.Name = "textBox366";
            this.textBox366.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53890722990036011D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox366.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox366.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox366.StyleName = "";
            // 
            // textBox367
            // 
            this.textBox367.Name = "textBox367";
            this.textBox367.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53890722990036011D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox367.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox367.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox367.StyleName = "";
            // 
            // textBox368
            // 
            this.textBox368.Name = "textBox368";
            this.textBox368.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.53890722990036011D), Telerik.Reporting.Drawing.Unit.Cm(0.44708329439163208D));
            this.textBox368.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.textBox368.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox368.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox368.StyleName = "";
            // 
            // textBox369
            // 
            this.textBox369.Name = "textBox369";
            this.textBox369.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.560922622680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox369.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox369.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox369.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox369.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox369.StyleName = "";
            this.textBox369.Value = "You have attached a signed prescribed Certificate of Electricity Safety supplied " +
    "by a registered electrical contractor";
            // 
            // textBox370
            // 
            this.textBox370.Name = "textBox370";
            this.textBox370.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.560922622680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox370.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox370.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox370.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox370.StyleName = "";
            this.textBox370.Value = "You have attached an Electrical Works Request supplied by a registered electrical" +
    " contractor. (where applicable)";
            // 
            // textBox371
            // 
            this.textBox371.Name = "textBox371";
            this.textBox371.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.560922622680664D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox371.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox371.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox371.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox371.StyleName = "";
            this.textBox371.Value = "You acknowledge that we will not commence the connection works until the relevant" +
    " connection charge has been paid.";
            // 
            // textBox312
            // 
            this.textBox312.Name = "textBox312";
            this.textBox312.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox312.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox312.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox312.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox312.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox312.StyleName = "";
            this.textBox312.Value = "(7)";
            // 
            // textBox310
            // 
            this.textBox310.Name = "textBox310";
            this.textBox310.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox310.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox310.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox310.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox310.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox310.StyleName = "";
            this.textBox310.Value = "(6)";
            // 
            // textBox292
            // 
            this.textBox292.Name = "textBox292";
            this.textBox292.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox292.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox292.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox292.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox292.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox292.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox292.StyleName = "";
            this.textBox292.Value = "(5)";
            // 
            // htmlTextBox6
            // 
            this.htmlTextBox6.Name = "htmlTextBox6";
            this.htmlTextBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.44708329439163208D));
            this.htmlTextBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.htmlTextBox6.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.htmlTextBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.htmlTextBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox6.StyleName = "";
            this.htmlTextBox6.Value = "<ul><li>.</li></ul>";
            // 
            // htmlTextBox5
            // 
            this.htmlTextBox5.Name = "htmlTextBox5";
            this.htmlTextBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.htmlTextBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.htmlTextBox5.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.htmlTextBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.htmlTextBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox5.StyleName = "";
            this.htmlTextBox5.Value = "<ul><li>.</li></ul>";
            // 
            // htmlTextBox4
            // 
            this.htmlTextBox4.Name = "htmlTextBox4";
            this.htmlTextBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.htmlTextBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.htmlTextBox4.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.htmlTextBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.htmlTextBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox4.StyleName = "";
            this.htmlTextBox4.Value = "<ul><li>.</li></ul>";
            // 
            // htmlTextBox3
            // 
            this.htmlTextBox3.Name = "htmlTextBox3";
            this.htmlTextBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.htmlTextBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.htmlTextBox3.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.htmlTextBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.htmlTextBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox3.StyleName = "";
            this.htmlTextBox3.Value = "<ul><li>.</li></ul>";
            // 
            // htmlTextBox2
            // 
            this.htmlTextBox2.Name = "htmlTextBox2";
            this.htmlTextBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.52645838260650635D));
            this.htmlTextBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.htmlTextBox2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.htmlTextBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.htmlTextBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox2.StyleName = "";
            this.htmlTextBox2.Value = "<ul><li>.</li></ul>";
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.68520849943161011D));
            this.htmlTextBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.htmlTextBox1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Mm(0D);
            this.htmlTextBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.htmlTextBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox1.StyleName = "";
            this.htmlTextBox1.Value = "<ul><li>.</li></ul>";
            // 
            // textBox304
            // 
            this.textBox304.Name = "textBox304";
            this.textBox304.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.65874940156936646D));
            this.textBox304.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox304.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox304.StyleName = "";
            this.textBox304.Value = "(4)";
            // 
            // textBox300
            // 
            this.textBox300.Name = "textBox300";
            this.textBox300.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.552916407585144D));
            this.textBox300.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox300.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox300.StyleName = "";
            this.textBox300.Value = "";
            // 
            // textBox298
            // 
            this.textBox298.Name = "textBox298";
            this.textBox298.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.52645838260650635D));
            this.textBox298.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox298.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox298.StyleName = "";
            this.textBox298.Value = "(3)";
            // 
            // textBox301
            // 
            this.textBox301.Name = "textBox301";
            this.textBox301.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox301.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox301.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox301.StyleName = "";
            this.textBox301.Value = "(2)";
            // 
            // textBox296
            // 
            this.textBox296.Name = "textBox296";
            this.textBox296.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox296.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(10D);
            this.textBox296.StyleName = "";
            this.textBox296.Value = "";
            // 
            // textBox295
            // 
            this.textBox295.Name = "textBox295";
            this.textBox295.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.99135333299636841D), Telerik.Reporting.Drawing.Unit.Cm(0.49999970197677612D));
            this.textBox295.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Pixel(11D);
            this.textBox295.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
            this.textBox295.StyleName = "";
            this.textBox295.Value = "(1)";
            // 
            // MicroEmbeddedGenerator
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "MicroEmbeddedGenerator";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210.00399780273438D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox piclogo;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.CheckBox checkBox1;
        private Telerik.Reporting.CheckBox checkBox2;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.CheckBox checkBox3;
        private Telerik.Reporting.CheckBox checkBox4;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.CheckBox checkBox5;
        private Telerik.Reporting.CheckBox checkBox6;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.TextBox textBox112;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.TextBox textBox114;
        private Telerik.Reporting.TextBox textBox115;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox118;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.TextBox textBox106;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.TextBox textBox119;
        private Telerik.Reporting.TextBox textBox120;
        private Telerik.Reporting.TextBox textBox121;
        private Telerik.Reporting.TextBox textBox122;
        private Telerik.Reporting.TextBox textBox123;
        private Telerik.Reporting.TextBox textBox124;
        private Telerik.Reporting.TextBox textBox125;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.TextBox textBox128;
        private Telerik.Reporting.TextBox textBox129;
        private Telerik.Reporting.TextBox textBox130;
        private Telerik.Reporting.TextBox textBox131;
        private Telerik.Reporting.TextBox textBox132;
        private Telerik.Reporting.TextBox textBox133;
        private Telerik.Reporting.TextBox textBox134;
        private Telerik.Reporting.TextBox textBox135;
        private Telerik.Reporting.TextBox textBox136;
        private Telerik.Reporting.TextBox textBox137;
        private Telerik.Reporting.TextBox textBox138;
        private Telerik.Reporting.TextBox textBox139;
        private Telerik.Reporting.TextBox textBox140;
        private Telerik.Reporting.TextBox textBox141;
        private Telerik.Reporting.TextBox textBox142;
        private Telerik.Reporting.TextBox textBox143;
        private Telerik.Reporting.TextBox textBox144;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox145;
        private Telerik.Reporting.TextBox textBox147;
        private Telerik.Reporting.TextBox textBox149;
        private Telerik.Reporting.TextBox textBox151;
        private Telerik.Reporting.TextBox textBox152;
        private Telerik.Reporting.TextBox textBox153;
        private Telerik.Reporting.TextBox textBox146;
        private Telerik.Reporting.TextBox textBox148;
        private Telerik.Reporting.TextBox textBox150;
        private Telerik.Reporting.TextBox textBox154;
        private Telerik.Reporting.TextBox textBox157;
        private Telerik.Reporting.TextBox textBox158;
        private Telerik.Reporting.TextBox textBox159;
        private Telerik.Reporting.TextBox textBox160;
        private Telerik.Reporting.TextBox textBox161;
        private Telerik.Reporting.TextBox textBox163;
        private Telerik.Reporting.TextBox textBox164;
        private Telerik.Reporting.TextBox textBox165;
        private Telerik.Reporting.TextBox textBox166;
        private Telerik.Reporting.TextBox textBox167;
        private Telerik.Reporting.TextBox textBox169;
        private Telerik.Reporting.TextBox textBox170;
        private Telerik.Reporting.TextBox textBox171;
        private Telerik.Reporting.TextBox textBox172;
        private Telerik.Reporting.TextBox textBox173;
        private Telerik.Reporting.TextBox textBox175;
        private Telerik.Reporting.TextBox textBox176;
        private Telerik.Reporting.TextBox textBox177;
        private Telerik.Reporting.TextBox textBox178;
        private Telerik.Reporting.TextBox textBox179;
        private Telerik.Reporting.TextBox textBox181;
        private Telerik.Reporting.TextBox textBox182;
        private Telerik.Reporting.TextBox textBox183;
        private Telerik.Reporting.TextBox textBox184;
        private Telerik.Reporting.TextBox textBox185;
        private Telerik.Reporting.TextBox textBox187;
        private Telerik.Reporting.TextBox textBox188;
        private Telerik.Reporting.TextBox textBox189;
        private Telerik.Reporting.TextBox textBox190;
        private Telerik.Reporting.TextBox textBox191;
        private Telerik.Reporting.TextBox textBox155;
        private Telerik.Reporting.TextBox textBox156;
        private Telerik.Reporting.TextBox textBox162;
        private Telerik.Reporting.TextBox textBox168;
        private Telerik.Reporting.TextBox textBox174;
        private Telerik.Reporting.TextBox textBox180;
        private Telerik.Reporting.TextBox textBox186;
        private Telerik.Reporting.TextBox textBox192;
        private Telerik.Reporting.TextBox textBox193;
        private Telerik.Reporting.TextBox textBox194;
        private Telerik.Reporting.TextBox textBox195;
        private Telerik.Reporting.TextBox textBox196;
        private Telerik.Reporting.TextBox textBox197;
        private Telerik.Reporting.TextBox textBox198;
        private Telerik.Reporting.TextBox textBox199;
        private Telerik.Reporting.TextBox textBox200;
        private Telerik.Reporting.TextBox textBox201;
        private Telerik.Reporting.TextBox textBox202;
        private Telerik.Reporting.TextBox textBox203;
        private Telerik.Reporting.TextBox textBox204;
        private Telerik.Reporting.TextBox textBox205;
        private Telerik.Reporting.TextBox textBox206;
        private Telerik.Reporting.TextBox textBox207;
        private Telerik.Reporting.TextBox textBox208;
        private Telerik.Reporting.TextBox textBox209;
        private Telerik.Reporting.TextBox textBox210;
        private Telerik.Reporting.TextBox textBox211;
        private Telerik.Reporting.TextBox textBox212;
        private Telerik.Reporting.TextBox textBox213;
        private Telerik.Reporting.TextBox textBox214;
        private Telerik.Reporting.TextBox textBox215;
        private Telerik.Reporting.TextBox textBox216;
        private Telerik.Reporting.TextBox textBox217;
        private Telerik.Reporting.TextBox textBox218;
        private Telerik.Reporting.TextBox textBox219;
        private Telerik.Reporting.TextBox textBox220;
        private Telerik.Reporting.TextBox textBox221;
        private Telerik.Reporting.TextBox textBox222;
        private Telerik.Reporting.TextBox textBox223;
        private Telerik.Reporting.TextBox textBox224;
        private Telerik.Reporting.TextBox textBox225;
        private Telerik.Reporting.TextBox textBox226;
        private Telerik.Reporting.TextBox textBox227;
        private Telerik.Reporting.TextBox textBox228;
        private Telerik.Reporting.TextBox textBox229;
        private Telerik.Reporting.TextBox textBox230;
        private Telerik.Reporting.TextBox textBox231;
        private Telerik.Reporting.TextBox textBox232;
        private Telerik.Reporting.TextBox textBox233;
        private Telerik.Reporting.TextBox textBox234;
        private Telerik.Reporting.TextBox textBox235;
        private Telerik.Reporting.TextBox textBox236;
        private Telerik.Reporting.TextBox textBox237;
        private Telerik.Reporting.TextBox textBox238;
        private Telerik.Reporting.TextBox textBox239;
        private Telerik.Reporting.TextBox textBox240;
        private Telerik.Reporting.TextBox textBox241;
        private Telerik.Reporting.TextBox textBox242;
        private Telerik.Reporting.TextBox textBox243;
        private Telerik.Reporting.TextBox textBox244;
        private Telerik.Reporting.TextBox textBox245;
        private Telerik.Reporting.TextBox textBox246;
        private Telerik.Reporting.TextBox textBox247;
        private Telerik.Reporting.TextBox textBox248;
        private Telerik.Reporting.TextBox textBox249;
        private Telerik.Reporting.TextBox textBox250;
        private Telerik.Reporting.TextBox textBox251;
        private Telerik.Reporting.TextBox textBox252;
        private Telerik.Reporting.TextBox textBox253;
        private Telerik.Reporting.TextBox textBox254;
        private Telerik.Reporting.TextBox textBox255;
        private Telerik.Reporting.TextBox textBox256;
        private Telerik.Reporting.TextBox textBox257;
        private Telerik.Reporting.TextBox textBox259;
        private Telerik.Reporting.TextBox textBox260;
        private Telerik.Reporting.TextBox textBox261;
        private Telerik.Reporting.TextBox textBox262;
        private Telerik.Reporting.TextBox textBox263;
        private Telerik.Reporting.TextBox textBox264;
        private Telerik.Reporting.TextBox textBox265;
        private Telerik.Reporting.TextBox textBox266;
        private Telerik.Reporting.TextBox textBox267;
        private Telerik.Reporting.TextBox textBox268;
        private Telerik.Reporting.TextBox textBox269;
        private Telerik.Reporting.TextBox textBox270;
        private Telerik.Reporting.TextBox textBox271;
        private Telerik.Reporting.TextBox textBox272;
        private Telerik.Reporting.TextBox textBox273;
        private Telerik.Reporting.TextBox textBox275;
        private Telerik.Reporting.TextBox textBox276;
        private Telerik.Reporting.TextBox textBox279;
        private Telerik.Reporting.CheckBox checkBox7;
        private Telerik.Reporting.CheckBox checkBox8;
        private Telerik.Reporting.TextBox textBox274;
        private Telerik.Reporting.TextBox textBox277;
        private Telerik.Reporting.TextBox textBox278;
        private Telerik.Reporting.TextBox textBox280;
        private Telerik.Reporting.TextBox textBox281;
        private Telerik.Reporting.TextBox textBox283;
        private Telerik.Reporting.TextBox textBox282;
        private Telerik.Reporting.TextBox textBox284;
        private Telerik.Reporting.TextBox textBox285;
        private Telerik.Reporting.TextBox textBox287;
        private Telerik.Reporting.TextBox textBox289;
        private Telerik.Reporting.TextBox textBox286;
        private Telerik.Reporting.CheckBox checkBox9;
        private Telerik.Reporting.TextBox textBox291;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox306;
        private Telerik.Reporting.TextBox textBox308;
        private Telerik.Reporting.TextBox textBox309;
        private Telerik.Reporting.TextBox textBox293;
        private Telerik.Reporting.TextBox textBox294;
        private Telerik.Reporting.TextBox textBox314;
        private Telerik.Reporting.TextBox textBox315;
        private Telerik.Reporting.TextBox textBox316;
        private Telerik.Reporting.TextBox textBox317;
        private Telerik.Reporting.TextBox textBox318;
        private Telerik.Reporting.TextBox textBox319;
        private Telerik.Reporting.TextBox textBox320;
        private Telerik.Reporting.TextBox textBox321;
        private Telerik.Reporting.TextBox textBox325;
        private Telerik.Reporting.TextBox textBox326;
        private Telerik.Reporting.TextBox textBox327;
        private Telerik.Reporting.TextBox textBox328;
        private Telerik.Reporting.TextBox textBox329;
        private Telerik.Reporting.TextBox textBox330;
        private Telerik.Reporting.TextBox textBox332;
        private Telerik.Reporting.TextBox textBox333;
        private Telerik.Reporting.TextBox textBox334;
        private Telerik.Reporting.TextBox textBox335;
        private Telerik.Reporting.TextBox textBox336;
        private Telerik.Reporting.TextBox textBox337;
        private Telerik.Reporting.TextBox textBox338;
        private Telerik.Reporting.HtmlTextBox htmlTextBox7;
        private Telerik.Reporting.HtmlTextBox htmlTextBox8;
        private Telerik.Reporting.HtmlTextBox htmlTextBox9;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox288;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox339;
        private Telerik.Reporting.TextBox textBox340;
        private Telerik.Reporting.TextBox textBox258;
        private Telerik.Reporting.TextBox textBox331;
        private Telerik.Reporting.TextBox textBox341;
        private Telerik.Reporting.TextBox textBox342;
        private Telerik.Reporting.TextBox textBox343;
        private Telerik.Reporting.TextBox textBox344;
        private Telerik.Reporting.TextBox textBox345;
        private Telerik.Reporting.TextBox textBox346;
        private Telerik.Reporting.TextBox textBox347;
        private Telerik.Reporting.TextBox textBox348;
        private Telerik.Reporting.TextBox textBox349;
        private Telerik.Reporting.TextBox textBox350;
        private Telerik.Reporting.TextBox textBox351;
        private Telerik.Reporting.TextBox textBox352;
        private Telerik.Reporting.TextBox textBox353;
        private Telerik.Reporting.TextBox textBox354;
        private Telerik.Reporting.TextBox textBox355;
        private Telerik.Reporting.TextBox textBox356;
        private Telerik.Reporting.TextBox textBox357;
        private Telerik.Reporting.TextBox textBox358;
        private Telerik.Reporting.TextBox textBox359;
        private Telerik.Reporting.TextBox textBox360;
        private Telerik.Reporting.HtmlTextBox htmlTextBox10;
        private Telerik.Reporting.HtmlTextBox htmlTextBox11;
        private Telerik.Reporting.TextBox textBox322;
        private Telerik.Reporting.TextBox textBox323;
        private Telerik.Reporting.TextBox textBox324;
        private Telerik.Reporting.TextBox textBox362;
        private Telerik.Reporting.TextBox textBox363;
        private Telerik.Reporting.TextBox textBox364;
        private Telerik.Reporting.TextBox textBox365;
        private Telerik.Reporting.TextBox textBox366;
        private Telerik.Reporting.TextBox textBox367;
        private Telerik.Reporting.TextBox textBox368;
        private Telerik.Reporting.TextBox textBox369;
        private Telerik.Reporting.TextBox textBox370;
        private Telerik.Reporting.TextBox textBox371;
        private Telerik.Reporting.TextBox textBox307;
        private Telerik.Reporting.TextBox textBox295;
        private Telerik.Reporting.TextBox textBox301;
        private Telerik.Reporting.TextBox textBox298;
        private Telerik.Reporting.TextBox textBox304;
        private Telerik.Reporting.TextBox textBox292;
        private Telerik.Reporting.TextBox textBox310;
        private Telerik.Reporting.TextBox textBox312;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox2;
        private Telerik.Reporting.HtmlTextBox htmlTextBox3;
        private Telerik.Reporting.HtmlTextBox htmlTextBox4;
        private Telerik.Reporting.HtmlTextBox htmlTextBox5;
        private Telerik.Reporting.HtmlTextBox htmlTextBox6;
        private Telerik.Reporting.TextBox textBox296;
        private Telerik.Reporting.TextBox textBox300;
    }
}