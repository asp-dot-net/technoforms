namespace TelerikReports
{
    partial class SystemConnectionForm
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SystemConnectionForm));
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup60 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup61 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup62 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup63 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup64 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup65 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup66 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup67 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup68 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup69 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup70 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup71 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup72 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup73 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup74 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup75 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup76 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup77 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup78 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup79 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup80 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup81 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup82 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup83 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup84 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup85 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup86 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup87 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup88 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup89 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup90 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup91 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup92 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup93 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup94 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup95 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup96 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup97 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup98 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup99 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup100 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup101 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup102 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup103 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup104 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup105 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup106 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup107 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup108 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup109 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup110 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup111 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup112 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.detail = new Telerik.Reporting.DetailSection();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.htmlTextBox6 = new Telerik.Reporting.HtmlTextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.htmlTextBox7 = new Telerik.Reporting.HtmlTextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.table10 = new Telerik.Reporting.Table();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.textBox106 = new Telerik.Reporting.TextBox();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.htmlTextBox8 = new Telerik.Reporting.HtmlTextBox();
            this.table11 = new Telerik.Reporting.Table();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.textBox114 = new Telerik.Reporting.TextBox();
            this.textBox115 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.textBox119 = new Telerik.Reporting.TextBox();
            this.textBox120 = new Telerik.Reporting.TextBox();
            this.textBox121 = new Telerik.Reporting.TextBox();
            this.textBox122 = new Telerik.Reporting.TextBox();
            this.textBox123 = new Telerik.Reporting.TextBox();
            this.textBox124 = new Telerik.Reporting.TextBox();
            this.textBox125 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.table12 = new Telerik.Reporting.Table();
            this.textBox128 = new Telerik.Reporting.TextBox();
            this.textBox133 = new Telerik.Reporting.TextBox();
            this.textBox135 = new Telerik.Reporting.TextBox();
            this.textBox129 = new Telerik.Reporting.TextBox();
            this.textBox134 = new Telerik.Reporting.TextBox();
            this.textBox136 = new Telerik.Reporting.TextBox();
            this.textBox137 = new Telerik.Reporting.TextBox();
            this.textBox138 = new Telerik.Reporting.TextBox();
            this.textBox139 = new Telerik.Reporting.TextBox();
            this.textBox140 = new Telerik.Reporting.TextBox();
            this.textBox141 = new Telerik.Reporting.TextBox();
            this.textBox142 = new Telerik.Reporting.TextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.table13 = new Telerik.Reporting.Table();
            this.textBox144 = new Telerik.Reporting.TextBox();
            this.textBox143 = new Telerik.Reporting.TextBox();
            this.textBox145 = new Telerik.Reporting.TextBox();
            this.textBox146 = new Telerik.Reporting.TextBox();
            this.textBox147 = new Telerik.Reporting.TextBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.textBox130 = new Telerik.Reporting.TextBox();
            this.textBox132 = new Telerik.Reporting.TextBox();
            this.textBox131 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox149 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox150 = new Telerik.Reporting.TextBox();
            this.textBox152 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox148 = new Telerik.Reporting.TextBox();
            this.textBox153 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox154 = new Telerik.Reporting.TextBox();
            this.textBox156 = new Telerik.Reporting.TextBox();
            this.textBox157 = new Telerik.Reporting.TextBox();
            this.textBox151 = new Telerik.Reporting.TextBox();
            this.textBox160 = new Telerik.Reporting.TextBox();
            this.textBox161 = new Telerik.Reporting.TextBox();
            this.textBox162 = new Telerik.Reporting.TextBox();
            this.textBox163 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(550.00201416015625D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox2,
            this.table1,
            this.table2,
            this.table5,
            this.table3,
            this.table4,
            this.table6,
            this.table7,
            this.pictureBox1,
            this.table8,
            this.table9,
            this.table10,
            this.table11,
            this.table12,
            this.pictureBox2,
            this.table13,
            this.pictureBox3});
            this.detail.Name = "detail";
            this.detail.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0.40854156017303467D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.353240966796875D), Telerik.Reporting.Drawing.Unit.Cm(0.70000004768371582D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
            this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Upgrade Existing Solar Photovoltaic (PV) System Connection Form";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(1.1087414026260376D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.600000381469727D), Telerik.Reporting.Drawing.Unit.Cm(0.60000008344650269D));
            this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox2.Value = "To be completed by customer & solar PV installer";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.67708325386047363D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(14.499164581298828D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.4497915506362915D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D)));
            this.table1.Body.SetCellContent(1, 1, this.textBox4);
            this.table1.Body.SetCellContent(2, 1, this.textBox5);
            this.table1.Body.SetCellContent(1, 0, this.textBox7);
            this.table1.Body.SetCellContent(2, 0, this.textBox8);
            this.table1.Body.SetCellContent(3, 0, this.textBox9);
            this.table1.Body.SetCellContent(3, 1, this.textBox10);
            this.table1.Body.SetCellContent(0, 0, this.textBox3, 1, 2);
            tableGroup1.Name = "group2";
            tableGroup2.Name = "tableGroup";
            this.table1.ColumnGroups.Add(tableGroup1);
            this.table1.ColumnGroups.Add(tableGroup2);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox3,
            this.textBox7,
            this.textBox4,
            this.textBox8,
            this.textBox5,
            this.textBox9,
            this.textBox10});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(2.0999999046325684D));
            this.table1.Name = "table1";
            tableGroup4.Name = "group66";
            tableGroup5.Name = "group";
            tableGroup6.Name = "group1";
            tableGroup7.Name = "group3";
            tableGroup3.ChildGroups.Add(tableGroup4);
            tableGroup3.ChildGroups.Add(tableGroup5);
            tableGroup3.ChildGroups.Add(tableGroup6);
            tableGroup3.ChildGroups.Add(tableGroup7);
            tableGroup3.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup3.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup3);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.176246643066406D), Telerik.Reporting.Drawing.Unit.Cm(1.949791431427002D));
            this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.499166488647461D), Telerik.Reporting.Drawing.Unit.Cm(0.44979166984558105D));
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.499166488647461D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox5.StyleName = "";
            this.textBox5.Value = "Changing capacity of existing solar PV system, AND";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.67708331346511841D), Telerik.Reporting.Drawing.Unit.Cm(0.44979161024093628D));
            this.textBox7.StyleName = "";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.67708331346511841D), Telerik.Reporting.Drawing.Unit.Cm(0.49999991059303284D));
            this.textBox8.StyleName = "";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.67708331346511841D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox9.StyleName = "";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.499165534973145D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox10.StyleName = "";
            this.textBox10.Value = "On specific request from United Energy";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(21D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50065761804580688D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60934233665466309D)));
            this.table2.Body.SetCellContent(1, 0, this.textBox12);
            this.table2.Body.SetCellContent(0, 0, this.textBox6);
            tableGroup8.Name = "tableGroup1";
            this.table2.ColumnGroups.Add(tableGroup8);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox12,
            this.textBox6});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(4.8000001907348633D));
            this.table2.Name = "table2";
            tableGroup10.Name = "group68";
            tableGroup11.Name = "group67";
            tableGroup9.ChildGroups.Add(tableGroup10);
            tableGroup9.ChildGroups.Add(tableGroup11);
            tableGroup9.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup9.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup9);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(1.1100000143051148D));
            this.table2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.table2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(0.60934233665466309D));
            this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox12.Value = "greater than 10 kW per phase in size, or for further information, please contact " +
    "United Energy.";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.1682054996490479D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.8559598922729492D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.293393611907959D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9437947273254395D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.7386460304260254D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.71224528551101685D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50040644407272339D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50040644407272339D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.81816339492797852D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.52938926219940186D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.52938926219940186D)));
            this.table5.Body.SetCellContent(4, 0, this.textBox39);
            this.table5.Body.SetCellContent(5, 0, this.textBox42);
            this.table5.Body.SetCellContent(3, 0, this.textBox34);
            this.table5.Body.SetCellContent(3, 1, this.textBox36);
            this.table5.Body.SetCellContent(4, 1, this.textBox37);
            this.table5.Body.SetCellContent(3, 2, this.textBox53);
            this.table5.Body.SetCellContent(0, 0, this.htmlTextBox1, 1, 4);
            this.table5.Body.SetCellContent(1, 1, this.textBox150, 2, 1);
            this.table5.Body.SetCellContent(1, 0, this.textBox152, 2, 1);
            this.table5.Body.SetCellContent(0, 4, this.textBox156);
            this.table5.Body.SetCellContent(1, 2, this.textBox11, 1, 2);
            this.table5.Body.SetCellContent(2, 2, this.textBox149, 1, 2);
            this.table5.Body.SetCellContent(3, 3, this.textBox46, 1, 2);
            this.table5.Body.SetCellContent(1, 4, this.textBox157, 2, 1);
            this.table5.Body.SetCellContent(4, 2, this.textBox54, 1, 3);
            this.table5.Body.SetCellContent(5, 1, this.textBox38, 1, 4);
            tableGroup12.Name = "tableGroup7";
            tableGroup13.Name = "group18";
            tableGroup14.Name = "group22";
            tableGroup15.Name = "group20";
            tableGroup16.Name = "group72";
            this.table5.ColumnGroups.Add(tableGroup12);
            this.table5.ColumnGroups.Add(tableGroup13);
            this.table5.ColumnGroups.Add(tableGroup14);
            this.table5.ColumnGroups.Add(tableGroup15);
            this.table5.ColumnGroups.Add(tableGroup16);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox1,
            this.textBox150,
            this.textBox34,
            this.textBox36,
            this.textBox11,
            this.textBox157,
            this.textBox53,
            this.textBox149,
            this.textBox39,
            this.textBox37,
            this.textBox42,
            this.textBox46,
            this.textBox54,
            this.textBox152,
            this.textBox156,
            this.textBox38});
            this.table5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(6.5999999046325684D));
            this.table5.Name = "table5";
            tableGroup18.Name = "group4";
            tableGroup19.Name = "group70";
            tableGroup20.Name = "group69";
            tableGroup21.Name = "group14";
            tableGroup22.Name = "group16";
            tableGroup23.Name = "group17";
            tableGroup17.ChildGroups.Add(tableGroup18);
            tableGroup17.ChildGroups.Add(tableGroup19);
            tableGroup17.ChildGroups.Add(tableGroup20);
            tableGroup17.ChildGroups.Add(tableGroup21);
            tableGroup17.ChildGroups.Add(tableGroup22);
            tableGroup17.ChildGroups.Add(tableGroup23);
            tableGroup17.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup17.Name = "detailTableGroup4";
            this.table5.RowGroups.Add(tableGroup17);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(3.5899999141693115D));
            this.table5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.table5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1682054996490479D), Telerik.Reporting.Drawing.Unit.Cm(0.52938926219940186D));
            this.textBox39.Style.Font.Bold = true;
            this.textBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox39.StyleName = "";
            this.textBox39.Value = "Phone Number";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1682054996490479D), Telerik.Reporting.Drawing.Unit.Cm(0.52938926219940186D));
            this.textBox42.Style.Font.Bold = true;
            this.textBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.StyleName = "";
            this.textBox42.Value = "Email Address";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1682054996490479D), Telerik.Reporting.Drawing.Unit.Cm(0.81816339492797852D));
            this.textBox34.Style.Font.Bold = true;
            this.textBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox34.StyleName = "";
            this.textBox34.Value = "Supply Address";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.8559598922729492D), Telerik.Reporting.Drawing.Unit.Cm(0.81816339492797852D));
            this.textBox36.Style.Font.Bold = true;
            this.textBox36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.StyleName = "";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.8559598922729492D), Telerik.Reporting.Drawing.Unit.Cm(0.52938926219940186D));
            this.textBox37.Style.Font.Bold = true;
            this.textBox37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.StyleName = "";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.6824407577514648D), Telerik.Reporting.Drawing.Unit.Cm(0.81816339492797852D));
            this.textBox46.Style.Font.Bold = true;
            this.textBox46.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.StyleName = "";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.293393611907959D), Telerik.Reporting.Drawing.Unit.Cm(0.81816339492797852D));
            this.textBox53.Style.Font.Bold = true;
            this.textBox53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.StyleName = "";
            this.textBox53.Value = "Mailing\r\nAddress";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.831794738769531D), Telerik.Reporting.Drawing.Unit.Cm(0.52938926219940186D));
            this.textBox38.Style.Font.Bold = true;
            this.textBox38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.9758338928222656D), Telerik.Reporting.Drawing.Unit.Cm(0.52938926219940186D));
            this.textBox54.Style.Font.Bold = true;
            this.textBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.StyleName = "";
            this.textBox54.Value = "After Hours";
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.261354446411133D), Telerik.Reporting.Drawing.Unit.Cm(0.71224528551101685D));
            this.htmlTextBox1.Style.Color = System.Drawing.Color.Red;
            this.htmlTextBox1.Style.Font.Bold = true;
            this.htmlTextBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.htmlTextBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.htmlTextBox1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.htmlTextBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox1.StyleName = "";
            this.htmlTextBox1.Value = resources.GetString("htmlTextBox1.Value");
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.85886812210083D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.7168948650360107D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.0533246994018555D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.3749618530273438D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.9959503412246704D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.68520820140838623D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.7435417175292969D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.66250038146972656D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.71166610717773438D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.94979137182235718D)));
            this.table3.Body.SetCellContent(3, 0, this.textBox13);
            this.table3.Body.SetCellContent(3, 1, this.textBox15);
            this.table3.Body.SetCellContent(4, 0, this.textBox21);
            this.table3.Body.SetCellContent(4, 1, this.textBox22);
            this.table3.Body.SetCellContent(4, 2, this.textBox23);
            this.table3.Body.SetCellContent(5, 0, this.textBox24);
            this.table3.Body.SetCellContent(5, 1, this.textBox25);
            this.table3.Body.SetCellContent(5, 2, this.textBox26);
            this.table3.Body.SetCellContent(6, 0, this.textBox27);
            this.table3.Body.SetCellContent(3, 3, this.textBox31);
            this.table3.Body.SetCellContent(4, 3, this.textBox40);
            this.table3.Body.SetCellContent(5, 3, this.textBox41);
            this.table3.Body.SetCellContent(3, 2, this.htmlTextBox6);
            this.table3.Body.SetCellContent(6, 1, this.textBox28, 1, 3);
            this.table3.Body.SetCellContent(1, 0, this.textBox33);
            this.table3.Body.SetCellContent(1, 2, this.textBox45);
            this.table3.Body.SetCellContent(0, 0, this.textBox153, 1, 4);
            this.table3.Body.SetCellContent(2, 0, this.textBox16);
            this.table3.Body.SetCellContent(2, 2, this.textBox154);
            this.table3.Body.SetCellContent(0, 4, this.textBox151);
            this.table3.Body.SetCellContent(3, 4, this.textBox160);
            this.table3.Body.SetCellContent(4, 4, this.textBox161);
            this.table3.Body.SetCellContent(5, 4, this.textBox162);
            this.table3.Body.SetCellContent(6, 4, this.textBox163);
            this.table3.Body.SetCellContent(1, 1, this.textBox35, 2, 1);
            this.table3.Body.SetCellContent(1, 3, this.textBox148, 2, 2);
            tableGroup24.Name = "tableGroup2";
            tableGroup25.Name = "tableGroup3";
            tableGroup26.Name = "tableGroup4";
            tableGroup27.Name = "group11";
            tableGroup28.Name = "group73";
            this.table3.ColumnGroups.Add(tableGroup24);
            this.table3.ColumnGroups.Add(tableGroup25);
            this.table3.ColumnGroups.Add(tableGroup26);
            this.table3.ColumnGroups.Add(tableGroup27);
            this.table3.ColumnGroups.Add(tableGroup28);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox153,
            this.textBox13,
            this.textBox15,
            this.textBox35,
            this.htmlTextBox6,
            this.textBox148,
            this.textBox31,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox40,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox41,
            this.textBox27,
            this.textBox28,
            this.textBox33,
            this.textBox45,
            this.textBox16,
            this.textBox154,
            this.textBox151,
            this.textBox160,
            this.textBox161,
            this.textBox162,
            this.textBox163});
            this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.00010012308484874666D), Telerik.Reporting.Drawing.Unit.Cm(10.800000190734863D));
            this.table3.Name = "table3";
            tableGroup29.Name = "group71";
            tableGroup30.Name = "group15";
            tableGroup31.Name = "group5";
            tableGroup32.Name = "group6";
            tableGroup33.Name = "group8";
            tableGroup34.Name = "group9";
            tableGroup35.Name = "group10";
            this.table3.RowGroups.Add(tableGroup29);
            this.table3.RowGroups.Add(tableGroup30);
            this.table3.RowGroups.Add(tableGroup31);
            this.table3.RowGroups.Add(tableGroup32);
            this.table3.RowGroups.Add(tableGroup33);
            this.table3.RowGroups.Add(tableGroup34);
            this.table3.RowGroups.Add(tableGroup35);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(5.7527074813842773D));
            this.table3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.85886812210083D), Telerik.Reporting.Drawing.Unit.Cm(1.7435417175292969D));
            this.textBox13.Style.Font.Bold = true;
            this.textBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7168948650360107D), Telerik.Reporting.Drawing.Unit.Cm(1.7435417175292969D));
            this.textBox15.StyleName = "";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.85886812210083D), Telerik.Reporting.Drawing.Unit.Cm(0.66250038146972656D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.StyleName = "";
            this.textBox21.Value = "Solar Inverter Manufacturer";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7168948650360107D), Telerik.Reporting.Drawing.Unit.Cm(0.66250038146972656D));
            this.textBox22.StyleName = "";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0533246994018555D), Telerik.Reporting.Drawing.Unit.Cm(0.66250038146972656D));
            this.textBox23.Style.Font.Bold = true;
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            this.textBox23.Value = "\tSolar Inverter Model Name";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.85886812210083D), Telerik.Reporting.Drawing.Unit.Cm(0.71166610717773438D));
            this.textBox24.Style.Font.Bold = true;
            this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            this.textBox24.Value = "Installer Company Name";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7168948650360107D), Telerik.Reporting.Drawing.Unit.Cm(0.71166610717773438D));
            this.textBox25.StyleName = "";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0533246994018555D), Telerik.Reporting.Drawing.Unit.Cm(0.71166610717773438D));
            this.textBox26.Style.Font.Bold = true;
            this.textBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "Installer Phone Number";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.85886812210083D), Telerik.Reporting.Drawing.Unit.Cm(0.94979137182235718D));
            this.textBox27.Style.Font.Bold = true;
            this.textBox27.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.StyleName = "";
            this.textBox27.Value = "Installer Company Address";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3749618530273438D), Telerik.Reporting.Drawing.Unit.Cm(1.7435417175292969D));
            this.textBox31.StyleName = "";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3749618530273438D), Telerik.Reporting.Drawing.Unit.Cm(0.66250038146972656D));
            this.textBox40.StyleName = "";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3749618530273438D), Telerik.Reporting.Drawing.Unit.Cm(0.71166610717773438D));
            this.textBox41.StyleName = "";
            // 
            // htmlTextBox6
            // 
            this.htmlTextBox6.Name = "htmlTextBox6";
            this.htmlTextBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0533246994018555D), Telerik.Reporting.Drawing.Unit.Cm(1.7435417175292969D));
            this.htmlTextBox6.Style.Font.Bold = true;
            this.htmlTextBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.htmlTextBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox6.StyleName = "";
            this.htmlTextBox6.Value = "<p><strong>Do you have a copy of Photovoltaic (Solar) Generator Connection form f" +
    "rom existing installation?</strong> <span style=\"font-size: 10pt\">(Please attach" +
    ")</span></p><p></p>";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.145181655883789D), Telerik.Reporting.Drawing.Unit.Cm(0.94979137182235718D));
            this.textBox28.StyleName = "";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.3333572149276733D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(19.666641235351562D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.44971668720245361D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.42056313157081604D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49993816018104553D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox14, 1, 2);
            this.table4.Body.SetCellContent(1, 0, this.textBox18, 1, 2);
            this.table4.Body.SetCellContent(2, 0, this.textBox17);
            this.table4.Body.SetCellContent(2, 1, this.textBox19);
            this.table4.Body.SetCellContent(3, 0, this.textBox20);
            this.table4.Body.SetCellContent(3, 1, this.textBox29);
            this.table4.Body.SetCellContent(4, 0, this.textBox32);
            this.table4.Body.SetCellContent(4, 1, this.textBox43);
            this.table4.Body.SetCellContent(5, 0, this.textBox44);
            this.table4.Body.SetCellContent(5, 1, this.textBox47);
            this.table4.Body.SetCellContent(6, 0, this.textBox48);
            this.table4.Body.SetCellContent(6, 1, this.textBox49);
            this.table4.Body.SetCellContent(7, 0, this.textBox50);
            this.table4.Body.SetCellContent(7, 1, this.textBox51);
            this.table4.Body.SetCellContent(8, 0, this.textBox52);
            this.table4.Body.SetCellContent(8, 1, this.textBox55);
            this.table4.Body.SetCellContent(9, 0, this.textBox56);
            this.table4.Body.SetCellContent(9, 1, this.textBox57);
            this.table4.Body.SetCellContent(10, 0, this.textBox58);
            this.table4.Body.SetCellContent(10, 1, this.textBox59);
            this.table4.Body.SetCellContent(11, 0, this.textBox60);
            this.table4.Body.SetCellContent(11, 1, this.textBox61);
            tableGroup36.Name = "group7";
            tableGroup37.Name = "tableGroup5";
            this.table4.ColumnGroups.Add(tableGroup36);
            this.table4.ColumnGroups.Add(tableGroup37);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox14,
            this.textBox18,
            this.textBox17,
            this.textBox19,
            this.textBox20,
            this.textBox29,
            this.textBox32,
            this.textBox43,
            this.textBox44,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox51,
            this.textBox52,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.textBox60,
            this.textBox61});
            this.table4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(17.19999885559082D));
            this.table4.Name = "table4";
            tableGroup39.Name = "group12";
            tableGroup40.Name = "group13";
            tableGroup41.Name = "group19";
            tableGroup42.Name = "group21";
            tableGroup43.Name = "group23";
            tableGroup44.Name = "group24";
            tableGroup45.Name = "group25";
            tableGroup46.Name = "group26";
            tableGroup47.Name = "group27";
            tableGroup48.Name = "group28";
            tableGroup49.Name = "group29";
            tableGroup50.Name = "group30";
            tableGroup38.ChildGroups.Add(tableGroup39);
            tableGroup38.ChildGroups.Add(tableGroup40);
            tableGroup38.ChildGroups.Add(tableGroup41);
            tableGroup38.ChildGroups.Add(tableGroup42);
            tableGroup38.ChildGroups.Add(tableGroup43);
            tableGroup38.ChildGroups.Add(tableGroup44);
            tableGroup38.ChildGroups.Add(tableGroup45);
            tableGroup38.ChildGroups.Add(tableGroup46);
            tableGroup38.ChildGroups.Add(tableGroup47);
            tableGroup38.ChildGroups.Add(tableGroup48);
            tableGroup38.ChildGroups.Add(tableGroup49);
            tableGroup38.ChildGroups.Add(tableGroup50);
            tableGroup38.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup38.Name = "detailTableGroup3";
            this.table4.RowGroups.Add(tableGroup38);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(5.8696603775024414D));
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.44971665740013123D));
            this.textBox14.Style.Font.Bold = false;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "CUSTOMER ACKNOWLEDGEMENT, INDEMNITY AND RELEASE";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(0.42056313157081604D));
            this.textBox18.Style.Font.Bold = false;
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "By signing this form you acknowledge that you:";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.333357572555542D), Telerik.Reporting.Drawing.Unit.Cm(0.49993810057640076D));
            this.textBox17.Style.Font.Bold = true;
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.StyleName = "";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.666641235351562D), Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D));
            this.textBox19.Style.Font.Bold = true;
            this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.333357572555542D), Telerik.Reporting.Drawing.Unit.Cm(0.49993810057640076D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.666641235351562D), Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D));
            this.textBox29.Style.Font.Bold = false;
            this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "Are the owner of the PV (Solar) System listed under section 2";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.333357572555542D), Telerik.Reporting.Drawing.Unit.Cm(0.49993810057640076D));
            this.textBox32.Style.Font.Bold = true;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.StyleName = "";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.666641235351562D), Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D));
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.StyleName = "";
            this.textBox43.Value = "Have received a Photovoltaic Embedded Generator operating manual from, and been i" +
    "nstructed on the operation of the Photovoltaic Embedded Generator by, the";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.333357572555542D), Telerik.Reporting.Drawing.Unit.Cm(0.49993810057640076D));
            this.textBox44.Style.Font.Bold = true;
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.StyleName = "";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.666641235351562D), Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D));
            this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.StyleName = "";
            this.textBox47.Value = "Installation Company detailed in section 3";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.333357572555542D), Telerik.Reporting.Drawing.Unit.Cm(0.49993810057640076D));
            this.textBox48.Style.Font.Bold = true;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.StyleName = "";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.666641235351562D), Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D));
            this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.StyleName = "";
            this.textBox49.Value = "Accept that approval will only be granted for the Photovoltaic Embedded Generator" +
    " detailed in this form, and that you must obtain further prior approval from you" +
    "r";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.333357572555542D), Telerik.Reporting.Drawing.Unit.Cm(0.49993810057640076D));
            this.textBox50.Style.Font.Bold = true;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.StyleName = "";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.666641235351562D), Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D));
            this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.StyleName = "";
            this.textBox51.Value = "Distributor to alter your Photovoltaic Embedded Generator in any way";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.333357572555542D), Telerik.Reporting.Drawing.Unit.Cm(0.49993810057640076D));
            this.textBox52.Style.Font.Bold = true;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.StyleName = "";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.666641235351562D), Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D));
            this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.StyleName = "";
            this.textBox55.Value = "Release and indemnify and agree to keep indemnified your Distributor, its officer" +
    "s, employees and agents against all actions, proceedings, claims and demands";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.333357572555542D), Telerik.Reporting.Drawing.Unit.Cm(0.49993810057640076D));
            this.textBox56.Style.Font.Bold = true;
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox56.StyleName = "";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.666641235351562D), Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D));
            this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.StyleName = "";
            this.textBox57.Value = "whatsoever which may be brought, including any indirect or consequential loss or " +
    "any other form of pure economic loss, made or prosecuted against them or any of";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.333357572555542D), Telerik.Reporting.Drawing.Unit.Cm(0.49993813037872314D));
            this.textBox58.Style.Font.Bold = true;
            this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox58.StyleName = "";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.666641235351562D), Telerik.Reporting.Drawing.Unit.Cm(0.49993813037872314D));
            this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.StyleName = "";
            this.textBox59.Value = "them by any person in respect of the installation of your Photovoltaic Embedded G" +
    "enerator, particularly in relation to works completed by the Installation Compan" +
    "y";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.333357572555542D), Telerik.Reporting.Drawing.Unit.Cm(0.49993810057640076D));
            this.textBox60.Style.Font.Bold = true;
            this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox60.StyleName = "";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.666641235351562D), Telerik.Reporting.Drawing.Unit.Cm(0.49993807077407837D));
            this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.StyleName = "";
            this.textBox61.Value = "detailed in section 2, or in respect of connection of your Photovoltaic Embedded " +
    "Generator to the Victorian electricity grid.";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0748953819274902D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.5715274810791016D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.1089353561401367D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.7236523628234863D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.1589512825012207D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.36307954788208D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60854130983352661D)));
            this.table6.Body.SetCellContent(0, 0, this.textBox65);
            this.table6.Body.SetCellContent(0, 1, this.textBox67);
            this.table6.Body.SetCellContent(0, 2, this.textBox69);
            this.table6.Body.SetCellContent(0, 3, this.textBox70);
            this.table6.Body.SetCellContent(0, 4, this.textBox72);
            this.table6.Body.SetCellContent(0, 5, this.textBox74);
            tableGroup51.Name = "tableGroup6";
            tableGroup52.Name = "tableGroup8";
            tableGroup53.Name = "tableGroup9";
            tableGroup54.Name = "group33";
            tableGroup55.Name = "group34";
            tableGroup56.Name = "group35";
            this.table6.ColumnGroups.Add(tableGroup51);
            this.table6.ColumnGroups.Add(tableGroup52);
            this.table6.ColumnGroups.Add(tableGroup53);
            this.table6.ColumnGroups.Add(tableGroup54);
            this.table6.ColumnGroups.Add(tableGroup55);
            this.table6.ColumnGroups.Add(tableGroup56);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox65,
            this.textBox67,
            this.textBox69,
            this.textBox70,
            this.textBox72,
            this.textBox74});
            this.table6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(24D));
            this.table6.Name = "table6";
            tableGroup57.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup57.Name = "detailTableGroup5";
            this.table6.RowGroups.Add(tableGroup57);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21.001041412353516D), Telerik.Reporting.Drawing.Unit.Cm(0.60854130983352661D));
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0748953819274902D), Telerik.Reporting.Drawing.Unit.Cm(0.60854130983352661D));
            this.textBox65.Style.Font.Bold = true;
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.Value = "Customer Name:";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5715274810791016D), Telerik.Reporting.Drawing.Unit.Cm(0.60854130983352661D));
            this.textBox67.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1089348793029785D), Telerik.Reporting.Drawing.Unit.Cm(0.60854130983352661D));
            this.textBox69.Style.Font.Bold = true;
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.Value = "Customer Signature:";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.7236523628234863D), Telerik.Reporting.Drawing.Unit.Cm(0.60854130983352661D));
            this.textBox70.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox70.StyleName = "";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1589512825012207D), Telerik.Reporting.Drawing.Unit.Cm(0.60854130983352661D));
            this.textBox72.Style.Font.Bold = true;
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.StyleName = "";
            this.textBox72.Value = "Date:";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.363079309463501D), Telerik.Reporting.Drawing.Unit.Cm(0.60854130983352661D));
            this.textBox74.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox74.StyleName = "";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(21D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999985098838806D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox64);
            this.table7.Body.SetCellContent(1, 0, this.textBox66);
            tableGroup58.Name = "tableGroup10";
            this.table7.ColumnGroups.Add(tableGroup58);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox64,
            this.textBox66});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(25.200000762939453D));
            this.table7.Name = "table7";
            tableGroup59.Name = "group32";
            tableGroup60.Name = "group36";
            this.table7.RowGroups.Add(tableGroup59);
            this.table7.RowGroups.Add(tableGroup60);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(0.49999985098838806D));
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox64.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox64.StyleName = "";
            this.textBox64.Value = "PRIVACY STATEMENT: The personal information you provide in this form will be coll" +
    "ected by your Distributor for the purpose of connecting your Photovoltaic Embedd" +
    "ed";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(0.50000017881393433D));
            this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox66.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox66.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox66.StyleName = "";
            this.textBox66.Value = "Generator to the Victorian electricity grid. This information will be used and di" +
    "sclosed by your Distributor in accordance with its privacy processes";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.1770831346511841D), Telerik.Reporting.Drawing.Unit.Cm(27.099998474121094D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(2.2002015113830566D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.42333301901817322D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999982118606567D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.34125000238418579D)));
            this.table8.Body.SetCellContent(0, 0, this.textBox71);
            this.table8.Body.SetCellContent(1, 0, this.textBox68);
            this.table8.Body.SetCellContent(2, 0, this.textBox73);
            this.table8.Body.SetCellContent(3, 0, this.textBox75);
            this.table8.Body.SetCellContent(4, 0, this.textBox76);
            tableGroup61.Name = "tableGroup11";
            this.table8.ColumnGroups.Add(tableGroup61);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox71,
            this.textBox68,
            this.textBox73,
            this.textBox75,
            this.textBox76});
            this.table8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.1999998092651367D), Telerik.Reporting.Drawing.Unit.Cm(27.19999885559082D));
            this.table8.Name = "table8";
            tableGroup62.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup62.Name = "detailTableGroup6";
            tableGroup63.Name = "group37";
            tableGroup64.Name = "group38";
            tableGroup65.Name = "group39";
            tableGroup66.Name = "group40";
            this.table8.RowGroups.Add(tableGroup62);
            this.table8.RowGroups.Add(tableGroup63);
            this.table8.RowGroups.Add(tableGroup64);
            this.table8.RowGroups.Add(tableGroup65);
            this.table8.RowGroups.Add(tableGroup66);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(2.0291664600372314D));
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(0.42333298921585083D));
            this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.Value = "Jemena Asset Management (6) Pvt Ltd";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(0.49999979138374329D));
            this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.StyleName = "";
            this.textBox68.Value = "ABN 52 014 352 650";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox73.StyleName = "";
            // 
            // textBox75
            // 
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(0.5000002384185791D));
            this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox75.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox75.StyleName = "";
            this.textBox75.Value = "Jemena Asset Management (6) Pty Ltd, Australia�s leading provider of infrastructu" +
    "re solutions is the prime contractor for the";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(0.34125000238418579D));
            this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox76.StyleName = "";
            this.textBox76.Value = "electricity network owned by United Energy.";
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.5664567947387695D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.2862491607666016D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.9687519073486328D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.016871452331543D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.0074998140335083D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.87541657686233521D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.74312520027160645D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(2.27270770072937D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.4789588451385498D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.2937502861022949D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.870416522026062D)));
            this.table9.Body.SetCellContent(1, 1, this.textBox77);
            this.table9.Body.SetCellContent(1, 2, this.textBox79);
            this.table9.Body.SetCellContent(1, 3, this.textBox81);
            this.table9.Body.SetCellContent(1, 0, this.textBox84);
            this.table9.Body.SetCellContent(0, 0, this.htmlTextBox7, 1, 4);
            this.table9.Body.SetCellContent(2, 0, this.textBox78);
            this.table9.Body.SetCellContent(2, 1, this.textBox80);
            this.table9.Body.SetCellContent(2, 2, this.textBox82);
            this.table9.Body.SetCellContent(2, 3, this.textBox83);
            this.table9.Body.SetCellContent(3, 0, this.textBox85);
            this.table9.Body.SetCellContent(3, 1, this.textBox86);
            this.table9.Body.SetCellContent(3, 2, this.textBox87);
            this.table9.Body.SetCellContent(3, 3, this.textBox88);
            this.table9.Body.SetCellContent(4, 0, this.textBox89);
            this.table9.Body.SetCellContent(4, 3, this.textBox92);
            this.table9.Body.SetCellContent(4, 1, this.textBox90, 1, 2);
            this.table9.Body.SetCellContent(5, 0, this.textBox91);
            this.table9.Body.SetCellContent(5, 1, this.textBox93);
            this.table9.Body.SetCellContent(5, 2, this.textBox94);
            this.table9.Body.SetCellContent(5, 3, this.textBox95);
            this.table9.Body.SetCellContent(6, 0, this.textBox96, 1, 4);
            tableGroup67.Name = "group42";
            tableGroup68.Name = "tableGroup12";
            tableGroup69.Name = "tableGroup13";
            tableGroup70.Name = "tableGroup14";
            this.table9.ColumnGroups.Add(tableGroup67);
            this.table9.ColumnGroups.Add(tableGroup68);
            this.table9.ColumnGroups.Add(tableGroup69);
            this.table9.ColumnGroups.Add(tableGroup70);
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox7,
            this.textBox84,
            this.textBox77,
            this.textBox79,
            this.textBox81,
            this.textBox78,
            this.textBox80,
            this.textBox82,
            this.textBox83,
            this.textBox85,
            this.textBox86,
            this.textBox87,
            this.textBox88,
            this.textBox89,
            this.textBox90,
            this.textBox92,
            this.textBox91,
            this.textBox93,
            this.textBox94,
            this.textBox95,
            this.textBox96});
            this.table9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.12032493203878403D), Telerik.Reporting.Drawing.Unit.Cm(29.899999618530273D));
            this.table9.Name = "table9";
            tableGroup71.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup71.Name = "detailTableGroup7";
            tableGroup72.Name = "group41";
            tableGroup73.Name = "group43";
            tableGroup74.Name = "group44";
            tableGroup75.Name = "group45";
            tableGroup76.Name = "group46";
            tableGroup77.Name = "group47";
            this.table9.RowGroups.Add(tableGroup71);
            this.table9.RowGroups.Add(tableGroup72);
            this.table9.RowGroups.Add(tableGroup73);
            this.table9.RowGroups.Add(tableGroup74);
            this.table9.RowGroups.Add(tableGroup75);
            this.table9.RowGroups.Add(tableGroup76);
            this.table9.RowGroups.Add(tableGroup77);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.838329315185547D), Telerik.Reporting.Drawing.Unit.Cm(8.541874885559082D));
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2862491607666016D), Telerik.Reporting.Drawing.Unit.Cm(0.87541681528091431D));
            this.textBox77.StyleName = "";
            // 
            // textBox79
            // 
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9687516689300537D), Telerik.Reporting.Drawing.Unit.Cm(0.87541681528091431D));
            this.textBox79.Style.Font.Bold = true;
            this.textBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox79.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox79.StyleName = "";
            this.textBox79.Value = "Installer Phone Number";
            // 
            // textBox81
            // 
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0168704986572266D), Telerik.Reporting.Drawing.Unit.Cm(0.87541681528091431D));
            this.textBox81.StyleName = "";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5664567947387695D), Telerik.Reporting.Drawing.Unit.Cm(0.87541681528091431D));
            this.textBox84.Style.Font.Bold = true;
            this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox84.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.StyleName = "";
            this.textBox84.Value = "Installer Company Name";
            // 
            // htmlTextBox7
            // 
            this.htmlTextBox7.Name = "htmlTextBox7";
            this.htmlTextBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.838329315185547D), Telerik.Reporting.Drawing.Unit.Cm(1.0074998140335083D));
            this.htmlTextBox7.Style.Font.Bold = true;
            this.htmlTextBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.htmlTextBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox7.StyleName = "";
            this.htmlTextBox7.Value = resources.GetString("htmlTextBox7.Value");
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5664567947387695D), Telerik.Reporting.Drawing.Unit.Cm(0.74312508106231689D));
            this.textBox78.Style.Font.Bold = true;
            this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox78.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.StyleName = "";
            this.textBox78.Value = "Installer Company Address";
            // 
            // textBox80
            // 
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2862491607666016D), Telerik.Reporting.Drawing.Unit.Cm(0.74312508106231689D));
            this.textBox80.StyleName = "";
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9687516689300537D), Telerik.Reporting.Drawing.Unit.Cm(0.74312508106231689D));
            this.textBox82.StyleName = "";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0168704986572266D), Telerik.Reporting.Drawing.Unit.Cm(0.74312508106231689D));
            this.textBox83.StyleName = "";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5664567947387695D), Telerik.Reporting.Drawing.Unit.Cm(2.2727081775665283D));
            this.textBox85.Style.Font.Bold = true;
            this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox85.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox85.StyleName = "";
            this.textBox85.Value = "New capacity of installation\r\n(total rating of all solar panels)";
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2862491607666016D), Telerik.Reporting.Drawing.Unit.Cm(2.2727081775665283D));
            this.textBox86.StyleName = "";
            // 
            // textBox87
            // 
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9687516689300537D), Telerik.Reporting.Drawing.Unit.Cm(2.2727081775665283D));
            this.textBox87.Style.Font.Bold = true;
            this.textBox87.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox87.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox87.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox87.StyleName = "";
            this.textBox87.Value = "Has a replacement or additional\r\nsolar inverter been installed?\r\n(Please select o" +
    "ne)";
            // 
            // textBox88
            // 
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0168704986572266D), Telerik.Reporting.Drawing.Unit.Cm(2.2727081775665283D));
            this.textBox88.StyleName = "";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5664567947387695D), Telerik.Reporting.Drawing.Unit.Cm(1.4789582490921021D));
            this.textBox89.Style.Font.Bold = true;
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox89.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox89.StyleName = "";
            this.textBox89.Value = "What is the Solar Inverter Power Rating?\r\n(1 hour or continuous rating)";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0168704986572266D), Telerik.Reporting.Drawing.Unit.Cm(1.4789582490921021D));
            this.textBox92.StyleName = "";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.255000114440918D), Telerik.Reporting.Drawing.Unit.Cm(1.4789582490921021D));
            this.textBox90.StyleName = "";
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5664567947387695D), Telerik.Reporting.Drawing.Unit.Cm(1.2937500476837158D));
            this.textBox91.Style.Font.Bold = true;
            this.textBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox91.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox91.StyleName = "";
            this.textBox91.Value = "Solar Inverter Manufacturer";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.2862491607666016D), Telerik.Reporting.Drawing.Unit.Cm(1.2937500476837158D));
            this.textBox93.StyleName = "";
            // 
            // textBox94
            // 
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.9687516689300537D), Telerik.Reporting.Drawing.Unit.Cm(1.2937500476837158D));
            this.textBox94.Style.Font.Bold = true;
            this.textBox94.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox94.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox94.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox94.StyleName = "";
            this.textBox94.Value = "Solar Inverter Model Name";
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.016871452331543D), Telerik.Reporting.Drawing.Unit.Cm(1.2937500476837158D));
            this.textBox95.StyleName = "";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.838329315185547D), Telerik.Reporting.Drawing.Unit.Cm(0.87041658163070679D));
            this.textBox96.Style.Color = System.Drawing.Color.Red;
            this.textBox96.Style.Font.Bold = true;
            this.textBox96.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox96.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox96.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox96.StyleName = "";
            this.textBox96.Value = "SECTION 4: CONNECTION COMPLIANCE (to be completed by the solar PV installer)";
            // 
            // table10
            // 
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(15.160628318786621D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.9897916316986084D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.7516670227050781D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60854101181030273D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.65874993801116943D)));
            this.table10.Body.SetCellContent(0, 0, this.textBox98);
            this.table10.Body.SetCellContent(0, 1, this.textBox100);
            this.table10.Body.SetCellContent(0, 2, this.textBox102);
            this.table10.Body.SetCellContent(1, 0, this.textBox97);
            this.table10.Body.SetCellContent(1, 1, this.textBox99);
            this.table10.Body.SetCellContent(1, 2, this.textBox101);
            this.table10.Body.SetCellContent(2, 0, this.textBox103);
            this.table10.Body.SetCellContent(2, 1, this.textBox104);
            this.table10.Body.SetCellContent(2, 2, this.textBox105);
            this.table10.Body.SetCellContent(3, 0, this.textBox106);
            this.table10.Body.SetCellContent(3, 1, this.textBox107);
            this.table10.Body.SetCellContent(3, 2, this.textBox108);
            this.table10.Body.SetCellContent(4, 1, this.textBox110);
            this.table10.Body.SetCellContent(4, 2, this.textBox111);
            this.table10.Body.SetCellContent(4, 0, this.htmlTextBox8);
            tableGroup78.Name = "tableGroup15";
            tableGroup79.Name = "tableGroup16";
            tableGroup80.Name = "tableGroup17";
            this.table10.ColumnGroups.Add(tableGroup78);
            this.table10.ColumnGroups.Add(tableGroup79);
            this.table10.ColumnGroups.Add(tableGroup80);
            this.table10.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox98,
            this.textBox100,
            this.textBox102,
            this.textBox97,
            this.textBox99,
            this.textBox101,
            this.textBox103,
            this.textBox104,
            this.textBox105,
            this.textBox106,
            this.textBox107,
            this.textBox108,
            this.htmlTextBox8,
            this.textBox110,
            this.textBox111});
            this.table10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.12032493203878403D), Telerik.Reporting.Drawing.Unit.Cm(38.5D));
            this.table10.Name = "table10";
            tableGroup81.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup81.Name = "detailTableGroup8";
            tableGroup82.Name = "group48";
            tableGroup83.Name = "group49";
            tableGroup84.Name = "group50";
            tableGroup85.Name = "group51";
            this.table10.RowGroups.Add(tableGroup81);
            this.table10.RowGroups.Add(tableGroup82);
            this.table10.RowGroups.Add(tableGroup83);
            this.table10.RowGroups.Add(tableGroup84);
            this.table10.RowGroups.Add(tableGroup85);
            this.table10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.902088165283203D), Telerik.Reporting.Drawing.Unit.Cm(3.0972909927368164D));
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.160625457763672D), Telerik.Reporting.Drawing.Unit.Cm(0.60854130983352661D));
            this.textBox98.Style.Font.Bold = true;
            this.textBox98.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox98.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox98.Value = "Inverter complies with AS4777?";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9897916316986084D), Telerik.Reporting.Drawing.Unit.Cm(0.60854166746139526D));
            // 
            // textBox102
            // 
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7516674995422363D), Telerik.Reporting.Drawing.Unit.Cm(0.60854136943817139D));
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.160630226135254D), Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D));
            this.textBox97.Style.Font.Bold = true;
            this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox97.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox97.StyleName = "";
            this.textBox97.Value = "Inverter connected to a dedicated circuit complete with lockable isolating switch" +
    "?";
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9897918701171875D), Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D));
            this.textBox99.StyleName = "";
            // 
            // textBox101
            // 
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7516674995422363D), Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D));
            this.textBox101.StyleName = "";
            // 
            // textBox103
            // 
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.160629272460938D), Telerik.Reporting.Drawing.Unit.Cm(0.60999995470047D));
            this.textBox103.Style.Font.Bold = true;
            this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox103.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox103.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox103.StyleName = "";
            this.textBox103.Value = "Labelling of main switchboard?";
            // 
            // textBox104
            // 
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9897918701171875D), Telerik.Reporting.Drawing.Unit.Cm(0.60999995470047D));
            this.textBox104.StyleName = "";
            // 
            // textBox105
            // 
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7516672611236572D), Telerik.Reporting.Drawing.Unit.Cm(0.60999995470047D));
            this.textBox105.StyleName = "";
            // 
            // textBox106
            // 
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.160630226135254D), Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D));
            this.textBox106.Style.Font.Bold = true;
            this.textBox106.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox106.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox106.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox106.StyleName = "";
            this.textBox106.Value = "Labelling of isolating fuse/switch/circuit breaker?";
            // 
            // textBox107
            // 
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9897921085357666D), Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D));
            this.textBox107.StyleName = "";
            // 
            // textBox108
            // 
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7516670227050781D), Telerik.Reporting.Drawing.Unit.Cm(0.61000001430511475D));
            this.textBox108.StyleName = "";
            // 
            // textBox110
            // 
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9897913932800293D), Telerik.Reporting.Drawing.Unit.Cm(0.65874987840652466D));
            this.textBox110.Style.Font.Bold = false;
            this.textBox110.StyleName = "";
            // 
            // textBox111
            // 
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7516672611236572D), Telerik.Reporting.Drawing.Unit.Cm(0.65874987840652466D));
            this.textBox111.Style.Font.Bold = false;
            this.textBox111.StyleName = "";
            // 
            // htmlTextBox8
            // 
            this.htmlTextBox8.Name = "htmlTextBox8";
            this.htmlTextBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.160629272460938D), Telerik.Reporting.Drawing.Unit.Cm(0.65874987840652466D));
            this.htmlTextBox8.Style.Font.Bold = false;
            this.htmlTextBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.htmlTextBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.htmlTextBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox8.StyleName = "";
            this.htmlTextBox8.Value = resources.GetString("htmlTextBox8.Value");
            // 
            // table11
            // 
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.66145908832550049D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(19.843753814697266D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.60854107141494751D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.52645832300186157D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D)));
            this.table11.Body.SetCellContent(1, 0, this.textBox109);
            this.table11.Body.SetCellContent(1, 1, this.textBox113);
            this.table11.Body.SetCellContent(0, 0, this.textBox112, 1, 2);
            this.table11.Body.SetCellContent(2, 0, this.textBox114);
            this.table11.Body.SetCellContent(2, 1, this.textBox115);
            this.table11.Body.SetCellContent(3, 0, this.textBox116);
            this.table11.Body.SetCellContent(3, 1, this.textBox117);
            this.table11.Body.SetCellContent(4, 0, this.textBox118);
            this.table11.Body.SetCellContent(4, 1, this.textBox119);
            this.table11.Body.SetCellContent(5, 0, this.textBox120);
            this.table11.Body.SetCellContent(5, 1, this.textBox121);
            this.table11.Body.SetCellContent(6, 0, this.textBox122);
            this.table11.Body.SetCellContent(6, 1, this.textBox123);
            this.table11.Body.SetCellContent(7, 0, this.textBox124);
            this.table11.Body.SetCellContent(7, 1, this.textBox125);
            this.table11.Body.SetCellContent(8, 0, this.textBox126);
            this.table11.Body.SetCellContent(8, 1, this.textBox127);
            tableGroup86.Name = "tableGroup18";
            tableGroup87.Name = "tableGroup19";
            this.table11.ColumnGroups.Add(tableGroup86);
            this.table11.ColumnGroups.Add(tableGroup87);
            this.table11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox112,
            this.textBox109,
            this.textBox113,
            this.textBox114,
            this.textBox115,
            this.textBox116,
            this.textBox117,
            this.textBox118,
            this.textBox119,
            this.textBox120,
            this.textBox121,
            this.textBox122,
            this.textBox123,
            this.textBox124,
            this.textBox125,
            this.textBox126,
            this.textBox127});
            this.table11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D), Telerik.Reporting.Drawing.Unit.Cm(42.599998474121094D));
            this.table11.Name = "table11";
            tableGroup88.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup88.Name = "detailTableGroup9";
            tableGroup89.Name = "group52";
            tableGroup90.Name = "group53";
            tableGroup91.Name = "group54";
            tableGroup92.Name = "group55";
            tableGroup93.Name = "group56";
            tableGroup94.Name = "group57";
            tableGroup95.Name = "group58";
            tableGroup96.Name = "group59";
            this.table11.RowGroups.Add(tableGroup88);
            this.table11.RowGroups.Add(tableGroup89);
            this.table11.RowGroups.Add(tableGroup90);
            this.table11.RowGroups.Add(tableGroup91);
            this.table11.RowGroups.Add(tableGroup92);
            this.table11.RowGroups.Add(tableGroup93);
            this.table11.RowGroups.Add(tableGroup94);
            this.table11.RowGroups.Add(tableGroup95);
            this.table11.RowGroups.Add(tableGroup96);
            this.table11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.505210876464844D), Telerik.Reporting.Drawing.Unit.Cm(4.6349997520446777D));
            // 
            // textBox109
            // 
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.66145914793014526D), Telerik.Reporting.Drawing.Unit.Cm(0.5264580249786377D));
            this.textBox109.StyleName = "";
            // 
            // textBox113
            // 
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.843751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.52645832300186157D));
            this.textBox113.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox113.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox113.StyleName = "";
            this.textBox113.Value = "The PV (Solar) Generator complies with the Electricity Safety Act 1998 (Vic) and " +
    "associated Safety Regulations, the  Electricity Distribution Code, ";
            // 
            // textBox112
            // 
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.505210876464844D), Telerik.Reporting.Drawing.Unit.Cm(0.60854107141494751D));
            this.textBox112.Style.Font.Bold = true;
            this.textBox112.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox112.Value = "By signing this form you acknowledge that:";
            // 
            // textBox114
            // 
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.66145914793014526D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox114.StyleName = "";
            // 
            // textBox115
            // 
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.843751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox115.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox115.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox115.StyleName = "";
            this.textBox115.Value = "the VictorianService & Installation Rules, AS/NZS3000 (Wiring Rules) and AS4777 (" +
    "Grid Connection of Energy Systems via Inverters),";
            // 
            // textBox116
            // 
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.66145920753479D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox116.StyleName = "";
            // 
            // textBox117
            // 
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.843751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox117.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox117.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox117.StyleName = "";
            this.textBox117.Value = "and any other relevant Acts, regulations,\r\nstandards or guidelines";
            // 
            // textBox118
            // 
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.66145920753479D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox118.StyleName = "";
            // 
            // textBox119
            // 
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.843751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.50000005960464478D));
            this.textBox119.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox119.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox119.StyleName = "";
            this.textBox119.Value = "The PV (Solar) Generator is connected to a dedicated circuit complete with lockab" +
    "le isolating switch at the switchboard";
            // 
            // textBox120
            // 
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.66145914793014526D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox120.StyleName = "";
            // 
            // textBox121
            // 
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.843751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox121.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox121.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox121.StyleName = "";
            this.textBox121.Value = "The main switchboard, isolating fuse/switch/circuit breaker are labelled correctl" +
    "y";
            // 
            // textBox122
            // 
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.66145914793014526D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox122.StyleName = "";
            // 
            // textBox123
            // 
            this.textBox123.Name = "textBox123";
            this.textBox123.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.843751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox123.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox123.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox123.StyleName = "";
            this.textBox123.Value = "Alternative supply signage has been installed";
            // 
            // textBox124
            // 
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.66145920753479D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox124.StyleName = "";
            // 
            // textBox125
            // 
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.843751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox125.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox125.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox125.StyleName = "";
            this.textBox125.Value = "The generator owner has been advised that the PV (Solar) Generator should remain " +
    "switched off until any metering upgrades are complete to avoid ";
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.66145920753479D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox126.StyleName = "";
            // 
            // textBox127
            // 
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.843751907348633D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox127.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox127.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox127.StyleName = "";
            this.textBox127.Value = "potential metering and billing issues.";
            // 
            // table12
            // 
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.0691673755645752D)));
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.31750026345252991D)));
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(7.3024959564208984D)));
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.8785381317138672D)));
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.26458320021629333D)));
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.9422893524169922D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.79104149341583252D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.1879168748855591D)));
            this.table12.Body.SetCellContent(0, 0, this.textBox128);
            this.table12.Body.SetCellContent(0, 1, this.textBox133);
            this.table12.Body.SetCellContent(0, 2, this.textBox135);
            this.table12.Body.SetCellContent(0, 4, this.textBox129);
            this.table12.Body.SetCellContent(0, 3, this.textBox134);
            this.table12.Body.SetCellContent(0, 5, this.textBox136);
            this.table12.Body.SetCellContent(1, 0, this.textBox137);
            this.table12.Body.SetCellContent(1, 1, this.textBox138);
            this.table12.Body.SetCellContent(1, 2, this.textBox139);
            this.table12.Body.SetCellContent(1, 3, this.textBox140);
            this.table12.Body.SetCellContent(1, 4, this.textBox141);
            this.table12.Body.SetCellContent(1, 5, this.textBox142);
            tableGroup97.Name = "tableGroup20";
            tableGroup98.Name = "tableGroup21";
            tableGroup99.Name = "tableGroup22";
            tableGroup100.Name = "group62";
            tableGroup101.Name = "group60";
            tableGroup102.Name = "group63";
            this.table12.ColumnGroups.Add(tableGroup97);
            this.table12.ColumnGroups.Add(tableGroup98);
            this.table12.ColumnGroups.Add(tableGroup99);
            this.table12.ColumnGroups.Add(tableGroup100);
            this.table12.ColumnGroups.Add(tableGroup101);
            this.table12.ColumnGroups.Add(tableGroup102);
            this.table12.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox128,
            this.textBox133,
            this.textBox135,
            this.textBox134,
            this.textBox129,
            this.textBox136,
            this.textBox137,
            this.textBox138,
            this.textBox139,
            this.textBox140,
            this.textBox141,
            this.textBox142});
            this.table12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.27907451987266541D), Telerik.Reporting.Drawing.Unit.Cm(48.600002288818359D));
            this.table12.Name = "table12";
            tableGroup104.Name = "group61";
            tableGroup103.ChildGroups.Add(tableGroup104);
            tableGroup103.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup103.Name = "detailTableGroup10";
            tableGroup106.Name = "group65";
            tableGroup105.ChildGroups.Add(tableGroup106);
            tableGroup105.Name = "group64";
            this.table12.RowGroups.Add(tableGroup103);
            this.table12.RowGroups.Add(tableGroup105);
            this.table12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.774574279785156D), Telerik.Reporting.Drawing.Unit.Cm(1.9789583683013916D));
            // 
            // textBox128
            // 
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0691673755645752D), Telerik.Reporting.Drawing.Unit.Cm(0.79104149341583252D));
            this.textBox128.Style.Font.Bold = true;
            this.textBox128.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox128.StyleName = "";
            this.textBox128.Value = "Rec Name";
            // 
            // textBox133
            // 
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.31750026345252991D), Telerik.Reporting.Drawing.Unit.Cm(0.79104149341583252D));
            this.textBox133.Style.Font.Bold = true;
            this.textBox133.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox133.StyleName = "";
            this.textBox133.Value = ":";
            // 
            // textBox135
            // 
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.30249547958374D), Telerik.Reporting.Drawing.Unit.Cm(0.79104149341583252D));
            this.textBox135.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox135.StyleName = "";
            // 
            // textBox129
            // 
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458320021629333D), Telerik.Reporting.Drawing.Unit.Cm(0.79104149341583252D));
            this.textBox129.Style.Font.Bold = true;
            this.textBox129.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox129.StyleName = "";
            this.textBox129.Value = ":";
            // 
            // textBox134
            // 
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8785381317138672D), Telerik.Reporting.Drawing.Unit.Cm(0.79104149341583252D));
            this.textBox134.Style.Font.Bold = true;
            this.textBox134.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox134.StyleName = "";
            this.textBox134.Value = "REC Licence Number";
            // 
            // textBox136
            // 
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9422893524169922D), Telerik.Reporting.Drawing.Unit.Cm(0.79104149341583252D));
            this.textBox136.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox136.StyleName = "";
            // 
            // textBox137
            // 
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.0691671371459961D), Telerik.Reporting.Drawing.Unit.Cm(1.1879168748855591D));
            this.textBox137.Style.Font.Bold = true;
            this.textBox137.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox137.StyleName = "";
            this.textBox137.Value = "REC Signature";
            // 
            // textBox138
            // 
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.31750026345252991D), Telerik.Reporting.Drawing.Unit.Cm(1.1879168748855591D));
            this.textBox138.Style.Font.Bold = true;
            this.textBox138.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox138.StyleName = "";
            this.textBox138.Value = ":";
            // 
            // textBox139
            // 
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.30249547958374D), Telerik.Reporting.Drawing.Unit.Cm(1.1879168748855591D));
            this.textBox139.StyleName = "";
            // 
            // textBox140
            // 
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.8785378932952881D), Telerik.Reporting.Drawing.Unit.Cm(1.1879168748855591D));
            this.textBox140.Style.Font.Bold = true;
            this.textBox140.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox140.StyleName = "";
            this.textBox140.Value = "Date";
            // 
            // textBox141
            // 
            this.textBox141.Name = "textBox141";
            this.textBox141.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.26458320021629333D), Telerik.Reporting.Drawing.Unit.Cm(1.1879168748855591D));
            this.textBox141.Style.Font.Bold = true;
            this.textBox141.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox141.StyleName = "";
            this.textBox141.Value = ":";
            // 
            // textBox142
            // 
            this.textBox142.Name = "textBox142";
            this.textBox142.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.9422893524169922D), Telerik.Reporting.Drawing.Unit.Cm(1.1879168748855591D));
            this.textBox142.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox142.StyleName = "";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D), Telerik.Reporting.Drawing.Unit.Cm(52.799999237060547D));
            this.pictureBox2.MimeType = "image/png";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6000003814697266D), Telerik.Reporting.Drawing.Unit.Cm(2.2002015113830566D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // table13
            // 
            this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D)));
            this.table13.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.42333301901817322D)));
            this.table13.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.49999979138374329D)));
            this.table13.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D)));
            this.table13.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.50000029802322388D)));
            this.table13.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.3412499725818634D)));
            this.table13.Body.SetCellContent(0, 0, this.textBox144);
            this.table13.Body.SetCellContent(1, 0, this.textBox143);
            this.table13.Body.SetCellContent(2, 0, this.textBox145);
            this.table13.Body.SetCellContent(3, 0, this.textBox146);
            this.table13.Body.SetCellContent(4, 0, this.textBox147);
            tableGroup107.Name = "tableGroup11";
            this.table13.ColumnGroups.Add(tableGroup107);
            this.table13.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox144,
            this.textBox143,
            this.textBox145,
            this.textBox146,
            this.textBox147});
            this.table13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5229170322418213D), Telerik.Reporting.Drawing.Unit.Cm(52.899997711181641D));
            this.table13.Name = "table13";
            tableGroup108.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup108.Name = "detailTableGroup6";
            tableGroup109.Name = "group37";
            tableGroup110.Name = "group38";
            tableGroup111.Name = "group39";
            tableGroup112.Name = "group40";
            this.table13.RowGroups.Add(tableGroup108);
            this.table13.RowGroups.Add(tableGroup109);
            this.table13.RowGroups.Add(tableGroup110);
            this.table13.RowGroups.Add(tableGroup111);
            this.table13.RowGroups.Add(tableGroup112);
            this.table13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(2.0291664600372314D));
            // 
            // textBox144
            // 
            this.textBox144.Name = "textBox144";
            this.textBox144.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(0.42333298921585083D));
            this.textBox144.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox144.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox144.Value = "Jemena Asset Management (6) Pvt Ltd";
            // 
            // textBox143
            // 
            this.textBox143.Name = "textBox143";
            this.textBox143.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(0.49999979138374329D));
            this.textBox143.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox143.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox143.StyleName = "";
            this.textBox143.Value = "ABN 52 014 352 650";
            // 
            // textBox145
            // 
            this.textBox145.Name = "textBox145";
            this.textBox145.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(0.26458337903022766D));
            this.textBox145.StyleName = "";
            // 
            // textBox146
            // 
            this.textBox146.Name = "textBox146";
            this.textBox146.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(0.5000002384185791D));
            this.textBox146.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox146.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox146.StyleName = "";
            this.textBox146.Value = "Jemena Asset Management (6) Pty Ltd, Australia�s leading provider of infrastructu" +
    "re solutions is the prime contractor for the";
            // 
            // textBox147
            // 
            this.textBox147.Name = "textBox147";
            this.textBox147.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.801040649414062D), Telerik.Reporting.Drawing.Unit.Cm(0.34125000238418579D));
            this.textBox147.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox147.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox147.StyleName = "";
            this.textBox147.Value = "electricity network owned by United Energy.";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(17.35344123840332D), Telerik.Reporting.Drawing.Unit.Cm(0.40854156017303467D));
            this.pictureBox3.MimeType = "image/png";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.646458625793457D), Telerik.Reporting.Drawing.Unit.Cm(3.5497913360595703D));
            this.pictureBox3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
            this.pictureBox3.Value = ((object)(resources.GetObject("pictureBox3.Value")));
            // 
            // textBox130
            // 
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.545414924621582D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox130.StyleName = "";
            this.textBox130.Value = "REC Licence Number";
            // 
            // textBox132
            // 
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.8208327293396D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox132.StyleName = "";
            // 
            // textBox131
            // 
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.34999942779541D), Telerik.Reporting.Drawing.Unit.Cm(0.49999997019767761D));
            this.textBox131.StyleName = "";
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(15.176247596740723D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox3.StyleName = "";
            this.textBox3.Value = "This form is to be submitted by solar PV customers only under the following circu" +
    "mstances:";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(21D), Telerik.Reporting.Drawing.Unit.Cm(0.50065767765045166D));
            this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox6.StyleName = "";
            this.textBox6.Value = "All fields are mandatory. Solar PV inverters above 10 kW per phase in size will b" +
    "e permitted in certain circumstances. If your solar PV inverter is";
            // 
            // textBox149
            // 
            this.textBox149.Name = "textBox149";
            this.textBox149.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2371883392333984D), Telerik.Reporting.Drawing.Unit.Cm(0.50040644407272339D));
            this.textBox149.Style.Font.Bold = true;
            this.textBox149.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox149.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox149.StyleName = "";
            this.textBox149.Value = "(Refert to your electricity bill)";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.2371883392333984D), Telerik.Reporting.Drawing.Unit.Cm(0.50040644407272339D));
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.StyleName = "";
            this.textBox11.Value = "Customer NMI\r\n";
            // 
            // textBox150
            // 
            this.textBox150.Name = "textBox150";
            this.textBox150.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.8559598922729492D), Telerik.Reporting.Drawing.Unit.Cm(1.0008128881454468D));
            this.textBox150.Style.Font.Bold = true;
            this.textBox150.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox150.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox150.StyleName = "";
            // 
            // textBox152
            // 
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.1682054996490479D), Telerik.Reporting.Drawing.Unit.Cm(1.0008128881454468D));
            this.textBox152.Style.Font.Bold = true;
            this.textBox152.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.textBox152.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox152.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox152.StyleName = "";
            this.textBox152.Value = "\r\nCustomer Name";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.85886812210083D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox33.Style.Font.Bold = true;
            this.textBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.StyleName = "";
            this.textBox33.Value = "Capacity of existing installation";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7168948650360107D), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D));
            this.textBox35.StyleName = "";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0533246994018555D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox45.Style.Font.Bold = true;
            this.textBox45.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.StyleName = "";
            this.textBox45.Value = "Existing solar PV inverter power rating";
            // 
            // textBox148
            // 
            this.textBox148.Name = "textBox148";
            this.textBox148.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.3709120750427246D), Telerik.Reporting.Drawing.Unit.Cm(0.99999988079071045D));
            this.textBox148.StyleName = "";
            // 
            // textBox153
            // 
            this.textBox153.Name = "textBox153";
            this.textBox153.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.004049301147461D), Telerik.Reporting.Drawing.Unit.Cm(0.68520820140838623D));
            this.textBox153.Style.Color = System.Drawing.Color.Red;
            this.textBox153.Style.Font.Bold = true;
            this.textBox153.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox153.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox153.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox153.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox153.StyleName = "";
            this.textBox153.Value = "SECTION 2: EXISTING SOLAR PV SYSTEM DETAILS (to be completed by the householder)";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.85886812210083D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox16.Style.Font.Bold = false;
            this.textBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.StyleName = "";
            this.textBox16.Value = "(total rating of all solar panels)";
            // 
            // textBox154
            // 
            this.textBox154.Name = "textBox154";
            this.textBox154.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.0533246994018555D), Telerik.Reporting.Drawing.Unit.Cm(0.49999994039535522D));
            this.textBox154.Style.Font.Bold = false;
            this.textBox154.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox154.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox154.StyleName = "";
            this.textBox154.Value = "(1 hour or continuous rating)";
            // 
            // textBox156
            // 
            this.textBox156.Name = "textBox156";
            this.textBox156.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7386460304260254D), Telerik.Reporting.Drawing.Unit.Cm(0.71224528551101685D));
            this.textBox156.Style.Color = System.Drawing.Color.Red;
            this.textBox156.Style.Font.Bold = true;
            this.textBox156.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox156.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox156.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox156.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox156.StyleName = "";
            // 
            // textBox157
            // 
            this.textBox157.Name = "textBox157";
            this.textBox157.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7386460304260254D), Telerik.Reporting.Drawing.Unit.Cm(1.0008128881454468D));
            this.textBox157.Style.Font.Bold = true;
            this.textBox157.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox157.StyleName = "";
            // 
            // textBox151
            // 
            this.textBox151.Name = "textBox151";
            this.textBox151.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9959503412246704D), Telerik.Reporting.Drawing.Unit.Cm(0.68520820140838623D));
            this.textBox151.Style.Color = System.Drawing.Color.Red;
            this.textBox151.Style.Font.Bold = true;
            this.textBox151.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox151.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox151.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox151.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox151.StyleName = "";
            // 
            // textBox160
            // 
            this.textBox160.Name = "textBox160";
            this.textBox160.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9959503412246704D), Telerik.Reporting.Drawing.Unit.Cm(1.7435417175292969D));
            this.textBox160.StyleName = "";
            // 
            // textBox161
            // 
            this.textBox161.Name = "textBox161";
            this.textBox161.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9959503412246704D), Telerik.Reporting.Drawing.Unit.Cm(0.66250038146972656D));
            this.textBox161.StyleName = "";
            // 
            // textBox162
            // 
            this.textBox162.Name = "textBox162";
            this.textBox162.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9959503412246704D), Telerik.Reporting.Drawing.Unit.Cm(0.71166610717773438D));
            this.textBox162.StyleName = "";
            // 
            // textBox163
            // 
            this.textBox163.Name = "textBox163";
            this.textBox163.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.9959503412246704D), Telerik.Reporting.Drawing.Unit.Cm(0.94979137182235718D));
            this.textBox163.StyleName = "";
            // 
            // SystemConnectionForm
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SystemConnectionForm";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Mm(210.5364990234375D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.HtmlTextBox htmlTextBox6;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.Table table9;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.HtmlTextBox htmlTextBox7;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.Table table10;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.TextBox textBox106;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.HtmlTextBox htmlTextBox8;
        private Telerik.Reporting.Table table11;
        private Telerik.Reporting.TextBox textBox112;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.TextBox textBox114;
        private Telerik.Reporting.TextBox textBox115;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.TextBox textBox118;
        private Telerik.Reporting.TextBox textBox119;
        private Telerik.Reporting.TextBox textBox120;
        private Telerik.Reporting.TextBox textBox121;
        private Telerik.Reporting.TextBox textBox122;
        private Telerik.Reporting.TextBox textBox123;
        private Telerik.Reporting.TextBox textBox124;
        private Telerik.Reporting.TextBox textBox125;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.TextBox textBox130;
        private Telerik.Reporting.TextBox textBox132;
        private Telerik.Reporting.TextBox textBox131;
        private Telerik.Reporting.Table table12;
        private Telerik.Reporting.TextBox textBox128;
        private Telerik.Reporting.TextBox textBox133;
        private Telerik.Reporting.TextBox textBox135;
        private Telerik.Reporting.TextBox textBox129;
        private Telerik.Reporting.TextBox textBox134;
        private Telerik.Reporting.TextBox textBox136;
        private Telerik.Reporting.TextBox textBox137;
        private Telerik.Reporting.TextBox textBox138;
        private Telerik.Reporting.TextBox textBox139;
        private Telerik.Reporting.TextBox textBox140;
        private Telerik.Reporting.TextBox textBox141;
        private Telerik.Reporting.TextBox textBox142;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.Table table13;
        private Telerik.Reporting.TextBox textBox144;
        private Telerik.Reporting.TextBox textBox143;
        private Telerik.Reporting.TextBox textBox145;
        private Telerik.Reporting.TextBox textBox146;
        private Telerik.Reporting.TextBox textBox147;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox150;
        private Telerik.Reporting.TextBox textBox152;
        private Telerik.Reporting.TextBox textBox156;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox149;
        private Telerik.Reporting.TextBox textBox157;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox153;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox154;
        private Telerik.Reporting.TextBox textBox151;
        private Telerik.Reporting.TextBox textBox160;
        private Telerik.Reporting.TextBox textBox161;
        private Telerik.Reporting.TextBox textBox162;
        private Telerik.Reporting.TextBox textBox163;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox148;
    }
}