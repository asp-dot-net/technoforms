namespace TechnoForms.Report
{
	partial class SAOrigin
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SAOrigin));
            Telerik.Reporting.NavigateToUrlAction navigateToUrlAction1 = new Telerik.Reporting.NavigateToUrlAction();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup72 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup73 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup74 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup75 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup76 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup77 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup78 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup79 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup80 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup81 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup82 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup83 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup84 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup85 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup86 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup87 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup88 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup89 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup90 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup91 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup92 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup93 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup60 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup61 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup62 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup63 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup64 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup65 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup66 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup67 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup68 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup69 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup70 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup71 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup122 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup123 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup124 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup125 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup126 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup127 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup128 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup129 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup130 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup131 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup132 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup133 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup134 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup135 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup96 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup97 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup98 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup99 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup100 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup101 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup102 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup103 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup94 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup95 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup106 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup107 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup108 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup104 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup105 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup113 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup114 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup115 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup116 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup117 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup118 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup119 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup120 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup121 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup109 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup110 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup111 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup112 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup146 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup147 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup148 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup149 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup150 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup151 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup152 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup153 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup154 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup155 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup156 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup157 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup158 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup159 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup160 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup161 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup162 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup163 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup164 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup165 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup166 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup167 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup168 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup169 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup136 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup137 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup138 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup139 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup140 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup141 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup142 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup143 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup144 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup145 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup170 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup171 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup172 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup173 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup174 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup175 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup176 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup177 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup178 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup179 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.table28 = new Telerik.Reporting.Table();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.table40 = new Telerik.Reporting.Table();
            this.textBox166 = new Telerik.Reporting.TextBox();
            this.table41 = new Telerik.Reporting.Table();
            this.textBox154 = new Telerik.Reporting.TextBox();
            this.table42 = new Telerik.Reporting.Table();
            this.textBox162 = new Telerik.Reporting.TextBox();
            this.pictureBox2 = new Telerik.Reporting.PictureBox();
            this.textBox168 = new Telerik.Reporting.TextBox();
            this.pictureBox3 = new Telerik.Reporting.PictureBox();
            this.textBox170 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.panel1 = new Telerik.Reporting.Panel();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.checkBox1 = new Telerik.Reporting.CheckBox();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.checkBox3 = new Telerik.Reporting.CheckBox();
            this.checkBox4 = new Telerik.Reporting.CheckBox();
            this.checkBox2 = new Telerik.Reporting.CheckBox();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.table10 = new Telerik.Reporting.Table();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.table11 = new Telerik.Reporting.Table();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.table12 = new Telerik.Reporting.Table();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.table13 = new Telerik.Reporting.Table();
            this.checkBox5 = new Telerik.Reporting.CheckBox();
            this.checkBox6 = new Telerik.Reporting.CheckBox();
            this.table14 = new Telerik.Reporting.Table();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.table15 = new Telerik.Reporting.Table();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.table16 = new Telerik.Reporting.Table();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.table17 = new Telerik.Reporting.Table();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.table18 = new Telerik.Reporting.Table();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.table19 = new Telerik.Reporting.Table();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.table20 = new Telerik.Reporting.Table();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.table21 = new Telerik.Reporting.Table();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.table22 = new Telerik.Reporting.Table();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.table23 = new Telerik.Reporting.Table();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.table24 = new Telerik.Reporting.Table();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.htmlTextBox2 = new Telerik.Reporting.HtmlTextBox();
            this.checkBox7 = new Telerik.Reporting.CheckBox();
            this.checkBox8 = new Telerik.Reporting.CheckBox();
            this.checkBox9 = new Telerik.Reporting.CheckBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.checkBox10 = new Telerik.Reporting.CheckBox();
            this.checkBox11 = new Telerik.Reporting.CheckBox();
            this.checkBox12 = new Telerik.Reporting.CheckBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.table25 = new Telerik.Reporting.Table();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.table26 = new Telerik.Reporting.Table();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.checkBox13 = new Telerik.Reporting.CheckBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.checkBox14 = new Telerik.Reporting.CheckBox();
            this.checkBox15 = new Telerik.Reporting.CheckBox();
            this.checkBox16 = new Telerik.Reporting.CheckBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.checkBox17 = new Telerik.Reporting.CheckBox();
            this.checkBox18 = new Telerik.Reporting.CheckBox();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.table27 = new Telerik.Reporting.Table();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.table29 = new Telerik.Reporting.Table();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.table30 = new Telerik.Reporting.Table();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.checkBox19 = new Telerik.Reporting.CheckBox();
            this.checkBox20 = new Telerik.Reporting.CheckBox();
            this.table31 = new Telerik.Reporting.Table();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.table32 = new Telerik.Reporting.Table();
            this.textBox112 = new Telerik.Reporting.TextBox();
            this.panel2 = new Telerik.Reporting.Panel();
            this.table33 = new Telerik.Reporting.Table();
            this.textBox106 = new Telerik.Reporting.TextBox();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.textBox114 = new Telerik.Reporting.TextBox();
            this.textBox115 = new Telerik.Reporting.TextBox();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.textBox117 = new Telerik.Reporting.TextBox();
            this.table35 = new Telerik.Reporting.Table();
            this.textBox119 = new Telerik.Reporting.TextBox();
            this.textBox120 = new Telerik.Reporting.TextBox();
            this.textBox122 = new Telerik.Reporting.TextBox();
            this.textBox124 = new Telerik.Reporting.TextBox();
            this.panel3 = new Telerik.Reporting.Panel();
            this.table37 = new Telerik.Reporting.Table();
            this.textBox123 = new Telerik.Reporting.TextBox();
            this.textBox125 = new Telerik.Reporting.TextBox();
            this.textBox130 = new Telerik.Reporting.TextBox();
            this.table34 = new Telerik.Reporting.Table();
            this.textBox131 = new Telerik.Reporting.TextBox();
            this.table36 = new Telerik.Reporting.Table();
            this.textBox121 = new Telerik.Reporting.TextBox();
            this.textBox126 = new Telerik.Reporting.TextBox();
            this.table38 = new Telerik.Reporting.Table();
            this.textBox118 = new Telerik.Reporting.TextBox();
            this.textBox127 = new Telerik.Reporting.TextBox();
            this.textBox128 = new Telerik.Reporting.TextBox();
            this.textBox129 = new Telerik.Reporting.TextBox();
            this.textBox132 = new Telerik.Reporting.TextBox();
            this.textBox133 = new Telerik.Reporting.TextBox();
            this.textBox134 = new Telerik.Reporting.TextBox();
            this.textBox135 = new Telerik.Reporting.TextBox();
            this.textBox138 = new Telerik.Reporting.TextBox();
            this.textBox139 = new Telerik.Reporting.TextBox();
            this.textBox140 = new Telerik.Reporting.TextBox();
            this.textBox136 = new Telerik.Reporting.TextBox();
            this.textBox137 = new Telerik.Reporting.TextBox();
            this.textBox141 = new Telerik.Reporting.TextBox();
            this.textBox142 = new Telerik.Reporting.TextBox();
            this.textBox143 = new Telerik.Reporting.TextBox();
            this.textBox144 = new Telerik.Reporting.TextBox();
            this.textBox145 = new Telerik.Reporting.TextBox();
            this.textBox146 = new Telerik.Reporting.TextBox();
            this.textBox152 = new Telerik.Reporting.TextBox();
            this.textBox155 = new Telerik.Reporting.TextBox();
            this.textBox156 = new Telerik.Reporting.TextBox();
            this.textBox157 = new Telerik.Reporting.TextBox();
            this.textBox147 = new Telerik.Reporting.TextBox();
            this.textBox153 = new Telerik.Reporting.TextBox();
            this.textBox148 = new Telerik.Reporting.TextBox();
            this.textBox149 = new Telerik.Reporting.TextBox();
            this.textBox150 = new Telerik.Reporting.TextBox();
            this.textBox159 = new Telerik.Reporting.TextBox();
            this.textBox160 = new Telerik.Reporting.TextBox();
            this.textBox161 = new Telerik.Reporting.TextBox();
            this.textBox151 = new Telerik.Reporting.TextBox();
            this.htmlTextBox3 = new Telerik.Reporting.HtmlTextBox();
            this.table39 = new Telerik.Reporting.Table();
            this.textBox158 = new Telerik.Reporting.TextBox();
            this.textBox163 = new Telerik.Reporting.TextBox();
            this.textBox165 = new Telerik.Reporting.TextBox();
            this.htmlTextBox4 = new Telerik.Reporting.HtmlTextBox();
            this.table43 = new Telerik.Reporting.Table();
            this.textBox167 = new Telerik.Reporting.TextBox();
            this.textBox169 = new Telerik.Reporting.TextBox();
            this.textBox171 = new Telerik.Reporting.TextBox();
            this.textBox173 = new Telerik.Reporting.TextBox();
            this.textBox164 = new Telerik.Reporting.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox102
            // 
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.374D), Telerik.Reporting.Drawing.Unit.Cm(0.701D));
            this.textBox102.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox102.Value = "Other";
            // 
            // table28
            // 
            this.table28.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(14.352D)));
            this.table28.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.539D)));
            this.table28.Body.SetCellContent(0, 0, this.textBox101);
            tableGroup1.Name = "tableGroup3";
            this.table28.ColumnGroups.Add(tableGroup1);
            this.table28.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox101});
            this.table28.Name = "table28";
            tableGroup2.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup2.Name = "detailTableGroup1";
            this.table28.RowGroups.Add(tableGroup2);
            this.table28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.564D), Telerik.Reporting.Drawing.Unit.Cm(0.751D));
            this.table28.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table28.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table28.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table28.StyleName = "";
            // 
            // textBox101
            // 
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.352D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox101.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // table40
            // 
            this.table40.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.613D)));
            this.table40.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.697D)));
            this.table40.Body.SetCellContent(0, 0, this.textBox166);
            tableGroup3.Name = "tableGroup3";
            this.table40.ColumnGroups.Add(tableGroup3);
            this.table40.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox166});
            this.table40.Name = "table40";
            tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup4.Name = "detailTableGroup1";
            this.table40.RowGroups.Add(tableGroup4);
            this.table40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.825D), Telerik.Reporting.Drawing.Unit.Cm(0.909D));
            this.table40.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table40.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table40.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table40.StyleName = "";
            // 
            // textBox166
            // 
            this.textBox166.Name = "textBox166";
            this.textBox166.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.614D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.textBox166.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // table41
            // 
            this.table41.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.613D)));
            this.table41.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.697D)));
            this.table41.Body.SetCellContent(0, 0, this.textBox154);
            tableGroup5.Name = "tableGroup3";
            this.table41.ColumnGroups.Add(tableGroup5);
            this.table41.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox154});
            this.table41.Name = "table41";
            tableGroup6.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup6.Name = "detailTableGroup1";
            this.table41.RowGroups.Add(tableGroup6);
            this.table41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.825D), Telerik.Reporting.Drawing.Unit.Cm(0.909D));
            this.table41.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table41.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table41.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table41.StyleName = "";
            // 
            // textBox154
            // 
            this.textBox154.Name = "textBox154";
            this.textBox154.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.614D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.textBox154.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // table42
            // 
            this.table42.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.613D)));
            this.table42.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.697D)));
            this.table42.Body.SetCellContent(0, 0, this.textBox162);
            tableGroup7.Name = "tableGroup3";
            this.table42.ColumnGroups.Add(tableGroup7);
            this.table42.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox162});
            this.table42.Name = "table42";
            tableGroup8.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup8.Name = "detailTableGroup1";
            this.table42.RowGroups.Add(tableGroup8);
            this.table42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.825D), Telerik.Reporting.Drawing.Unit.Cm(0.909D));
            this.table42.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table42.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table42.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table42.StyleName = "";
            // 
            // textBox162
            // 
            this.textBox162.Name = "textBox162";
            this.textBox162.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.614D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.textBox162.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // pictureBox2
            // 
            this.pictureBox2.MimeType = "image/png";
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.75D), Telerik.Reporting.Drawing.Unit.Cm(0.75D));
            this.pictureBox2.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            this.pictureBox2.StyleName = "";
            this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
            // 
            // textBox168
            // 
            this.textBox168.Name = "textBox168";
            this.textBox168.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.551D), Telerik.Reporting.Drawing.Unit.Cm(0.75D));
            this.textBox168.Style.Font.Bold = true;
            this.textBox168.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox168.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox168.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox168.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox168.Value = "1300 132 480";
            // 
            // pictureBox3
            // 
            this.pictureBox3.MimeType = "image/png";
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.756D), Telerik.Reporting.Drawing.Unit.Cm(0.75D));
            this.pictureBox3.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Center;
            this.pictureBox3.StyleName = "";
            this.pictureBox3.Value = ((object)(resources.GetObject("pictureBox3.Value")));
            // 
            // textBox170
            // 
            navigateToUrlAction1.Url = "https://www.originenergy.com.au/meters";
            this.textBox170.Action = navigateToUrlAction1;
            this.textBox170.Name = "textBox170";
            this.textBox170.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.377D), Telerik.Reporting.Drawing.Unit.Cm(0.75D));
            this.textBox170.Style.Font.Bold = true;
            this.textBox170.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox170.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox170.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox170.Value = "www.originenergy.com.au/meters";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(552.34D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.textBox1,
            this.htmlTextBox1,
            this.panel1,
            this.panel2});
            this.detail.Name = "detail";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pictureBox1.MimeType = "image/jpeg";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.462D), Telerik.Reporting.Drawing.Unit.Inch(1.544D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Stretch;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.381D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.4D), Telerik.Reporting.Drawing.Unit.Inch(0.785D));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Name = "MS UI Gothic";
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(22D);
            this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Justify;
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "APPLICATION TO CHANGE OR\r\nREMOVE AN ELECTRICITY SUPPLY";
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0.381D), Telerik.Reporting.Drawing.Unit.Inch(1.044D));
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.6D), Telerik.Reporting.Drawing.Unit.Inch(0.5D));
            this.htmlTextBox1.Value = resources.GetString("htmlTextBox1.Value");
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table1,
            this.table7,
            this.table10,
            this.table24});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.719D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.481D), Telerik.Reporting.Drawing.Unit.Inch(8.782D));
            this.panel1.Style.BackgroundColor = System.Drawing.SystemColors.MenuBar;
            this.panel1.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.5D);
            this.panel1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.5D);
            this.panel1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0.5D);
            this.panel1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.5D);
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.563D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.453D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.015D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.713D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.732D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.615D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.28D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.08D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.08D)));
            this.table1.Body.SetCellContent(0, 1, this.textBox3);
            this.table1.Body.SetCellContent(0, 0, this.textBox2);
            this.table1.Body.SetCellContent(1, 0, this.textBox4);
            this.table1.Body.SetCellContent(1, 1, this.textBox6);
            this.table1.Body.SetCellContent(1, 2, this.textBox8);
            this.table1.Body.SetCellContent(1, 3, this.textBox9);
            this.table1.Body.SetCellContent(2, 0, this.textBox10);
            this.table1.Body.SetCellContent(2, 1, this.textBox11);
            this.table1.Body.SetCellContent(2, 3, this.textBox13);
            this.table1.Body.SetCellContent(0, 2, this.checkBox1);
            this.table1.Body.SetCellContent(1, 4, this.textBox7);
            this.table1.Body.SetCellContent(2, 2, this.table2);
            this.table1.Body.SetCellContent(2, 4, this.table3);
            this.table1.Body.SetCellContent(0, 3, this.checkBox2, 1, 2);
            this.table1.Body.SetCellContent(3, 0, this.textBox5);
            this.table1.Body.SetCellContent(3, 1, this.table4, 1, 4);
            tableGroup23.Name = "group";
            tableGroup24.Name = "tableGroup";
            tableGroup25.Name = "tableGroup1";
            tableGroup26.Name = "tableGroup2";
            tableGroup27.Name = "group4";
            this.table1.ColumnGroups.Add(tableGroup23);
            this.table1.ColumnGroups.Add(tableGroup24);
            this.table1.ColumnGroups.Add(tableGroup25);
            this.table1.ColumnGroups.Add(tableGroup26);
            this.table1.ColumnGroups.Add(tableGroup27);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox2,
            this.textBox3,
            this.checkBox1,
            this.checkBox2,
            this.textBox4,
            this.textBox6,
            this.textBox8,
            this.textBox9,
            this.textBox7,
            this.textBox10,
            this.textBox11,
            this.table2,
            this.textBox13,
            this.table3,
            this.textBox5,
            this.table4});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.188D));
            this.table1.Name = "table1";
            tableGroup29.Name = "group1";
            tableGroup30.Name = "group2";
            tableGroup31.Name = "group3";
            tableGroup32.Name = "group5";
            tableGroup28.ChildGroups.Add(tableGroup29);
            tableGroup28.ChildGroups.Add(tableGroup30);
            tableGroup28.ChildGroups.Add(tableGroup31);
            tableGroup28.ChildGroups.Add(tableGroup32);
            tableGroup28.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup28.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup28);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.476D), Telerik.Reporting.Drawing.Unit.Cm(3.055D));
            // 
            // textBox3
            // 
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.453D), Telerik.Reporting.Drawing.Unit.Cm(0.615D));
            this.textBox3.Style.Font.Bold = true;
            this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox3.Value = "REQUEST TYPE";
            // 
            // textBox2
            // 
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.615D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox2.StyleName = "";
            this.textBox2.Value = "1.";
            // 
            // textBox4
            // 
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.28D));
            this.textBox4.StyleName = "";
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.453D), Telerik.Reporting.Drawing.Unit.Cm(0.28D));
            this.textBox6.StyleName = "";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.015D), Telerik.Reporting.Drawing.Unit.Cm(0.28D));
            this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(30D);
            this.textBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "Complete all sections";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.713D), Telerik.Reporting.Drawing.Unit.Cm(0.28D));
            this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(30D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.StyleName = "";
            this.textBox9.Value = "Complete sections 1, 2, 3 & 5";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox10.StyleName = "";
            // 
            // textBox11
            // 
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.453D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox11.StyleName = "";
            this.textBox11.Value = "Change/remove supply on";
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.713D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox13.StyleName = "";
            this.textBox13.Value = "Will the property be vacant? ";
            // 
            // checkBox1
            // 
            this.checkBox1.CheckAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.checkBox1.CheckedImage = ((object)(resources.GetObject("checkBox1.CheckedImage")));
            this.checkBox1.IndeterminateImage = ((object)(resources.GetObject("checkBox1.IndeterminateImage")));
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.015D), Telerik.Reporting.Drawing.Unit.Cm(0.615D));
            this.checkBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.checkBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.checkBox1.StyleName = "";
            this.checkBox1.Text = "Change electricity supply";
            this.checkBox1.UncheckedImage = ((object)(resources.GetObject("checkBox1.UncheckedImage")));
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.732D), Telerik.Reporting.Drawing.Unit.Cm(0.28D));
            this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.StyleName = "";
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.662D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox16);
            tableGroup9.Name = "tableGroup3";
            this.table2.ColumnGroups.Add(tableGroup9);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox16});
            this.table2.Name = "table2";
            tableGroup10.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup10.Name = "detailTableGroup1";
            this.table2.RowGroups.Add(tableGroup10);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.015D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table2.StyleName = "";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.663D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox16.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.378D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.354D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.08D)));
            this.table3.Body.SetCellContent(0, 0, this.checkBox3);
            this.table3.Body.SetCellContent(0, 1, this.checkBox4);
            tableGroup11.Name = "tableGroup4";
            tableGroup12.Name = "tableGroup5";
            this.table3.ColumnGroups.Add(tableGroup11);
            this.table3.ColumnGroups.Add(tableGroup12);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox3,
            this.checkBox4});
            this.table3.Name = "table3";
            tableGroup13.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup13.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup13);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.732D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table3.StyleName = "";
            // 
            // checkBox3
            // 
            this.checkBox3.CheckedImage = ((object)(resources.GetObject("checkBox3.CheckedImage")));
            this.checkBox3.IndeterminateImage = ((object)(resources.GetObject("checkBox3.IndeterminateImage")));
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.378D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.checkBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox3.StyleName = "";
            this.checkBox3.Text = "Yes";
            this.checkBox3.UncheckedImage = ((object)(resources.GetObject("checkBox3.UncheckedImage")));
            // 
            // checkBox4
            // 
            this.checkBox4.CheckedImage = ((object)(resources.GetObject("checkBox4.CheckedImage")));
            this.checkBox4.IndeterminateImage = ((object)(resources.GetObject("checkBox4.IndeterminateImage")));
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.354D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.checkBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox4.StyleName = "";
            this.checkBox4.Text = "No";
            this.checkBox4.UncheckedImage = ((object)(resources.GetObject("checkBox4.UncheckedImage")));
            // 
            // checkBox2
            // 
            this.checkBox2.CheckAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.checkBox2.CheckedImage = ((object)(resources.GetObject("checkBox2.CheckedImage")));
            this.checkBox2.IndeterminateImage = ((object)(resources.GetObject("checkBox2.IndeterminateImage")));
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.445D), Telerik.Reporting.Drawing.Unit.Cm(0.615D));
            this.checkBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.checkBox2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(60D);
            this.checkBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.checkBox2.StyleName = "";
            this.checkBox2.Text = "Remove electricity supply";
            this.checkBox2.UncheckedImage = ((object)(resources.GetObject("checkBox2.UncheckedImage")));
            // 
            // textBox5
            // 
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox5.StyleName = "";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.039D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(9.644D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.66D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.569D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.08D)));
            this.table4.Body.SetCellContent(0, 0, this.textBox15);
            this.table4.Body.SetCellContent(0, 2, this.textBox14);
            this.table4.Body.SetCellContent(0, 1, this.table5);
            this.table4.Body.SetCellContent(0, 3, this.table6);
            tableGroup18.Name = "tableGroup6";
            tableGroup19.Name = "tableGroup7";
            tableGroup20.Name = "group6";
            tableGroup21.Name = "tableGroup8";
            this.table4.ColumnGroups.Add(tableGroup18);
            this.table4.ColumnGroups.Add(tableGroup19);
            this.table4.ColumnGroups.Add(tableGroup20);
            this.table4.ColumnGroups.Add(tableGroup21);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox15,
            this.table5,
            this.textBox14,
            this.table6});
            this.table4.Name = "table4";
            tableGroup22.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup22.Name = "detailTableGroup3";
            this.table4.RowGroups.Add(tableGroup22);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.912D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table4.StyleName = "";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.039D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.Value = "Company";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.66D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "ABN";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(9.291D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table5.Body.SetCellContent(0, 0, this.textBox12);
            tableGroup14.Name = "tableGroup3";
            this.table5.ColumnGroups.Add(tableGroup14);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox12});
            this.table5.Name = "table5";
            tableGroup15.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup15.Name = "detailTableGroup1";
            this.table5.RowGroups.Add(tableGroup15);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.644D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table5.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table5.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table5.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table5.StyleName = "";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.291D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox12.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.216D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table6.Body.SetCellContent(0, 0, this.textBox17);
            tableGroup16.Name = "tableGroup3";
            this.table6.ColumnGroups.Add(tableGroup16);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox17});
            this.table6.Name = "table6";
            tableGroup17.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup17.Name = "detailTableGroup1";
            this.table6.RowGroups.Add(tableGroup17);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.569D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table6.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table6.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table6.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table6.StyleName = "";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.217D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox17.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox17.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.564D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.995D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.998D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.521D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.398D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.751D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.08D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox20);
            this.table7.Body.SetCellContent(0, 3, this.textBox23);
            this.table7.Body.SetCellContent(1, 0, this.textBox24);
            this.table7.Body.SetCellContent(1, 1, this.textBox25);
            this.table7.Body.SetCellContent(1, 3, this.textBox26);
            this.table7.Body.SetCellContent(0, 4, this.textBox27);
            this.table7.Body.SetCellContent(1, 2, this.table8);
            this.table7.Body.SetCellContent(1, 4, this.table9);
            this.table7.Body.SetCellContent(0, 1, this.textBox21, 1, 2);
            tableGroup37.Name = "group";
            tableGroup38.Name = "tableGroup";
            tableGroup39.Name = "tableGroup1";
            tableGroup40.Name = "tableGroup2";
            tableGroup41.Name = "group4";
            this.table7.ColumnGroups.Add(tableGroup37);
            this.table7.ColumnGroups.Add(tableGroup38);
            this.table7.ColumnGroups.Add(tableGroup39);
            this.table7.ColumnGroups.Add(tableGroup40);
            this.table7.ColumnGroups.Add(tableGroup41);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox20,
            this.textBox21,
            this.textBox23,
            this.textBox27,
            this.textBox24,
            this.textBox25,
            this.table8,
            this.textBox26,
            this.table9});
            this.table7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(3.624D));
            this.table7.Name = "table7";
            tableGroup43.Name = "group2";
            tableGroup44.Name = "group3";
            tableGroup42.ChildGroups.Add(tableGroup43);
            tableGroup42.ChildGroups.Add(tableGroup44);
            tableGroup42.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup42.Name = "detailTableGroup";
            this.table7.RowGroups.Add(tableGroup42);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.476D), Telerik.Reporting.Drawing.Unit.Cm(1.831D));
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.564D), Telerik.Reporting.Drawing.Unit.Cm(0.751D));
            this.textBox20.Style.Font.Bold = true;
            this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "2.";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.521D), Telerik.Reporting.Drawing.Unit.Cm(0.751D));
            this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(30D);
            this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox23.StyleName = "";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.564D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox24.StyleName = "";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.995D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox25.StyleName = "";
            this.textBox25.Value = "Full name";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.521D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox26.StyleName = "";
            this.textBox26.Value = "Daytime no.";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.398D), Telerik.Reporting.Drawing.Unit.Cm(0.751D));
            this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.StyleName = "";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.645D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table8.Body.SetCellContent(0, 0, this.textBox28);
            tableGroup33.Name = "tableGroup3";
            this.table8.ColumnGroups.Add(tableGroup33);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox28});
            this.table8.Name = "table8";
            tableGroup34.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup34.Name = "detailTableGroup1";
            this.table8.RowGroups.Add(tableGroup34);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.998D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table8.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table8.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table8.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table8.StyleName = "";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.645D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox28.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.045D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table9.Body.SetCellContent(0, 0, this.textBox30);
            tableGroup35.Name = "tableGroup3";
            this.table9.ColumnGroups.Add(tableGroup35);
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox30});
            this.table9.Name = "table9";
            tableGroup36.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup36.Name = "detailTableGroup1";
            this.table9.RowGroups.Add(tableGroup36);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.398D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table9.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table9.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table9.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table9.StyleName = "";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.045D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox30.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox30.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(10.993D), Telerik.Reporting.Drawing.Unit.Cm(0.751D));
            this.textBox21.Style.Font.Bold = true;
            this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox21.StyleName = "";
            this.textBox21.Value = "CUSTOMER DETAILS";
            // 
            // table10
            // 
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.516D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.315D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.8D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.986D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.617D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.342D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.485D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.625D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.669D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.16D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.566D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.394D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.539D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.799D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.08D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.08D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.447D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.72D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.948D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.81D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.81D)));
            this.table10.Body.SetCellContent(0, 0, this.textBox18);
            this.table10.Body.SetCellContent(2, 0, this.textBox22);
            this.table10.Body.SetCellContent(2, 1, this.textBox29);
            this.table10.Body.SetCellContent(2, 3, this.textBox31);
            this.table10.Body.SetCellContent(0, 4, this.textBox32);
            this.table10.Body.SetCellContent(2, 2, this.table11);
            this.table10.Body.SetCellContent(2, 4, this.table12);
            this.table10.Body.SetCellContent(0, 5, this.textBox36);
            this.table10.Body.SetCellContent(2, 5, this.textBox37);
            this.table10.Body.SetCellContent(0, 6, this.textBox38);
            this.table10.Body.SetCellContent(0, 7, this.textBox40);
            this.table10.Body.SetCellContent(2, 7, this.textBox41);
            this.table10.Body.SetCellContent(0, 9, this.textBox42);
            this.table10.Body.SetCellContent(2, 9, this.textBox43);
            this.table10.Body.SetCellContent(0, 8, this.textBox44);
            this.table10.Body.SetCellContent(0, 10, this.textBox46);
            this.table10.Body.SetCellContent(0, 11, this.textBox48);
            this.table10.Body.SetCellContent(0, 1, this.textBox35, 1, 3);
            this.table10.Body.SetCellContent(3, 0, this.textBox19);
            this.table10.Body.SetCellContent(3, 1, this.textBox50);
            this.table10.Body.SetCellContent(3, 7, this.textBox56);
            this.table10.Body.SetCellContent(3, 10, this.textBox59);
            this.table10.Body.SetCellContent(1, 0, this.textBox61);
            this.table10.Body.SetCellContent(1, 7, this.textBox68);
            this.table10.Body.SetCellContent(1, 8, this.textBox69);
            this.table10.Body.SetCellContent(1, 9, this.textBox70);
            this.table10.Body.SetCellContent(1, 10, this.textBox71);
            this.table10.Body.SetCellContent(1, 11, this.textBox72);
            this.table10.Body.SetCellContent(1, 1, this.textBox62, 1, 2);
            this.table10.Body.SetCellContent(1, 3, this.table13, 1, 4);
            this.table10.Body.SetCellContent(2, 6, this.table14);
            this.table10.Body.SetCellContent(2, 8, this.table15);
            this.table10.Body.SetCellContent(2, 10, this.table16, 1, 2);
            this.table10.Body.SetCellContent(3, 2, this.table17, 1, 5);
            this.table10.Body.SetCellContent(3, 8, this.table18, 1, 2);
            this.table10.Body.SetCellContent(3, 11, this.table19);
            this.table10.Body.SetCellContent(4, 0, this.textBox53);
            this.table10.Body.SetCellContent(5, 0, this.textBox74);
            this.table10.Body.SetCellContent(4, 1, this.textBox54, 1, 11);
            this.table10.Body.SetCellContent(5, 1, this.textBox75, 1, 11);
            this.table10.Body.SetCellContent(6, 0, this.textBox55);
            this.table10.Body.SetCellContent(6, 1, this.textBox57);
            this.table10.Body.SetCellContent(6, 2, this.table20, 1, 4);
            this.table10.Body.SetCellContent(6, 6, this.textBox65, 1, 2);
            this.table10.Body.SetCellContent(6, 8, this.table21, 1, 4);
            this.table10.Body.SetCellContent(7, 0, this.textBox63);
            this.table10.Body.SetCellContent(7, 6, this.textBox77);
            this.table10.Body.SetCellContent(7, 7, this.textBox78);
            this.table10.Body.SetCellContent(8, 0, this.textBox83);
            this.table10.Body.SetCellContent(8, 6, this.textBox89);
            this.table10.Body.SetCellContent(8, 7, this.textBox90);
            this.table10.Body.SetCellContent(7, 1, this.textBox64, 2, 5);
            this.table10.Body.SetCellContent(7, 8, this.table22, 1, 4);
            this.table10.Body.SetCellContent(8, 8, this.table23, 1, 4);
            tableGroup72.Name = "group";
            tableGroup73.Name = "tableGroup";
            tableGroup74.Name = "tableGroup1";
            tableGroup75.Name = "tableGroup2";
            tableGroup76.Name = "group4";
            tableGroup77.Name = "group7";
            tableGroup78.Name = "group8";
            tableGroup79.Name = "group9";
            tableGroup80.Name = "group11";
            tableGroup81.Name = "group10";
            tableGroup82.Name = "group12";
            tableGroup83.Name = "group13";
            this.table10.ColumnGroups.Add(tableGroup72);
            this.table10.ColumnGroups.Add(tableGroup73);
            this.table10.ColumnGroups.Add(tableGroup74);
            this.table10.ColumnGroups.Add(tableGroup75);
            this.table10.ColumnGroups.Add(tableGroup76);
            this.table10.ColumnGroups.Add(tableGroup77);
            this.table10.ColumnGroups.Add(tableGroup78);
            this.table10.ColumnGroups.Add(tableGroup79);
            this.table10.ColumnGroups.Add(tableGroup80);
            this.table10.ColumnGroups.Add(tableGroup81);
            this.table10.ColumnGroups.Add(tableGroup82);
            this.table10.ColumnGroups.Add(tableGroup83);
            this.table10.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox18,
            this.textBox35,
            this.textBox32,
            this.textBox36,
            this.textBox38,
            this.textBox40,
            this.textBox44,
            this.textBox42,
            this.textBox46,
            this.textBox48,
            this.textBox61,
            this.textBox62,
            this.table13,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.textBox22,
            this.textBox29,
            this.table11,
            this.textBox31,
            this.table12,
            this.textBox37,
            this.table14,
            this.textBox41,
            this.table15,
            this.textBox43,
            this.table16,
            this.textBox19,
            this.textBox50,
            this.table17,
            this.textBox56,
            this.table18,
            this.textBox59,
            this.table19,
            this.textBox53,
            this.textBox54,
            this.textBox74,
            this.textBox75,
            this.textBox55,
            this.textBox57,
            this.table20,
            this.textBox65,
            this.table21,
            this.textBox63,
            this.textBox64,
            this.textBox77,
            this.textBox78,
            this.table22,
            this.textBox83,
            this.textBox89,
            this.textBox90,
            this.table23});
            this.table10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(5.84D));
            this.table10.Name = "table10";
            tableGroup85.Name = "group2";
            tableGroup86.Name = "group15";
            tableGroup87.Name = "group3";
            tableGroup88.Name = "group14";
            tableGroup89.Name = "group16";
            tableGroup90.Name = "group17";
            tableGroup91.Name = "group18";
            tableGroup92.Name = "group19";
            tableGroup93.Name = "group20";
            tableGroup84.ChildGroups.Add(tableGroup85);
            tableGroup84.ChildGroups.Add(tableGroup86);
            tableGroup84.ChildGroups.Add(tableGroup87);
            tableGroup84.ChildGroups.Add(tableGroup88);
            tableGroup84.ChildGroups.Add(tableGroup89);
            tableGroup84.ChildGroups.Add(tableGroup90);
            tableGroup84.ChildGroups.Add(tableGroup91);
            tableGroup84.ChildGroups.Add(tableGroup92);
            tableGroup84.ChildGroups.Add(tableGroup93);
            tableGroup84.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup84.Name = "detailTableGroup";
            this.table10.RowGroups.Add(tableGroup84);
            this.table10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.476D), Telerik.Reporting.Drawing.Unit.Cm(7.233D));
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.516D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox18.Style.Font.Bold = true;
            this.textBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox18.StyleName = "";
            this.textBox18.Value = "3.";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.516D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox22.StyleName = "";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.315D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox29.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "Lot";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.986D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox31.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox31.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox31.StyleName = "";
            this.textBox31.Value = "Level";
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.617D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox32.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox32.StyleName = "";
            // 
            // table11
            // 
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.447D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table11.Body.SetCellContent(0, 0, this.textBox33);
            tableGroup45.Name = "tableGroup3";
            this.table11.ColumnGroups.Add(tableGroup45);
            this.table11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox33});
            this.table11.Name = "table11";
            tableGroup46.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup46.Name = "detailTableGroup1";
            this.table11.RowGroups.Add(tableGroup46);
            this.table11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table11.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table11.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table11.StyleName = "";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.447D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox33.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // table12
            // 
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.264D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table12.Body.SetCellContent(0, 0, this.textBox34);
            tableGroup47.Name = "tableGroup3";
            this.table12.ColumnGroups.Add(tableGroup47);
            this.table12.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox34});
            this.table12.Name = "table12";
            tableGroup48.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup48.Name = "detailTableGroup1";
            this.table12.RowGroups.Add(tableGroup48);
            this.table12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.617D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table12.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table12.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table12.StyleName = "";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.264D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox34.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox34.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.342D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox36.StyleName = "";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.342D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox37.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox37.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox37.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox37.StyleName = "";
            this.textBox37.Value = "Unit no.";
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.485D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox38.StyleName = "";
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.625D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox40.StyleName = "";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.625D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox41.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox41.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox41.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.StyleName = "";
            this.textBox41.Value = "Street no";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.16D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox42.StyleName = "";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.16D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox43.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox43.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox43.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox43.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.StyleName = "";
            this.textBox43.Value = "Street";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.669D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox44.StyleName = "";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.566D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.StyleName = "";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.394D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.StyleName = "";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.101D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox35.Style.Font.Bold = true;
            this.textBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox35.StyleName = "";
            this.textBox35.Value = "PROPERTY DETAILS";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.516D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.315D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox50.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.StyleName = "";
            this.textBox50.Value = "Suburb";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.625D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox56.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox56.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox56.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox56.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox56.StyleName = "";
            this.textBox56.Value = "State";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.566D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.textBox59.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox59.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox59.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox59.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox59.StyleName = "";
            this.textBox59.Value = "Postcode";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.516D), Telerik.Reporting.Drawing.Unit.Cm(0.799D));
            this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox61.StyleName = "";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.625D), Telerik.Reporting.Drawing.Unit.Cm(0.799D));
            this.textBox68.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox68.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox68.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox68.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.StyleName = "";
            // 
            // textBox69
            // 
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.669D), Telerik.Reporting.Drawing.Unit.Cm(0.799D));
            this.textBox69.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox69.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox69.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox69.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox69.StyleName = "";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.16D), Telerik.Reporting.Drawing.Unit.Cm(0.799D));
            this.textBox70.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox70.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox70.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox70.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox70.StyleName = "";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.566D), Telerik.Reporting.Drawing.Unit.Cm(0.799D));
            this.textBox71.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox71.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox71.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox71.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.StyleName = "";
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.394D), Telerik.Reporting.Drawing.Unit.Cm(0.799D));
            this.textBox72.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox72.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox72.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox72.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox72.StyleName = "";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.115D), Telerik.Reporting.Drawing.Unit.Cm(0.799D));
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.StyleName = "";
            this.textBox62.Value = "Property type ";
            // 
            // table13
            // 
            this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.739D)));
            this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.691D)));
            this.table13.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.799D)));
            this.table13.Body.SetCellContent(0, 0, this.checkBox5);
            this.table13.Body.SetCellContent(0, 1, this.checkBox6);
            tableGroup49.Name = "tableGroup4";
            tableGroup50.Name = "tableGroup5";
            this.table13.ColumnGroups.Add(tableGroup49);
            this.table13.ColumnGroups.Add(tableGroup50);
            this.table13.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox5,
            this.checkBox6});
            this.table13.Name = "table13";
            tableGroup51.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup51.Name = "detailTableGroup2";
            this.table13.RowGroups.Add(tableGroup51);
            this.table13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.43D), Telerik.Reporting.Drawing.Unit.Cm(0.799D));
            this.table13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table13.StyleName = "";
            // 
            // checkBox5
            // 
            this.checkBox5.CheckedImage = ((object)(resources.GetObject("checkBox5.CheckedImage")));
            this.checkBox5.IndeterminateImage = ((object)(resources.GetObject("checkBox5.IndeterminateImage")));
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.739D), Telerik.Reporting.Drawing.Unit.Cm(0.799D));
            this.checkBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox5.StyleName = "";
            this.checkBox5.Text = "Residential";
            this.checkBox5.UncheckedImage = ((object)(resources.GetObject("checkBox5.UncheckedImage")));
            // 
            // checkBox6
            // 
            this.checkBox6.CheckedImage = ((object)(resources.GetObject("checkBox6.CheckedImage")));
            this.checkBox6.IndeterminateImage = ((object)(resources.GetObject("checkBox6.IndeterminateImage")));
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.691D), Telerik.Reporting.Drawing.Unit.Cm(0.799D));
            this.checkBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox6.StyleName = "";
            this.checkBox6.Text = "Commercial";
            this.checkBox6.UncheckedImage = ((object)(resources.GetObject("checkBox6.UncheckedImage")));
            // 
            // table14
            // 
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.132D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table14.Body.SetCellContent(0, 0, this.textBox49);
            tableGroup52.Name = "tableGroup3";
            this.table14.ColumnGroups.Add(tableGroup52);
            this.table14.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox49});
            this.table14.Name = "table14";
            tableGroup53.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup53.Name = "detailTableGroup1";
            this.table14.RowGroups.Add(tableGroup53);
            this.table14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.485D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table14.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table14.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table14.StyleName = "";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.132D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox49.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox49.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // table15
            // 
            this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.316D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table15.Body.SetCellContent(0, 0, this.textBox39);
            tableGroup54.Name = "tableGroup3";
            this.table15.ColumnGroups.Add(tableGroup54);
            this.table15.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox39});
            this.table15.Name = "table15";
            tableGroup55.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup55.Name = "detailTableGroup1";
            this.table15.RowGroups.Add(tableGroup55);
            this.table15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.669D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table15.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table15.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table15.StyleName = "";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.316D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox39.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox39.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // table16
            // 
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.607D)));
            this.table16.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table16.Body.SetCellContent(0, 0, this.textBox45);
            tableGroup56.Name = "tableGroup3";
            this.table16.ColumnGroups.Add(tableGroup56);
            this.table16.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox45});
            this.table16.Name = "table16";
            tableGroup57.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup57.Name = "detailTableGroup1";
            this.table16.RowGroups.Add(tableGroup57);
            this.table16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.96D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table16.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table16.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table16.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table16.StyleName = "";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.608D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox45.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox45.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // table17
            // 
            this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.877D)));
            this.table17.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table17.Body.SetCellContent(0, 0, this.textBox47);
            tableGroup58.Name = "tableGroup3";
            this.table17.ColumnGroups.Add(tableGroup58);
            this.table17.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox47});
            this.table17.Name = "table17";
            tableGroup59.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup59.Name = "detailTableGroup1";
            this.table17.RowGroups.Add(tableGroup59);
            this.table17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.23D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table17.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table17.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table17.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table17.StyleName = "";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.878D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox47.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // table18
            // 
            this.table18.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.476D)));
            this.table18.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table18.Body.SetCellContent(0, 0, this.textBox51);
            tableGroup60.Name = "tableGroup3";
            this.table18.ColumnGroups.Add(tableGroup60);
            this.table18.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox51});
            this.table18.Name = "table18";
            tableGroup61.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup61.Name = "detailTableGroup1";
            this.table18.RowGroups.Add(tableGroup61);
            this.table18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.829D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table18.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table18.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table18.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table18.StyleName = "";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.476D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox51.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox51.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // table19
            // 
            this.table19.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.041D)));
            this.table19.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.657D)));
            this.table19.Body.SetCellContent(0, 0, this.textBox52);
            tableGroup62.Name = "tableGroup3";
            this.table19.ColumnGroups.Add(tableGroup62);
            this.table19.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox52});
            this.table19.Name = "table19";
            tableGroup63.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup63.Name = "detailTableGroup1";
            this.table19.RowGroups.Add(tableGroup63);
            this.table19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.394D), Telerik.Reporting.Drawing.Unit.Cm(1.08D));
            this.table19.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table19.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table19.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table19.StyleName = "";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.041D), Telerik.Reporting.Drawing.Unit.Cm(0.657D));
            this.textBox52.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox52.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.516D), Telerik.Reporting.Drawing.Unit.Cm(0.447D));
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox53.StyleName = "";
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.516D), Telerik.Reporting.Drawing.Unit.Cm(0.72D));
            this.textBox74.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox74.StyleName = "";
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.96D), Telerik.Reporting.Drawing.Unit.Cm(0.447D));
            this.textBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox54.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.StyleName = "";
            this.textBox54.Value = "Description of work (e.g. upgrade to phase 3 power, add solar, add service line t" +
    "o property, etc.)";
            // 
            // textBox75
            // 
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.96D), Telerik.Reporting.Drawing.Unit.Cm(0.72D));
            this.textBox75.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox75.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox75.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox75.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox75.StyleName = "";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.516D), Telerik.Reporting.Drawing.Unit.Cm(0.948D));
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.StyleName = "";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.315D), Telerik.Reporting.Drawing.Unit.Cm(0.948D));
            this.textBox57.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox57.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox57.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox57.StyleName = "";
            this.textBox57.Value = "NMI";
            // 
            // table20
            // 
            this.table20.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.393D)));
            this.table20.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.595D)));
            this.table20.Body.SetCellContent(0, 0, this.textBox60);
            tableGroup64.Name = "tableGroup3";
            this.table20.ColumnGroups.Add(tableGroup64);
            this.table20.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox60});
            this.table20.Name = "table20";
            tableGroup65.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup65.Name = "detailTableGroup1";
            this.table20.RowGroups.Add(tableGroup65);
            this.table20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.746D), Telerik.Reporting.Drawing.Unit.Cm(0.948D));
            this.table20.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table20.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table20.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table20.StyleName = "";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.393D), Telerik.Reporting.Drawing.Unit.Cm(0.595D));
            this.textBox60.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.11D), Telerik.Reporting.Drawing.Unit.Cm(0.948D));
            this.textBox65.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox65.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox65.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox65.StyleName = "";
            this.textBox65.Value = "Meter number(s)";
            // 
            // table21
            // 
            this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.436D)));
            this.table21.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.595D)));
            this.table21.Body.SetCellContent(0, 0, this.textBox58);
            tableGroup66.Name = "tableGroup3";
            this.table21.ColumnGroups.Add(tableGroup66);
            this.table21.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox58});
            this.table21.Name = "table21";
            tableGroup67.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup67.Name = "detailTableGroup1";
            this.table21.RowGroups.Add(tableGroup67);
            this.table21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.789D), Telerik.Reporting.Drawing.Unit.Cm(0.948D));
            this.table21.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table21.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table21.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.table21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table21.StyleName = "";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.437D), Telerik.Reporting.Drawing.Unit.Cm(0.595D));
            this.textBox58.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox58.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(7D);
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.516D), Telerik.Reporting.Drawing.Unit.Cm(0.81D));
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.StyleName = "";
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.485D), Telerik.Reporting.Drawing.Unit.Cm(0.81D));
            this.textBox77.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox77.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox77.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox77.StyleName = "";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.625D), Telerik.Reporting.Drawing.Unit.Cm(0.81D));
            this.textBox78.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox78.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox78.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox78.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox78.StyleName = "";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.516D), Telerik.Reporting.Drawing.Unit.Cm(0.81D));
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox83.StyleName = "";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.485D), Telerik.Reporting.Drawing.Unit.Cm(0.81D));
            this.textBox89.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox89.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox89.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox89.StyleName = "";
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.625D), Telerik.Reporting.Drawing.Unit.Cm(0.81D));
            this.textBox90.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox90.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox90.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox90.StyleName = "";
            // 
            // textBox64
            // 
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.061D), Telerik.Reporting.Drawing.Unit.Cm(1.62D));
            this.textBox64.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox64.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox64.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox64.StyleName = "";
            this.textBox64.Value = "Providing accurate NMI and meter numbers helps us to change or remove the correct" +
    " service";
            // 
            // table22
            // 
            this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.436D)));
            this.table22.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.598D)));
            this.table22.Body.SetCellContent(0, 0, this.textBox66);
            tableGroup68.Name = "tableGroup3";
            this.table22.ColumnGroups.Add(tableGroup68);
            this.table22.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox66});
            this.table22.Name = "table22";
            tableGroup69.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup69.Name = "detailTableGroup1";
            this.table22.RowGroups.Add(tableGroup69);
            this.table22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.789D), Telerik.Reporting.Drawing.Unit.Cm(0.81D));
            this.table22.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table22.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table22.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table22.StyleName = "";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.437D), Telerik.Reporting.Drawing.Unit.Cm(0.598D));
            this.textBox66.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // table23
            // 
            this.table23.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.436D)));
            this.table23.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.598D)));
            this.table23.Body.SetCellContent(0, 0, this.textBox67);
            tableGroup70.Name = "tableGroup3";
            this.table23.ColumnGroups.Add(tableGroup70);
            this.table23.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox67});
            this.table23.Name = "table23";
            tableGroup71.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup71.Name = "detailTableGroup1";
            this.table23.RowGroups.Add(tableGroup71);
            this.table23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.789D), Telerik.Reporting.Drawing.Unit.Cm(0.81D));
            this.table23.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table23.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table23.StyleName = "";
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.437D), Telerik.Reporting.Drawing.Unit.Cm(0.598D));
            this.textBox67.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // table24
            // 
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.565D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.408D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.812D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.161D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.557D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.539D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.776D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.712D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(2.035D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.791D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.791D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.452D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.818D)));
            this.table24.Body.SetCellContent(0, 0, this.textBox73);
            this.table24.Body.SetCellContent(1, 0, this.textBox79);
            this.table24.Body.SetCellContent(1, 1, this.textBox80);
            this.table24.Body.SetCellContent(0, 1, this.htmlTextBox2, 1, 4);
            this.table24.Body.SetCellContent(1, 2, this.checkBox7);
            this.table24.Body.SetCellContent(1, 3, this.checkBox8);
            this.table24.Body.SetCellContent(1, 4, this.checkBox9);
            this.table24.Body.SetCellContent(2, 0, this.textBox76);
            this.table24.Body.SetCellContent(2, 1, this.textBox81);
            this.table24.Body.SetCellContent(2, 2, this.checkBox10);
            this.table24.Body.SetCellContent(2, 4, this.checkBox11);
            this.table24.Body.SetCellContent(2, 3, this.checkBox12);
            this.table24.Body.SetCellContent(3, 0, this.textBox82);
            this.table24.Body.SetCellContent(3, 1, this.table25, 1, 4);
            this.table24.Body.SetCellContent(4, 0, this.textBox84);
            this.table24.Body.SetCellContent(4, 1, this.textBox88);
            this.table24.Body.SetCellContent(4, 2, this.checkBox14);
            this.table24.Body.SetCellContent(4, 3, this.checkBox15);
            this.table24.Body.SetCellContent(4, 4, this.checkBox16);
            this.table24.Body.SetCellContent(5, 0, this.textBox91);
            this.table24.Body.SetCellContent(5, 1, this.textBox92, 1, 2);
            this.table24.Body.SetCellContent(5, 3, this.checkBox17);
            this.table24.Body.SetCellContent(5, 4, this.checkBox18);
            this.table24.Body.SetCellContent(6, 0, this.textBox93);
            this.table24.Body.SetCellContent(6, 1, this.table27, 1, 4);
            this.table24.Body.SetCellContent(7, 0, this.textBox105);
            this.table24.Body.SetCellContent(7, 1, this.table30, 1, 4);
            tableGroup122.Name = "group";
            tableGroup123.Name = "tableGroup";
            tableGroup124.Name = "tableGroup1";
            tableGroup125.Name = "tableGroup2";
            tableGroup126.Name = "group4";
            this.table24.ColumnGroups.Add(tableGroup122);
            this.table24.ColumnGroups.Add(tableGroup123);
            this.table24.ColumnGroups.Add(tableGroup124);
            this.table24.ColumnGroups.Add(tableGroup125);
            this.table24.ColumnGroups.Add(tableGroup126);
            this.table24.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox73,
            this.htmlTextBox2,
            this.textBox79,
            this.textBox80,
            this.checkBox7,
            this.checkBox8,
            this.checkBox9,
            this.textBox76,
            this.textBox81,
            this.checkBox10,
            this.checkBox12,
            this.checkBox11,
            this.textBox82,
            this.table25,
            this.textBox84,
            this.textBox88,
            this.checkBox14,
            this.checkBox15,
            this.checkBox16,
            this.textBox91,
            this.textBox92,
            this.checkBox17,
            this.checkBox18,
            this.textBox93,
            this.table27,
            this.textBox105,
            this.table30});
            this.table24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(13.393D));
            this.table24.Name = "table24";
            tableGroup128.Name = "group2";
            tableGroup129.Name = "group3";
            tableGroup130.Name = "group21";
            tableGroup131.Name = "group22";
            tableGroup132.Name = "group23";
            tableGroup133.Name = "group27";
            tableGroup134.Name = "group28";
            tableGroup135.Name = "group29";
            tableGroup127.ChildGroups.Add(tableGroup128);
            tableGroup127.ChildGroups.Add(tableGroup129);
            tableGroup127.ChildGroups.Add(tableGroup130);
            tableGroup127.ChildGroups.Add(tableGroup131);
            tableGroup127.ChildGroups.Add(tableGroup132);
            tableGroup127.ChildGroups.Add(tableGroup133);
            tableGroup127.ChildGroups.Add(tableGroup134);
            tableGroup127.ChildGroups.Add(tableGroup135);
            tableGroup127.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup127.Name = "detailTableGroup";
            this.table24.RowGroups.Add(tableGroup127);
            this.table24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.503D), Telerik.Reporting.Drawing.Unit.Cm(7.914D));
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.565D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.textBox73.Style.Font.Bold = true;
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox73.StyleName = "";
            this.textBox73.Value = "4.";
            // 
            // textBox79
            // 
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.565D), Telerik.Reporting.Drawing.Unit.Cm(0.776D));
            this.textBox79.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox79.StyleName = "";
            // 
            // textBox80
            // 
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.408D), Telerik.Reporting.Drawing.Unit.Cm(0.776D));
            this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox80.StyleName = "";
            this.textBox80.Value = "Supply required ";
            // 
            // htmlTextBox2
            // 
            this.htmlTextBox2.Name = "htmlTextBox2";
            this.htmlTextBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.938D), Telerik.Reporting.Drawing.Unit.Cm(0.539D));
            this.htmlTextBox2.Style.Font.Bold = false;
            this.htmlTextBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox2.StyleName = "";
            this.htmlTextBox2.Value = "<strong>SUPPLY CHANGES REQUIRED</strong> (complete only for changes)";
            // 
            // checkBox7
            // 
            this.checkBox7.CheckedImage = ((object)(resources.GetObject("checkBox7.CheckedImage")));
            this.checkBox7.IndeterminateImage = ((object)(resources.GetObject("checkBox7.IndeterminateImage")));
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.812D), Telerik.Reporting.Drawing.Unit.Cm(0.776D));
            this.checkBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox7.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox7.StyleName = "";
            this.checkBox7.Text = "Overhead ";
            this.checkBox7.UncheckedImage = ((object)(resources.GetObject("checkBox7.UncheckedImage")));
            // 
            // checkBox8
            // 
            this.checkBox8.CheckedImage = ((object)(resources.GetObject("checkBox8.CheckedImage")));
            this.checkBox8.IndeterminateImage = ((object)(resources.GetObject("checkBox8.IndeterminateImage")));
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.161D), Telerik.Reporting.Drawing.Unit.Cm(0.776D));
            this.checkBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox8.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(10D);
            this.checkBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox8.StyleName = "";
            this.checkBox8.Text = "Underground";
            this.checkBox8.UncheckedImage = ((object)(resources.GetObject("checkBox8.UncheckedImage")));
            // 
            // checkBox9
            // 
            this.checkBox9.CheckedImage = ((object)(resources.GetObject("checkBox9.CheckedImage")));
            this.checkBox9.IndeterminateImage = ((object)(resources.GetObject("checkBox9.IndeterminateImage")));
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.557D), Telerik.Reporting.Drawing.Unit.Cm(0.776D));
            this.checkBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.checkBox9.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(50D);
            this.checkBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox9.StyleName = "";
            this.checkBox9.Text = "Overhead or underground conversion";
            this.checkBox9.UncheckedImage = ((object)(resources.GetObject("checkBox9.UncheckedImage")));
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.565D), Telerik.Reporting.Drawing.Unit.Cm(0.712D));
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox76.StyleName = "";
            // 
            // textBox81
            // 
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.408D), Telerik.Reporting.Drawing.Unit.Cm(0.712D));
            this.textBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox81.StyleName = "";
            this.textBox81.Value = "Supply phases";
            // 
            // checkBox10
            // 
            this.checkBox10.CheckedImage = ((object)(resources.GetObject("checkBox10.CheckedImage")));
            this.checkBox10.IndeterminateImage = ((object)(resources.GetObject("checkBox10.IndeterminateImage")));
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.812D), Telerik.Reporting.Drawing.Unit.Cm(0.712D));
            this.checkBox10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox10.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(45D);
            this.checkBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox10.StyleName = "";
            this.checkBox10.Text = "1";
            this.checkBox10.UncheckedImage = ((object)(resources.GetObject("checkBox10.UncheckedImage")));
            // 
            // checkBox11
            // 
            this.checkBox11.CheckedImage = ((object)(resources.GetObject("checkBox11.CheckedImage")));
            this.checkBox11.IndeterminateImage = ((object)(resources.GetObject("checkBox11.IndeterminateImage")));
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.557D), Telerik.Reporting.Drawing.Unit.Cm(0.712D));
            this.checkBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(150D);
            this.checkBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox11.StyleName = "";
            this.checkBox11.Text = "3";
            this.checkBox11.UncheckedImage = ((object)(resources.GetObject("checkBox11.UncheckedImage")));
            // 
            // checkBox12
            // 
            this.checkBox12.CheckedImage = ((object)(resources.GetObject("checkBox12.CheckedImage")));
            this.checkBox12.IndeterminateImage = ((object)(resources.GetObject("checkBox12.IndeterminateImage")));
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.161D), Telerik.Reporting.Drawing.Unit.Cm(0.712D));
            this.checkBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(50D);
            this.checkBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox12.StyleName = "";
            this.checkBox12.Text = "2";
            this.checkBox12.UncheckedImage = ((object)(resources.GetObject("checkBox12.UncheckedImage")));
            // 
            // textBox82
            // 
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.565D), Telerik.Reporting.Drawing.Unit.Cm(2.035D));
            this.textBox82.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox82.StyleName = "";
            // 
            // table25
            // 
            this.table25.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.94D)));
            this.table25.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.876D)));
            this.table25.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.795D)));
            this.table25.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.327D)));
            this.table25.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.778D)));
            this.table25.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.943D)));
            this.table25.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.314D)));
            this.table25.Body.SetCellContent(0, 0, this.textBox85);
            this.table25.Body.SetCellContent(0, 1, this.table26);
            this.table25.Body.SetCellContent(0, 2, this.textBox86, 1, 2);
            this.table25.Body.SetCellContent(1, 2, this.textBox96);
            this.table25.Body.SetCellContent(1, 3, this.textBox97);
            this.table25.Body.SetCellContent(1, 0, this.checkBox13);
            this.table25.Body.SetCellContent(1, 1, this.textBox94);
            this.table25.Body.SetCellContent(2, 0, this.textBox95);
            this.table25.Body.SetCellContent(2, 1, this.textBox98);
            this.table25.Body.SetCellContent(2, 2, this.textBox99);
            this.table25.Body.SetCellContent(2, 3, this.textBox100);
            tableGroup96.Name = "tableGroup6";
            tableGroup97.Name = "tableGroup7";
            tableGroup98.Name = "group6";
            tableGroup99.Name = "tableGroup8";
            this.table25.ColumnGroups.Add(tableGroup96);
            this.table25.ColumnGroups.Add(tableGroup97);
            this.table25.ColumnGroups.Add(tableGroup98);
            this.table25.ColumnGroups.Add(tableGroup99);
            this.table25.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox85,
            this.table26,
            this.textBox86,
            this.checkBox13,
            this.textBox94,
            this.textBox96,
            this.textBox97,
            this.textBox95,
            this.textBox98,
            this.textBox99,
            this.textBox100});
            this.table25.Name = "table25";
            tableGroup101.Name = "group24";
            tableGroup102.Name = "group25";
            tableGroup103.Name = "group26";
            tableGroup100.ChildGroups.Add(tableGroup101);
            tableGroup100.ChildGroups.Add(tableGroup102);
            tableGroup100.ChildGroups.Add(tableGroup103);
            tableGroup100.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup100.Name = "detailTableGroup3";
            this.table25.RowGroups.Add(tableGroup100);
            this.table25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.938D), Telerik.Reporting.Drawing.Unit.Cm(2.035D));
            this.table25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table25.StyleName = "";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.94D), Telerik.Reporting.Drawing.Unit.Cm(0.778D));
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox85.Value = "Maximum demand at the site";
            // 
            // table26
            // 
            this.table26.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.664D)));
            this.table26.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.566D)));
            this.table26.Body.SetCellContent(0, 0, this.textBox87);
            tableGroup94.Name = "tableGroup3";
            this.table26.ColumnGroups.Add(tableGroup94);
            this.table26.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox87});
            this.table26.Name = "table26";
            tableGroup95.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup95.Name = "detailTableGroup1";
            this.table26.RowGroups.Add(tableGroup95);
            this.table26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.876D), Telerik.Reporting.Drawing.Unit.Cm(0.778D));
            this.table26.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table26.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table26.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table26.StyleName = "";
            // 
            // textBox87
            // 
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.664D), Telerik.Reporting.Drawing.Unit.Cm(0.566D));
            this.textBox87.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.122D), Telerik.Reporting.Drawing.Unit.Cm(0.778D));
            this.textBox86.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox86.StyleName = "";
            this.textBox86.Value = "amps /phase";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.795D), Telerik.Reporting.Drawing.Unit.Cm(0.943D));
            this.textBox96.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox96.StyleName = "";
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.327D), Telerik.Reporting.Drawing.Unit.Cm(0.943D));
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox97.StyleName = "";
            // 
            // checkBox13
            // 
            this.checkBox13.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox13.CheckedImage = ((object)(resources.GetObject("checkBox13.CheckedImage")));
            this.checkBox13.IndeterminateImage = ((object)(resources.GetObject("checkBox13.IndeterminateImage")));
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.94D), Telerik.Reporting.Drawing.Unit.Cm(0.943D));
            this.checkBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.checkBox13.StyleName = "";
            this.checkBox13.Text = "Current Transformer (CT)        meeting";
            this.checkBox13.UncheckedImage = ((object)(resources.GetObject("checkBox13.UncheckedImage")));
            // 
            // textBox94
            // 
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.876D), Telerik.Reporting.Drawing.Unit.Cm(0.943D));
            this.textBox94.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox94.StyleName = "";
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.94D), Telerik.Reporting.Drawing.Unit.Cm(0.314D));
            this.textBox95.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox95.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox95.StyleName = "";
            this.textBox95.Value = "(select if already there or required)";
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.876D), Telerik.Reporting.Drawing.Unit.Cm(0.314D));
            this.textBox98.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox98.StyleName = "";
            // 
            // textBox99
            // 
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.795D), Telerik.Reporting.Drawing.Unit.Cm(0.314D));
            this.textBox99.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox99.StyleName = "";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.327D), Telerik.Reporting.Drawing.Unit.Cm(0.314D));
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox100.StyleName = "";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.565D), Telerik.Reporting.Drawing.Unit.Cm(0.791D));
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.StyleName = "";
            // 
            // textBox88
            // 
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.408D), Telerik.Reporting.Drawing.Unit.Cm(0.791D));
            this.textBox88.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox88.StyleName = "";
            this.textBox88.Value = "Metering phases required";
            // 
            // checkBox14
            // 
            this.checkBox14.CheckedImage = ((object)(resources.GetObject("checkBox14.CheckedImage")));
            this.checkBox14.IndeterminateImage = ((object)(resources.GetObject("checkBox14.IndeterminateImage")));
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.812D), Telerik.Reporting.Drawing.Unit.Cm(0.791D));
            this.checkBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(45D);
            this.checkBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox14.StyleName = "";
            this.checkBox14.Text = "1";
            this.checkBox14.UncheckedImage = ((object)(resources.GetObject("checkBox14.UncheckedImage")));
            // 
            // checkBox15
            // 
            this.checkBox15.CheckedImage = ((object)(resources.GetObject("checkBox15.CheckedImage")));
            this.checkBox15.IndeterminateImage = ((object)(resources.GetObject("checkBox15.IndeterminateImage")));
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.161D), Telerik.Reporting.Drawing.Unit.Cm(0.791D));
            this.checkBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(50D);
            this.checkBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox15.StyleName = "";
            this.checkBox15.Text = "2";
            this.checkBox15.UncheckedImage = ((object)(resources.GetObject("checkBox15.UncheckedImage")));
            // 
            // checkBox16
            // 
            this.checkBox16.CheckedImage = ((object)(resources.GetObject("checkBox16.CheckedImage")));
            this.checkBox16.IndeterminateImage = ((object)(resources.GetObject("checkBox16.IndeterminateImage")));
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.557D), Telerik.Reporting.Drawing.Unit.Cm(0.791D));
            this.checkBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox16.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(150D);
            this.checkBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox16.StyleName = "";
            this.checkBox16.Text = "3";
            this.checkBox16.UncheckedImage = ((object)(resources.GetObject("checkBox16.UncheckedImage")));
            // 
            // textBox91
            // 
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.565D), Telerik.Reporting.Drawing.Unit.Cm(0.791D));
            this.textBox91.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox91.StyleName = "";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.22D), Telerik.Reporting.Drawing.Unit.Cm(0.791D));
            this.textBox92.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox92.StyleName = "";
            this.textBox92.Value = "Off-peak installations required";
            // 
            // checkBox17
            // 
            this.checkBox17.CheckedImage = ((object)(resources.GetObject("checkBox17.CheckedImage")));
            this.checkBox17.IndeterminateImage = ((object)(resources.GetObject("checkBox17.IndeterminateImage")));
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.161D), Telerik.Reporting.Drawing.Unit.Cm(0.791D));
            this.checkBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox17.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(20D);
            this.checkBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox17.StyleName = "";
            this.checkBox17.Text = "Hot water";
            this.checkBox17.UncheckedImage = ((object)(resources.GetObject("checkBox17.UncheckedImage")));
            // 
            // checkBox18
            // 
            this.checkBox18.CheckedImage = ((object)(resources.GetObject("checkBox18.CheckedImage")));
            this.checkBox18.IndeterminateImage = ((object)(resources.GetObject("checkBox18.IndeterminateImage")));
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.557D), Telerik.Reporting.Drawing.Unit.Cm(0.791D));
            this.checkBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox18.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(100D);
            this.checkBox18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox18.StyleName = "";
            this.checkBox18.Text = "Floor heating ";
            this.checkBox18.UncheckedImage = ((object)(resources.GetObject("checkBox18.UncheckedImage")));
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.565D), Telerik.Reporting.Drawing.Unit.Cm(1.452D));
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox93.StyleName = "";
            // 
            // table27
            // 
            this.table27.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.374D)));
            this.table27.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(14.564D)));
            this.table27.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.701D)));
            this.table27.Body.SetCellContent(0, 0, this.textBox103);
            this.table27.Body.SetCellContent(0, 1, this.table29);
            tableGroup106.Name = "tableGroup9";
            tableGroup106.ReportItem = this.textBox102;
            tableGroup107.Name = "tableGroup10";
            tableGroup107.ReportItem = this.table28;
            this.table27.ColumnGroups.Add(tableGroup106);
            this.table27.ColumnGroups.Add(tableGroup107);
            this.table27.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox103,
            this.table29,
            this.textBox102,
            this.table28});
            this.table27.Name = "table27";
            tableGroup108.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup108.Name = "detailTableGroup4";
            this.table27.RowGroups.Add(tableGroup108);
            this.table27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.938D), Telerik.Reporting.Drawing.Unit.Cm(1.452D));
            this.table27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table27.StyleName = "";
            // 
            // textBox103
            // 
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.374D), Telerik.Reporting.Drawing.Unit.Cm(0.701D));
            // 
            // table29
            // 
            this.table29.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(14.352D)));
            this.table29.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.489D)));
            this.table29.Body.SetCellContent(0, 0, this.textBox104);
            tableGroup104.Name = "tableGroup3";
            this.table29.ColumnGroups.Add(tableGroup104);
            this.table29.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox104});
            this.table29.Name = "table29";
            tableGroup105.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup105.Name = "detailTableGroup1";
            this.table29.RowGroups.Add(tableGroup105);
            this.table29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.564D), Telerik.Reporting.Drawing.Unit.Cm(0.701D));
            this.table29.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table29.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table29.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table29.StyleName = "";
            // 
            // textBox104
            // 
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(14.352D), Telerik.Reporting.Drawing.Unit.Cm(0.49D));
            this.textBox104.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // textBox105
            // 
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.565D), Telerik.Reporting.Drawing.Unit.Cm(0.818D));
            this.textBox105.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox105.StyleName = "";
            // 
            // table30
            // 
            this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.576D)));
            this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.314D)));
            this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.161D)));
            this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.341D)));
            this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.28D)));
            this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.601D)));
            this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.74D)));
            this.table30.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.925D)));
            this.table30.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.818D)));
            this.table30.Body.SetCellContent(0, 0, this.textBox108);
            this.table30.Body.SetCellContent(0, 3, this.textBox110);
            this.table30.Body.SetCellContent(0, 5, this.textBox111);
            this.table30.Body.SetCellContent(0, 6, this.textBox113);
            this.table30.Body.SetCellContent(0, 1, this.checkBox19);
            this.table30.Body.SetCellContent(0, 2, this.checkBox20);
            this.table30.Body.SetCellContent(0, 4, this.table31);
            this.table30.Body.SetCellContent(0, 7, this.table32);
            tableGroup113.Name = "tableGroup11";
            tableGroup114.Name = "group31";
            tableGroup115.Name = "group30";
            tableGroup116.Name = "tableGroup12";
            tableGroup117.Name = "tableGroup13";
            tableGroup118.Name = "group32";
            tableGroup119.Name = "group33";
            tableGroup120.Name = "group34";
            this.table30.ColumnGroups.Add(tableGroup113);
            this.table30.ColumnGroups.Add(tableGroup114);
            this.table30.ColumnGroups.Add(tableGroup115);
            this.table30.ColumnGroups.Add(tableGroup116);
            this.table30.ColumnGroups.Add(tableGroup117);
            this.table30.ColumnGroups.Add(tableGroup118);
            this.table30.ColumnGroups.Add(tableGroup119);
            this.table30.ColumnGroups.Add(tableGroup120);
            this.table30.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox108,
            this.checkBox19,
            this.checkBox20,
            this.textBox110,
            this.table31,
            this.textBox111,
            this.textBox113,
            this.table32});
            this.table30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.535D), Telerik.Reporting.Drawing.Unit.Cm(30.295D));
            this.table30.Name = "table30";
            tableGroup121.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup121.Name = "detailTableGroup5";
            this.table30.RowGroups.Add(tableGroup121);
            this.table30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.938D), Telerik.Reporting.Drawing.Unit.Cm(0.818D));
            this.table30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table30.StyleName = "";
            // 
            // textBox108
            // 
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.576D), Telerik.Reporting.Drawing.Unit.Cm(0.818D));
            this.textBox108.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox108.Value = "Solar required? ";
            // 
            // textBox110
            // 
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.341D), Telerik.Reporting.Drawing.Unit.Cm(0.818D));
            this.textBox110.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox110.Value = "Size of solar system ";
            // 
            // textBox111
            // 
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.601D), Telerik.Reporting.Drawing.Unit.Cm(0.818D));
            this.textBox111.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox111.StyleName = "";
            this.textBox111.Value = "kw";
            // 
            // textBox113
            // 
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.74D), Telerik.Reporting.Drawing.Unit.Cm(0.818D));
            this.textBox113.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox113.StyleName = "";
            this.textBox113.Value = "SEG approval number";
            // 
            // checkBox19
            // 
            this.checkBox19.CheckedImage = ((object)(resources.GetObject("checkBox19.CheckedImage")));
            this.checkBox19.IndeterminateImage = ((object)(resources.GetObject("checkBox19.IndeterminateImage")));
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.314D), Telerik.Reporting.Drawing.Unit.Cm(0.818D));
            this.checkBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.checkBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox19.StyleName = "";
            this.checkBox19.Text = "Yes";
            this.checkBox19.UncheckedImage = ((object)(resources.GetObject("checkBox19.UncheckedImage")));
            // 
            // checkBox20
            // 
            this.checkBox20.CheckedImage = ((object)(resources.GetObject("checkBox20.CheckedImage")));
            this.checkBox20.IndeterminateImage = ((object)(resources.GetObject("checkBox20.IndeterminateImage")));
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.161D), Telerik.Reporting.Drawing.Unit.Cm(0.818D));
            this.checkBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.checkBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox20.StyleName = "";
            this.checkBox20.Text = "No";
            this.checkBox20.UncheckedImage = ((object)(resources.GetObject("checkBox20.UncheckedImage")));
            // 
            // table31
            // 
            this.table31.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.068D)));
            this.table31.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.606D)));
            this.table31.Body.SetCellContent(0, 0, this.textBox107);
            tableGroup109.Name = "tableGroup3";
            this.table31.ColumnGroups.Add(tableGroup109);
            this.table31.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox107});
            this.table31.Name = "table31";
            tableGroup110.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup110.Name = "detailTableGroup1";
            this.table31.RowGroups.Add(tableGroup110);
            this.table31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.28D), Telerik.Reporting.Drawing.Unit.Cm(0.818D));
            this.table31.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table31.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table31.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table31.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table31.StyleName = "";
            // 
            // textBox107
            // 
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.068D), Telerik.Reporting.Drawing.Unit.Cm(0.606D));
            this.textBox107.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox107.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // table32
            // 
            this.table32.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.713D)));
            this.table32.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.606D)));
            this.table32.Body.SetCellContent(0, 0, this.textBox112);
            tableGroup111.Name = "tableGroup3";
            this.table32.ColumnGroups.Add(tableGroup111);
            this.table32.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox112});
            this.table32.Name = "table32";
            tableGroup112.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup112.Name = "detailTableGroup1";
            this.table32.RowGroups.Add(tableGroup112);
            this.table32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.925D), Telerik.Reporting.Drawing.Unit.Cm(0.818D));
            this.table32.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table32.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table32.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table32.StyleName = "";
            // 
            // textBox112
            // 
            this.textBox112.Name = "textBox112";
            this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.714D), Telerik.Reporting.Drawing.Unit.Cm(0.606D));
            this.textBox112.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox112.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox112.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox112.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox112.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            // 
            // panel2
            // 
            this.panel2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table33,
            this.table39,
            this.table43,
            this.textBox164});
            this.panel2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(27.914D));
            this.panel2.Name = "panel2";
            this.panel2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.019D), Telerik.Reporting.Drawing.Unit.Cm(27.32D));
            this.panel2.Style.BackgroundColor = System.Drawing.SystemColors.MenuBar;
            this.panel2.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0.2D);
            this.panel2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0.5D);
            this.panel2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0.5D);
            this.panel2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0.5D);
            // 
            // table33
            // 
            this.table33.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.563D)));
            this.table33.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.99D)));
            this.table33.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.073D)));
            this.table33.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.945D)));
            this.table33.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(2.515D)));
            this.table33.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.39D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.645D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.908D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.908D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.9D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.765D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.426D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.553D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.976D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.5D)));
            this.table33.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.976D)));
            this.table33.Body.SetCellContent(0, 0, this.textBox106);
            this.table33.Body.SetCellContent(0, 4, this.textBox109);
            this.table33.Body.SetCellContent(1, 0, this.textBox114);
            this.table33.Body.SetCellContent(1, 1, this.textBox115);
            this.table33.Body.SetCellContent(1, 4, this.textBox116);
            this.table33.Body.SetCellContent(0, 5, this.textBox117);
            this.table33.Body.SetCellContent(1, 5, this.table35);
            this.table33.Body.SetCellContent(0, 1, this.textBox120, 1, 2);
            this.table33.Body.SetCellContent(2, 1, this.textBox122);
            this.table33.Body.SetCellContent(2, 4, this.textBox124);
            this.table33.Body.SetCellContent(2, 0, this.panel3);
            this.table33.Body.SetCellContent(2, 5, this.table37);
            this.table33.Body.SetCellContent(3, 0, this.textBox125);
            this.table33.Body.SetCellContent(0, 3, this.textBox130);
            this.table33.Body.SetCellContent(1, 2, this.table34, 1, 2);
            this.table33.Body.SetCellContent(2, 2, this.table36, 1, 2);
            this.table33.Body.SetCellContent(3, 1, this.textBox126, 1, 2);
            this.table33.Body.SetCellContent(3, 3, this.table38, 1, 3);
            this.table33.Body.SetCellContent(4, 0, this.textBox127);
            this.table33.Body.SetCellContent(4, 1, this.textBox128);
            this.table33.Body.SetCellContent(4, 2, this.textBox129);
            this.table33.Body.SetCellContent(4, 3, this.textBox132);
            this.table33.Body.SetCellContent(4, 4, this.textBox133);
            this.table33.Body.SetCellContent(4, 5, this.textBox134);
            this.table33.Body.SetCellContent(5, 0, this.textBox135);
            this.table33.Body.SetCellContent(5, 3, this.textBox138);
            this.table33.Body.SetCellContent(5, 4, this.textBox139);
            this.table33.Body.SetCellContent(5, 5, this.textBox140);
            this.table33.Body.SetCellContent(5, 1, this.textBox136, 1, 2);
            this.table33.Body.SetCellContent(6, 0, this.textBox137);
            this.table33.Body.SetCellContent(6, 1, this.textBox141);
            this.table33.Body.SetCellContent(6, 2, this.textBox142);
            this.table33.Body.SetCellContent(6, 3, this.textBox143);
            this.table33.Body.SetCellContent(6, 4, this.textBox144);
            this.table33.Body.SetCellContent(6, 5, this.textBox145);
            this.table33.Body.SetCellContent(7, 0, this.textBox146);
            this.table33.Body.SetCellContent(8, 0, this.textBox152);
            this.table33.Body.SetCellContent(8, 3, this.textBox155);
            this.table33.Body.SetCellContent(8, 4, this.textBox156);
            this.table33.Body.SetCellContent(8, 5, this.textBox157);
            this.table33.Body.SetCellContent(7, 1, this.textBox147, 1, 5);
            this.table33.Body.SetCellContent(8, 1, this.textBox153, 1, 2);
            this.table33.Body.SetCellContent(9, 0, this.textBox148);
            this.table33.Body.SetCellContent(9, 1, this.textBox149, 1, 5);
            this.table33.Body.SetCellContent(10, 0, this.textBox150);
            this.table33.Body.SetCellContent(10, 4, this.textBox159);
            this.table33.Body.SetCellContent(10, 5, this.textBox160);
            this.table33.Body.SetCellContent(11, 0, this.textBox161);
            this.table33.Body.SetCellContent(10, 1, this.textBox151, 1, 3);
            this.table33.Body.SetCellContent(11, 1, this.htmlTextBox3, 1, 5);
            tableGroup146.Name = "group";
            tableGroup147.Name = "tableGroup";
            tableGroup148.Name = "tableGroup1";
            tableGroup149.Name = "group37";
            tableGroup150.Name = "tableGroup2";
            tableGroup151.Name = "group4";
            this.table33.ColumnGroups.Add(tableGroup146);
            this.table33.ColumnGroups.Add(tableGroup147);
            this.table33.ColumnGroups.Add(tableGroup148);
            this.table33.ColumnGroups.Add(tableGroup149);
            this.table33.ColumnGroups.Add(tableGroup150);
            this.table33.ColumnGroups.Add(tableGroup151);
            this.table33.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox106,
            this.textBox120,
            this.textBox130,
            this.textBox109,
            this.textBox117,
            this.textBox114,
            this.textBox115,
            this.table34,
            this.textBox116,
            this.table35,
            this.panel3,
            this.textBox122,
            this.table36,
            this.textBox124,
            this.table37,
            this.textBox125,
            this.textBox126,
            this.table38,
            this.textBox127,
            this.textBox128,
            this.textBox129,
            this.textBox132,
            this.textBox133,
            this.textBox134,
            this.textBox135,
            this.textBox136,
            this.textBox138,
            this.textBox139,
            this.textBox140,
            this.textBox137,
            this.textBox141,
            this.textBox142,
            this.textBox143,
            this.textBox144,
            this.textBox145,
            this.textBox146,
            this.textBox147,
            this.textBox152,
            this.textBox153,
            this.textBox155,
            this.textBox156,
            this.textBox157,
            this.textBox148,
            this.textBox149,
            this.textBox150,
            this.textBox151,
            this.textBox159,
            this.textBox160,
            this.textBox161,
            this.htmlTextBox3});
            this.table33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.501D), Telerik.Reporting.Drawing.Unit.Cm(0.288D));
            this.table33.Name = "table33";
            tableGroup153.Name = "group2";
            tableGroup154.Name = "group3";
            tableGroup155.Name = "group35";
            tableGroup156.Name = "group36";
            tableGroup157.Name = "group38";
            tableGroup158.Name = "group39";
            tableGroup159.Name = "group40";
            tableGroup152.ChildGroups.Add(tableGroup153);
            tableGroup152.ChildGroups.Add(tableGroup154);
            tableGroup152.ChildGroups.Add(tableGroup155);
            tableGroup152.ChildGroups.Add(tableGroup156);
            tableGroup152.ChildGroups.Add(tableGroup157);
            tableGroup152.ChildGroups.Add(tableGroup158);
            tableGroup152.ChildGroups.Add(tableGroup159);
            tableGroup152.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup152.Name = "detailTableGroup";
            tableGroup161.Name = "group42";
            tableGroup160.ChildGroups.Add(tableGroup161);
            tableGroup160.Name = "group41";
            tableGroup163.Name = "group44";
            tableGroup162.ChildGroups.Add(tableGroup163);
            tableGroup162.Name = "group43";
            tableGroup165.Name = "group46";
            tableGroup164.ChildGroups.Add(tableGroup165);
            tableGroup164.Name = "group45";
            tableGroup167.Name = "group48";
            tableGroup166.ChildGroups.Add(tableGroup167);
            tableGroup166.Name = "group47";
            tableGroup169.Name = "group50";
            tableGroup168.ChildGroups.Add(tableGroup169);
            tableGroup168.Name = "group49";
            this.table33.RowGroups.Add(tableGroup152);
            this.table33.RowGroups.Add(tableGroup160);
            this.table33.RowGroups.Add(tableGroup162);
            this.table33.RowGroups.Add(tableGroup164);
            this.table33.RowGroups.Add(tableGroup166);
            this.table33.RowGroups.Add(tableGroup168);
            this.table33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.476D), Telerik.Reporting.Drawing.Unit.Cm(9.557D));
            // 
            // textBox106
            // 
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.645D));
            this.textBox106.Style.Font.Bold = true;
            this.textBox106.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox106.StyleName = "";
            this.textBox106.Value = "5.";
            // 
            // textBox109
            // 
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.515D), Telerik.Reporting.Drawing.Unit.Cm(0.645D));
            this.textBox109.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox109.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(30D);
            this.textBox109.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox109.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox109.StyleName = "";
            // 
            // textBox114
            // 
            this.textBox114.Name = "textBox114";
            this.textBox114.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.908D));
            this.textBox114.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox114.StyleName = "";
            // 
            // textBox115
            // 
            this.textBox115.Name = "textBox115";
            this.textBox115.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.99D), Telerik.Reporting.Drawing.Unit.Cm(0.908D));
            this.textBox115.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox115.StyleName = "";
            this.textBox115.Value = "Full name";
            // 
            // textBox116
            // 
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.515D), Telerik.Reporting.Drawing.Unit.Cm(0.908D));
            this.textBox116.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox116.StyleName = "";
            this.textBox116.Value = "Daytime no.";
            // 
            // textBox117
            // 
            this.textBox117.Name = "textBox117";
            this.textBox117.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.39D), Telerik.Reporting.Drawing.Unit.Cm(0.645D));
            this.textBox117.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
            this.textBox117.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox117.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox117.StyleName = "";
            // 
            // table35
            // 
            this.table35.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.178D)));
            this.table35.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.696D)));
            this.table35.Body.SetCellContent(0, 0, this.textBox119);
            tableGroup136.Name = "tableGroup3";
            this.table35.ColumnGroups.Add(tableGroup136);
            this.table35.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox119});
            this.table35.Name = "table35";
            tableGroup137.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup137.Name = "detailTableGroup1";
            this.table35.RowGroups.Add(tableGroup137);
            this.table35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.39D), Telerik.Reporting.Drawing.Unit.Cm(0.908D));
            this.table35.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table35.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table35.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table35.StyleName = "";
            // 
            // textBox119
            // 
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.178D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.textBox119.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox119.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox119.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox119.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox119.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            // 
            // textBox120
            // 
            this.textBox120.Name = "textBox120";
            this.textBox120.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.063D), Telerik.Reporting.Drawing.Unit.Cm(0.645D));
            this.textBox120.Style.Font.Bold = true;
            this.textBox120.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox120.StyleName = "";
            this.textBox120.Value = "ELECTRICIANíS DETAILS";
            // 
            // textBox122
            // 
            this.textBox122.Name = "textBox122";
            this.textBox122.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.99D), Telerik.Reporting.Drawing.Unit.Cm(0.908D));
            this.textBox122.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox122.StyleName = "";
            this.textBox122.Value = "Email";
            // 
            // textBox124
            // 
            this.textBox124.Name = "textBox124";
            this.textBox124.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.515D), Telerik.Reporting.Drawing.Unit.Cm(0.908D));
            this.textBox124.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox124.StyleName = "";
            this.textBox124.Value = "Licence No.";
            // 
            // panel3
            // 
            this.panel3.Name = "panel3";
            this.panel3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.908D));
            // 
            // table37
            // 
            this.table37.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(3.178D)));
            this.table37.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.696D)));
            this.table37.Body.SetCellContent(0, 0, this.textBox123);
            tableGroup138.Name = "tableGroup3";
            this.table37.ColumnGroups.Add(tableGroup138);
            this.table37.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox123});
            this.table37.Name = "table37";
            tableGroup139.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup139.Name = "detailTableGroup1";
            this.table37.RowGroups.Add(tableGroup139);
            this.table37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.39D), Telerik.Reporting.Drawing.Unit.Cm(0.908D));
            this.table37.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table37.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table37.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table37.StyleName = "";
            // 
            // textBox123
            // 
            this.textBox123.Name = "textBox123";
            this.textBox123.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.178D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.textBox123.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox123.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
            // 
            // textBox125
            // 
            this.textBox125.Name = "textBox125";
            this.textBox125.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox125.StyleName = "";
            // 
            // textBox130
            // 
            this.textBox130.Name = "textBox130";
            this.textBox130.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.945D), Telerik.Reporting.Drawing.Unit.Cm(0.645D));
            this.textBox130.Style.Font.Bold = true;
            this.textBox130.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox130.StyleName = "";
            // 
            // table34
            // 
            this.table34.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.806D)));
            this.table34.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.696D)));
            this.table34.Body.SetCellContent(0, 0, this.textBox131);
            tableGroup140.Name = "tableGroup3";
            this.table34.ColumnGroups.Add(tableGroup140);
            this.table34.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox131});
            this.table34.Name = "table34";
            tableGroup141.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup141.Name = "detailTableGroup1";
            this.table34.RowGroups.Add(tableGroup141);
            this.table34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.018D), Telerik.Reporting.Drawing.Unit.Cm(0.908D));
            this.table34.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table34.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table34.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table34.StyleName = "";
            // 
            // textBox131
            // 
            this.textBox131.Name = "textBox131";
            this.textBox131.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.807D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.textBox131.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // table36
            // 
            this.table36.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.806D)));
            this.table36.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.696D)));
            this.table36.Body.SetCellContent(0, 0, this.textBox121);
            tableGroup142.Name = "tableGroup3";
            this.table36.ColumnGroups.Add(tableGroup142);
            this.table36.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox121});
            this.table36.Name = "table36";
            tableGroup143.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup143.Name = "detailTableGroup1";
            this.table36.RowGroups.Add(tableGroup143);
            this.table36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.018D), Telerik.Reporting.Drawing.Unit.Cm(0.908D));
            this.table36.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table36.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table36.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table36.StyleName = "";
            // 
            // textBox121
            // 
            this.textBox121.Name = "textBox121";
            this.textBox121.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.807D), Telerik.Reporting.Drawing.Unit.Cm(0.697D));
            this.textBox121.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            // 
            // textBox126
            // 
            this.textBox126.Name = "textBox126";
            this.textBox126.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.063D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.textBox126.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.5D);
            this.textBox126.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox126.StyleName = "";
            this.textBox126.Value = "Permission to connect ref. no. (from the distributor)";
            // 
            // table38
            // 
            this.table38.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(8.638D)));
            this.table38.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.688D)));
            this.table38.Body.SetCellContent(0, 0, this.textBox118);
            tableGroup144.Name = "tableGroup3";
            this.table38.ColumnGroups.Add(tableGroup144);
            this.table38.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox118});
            this.table38.Name = "table38";
            tableGroup145.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup145.Name = "detailTableGroup1";
            this.table38.RowGroups.Add(tableGroup145);
            this.table38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.85D), Telerik.Reporting.Drawing.Unit.Cm(0.9D));
            this.table38.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table38.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table38.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.table38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table38.StyleName = "";
            // 
            // textBox118
            // 
            this.textBox118.Name = "textBox118";
            this.textBox118.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.639D), Telerik.Reporting.Drawing.Unit.Cm(0.688D));
            this.textBox118.Style.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(221)))), ((int)(((byte)(239)))));
            this.textBox118.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox118.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox118.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox118.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            // 
            // textBox127
            // 
            this.textBox127.Name = "textBox127";
            this.textBox127.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox127.StyleName = "";
            // 
            // textBox128
            // 
            this.textBox128.Name = "textBox128";
            this.textBox128.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.99D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox128.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.5D);
            this.textBox128.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox128.StyleName = "";
            // 
            // textBox129
            // 
            this.textBox129.Name = "textBox129";
            this.textBox129.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.073D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox129.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.5D);
            this.textBox129.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox129.StyleName = "";
            // 
            // textBox132
            // 
            this.textBox132.Name = "textBox132";
            this.textBox132.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.945D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox132.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox132.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox132.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox132.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox132.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox132.StyleName = "";
            // 
            // textBox133
            // 
            this.textBox133.Name = "textBox133";
            this.textBox133.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.515D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox133.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox133.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox133.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox133.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox133.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox133.StyleName = "";
            // 
            // textBox134
            // 
            this.textBox134.Name = "textBox134";
            this.textBox134.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.39D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox134.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox134.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox134.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox134.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox134.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox134.StyleName = "";
            // 
            // textBox135
            // 
            this.textBox135.Name = "textBox135";
            this.textBox135.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.765D));
            this.textBox135.StyleName = "";
            // 
            // textBox138
            // 
            this.textBox138.Name = "textBox138";
            this.textBox138.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.945D), Telerik.Reporting.Drawing.Unit.Cm(0.765D));
            this.textBox138.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox138.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox138.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox138.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox138.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox138.StyleName = "";
            // 
            // textBox139
            // 
            this.textBox139.Name = "textBox139";
            this.textBox139.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.515D), Telerik.Reporting.Drawing.Unit.Cm(0.765D));
            this.textBox139.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox139.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox139.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox139.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox139.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox139.StyleName = "";
            // 
            // textBox140
            // 
            this.textBox140.Name = "textBox140";
            this.textBox140.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.39D), Telerik.Reporting.Drawing.Unit.Cm(0.765D));
            this.textBox140.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox140.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox140.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox140.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox140.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox140.StyleName = "";
            // 
            // textBox136
            // 
            this.textBox136.Name = "textBox136";
            this.textBox136.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.063D), Telerik.Reporting.Drawing.Unit.Cm(0.765D));
            this.textBox136.Style.Font.Bold = true;
            this.textBox136.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.5D);
            this.textBox136.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox136.StyleName = "";
            this.textBox136.Value = "FEES, PROCESS AND TIMINGS";
            // 
            // textBox137
            // 
            this.textBox137.Name = "textBox137";
            this.textBox137.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox137.StyleName = "";
            // 
            // textBox141
            // 
            this.textBox141.Name = "textBox141";
            this.textBox141.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.99D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox141.Style.Font.Bold = true;
            this.textBox141.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.5D);
            this.textBox141.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox141.StyleName = "";
            this.textBox141.Value = "Fees";
            // 
            // textBox142
            // 
            this.textBox142.Name = "textBox142";
            this.textBox142.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.073D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox142.Style.Font.Bold = true;
            this.textBox142.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.5D);
            this.textBox142.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox142.StyleName = "";
            // 
            // textBox143
            // 
            this.textBox143.Name = "textBox143";
            this.textBox143.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.945D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox143.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox143.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox143.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox143.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox143.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox143.StyleName = "";
            // 
            // textBox144
            // 
            this.textBox144.Name = "textBox144";
            this.textBox144.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.515D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox144.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox144.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox144.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox144.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox144.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox144.StyleName = "";
            // 
            // textBox145
            // 
            this.textBox145.Name = "textBox145";
            this.textBox145.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.39D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox145.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox145.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox145.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox145.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox145.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox145.StyleName = "";
            // 
            // textBox146
            // 
            this.textBox146.Name = "textBox146";
            this.textBox146.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(1.426D));
            this.textBox146.StyleName = "";
            // 
            // textBox152
            // 
            this.textBox152.Name = "textBox152";
            this.textBox152.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.553D));
            this.textBox152.StyleName = "";
            // 
            // textBox155
            // 
            this.textBox155.Name = "textBox155";
            this.textBox155.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.945D), Telerik.Reporting.Drawing.Unit.Cm(0.553D));
            this.textBox155.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox155.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox155.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox155.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox155.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox155.StyleName = "";
            // 
            // textBox156
            // 
            this.textBox156.Name = "textBox156";
            this.textBox156.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.515D), Telerik.Reporting.Drawing.Unit.Cm(0.553D));
            this.textBox156.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox156.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox156.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox156.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox156.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox156.StyleName = "";
            // 
            // textBox157
            // 
            this.textBox157.Name = "textBox157";
            this.textBox157.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.39D), Telerik.Reporting.Drawing.Unit.Cm(0.553D));
            this.textBox157.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox157.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox157.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox157.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(3D);
            this.textBox157.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox157.StyleName = "";
            // 
            // textBox147
            // 
            this.textBox147.Name = "textBox147";
            this.textBox147.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.913D), Telerik.Reporting.Drawing.Unit.Cm(1.426D));
            this.textBox147.Style.Font.Bold = false;
            this.textBox147.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox147.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox147.StyleName = "";
            this.textBox147.Value = resources.GetString("textBox147.Value");
            // 
            // textBox153
            // 
            this.textBox153.Name = "textBox153";
            this.textBox153.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.063D), Telerik.Reporting.Drawing.Unit.Cm(0.553D));
            this.textBox153.Style.Font.Bold = true;
            this.textBox153.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.5D);
            this.textBox153.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox153.StyleName = "";
            this.textBox153.Value = "Access to the property and meter";
            // 
            // textBox148
            // 
            this.textBox148.Name = "textBox148";
            this.textBox148.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.976D));
            this.textBox148.StyleName = "";
            // 
            // textBox149
            // 
            this.textBox149.Name = "textBox149";
            this.textBox149.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.913D), Telerik.Reporting.Drawing.Unit.Cm(0.976D));
            this.textBox149.Style.Font.Bold = false;
            this.textBox149.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox149.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox149.StyleName = "";
            this.textBox149.Value = resources.GetString("textBox149.Value");
            // 
            // textBox150
            // 
            this.textBox150.Name = "textBox150";
            this.textBox150.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox150.StyleName = "";
            // 
            // textBox159
            // 
            this.textBox159.Name = "textBox159";
            this.textBox159.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.515D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox159.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox159.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox159.StyleName = "";
            // 
            // textBox160
            // 
            this.textBox160.Name = "textBox160";
            this.textBox160.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.39D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox160.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox160.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox160.StyleName = "";
            // 
            // textBox161
            // 
            this.textBox161.Name = "textBox161";
            this.textBox161.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.563D), Telerik.Reporting.Drawing.Unit.Cm(0.976D));
            this.textBox161.StyleName = "";
            // 
            // textBox151
            // 
            this.textBox151.Name = "textBox151";
            this.textBox151.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.009D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
            this.textBox151.Style.Font.Bold = true;
            this.textBox151.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9.5D);
            this.textBox151.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox151.StyleName = "";
            this.textBox151.Value = "Process and timings";
            // 
            // htmlTextBox3
            // 
            this.htmlTextBox3.Name = "htmlTextBox3";
            this.htmlTextBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(16.913D), Telerik.Reporting.Drawing.Unit.Cm(0.976D));
            this.htmlTextBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox3.StyleName = "";
            this.htmlTextBox3.Value = resources.GetString("htmlTextBox3.Value");
            // 
            // table39
            // 
            this.table39.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.825D)));
            this.table39.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.825D)));
            this.table39.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(5.825D)));
            this.table39.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.032D)));
            this.table39.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(1.003D)));
            this.table39.Body.SetCellContent(0, 0, this.textBox158);
            this.table39.Body.SetCellContent(0, 1, this.textBox163);
            this.table39.Body.SetCellContent(0, 2, this.textBox165);
            this.table39.Body.SetCellContent(1, 0, this.htmlTextBox4, 1, 3);
            tableGroup170.Name = "tableGroup14";
            tableGroup170.ReportItem = this.table40;
            tableGroup171.Name = "tableGroup15";
            tableGroup171.ReportItem = this.table41;
            tableGroup172.Name = "tableGroup16";
            tableGroup172.ReportItem = this.table42;
            this.table39.ColumnGroups.Add(tableGroup170);
            this.table39.ColumnGroups.Add(tableGroup171);
            this.table39.ColumnGroups.Add(tableGroup172);
            this.table39.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox158,
            this.textBox163,
            this.textBox165,
            this.htmlTextBox4,
            this.table40,
            this.table41,
            this.table42});
            this.table39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(10.321D));
            this.table39.Name = "table39";
            tableGroup173.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup173.Name = "detailTableGroup6";
            tableGroup174.Name = "group51";
            this.table39.RowGroups.Add(tableGroup173);
            this.table39.RowGroups.Add(tableGroup174);
            this.table39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.475D), Telerik.Reporting.Drawing.Unit.Cm(2.944D));
            // 
            // textBox158
            // 
            this.textBox158.Name = "textBox158";
            this.textBox158.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.825D), Telerik.Reporting.Drawing.Unit.Cm(1.032D));
            this.textBox158.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox158.Value = "Name of Signatory";
            // 
            // textBox163
            // 
            this.textBox163.Name = "textBox163";
            this.textBox163.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.825D), Telerik.Reporting.Drawing.Unit.Cm(1.032D));
            this.textBox163.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox163.Value = "Signature";
            // 
            // textBox165
            // 
            this.textBox165.Name = "textBox165";
            this.textBox165.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.825D), Telerik.Reporting.Drawing.Unit.Cm(1.032D));
            this.textBox165.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox165.Value = "Date";
            // 
            // htmlTextBox4
            // 
            this.htmlTextBox4.Name = "htmlTextBox4";
            this.htmlTextBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.476D), Telerik.Reporting.Drawing.Unit.Cm(1.003D));
            this.htmlTextBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.htmlTextBox4.StyleName = "";
            this.htmlTextBox4.Value = "To find out more about how Origin collects, uses, holds and discloses personal an" +
    "d credit information about individuals, see our privacy statements at <strong>or" +
    "iginenergy.com.au/privacy</strong>";
            // 
            // table43
            // 
            this.table43.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.75D)));
            this.table43.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(4.551D)));
            this.table43.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(0.756D)));
            this.table43.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(11.377D)));
            this.table43.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.609D)));
            this.table43.Body.SetCellContent(0, 0, this.textBox167);
            this.table43.Body.SetCellContent(0, 1, this.textBox169);
            this.table43.Body.SetCellContent(0, 3, this.textBox171);
            this.table43.Body.SetCellContent(0, 2, this.textBox173);
            tableGroup175.Name = "tableGroup17";
            tableGroup175.ReportItem = this.pictureBox2;
            tableGroup176.Name = "tableGroup18";
            tableGroup176.ReportItem = this.textBox168;
            tableGroup177.Name = "group52";
            tableGroup177.ReportItem = this.pictureBox3;
            tableGroup178.Name = "tableGroup19";
            tableGroup178.ReportItem = this.textBox170;
            this.table43.ColumnGroups.Add(tableGroup175);
            this.table43.ColumnGroups.Add(tableGroup176);
            this.table43.ColumnGroups.Add(tableGroup177);
            this.table43.ColumnGroups.Add(tableGroup178);
            this.table43.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox167,
            this.textBox169,
            this.textBox173,
            this.textBox171,
            this.pictureBox2,
            this.textBox168,
            this.pictureBox3,
            this.textBox170});
            this.table43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.505D), Telerik.Reporting.Drawing.Unit.Cm(13.575D));
            this.table43.Name = "table43";
            tableGroup179.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup179.Name = "detailTableGroup7";
            this.table43.RowGroups.Add(tableGroup179);
            this.table43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.434D), Telerik.Reporting.Drawing.Unit.Cm(1.359D));
            // 
            // textBox167
            // 
            this.textBox167.Name = "textBox167";
            this.textBox167.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.75D), Telerik.Reporting.Drawing.Unit.Cm(0.609D));
            // 
            // textBox169
            // 
            this.textBox169.Name = "textBox169";
            this.textBox169.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.551D), Telerik.Reporting.Drawing.Unit.Cm(0.609D));
            // 
            // textBox171
            // 
            this.textBox171.Name = "textBox171";
            this.textBox171.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.377D), Telerik.Reporting.Drawing.Unit.Cm(0.609D));
            // 
            // textBox173
            // 
            this.textBox173.Name = "textBox173";
            this.textBox173.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.756D), Telerik.Reporting.Drawing.Unit.Cm(0.609D));
            this.textBox173.StyleName = "";
            // 
            // textBox164
            // 
            this.textBox164.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.506D), Telerik.Reporting.Drawing.Unit.Cm(25.535D));
            this.textBox164.Name = "textBox164";
            this.textBox164.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(17.434D), Telerik.Reporting.Drawing.Unit.Cm(0.997D));
            this.textBox164.Style.Color = System.Drawing.Color.Gray;
            this.textBox164.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7D);
            this.textBox164.Value = resources.GetString("textBox164.Value");
            // 
            // SAOrigin
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SAOrigin";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(1D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.UnitOfMeasure = Telerik.Reporting.Drawing.UnitType.Cm;
            this.Width = Telerik.Reporting.Drawing.Unit.Cm(19.019D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion
		private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.CheckBox checkBox1;
        private Telerik.Reporting.CheckBox checkBox2;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.CheckBox checkBox3;
        private Telerik.Reporting.CheckBox checkBox4;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.Table table9;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.Table table10;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.Table table11;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.Table table12;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.Table table13;
        private Telerik.Reporting.CheckBox checkBox5;
        private Telerik.Reporting.CheckBox checkBox6;
        private Telerik.Reporting.Table table14;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.Table table15;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.Table table16;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.Table table17;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.Table table18;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.Table table19;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.Table table20;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.Table table21;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.Table table22;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.Table table23;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.Table table24;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.HtmlTextBox htmlTextBox2;
        private Telerik.Reporting.CheckBox checkBox7;
        private Telerik.Reporting.CheckBox checkBox8;
        private Telerik.Reporting.CheckBox checkBox9;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.CheckBox checkBox10;
        private Telerik.Reporting.CheckBox checkBox11;
        private Telerik.Reporting.CheckBox checkBox12;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.Table table25;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.Table table26;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.CheckBox checkBox13;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.CheckBox checkBox14;
        private Telerik.Reporting.CheckBox checkBox15;
        private Telerik.Reporting.CheckBox checkBox16;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.CheckBox checkBox17;
        private Telerik.Reporting.CheckBox checkBox18;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.Table table27;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.Table table29;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.Table table28;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.Table table30;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.CheckBox checkBox19;
        private Telerik.Reporting.CheckBox checkBox20;
        private Telerik.Reporting.Table table31;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.Table table32;
        private Telerik.Reporting.TextBox textBox112;
        private Telerik.Reporting.Panel panel2;
        private Telerik.Reporting.Table table33;
        private Telerik.Reporting.TextBox textBox106;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.TextBox textBox114;
        private Telerik.Reporting.TextBox textBox115;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.TextBox textBox117;
        private Telerik.Reporting.Table table35;
        private Telerik.Reporting.TextBox textBox119;
        private Telerik.Reporting.TextBox textBox120;
        private Telerik.Reporting.TextBox textBox122;
        private Telerik.Reporting.TextBox textBox124;
        private Telerik.Reporting.Panel panel3;
        private Telerik.Reporting.Table table37;
        private Telerik.Reporting.TextBox textBox123;
        private Telerik.Reporting.TextBox textBox125;
        private Telerik.Reporting.TextBox textBox126;
        private Telerik.Reporting.TextBox textBox130;
        private Telerik.Reporting.Table table34;
        private Telerik.Reporting.TextBox textBox131;
        private Telerik.Reporting.Table table36;
        private Telerik.Reporting.TextBox textBox121;
        private Telerik.Reporting.Table table38;
        private Telerik.Reporting.TextBox textBox118;
        private Telerik.Reporting.TextBox textBox127;
        private Telerik.Reporting.TextBox textBox128;
        private Telerik.Reporting.TextBox textBox129;
        private Telerik.Reporting.TextBox textBox132;
        private Telerik.Reporting.TextBox textBox133;
        private Telerik.Reporting.TextBox textBox134;
        private Telerik.Reporting.TextBox textBox135;
        private Telerik.Reporting.TextBox textBox138;
        private Telerik.Reporting.TextBox textBox139;
        private Telerik.Reporting.TextBox textBox140;
        private Telerik.Reporting.TextBox textBox136;
        private Telerik.Reporting.TextBox textBox137;
        private Telerik.Reporting.TextBox textBox141;
        private Telerik.Reporting.TextBox textBox142;
        private Telerik.Reporting.TextBox textBox143;
        private Telerik.Reporting.TextBox textBox144;
        private Telerik.Reporting.TextBox textBox145;
        private Telerik.Reporting.TextBox textBox146;
        private Telerik.Reporting.TextBox textBox152;
        private Telerik.Reporting.TextBox textBox153;
        private Telerik.Reporting.TextBox textBox155;
        private Telerik.Reporting.TextBox textBox156;
        private Telerik.Reporting.TextBox textBox157;
        private Telerik.Reporting.TextBox textBox147;
        private Telerik.Reporting.TextBox textBox148;
        private Telerik.Reporting.TextBox textBox149;
        private Telerik.Reporting.TextBox textBox150;
        private Telerik.Reporting.TextBox textBox159;
        private Telerik.Reporting.TextBox textBox160;
        private Telerik.Reporting.TextBox textBox161;
        private Telerik.Reporting.TextBox textBox151;
        private Telerik.Reporting.HtmlTextBox htmlTextBox3;
        private Telerik.Reporting.Table table39;
        private Telerik.Reporting.TextBox textBox158;
        private Telerik.Reporting.TextBox textBox163;
        private Telerik.Reporting.TextBox textBox165;
        private Telerik.Reporting.Table table40;
        private Telerik.Reporting.TextBox textBox166;
        private Telerik.Reporting.Table table41;
        private Telerik.Reporting.TextBox textBox154;
        private Telerik.Reporting.Table table42;
        private Telerik.Reporting.TextBox textBox162;
        private Telerik.Reporting.HtmlTextBox htmlTextBox4;
        private Telerik.Reporting.Table table43;
        private Telerik.Reporting.TextBox textBox167;
        private Telerik.Reporting.TextBox textBox169;
        private Telerik.Reporting.TextBox textBox171;
        private Telerik.Reporting.TextBox textBox173;
        private Telerik.Reporting.TextBox textBox168;
        private Telerik.Reporting.TextBox textBox170;
        private Telerik.Reporting.PictureBox pictureBox2;
        private Telerik.Reporting.PictureBox pictureBox3;
        private Telerik.Reporting.TextBox textBox164;
    }
}