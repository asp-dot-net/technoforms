namespace TechnoForms.Report
{
    partial class SWR
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SWR));
			Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.textBox73 = new Telerik.Reporting.TextBox();
			this.detail = new Telerik.Reporting.DetailSection();
			this.pictureBox1 = new Telerik.Reporting.PictureBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.table1 = new Telerik.Reporting.Table();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox26 = new Telerik.Reporting.TextBox();
			this.textBox29 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox30 = new Telerik.Reporting.TextBox();
			this.textBox27 = new Telerik.Reporting.TextBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.textBox38 = new Telerik.Reporting.TextBox();
			this.textBox32 = new Telerik.Reporting.TextBox();
			this.textBox35 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox25 = new Telerik.Reporting.TextBox();
			this.textBox28 = new Telerik.Reporting.TextBox();
			this.textBox31 = new Telerik.Reporting.TextBox();
			this.textBox33 = new Telerik.Reporting.TextBox();
			this.textBox34 = new Telerik.Reporting.TextBox();
			this.textBox36 = new Telerik.Reporting.TextBox();
			this.textBox37 = new Telerik.Reporting.TextBox();
			this.textBox39 = new Telerik.Reporting.TextBox();
			this.textBox40 = new Telerik.Reporting.TextBox();
			this.textBox41 = new Telerik.Reporting.TextBox();
			this.textBox42 = new Telerik.Reporting.TextBox();
			this.textBox43 = new Telerik.Reporting.TextBox();
			this.textBox44 = new Telerik.Reporting.TextBox();
			this.textBox45 = new Telerik.Reporting.TextBox();
			this.textBox46 = new Telerik.Reporting.TextBox();
			this.textBox47 = new Telerik.Reporting.TextBox();
			this.textBox48 = new Telerik.Reporting.TextBox();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.textBox50 = new Telerik.Reporting.TextBox();
			this.textBox51 = new Telerik.Reporting.TextBox();
			this.textBox52 = new Telerik.Reporting.TextBox();
			this.textBox53 = new Telerik.Reporting.TextBox();
			this.textBox54 = new Telerik.Reporting.TextBox();
			this.textBox55 = new Telerik.Reporting.TextBox();
			this.textBox56 = new Telerik.Reporting.TextBox();
			this.textBox57 = new Telerik.Reporting.TextBox();
			this.textBox58 = new Telerik.Reporting.TextBox();
			this.textBox59 = new Telerik.Reporting.TextBox();
			this.textBox60 = new Telerik.Reporting.TextBox();
			this.textBox61 = new Telerik.Reporting.TextBox();
			this.textBox62 = new Telerik.Reporting.TextBox();
			this.textBox63 = new Telerik.Reporting.TextBox();
			this.textBox64 = new Telerik.Reporting.TextBox();
			this.textBox65 = new Telerik.Reporting.TextBox();
			this.textBox66 = new Telerik.Reporting.TextBox();
			this.textBox67 = new Telerik.Reporting.TextBox();
			this.textBox68 = new Telerik.Reporting.TextBox();
			this.textBox69 = new Telerik.Reporting.TextBox();
			this.textBox70 = new Telerik.Reporting.TextBox();
			this.table2 = new Telerik.Reporting.Table();
			this.textBox71 = new Telerik.Reporting.TextBox();
			this.textBox72 = new Telerik.Reporting.TextBox();
			this.textBox77 = new Telerik.Reporting.TextBox();
			this.textBox80 = new Telerik.Reporting.TextBox();
			this.textBox83 = new Telerik.Reporting.TextBox();
			this.textBox86 = new Telerik.Reporting.TextBox();
			this.textBox89 = new Telerik.Reporting.TextBox();
			this.table3 = new Telerik.Reporting.Table();
			this.textBox81 = new Telerik.Reporting.TextBox();
			this.textBox85 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// textBox73
			// 
			this.textBox73.Name = "textBox73";
			this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.974D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox73.Style.Font.Bold = true;
			this.textBox73.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox73.Value = "Information about our privacy policy ";
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(297D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.table1,
            this.textBox8,
            this.textBox9,
            this.textBox11,
            this.textBox15,
            this.textBox16,
            this.textBox19,
            this.textBox21,
            this.textBox22,
            this.textBox25,
            this.textBox28,
            this.textBox31,
            this.textBox33,
            this.textBox34,
            this.textBox36,
            this.textBox37,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox51,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.table2,
            this.table3});
			this.detail.Name = "detail";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
			this.pictureBox1.MimeType = "image/jpeg";
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
			this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.ScaleProportional;
			this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(1.1D));
			this.textBox1.Style.Font.Bold = true;
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
			this.textBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox1.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox1.Value = "Service Works Request ";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(1.1D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
			this.textBox2.Style.Font.Bold = true;
			this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(20D);
			this.textBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox2.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox2.Value = "for a solar meter ";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(0.682D));
			this.textBox3.Style.Font.Bold = true;
			this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox3.Value = "(Electricity)\r\n";
			// 
			// textBox4
			// 
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(2.7D));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox4.Style.Font.Bold = true;
			this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox4.Value = "Use this form to apply for: ";
			// 
			// textBox5
			// 
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(3.439D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(0.45D));
			this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(25D);
			this.textBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox5.Value = "Solar meter upgrade ";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(3.9D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(0.45D));
			this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(25D);
			this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox6.Value = "NSW, ACT and SA customers only\r\n";
			// 
			// textBox7
			// 
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(4.5D));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(1.9D));
			this.textBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox7.Value = resources.GetString("textBox7.Value");
			// 
			// table1
			// 
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.392D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(1.63D)));
			this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(6.968D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.52D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.441D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.467D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.441D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.441D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.441D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.441D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.542D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.418D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.42D)));
			this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.418D)));
			this.table1.Body.SetCellContent(1, 2, this.textBox12);
			this.table1.Body.SetCellContent(3, 0, this.textBox17);
			this.table1.Body.SetCellContent(7, 0, this.textBox23);
			this.table1.Body.SetCellContent(6, 0, this.textBox26);
			this.table1.Body.SetCellContent(5, 0, this.textBox29);
			this.table1.Body.SetCellContent(0, 0, this.textBox14, 1, 3);
			this.table1.Body.SetCellContent(1, 0, this.textBox10, 1, 2);
			this.table1.Body.SetCellContent(2, 0, this.textBox13, 1, 3);
			this.table1.Body.SetCellContent(3, 1, this.textBox18, 1, 2);
			this.table1.Body.SetCellContent(4, 0, this.textBox20, 1, 3);
			this.table1.Body.SetCellContent(5, 1, this.textBox30, 1, 2);
			this.table1.Body.SetCellContent(6, 1, this.textBox27, 1, 2);
			this.table1.Body.SetCellContent(7, 1, this.textBox24, 1, 2);
			this.table1.Body.SetCellContent(8, 0, this.textBox38, 1, 3);
			this.table1.Body.SetCellContent(9, 0, this.textBox32, 1, 3);
			this.table1.Body.SetCellContent(10, 0, this.textBox35, 1, 3);
			tableGroup1.Name = "tableGroup";
			tableGroup2.Name = "group";
			tableGroup3.Name = "group2";
			this.table1.ColumnGroups.Add(tableGroup1);
			this.table1.ColumnGroups.Add(tableGroup2);
			this.table1.ColumnGroups.Add(tableGroup3);
			this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox14,
            this.textBox10,
            this.textBox12,
            this.textBox13,
            this.textBox17,
            this.textBox18,
            this.textBox20,
            this.textBox29,
            this.textBox30,
            this.textBox26,
            this.textBox27,
            this.textBox23,
            this.textBox24,
            this.textBox38,
            this.textBox32,
            this.textBox35});
			this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.6D), Telerik.Reporting.Drawing.Unit.Cm(1.6D));
			this.table1.Name = "table1";
			tableGroup5.Name = "group1";
			tableGroup6.Name = "group3";
			tableGroup7.Name = "group4";
			tableGroup8.Name = "group5";
			tableGroup9.Name = "group6";
			tableGroup10.Name = "group9";
			tableGroup11.Name = "group8";
			tableGroup12.Name = "group7";
			tableGroup13.Name = "group12";
			tableGroup14.Name = "group10";
			tableGroup15.Name = "group11";
			tableGroup4.ChildGroups.Add(tableGroup5);
			tableGroup4.ChildGroups.Add(tableGroup6);
			tableGroup4.ChildGroups.Add(tableGroup7);
			tableGroup4.ChildGroups.Add(tableGroup8);
			tableGroup4.ChildGroups.Add(tableGroup9);
			tableGroup4.ChildGroups.Add(tableGroup10);
			tableGroup4.ChildGroups.Add(tableGroup11);
			tableGroup4.ChildGroups.Add(tableGroup12);
			tableGroup4.ChildGroups.Add(tableGroup13);
			tableGroup4.ChildGroups.Add(tableGroup14);
			tableGroup4.ChildGroups.Add(tableGroup15);
			tableGroup4.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup4.Name = "detailTableGroup";
			this.table1.RowGroups.Add(tableGroup4);
			this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.99D), Telerik.Reporting.Drawing.Unit.Cm(4.99D));
			this.table1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			// 
			// textBox12
			// 
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.968D), Telerik.Reporting.Drawing.Unit.Cm(0.441D));
			this.textBox12.StyleName = "";
			this.textBox12.Value = "solarconnections@energyaustralia.com.au ";
			// 
			// textBox17
			// 
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.392D), Telerik.Reporting.Drawing.Unit.Cm(0.441D));
			this.textBox17.Style.Font.Bold = true;
			this.textBox17.StyleName = "";
			this.textBox17.Value = "form to: ";
			// 
			// textBox23
			// 
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.392D), Telerik.Reporting.Drawing.Unit.Cm(0.542D));
			this.textBox23.Style.Font.Bold = true;
			this.textBox23.StyleName = "";
			this.textBox23.Value = "Fax: ";
			// 
			// textBox26
			// 
			this.textBox26.Name = "textBox26";
			this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.392D), Telerik.Reporting.Drawing.Unit.Cm(0.441D));
			this.textBox26.StyleName = "";
			// 
			// textBox29
			// 
			this.textBox29.Name = "textBox29";
			this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.392D), Telerik.Reporting.Drawing.Unit.Cm(0.441D));
			this.textBox29.Style.Font.Bold = true;
			this.textBox29.StyleName = "";
			this.textBox29.Value = "Post: ";
			// 
			// textBox14
			// 
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.99D), Telerik.Reporting.Drawing.Unit.Cm(0.52D));
			this.textBox14.Style.Font.Bold = true;
			this.textBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox14.StyleName = "";
			this.textBox14.Value = "If solar panels were not installed by us, please email your ";
			// 
			// textBox10
			// 
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.022D), Telerik.Reporting.Drawing.Unit.Cm(0.441D));
			this.textBox10.Style.Font.Bold = true;
			this.textBox10.StyleName = "";
			this.textBox10.Value = "completed form to: ";
			// 
			// textBox13
			// 
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.99D), Telerik.Reporting.Drawing.Unit.Cm(0.467D));
			this.textBox13.Style.Font.Bold = true;
			this.textBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox13.StyleName = "";
			this.textBox13.Value = "If solar panels were installed by us, please email your completed ";
			// 
			// textBox18
			// 
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.598D), Telerik.Reporting.Drawing.Unit.Cm(0.441D));
			this.textBox18.StyleName = "";
			this.textBox18.Value = "solarnextgen@energyaustralia.com.au ";
			// 
			// textBox20
			// 
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.99D), Telerik.Reporting.Drawing.Unit.Cm(0.441D));
			this.textBox20.Style.Font.Bold = true;
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox20.StyleName = "";
			this.textBox20.Value = "Alternatively, you can send your form to us via: ";
			// 
			// textBox30
			// 
			this.textBox30.Name = "textBox30";
			this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.598D), Telerik.Reporting.Drawing.Unit.Cm(0.441D));
			this.textBox30.StyleName = "";
			this.textBox30.Value = "EnergyAustralia Electricity New Connections ";
			// 
			// textBox27
			// 
			this.textBox27.Name = "textBox27";
			this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.598D), Telerik.Reporting.Drawing.Unit.Cm(0.441D));
			this.textBox27.StyleName = "";
			this.textBox27.Value = "Locked Bag 14060, Melbourne City MC VIC 8001 ";
			// 
			// textBox24
			// 
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.598D), Telerik.Reporting.Drawing.Unit.Cm(0.542D));
			this.textBox24.StyleName = "";
			this.textBox24.Value = "1800 654 295 ";
			// 
			// textBox38
			// 
			this.textBox38.Name = "textBox38";
			this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.99D), Telerik.Reporting.Drawing.Unit.Cm(0.418D));
			this.textBox38.StyleName = "";
			this.textBox38.Value = "If you have any questions, or need help with your application, ";
			// 
			// textBox32
			// 
			this.textBox32.Name = "textBox32";
			this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.99D), Telerik.Reporting.Drawing.Unit.Cm(0.42D));
			this.textBox32.StyleName = "";
			this.textBox32.Value = "please call us on 1800 818 378 (Monday to Friday, 8am-5pm AEST) ";
			// 
			// textBox35
			// 
			this.textBox35.Name = "textBox35";
			this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.99D), Telerik.Reporting.Drawing.Unit.Cm(0.418D));
			this.textBox35.StyleName = "";
			this.textBox35.Value = "and we�ll be happy to help.\r\n";
			// 
			// textBox8
			// 
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.339D), Telerik.Reporting.Drawing.Unit.Cm(6.8D));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.171D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox8.Style.Color = System.Drawing.Color.White;
			this.textBox8.Style.Font.Bold = true;
			this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox8.Value = "1. Site address\r\n";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.339D), Telerik.Reporting.Drawing.Unit.Cm(7.626D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.461D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox9.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox9.Value = "Lot no:";
			// 
			// textBox11
			// 
			this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(7.626D));
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox11.Value = "Unit no:";
			// 
			// textBox15
			// 
			this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.826D), Telerik.Reporting.Drawing.Unit.Cm(7.626D));
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox15.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox15.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox15.Value = "Street no:";
			// 
			// textBox16
			// 
			this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.1D), Telerik.Reporting.Drawing.Unit.Cm(7.626D));
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox16.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox16.Value = "Street name:";
			// 
			// textBox19
			// 
			this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(8.426D));
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.561D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox19.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox19.Value = "Suburb:";
			// 
			// textBox21
			// 
			this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.1D), Telerik.Reporting.Drawing.Unit.Cm(8.426D));
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox21.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox21.Value = "State:";
			// 
			// textBox22
			// 
			this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.626D), Telerik.Reporting.Drawing.Unit.Cm(8.426D));
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox22.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox22.Value = "Postcode:";
			// 
			// textBox25
			// 
			this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.339D), Telerik.Reporting.Drawing.Unit.Cm(9.2D));
			this.textBox25.Name = "textBox25";
			this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.861D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox25.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox25.Value = "Plan of sub-division no:";
			// 
			// textBox28
			// 
			this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.8D), Telerik.Reporting.Drawing.Unit.Cm(9.2D));
			this.textBox28.Name = "textBox28";
			this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox28.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox28.Value = "Electricity meter ID or NMI no:\r\n";
			// 
			// textBox31
			// 
			this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(10.139D));
			this.textBox31.Name = "textBox31";
			this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.151D), Telerik.Reporting.Drawing.Unit.Cm(0.661D));
			this.textBox31.Style.Color = System.Drawing.Color.White;
			this.textBox31.Style.Font.Bold = true;
			this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox31.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox31.Value = "2. Solar installer�s details\r\n";
			// 
			// textBox33
			// 
			this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(14.3D));
			this.textBox33.Name = "textBox33";
			this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.151D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox33.Style.Color = System.Drawing.Color.White;
			this.textBox33.Style.Font.Bold = true;
			this.textBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox33.Value = "3. Account holder details\r\n";
			// 
			// textBox34
			// 
			this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(16.689D));
			this.textBox34.Name = "textBox34";
			this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.151D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox34.Style.Color = System.Drawing.Color.White;
			this.textBox34.Style.Font.Bold = true;
			this.textBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox34.Value = "Mailing address\r\n";
			// 
			// textBox36
			// 
			this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(20.639D));
			this.textBox36.Name = "textBox36";
			this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.151D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox36.Style.Color = System.Drawing.Color.White;
			this.textBox36.Style.Font.Bold = true;
			this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox36.Value = "4. Solar application details\r\n";
			// 
			// textBox37
			// 
			this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(23.9D));
			this.textBox37.Name = "textBox37";
			this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.151D), Telerik.Reporting.Drawing.Unit.Cm(0.65D));
			this.textBox37.Style.Color = System.Drawing.Color.White;
			this.textBox37.Style.Font.Bold = true;
			this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox37.Value = "5. To be completed by the account holder";
			// 
			// textBox39
			// 
			this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(15.1D));
			this.textBox39.Name = "textBox39";
			this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.241D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox39.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox39.Value = "Title:";
			// 
			// textBox40
			// 
			this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(15.1D));
			this.textBox40.Name = "textBox40";
			this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.5D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox40.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox40.Value = "First name:";
			// 
			// textBox41
			// 
			this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.3D), Telerik.Reporting.Drawing.Unit.Cm(15.1D));
			this.textBox41.Name = "textBox41";
			this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox41.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox41.Value = "Last name:";
			// 
			// textBox42
			// 
			this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(15.9D));
			this.textBox42.Name = "textBox42";
			this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.841D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox42.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox42.Value = "Driver�s licence or Medicare number:";
			// 
			// textBox43
			// 
			this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.727D), Telerik.Reporting.Drawing.Unit.Cm(15.9D));
			this.textBox43.Name = "textBox43";
			this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.473D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox43.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox43.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox43.Value = "Issuing State:";
			// 
			// textBox44
			// 
			this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.5D), Telerik.Reporting.Drawing.Unit.Cm(15.9D));
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox44.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox44.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox44.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox44.Value = "Expiry Date:";
			// 
			// textBox45
			// 
			this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(11D));
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox45.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox45.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox45.Value = "First name:";
			// 
			// textBox46
			// 
			this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(11.8D));
			this.textBox46.Name = "textBox46";
			this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox46.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox46.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox46.Value = "Electrical Lic. No:";
			// 
			// textBox47
			// 
			this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.2D), Telerik.Reporting.Drawing.Unit.Cm(10.989D));
			this.textBox47.Name = "textBox47";
			this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox47.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox47.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox47.Value = "Last name:";
			// 
			// textBox48
			// 
			this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.6D), Telerik.Reporting.Drawing.Unit.Cm(11.8D));
			this.textBox48.Name = "textBox48";
			this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.52D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox48.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox48.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox48.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox48.Value = "Mobile:";
			// 
			// textBox49
			// 
			this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.892D), Telerik.Reporting.Drawing.Unit.Cm(11.8D));
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox49.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox49.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox49.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox49.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox49.Value = "Alternative Phone Number:";
			// 
			// textBox50
			// 
			this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.6D), Telerik.Reporting.Drawing.Unit.Cm(12.6D));
			this.textBox50.Name = "textBox50";
			this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox50.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox50.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox50.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(3D);
			this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox50.Value = "Business name:";
			// 
			// textBox51
			// 
			this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.6D), Telerik.Reporting.Drawing.Unit.Cm(13.3D));
			this.textBox51.Name = "textBox51";
			this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.5D), Telerik.Reporting.Drawing.Unit.Cm(0.8D));
			this.textBox51.Style.Font.Bold = true;
			this.textBox51.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox51.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox51.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox51.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox51.Value = "PV SEG approval\r\n(SA solar customers only):\r\n";
			// 
			// textBox52
			// 
			this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(12.6D));
			this.textBox52.Name = "textBox52";
			this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox52.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox52.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox52.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox52.Value = "Email:";
			// 
			// textBox53
			// 
			this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(13.4D));
			this.textBox53.Name = "textBox53";
			this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox53.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox53.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(8D);
			this.textBox53.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox53.Value = "ABN:";
			// 
			// textBox54
			// 
			this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(17.359D));
			this.textBox54.Name = "textBox54";
			this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.561D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox54.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox54.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox54.Value = "Unit no:";
			// 
			// textBox55
			// 
			this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.359D), Telerik.Reporting.Drawing.Unit.Cm(17.359D));
			this.textBox55.Name = "textBox55";
			this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.341D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox55.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox55.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox55.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox55.Value = "Street no:";
			// 
			// textBox56
			// 
			this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.1D), Telerik.Reporting.Drawing.Unit.Cm(17.359D));
			this.textBox56.Name = "textBox56";
			this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox56.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox56.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox56.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox56.Value = "Street name:";
			// 
			// textBox57
			// 
			this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(18.159D));
			this.textBox57.Name = "textBox57";
			this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.561D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox57.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox57.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox57.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox57.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox57.Value = "Suburb:";
			// 
			// textBox58
			// 
			this.textBox58.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.159D), Telerik.Reporting.Drawing.Unit.Cm(18.159D));
			this.textBox58.Name = "textBox58";
			this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox58.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox58.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox58.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox58.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox58.Value = "State:";
			// 
			// textBox59
			// 
			this.textBox59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.692D), Telerik.Reporting.Drawing.Unit.Cm(18.159D));
			this.textBox59.Name = "textBox59";
			this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox59.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox59.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox59.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox59.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox59.Value = "Postcode:";
			// 
			// textBox60
			// 
			this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(18.959D));
			this.textBox60.Name = "textBox60";
			this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox60.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox60.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox60.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox60.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox60.Value = "Alternative Phone Number:";
			// 
			// textBox61
			// 
			this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.2D), Telerik.Reporting.Drawing.Unit.Cm(18.959D));
			this.textBox61.Name = "textBox61";
			this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.65D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox61.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox61.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox61.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox61.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox61.Value = "Email:";
			// 
			// textBox62
			// 
			this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.727D), Telerik.Reporting.Drawing.Unit.Cm(18.159D));
			this.textBox62.Name = "textBox62";
			this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.873D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox62.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox62.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox62.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox62.Value = "Mobile:";
			// 
			// textBox63
			// 
			this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.9D), Telerik.Reporting.Drawing.Unit.Cm(19.759D));
			this.textBox63.Name = "textBox63";
			this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox63.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox63.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox63.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox63.Value = "ABN:";
			// 
			// textBox64
			// 
			this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(19.759D));
			this.textBox64.Name = "textBox64";
			this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox64.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox64.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox64.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox64.Value = "Business name (if applicable):";
			// 
			// textBox65
			// 
			this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.226D), Telerik.Reporting.Drawing.Unit.Cm(21.426D));
			this.textBox65.Name = "textBox65";
			this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox65.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox65.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox65.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox65.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox65.Value = "New solar system:";
			// 
			// textBox66
			// 
			this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.226D), Telerik.Reporting.Drawing.Unit.Cm(22.226D));
			this.textBox66.Name = "textBox66";
			this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.074D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox66.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox66.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox66.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox66.Value = "Upgrade to existing solar system:";
			// 
			// textBox67
			// 
			this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.6D), Telerik.Reporting.Drawing.Unit.Cm(23.1D));
			this.textBox67.Name = "textBox67";
			this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.1D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox67.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox67.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(4D);
			this.textBox67.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox67.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox67.Value = "Description of solar works:";
			// 
			// textBox68
			// 
			this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.6D), Telerik.Reporting.Drawing.Unit.Cm(29.2D));
			this.textBox68.Name = "textBox68";
			this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox68.Value = "Signature of applicant\r\n";
			// 
			// textBox69
			// 
			this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.559D), Telerik.Reporting.Drawing.Unit.Cm(28.5D));
			this.textBox69.Name = "textBox69";
			this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.741D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox69.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox69.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox69.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox69.Value = "Date:";
			// 
			// textBox70
			// 
			this.textBox70.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(24.55D));
			this.textBox70.Name = "textBox70";
			this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.151D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox70.Style.Font.Bold = true;
			this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox70.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox70.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox70.Value = "Please indicate your acceptance by signing below.";
			// 
			// table2
			// 
			this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(19.924D)));
			this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.334D)));
			this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.286D)));
			this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.286D)));
			this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.286D)));
			this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.286D)));
			this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.286D)));
			this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.334D)));
			this.table2.Body.SetCellContent(0, 0, this.textBox71);
			this.table2.Body.SetCellContent(6, 0, this.textBox72);
			this.table2.Body.SetCellContent(5, 0, this.textBox77);
			this.table2.Body.SetCellContent(4, 0, this.textBox80);
			this.table2.Body.SetCellContent(3, 0, this.textBox83);
			this.table2.Body.SetCellContent(2, 0, this.textBox86);
			this.table2.Body.SetCellContent(1, 0, this.textBox89);
			tableGroup16.Name = "tableGroup1";
			this.table2.ColumnGroups.Add(tableGroup16);
			this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox71,
            this.textBox89,
            this.textBox86,
            this.textBox83,
            this.textBox80,
            this.textBox77,
            this.textBox72});
			this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(25D));
			this.table2.Name = "table2";
			tableGroup18.Name = "group14";
			tableGroup19.Name = "group19";
			tableGroup20.Name = "group18";
			tableGroup21.Name = "group17";
			tableGroup22.Name = "group16";
			tableGroup23.Name = "group15";
			tableGroup24.Name = "group13";
			tableGroup17.ChildGroups.Add(tableGroup18);
			tableGroup17.ChildGroups.Add(tableGroup19);
			tableGroup17.ChildGroups.Add(tableGroup20);
			tableGroup17.ChildGroups.Add(tableGroup21);
			tableGroup17.ChildGroups.Add(tableGroup22);
			tableGroup17.ChildGroups.Add(tableGroup23);
			tableGroup17.ChildGroups.Add(tableGroup24);
			tableGroup17.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup17.Name = "detailTableGroup1";
			this.table2.RowGroups.Add(tableGroup17);
			this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.1D), Telerik.Reporting.Drawing.Unit.Cm(2.098D));
			this.table2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.3D);
			this.table2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.table2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0D);
			// 
			// textBox71
			// 
			this.textBox71.Name = "textBox71";
			this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.924D), Telerik.Reporting.Drawing.Unit.Cm(0.334D));
			this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox71.StyleName = "";
			this.textBox71.Value = "Please note: If the premises are leased, please include a letter of permission fr" +
    "om the owner/landlord with your application. ";
			// 
			// textBox72
			// 
			this.textBox72.Name = "textBox72";
			this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.924D), Telerik.Reporting.Drawing.Unit.Cm(0.334D));
			this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox72.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
			this.textBox72.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
			this.textBox72.StyleName = "";
			this.textBox72.Value = "I authorise EnergyAustralia to contact me to set me up on a solar plan. ";
			// 
			// textBox77
			// 
			this.textBox77.Name = "textBox77";
			this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.924D), Telerik.Reporting.Drawing.Unit.Cm(0.286D));
			this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox77.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox77.StyleName = "";
			this.textBox77.Value = "I understand that if the technician determines additional works are needed they w" +
    "ill not commence the job and EnergyAustralia will contact me to discuss any addi" +
    "tional charges and/or work required. ";
			// 
			// textBox80
			// 
			this.textBox80.Name = "textBox80";
			this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.924D), Telerik.Reporting.Drawing.Unit.Cm(0.286D));
			this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox80.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox80.StyleName = "";
			this.textBox80.Value = "clear and safe access) and they can vary depending on the works required. I agree" +
    " to accept any charges associated with the alteration and understand these charg" +
    "es will appear on my electricity bill. ";
			// 
			// textBox83
			// 
			this.textBox83.Name = "textBox83";
			this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.924D), Telerik.Reporting.Drawing.Unit.Cm(0.286D));
			this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox83.StyleName = "";
			this.textBox83.Value = "and debris and have clear address numbering) and the meter board must not be defe" +
    "ctive or otherwise unsafe. I understand that charges may apply to the alteration" +
    " (including if I am unable to provide";
			// 
			// textBox86
			// 
			this.textBox86.Name = "textBox86";
			this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.924D), Telerik.Reporting.Drawing.Unit.Cm(0.286D));
			this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox86.StyleName = "";
			this.textBox86.Value = resources.GetString("textBox86.Value");
			// 
			// textBox89
			// 
			this.textBox89.Name = "textBox89";
			this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.924D), Telerik.Reporting.Drawing.Unit.Cm(0.286D));
			this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox89.StyleName = "";
			this.textBox89.Value = resources.GetString("textBox89.Value");
			// 
			// table3
			// 
			this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Cm(19.975D)));
			this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.3D)));
			this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Cm(0.3D)));
			this.table3.Body.SetCellContent(1, 0, this.textBox81);
			this.table3.Body.SetCellContent(0, 0, this.textBox85);
			tableGroup25.Name = "tableGroup2";
			tableGroup25.ReportItem = this.textBox73;
			this.table3.ColumnGroups.Add(tableGroup25);
			this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox85,
            this.textBox81,
            this.textBox73});
			this.table3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.359D), Telerik.Reporting.Drawing.Unit.Cm(27.1D));
			this.table3.Name = "table3";
			tableGroup27.Name = "group22";
			tableGroup28.Name = "group21";
			tableGroup26.ChildGroups.Add(tableGroup27);
			tableGroup26.ChildGroups.Add(tableGroup28);
			tableGroup26.Groupings.Add(new Telerik.Reporting.Grouping(null));
			tableGroup26.Name = "detailTableGroup2";
			this.table3.RowGroups.Add(tableGroup26);
			this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(20.151D), Telerik.Reporting.Drawing.Unit.Cm(1D));
			this.table3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
			// 
			// textBox81
			// 
			this.textBox81.Name = "textBox81";
			this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.975D), Telerik.Reporting.Drawing.Unit.Cm(0.3D));
			this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox81.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox81.StyleName = "";
			this.textBox81.Value = "metering service provider and/or electricity distributor in accordance with our p" +
    "rivacy policy which can be found at energyaustralia.com.au/privacy.\r\n";
			// 
			// textBox85
			// 
			this.textBox85.Name = "textBox85";
			this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.975D), Telerik.Reporting.Drawing.Unit.Cm(0.3D));
			this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox85.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox85.StyleName = "";
			this.textBox85.Value = resources.GetString("textBox85.Value");
			// 
			// SWR
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
			this.Name = "SWR";
			this.PageSettings.ContinuousPaper = false;
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.PictureBox pictureBox1;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox3;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox5;
		private Telerik.Reporting.TextBox textBox6;
		private Telerik.Reporting.TextBox textBox7;
		private Telerik.Reporting.Table table1;
		private Telerik.Reporting.TextBox textBox14;
		private Telerik.Reporting.TextBox textBox12;
		private Telerik.Reporting.TextBox textBox17;
		private Telerik.Reporting.TextBox textBox23;
		private Telerik.Reporting.TextBox textBox26;
		private Telerik.Reporting.TextBox textBox29;
		private Telerik.Reporting.TextBox textBox10;
		private Telerik.Reporting.TextBox textBox13;
		private Telerik.Reporting.TextBox textBox18;
		private Telerik.Reporting.TextBox textBox20;
		private Telerik.Reporting.TextBox textBox30;
		private Telerik.Reporting.TextBox textBox27;
		private Telerik.Reporting.TextBox textBox24;
		private Telerik.Reporting.TextBox textBox38;
		private Telerik.Reporting.TextBox textBox32;
		private Telerik.Reporting.TextBox textBox35;
		private Telerik.Reporting.TextBox textBox8;
		private Telerik.Reporting.TextBox textBox9;
		private Telerik.Reporting.TextBox textBox11;
		private Telerik.Reporting.TextBox textBox15;
		private Telerik.Reporting.TextBox textBox16;
		private Telerik.Reporting.TextBox textBox19;
		private Telerik.Reporting.TextBox textBox21;
		private Telerik.Reporting.TextBox textBox22;
		private Telerik.Reporting.TextBox textBox25;
		private Telerik.Reporting.TextBox textBox28;
		private Telerik.Reporting.TextBox textBox31;
		private Telerik.Reporting.TextBox textBox33;
		private Telerik.Reporting.TextBox textBox34;
		private Telerik.Reporting.TextBox textBox36;
		private Telerik.Reporting.TextBox textBox37;
		private Telerik.Reporting.TextBox textBox39;
		private Telerik.Reporting.TextBox textBox40;
		private Telerik.Reporting.TextBox textBox41;
		private Telerik.Reporting.TextBox textBox42;
		private Telerik.Reporting.TextBox textBox43;
		private Telerik.Reporting.TextBox textBox44;
		private Telerik.Reporting.TextBox textBox45;
		private Telerik.Reporting.TextBox textBox46;
		private Telerik.Reporting.TextBox textBox47;
		private Telerik.Reporting.TextBox textBox48;
		private Telerik.Reporting.TextBox textBox49;
		private Telerik.Reporting.TextBox textBox50;
		private Telerik.Reporting.TextBox textBox51;
		private Telerik.Reporting.TextBox textBox52;
		private Telerik.Reporting.TextBox textBox53;
		private Telerik.Reporting.TextBox textBox54;
		private Telerik.Reporting.TextBox textBox55;
		private Telerik.Reporting.TextBox textBox56;
		private Telerik.Reporting.TextBox textBox57;
		private Telerik.Reporting.TextBox textBox58;
		private Telerik.Reporting.TextBox textBox59;
		private Telerik.Reporting.TextBox textBox60;
		private Telerik.Reporting.TextBox textBox61;
		private Telerik.Reporting.TextBox textBox62;
		private Telerik.Reporting.TextBox textBox63;
		private Telerik.Reporting.TextBox textBox64;
		private Telerik.Reporting.TextBox textBox65;
		private Telerik.Reporting.TextBox textBox66;
		private Telerik.Reporting.TextBox textBox67;
		private Telerik.Reporting.TextBox textBox68;
		private Telerik.Reporting.TextBox textBox69;
		private Telerik.Reporting.TextBox textBox70;
		private Telerik.Reporting.Table table2;
		private Telerik.Reporting.TextBox textBox71;
		private Telerik.Reporting.TextBox textBox72;
		private Telerik.Reporting.TextBox textBox77;
		private Telerik.Reporting.TextBox textBox80;
		private Telerik.Reporting.TextBox textBox83;
		private Telerik.Reporting.TextBox textBox86;
		private Telerik.Reporting.TextBox textBox89;
		private Telerik.Reporting.Table table3;
		private Telerik.Reporting.TextBox textBox81;
		private Telerik.Reporting.TextBox textBox85;
		private Telerik.Reporting.TextBox textBox73;
	}
}