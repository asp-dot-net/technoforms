namespace TechnoForms.Report
{
    partial class NSWOrigin
    {
        #region Component Designer generated code
        /// <summary>
        /// Required method for telerik Reporting designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NSWOrigin));
			Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
			this.detail = new Telerik.Reporting.DetailSection();
			this.pictureBox1 = new Telerik.Reporting.PictureBox();
			this.pictureBox2 = new Telerik.Reporting.PictureBox();
			this.textBox1 = new Telerik.Reporting.TextBox();
			this.textBox2 = new Telerik.Reporting.TextBox();
			this.textBox3 = new Telerik.Reporting.TextBox();
			this.textBox4 = new Telerik.Reporting.TextBox();
			this.textBox5 = new Telerik.Reporting.TextBox();
			this.textBox6 = new Telerik.Reporting.TextBox();
			this.textBox7 = new Telerik.Reporting.TextBox();
			this.textBox8 = new Telerik.Reporting.TextBox();
			this.textBox9 = new Telerik.Reporting.TextBox();
			this.textBox10 = new Telerik.Reporting.TextBox();
			this.textBox11 = new Telerik.Reporting.TextBox();
			this.textBox12 = new Telerik.Reporting.TextBox();
			this.textBox13 = new Telerik.Reporting.TextBox();
			this.textBox14 = new Telerik.Reporting.TextBox();
			this.textBox15 = new Telerik.Reporting.TextBox();
			this.textBox16 = new Telerik.Reporting.TextBox();
			this.textBox17 = new Telerik.Reporting.TextBox();
			this.textBox18 = new Telerik.Reporting.TextBox();
			this.textBox19 = new Telerik.Reporting.TextBox();
			this.textBox20 = new Telerik.Reporting.TextBox();
			this.textBox21 = new Telerik.Reporting.TextBox();
			this.textBox22 = new Telerik.Reporting.TextBox();
			this.textBox23 = new Telerik.Reporting.TextBox();
			this.textBox24 = new Telerik.Reporting.TextBox();
			this.textBox25 = new Telerik.Reporting.TextBox();
			this.textBox26 = new Telerik.Reporting.TextBox();
			this.textBox27 = new Telerik.Reporting.TextBox();
			this.textBox28 = new Telerik.Reporting.TextBox();
			this.textBox29 = new Telerik.Reporting.TextBox();
			this.textBox30 = new Telerik.Reporting.TextBox();
			this.textBox31 = new Telerik.Reporting.TextBox();
			this.textBox32 = new Telerik.Reporting.TextBox();
			this.textBox33 = new Telerik.Reporting.TextBox();
			this.textBox34 = new Telerik.Reporting.TextBox();
			this.textBox35 = new Telerik.Reporting.TextBox();
			this.textBox36 = new Telerik.Reporting.TextBox();
			this.textBox37 = new Telerik.Reporting.TextBox();
			this.textBox38 = new Telerik.Reporting.TextBox();
			this.textBox39 = new Telerik.Reporting.TextBox();
			this.textBox40 = new Telerik.Reporting.TextBox();
			this.textBox41 = new Telerik.Reporting.TextBox();
			this.textBox42 = new Telerik.Reporting.TextBox();
			this.textBox43 = new Telerik.Reporting.TextBox();
			this.textBox44 = new Telerik.Reporting.TextBox();
			this.textBox45 = new Telerik.Reporting.TextBox();
			this.textBox46 = new Telerik.Reporting.TextBox();
			this.textBox47 = new Telerik.Reporting.TextBox();
			this.textBox48 = new Telerik.Reporting.TextBox();
			this.textBox49 = new Telerik.Reporting.TextBox();
			this.textBox50 = new Telerik.Reporting.TextBox();
			this.textBox51 = new Telerik.Reporting.TextBox();
			this.textBox52 = new Telerik.Reporting.TextBox();
			this.textBox53 = new Telerik.Reporting.TextBox();
			this.textBox54 = new Telerik.Reporting.TextBox();
			this.textBox55 = new Telerik.Reporting.TextBox();
			this.textBox56 = new Telerik.Reporting.TextBox();
			this.textBox57 = new Telerik.Reporting.TextBox();
			this.textBox58 = new Telerik.Reporting.TextBox();
			this.textBox59 = new Telerik.Reporting.TextBox();
			this.textBox60 = new Telerik.Reporting.TextBox();
			this.textBox61 = new Telerik.Reporting.TextBox();
			this.textBox62 = new Telerik.Reporting.TextBox();
			this.textBox63 = new Telerik.Reporting.TextBox();
			this.textBox64 = new Telerik.Reporting.TextBox();
			this.textBox65 = new Telerik.Reporting.TextBox();
			this.textBox66 = new Telerik.Reporting.TextBox();
			this.textBox67 = new Telerik.Reporting.TextBox();
			this.textBox68 = new Telerik.Reporting.TextBox();
			this.textBox69 = new Telerik.Reporting.TextBox();
			this.textBox70 = new Telerik.Reporting.TextBox();
			this.textBox71 = new Telerik.Reporting.TextBox();
			this.textBox72 = new Telerik.Reporting.TextBox();
			this.textBox73 = new Telerik.Reporting.TextBox();
			this.textBox74 = new Telerik.Reporting.TextBox();
			this.textBox75 = new Telerik.Reporting.TextBox();
			this.textBox76 = new Telerik.Reporting.TextBox();
			this.textBox77 = new Telerik.Reporting.TextBox();
			this.textBox78 = new Telerik.Reporting.TextBox();
			this.textBox79 = new Telerik.Reporting.TextBox();
			this.textBox80 = new Telerik.Reporting.TextBox();
			this.textBox81 = new Telerik.Reporting.TextBox();
			this.textBox82 = new Telerik.Reporting.TextBox();
			this.textBox83 = new Telerik.Reporting.TextBox();
			this.textBox84 = new Telerik.Reporting.TextBox();
			this.textBox85 = new Telerik.Reporting.TextBox();
			this.textBox86 = new Telerik.Reporting.TextBox();
			this.textBox87 = new Telerik.Reporting.TextBox();
			this.textBox88 = new Telerik.Reporting.TextBox();
			this.textBox89 = new Telerik.Reporting.TextBox();
			this.textBox90 = new Telerik.Reporting.TextBox();
			this.textBox91 = new Telerik.Reporting.TextBox();
			this.textBox92 = new Telerik.Reporting.TextBox();
			this.textBox93 = new Telerik.Reporting.TextBox();
			this.textBox94 = new Telerik.Reporting.TextBox();
			this.textBox95 = new Telerik.Reporting.TextBox();
			this.textBox96 = new Telerik.Reporting.TextBox();
			this.textBox97 = new Telerik.Reporting.TextBox();
			this.textBox98 = new Telerik.Reporting.TextBox();
			this.textBox99 = new Telerik.Reporting.TextBox();
			this.textBox100 = new Telerik.Reporting.TextBox();
			this.textBox101 = new Telerik.Reporting.TextBox();
			this.textBox102 = new Telerik.Reporting.TextBox();
			this.textBox103 = new Telerik.Reporting.TextBox();
			this.textBox104 = new Telerik.Reporting.TextBox();
			this.textBox105 = new Telerik.Reporting.TextBox();
			this.textBox106 = new Telerik.Reporting.TextBox();
			this.textBox107 = new Telerik.Reporting.TextBox();
			this.textBox108 = new Telerik.Reporting.TextBox();
			this.textBox109 = new Telerik.Reporting.TextBox();
			this.textBox110 = new Telerik.Reporting.TextBox();
			this.textBox111 = new Telerik.Reporting.TextBox();
			this.textBox112 = new Telerik.Reporting.TextBox();
			this.textBox113 = new Telerik.Reporting.TextBox();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// detail
			// 
			this.detail.Height = Telerik.Reporting.Drawing.Unit.Mm(594D);
			this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.pictureBox1,
            this.pictureBox2,
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.textBox5,
            this.textBox6,
            this.textBox7,
            this.textBox8,
            this.textBox9,
            this.textBox10,
            this.textBox11,
            this.textBox12,
            this.textBox13,
            this.textBox14,
            this.textBox15,
            this.textBox16,
            this.textBox17,
            this.textBox18,
            this.textBox19,
            this.textBox20,
            this.textBox21,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox27,
            this.textBox28,
            this.textBox29,
            this.textBox30,
            this.textBox31,
            this.textBox32,
            this.textBox33,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox38,
            this.textBox39,
            this.textBox40,
            this.textBox41,
            this.textBox42,
            this.textBox43,
            this.textBox44,
            this.textBox45,
            this.textBox46,
            this.textBox47,
            this.textBox48,
            this.textBox49,
            this.textBox50,
            this.textBox51,
            this.textBox52,
            this.textBox53,
            this.textBox54,
            this.textBox55,
            this.textBox56,
            this.textBox57,
            this.textBox58,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox62,
            this.textBox63,
            this.textBox64,
            this.textBox65,
            this.textBox66,
            this.textBox67,
            this.textBox68,
            this.textBox69,
            this.textBox70,
            this.textBox71,
            this.textBox72,
            this.textBox73,
            this.textBox74,
            this.textBox75,
            this.textBox76,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox80,
            this.textBox81,
            this.textBox82,
            this.textBox83,
            this.textBox84,
            this.textBox85,
            this.textBox86,
            this.textBox87,
            this.textBox88,
            this.textBox89,
            this.textBox90,
            this.textBox91,
            this.textBox92,
            this.textBox93,
            this.textBox94,
            this.textBox95,
            this.textBox96,
            this.textBox97,
            this.textBox98,
            this.textBox99,
            this.textBox100,
            this.textBox101,
            this.textBox102,
            this.textBox103,
            this.textBox104,
            this.textBox105,
            this.textBox106,
            this.textBox107,
            this.textBox108,
            this.textBox109,
            this.textBox110,
            this.textBox111,
            this.textBox112,
            this.textBox113});
			this.detail.Name = "detail";
			// 
			// pictureBox1
			// 
			this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
			this.pictureBox1.MimeType = "image/jpeg";
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
			this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
			// 
			// pictureBox2
			// 
			this.pictureBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(29.7D));
			this.pictureBox2.MimeType = "image/jpeg";
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Mm(210D), Telerik.Reporting.Drawing.Unit.Mm(297D));
			this.pictureBox2.Value = ((object)(resources.GetObject("pictureBox2.Value")));
			// 
			// textBox1
			// 
			this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0D), Telerik.Reporting.Drawing.Unit.Cm(0D));
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(1.1D));
			this.textBox1.Style.Color = System.Drawing.Color.Red;
			this.textBox1.Style.Font.Bold = true;
			this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox1.Value = "NSW";
			// 
			// textBox2
			// 
			this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2D), Telerik.Reporting.Drawing.Unit.Cm(0D));
			this.textBox2.Name = "textBox2";
			this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.7D), Telerik.Reporting.Drawing.Unit.Cm(1.1D));
			this.textBox2.Style.Color = System.Drawing.Color.White;
			this.textBox2.Style.Font.Bold = true;
			this.textBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(15D);
			this.textBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
			this.textBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox2.Value = "Change";
			// 
			// textBox3
			// 
			this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.4D), Telerik.Reporting.Drawing.Unit.Cm(2.226D));
			this.textBox3.Name = "textBox3";
			this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.8D), Telerik.Reporting.Drawing.Unit.Cm(1.3D));
			this.textBox3.Style.Color = System.Drawing.Color.White;
			this.textBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(30D);
			this.textBox3.Value = "Application to change ";
			// 
			// textBox4
			// 
			this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.4D), Telerik.Reporting.Drawing.Unit.Cm(3.559D));
			this.textBox4.Name = "textBox4";
			this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.8D), Telerik.Reporting.Drawing.Unit.Cm(1.3D));
			this.textBox4.Style.Color = System.Drawing.Color.White;
			this.textBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(30D);
			this.textBox4.Value = "an electricity supply\r\n";
			// 
			// textBox5
			// 
			this.textBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.4D), Telerik.Reporting.Drawing.Unit.Cm(6.4D));
			this.textBox5.Name = "textBox5";
			this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox5.Style.Color = System.Drawing.Color.White;
			this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox5.Value = "Use this form to request changes to the electricity supply";
			// 
			// textBox6
			// 
			this.textBox6.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.4D), Telerik.Reporting.Drawing.Unit.Cm(7D));
			this.textBox6.Name = "textBox6";
			this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(12.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox6.Style.Color = System.Drawing.Color.White;
			this.textBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(13D);
			this.textBox6.Value = "for a property in New South Wales.\r\n";
			// 
			// textBox7
			// 
			this.textBox7.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(8.9D));
			this.textBox7.Name = "textBox7";
			this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox7.Value = "Email your completed form to newconnections@originenergy.com.au � just remember t" +
    "o scan both sides. ";
			// 
			// textBox8
			// 
			this.textBox8.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(10.1D));
			this.textBox8.Name = "textBox8";
			this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox8.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox8.Style.Font.Bold = true;
			this.textBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox8.Value = "1. Customer details ";
			// 
			// textBox9
			// 
			this.textBox9.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(10.9D));
			this.textBox9.Name = "textBox9";
			this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox9.Value = "This is the account holder�s details � if you�re not the account holder, ensure y" +
    "ou have their consent to provide ";
			// 
			// textBox10
			// 
			this.textBox10.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(11.5D));
			this.textBox10.Name = "textBox10";
			this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox10.Value = "these details to us. ";
			// 
			// textBox11
			// 
			this.textBox11.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(12.25D));
			this.textBox11.Name = "textBox11";
			this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.7D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox11.Style.Font.Bold = true;
			this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox11.Value = "Full name ";
			// 
			// textBox12
			// 
			this.textBox12.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(13.1D));
			this.textBox12.Name = "textBox12";
			this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox12.Style.Font.Bold = true;
			this.textBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox12.Value = "Phone";
			// 
			// textBox13
			// 
			this.textBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8D), Telerik.Reporting.Drawing.Unit.Cm(13.1D));
			this.textBox13.Name = "textBox13";
			this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox13.Style.Font.Bold = true;
			this.textBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox13.Value = "Email";
			// 
			// textBox14
			// 
			this.textBox14.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.785D), Telerik.Reporting.Drawing.Unit.Cm(12.25D));
			this.textBox14.Name = "textBox14";
			this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox14.Style.Font.Bold = true;
			this.textBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox14.Value = "Company\r\n";
			// 
			// textBox15
			// 
			this.textBox15.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.785D), Telerik.Reporting.Drawing.Unit.Cm(12.75D));
			this.textBox15.Name = "textBox15";
			this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox15.Style.Font.Bold = true;
			this.textBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox15.Value = "If applicable";
			// 
			// textBox16
			// 
			this.textBox16.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(14.3D));
			this.textBox16.Name = "textBox16";
			this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox16.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox16.Style.Font.Bold = true;
			this.textBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox16.Value = "2. Property details ";
			// 
			// textBox17
			// 
			this.textBox17.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(15.1D));
			this.textBox17.Name = "textBox17";
			this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.45D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
			this.textBox17.Style.Font.Bold = true;
			this.textBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox17.Value = "Property type ";
			// 
			// textBox18
			// 
			this.textBox18.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(15.1D));
			this.textBox18.Name = "textBox18";
			this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
			this.textBox18.Style.Font.Bold = false;
			this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox18.Value = "Residential ";
			// 
			// textBox19
			// 
			this.textBox19.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.5D), Telerik.Reporting.Drawing.Unit.Cm(15.1D));
			this.textBox19.Name = "textBox19";
			this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.2D), Telerik.Reporting.Drawing.Unit.Cm(0.474D));
			this.textBox19.Style.Font.Bold = false;
			this.textBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox19.Value = "Commercial ";
			// 
			// textBox20
			// 
			this.textBox20.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(15.8D));
			this.textBox20.Name = "textBox20";
			this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.7D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox20.Style.Font.Bold = true;
			this.textBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox20.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox20.Value = "Lot";
			// 
			// textBox21
			// 
			this.textBox21.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.043D), Telerik.Reporting.Drawing.Unit.Cm(15.8D));
			this.textBox21.Name = "textBox21";
			this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.357D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox21.Style.Font.Bold = true;
			this.textBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox21.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox21.Value = "Level";
			// 
			// textBox22
			// 
			this.textBox22.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.9D), Telerik.Reporting.Drawing.Unit.Cm(15.8D));
			this.textBox22.Name = "textBox22";
			this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.8D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox22.Style.Font.Bold = true;
			this.textBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox22.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox22.Value = "Unit no.";
			// 
			// textBox23
			// 
			this.textBox23.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.2D), Telerik.Reporting.Drawing.Unit.Cm(15.8D));
			this.textBox23.Name = "textBox23";
			this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox23.Style.Font.Bold = true;
			this.textBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox23.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox23.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox23.Value = "Street no.";
			// 
			// textBox24
			// 
			this.textBox24.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.8D), Telerik.Reporting.Drawing.Unit.Cm(15.8D));
			this.textBox24.Name = "textBox24";
			this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.45D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox24.Style.Font.Bold = true;
			this.textBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox24.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox24.Value = "Street";
			// 
			// textBox25
			// 
			this.textBox25.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(16.54D));
			this.textBox25.Name = "textBox25";
			this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox25.Style.Font.Bold = true;
			this.textBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox25.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox25.Value = "Suburb";
			// 
			// textBox26
			// 
			this.textBox26.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.8D), Telerik.Reporting.Drawing.Unit.Cm(16.54D));
			this.textBox26.Name = "textBox26";
			this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox26.Style.Font.Bold = true;
			this.textBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox26.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox26.Value = "State";
			// 
			// textBox27
			// 
			this.textBox27.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.6D), Telerik.Reporting.Drawing.Unit.Cm(16.54D));
			this.textBox27.Name = "textBox27";
			this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox27.Style.Font.Bold = true;
			this.textBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox27.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox27.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox27.Value = "Postcode";
			// 
			// textBox28
			// 
			this.textBox28.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.1D), Telerik.Reporting.Drawing.Unit.Cm(17.3D));
			this.textBox28.Name = "textBox28";
			this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox28.Style.Font.Bold = true;
			this.textBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox28.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox28.Value = "Meter number(s) ";
			// 
			// textBox29
			// 
			this.textBox29.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(17.3D));
			this.textBox29.Name = "textBox29";
			this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.9D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox29.Style.Font.Bold = true;
			this.textBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
			this.textBox29.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(7D);
			this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Right;
			this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox29.Value = "NMI";
			// 
			// textBox30
			// 
			this.textBox30.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.7D), Telerik.Reporting.Drawing.Unit.Cm(17.9D));
			this.textBox30.Name = "textBox30";
			this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.4D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox30.Value = "Providing accurate NMI and meter numbers helps us to change the correct service";
			// 
			// textBox31
			// 
			this.textBox31.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(18.5D));
			this.textBox31.Name = "textBox31";
			this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox31.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox31.Style.Font.Bold = true;
			this.textBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox31.Value = "3. Access details ";
			// 
			// textBox32
			// 
			this.textBox32.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(19.265D));
			this.textBox32.Name = "textBox32";
			this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.9D), Telerik.Reporting.Drawing.Unit.Cm(0.45D));
			this.textBox32.Style.Font.Bold = true;
			this.textBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox32.Value = "Is there clear and safe access to the property? ";
			// 
			// textBox33
			// 
			this.textBox33.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(19.265D));
			this.textBox33.Name = "textBox33";
			this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1D), Telerik.Reporting.Drawing.Unit.Cm(0.45D));
			this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox33.Value = "Yes";
			// 
			// textBox34
			// 
			this.textBox34.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.45D), Telerik.Reporting.Drawing.Unit.Cm(19.265D));
			this.textBox34.Name = "textBox34";
			this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.65D), Telerik.Reporting.Drawing.Unit.Cm(0.45D));
			this.textBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox34.Value = "No";
			// 
			// textBox35
			// 
			this.textBox35.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(20D));
			this.textBox35.Name = "textBox35";
			this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox35.Style.Font.Bold = true;
			this.textBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox35.Value = "If no, what�s preventing access? (eg. locked gate, building debris, unrestrained " +
    "animals on site)";
			// 
			// textBox36
			// 
			this.textBox36.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(21.9D));
			this.textBox36.Name = "textBox36";
			this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.9D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox36.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox36.Style.Font.Bold = true;
			this.textBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox36.Value = "4. Type of alteration (select all that apply)";
			// 
			// textBox37
			// 
			this.textBox37.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(22.8D));
			this.textBox37.Name = "textBox37";
			this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox37.Style.Font.Bold = true;
			this.textBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox37.Value = "Solar";
			// 
			// textBox38
			// 
			this.textBox38.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(22.8D));
			this.textBox38.Name = "textBox38";
			this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox38.Style.Font.Bold = true;
			this.textBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox38.Value = "Phase upgrade ";
			// 
			// textBox39
			// 
			this.textBox39.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.3D), Telerik.Reporting.Drawing.Unit.Cm(22.8D));
			this.textBox39.Name = "textBox39";
			this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.9D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox39.Style.Font.Bold = true;
			this.textBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox39.Value = " Mains upgrade ";
			// 
			// textBox40
			// 
			this.textBox40.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.585D), Telerik.Reporting.Drawing.Unit.Cm(22.8D));
			this.textBox40.Name = "textBox40";
			this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.615D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox40.Style.Font.Bold = true;
			this.textBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox40.Value = "Overhead to underground conversion ";
			// 
			// textBox41
			// 
			this.textBox41.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.6D), Telerik.Reporting.Drawing.Unit.Cm(22.8D));
			this.textBox41.Name = "textBox41";
			this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox41.Style.Font.Bold = true;
			this.textBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox41.Value = " POA relocation";
			// 
			// textBox42
			// 
			this.textBox42.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(23.5D));
			this.textBox42.Name = "textBox42";
			this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox42.Style.Font.Bold = true;
			this.textBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox42.Value = "  Add off peak ";
			// 
			// textBox43
			// 
			this.textBox43.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(24.2D));
			this.textBox43.Name = "textBox43";
			this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox43.Style.Font.Bold = true;
			this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox43.Value = "Other";
			// 
			// textBox44
			// 
			this.textBox44.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.2D), Telerik.Reporting.Drawing.Unit.Cm(23.5D));
			this.textBox44.Name = "textBox44";
			this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.2D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox44.Style.Font.Bold = true;
			this.textBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox44.Value = "Remove off peak ";
			// 
			// textBox45
			// 
			this.textBox45.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.8D), Telerik.Reporting.Drawing.Unit.Cm(23.5D));
			this.textBox45.Name = "textBox45";
			this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox45.Style.Font.Bold = true;
			this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox45.Value = " Relocate to group meter position ";
			// 
			// textBox46
			// 
			this.textBox46.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14D), Telerik.Reporting.Drawing.Unit.Cm(23.5D));
			this.textBox46.Name = "textBox46";
			this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.1D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox46.Style.Font.Bold = true;
			this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox46.Value = " Move meter position";
			// 
			// textBox47
			// 
			this.textBox47.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(25.3D));
			this.textBox47.Name = "textBox47";
			this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox47.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox47.Style.Font.Bold = true;
			this.textBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox47.Value = "5. Coordination of electrical works ";
			// 
			// textBox48
			// 
			this.textBox48.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(26.2D));
			this.textBox48.Name = "textBox48";
			this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.7D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox48.Value = "Do you need to coordinate work with the meter provider?";
			// 
			// textBox49
			// 
			this.textBox49.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.9D), Telerik.Reporting.Drawing.Unit.Cm(26.2D));
			this.textBox49.Name = "textBox49";
			this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox49.Value = "Yes";
			// 
			// textBox50
			// 
			this.textBox50.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.4D), Telerik.Reporting.Drawing.Unit.Cm(26.2D));
			this.textBox50.Name = "textBox50";
			this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox50.Value = "No";
			// 
			// textBox51
			// 
			this.textBox51.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(26.9D));
			this.textBox51.Name = "textBox51";
			this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox51.Value = "If yes, contact name";
			// 
			// textBox52
			// 
			this.textBox52.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.9D), Telerik.Reporting.Drawing.Unit.Cm(26.9D));
			this.textBox52.Name = "textBox52";
			this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox52.Value = "Contact no. ";
			// 
			// textBox53
			// 
			this.textBox53.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(27.64D));
			this.textBox53.Name = "textBox53";
			this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(0.45D));
			this.textBox53.Value = "Preferred appointment date* ";
			// 
			// textBox54
			// 
			this.textBox54.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.2D), Telerik.Reporting.Drawing.Unit.Cm(27.64D));
			this.textBox54.Name = "textBox54";
			this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.26D));
			this.textBox54.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox54.Value = "*This date needs to be atlist 15 business days from";
			// 
			// textBox55
			// 
			this.textBox55.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.2D), Telerik.Reporting.Drawing.Unit.Cm(27.9D));
			this.textBox55.Name = "textBox55";
			this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6D), Telerik.Reporting.Drawing.Unit.Cm(0.3D));
			this.textBox55.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox55.Value = "the date you submit this form to us.";
			// 
			// textBox56
			// 
			this.textBox56.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(30.598D));
			this.textBox56.Name = "textBox56";
			this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox56.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox56.Style.Font.Bold = true;
			this.textBox56.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox56.Value = "6. Supply details ";
			// 
			// textBox57
			// 
			this.textBox57.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(31.3D));
			this.textBox57.Name = "textBox57";
			this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox57.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox57.Value = "Supply type ";
			// 
			// textBox58
			// 
			this.textBox58.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.5D), Telerik.Reporting.Drawing.Unit.Cm(31.3D));
			this.textBox58.Name = "textBox58";
			this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox58.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox58.Value = " Overhead ";
			// 
			// textBox59
			// 
			this.textBox59.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(5.9D), Telerik.Reporting.Drawing.Unit.Cm(31.3D));
			this.textBox59.Name = "textBox59";
			this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox59.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox59.Value = " Underground ";
			// 
			// textBox60
			// 
			this.textBox60.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.15D), Telerik.Reporting.Drawing.Unit.Cm(31.3D));
			this.textBox60.Name = "textBox60";
			this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(6.25D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox60.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox60.Value = "Overhead or underground conversion ";
			// 
			// textBox61
			// 
			this.textBox61.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(32D));
			this.textBox61.Name = "textBox61";
			this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.65D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox61.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox61.Value = "Supply phases ";
			// 
			// textBox62
			// 
			this.textBox62.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(3.9D), Telerik.Reporting.Drawing.Unit.Cm(32D));
			this.textBox62.Name = "textBox62";
			this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.6D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox62.Value = "1";
			// 
			// textBox63
			// 
			this.textBox63.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(6.05D), Telerik.Reporting.Drawing.Unit.Cm(32D));
			this.textBox63.Name = "textBox63";
			this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.35D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox63.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox63.Value = "3";
			// 
			// textBox64
			// 
			this.textBox64.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(32.7D));
			this.textBox64.Name = "textBox64";
			this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox64.Value = "Maximum demand at the site ";
			// 
			// textBox65
			// 
			this.textBox65.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(7.1D), Telerik.Reporting.Drawing.Unit.Cm(32.7D));
			this.textBox65.Name = "textBox65";
			this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox65.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox65.Value = "Amps ";
			// 
			// textBox66
			// 
			this.textBox66.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.585D), Telerik.Reporting.Drawing.Unit.Cm(32.7D));
			this.textBox66.Name = "textBox66";
			this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.315D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox66.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox66.Value = "Is temporary isolation required? ";
			// 
			// textBox67
			// 
			this.textBox67.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.3D), Telerik.Reporting.Drawing.Unit.Cm(32.7D));
			this.textBox67.Name = "textBox67";
			this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.05D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox67.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox67.Value = "Yes";
			// 
			// textBox68
			// 
			this.textBox68.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(16.8D), Telerik.Reporting.Drawing.Unit.Cm(32.7D));
			this.textBox68.Name = "textBox68";
			this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox68.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox68.Value = "No";
			// 
			// textBox69
			// 
			this.textBox69.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(4.9D), Telerik.Reporting.Drawing.Unit.Cm(32D));
			this.textBox69.Name = "textBox69";
			this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox69.Value = "2";
			// 
			// textBox70
			// 
			this.textBox70.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.585D), Telerik.Reporting.Drawing.Unit.Cm(33.1D));
			this.textBox70.Name = "textBox70";
			this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.315D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox70.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(5D);
			this.textBox70.Value = "If yes, ensure you complete section 5.";
			// 
			// textBox71
			// 
			this.textBox71.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(33.8D));
			this.textBox71.Name = "textBox71";
			this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(7.7D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox71.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox71.Style.Font.Bold = true;
			this.textBox71.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox71.Value = "7. Meter details ";
			// 
			// textBox72
			// 
			this.textBox72.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(34.78D));
			this.textBox72.Name = "textBox72";
			this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox72.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox72.Value = "Metering phases required   ";
			// 
			// textBox73
			// 
			this.textBox73.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(35.5D));
			this.textBox73.Name = "textBox73";
			this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox73.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox73.Value = "Off-peak installations required (Optional) ";
			// 
			// textBox74
			// 
			this.textBox74.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(36.2D));
			this.textBox74.Name = "textBox74";
			this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox74.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox74.Value = "Does site have Current Transformer (CT) metering? ";
			// 
			// textBox75
			// 
			this.textBox75.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(36.9D));
			this.textBox75.Name = "textBox75";
			this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(8.7D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox75.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox75.Value = "Do you require CT metering to be installed?";
			// 
			// textBox76
			// 
			this.textBox76.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9D), Telerik.Reporting.Drawing.Unit.Cm(34.78D));
			this.textBox76.Name = "textBox76";
			this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.7D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox76.Value = "1";
			// 
			// textBox77
			// 
			this.textBox77.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(10.9D), Telerik.Reporting.Drawing.Unit.Cm(34.78D));
			this.textBox77.Name = "textBox77";
			this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox77.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox77.Value = "2";
			// 
			// textBox78
			// 
			this.textBox78.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.1D), Telerik.Reporting.Drawing.Unit.Cm(34.78D));
			this.textBox78.Name = "textBox78";
			this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(0.7D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox78.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox78.Value = "3";
			// 
			// textBox79
			// 
			this.textBox79.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.226D), Telerik.Reporting.Drawing.Unit.Cm(34.78D));
			this.textBox79.Name = "textBox79";
			this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox79.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox79.Value = " Amps per phase ";
			// 
			// textBox80
			// 
			this.textBox80.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9D), Telerik.Reporting.Drawing.Unit.Cm(35.5D));
			this.textBox80.Name = "textBox80";
			this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.1D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox80.Value = "Hot water ";
			// 
			// textBox81
			// 
			this.textBox81.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.4D), Telerik.Reporting.Drawing.Unit.Cm(35.5D));
			this.textBox81.Name = "textBox81";
			this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.7D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox81.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox81.Value = "Floor heating ";
			// 
			// textBox82
			// 
			this.textBox82.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(15.5D), Telerik.Reporting.Drawing.Unit.Cm(35.5D));
			this.textBox82.Name = "textBox82";
			this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.4D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox82.Value = "  Other ";
			// 
			// textBox83
			// 
			this.textBox83.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9D), Telerik.Reporting.Drawing.Unit.Cm(36.2D));
			this.textBox83.Name = "textBox83";
			this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox83.Value = "Yes";
			// 
			// textBox84
			// 
			this.textBox84.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(9.9D), Telerik.Reporting.Drawing.Unit.Cm(36.9D));
			this.textBox84.Name = "textBox84";
			this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox84.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox84.Value = "Yes";
			// 
			// textBox85
			// 
			this.textBox85.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.4D), Telerik.Reporting.Drawing.Unit.Cm(36.2D));
			this.textBox85.Name = "textBox85";
			this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox85.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox85.Value = "No";
			// 
			// textBox86
			// 
			this.textBox86.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.4D), Telerik.Reporting.Drawing.Unit.Cm(36.9D));
			this.textBox86.Name = "textBox86";
			this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(0.35D));
			this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox86.Value = "No";
			// 
			// textBox87
			// 
			this.textBox87.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(37.8D));
			this.textBox87.Name = "textBox87";
			this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(11.6D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox87.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox87.Style.Font.Bold = true;
			this.textBox87.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox87.Value = "Complete only if solar selected in section 4 ";
			// 
			// textBox88
			// 
			this.textBox88.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(38.6D));
			this.textBox88.Name = "textBox88";
			this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(5.2D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox88.Value = "Date solar panels were installed ";
			// 
			// textBox89
			// 
			this.textBox89.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(8.4D), Telerik.Reporting.Drawing.Unit.Cm(38.6D));
			this.textBox89.Name = "textBox89";
			this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(3.15D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox89.Value = "Solar system size ";
			// 
			// textBox90
			// 
			this.textBox90.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(14.043D), Telerik.Reporting.Drawing.Unit.Cm(38.6D));
			this.textBox90.Name = "textBox90";
			this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.6D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox90.Value = "Network ref no. ";
			// 
			// textBox91
			// 
			this.textBox91.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.043D), Telerik.Reporting.Drawing.Unit.Cm(38.6D));
			this.textBox91.Name = "textBox91";
			this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox91.Value = " kW ";
			// 
			// textBox92
			// 
			this.textBox92.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(39.892D));
			this.textBox92.Name = "textBox92";
			this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox92.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox92.Style.Font.Bold = true;
			this.textBox92.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox92.Value = "8. Electrician�s details ";
			// 
			// textBox93
			// 
			this.textBox93.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(40.609D));
			this.textBox93.Name = "textBox93";
			this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.75D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox93.Value = "Full name";
			// 
			// textBox94
			// 
			this.textBox94.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(41.4D));
			this.textBox94.Name = "textBox94";
			this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.1D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox94.Value = "Email";
			// 
			// textBox95
			// 
			this.textBox95.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(13.7D), Telerik.Reporting.Drawing.Unit.Cm(40.7D));
			this.textBox95.Name = "textBox95";
			this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox95.Value = "Phone";
			// 
			// textBox96
			// 
			this.textBox96.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(12.8D), Telerik.Reporting.Drawing.Unit.Cm(41.4D));
			this.textBox96.Name = "textBox96";
			this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.4D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox96.Value = " Licence no. ";
			// 
			// textBox97
			// 
			this.textBox97.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(42.4D));
			this.textBox97.Name = "textBox97";
			this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox97.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox97.Style.Font.Bold = true;
			this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(11D);
			this.textBox97.Value = "Fees, process and timings ";
			// 
			// textBox98
			// 
			this.textBox98.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(43.267D));
			this.textBox98.Name = "textBox98";
			this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.3D), Telerik.Reporting.Drawing.Unit.Cm(0.433D));
			this.textBox98.Style.Font.Bold = true;
			this.textBox98.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox98.Value = "Meter alteration fees ";
			// 
			// textBox99
			// 
			this.textBox99.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(43.908D));
			this.textBox99.Name = "textBox99";
			this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox99.Value = "Origin does not currently charge fees for changing a meter in NSW. However, if th" +
    "at has changed by the time we receive ";
			// 
			// textBox100
			// 
			this.textBox100.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(44.308D));
			this.textBox100.Name = "textBox100";
			this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox100.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox100.Value = "your completed form, we�ll contact you to let you know the fees that apply. You c" +
    "an find more details about the current ";
			// 
			// textBox101
			// 
			this.textBox101.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(44.708D));
			this.textBox101.Name = "textBox101";
			this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox101.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox101.Value = "fees at originenergy.com.au/meterfees. ";
			// 
			// textBox102
			// 
			this.textBox102.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(45.4D));
			this.textBox102.Name = "textBox102";
			this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox102.Style.Font.Bold = true;
			this.textBox102.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox102.Value = "Access to the property ";
			// 
			// textBox103
			// 
			this.textBox103.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(46.2D));
			this.textBox103.Name = "textBox103";
			this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox103.Value = "To get your work done, you�ll need to give us clear and safe access to the proper" +
    "ty and the meter.  ";
			// 
			// textBox104
			// 
			this.textBox104.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(46.6D));
			this.textBox104.Name = "textBox104";
			this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox104.Value = "Just so you know, if we don�t have this, we�ll need to reschedule the work for an" +
    "other day and fees may apply. ";
			// 
			// textBox105
			// 
			this.textBox105.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(47.3D));
			this.textBox105.Name = "textBox105";
			this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(9.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox105.Style.Font.Bold = true;
			this.textBox105.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox105.Value = "Process and timings ";
			// 
			// textBox106
			// 
			this.textBox106.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(48.1D));
			this.textBox106.Name = "textBox106";
			this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.4D));
			this.textBox106.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(10D);
			this.textBox106.Value = "For information about what happens next and when it�ll happen, head to originener" +
    "gy.com.au/meters.\r\n";
			// 
			// textBox107
			// 
			this.textBox107.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(49.25D));
			this.textBox107.Name = "textBox107";
			this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(0.45D));
			this.textBox107.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox107.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox107.Value = "Applicant name ";
			// 
			// textBox108
			// 
			this.textBox108.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(11.15D), Telerik.Reporting.Drawing.Unit.Cm(49.25D));
			this.textBox108.Name = "textBox108";
			this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(1.25D), Telerik.Reporting.Drawing.Unit.Cm(0.45D));
			this.textBox108.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox108.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox108.Value = " Date ";
			// 
			// textBox109
			// 
			this.textBox109.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(51D));
			this.textBox109.Name = "textBox109";
			this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(2.8D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox109.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
			this.textBox109.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
			this.textBox109.Value = "Signature";
			// 
			// textBox110
			// 
			this.textBox110.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(55D));
			this.textBox110.Name = "textBox110";
			this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox110.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox110.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox110.Value = "To find out more about how Origin collects, uses, holds and discloses personal an" +
    "d credit ";
			// 
			// textBox111
			// 
			this.textBox111.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(55.6D));
			this.textBox111.Name = "textBox111";
			this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.6D));
			this.textBox111.Style.Color = System.Drawing.Color.RoyalBlue;
			this.textBox111.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
			this.textBox111.Value = "information about individuals, see our privacy statements at originenergy.com.au/" +
    "privacy";
			// 
			// textBox112
			// 
			this.textBox112.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(58D));
			this.textBox112.Name = "textBox112";
			this.textBox112.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox112.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox112.Value = "Origin Energy Electricity Limited ABN 33 071 052 287, Origin Energy Retail Limite" +
    "d ABN 22 078 868 425, Origin Energy (Vic) Pty Limited ABN 11 086 013 283, ";
			// 
			// textBox113
			// 
			this.textBox113.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Cm(0.8D), Telerik.Reporting.Drawing.Unit.Cm(58.5D));
			this.textBox113.Name = "textBox113";
			this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.3D), Telerik.Reporting.Drawing.Unit.Cm(0.5D));
			this.textBox113.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6D);
			this.textBox113.Value = "Origin Energy LPG Limited ABN 77 000 508 369 100 Waymouth Street, Adelaide SA 500" +
    "0 � Telephone 13 24 63 � Facsimile 1800 132 463 � originenergy.com.au ";
			// 
			// NSWOrigin
			// 
			this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
			this.Name = "NSWOrigine";
			this.PageSettings.ContinuousPaper = false;
			this.PageSettings.Landscape = false;
			this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D), Telerik.Reporting.Drawing.Unit.Mm(0D));
			this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
			styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
			styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
			styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
			this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
			this.Width = Telerik.Reporting.Drawing.Unit.Mm(210D);
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }
        #endregion
        private Telerik.Reporting.DetailSection detail;
		private Telerik.Reporting.PictureBox pictureBox1;
		private Telerik.Reporting.PictureBox pictureBox2;
		private Telerik.Reporting.TextBox textBox1;
		private Telerik.Reporting.TextBox textBox2;
		private Telerik.Reporting.TextBox textBox3;
		private Telerik.Reporting.TextBox textBox4;
		private Telerik.Reporting.TextBox textBox5;
		private Telerik.Reporting.TextBox textBox6;
		private Telerik.Reporting.TextBox textBox7;
		private Telerik.Reporting.TextBox textBox8;
		private Telerik.Reporting.TextBox textBox9;
		private Telerik.Reporting.TextBox textBox10;
		private Telerik.Reporting.TextBox textBox11;
		private Telerik.Reporting.TextBox textBox12;
		private Telerik.Reporting.TextBox textBox13;
		private Telerik.Reporting.TextBox textBox14;
		private Telerik.Reporting.TextBox textBox15;
		private Telerik.Reporting.TextBox textBox16;
		private Telerik.Reporting.TextBox textBox17;
		private Telerik.Reporting.TextBox textBox18;
		private Telerik.Reporting.TextBox textBox19;
		private Telerik.Reporting.TextBox textBox20;
		private Telerik.Reporting.TextBox textBox21;
		private Telerik.Reporting.TextBox textBox22;
		private Telerik.Reporting.TextBox textBox23;
		private Telerik.Reporting.TextBox textBox24;
		private Telerik.Reporting.TextBox textBox25;
		private Telerik.Reporting.TextBox textBox26;
		private Telerik.Reporting.TextBox textBox27;
		private Telerik.Reporting.TextBox textBox28;
		private Telerik.Reporting.TextBox textBox29;
		private Telerik.Reporting.TextBox textBox30;
		private Telerik.Reporting.TextBox textBox31;
		private Telerik.Reporting.TextBox textBox32;
		private Telerik.Reporting.TextBox textBox33;
		private Telerik.Reporting.TextBox textBox34;
		private Telerik.Reporting.TextBox textBox35;
		private Telerik.Reporting.TextBox textBox36;
		private Telerik.Reporting.TextBox textBox37;
		private Telerik.Reporting.TextBox textBox38;
		private Telerik.Reporting.TextBox textBox39;
		private Telerik.Reporting.TextBox textBox40;
		private Telerik.Reporting.TextBox textBox41;
		private Telerik.Reporting.TextBox textBox42;
		private Telerik.Reporting.TextBox textBox43;
		private Telerik.Reporting.TextBox textBox44;
		private Telerik.Reporting.TextBox textBox45;
		private Telerik.Reporting.TextBox textBox46;
		private Telerik.Reporting.TextBox textBox47;
		private Telerik.Reporting.TextBox textBox48;
		private Telerik.Reporting.TextBox textBox49;
		private Telerik.Reporting.TextBox textBox50;
		private Telerik.Reporting.TextBox textBox51;
		private Telerik.Reporting.TextBox textBox52;
		private Telerik.Reporting.TextBox textBox53;
		private Telerik.Reporting.TextBox textBox54;
		private Telerik.Reporting.TextBox textBox55;
		private Telerik.Reporting.TextBox textBox56;
		private Telerik.Reporting.TextBox textBox57;
		private Telerik.Reporting.TextBox textBox58;
		private Telerik.Reporting.TextBox textBox59;
		private Telerik.Reporting.TextBox textBox60;
		private Telerik.Reporting.TextBox textBox61;
		private Telerik.Reporting.TextBox textBox62;
		private Telerik.Reporting.TextBox textBox63;
		private Telerik.Reporting.TextBox textBox64;
		private Telerik.Reporting.TextBox textBox65;
		private Telerik.Reporting.TextBox textBox66;
		private Telerik.Reporting.TextBox textBox67;
		private Telerik.Reporting.TextBox textBox68;
		private Telerik.Reporting.TextBox textBox69;
		private Telerik.Reporting.TextBox textBox70;
		private Telerik.Reporting.TextBox textBox71;
		private Telerik.Reporting.TextBox textBox72;
		private Telerik.Reporting.TextBox textBox73;
		private Telerik.Reporting.TextBox textBox74;
		private Telerik.Reporting.TextBox textBox75;
		private Telerik.Reporting.TextBox textBox76;
		private Telerik.Reporting.TextBox textBox77;
		private Telerik.Reporting.TextBox textBox78;
		private Telerik.Reporting.TextBox textBox79;
		private Telerik.Reporting.TextBox textBox80;
		private Telerik.Reporting.TextBox textBox81;
		private Telerik.Reporting.TextBox textBox82;
		private Telerik.Reporting.TextBox textBox83;
		private Telerik.Reporting.TextBox textBox84;
		private Telerik.Reporting.TextBox textBox85;
		private Telerik.Reporting.TextBox textBox86;
		private Telerik.Reporting.TextBox textBox87;
		private Telerik.Reporting.TextBox textBox88;
		private Telerik.Reporting.TextBox textBox89;
		private Telerik.Reporting.TextBox textBox90;
		private Telerik.Reporting.TextBox textBox91;
		private Telerik.Reporting.TextBox textBox92;
		private Telerik.Reporting.TextBox textBox93;
		private Telerik.Reporting.TextBox textBox94;
		private Telerik.Reporting.TextBox textBox95;
		private Telerik.Reporting.TextBox textBox96;
		private Telerik.Reporting.TextBox textBox97;
		private Telerik.Reporting.TextBox textBox98;
		private Telerik.Reporting.TextBox textBox99;
		private Telerik.Reporting.TextBox textBox100;
		private Telerik.Reporting.TextBox textBox101;
		private Telerik.Reporting.TextBox textBox102;
		private Telerik.Reporting.TextBox textBox103;
		private Telerik.Reporting.TextBox textBox104;
		private Telerik.Reporting.TextBox textBox105;
		private Telerik.Reporting.TextBox textBox106;
		private Telerik.Reporting.TextBox textBox107;
		private Telerik.Reporting.TextBox textBox108;
		private Telerik.Reporting.TextBox textBox109;
		private Telerik.Reporting.TextBox textBox110;
		private Telerik.Reporting.TextBox textBox111;
		private Telerik.Reporting.TextBox textBox112;
		private Telerik.Reporting.TextBox textBox113;
	}
}