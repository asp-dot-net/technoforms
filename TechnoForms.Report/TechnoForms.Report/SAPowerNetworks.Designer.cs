namespace TechnoForms.Report
{
	partial class SAPowerNetworks
	{
		#region Component Designer generated code
		/// <summary>
		/// Required method for telerik Reporting designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            Telerik.Reporting.TableGroup tableGroup1 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup2 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup3 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup4 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup5 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup6 = new Telerik.Reporting.TableGroup();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SAPowerNetworks));
            Telerik.Reporting.NavigateToUrlAction navigateToUrlAction1 = new Telerik.Reporting.NavigateToUrlAction();
            Telerik.Reporting.TableGroup tableGroup7 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup8 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup9 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup10 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup11 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup12 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup178 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup179 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup180 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup181 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup182 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup183 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup184 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup185 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup186 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup187 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup188 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup189 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup190 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup13 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup14 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup15 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup16 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup17 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup18 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup19 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup20 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup21 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup22 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup23 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup24 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup25 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup26 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup27 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup28 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup29 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup44 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup45 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup46 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup47 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup48 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup49 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup50 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup51 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup52 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup53 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup54 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup55 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup56 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup57 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup58 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup30 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup31 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup32 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup33 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup34 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup35 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup36 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup37 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup38 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup39 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup40 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup41 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup42 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup43 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup59 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup60 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup61 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup62 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup63 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup64 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup65 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup66 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup67 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup68 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup76 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup77 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup78 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup79 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup80 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup81 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup82 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup83 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup84 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup85 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup86 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup69 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup70 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup71 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup72 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup73 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup74 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup75 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup106 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup107 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup108 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup109 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup110 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup111 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup112 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup113 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup87 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup88 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup89 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup90 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup91 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup92 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup93 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup94 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup95 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup96 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup97 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup98 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup99 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup100 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup101 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup102 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup103 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup104 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup105 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup163 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup164 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup165 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup166 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup167 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup168 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup169 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup170 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup171 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup172 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup173 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup114 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup115 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup116 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup117 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup118 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup119 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup120 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup121 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup122 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup123 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup124 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup125 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup126 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup127 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup128 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup129 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup130 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup136 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup137 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup138 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup139 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup140 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup141 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup142 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup131 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup132 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup133 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup134 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup135 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup146 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup147 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup148 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup149 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup150 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup151 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup143 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup144 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup145 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup158 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup159 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup160 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup161 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup162 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup152 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup153 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup154 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup155 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup156 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup157 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup174 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup175 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup176 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.TableGroup tableGroup177 = new Telerik.Reporting.TableGroup();
            Telerik.Reporting.Drawing.StyleRule styleRule1 = new Telerik.Reporting.Drawing.StyleRule();
            this.textBox6 = new Telerik.Reporting.TextBox();
            this.table4 = new Telerik.Reporting.Table();
            this.checkBox76 = new Telerik.Reporting.CheckBox();
            this.checkBox5 = new Telerik.Reporting.CheckBox();
            this.checkBox6 = new Telerik.Reporting.CheckBox();
            this.checkBox7 = new Telerik.Reporting.CheckBox();
            this.checkBox8 = new Telerik.Reporting.CheckBox();
            this.textBox9 = new Telerik.Reporting.TextBox();
            this.textBox29 = new Telerik.Reporting.TextBox();
            this.textBox27 = new Telerik.Reporting.TextBox();
            this.textBox41 = new Telerik.Reporting.TextBox();
            this.textBox33 = new Telerik.Reporting.TextBox();
            this.textBox62 = new Telerik.Reporting.TextBox();
            this.textBox66 = new Telerik.Reporting.TextBox();
            this.textBox68 = new Telerik.Reporting.TextBox();
            this.textBox71 = new Telerik.Reporting.TextBox();
            this.textBox73 = new Telerik.Reporting.TextBox();
            this.textBox84 = new Telerik.Reporting.TextBox();
            this.textBox101 = new Telerik.Reporting.TextBox();
            this.textBox104 = new Telerik.Reporting.TextBox();
            this.detail = new Telerik.Reporting.DetailSection();
            this.panel1 = new Telerik.Reporting.Panel();
            this.textBox1 = new Telerik.Reporting.TextBox();
            this.textBox2 = new Telerik.Reporting.TextBox();
            this.textBox3 = new Telerik.Reporting.TextBox();
            this.textBox4 = new Telerik.Reporting.TextBox();
            this.checkBox13 = new Telerik.Reporting.CheckBox();
            this.table1 = new Telerik.Reporting.Table();
            this.textBox13 = new Telerik.Reporting.TextBox();
            this.checkBox43 = new Telerik.Reporting.CheckBox();
            this.checkBox1 = new Telerik.Reporting.CheckBox();
            this.checkBox2 = new Telerik.Reporting.CheckBox();
            this.checkBox3 = new Telerik.Reporting.CheckBox();
            this.checkBox4 = new Telerik.Reporting.CheckBox();
            this.pictureBox1 = new Telerik.Reporting.PictureBox();
            this.table2 = new Telerik.Reporting.Table();
            this.textBox5 = new Telerik.Reporting.TextBox();
            this.table3 = new Telerik.Reporting.Table();
            this.textBox7 = new Telerik.Reporting.TextBox();
            this.textBox10 = new Telerik.Reporting.TextBox();
            this.textBox12 = new Telerik.Reporting.TextBox();
            this.textBox22 = new Telerik.Reporting.TextBox();
            this.textBox23 = new Telerik.Reporting.TextBox();
            this.textBox24 = new Telerik.Reporting.TextBox();
            this.textBox25 = new Telerik.Reporting.TextBox();
            this.textBox30 = new Telerik.Reporting.TextBox();
            this.textBox34 = new Telerik.Reporting.TextBox();
            this.textBox35 = new Telerik.Reporting.TextBox();
            this.textBox36 = new Telerik.Reporting.TextBox();
            this.textBox58 = new Telerik.Reporting.TextBox();
            this.textBox59 = new Telerik.Reporting.TextBox();
            this.textBox60 = new Telerik.Reporting.TextBox();
            this.textBox14 = new Telerik.Reporting.TextBox();
            this.textBox20 = new Telerik.Reporting.TextBox();
            this.textBox26 = new Telerik.Reporting.TextBox();
            this.textBox28 = new Telerik.Reporting.TextBox();
            this.textBox31 = new Telerik.Reporting.TextBox();
            this.textBox37 = new Telerik.Reporting.TextBox();
            this.textBox61 = new Telerik.Reporting.TextBox();
            this.textBox47 = new Telerik.Reporting.TextBox();
            this.textBox50 = new Telerik.Reporting.TextBox();
            this.textBox42 = new Telerik.Reporting.TextBox();
            this.textBox39 = new Telerik.Reporting.TextBox();
            this.textBox44 = new Telerik.Reporting.TextBox();
            this.textBox52 = new Telerik.Reporting.TextBox();
            this.textBox11 = new Telerik.Reporting.TextBox();
            this.table5 = new Telerik.Reporting.Table();
            this.textBox43 = new Telerik.Reporting.TextBox();
            this.textBox45 = new Telerik.Reporting.TextBox();
            this.textBox46 = new Telerik.Reporting.TextBox();
            this.textBox48 = new Telerik.Reporting.TextBox();
            this.textBox49 = new Telerik.Reporting.TextBox();
            this.textBox51 = new Telerik.Reporting.TextBox();
            this.table6 = new Telerik.Reporting.Table();
            this.textBox17 = new Telerik.Reporting.TextBox();
            this.textBox53 = new Telerik.Reporting.TextBox();
            this.textBox54 = new Telerik.Reporting.TextBox();
            this.textBox56 = new Telerik.Reporting.TextBox();
            this.textBox15 = new Telerik.Reporting.TextBox();
            this.textBox16 = new Telerik.Reporting.TextBox();
            this.textBox19 = new Telerik.Reporting.TextBox();
            this.textBox55 = new Telerik.Reporting.TextBox();
            this.htmlTextBox1 = new Telerik.Reporting.HtmlTextBox();
            this.textBox8 = new Telerik.Reporting.TextBox();
            this.textBox18 = new Telerik.Reporting.TextBox();
            this.table7 = new Telerik.Reporting.Table();
            this.textBox21 = new Telerik.Reporting.TextBox();
            this.textBox32 = new Telerik.Reporting.TextBox();
            this.textBox38 = new Telerik.Reporting.TextBox();
            this.textBox40 = new Telerik.Reporting.TextBox();
            this.textBox57 = new Telerik.Reporting.TextBox();
            this.textBox64 = new Telerik.Reporting.TextBox();
            this.table8 = new Telerik.Reporting.Table();
            this.textBox65 = new Telerik.Reporting.TextBox();
            this.textBox67 = new Telerik.Reporting.TextBox();
            this.htmlTextBox2 = new Telerik.Reporting.HtmlTextBox();
            this.htmlTextBox3 = new Telerik.Reporting.HtmlTextBox();
            this.textBox69 = new Telerik.Reporting.TextBox();
            this.table9 = new Telerik.Reporting.Table();
            this.textBox70 = new Telerik.Reporting.TextBox();
            this.textBox72 = new Telerik.Reporting.TextBox();
            this.textBox74 = new Telerik.Reporting.TextBox();
            this.textBox63 = new Telerik.Reporting.TextBox();
            this.textBox75 = new Telerik.Reporting.TextBox();
            this.textBox76 = new Telerik.Reporting.TextBox();
            this.textBox77 = new Telerik.Reporting.TextBox();
            this.textBox78 = new Telerik.Reporting.TextBox();
            this.textBox79 = new Telerik.Reporting.TextBox();
            this.textBox82 = new Telerik.Reporting.TextBox();
            this.table10 = new Telerik.Reporting.Table();
            this.textBox83 = new Telerik.Reporting.TextBox();
            this.textBox85 = new Telerik.Reporting.TextBox();
            this.textBox87 = new Telerik.Reporting.TextBox();
            this.textBox90 = new Telerik.Reporting.TextBox();
            this.textBox92 = new Telerik.Reporting.TextBox();
            this.textBox81 = new Telerik.Reporting.TextBox();
            this.textBox94 = new Telerik.Reporting.TextBox();
            this.textBox95 = new Telerik.Reporting.TextBox();
            this.table11 = new Telerik.Reporting.Table();
            this.checkBox10 = new Telerik.Reporting.CheckBox();
            this.checkBox11 = new Telerik.Reporting.CheckBox();
            this.checkBox12 = new Telerik.Reporting.CheckBox();
            this.checkBox14 = new Telerik.Reporting.CheckBox();
            this.textBox86 = new Telerik.Reporting.TextBox();
            this.textBox88 = new Telerik.Reporting.TextBox();
            this.textBox80 = new Telerik.Reporting.TextBox();
            this.textBox91 = new Telerik.Reporting.TextBox();
            this.table12 = new Telerik.Reporting.Table();
            this.textBox93 = new Telerik.Reporting.TextBox();
            this.textBox96 = new Telerik.Reporting.TextBox();
            this.table13 = new Telerik.Reporting.Table();
            this.checkBox22 = new Telerik.Reporting.CheckBox();
            this.checkBox23 = new Telerik.Reporting.CheckBox();
            this.table14 = new Telerik.Reporting.Table();
            this.checkBox9 = new Telerik.Reporting.CheckBox();
            this.checkBox15 = new Telerik.Reporting.CheckBox();
            this.table15 = new Telerik.Reporting.Table();
            this.checkBox16 = new Telerik.Reporting.CheckBox();
            this.checkBox18 = new Telerik.Reporting.CheckBox();
            this.checkBox19 = new Telerik.Reporting.CheckBox();
            this.table16 = new Telerik.Reporting.Table();
            this.checkBox17 = new Telerik.Reporting.CheckBox();
            this.checkBox20 = new Telerik.Reporting.CheckBox();
            this.checkBox21 = new Telerik.Reporting.CheckBox();
            this.checkBox24 = new Telerik.Reporting.CheckBox();
            this.textBox97 = new Telerik.Reporting.TextBox();
            this.checkBox25 = new Telerik.Reporting.CheckBox();
            this.checkBox26 = new Telerik.Reporting.CheckBox();
            this.textBox99 = new Telerik.Reporting.TextBox();
            this.table17 = new Telerik.Reporting.Table();
            this.textBox89 = new Telerik.Reporting.TextBox();
            this.textBox100 = new Telerik.Reporting.TextBox();
            this.table18 = new Telerik.Reporting.Table();
            this.checkBox27 = new Telerik.Reporting.CheckBox();
            this.checkBox28 = new Telerik.Reporting.CheckBox();
            this.table19 = new Telerik.Reporting.Table();
            this.checkBox29 = new Telerik.Reporting.CheckBox();
            this.checkBox30 = new Telerik.Reporting.CheckBox();
            this.table20 = new Telerik.Reporting.Table();
            this.checkBox31 = new Telerik.Reporting.CheckBox();
            this.checkBox32 = new Telerik.Reporting.CheckBox();
            this.checkBox33 = new Telerik.Reporting.CheckBox();
            this.table21 = new Telerik.Reporting.Table();
            this.checkBox34 = new Telerik.Reporting.CheckBox();
            this.checkBox35 = new Telerik.Reporting.CheckBox();
            this.checkBox36 = new Telerik.Reporting.CheckBox();
            this.checkBox37 = new Telerik.Reporting.CheckBox();
            this.textBox102 = new Telerik.Reporting.TextBox();
            this.table22 = new Telerik.Reporting.Table();
            this.checkBox38 = new Telerik.Reporting.CheckBox();
            this.checkBox39 = new Telerik.Reporting.CheckBox();
            this.checkBox40 = new Telerik.Reporting.CheckBox();
            this.checkBox41 = new Telerik.Reporting.CheckBox();
            this.textBox103 = new Telerik.Reporting.TextBox();
            this.table23 = new Telerik.Reporting.Table();
            this.textBox98 = new Telerik.Reporting.TextBox();
            this.table24 = new Telerik.Reporting.Table();
            this.textBox106 = new Telerik.Reporting.TextBox();
            this.textBox111 = new Telerik.Reporting.TextBox();
            this.textBox105 = new Telerik.Reporting.TextBox();
            this.textBox107 = new Telerik.Reporting.TextBox();
            this.table25 = new Telerik.Reporting.Table();
            this.checkBox42 = new Telerik.Reporting.CheckBox();
            this.checkBox44 = new Telerik.Reporting.CheckBox();
            this.table26 = new Telerik.Reporting.Table();
            this.textBox108 = new Telerik.Reporting.TextBox();
            this.textBox113 = new Telerik.Reporting.TextBox();
            this.table27 = new Telerik.Reporting.Table();
            this.checkBox45 = new Telerik.Reporting.CheckBox();
            this.checkBox46 = new Telerik.Reporting.CheckBox();
            this.table28 = new Telerik.Reporting.Table();
            this.checkBox47 = new Telerik.Reporting.CheckBox();
            this.checkBox48 = new Telerik.Reporting.CheckBox();
            this.table29 = new Telerik.Reporting.Table();
            this.textBox116 = new Telerik.Reporting.TextBox();
            this.htmlTextBox4 = new Telerik.Reporting.HtmlTextBox();
            this.textBox109 = new Telerik.Reporting.TextBox();
            this.textBox110 = new Telerik.Reporting.TextBox();
            this.htmlTextBox5 = new Telerik.Reporting.HtmlTextBox();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // textBox6
            // 
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.177D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
            this.textBox6.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox6.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox6.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox6.Value = "Installation Type:";
            // 
            // table4
            // 
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.314D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.314D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.314D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.314D)));
            this.table4.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.081D)));
            this.table4.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.236D)));
            this.table4.Body.SetCellContent(0, 0, this.checkBox76);
            this.table4.Body.SetCellContent(0, 1, this.checkBox5);
            this.table4.Body.SetCellContent(0, 2, this.checkBox6);
            this.table4.Body.SetCellContent(0, 3, this.checkBox7);
            this.table4.Body.SetCellContent(0, 4, this.checkBox8);
            tableGroup1.Name = "tableGroup8";
            tableGroup2.Name = "tableGroup9";
            tableGroup3.Name = "tableGroup10";
            tableGroup4.Name = "group15";
            tableGroup5.Name = "group16";
            this.table4.ColumnGroups.Add(tableGroup1);
            this.table4.ColumnGroups.Add(tableGroup2);
            this.table4.ColumnGroups.Add(tableGroup3);
            this.table4.ColumnGroups.Add(tableGroup4);
            this.table4.ColumnGroups.Add(tableGroup5);
            this.table4.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox76,
            this.checkBox5,
            this.checkBox6,
            this.checkBox7,
            this.checkBox8});
            this.table4.Name = "table4";
            tableGroup6.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup6.Name = "detailTableGroup3";
            this.table4.RowGroups.Add(tableGroup6);
            this.table4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6.337D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
            this.table4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table4.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table4.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table4.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table4.StyleName = "";
            // 
            // checkBox76
            // 
            this.checkBox76.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox76.CheckedImage = ((object)(resources.GetObject("checkBox76.CheckedImage")));
            this.checkBox76.IndeterminateImage = ((object)(resources.GetObject("checkBox76.IndeterminateImage")));
            this.checkBox76.Name = "checkBox76";
            this.checkBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.314D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
            this.checkBox76.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox76.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox76.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.3D);
            this.checkBox76.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox76.StyleName = "";
            this.checkBox76.Text = "Rural";
            this.checkBox76.UncheckedImage = ((object)(resources.GetObject("checkBox76.UncheckedImage")));
            this.checkBox76.Value = "";
            // 
            // checkBox5
            // 
            this.checkBox5.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox5.CheckedImage = ((object)(resources.GetObject("checkBox5.CheckedImage")));
            this.checkBox5.IndeterminateImage = ((object)(resources.GetObject("checkBox5.IndeterminateImage")));
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.314D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
            this.checkBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox5.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox5.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.3D);
            this.checkBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox5.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox5.StyleName = "";
            this.checkBox5.Text = "Commercial";
            this.checkBox5.UncheckedImage = ((object)(resources.GetObject("checkBox5.UncheckedImage")));
            this.checkBox5.Value = "";
            // 
            // checkBox6
            // 
            this.checkBox6.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox6.CheckedImage = ((object)(resources.GetObject("checkBox6.CheckedImage")));
            this.checkBox6.IndeterminateImage = ((object)(resources.GetObject("checkBox6.IndeterminateImage")));
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.314D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
            this.checkBox6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox6.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox6.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.3D);
            this.checkBox6.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox6.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox6.StyleName = "";
            this.checkBox6.Text = "Industrial";
            this.checkBox6.UncheckedImage = ((object)(resources.GetObject("checkBox6.UncheckedImage")));
            this.checkBox6.Value = "";
            // 
            // checkBox7
            // 
            this.checkBox7.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox7.CheckedImage = ((object)(resources.GetObject("checkBox7.CheckedImage")));
            this.checkBox7.IndeterminateImage = ((object)(resources.GetObject("checkBox7.IndeterminateImage")));
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.314D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
            this.checkBox7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox7.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox7.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.3D);
            this.checkBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox7.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox7.StyleName = "";
            this.checkBox7.Text = "Domestic";
            this.checkBox7.UncheckedImage = ((object)(resources.GetObject("checkBox7.UncheckedImage")));
            this.checkBox7.Value = "";
            // 
            // checkBox8
            // 
            this.checkBox8.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox8.CheckedImage = ((object)(resources.GetObject("checkBox8.CheckedImage")));
            this.checkBox8.IndeterminateImage = ((object)(resources.GetObject("checkBox8.IndeterminateImage")));
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.081D), Telerik.Reporting.Drawing.Unit.Inch(0.236D));
            this.checkBox8.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox8.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.3D);
            this.checkBox8.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox8.StyleName = "";
            this.checkBox8.Text = "Temp Supply";
            this.checkBox8.UncheckedImage = ((object)(resources.GetObject("checkBox8.UncheckedImage")));
            this.checkBox8.Value = "";
            // 
            // textBox9
            // 
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.469D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox9.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox9.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox9.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox9.Value = "Unit";
            // 
            // textBox29
            // 
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.644D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox29.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox29.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox29.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox29.StyleName = "";
            this.textBox29.Value = "St. No.";
            // 
            // textBox27
            // 
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.033D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox27.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox27.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox27.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox27.Value = "Street Name / P.O. Box";
            // 
            // textBox41
            // 
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.017D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox41.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox41.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox41.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox41.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox41.StyleName = "";
            this.textBox41.Value = "Suburb / Town";
            // 
            // textBox33
            // 
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.349D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox33.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox33.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox33.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox33.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox33.Value = "Postcode";
            // 
            // textBox62
            // 
            this.textBox62.Name = "textBox62";
            this.textBox62.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.511D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            this.textBox62.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox62.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox62.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox62.Value = "Date of application";
            // 
            // textBox66
            // 
            this.textBox66.Name = "textBox66";
            this.textBox66.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.002D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            this.textBox66.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox66.Style.Font.Bold = true;
            this.textBox66.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox66.Value = "Customerís or Authorised Agentís Signature:";
            // 
            // textBox68
            // 
            this.textBox68.Name = "textBox68";
            this.textBox68.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.175D));
            this.textBox68.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox68.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox68.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox68.Value = "Electrical Contractors Licence Number";
            // 
            // textBox71
            // 
            this.textBox71.Name = "textBox71";
            this.textBox71.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.175D));
            this.textBox71.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox71.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox71.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox71.Value = "Electrician\'s Business Name";
            // 
            // textBox73
            // 
            this.textBox73.Name = "textBox73";
            this.textBox73.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.175D));
            this.textBox73.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox73.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox73.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox73.Value = "Contact Name";
            // 
            // textBox84
            // 
            this.textBox84.Name = "textBox84";
            this.textBox84.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.464D), Telerik.Reporting.Drawing.Unit.Inch(0.164D));
            this.textBox84.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox84.Value = "Service Phasing";
            // 
            // textBox101
            // 
            this.textBox101.Name = "textBox101";
            this.textBox101.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.518D), Telerik.Reporting.Drawing.Unit.Inch(0.193D));
            this.textBox101.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox101.Value = "Meter Phasing";
            // 
            // textBox104
            // 
            this.textBox104.Name = "textBox104";
            this.textBox104.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.971D), Telerik.Reporting.Drawing.Unit.Inch(0.15D));
            this.textBox104.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox104.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox104.Value = "PV SEG Approval Reference No";
            // 
            // detail
            // 
            this.detail.Height = Telerik.Reporting.Drawing.Unit.Cm(28.448D);
            this.detail.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.panel1,
            this.table2,
            this.htmlTextBox5});
            this.detail.Name = "detail";
            this.detail.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.detail.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.detail.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Cm(0D);
            this.detail.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Cm(0D);
            // 
            // panel1
            // 
            this.panel1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox1,
            this.textBox2,
            this.textBox3,
            this.textBox4,
            this.checkBox13,
            this.table1,
            this.pictureBox1});
            this.panel1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.panel1.Name = "panel1";
            this.panel1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.826D), Telerik.Reporting.Drawing.Unit.Inch(1.74D));
            // 
            // textBox1
            // 
            this.textBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(6D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
            this.textBox1.Style.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.textBox1.Style.Font.Bold = true;
            this.textBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(12D);
            this.textBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox1.Value = "Form A: Please send this form to your retailer";
            // 
            // textBox2
            // 
            this.textBox2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.4D));
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.2D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox2.Style.Font.Bold = true;
            this.textBox2.Value = "NOTE: SA Power Networks fees may apply for connection work requested";
            // 
            // textBox3
            // 
            this.textBox3.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.6D));
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.4D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox3.Value = "Visit the Negotiating Framework page";
            // 
            // textBox4
            // 
            navigateToUrlAction1.Url = "http://www.sapowernetworks.com.au/";
            this.textBox4.Action = navigateToUrlAction1;
            this.textBox4.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(2.4D), Telerik.Reporting.Drawing.Unit.Inch(0.6D));
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.7D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox4.Value = "http://www.sapowernetworks.com.au/";
            // 
            // checkBox13
            // 
            this.checkBox13.CheckAlign = System.Drawing.ContentAlignment.TopLeft;
            this.checkBox13.CheckedImage = ((object)(resources.GetObject("checkBox13.CheckedImage")));
            this.checkBox13.IndeterminateImage = ((object)(resources.GetObject("checkBox13.IndeterminateImage")));
            this.checkBox13.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(0.96D));
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Cm(19.878D), Telerik.Reporting.Drawing.Unit.Cm(0.839D));
            this.checkBox13.Style.Font.Name = "Microsoft Sans Serif";
            this.checkBox13.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox13.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.checkBox13.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.checkBox13.StyleName = "";
            this.checkBox13.Text = resources.GetString("checkBox13.Text");
            this.checkBox13.UncheckedImage = ((object)(resources.GetObject("checkBox13.UncheckedImage")));
            this.checkBox13.Value = "";
            // 
            // table1
            // 
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.531D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.667D)));
            this.table1.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.615D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.24D)));
            this.table1.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table1.Body.SetCellContent(1, 2, this.textBox13);
            this.table1.Body.SetCellContent(0, 0, this.checkBox43);
            this.table1.Body.SetCellContent(0, 1, this.checkBox1);
            this.table1.Body.SetCellContent(0, 2, this.checkBox2);
            this.table1.Body.SetCellContent(1, 0, this.checkBox3);
            this.table1.Body.SetCellContent(1, 1, this.checkBox4);
            tableGroup7.Name = "tableGroup";
            tableGroup8.Name = "tableGroup1";
            tableGroup9.Name = "tableGroup2";
            this.table1.ColumnGroups.Add(tableGroup7);
            this.table1.ColumnGroups.Add(tableGroup8);
            this.table1.ColumnGroups.Add(tableGroup9);
            this.table1.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox43,
            this.checkBox1,
            this.checkBox2,
            this.checkBox3,
            this.checkBox4,
            this.textBox13});
            this.table1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.25D));
            this.table1.Name = "table1";
            tableGroup11.Name = "group";
            tableGroup12.Name = "group2";
            tableGroup10.ChildGroups.Add(tableGroup11);
            tableGroup10.ChildGroups.Add(tableGroup12);
            tableGroup10.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup10.Name = "detailTableGroup";
            this.table1.RowGroups.Add(tableGroup10);
            this.table1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.813D), Telerik.Reporting.Drawing.Unit.Inch(0.44D));
            // 
            // textBox13
            // 
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.615D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox13.StyleName = "";
            // 
            // checkBox43
            // 
            this.checkBox43.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox43.CheckedImage = ((object)(resources.GetObject("checkBox43.CheckedImage")));
            this.checkBox43.IndeterminateImage = ((object)(resources.GetObject("checkBox43.IndeterminateImage")));
            this.checkBox43.Name = "checkBox43";
            this.checkBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.531D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox43.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox43.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.7D);
            this.checkBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox43.StyleName = "";
            this.checkBox43.Text = "New Electricity Supply";
            this.checkBox43.UncheckedImage = ((object)(resources.GetObject("checkBox43.UncheckedImage")));
            this.checkBox43.Value = "";
            // 
            // checkBox1
            // 
            this.checkBox1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox1.CheckedImage = ((object)(resources.GetObject("checkBox1.CheckedImage")));
            this.checkBox1.IndeterminateImage = ((object)(resources.GetObject("checkBox1.IndeterminateImage")));
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.667D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox1.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.7D);
            this.checkBox1.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox1.StyleName = "";
            this.checkBox1.Text = "Alteration of Service / Meter";
            this.checkBox1.UncheckedImage = ((object)(resources.GetObject("checkBox1.UncheckedImage")));
            this.checkBox1.Value = "";
            // 
            // checkBox2
            // 
            this.checkBox2.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox2.CheckedImage = ((object)(resources.GetObject("checkBox2.CheckedImage")));
            this.checkBox2.IndeterminateImage = ((object)(resources.GetObject("checkBox2.IndeterminateImage")));
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.615D), Telerik.Reporting.Drawing.Unit.Inch(0.24D));
            this.checkBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox2.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox2.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.5D);
            this.checkBox2.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox2.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox2.StyleName = "";
            this.checkBox2.Text = "Public/CLER Lighting (Connect Only)";
            this.checkBox2.UncheckedImage = ((object)(resources.GetObject("checkBox2.UncheckedImage")));
            this.checkBox2.Value = "";
            // 
            // checkBox3
            // 
            this.checkBox3.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox3.CheckedImage = ((object)(resources.GetObject("checkBox3.CheckedImage")));
            this.checkBox3.IndeterminateImage = ((object)(resources.GetObject("checkBox3.IndeterminateImage")));
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.531D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox3.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox3.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.7D);
            this.checkBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox3.StyleName = "";
            this.checkBox3.Text = "PV Metering Installation";
            this.checkBox3.UncheckedImage = ((object)(resources.GetObject("checkBox3.UncheckedImage")));
            this.checkBox3.Value = "";
            // 
            // checkBox4
            // 
            this.checkBox4.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox4.CheckedImage = ((object)(resources.GetObject("checkBox4.CheckedImage")));
            this.checkBox4.IndeterminateImage = ((object)(resources.GetObject("checkBox4.IndeterminateImage")));
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.667D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox4.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox4.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.7D);
            this.checkBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.checkBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox4.StyleName = "";
            this.checkBox4.Text = "Abolishment of Electricity Supply";
            this.checkBox4.UncheckedImage = ((object)(resources.GetObject("checkBox4.UncheckedImage")));
            this.checkBox4.Value = "";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(6.129D), Telerik.Reporting.Drawing.Unit.Inch(0D));
            this.pictureBox1.MimeType = "image/png";
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.626D), Telerik.Reporting.Drawing.Unit.Inch(0.95D));
            this.pictureBox1.Sizing = Telerik.Reporting.Drawing.ImageSizeMode.Center;
            this.pictureBox1.Value = ((object)(resources.GetObject("pictureBox1.Value")));
            // 
            // table2
            // 
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.344D)));
            this.table2.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(7.512D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(1.397D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(1.301D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(1.714D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.763D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.95D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.658D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(1.315D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.179D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.158D)));
            this.table2.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.408D)));
            this.table2.Body.SetCellContent(0, 0, this.textBox5);
            this.table2.Body.SetCellContent(0, 1, this.table3);
            this.table2.Body.SetCellContent(1, 0, this.textBox11);
            this.table2.Body.SetCellContent(1, 1, this.table5);
            this.table2.Body.SetCellContent(2, 0, this.textBox64);
            this.table2.Body.SetCellContent(2, 1, this.table8);
            this.table2.Body.SetCellContent(3, 0, this.htmlTextBox3, 1, 2);
            this.table2.Body.SetCellContent(4, 0, this.textBox69);
            this.table2.Body.SetCellContent(4, 1, this.table9);
            this.table2.Body.SetCellContent(5, 0, this.textBox82);
            this.table2.Body.SetCellContent(5, 1, this.table10);
            this.table2.Body.SetCellContent(6, 0, this.textBox91);
            this.table2.Body.SetCellContent(6, 1, this.table12);
            this.table2.Body.SetCellContent(7, 0, this.textBox99);
            this.table2.Body.SetCellContent(7, 1, this.table17);
            this.table2.Body.SetCellContent(8, 0, this.table29, 1, 2);
            this.table2.Body.SetCellContent(9, 0, this.textBox109, 1, 2);
            this.table2.Body.SetCellContent(10, 0, this.textBox110, 1, 2);
            tableGroup178.Name = "tableGroup3";
            tableGroup179.Name = "tableGroup4";
            this.table2.ColumnGroups.Add(tableGroup178);
            this.table2.ColumnGroups.Add(tableGroup179);
            this.table2.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox5,
            this.table3,
            this.textBox11,
            this.table5,
            this.textBox64,
            this.table8,
            this.htmlTextBox3,
            this.textBox69,
            this.table9,
            this.textBox82,
            this.table10,
            this.textBox91,
            this.table12,
            this.textBox99,
            this.table17,
            this.table29,
            this.textBox109,
            this.textBox110});
            this.table2.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(1.7D));
            this.table2.Name = "table2";
            tableGroup180.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup180.Name = "detailTableGroup1";
            tableGroup181.Name = "group17";
            tableGroup182.Name = "group35";
            tableGroup183.Name = "group37";
            tableGroup184.Name = "group38";
            tableGroup185.Name = "group41";
            tableGroup186.Name = "group50";
            tableGroup187.Name = "group57";
            tableGroup188.Name = "group61";
            tableGroup189.Name = "group65";
            tableGroup190.Name = "group66";
            this.table2.RowGroups.Add(tableGroup180);
            this.table2.RowGroups.Add(tableGroup181);
            this.table2.RowGroups.Add(tableGroup182);
            this.table2.RowGroups.Add(tableGroup183);
            this.table2.RowGroups.Add(tableGroup184);
            this.table2.RowGroups.Add(tableGroup185);
            this.table2.RowGroups.Add(tableGroup186);
            this.table2.RowGroups.Add(tableGroup187);
            this.table2.RowGroups.Add(tableGroup188);
            this.table2.RowGroups.Add(tableGroup189);
            this.table2.RowGroups.Add(tableGroup190);
            this.table2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.856D), Telerik.Reporting.Drawing.Unit.Inch(9.043D));
            this.table2.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            // 
            // textBox5
            // 
            this.textBox5.Angle = 270D;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.344D), Telerik.Reporting.Drawing.Unit.Inch(1.397D));
            this.textBox5.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox5.Style.Font.Bold = true;
            this.textBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox5.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox5.StyleName = "";
            this.textBox5.Value = "Section1                     Installtion Details";
            // 
            // table3
            // 
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.62D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.557D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.546D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.28D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.231D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.109D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.966D)));
            this.table3.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.203D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.206D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.191D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.191D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.191D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.191D)));
            this.table3.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.191D)));
            this.table3.Body.SetCellContent(0, 0, this.textBox7);
            this.table3.Body.SetCellContent(0, 1, this.textBox10);
            this.table3.Body.SetCellContent(0, 2, this.textBox12);
            this.table3.Body.SetCellContent(0, 7, this.textBox22);
            this.table3.Body.SetCellContent(1, 0, this.textBox23);
            this.table3.Body.SetCellContent(1, 1, this.textBox24);
            this.table3.Body.SetCellContent(1, 2, this.textBox25);
            this.table3.Body.SetCellContent(1, 7, this.textBox30);
            this.table3.Body.SetCellContent(2, 3, this.textBox34);
            this.table3.Body.SetCellContent(2, 4, this.textBox35);
            this.table3.Body.SetCellContent(2, 5, this.textBox36);
            this.table3.Body.SetCellContent(3, 3, this.textBox58);
            this.table3.Body.SetCellContent(3, 4, this.textBox59);
            this.table3.Body.SetCellContent(3, 5, this.textBox60);
            this.table3.Body.SetCellContent(0, 3, this.textBox14, 1, 2);
            this.table3.Body.SetCellContent(0, 5, this.textBox20, 1, 2);
            this.table3.Body.SetCellContent(1, 3, this.textBox26, 1, 2);
            this.table3.Body.SetCellContent(1, 5, this.textBox28, 1, 2);
            this.table3.Body.SetCellContent(2, 0, this.textBox31, 2, 3);
            this.table3.Body.SetCellContent(2, 6, this.textBox37, 1, 2);
            this.table3.Body.SetCellContent(3, 6, this.textBox61, 1, 2);
            this.table3.Body.SetCellContent(4, 0, this.textBox47, 1, 3);
            this.table3.Body.SetCellContent(4, 3, this.textBox50, 1, 2);
            this.table3.Body.SetCellContent(5, 3, this.textBox42, 1, 2);
            this.table3.Body.SetCellContent(5, 0, this.textBox39, 1, 3);
            this.table3.Body.SetCellContent(5, 5, this.textBox44, 1, 3);
            this.table3.Body.SetCellContent(4, 5, this.textBox52, 1, 3);
            tableGroup14.Name = "group13";
            tableGroup15.Name = "tableGroup6";
            tableGroup13.ChildGroups.Add(tableGroup14);
            tableGroup13.ChildGroups.Add(tableGroup15);
            tableGroup13.Name = "tableGroup5";
            tableGroup13.ReportItem = this.textBox6;
            tableGroup17.Name = "group14";
            tableGroup18.Name = "group1";
            tableGroup19.Name = "group3";
            tableGroup20.Name = "group5";
            tableGroup21.Name = "group4";
            tableGroup22.Name = "group6";
            tableGroup16.ChildGroups.Add(tableGroup17);
            tableGroup16.ChildGroups.Add(tableGroup18);
            tableGroup16.ChildGroups.Add(tableGroup19);
            tableGroup16.ChildGroups.Add(tableGroup20);
            tableGroup16.ChildGroups.Add(tableGroup21);
            tableGroup16.ChildGroups.Add(tableGroup22);
            tableGroup16.Name = "tableGroup7";
            tableGroup16.ReportItem = this.table4;
            this.table3.ColumnGroups.Add(tableGroup13);
            this.table3.ColumnGroups.Add(tableGroup16);
            this.table3.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox7,
            this.textBox10,
            this.textBox12,
            this.textBox14,
            this.textBox20,
            this.textBox22,
            this.textBox23,
            this.textBox24,
            this.textBox25,
            this.textBox26,
            this.textBox28,
            this.textBox30,
            this.textBox31,
            this.textBox34,
            this.textBox35,
            this.textBox36,
            this.textBox37,
            this.textBox58,
            this.textBox59,
            this.textBox60,
            this.textBox61,
            this.textBox47,
            this.textBox50,
            this.textBox52,
            this.textBox39,
            this.textBox42,
            this.textBox44,
            this.textBox6,
            this.table4});
            this.table3.Name = "table3";
            tableGroup24.Name = "group7";
            tableGroup25.Name = "group8";
            tableGroup26.Name = "group9";
            tableGroup27.Name = "group12";
            tableGroup28.Name = "group11";
            tableGroup29.Name = "group10";
            tableGroup23.ChildGroups.Add(tableGroup24);
            tableGroup23.ChildGroups.Add(tableGroup25);
            tableGroup23.ChildGroups.Add(tableGroup26);
            tableGroup23.ChildGroups.Add(tableGroup27);
            tableGroup23.ChildGroups.Add(tableGroup28);
            tableGroup23.ChildGroups.Add(tableGroup29);
            tableGroup23.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup23.Name = "detailTableGroup2";
            this.table3.RowGroups.Add(tableGroup23);
            this.table3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(1.397D));
            this.table3.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table3.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.table3.StyleName = "";
            // 
            // textBox7
            // 
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.62D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            this.textBox7.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox7.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox7.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox7.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox7.Value = "Unit";
            // 
            // textBox10
            // 
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.557D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            this.textBox10.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox10.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox10.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox10.Value = "Lot";
            // 
            // textBox12
            // 
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.546D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            this.textBox12.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox12.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox12.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox12.Value = "St. No.";
            // 
            // textBox22
            // 
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.203D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            this.textBox22.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox22.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox22.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Top;
            this.textBox22.StyleName = "";
            this.textBox22.Value = "Postcode";
            // 
            // textBox23
            // 
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.62D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox23.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox23.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox23.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox23.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox23.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox23.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox23.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox23.StyleName = "";
            // 
            // textBox24
            // 
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.557D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox24.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox24.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox24.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox24.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox24.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox24.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox24.StyleName = "";
            // 
            // textBox25
            // 
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.546D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox25.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox25.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox25.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox25.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox25.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox25.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox25.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox25.StyleName = "";
            // 
            // textBox30
            // 
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.203D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox30.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox30.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox30.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox30.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox30.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox30.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox30.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox30.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox30.StyleName = "";
            // 
            // textBox34
            // 
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.28D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox34.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox34.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox34.StyleName = "";
            this.textBox34.Value = "Transformer No.";
            // 
            // textBox35
            // 
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.231D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox35.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox35.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox35.StyleName = "";
            this.textBox35.Value = "Feeder No.";
            // 
            // textBox36
            // 
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.109D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox36.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox36.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox36.StyleName = "";
            this.textBox36.Value = "Section Number";
            // 
            // textBox58
            // 
            this.textBox58.Name = "textBox58";
            this.textBox58.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.28D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox58.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox58.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox58.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox58.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox58.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox58.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox58.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox58.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox58.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox58.StyleName = "";
            // 
            // textBox59
            // 
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.231D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox59.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox59.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox59.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox59.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox59.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox59.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox59.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox59.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox59.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox59.StyleName = "";
            // 
            // textBox60
            // 
            this.textBox60.Name = "textBox60";
            this.textBox60.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.109D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox60.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox60.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox60.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox60.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox60.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox60.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox60.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox60.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox60.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox60.StyleName = "";
            // 
            // textBox14
            // 
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.511D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            this.textBox14.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox14.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox14.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox14.StyleName = "";
            this.textBox14.Value = "Street Name";
            // 
            // textBox20
            // 
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.075D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            this.textBox20.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox20.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox20.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox20.StyleName = "";
            this.textBox20.Value = "Suburb/Town";
            // 
            // textBox26
            // 
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.511D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox26.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox26.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox26.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox26.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox26.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox26.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox26.StyleName = "";
            // 
            // textBox28
            // 
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.075D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox28.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox28.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox28.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox28.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox28.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox28.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox28.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox28.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox28.StyleName = "";
            // 
            // textBox31
            // 
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.723D), Telerik.Reporting.Drawing.Unit.Inch(0.382D));
            this.textBox31.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox31.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox31.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox31.Style.Font.Italic = true;
            this.textBox31.StyleName = "";
            this.textBox31.Value = "If installation is in a fringe\r\nor rural area, please also\r\ndescribe location by " +
    "=>";
            // 
            // textBox37
            // 
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.169D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox37.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox37.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox37.StyleName = "";
            this.textBox37.Value = "Hundred of";
            // 
            // textBox61
            // 
            this.textBox61.Name = "textBox61";
            this.textBox61.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.169D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox61.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox61.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox61.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox61.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox61.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox61.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox61.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox61.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox61.StyleName = "";
            // 
            // textBox47
            // 
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.723D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox47.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox47.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox47.StyleName = "";
            this.textBox47.Value = "Nominated Retailer";
            // 
            // textBox50
            // 
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.511D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox50.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox50.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox50.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox50.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox50.StyleName = "";
            this.textBox50.Value = "Meter Number (if existing installation)";
            // 
            // textBox42
            // 
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.511D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox42.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox42.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox42.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox42.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox42.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox42.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox42.StyleName = "";
            // 
            // textBox39
            // 
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.723D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox39.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox39.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox39.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox39.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox39.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox39.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox39.StyleName = "";
            // 
            // textBox44
            // 
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.278D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox44.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox44.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox44.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox44.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox44.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox44.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox44.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox44.StyleName = "";
            // 
            // textBox52
            // 
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.278D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox52.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox52.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox52.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox52.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox52.StyleName = "";
            this.textBox52.Value = "National Market Identifier (NMI Number if known)";
            // 
            // textBox11
            // 
            this.textBox11.Angle = 270D;
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.344D), Telerik.Reporting.Drawing.Unit.Inch(1.301D));
            this.textBox11.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox11.Style.Font.Bold = true;
            this.textBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox11.StyleName = "";
            this.textBox11.Value = "Section 2                     Customer Details";
            // 
            // table5
            // 
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.49D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.025D)));
            this.table5.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.997D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.274D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.114D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.191D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.191D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.191D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.149D)));
            this.table5.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.191D)));
            this.table5.Body.SetCellContent(5, 0, this.textBox43);
            this.table5.Body.SetCellContent(5, 1, this.textBox45);
            this.table5.Body.SetCellContent(5, 2, this.textBox46);
            this.table5.Body.SetCellContent(6, 0, this.textBox48);
            this.table5.Body.SetCellContent(6, 1, this.textBox49);
            this.table5.Body.SetCellContent(6, 2, this.textBox51);
            this.table5.Body.SetCellContent(0, 0, this.table6, 2, 3);
            this.table5.Body.SetCellContent(2, 0, this.textBox18, 1, 3);
            this.table5.Body.SetCellContent(3, 0, this.table7, 2, 3);
            tableGroup44.Name = "tableGroup11";
            tableGroup45.Name = "tableGroup12";
            tableGroup46.Name = "tableGroup13";
            this.table5.ColumnGroups.Add(tableGroup44);
            this.table5.ColumnGroups.Add(tableGroup45);
            this.table5.ColumnGroups.Add(tableGroup46);
            this.table5.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.table6,
            this.textBox18,
            this.table7,
            this.textBox43,
            this.textBox45,
            this.textBox46,
            this.textBox48,
            this.textBox49,
            this.textBox51});
            this.table5.Name = "table5";
            tableGroup48.Name = "group18";
            tableGroup49.Name = "group19";
            tableGroup50.Name = "group20";
            tableGroup47.ChildGroups.Add(tableGroup48);
            tableGroup47.ChildGroups.Add(tableGroup49);
            tableGroup47.ChildGroups.Add(tableGroup50);
            tableGroup47.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup47.Name = "detailTableGroup4";
            tableGroup52.Name = "group22";
            tableGroup51.ChildGroups.Add(tableGroup52);
            tableGroup51.Name = "group21";
            tableGroup54.Name = "group24";
            tableGroup53.ChildGroups.Add(tableGroup54);
            tableGroup53.Name = "group23";
            tableGroup56.Name = "group26";
            tableGroup55.ChildGroups.Add(tableGroup56);
            tableGroup55.Name = "group25";
            tableGroup58.Name = "group28";
            tableGroup57.ChildGroups.Add(tableGroup58);
            tableGroup57.Name = "group27";
            this.table5.RowGroups.Add(tableGroup47);
            this.table5.RowGroups.Add(tableGroup51);
            this.table5.RowGroups.Add(tableGroup53);
            this.table5.RowGroups.Add(tableGroup55);
            this.table5.RowGroups.Add(tableGroup57);
            this.table5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(1.301D));
            this.table5.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table5.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table5.StyleName = "";
            // 
            // textBox43
            // 
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.49D), Telerik.Reporting.Drawing.Unit.Inch(0.149D));
            this.textBox43.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox43.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox43.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox43.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox43.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox43.StyleName = "";
            this.textBox43.Value = "Business Hours Telephone";
            // 
            // textBox45
            // 
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.025D), Telerik.Reporting.Drawing.Unit.Inch(0.149D));
            this.textBox45.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox45.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox45.StyleName = "";
            this.textBox45.Value = "Email address (Mandatory)";
            // 
            // textBox46
            // 
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.997D), Telerik.Reporting.Drawing.Unit.Inch(0.149D));
            this.textBox46.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox46.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox46.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox46.StyleName = "";
            this.textBox46.Value = "Mobile Telephone (Mandatory)";
            // 
            // textBox48
            // 
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.49D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox48.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox48.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox48.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox48.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox48.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox48.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox48.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox48.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox48.StyleName = "";
            // 
            // textBox49
            // 
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.025D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox49.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox49.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox49.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox49.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox49.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox49.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox49.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox49.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox49.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox49.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox49.StyleName = "";
            // 
            // textBox51
            // 
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.997D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox51.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox51.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox51.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox51.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox51.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox51.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox51.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox51.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox51.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox51.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox51.StyleName = "";
            // 
            // table6
            // 
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.428D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.45D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.355D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.961D)));
            this.table6.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.318D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.194D)));
            this.table6.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.194D)));
            this.table6.Body.SetCellContent(0, 1, this.textBox17);
            this.table6.Body.SetCellContent(0, 2, this.textBox53);
            this.table6.Body.SetCellContent(0, 4, this.textBox54);
            this.table6.Body.SetCellContent(0, 3, this.textBox56);
            this.table6.Body.SetCellContent(1, 1, this.textBox15);
            this.table6.Body.SetCellContent(1, 2, this.textBox16);
            this.table6.Body.SetCellContent(1, 3, this.textBox19);
            this.table6.Body.SetCellContent(1, 4, this.textBox55);
            this.table6.Body.SetCellContent(0, 0, this.htmlTextBox1);
            this.table6.Body.SetCellContent(1, 0, this.textBox8);
            tableGroup30.Name = "tableGroup14";
            tableGroup31.Name = "tableGroup15";
            tableGroup32.Name = "tableGroup16";
            tableGroup33.Name = "group30";
            tableGroup34.Name = "group29";
            this.table6.ColumnGroups.Add(tableGroup30);
            this.table6.ColumnGroups.Add(tableGroup31);
            this.table6.ColumnGroups.Add(tableGroup32);
            this.table6.ColumnGroups.Add(tableGroup33);
            this.table6.ColumnGroups.Add(tableGroup34);
            this.table6.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox1,
            this.textBox17,
            this.textBox53,
            this.textBox56,
            this.textBox54,
            this.textBox8,
            this.textBox15,
            this.textBox16,
            this.textBox19,
            this.textBox55});
            this.table6.Name = "table6";
            tableGroup36.Name = "group31";
            tableGroup37.Name = "group32";
            tableGroup35.ChildGroups.Add(tableGroup36);
            tableGroup35.ChildGroups.Add(tableGroup37);
            tableGroup35.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup35.Name = "detailTableGroup5";
            this.table6.RowGroups.Add(tableGroup35);
            this.table6.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(0.388D));
            this.table6.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.table6.StyleName = "";
            // 
            // textBox17
            // 
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.45D), Telerik.Reporting.Drawing.Unit.Inch(0.194D));
            this.textBox17.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox17.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox17.Value = "Surname";
            // 
            // textBox53
            // 
            this.textBox53.Name = "textBox53";
            this.textBox53.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.355D), Telerik.Reporting.Drawing.Unit.Inch(0.194D));
            this.textBox53.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox53.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox53.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox53.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox53.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox53.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox53.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox53.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox53.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox54
            // 
            this.textBox54.Name = "textBox54";
            this.textBox54.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.318D), Telerik.Reporting.Drawing.Unit.Inch(0.194D));
            this.textBox54.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox54.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox54.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox54.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox54.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox54.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox54.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox54.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox54.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox54.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox54.StyleName = "";
            // 
            // textBox56
            // 
            this.textBox56.Name = "textBox56";
            this.textBox56.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.961D), Telerik.Reporting.Drawing.Unit.Inch(0.194D));
            this.textBox56.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox56.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox56.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox56.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox56.StyleName = "";
            this.textBox56.Value = "Given Name";
            // 
            // textBox15
            // 
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.45D), Telerik.Reporting.Drawing.Unit.Inch(0.194D));
            this.textBox15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox15.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox15.StyleName = "";
            this.textBox15.Value = "Business Name";
            // 
            // textBox16
            // 
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.355D), Telerik.Reporting.Drawing.Unit.Inch(0.194D));
            this.textBox16.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox16.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox16.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox16.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox16.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox16.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox16.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox16.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox16.StyleName = "";
            // 
            // textBox19
            // 
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.961D), Telerik.Reporting.Drawing.Unit.Inch(0.194D));
            this.textBox19.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox19.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox19.StyleName = "";
            this.textBox19.Value = "ABN No.";
            // 
            // textBox55
            // 
            this.textBox55.Name = "textBox55";
            this.textBox55.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.318D), Telerik.Reporting.Drawing.Unit.Inch(0.194D));
            this.textBox55.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox55.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox55.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox55.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.None;
            this.textBox55.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox55.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox55.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox55.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox55.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox55.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox55.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox55.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox55.StyleName = "";
            // 
            // htmlTextBox1
            // 
            this.htmlTextBox1.Name = "htmlTextBox1";
            this.htmlTextBox1.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.428D), Telerik.Reporting.Drawing.Unit.Inch(0.194D));
            this.htmlTextBox1.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.htmlTextBox1.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox1.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox1.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.htmlTextBox1.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.htmlTextBox1.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox1.StyleName = "";
            // 
            // textBox8
            // 
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.428D), Telerik.Reporting.Drawing.Unit.Inch(0.194D));
            this.textBox8.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox8.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox8.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(0.5D);
            this.textBox8.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox8.StyleName = "";
            this.textBox8.Value = "";
            // 
            // textBox18
            // 
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox18.Style.Font.Italic = true;
            this.textBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox18.StyleName = "";
            this.textBox18.Value = "Postal address for customer correspondence (if different to installation address " +
    "in Section 1 above)";
            // 
            // table7
            // 
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.469D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.644D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.033D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.017D)));
            this.table7.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.349D)));
            this.table7.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.191D)));
            this.table7.Body.SetCellContent(0, 0, this.textBox21);
            this.table7.Body.SetCellContent(0, 2, this.textBox32);
            this.table7.Body.SetCellContent(0, 4, this.textBox38);
            this.table7.Body.SetCellContent(0, 1, this.textBox40);
            this.table7.Body.SetCellContent(0, 3, this.textBox57);
            tableGroup38.Name = "tableGroup17";
            tableGroup38.ReportItem = this.textBox9;
            tableGroup39.Name = "group33";
            tableGroup39.ReportItem = this.textBox29;
            tableGroup40.Name = "tableGroup18";
            tableGroup40.ReportItem = this.textBox27;
            tableGroup41.Name = "group34";
            tableGroup41.ReportItem = this.textBox41;
            tableGroup42.Name = "tableGroup19";
            tableGroup42.ReportItem = this.textBox33;
            this.table7.ColumnGroups.Add(tableGroup38);
            this.table7.ColumnGroups.Add(tableGroup39);
            this.table7.ColumnGroups.Add(tableGroup40);
            this.table7.ColumnGroups.Add(tableGroup41);
            this.table7.ColumnGroups.Add(tableGroup42);
            this.table7.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox21,
            this.textBox40,
            this.textBox32,
            this.textBox57,
            this.textBox38,
            this.textBox9,
            this.textBox29,
            this.textBox27,
            this.textBox41,
            this.textBox33});
            this.table7.Name = "table7";
            tableGroup43.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup43.Name = "detailTableGroup6";
            this.table7.RowGroups.Add(tableGroup43);
            this.table7.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(0.382D));
            this.table7.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.table7.StyleName = "";
            // 
            // textBox21
            // 
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.469D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox21.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox21.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox21.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox21.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox21.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox21.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox21.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            // 
            // textBox32
            // 
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(3.033D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox32.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox32.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox32.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox32.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox32.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox32.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox32.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            // 
            // textBox38
            // 
            this.textBox38.Name = "textBox38";
            this.textBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.349D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox38.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox38.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox38.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox38.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox38.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox38.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox38.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            // 
            // textBox40
            // 
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.644D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox40.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox40.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox40.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox40.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox40.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox40.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox40.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox40.StyleName = "";
            // 
            // textBox57
            // 
            this.textBox57.Name = "textBox57";
            this.textBox57.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.017D), Telerik.Reporting.Drawing.Unit.Inch(0.191D));
            this.textBox57.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox57.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox57.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox57.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox57.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox57.Style.Padding.Bottom = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox57.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox57.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox57.Style.Padding.Top = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox57.StyleName = "";
            // 
            // textBox64
            // 
            this.textBox64.Angle = 270D;
            this.textBox64.Name = "textBox64";
            this.textBox64.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.344D), Telerik.Reporting.Drawing.Unit.Inch(1.714D));
            this.textBox64.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox64.Style.Font.Bold = true;
            this.textBox64.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox64.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox64.StyleName = "";
            this.textBox64.Value = "Section 2A                    Customer Acceptance";
            // 
            // table8
            // 
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.511D)));
            this.table8.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(5.002D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.172D)));
            this.table8.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(1.336D)));
            this.table8.Body.SetCellContent(0, 0, this.textBox65);
            this.table8.Body.SetCellContent(0, 1, this.textBox67);
            this.table8.Body.SetCellContent(1, 0, this.htmlTextBox2, 1, 2);
            tableGroup59.Name = "tableGroup20";
            tableGroup59.ReportItem = this.textBox62;
            tableGroup60.Name = "tableGroup21";
            tableGroup60.ReportItem = this.textBox66;
            this.table8.ColumnGroups.Add(tableGroup59);
            this.table8.ColumnGroups.Add(tableGroup60);
            this.table8.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox65,
            this.textBox67,
            this.htmlTextBox2,
            this.textBox62,
            this.textBox66});
            this.table8.Name = "table8";
            tableGroup61.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup61.Name = "detailTableGroup7";
            tableGroup62.Name = "group36";
            this.table8.RowGroups.Add(tableGroup61);
            this.table8.RowGroups.Add(tableGroup62);
            this.table8.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.513D), Telerik.Reporting.Drawing.Unit.Inch(1.714D));
            this.table8.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table8.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table8.StyleName = "";
            // 
            // textBox65
            // 
            this.textBox65.Name = "textBox65";
            this.textBox65.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.511D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
            this.textBox65.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            // 
            // textBox67
            // 
            this.textBox67.Name = "textBox67";
            this.textBox67.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.002D), Telerik.Reporting.Drawing.Unit.Inch(0.172D));
            this.textBox67.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            // 
            // htmlTextBox2
            // 
            this.htmlTextBox2.Name = "htmlTextBox2";
            this.htmlTextBox2.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.513D), Telerik.Reporting.Drawing.Unit.Inch(1.336D));
            this.htmlTextBox2.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox2.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox2.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.htmlTextBox2.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.htmlTextBox2.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.htmlTextBox2.StyleName = "";
            this.htmlTextBox2.Value = resources.GetString("htmlTextBox2.Value");
            // 
            // htmlTextBox3
            // 
            this.htmlTextBox3.Name = "htmlTextBox3";
            this.htmlTextBox3.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.856D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.htmlTextBox3.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.htmlTextBox3.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox3.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox3.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.htmlTextBox3.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.htmlTextBox3.Style.Font.Bold = true;
            this.htmlTextBox3.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.htmlTextBox3.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.htmlTextBox3.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox3.StyleName = "";
            this.htmlTextBox3.Value = "<span style=\"font-size: 9pt\">The following sections must be completed for applica" +
    "tions except<strong> Abolishment of Electricity Supply</strong></span>";
            // 
            // textBox69
            // 
            this.textBox69.Angle = 270D;
            this.textBox69.Name = "textBox69";
            this.textBox69.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.344D), Telerik.Reporting.Drawing.Unit.Inch(0.763D));
            this.textBox69.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox69.Style.Font.Bold = true;
            this.textBox69.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox69.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox69.StyleName = "";
            this.textBox69.Value = "Section 3            Electrician";
            // 
            // table9
            // 
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.504D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.504D)));
            this.table9.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.504D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.193D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.175D)));
            this.table9.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.219D)));
            this.table9.Body.SetCellContent(0, 0, this.textBox70);
            this.table9.Body.SetCellContent(0, 1, this.textBox72);
            this.table9.Body.SetCellContent(0, 2, this.textBox74);
            this.table9.Body.SetCellContent(1, 0, this.textBox63);
            this.table9.Body.SetCellContent(1, 1, this.textBox75);
            this.table9.Body.SetCellContent(1, 2, this.textBox76);
            this.table9.Body.SetCellContent(2, 0, this.textBox77);
            this.table9.Body.SetCellContent(2, 1, this.textBox78);
            this.table9.Body.SetCellContent(2, 2, this.textBox79);
            tableGroup63.Name = "tableGroup22";
            tableGroup63.ReportItem = this.textBox68;
            tableGroup64.Name = "tableGroup23";
            tableGroup64.ReportItem = this.textBox71;
            tableGroup65.Name = "tableGroup24";
            tableGroup65.ReportItem = this.textBox73;
            this.table9.ColumnGroups.Add(tableGroup63);
            this.table9.ColumnGroups.Add(tableGroup64);
            this.table9.ColumnGroups.Add(tableGroup65);
            this.table9.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox70,
            this.textBox72,
            this.textBox74,
            this.textBox63,
            this.textBox75,
            this.textBox76,
            this.textBox77,
            this.textBox78,
            this.textBox79,
            this.textBox68,
            this.textBox71,
            this.textBox73});
            this.table9.Name = "table9";
            tableGroup66.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup66.Name = "detailTableGroup8";
            tableGroup67.Name = "group39";
            tableGroup68.Name = "group40";
            this.table9.RowGroups.Add(tableGroup66);
            this.table9.RowGroups.Add(tableGroup67);
            this.table9.RowGroups.Add(tableGroup68);
            this.table9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(0.762D));
            this.table9.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.table9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.table9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table9.StyleName = "";
            // 
            // textBox70
            // 
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.193D));
            this.textBox70.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox70.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox70.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox70.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            // 
            // textBox72
            // 
            this.textBox72.Name = "textBox72";
            this.textBox72.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.193D));
            this.textBox72.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox72.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox72.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox72.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            // 
            // textBox74
            // 
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.193D));
            this.textBox74.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox74.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox74.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox74.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            // 
            // textBox63
            // 
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.175D));
            this.textBox63.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox63.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox63.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox63.StyleName = "";
            this.textBox63.Value = "Mobile Telephone (Mandatory)";
            // 
            // textBox75
            // 
            this.textBox75.Name = "textBox75";
            this.textBox75.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.175D));
            this.textBox75.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox75.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox75.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox75.StyleName = "";
            this.textBox75.Value = "Business Telephone";
            // 
            // textBox76
            // 
            this.textBox76.Name = "textBox76";
            this.textBox76.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.175D));
            this.textBox76.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox76.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox76.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox76.StyleName = "";
            this.textBox76.Value = "Email Address (Mandatory)";
            // 
            // textBox77
            // 
            this.textBox77.Name = "textBox77";
            this.textBox77.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.219D));
            this.textBox77.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox77.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox77.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox77.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox77.StyleName = "";
            // 
            // textBox78
            // 
            this.textBox78.Name = "textBox78";
            this.textBox78.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.219D));
            this.textBox78.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox78.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox78.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox78.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox78.StyleName = "";
            // 
            // textBox79
            // 
            this.textBox79.Name = "textBox79";
            this.textBox79.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.504D), Telerik.Reporting.Drawing.Unit.Inch(0.219D));
            this.textBox79.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox79.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox79.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox79.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox79.StyleName = "";
            // 
            // textBox82
            // 
            this.textBox82.Angle = 270D;
            this.textBox82.Name = "textBox82";
            this.textBox82.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.344D), Telerik.Reporting.Drawing.Unit.Inch(0.95D));
            this.textBox82.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox82.Style.Font.Bold = true;
            this.textBox82.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox82.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox82.StyleName = "";
            this.textBox82.Value = "Section 4            Load Details";
            // 
            // table10
            // 
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(5.351D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.222D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.048D)));
            this.table10.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.891D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.206D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.15D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.297D)));
            this.table10.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.297D)));
            this.table10.Body.SetCellContent(0, 0, this.textBox83);
            this.table10.Body.SetCellContent(0, 1, this.textBox85);
            this.table10.Body.SetCellContent(0, 2, this.textBox87);
            this.table10.Body.SetCellContent(3, 0, this.textBox90);
            this.table10.Body.SetCellContent(3, 2, this.textBox92);
            this.table10.Body.SetCellContent(0, 3, this.textBox81);
            this.table10.Body.SetCellContent(3, 3, this.textBox94);
            this.table10.Body.SetCellContent(3, 1, this.textBox95);
            this.table10.Body.SetCellContent(2, 0, this.table11, 1, 4);
            this.table10.Body.SetCellContent(1, 0, this.textBox80, 1, 4);
            tableGroup76.Name = "tableGroup25";
            tableGroup77.Name = "tableGroup26";
            tableGroup78.Name = "tableGroup27";
            tableGroup79.Name = "group48";
            this.table10.ColumnGroups.Add(tableGroup76);
            this.table10.ColumnGroups.Add(tableGroup77);
            this.table10.ColumnGroups.Add(tableGroup78);
            this.table10.ColumnGroups.Add(tableGroup79);
            this.table10.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox83,
            this.textBox85,
            this.textBox87,
            this.textBox81,
            this.textBox80,
            this.table11,
            this.textBox90,
            this.textBox95,
            this.textBox92,
            this.textBox94});
            this.table10.Name = "table10";
            tableGroup81.Name = "group42";
            tableGroup82.Name = "group43";
            tableGroup80.ChildGroups.Add(tableGroup81);
            tableGroup80.ChildGroups.Add(tableGroup82);
            tableGroup80.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup80.Name = "detailTableGroup9";
            tableGroup84.Name = "group45";
            tableGroup83.ChildGroups.Add(tableGroup84);
            tableGroup83.Name = "group44";
            tableGroup86.Name = "group47";
            tableGroup85.ChildGroups.Add(tableGroup86);
            tableGroup85.Name = "group46";
            this.table10.RowGroups.Add(tableGroup80);
            this.table10.RowGroups.Add(tableGroup83);
            this.table10.RowGroups.Add(tableGroup85);
            this.table10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(0.95D));
            this.table10.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.table10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.table10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table10.StyleName = "";
            // 
            // textBox83
            // 
            this.textBox83.Name = "textBox83";
            this.textBox83.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.351D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            this.textBox83.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox83.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox83.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Bottom;
            this.textBox83.Value = "Please list fixed appliances of 2.5kW or greater (e.g. motors, air conditioners, " +
    "EV chargers)";
            // 
            // textBox85
            // 
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.222D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            this.textBox85.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox85.Value = "=";
            // 
            // textBox87
            // 
            this.textBox87.Name = "textBox87";
            this.textBox87.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.048D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            // 
            // textBox90
            // 
            this.textBox90.Name = "textBox90";
            this.textBox90.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.351D), Telerik.Reporting.Drawing.Unit.Inch(0.297D));
            this.textBox90.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox90.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox90.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox90.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox90.StyleName = "";
            this.textBox90.Value = "Maximum Demand of installation in accordance with Australian Standard AS/NZS 3000" +
    "";
            // 
            // textBox92
            // 
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.048D), Telerik.Reporting.Drawing.Unit.Inch(0.297D));
            this.textBox92.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox92.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox92.StyleName = "";
            // 
            // textBox81
            // 
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.891D), Telerik.Reporting.Drawing.Unit.Inch(0.206D));
            this.textBox81.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox81.StyleName = "";
            this.textBox81.Value = "kw";
            // 
            // textBox94
            // 
            this.textBox94.Name = "textBox94";
            this.textBox94.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.891D), Telerik.Reporting.Drawing.Unit.Inch(0.297D));
            this.textBox94.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox94.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox94.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(7.5D);
            this.textBox94.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(0D);
            this.textBox94.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox94.StyleName = "";
            this.textBox94.Value = "AMPS Per Phase";
            // 
            // textBox95
            // 
            this.textBox95.Name = "textBox95";
            this.textBox95.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.222D), Telerik.Reporting.Drawing.Unit.Inch(0.297D));
            this.textBox95.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox95.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox95.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox95.Value = "=";
            // 
            // table11
            // 
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.566D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.21D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.372D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.264D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.88D)));
            this.table11.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.22D)));
            this.table11.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.297D)));
            this.table11.Body.SetCellContent(0, 1, this.checkBox10);
            this.table11.Body.SetCellContent(0, 2, this.checkBox11);
            this.table11.Body.SetCellContent(0, 3, this.checkBox12);
            this.table11.Body.SetCellContent(0, 4, this.checkBox14);
            this.table11.Body.SetCellContent(0, 5, this.textBox86);
            this.table11.Body.SetCellContent(0, 0, this.textBox88);
            tableGroup69.Name = "tableGroup8";
            tableGroup70.Name = "tableGroup9";
            tableGroup71.Name = "tableGroup10";
            tableGroup72.Name = "group15";
            tableGroup73.Name = "group16";
            tableGroup74.Name = "group49";
            this.table11.ColumnGroups.Add(tableGroup69);
            this.table11.ColumnGroups.Add(tableGroup70);
            this.table11.ColumnGroups.Add(tableGroup71);
            this.table11.ColumnGroups.Add(tableGroup72);
            this.table11.ColumnGroups.Add(tableGroup73);
            this.table11.ColumnGroups.Add(tableGroup74);
            this.table11.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox88,
            this.checkBox10,
            this.checkBox11,
            this.checkBox12,
            this.checkBox14,
            this.textBox86});
            this.table11.Name = "table11";
            tableGroup75.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup75.Name = "detailTableGroup3";
            this.table11.RowGroups.Add(tableGroup75);
            this.table11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(0.297D));
            this.table11.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table11.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table11.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table11.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table11.StyleName = "";
            // 
            // checkBox10
            // 
            this.checkBox10.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox10.CheckedImage = ((object)(resources.GetObject("checkBox10.CheckedImage")));
            this.checkBox10.IndeterminateImage = ((object)(resources.GetObject("checkBox10.IndeterminateImage")));
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.21D), Telerik.Reporting.Drawing.Unit.Inch(0.297D));
            this.checkBox10.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox10.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox10.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox10.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox10.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox10.StyleName = "";
            this.checkBox10.Text = "Off Peak HWS";
            this.checkBox10.UncheckedImage = ((object)(resources.GetObject("checkBox10.UncheckedImage")));
            this.checkBox10.Value = "";
            // 
            // checkBox11
            // 
            this.checkBox11.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox11.CheckedImage = ((object)(resources.GetObject("checkBox11.CheckedImage")));
            this.checkBox11.IndeterminateImage = ((object)(resources.GetObject("checkBox11.IndeterminateImage")));
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.372D), Telerik.Reporting.Drawing.Unit.Inch(0.297D));
            this.checkBox11.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox11.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox11.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox11.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox11.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox11.StyleName = "";
            this.checkBox11.Text = "Space heating";
            this.checkBox11.UncheckedImage = ((object)(resources.GetObject("checkBox11.UncheckedImage")));
            this.checkBox11.Value = "";
            // 
            // checkBox12
            // 
            this.checkBox12.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox12.CheckedImage = ((object)(resources.GetObject("checkBox12.CheckedImage")));
            this.checkBox12.IndeterminateImage = ((object)(resources.GetObject("checkBox12.IndeterminateImage")));
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.264D), Telerik.Reporting.Drawing.Unit.Inch(0.297D));
            this.checkBox12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox12.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox12.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox12.StyleName = "";
            this.checkBox12.Text = "Underfloor heating";
            this.checkBox12.UncheckedImage = ((object)(resources.GetObject("checkBox12.UncheckedImage")));
            this.checkBox12.Value = "";
            // 
            // checkBox14
            // 
            this.checkBox14.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox14.CheckedImage = ((object)(resources.GetObject("checkBox14.CheckedImage")));
            this.checkBox14.IndeterminateImage = ((object)(resources.GetObject("checkBox14.IndeterminateImage")));
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.88D), Telerik.Reporting.Drawing.Unit.Inch(0.297D));
            this.checkBox14.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox14.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox14.StyleName = "";
            this.checkBox14.Text = "Other";
            this.checkBox14.UncheckedImage = ((object)(resources.GetObject("checkBox14.UncheckedImage")));
            this.checkBox14.Value = "";
            // 
            // textBox86
            // 
            this.textBox86.Name = "textBox86";
            this.textBox86.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.22D), Telerik.Reporting.Drawing.Unit.Inch(0.297D));
            this.textBox86.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox86.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.textBox86.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.3D);
            this.textBox86.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox86.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox86.StyleName = "";
            this.textBox86.Value = "(see comments)";
            // 
            // textBox88
            // 
            this.textBox88.Name = "textBox88";
            this.textBox88.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.566D), Telerik.Reporting.Drawing.Unit.Inch(0.297D));
            this.textBox88.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox88.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.3D);
            this.textBox88.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox88.StyleName = "";
            this.textBox88.Value = "Off Peak Loads:";
            // 
            // textBox80
            // 
            this.textBox80.Name = "textBox80";
            this.textBox80.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(0.15D));
            this.textBox80.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox80.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox80.Style.Font.Italic = true;
            this.textBox80.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox80.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox80.StyleName = "";
            this.textBox80.Value = "Please do not make commitments for electrical equipment until SA Power Networks c" +
    "onfirms the phases and voltage are available.";
            // 
            // textBox91
            // 
            this.textBox91.Angle = 270D;
            this.textBox91.Name = "textBox91";
            this.textBox91.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.344D), Telerik.Reporting.Drawing.Unit.Inch(0.658D));
            this.textBox91.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox91.Style.Font.Bold = true;
            this.textBox91.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox91.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox91.StyleName = "";
            this.textBox91.Value = "Section 5            Service";
            // 
            // table12
            // 
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.628D)));
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.008D)));
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.37D)));
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.043D)));
            this.table12.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.464D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.329D)));
            this.table12.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.329D)));
            this.table12.Body.SetCellContent(0, 0, this.textBox93);
            this.table12.Body.SetCellContent(0, 2, this.textBox96);
            this.table12.Body.SetCellContent(0, 1, this.table13);
            this.table12.Body.SetCellContent(0, 3, this.table14);
            this.table12.Body.SetCellContent(0, 4, this.table15);
            this.table12.Body.SetCellContent(1, 0, this.table16, 1, 5);
            tableGroup106.Name = "tableGroup28";
            tableGroup107.Name = "tableGroup29";
            tableGroup108.Name = "group53";
            tableGroup109.Name = "group52";
            tableGroup110.Name = "tableGroup30";
            this.table12.ColumnGroups.Add(tableGroup106);
            this.table12.ColumnGroups.Add(tableGroup107);
            this.table12.ColumnGroups.Add(tableGroup108);
            this.table12.ColumnGroups.Add(tableGroup109);
            this.table12.ColumnGroups.Add(tableGroup110);
            this.table12.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox93,
            this.table13,
            this.textBox96,
            this.table14,
            this.table15,
            this.table16});
            this.table12.Name = "table12";
            tableGroup112.Name = "group51";
            tableGroup113.Name = "group54";
            tableGroup111.ChildGroups.Add(tableGroup112);
            tableGroup111.ChildGroups.Add(tableGroup113);
            tableGroup111.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup111.Name = "detailTableGroup10";
            this.table12.RowGroups.Add(tableGroup111);
            this.table12.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.513D), Telerik.Reporting.Drawing.Unit.Inch(0.658D));
            this.table12.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.table12.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.table12.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table12.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table12.StyleName = "";
            // 
            // textBox93
            // 
            this.textBox93.Name = "textBox93";
            this.textBox93.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.628D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.textBox93.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox93.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox93.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox93.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox93.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox93.Value = "New or Upgraded Service?";
            // 
            // textBox96
            // 
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.37D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.textBox96.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox96.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox96.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox96.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox96.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox96.StyleName = "";
            this.textBox96.Value = "Part of a Community/Strata installation?";
            // 
            // table13
            // 
            this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.486D)));
            this.table13.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.438D)));
            this.table13.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.329D)));
            this.table13.Body.SetCellContent(0, 0, this.checkBox22);
            this.table13.Body.SetCellContent(0, 1, this.checkBox23);
            tableGroup87.Name = "tableGroup7";
            tableGroup88.Name = "tableGroup9";
            this.table13.ColumnGroups.Add(tableGroup87);
            this.table13.ColumnGroups.Add(tableGroup88);
            this.table13.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox22,
            this.checkBox23});
            this.table13.Name = "table13";
            tableGroup89.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup89.Name = "detailTableGroup3";
            this.table13.RowGroups.Add(tableGroup89);
            this.table13.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.007D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.table13.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.table13.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table13.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table13.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table13.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table13.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table13.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table13.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table13.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.table13.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table13.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox22
            // 
            this.checkBox22.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox22.CheckedImage = ((object)(resources.GetObject("checkBox22.CheckedImage")));
            this.checkBox22.IndeterminateImage = ((object)(resources.GetObject("checkBox22.IndeterminateImage")));
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.486D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.checkBox22.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox22.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox22.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox22.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox22.StyleName = "";
            this.checkBox22.Text = " Yes";
            this.checkBox22.UncheckedImage = ((object)(resources.GetObject("checkBox22.UncheckedImage")));
            this.checkBox22.Value = "";
            // 
            // checkBox23
            // 
            this.checkBox23.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox23.CheckedImage = ((object)(resources.GetObject("checkBox23.CheckedImage")));
            this.checkBox23.IndeterminateImage = ((object)(resources.GetObject("checkBox23.IndeterminateImage")));
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.438D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.checkBox23.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox23.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox23.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox23.StyleName = "";
            this.checkBox23.Text = " No";
            this.checkBox23.UncheckedImage = ((object)(resources.GetObject("checkBox23.UncheckedImage")));
            this.checkBox23.Value = "";
            // 
            // table14
            // 
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.505D)));
            this.table14.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.455D)));
            this.table14.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.329D)));
            this.table14.Body.SetCellContent(0, 0, this.checkBox9);
            this.table14.Body.SetCellContent(0, 1, this.checkBox15);
            tableGroup90.Name = "tableGroup7";
            tableGroup91.Name = "tableGroup9";
            this.table14.ColumnGroups.Add(tableGroup90);
            this.table14.ColumnGroups.Add(tableGroup91);
            this.table14.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox9,
            this.checkBox15});
            this.table14.Name = "table14";
            tableGroup92.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup92.Name = "detailTableGroup3";
            this.table14.RowGroups.Add(tableGroup92);
            this.table14.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.043D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.table14.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.table14.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table14.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table14.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table14.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table14.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table14.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table14.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table14.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.table14.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table14.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox9
            // 
            this.checkBox9.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox9.CheckedImage = ((object)(resources.GetObject("checkBox9.CheckedImage")));
            this.checkBox9.IndeterminateImage = ((object)(resources.GetObject("checkBox9.IndeterminateImage")));
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.505D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.checkBox9.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox9.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox9.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox9.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox9.StyleName = "";
            this.checkBox9.Text = " Yes";
            this.checkBox9.UncheckedImage = ((object)(resources.GetObject("checkBox9.UncheckedImage")));
            this.checkBox9.Value = "";
            // 
            // checkBox15
            // 
            this.checkBox15.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox15.CheckedImage = ((object)(resources.GetObject("checkBox15.CheckedImage")));
            this.checkBox15.IndeterminateImage = ((object)(resources.GetObject("checkBox15.IndeterminateImage")));
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.455D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.checkBox15.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox15.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox15.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox15.StyleName = "";
            this.checkBox15.Text = " No";
            this.checkBox15.UncheckedImage = ((object)(resources.GetObject("checkBox15.UncheckedImage")));
            this.checkBox15.Value = "";
            // 
            // table15
            // 
            this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.488D)));
            this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.488D)));
            this.table15.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.488D)));
            this.table15.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.164D)));
            this.table15.Body.SetCellContent(0, 0, this.checkBox16);
            this.table15.Body.SetCellContent(0, 1, this.checkBox18);
            this.table15.Body.SetCellContent(0, 2, this.checkBox19);
            tableGroup94.Name = "group55";
            tableGroup95.Name = "tableGroup32";
            tableGroup96.Name = "tableGroup33";
            tableGroup93.ChildGroups.Add(tableGroup94);
            tableGroup93.ChildGroups.Add(tableGroup95);
            tableGroup93.ChildGroups.Add(tableGroup96);
            tableGroup93.Name = "tableGroup31";
            tableGroup93.ReportItem = this.textBox84;
            this.table15.ColumnGroups.Add(tableGroup93);
            this.table15.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox16,
            this.checkBox18,
            this.checkBox19,
            this.textBox84});
            this.table15.Name = "table15";
            tableGroup97.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup97.Name = "detailTableGroup11";
            this.table15.RowGroups.Add(tableGroup97);
            this.table15.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.464D), Telerik.Reporting.Drawing.Unit.Inch(0.328D));
            this.table15.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.table15.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table15.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table15.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table15.StyleName = "";
            // 
            // checkBox16
            // 
            this.checkBox16.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox16.CheckedImage = ((object)(resources.GetObject("checkBox16.CheckedImage")));
            this.checkBox16.IndeterminateImage = ((object)(resources.GetObject("checkBox16.IndeterminateImage")));
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.488D), Telerik.Reporting.Drawing.Unit.Inch(0.164D));
            this.checkBox16.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox16.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox16.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox16.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox16.StyleName = "";
            this.checkBox16.Text = " 1";
            this.checkBox16.UncheckedImage = ((object)(resources.GetObject("checkBox16.UncheckedImage")));
            this.checkBox16.Value = "";
            // 
            // checkBox18
            // 
            this.checkBox18.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox18.CheckedImage = ((object)(resources.GetObject("checkBox18.CheckedImage")));
            this.checkBox18.IndeterminateImage = ((object)(resources.GetObject("checkBox18.IndeterminateImage")));
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.488D), Telerik.Reporting.Drawing.Unit.Inch(0.164D));
            this.checkBox18.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox18.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox18.StyleName = "";
            this.checkBox18.Text = " 2";
            this.checkBox18.UncheckedImage = ((object)(resources.GetObject("checkBox18.UncheckedImage")));
            this.checkBox18.Value = "";
            // 
            // checkBox19
            // 
            this.checkBox19.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox19.CheckedImage = ((object)(resources.GetObject("checkBox19.CheckedImage")));
            this.checkBox19.IndeterminateImage = ((object)(resources.GetObject("checkBox19.IndeterminateImage")));
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.488D), Telerik.Reporting.Drawing.Unit.Inch(0.164D));
            this.checkBox19.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox19.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox19.StyleName = "";
            this.checkBox19.Text = " 3";
            this.checkBox19.UncheckedImage = ((object)(resources.GetObject("checkBox19.UncheckedImage")));
            this.checkBox19.Value = "";
            // 
            // table16
            // 
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.799D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.886D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.984D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.789D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.882D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.427D)));
            this.table16.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.745D)));
            this.table16.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.329D)));
            this.table16.Body.SetCellContent(0, 1, this.checkBox17);
            this.table16.Body.SetCellContent(0, 2, this.checkBox20);
            this.table16.Body.SetCellContent(0, 3, this.checkBox21);
            this.table16.Body.SetCellContent(0, 4, this.checkBox24);
            this.table16.Body.SetCellContent(0, 0, this.textBox97);
            this.table16.Body.SetCellContent(0, 5, this.checkBox25);
            this.table16.Body.SetCellContent(0, 6, this.checkBox26);
            tableGroup98.Name = "tableGroup8";
            tableGroup99.Name = "tableGroup9";
            tableGroup100.Name = "tableGroup10";
            tableGroup101.Name = "group15";
            tableGroup102.Name = "group16";
            tableGroup103.Name = "group49";
            tableGroup104.Name = "group56";
            this.table16.ColumnGroups.Add(tableGroup98);
            this.table16.ColumnGroups.Add(tableGroup99);
            this.table16.ColumnGroups.Add(tableGroup100);
            this.table16.ColumnGroups.Add(tableGroup101);
            this.table16.ColumnGroups.Add(tableGroup102);
            this.table16.ColumnGroups.Add(tableGroup103);
            this.table16.ColumnGroups.Add(tableGroup104);
            this.table16.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox97,
            this.checkBox17,
            this.checkBox20,
            this.checkBox21,
            this.checkBox24,
            this.checkBox25,
            this.checkBox26});
            this.table16.Name = "table16";
            tableGroup105.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup105.Name = "detailTableGroup3";
            this.table16.RowGroups.Add(tableGroup105);
            this.table16.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.table16.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.table16.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table16.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table16.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table16.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table16.StyleName = "";
            // 
            // checkBox17
            // 
            this.checkBox17.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox17.CheckedImage = ((object)(resources.GetObject("checkBox17.CheckedImage")));
            this.checkBox17.IndeterminateImage = ((object)(resources.GetObject("checkBox17.IndeterminateImage")));
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.886D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.checkBox17.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox17.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.checkBox17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox17.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox17.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox17.StyleName = "";
            this.checkBox17.Text = "Overhead";
            this.checkBox17.UncheckedImage = ((object)(resources.GetObject("checkBox17.UncheckedImage")));
            this.checkBox17.Value = "";
            // 
            // checkBox20
            // 
            this.checkBox20.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox20.CheckedImage = ((object)(resources.GetObject("checkBox20.CheckedImage")));
            this.checkBox20.IndeterminateImage = ((object)(resources.GetObject("checkBox20.IndeterminateImage")));
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.984D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.checkBox20.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox20.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.checkBox20.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox20.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox20.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox20.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox20.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox20.StyleName = "";
            this.checkBox20.Text = "Over/Unde";
            this.checkBox20.UncheckedImage = ((object)(resources.GetObject("checkBox20.UncheckedImage")));
            this.checkBox20.Value = "";
            // 
            // checkBox21
            // 
            this.checkBox21.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox21.CheckedImage = ((object)(resources.GetObject("checkBox21.CheckedImage")));
            this.checkBox21.IndeterminateImage = ((object)(resources.GetObject("checkBox21.IndeterminateImage")));
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.789D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.checkBox21.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox21.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.checkBox21.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox21.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox21.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox21.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox21.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox21.StyleName = "";
            this.checkBox21.Text = "Fused Pit";
            this.checkBox21.UncheckedImage = ((object)(resources.GetObject("checkBox21.UncheckedImage")));
            this.checkBox21.Value = "";
            // 
            // checkBox24
            // 
            this.checkBox24.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox24.CheckedImage = ((object)(resources.GetObject("checkBox24.CheckedImage")));
            this.checkBox24.IndeterminateImage = ((object)(resources.GetObject("checkBox24.IndeterminateImage")));
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.882D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.checkBox24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox24.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.checkBox24.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox24.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox24.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox24.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox24.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox24.StyleName = "";
            this.checkBox24.Text = "Unfused Pit";
            this.checkBox24.UncheckedImage = ((object)(resources.GetObject("checkBox24.UncheckedImage")));
            this.checkBox24.Value = "";
            // 
            // textBox97
            // 
            this.textBox97.Name = "textBox97";
            this.textBox97.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.799D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.textBox97.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox97.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox97.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox97.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0.1D);
            this.textBox97.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox97.StyleName = "";
            this.textBox97.Value = "Service Type: ";
            // 
            // checkBox25
            // 
            this.checkBox25.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox25.CheckedImage = ((object)(resources.GetObject("checkBox25.CheckedImage")));
            this.checkBox25.IndeterminateImage = ((object)(resources.GetObject("checkBox25.IndeterminateImage")));
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.427D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.checkBox25.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox25.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.checkBox25.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox25.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox25.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox25.StyleName = "";
            this.checkBox25.Text = "Pole Top Transformer";
            this.checkBox25.UncheckedImage = ((object)(resources.GetObject("checkBox25.UncheckedImage")));
            this.checkBox25.Value = "";
            // 
            // checkBox26
            // 
            this.checkBox26.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox26.CheckedImage = ((object)(resources.GetObject("checkBox26.CheckedImage")));
            this.checkBox26.IndeterminateImage = ((object)(resources.GetObject("checkBox26.IndeterminateImage")));
            this.checkBox26.Name = "checkBox26";
            this.checkBox26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.745D), Telerik.Reporting.Drawing.Unit.Inch(0.329D));
            this.checkBox26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.checkBox26.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.checkBox26.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox26.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox26.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox26.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox26.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox26.StyleName = "";
            this.checkBox26.Text = "Ground Level Transformer ";
            this.checkBox26.UncheckedImage = ((object)(resources.GetObject("checkBox26.UncheckedImage")));
            this.checkBox26.Value = "";
            // 
            // textBox99
            // 
            this.textBox99.Angle = 270D;
            this.textBox99.Name = "textBox99";
            this.textBox99.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.344D), Telerik.Reporting.Drawing.Unit.Inch(1.315D));
            this.textBox99.Style.BackgroundColor = System.Drawing.Color.Gray;
            this.textBox99.Style.Font.Bold = true;
            this.textBox99.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox99.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox99.StyleName = "";
            this.textBox99.Value = "Section 6                 Metering";
            // 
            // table17
            // 
            this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.804D)));
            this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.242D)));
            this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.895D)));
            this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.052D)));
            this.table17.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.519D)));
            this.table17.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.387D)));
            this.table17.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.249D)));
            this.table17.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.299D)));
            this.table17.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table17.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18D)));
            this.table17.Body.SetCellContent(0, 0, this.textBox89);
            this.table17.Body.SetCellContent(0, 2, this.textBox100);
            this.table17.Body.SetCellContent(0, 1, this.table18);
            this.table17.Body.SetCellContent(0, 3, this.table19);
            this.table17.Body.SetCellContent(0, 4, this.table20);
            this.table17.Body.SetCellContent(1, 0, this.table21, 1, 5);
            this.table17.Body.SetCellContent(2, 0, this.table22, 1, 5);
            this.table17.Body.SetCellContent(3, 0, this.table24, 1, 5);
            this.table17.Body.SetCellContent(4, 0, this.table26, 1, 5);
            tableGroup163.Name = "tableGroup28";
            tableGroup164.Name = "tableGroup29";
            tableGroup165.Name = "group53";
            tableGroup166.Name = "group52";
            tableGroup167.Name = "tableGroup30";
            this.table17.ColumnGroups.Add(tableGroup163);
            this.table17.ColumnGroups.Add(tableGroup164);
            this.table17.ColumnGroups.Add(tableGroup165);
            this.table17.ColumnGroups.Add(tableGroup166);
            this.table17.ColumnGroups.Add(tableGroup167);
            this.table17.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox89,
            this.table18,
            this.textBox100,
            this.table19,
            this.table20,
            this.table21,
            this.table22,
            this.table24,
            this.table26});
            this.table17.Name = "table17";
            tableGroup169.Name = "group51";
            tableGroup170.Name = "group54";
            tableGroup171.Name = "group58";
            tableGroup172.Name = "group59";
            tableGroup173.Name = "group60";
            tableGroup168.ChildGroups.Add(tableGroup169);
            tableGroup168.ChildGroups.Add(tableGroup170);
            tableGroup168.ChildGroups.Add(tableGroup171);
            tableGroup168.ChildGroups.Add(tableGroup172);
            tableGroup168.ChildGroups.Add(tableGroup173);
            tableGroup168.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup168.Name = "detailTableGroup10";
            this.table17.RowGroups.Add(tableGroup168);
            this.table17.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(1.315D));
            this.table17.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.table17.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.table17.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table17.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table17.StyleName = "";
            // 
            // textBox89
            // 
            this.textBox89.Name = "textBox89";
            this.textBox89.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.804D), Telerik.Reporting.Drawing.Unit.Inch(0.387D));
            this.textBox89.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox89.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox89.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox89.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox89.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox89.Value = "New or Upgraded Meter(s)?";
            // 
            // textBox100
            // 
            this.textBox100.Name = "textBox100";
            this.textBox100.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.895D), Telerik.Reporting.Drawing.Unit.Inch(0.387D));
            this.textBox100.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.textBox100.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox100.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox100.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox100.StyleName = "";
            this.textBox100.Value = "Use Existing Meter(s) ";
            // 
            // table18
            // 
            this.table18.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.609D)));
            this.table18.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.549D)));
            this.table18.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.387D)));
            this.table18.Body.SetCellContent(0, 0, this.checkBox27);
            this.table18.Body.SetCellContent(0, 1, this.checkBox28);
            tableGroup114.Name = "tableGroup7";
            tableGroup115.Name = "tableGroup9";
            this.table18.ColumnGroups.Add(tableGroup114);
            this.table18.ColumnGroups.Add(tableGroup115);
            this.table18.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox27,
            this.checkBox28});
            this.table18.Name = "table18";
            tableGroup116.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup116.Name = "detailTableGroup3";
            this.table18.RowGroups.Add(tableGroup116);
            this.table18.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.241D), Telerik.Reporting.Drawing.Unit.Inch(0.387D));
            this.table18.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.table18.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table18.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table18.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table18.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table18.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table18.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table18.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.table18.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table18.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox27
            // 
            this.checkBox27.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox27.CheckedImage = ((object)(resources.GetObject("checkBox27.CheckedImage")));
            this.checkBox27.IndeterminateImage = ((object)(resources.GetObject("checkBox27.IndeterminateImage")));
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.609D), Telerik.Reporting.Drawing.Unit.Inch(0.387D));
            this.checkBox27.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox27.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox27.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox27.StyleName = "";
            this.checkBox27.Text = " Yes";
            this.checkBox27.UncheckedImage = ((object)(resources.GetObject("checkBox27.UncheckedImage")));
            this.checkBox27.Value = "";
            // 
            // checkBox28
            // 
            this.checkBox28.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox28.CheckedImage = ((object)(resources.GetObject("checkBox28.CheckedImage")));
            this.checkBox28.IndeterminateImage = ((object)(resources.GetObject("checkBox28.IndeterminateImage")));
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.549D), Telerik.Reporting.Drawing.Unit.Inch(0.387D));
            this.checkBox28.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox28.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox28.StyleName = "";
            this.checkBox28.Text = " No";
            this.checkBox28.UncheckedImage = ((object)(resources.GetObject("checkBox28.UncheckedImage")));
            this.checkBox28.Value = "";
            // 
            // table19
            // 
            this.table19.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.51D)));
            this.table19.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.459D)));
            this.table19.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.387D)));
            this.table19.Body.SetCellContent(0, 0, this.checkBox29);
            this.table19.Body.SetCellContent(0, 1, this.checkBox30);
            tableGroup117.Name = "tableGroup7";
            tableGroup118.Name = "tableGroup9";
            this.table19.ColumnGroups.Add(tableGroup117);
            this.table19.ColumnGroups.Add(tableGroup118);
            this.table19.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox29,
            this.checkBox30});
            this.table19.Name = "table19";
            tableGroup119.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup119.Name = "detailTableGroup3";
            this.table19.RowGroups.Add(tableGroup119);
            this.table19.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.052D), Telerik.Reporting.Drawing.Unit.Inch(0.387D));
            this.table19.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.table19.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table19.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table19.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table19.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table19.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table19.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table19.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.table19.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table19.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox29
            // 
            this.checkBox29.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox29.CheckedImage = ((object)(resources.GetObject("checkBox29.CheckedImage")));
            this.checkBox29.IndeterminateImage = ((object)(resources.GetObject("checkBox29.IndeterminateImage")));
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.51D), Telerik.Reporting.Drawing.Unit.Inch(0.387D));
            this.checkBox29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox29.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox29.StyleName = "";
            this.checkBox29.Text = " Yes";
            this.checkBox29.UncheckedImage = ((object)(resources.GetObject("checkBox29.UncheckedImage")));
            this.checkBox29.Value = "";
            // 
            // checkBox30
            // 
            this.checkBox30.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox30.CheckedImage = ((object)(resources.GetObject("checkBox30.CheckedImage")));
            this.checkBox30.IndeterminateImage = ((object)(resources.GetObject("checkBox30.IndeterminateImage")));
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.459D), Telerik.Reporting.Drawing.Unit.Inch(0.387D));
            this.checkBox30.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox30.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox30.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox30.StyleName = "";
            this.checkBox30.Text = " No";
            this.checkBox30.UncheckedImage = ((object)(resources.GetObject("checkBox30.UncheckedImage")));
            this.checkBox30.Value = "";
            // 
            // table20
            // 
            this.table20.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.506D)));
            this.table20.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.506D)));
            this.table20.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.506D)));
            this.table20.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.193D)));
            this.table20.Body.SetCellContent(0, 0, this.checkBox31);
            this.table20.Body.SetCellContent(0, 1, this.checkBox32);
            this.table20.Body.SetCellContent(0, 2, this.checkBox33);
            tableGroup121.Name = "group55";
            tableGroup122.Name = "tableGroup32";
            tableGroup123.Name = "tableGroup33";
            tableGroup120.ChildGroups.Add(tableGroup121);
            tableGroup120.ChildGroups.Add(tableGroup122);
            tableGroup120.ChildGroups.Add(tableGroup123);
            tableGroup120.Name = "tableGroup31";
            tableGroup120.ReportItem = this.textBox101;
            this.table20.ColumnGroups.Add(tableGroup120);
            this.table20.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox31,
            this.checkBox32,
            this.checkBox33,
            this.textBox101});
            this.table20.Name = "table20";
            tableGroup124.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup124.Name = "detailTableGroup11";
            this.table20.RowGroups.Add(tableGroup124);
            this.table20.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.518D), Telerik.Reporting.Drawing.Unit.Inch(0.386D));
            this.table20.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.table20.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table20.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table20.StyleName = "";
            // 
            // checkBox31
            // 
            this.checkBox31.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox31.CheckedImage = ((object)(resources.GetObject("checkBox31.CheckedImage")));
            this.checkBox31.IndeterminateImage = ((object)(resources.GetObject("checkBox31.IndeterminateImage")));
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.506D), Telerik.Reporting.Drawing.Unit.Inch(0.193D));
            this.checkBox31.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox31.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox31.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox31.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox31.StyleName = "";
            this.checkBox31.Text = " 1";
            this.checkBox31.UncheckedImage = ((object)(resources.GetObject("checkBox31.UncheckedImage")));
            this.checkBox31.Value = "";
            // 
            // checkBox32
            // 
            this.checkBox32.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox32.CheckedImage = ((object)(resources.GetObject("checkBox32.CheckedImage")));
            this.checkBox32.IndeterminateImage = ((object)(resources.GetObject("checkBox32.IndeterminateImage")));
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.506D), Telerik.Reporting.Drawing.Unit.Inch(0.193D));
            this.checkBox32.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox32.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox32.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox32.StyleName = "";
            this.checkBox32.Text = " 2";
            this.checkBox32.UncheckedImage = ((object)(resources.GetObject("checkBox32.UncheckedImage")));
            this.checkBox32.Value = "";
            // 
            // checkBox33
            // 
            this.checkBox33.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox33.CheckedImage = ((object)(resources.GetObject("checkBox33.CheckedImage")));
            this.checkBox33.IndeterminateImage = ((object)(resources.GetObject("checkBox33.IndeterminateImage")));
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.506D), Telerik.Reporting.Drawing.Unit.Inch(0.193D));
            this.checkBox33.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox33.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox33.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox33.StyleName = "";
            this.checkBox33.Text = " 3";
            this.checkBox33.UncheckedImage = ((object)(resources.GetObject("checkBox33.UncheckedImage")));
            this.checkBox33.Value = "";
            // 
            // table21
            // 
            this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.373D)));
            this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.362D)));
            this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.487D)));
            this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.573D)));
            this.table21.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.718D)));
            this.table21.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.249D)));
            this.table21.Body.SetCellContent(0, 1, this.checkBox34);
            this.table21.Body.SetCellContent(0, 2, this.checkBox35);
            this.table21.Body.SetCellContent(0, 3, this.checkBox36);
            this.table21.Body.SetCellContent(0, 4, this.checkBox37);
            this.table21.Body.SetCellContent(0, 0, this.textBox102);
            tableGroup125.Name = "tableGroup8";
            tableGroup126.Name = "tableGroup9";
            tableGroup127.Name = "tableGroup10";
            tableGroup128.Name = "group15";
            tableGroup129.Name = "group16";
            this.table21.ColumnGroups.Add(tableGroup125);
            this.table21.ColumnGroups.Add(tableGroup126);
            this.table21.ColumnGroups.Add(tableGroup127);
            this.table21.ColumnGroups.Add(tableGroup128);
            this.table21.ColumnGroups.Add(tableGroup129);
            this.table21.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox102,
            this.checkBox34,
            this.checkBox35,
            this.checkBox36,
            this.checkBox37});
            this.table21.Name = "table21";
            tableGroup130.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup130.Name = "detailTableGroup3";
            this.table21.RowGroups.Add(tableGroup130);
            this.table21.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.513D), Telerik.Reporting.Drawing.Unit.Inch(0.249D));
            this.table21.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.table21.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table21.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table21.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table21.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table21.StyleName = "";
            // 
            // checkBox34
            // 
            this.checkBox34.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox34.CheckedImage = ((object)(resources.GetObject("checkBox34.CheckedImage")));
            this.checkBox34.IndeterminateImage = ((object)(resources.GetObject("checkBox34.IndeterminateImage")));
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.362D), Telerik.Reporting.Drawing.Unit.Inch(0.249D));
            this.checkBox34.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox34.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox34.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox34.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox34.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox34.StyleName = "";
            this.checkBox34.Text = "Basic (Type 6)";
            this.checkBox34.UncheckedImage = ((object)(resources.GetObject("checkBox34.UncheckedImage")));
            this.checkBox34.Value = "";
            // 
            // checkBox35
            // 
            this.checkBox35.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox35.CheckedImage = ((object)(resources.GetObject("checkBox35.CheckedImage")));
            this.checkBox35.IndeterminateImage = ((object)(resources.GetObject("checkBox35.IndeterminateImage")));
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.487D), Telerik.Reporting.Drawing.Unit.Inch(0.249D));
            this.checkBox35.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox35.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox35.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox35.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox35.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox35.StyleName = "";
            this.checkBox35.Text = "Interval (Type 5) ";
            this.checkBox35.UncheckedImage = ((object)(resources.GetObject("checkBox35.UncheckedImage")));
            this.checkBox35.Value = "";
            // 
            // checkBox36
            // 
            this.checkBox36.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox36.CheckedImage = ((object)(resources.GetObject("checkBox36.CheckedImage")));
            this.checkBox36.IndeterminateImage = ((object)(resources.GetObject("checkBox36.IndeterminateImage")));
            this.checkBox36.Name = "checkBox36";
            this.checkBox36.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.573D), Telerik.Reporting.Drawing.Unit.Inch(0.249D));
            this.checkBox36.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox36.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox36.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox36.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox36.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox36.StyleName = "";
            this.checkBox36.Text = "Interval (Type 1-4)";
            this.checkBox36.UncheckedImage = ((object)(resources.GetObject("checkBox36.UncheckedImage")));
            this.checkBox36.Value = "";
            // 
            // checkBox37
            // 
            this.checkBox37.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox37.CheckedImage = ((object)(resources.GetObject("checkBox37.CheckedImage")));
            this.checkBox37.IndeterminateImage = ((object)(resources.GetObject("checkBox37.IndeterminateImage")));
            this.checkBox37.Name = "checkBox37";
            this.checkBox37.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.718D), Telerik.Reporting.Drawing.Unit.Inch(0.249D));
            this.checkBox37.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox37.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox37.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox37.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox37.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox37.StyleName = "";
            this.checkBox37.Text = "Un Metered (Type 7)";
            this.checkBox37.UncheckedImage = ((object)(resources.GetObject("checkBox37.UncheckedImage")));
            this.checkBox37.Value = "";
            // 
            // textBox102
            // 
            this.textBox102.Name = "textBox102";
            this.textBox102.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.373D), Telerik.Reporting.Drawing.Unit.Inch(0.249D));
            this.textBox102.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox102.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0.1D);
            this.textBox102.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox102.StyleName = "";
            this.textBox102.Value = "Type of Metering:";
            // 
            // table22
            // 
            this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.176D)));
            this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.004D)));
            this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.785D)));
            this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.162D)));
            this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.413D)));
            this.table22.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.972D)));
            this.table22.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.299D)));
            this.table22.Body.SetCellContent(0, 1, this.checkBox38);
            this.table22.Body.SetCellContent(0, 2, this.checkBox39);
            this.table22.Body.SetCellContent(0, 3, this.checkBox40);
            this.table22.Body.SetCellContent(0, 4, this.checkBox41);
            this.table22.Body.SetCellContent(0, 0, this.textBox103);
            this.table22.Body.SetCellContent(0, 5, this.table23);
            tableGroup136.Name = "tableGroup8";
            tableGroup137.Name = "tableGroup9";
            tableGroup138.Name = "tableGroup10";
            tableGroup139.Name = "group15";
            tableGroup140.Name = "group16";
            tableGroup141.Name = "group62";
            this.table22.ColumnGroups.Add(tableGroup136);
            this.table22.ColumnGroups.Add(tableGroup137);
            this.table22.ColumnGroups.Add(tableGroup138);
            this.table22.ColumnGroups.Add(tableGroup139);
            this.table22.ColumnGroups.Add(tableGroup140);
            this.table22.ColumnGroups.Add(tableGroup141);
            this.table22.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox103,
            this.checkBox38,
            this.checkBox39,
            this.checkBox40,
            this.checkBox41,
            this.table23});
            this.table22.Name = "table22";
            tableGroup142.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup142.Name = "detailTableGroup3";
            this.table22.RowGroups.Add(tableGroup142);
            this.table22.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(0.299D));
            this.table22.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.table22.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table22.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table22.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table22.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table22.StyleName = "";
            // 
            // checkBox38
            // 
            this.checkBox38.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox38.CheckedImage = ((object)(resources.GetObject("checkBox38.CheckedImage")));
            this.checkBox38.IndeterminateImage = ((object)(resources.GetObject("checkBox38.IndeterminateImage")));
            this.checkBox38.Name = "checkBox38";
            this.checkBox38.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.004D), Telerik.Reporting.Drawing.Unit.Inch(0.299D));
            this.checkBox38.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox38.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox38.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox38.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox38.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox38.StyleName = "";
            this.checkBox38.Text = "Import/Export";
            this.checkBox38.UncheckedImage = ((object)(resources.GetObject("checkBox38.UncheckedImage")));
            this.checkBox38.Value = "";
            // 
            // checkBox39
            // 
            this.checkBox39.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox39.CheckedImage = ((object)(resources.GetObject("checkBox39.CheckedImage")));
            this.checkBox39.IndeterminateImage = ((object)(resources.GetObject("checkBox39.IndeterminateImage")));
            this.checkBox39.Name = "checkBox39";
            this.checkBox39.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.785D), Telerik.Reporting.Drawing.Unit.Inch(0.299D));
            this.checkBox39.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox39.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox39.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox39.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox39.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox39.StyleName = "";
            this.checkBox39.Text = "2 Rate";
            this.checkBox39.UncheckedImage = ((object)(resources.GetObject("checkBox39.UncheckedImage")));
            this.checkBox39.Value = "";
            // 
            // checkBox40
            // 
            this.checkBox40.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox40.CheckedImage = ((object)(resources.GetObject("checkBox40.CheckedImage")));
            this.checkBox40.IndeterminateImage = ((object)(resources.GetObject("checkBox40.IndeterminateImage")));
            this.checkBox40.Name = "checkBox40";
            this.checkBox40.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.162D), Telerik.Reporting.Drawing.Unit.Inch(0.299D));
            this.checkBox40.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox40.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox40.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox40.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox40.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox40.StyleName = "";
            this.checkBox40.Text = "Controlled Load";
            this.checkBox40.UncheckedImage = ((object)(resources.GetObject("checkBox40.UncheckedImage")));
            this.checkBox40.Value = "";
            // 
            // checkBox41
            // 
            this.checkBox41.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox41.CheckedImage = ((object)(resources.GetObject("checkBox41.CheckedImage")));
            this.checkBox41.IndeterminateImage = ((object)(resources.GetObject("checkBox41.IndeterminateImage")));
            this.checkBox41.Name = "checkBox41";
            this.checkBox41.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.413D), Telerik.Reporting.Drawing.Unit.Inch(0.299D));
            this.checkBox41.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox41.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.checkBox41.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Inch(0.1D);
            this.checkBox41.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox41.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox41.StyleName = "";
            this.checkBox41.Text = "Current Transformer";
            this.checkBox41.UncheckedImage = ((object)(resources.GetObject("checkBox41.UncheckedImage")));
            this.checkBox41.Value = "";
            // 
            // textBox103
            // 
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.176D), Telerik.Reporting.Drawing.Unit.Inch(0.299D));
            this.textBox103.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox103.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(0.1D);
            this.textBox103.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox103.StyleName = "";
            this.textBox103.Value = "Meter Sub Type(s): ";
            // 
            // table23
            // 
            this.table23.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.657D)));
            this.table23.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.657D)));
            this.table23.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.657D)));
            this.table23.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.15D)));
            this.table23.Body.SetCellContent(0, 0, this.textBox98, 1, 3);
            tableGroup132.Name = "group55";
            tableGroup133.Name = "tableGroup32";
            tableGroup134.Name = "tableGroup33";
            tableGroup131.ChildGroups.Add(tableGroup132);
            tableGroup131.ChildGroups.Add(tableGroup133);
            tableGroup131.ChildGroups.Add(tableGroup134);
            tableGroup131.Name = "tableGroup31";
            tableGroup131.ReportItem = this.textBox104;
            this.table23.ColumnGroups.Add(tableGroup131);
            this.table23.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox98,
            this.textBox104});
            this.table23.Name = "table23";
            tableGroup135.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup135.Name = "detailTableGroup11";
            this.table23.RowGroups.Add(tableGroup135);
            this.table23.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.971D), Telerik.Reporting.Drawing.Unit.Inch(0.3D));
            this.table23.Style.BorderStyle.Left = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table23.StyleName = "";
            // 
            // textBox98
            // 
            this.textBox98.Name = "textBox98";
            this.textBox98.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.971D), Telerik.Reporting.Drawing.Unit.Inch(0.15D));
            this.textBox98.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox98.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.textBox98.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox98.StyleName = "";
            this.textBox98.Value = "textBox98";
            // 
            // table24
            // 
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.361D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.05D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.276D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.934D)));
            this.table24.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.891D)));
            this.table24.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table24.Body.SetCellContent(0, 0, this.textBox106);
            this.table24.Body.SetCellContent(0, 2, this.textBox111);
            this.table24.Body.SetCellContent(0, 4, this.textBox105);
            this.table24.Body.SetCellContent(0, 3, this.textBox107);
            this.table24.Body.SetCellContent(0, 1, this.table25);
            tableGroup146.Name = "tableGroup34";
            tableGroup147.Name = "tableGroup35";
            tableGroup148.Name = "tableGroup36";
            tableGroup149.Name = "group64";
            tableGroup150.Name = "group63";
            this.table24.ColumnGroups.Add(tableGroup146);
            this.table24.ColumnGroups.Add(tableGroup147);
            this.table24.ColumnGroups.Add(tableGroup148);
            this.table24.ColumnGroups.Add(tableGroup149);
            this.table24.ColumnGroups.Add(tableGroup150);
            this.table24.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox106,
            this.table25,
            this.textBox111,
            this.textBox107,
            this.textBox105});
            this.table24.Name = "table24";
            tableGroup151.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup151.Name = "detailTableGroup12";
            this.table24.RowGroups.Add(tableGroup151);
            this.table24.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table24.Style.BorderColor.Default = System.Drawing.Color.Black;
            this.table24.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table24.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table24.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table24.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table24.StyleName = "";
            // 
            // textBox106
            // 
            this.textBox106.Name = "textBox106";
            this.textBox106.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.361D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox106.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox106.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox106.Value = "Is SA Power Networks the meter provider? ";
            // 
            // textBox111
            // 
            this.textBox111.Name = "textBox111";
            this.textBox111.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.276D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox111.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            // 
            // textBox105
            // 
            this.textBox105.Name = "textBox105";
            this.textBox105.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.891D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox105.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Dotted;
            this.textBox105.StyleName = "";
            // 
            // textBox107
            // 
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.934D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.textBox107.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox107.StyleName = "";
            this.textBox107.Value = "Name of meter provider:";
            // 
            // table25
            // 
            this.table25.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.505D)));
            this.table25.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.462D)));
            this.table25.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.2D)));
            this.table25.Body.SetCellContent(0, 0, this.checkBox42);
            this.table25.Body.SetCellContent(0, 1, this.checkBox44);
            tableGroup143.Name = "tableGroup7";
            tableGroup144.Name = "tableGroup9";
            this.table25.ColumnGroups.Add(tableGroup143);
            this.table25.ColumnGroups.Add(tableGroup144);
            this.table25.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox42,
            this.checkBox44});
            this.table25.Name = "table25";
            tableGroup145.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup145.Name = "detailTableGroup3";
            this.table25.RowGroups.Add(tableGroup145);
            this.table25.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.05D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.table25.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table25.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table25.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table25.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table25.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.table25.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table25.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox42
            // 
            this.checkBox42.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox42.CheckedImage = ((object)(resources.GetObject("checkBox42.CheckedImage")));
            this.checkBox42.IndeterminateImage = ((object)(resources.GetObject("checkBox42.IndeterminateImage")));
            this.checkBox42.Name = "checkBox42";
            this.checkBox42.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.505D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox42.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.checkBox42.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox42.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox42.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox42.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox42.StyleName = "";
            this.checkBox42.Text = " Yes";
            this.checkBox42.UncheckedImage = ((object)(resources.GetObject("checkBox42.UncheckedImage")));
            this.checkBox42.Value = "";
            // 
            // checkBox44
            // 
            this.checkBox44.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox44.CheckedImage = ((object)(resources.GetObject("checkBox44.CheckedImage")));
            this.checkBox44.IndeterminateImage = ((object)(resources.GetObject("checkBox44.IndeterminateImage")));
            this.checkBox44.Name = "checkBox44";
            this.checkBox44.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.462D), Telerik.Reporting.Drawing.Unit.Inch(0.2D));
            this.checkBox44.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox44.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox44.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox44.StyleName = "";
            this.checkBox44.Text = " No";
            this.checkBox44.UncheckedImage = ((object)(resources.GetObject("checkBox44.UncheckedImage")));
            this.checkBox44.Value = "";
            // 
            // table26
            // 
            this.table26.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.462D)));
            this.table26.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.207D)));
            this.table26.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.676D)));
            this.table26.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(1.167D)));
            this.table26.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18D)));
            this.table26.Body.SetCellContent(0, 0, this.textBox108);
            this.table26.Body.SetCellContent(0, 2, this.textBox113);
            this.table26.Body.SetCellContent(0, 1, this.table27);
            this.table26.Body.SetCellContent(0, 3, this.table28);
            tableGroup158.Name = "tableGroup34";
            tableGroup159.Name = "tableGroup35";
            tableGroup160.Name = "group64";
            tableGroup161.Name = "group63";
            this.table26.ColumnGroups.Add(tableGroup158);
            this.table26.ColumnGroups.Add(tableGroup159);
            this.table26.ColumnGroups.Add(tableGroup160);
            this.table26.ColumnGroups.Add(tableGroup161);
            this.table26.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.textBox108,
            this.table27,
            this.textBox113,
            this.table28});
            this.table26.Name = "table26";
            tableGroup162.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup162.Name = "detailTableGroup12";
            this.table26.RowGroups.Add(tableGroup162);
            this.table26.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.512D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
            this.table26.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table26.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table26.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table26.Style.BorderWidth.Right = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table26.StyleName = "";
            // 
            // textBox108
            // 
            this.textBox108.Name = "textBox108";
            this.textBox108.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.462D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
            this.textBox108.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.textBox108.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox108.Value = "Is the meter position to be altered?";
            // 
            // textBox113
            // 
            this.textBox113.Name = "textBox113";
            this.textBox113.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.676D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
            this.textBox113.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox113.StyleName = "";
            this.textBox113.Value = "Are consumer mains being replaced? ";
            // 
            // table27
            // 
            this.table27.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.586D)));
            this.table27.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.537D)));
            this.table27.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18D)));
            this.table27.Body.SetCellContent(0, 0, this.checkBox45);
            this.table27.Body.SetCellContent(0, 1, this.checkBox46);
            tableGroup152.Name = "tableGroup7";
            tableGroup153.Name = "tableGroup9";
            this.table27.ColumnGroups.Add(tableGroup152);
            this.table27.ColumnGroups.Add(tableGroup153);
            this.table27.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox45,
            this.checkBox46});
            this.table27.Name = "table27";
            tableGroup154.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup154.Name = "detailTableGroup3";
            this.table27.RowGroups.Add(tableGroup154);
            this.table27.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.206D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
            this.table27.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table27.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table27.Style.BorderStyle.Right = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table27.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table27.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table27.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.table27.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table27.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox45
            // 
            this.checkBox45.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox45.CheckedImage = ((object)(resources.GetObject("checkBox45.CheckedImage")));
            this.checkBox45.IndeterminateImage = ((object)(resources.GetObject("checkBox45.IndeterminateImage")));
            this.checkBox45.Name = "checkBox45";
            this.checkBox45.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.586D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
            this.checkBox45.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.checkBox45.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox45.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox45.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox45.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox45.StyleName = "";
            this.checkBox45.Text = " Yes";
            this.checkBox45.UncheckedImage = ((object)(resources.GetObject("checkBox45.UncheckedImage")));
            this.checkBox45.Value = "";
            // 
            // checkBox46
            // 
            this.checkBox46.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox46.CheckedImage = ((object)(resources.GetObject("checkBox46.CheckedImage")));
            this.checkBox46.IndeterminateImage = ((object)(resources.GetObject("checkBox46.IndeterminateImage")));
            this.checkBox46.Name = "checkBox46";
            this.checkBox46.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.537D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
            this.checkBox46.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox46.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox46.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox46.StyleName = "";
            this.checkBox46.Text = " No";
            this.checkBox46.UncheckedImage = ((object)(resources.GetObject("checkBox46.UncheckedImage")));
            this.checkBox46.Value = "";
            // 
            // table28
            // 
            this.table28.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.566D)));
            this.table28.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(0.518D)));
            this.table28.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.18D)));
            this.table28.Body.SetCellContent(0, 0, this.checkBox47);
            this.table28.Body.SetCellContent(0, 1, this.checkBox48);
            tableGroup155.Name = "tableGroup7";
            tableGroup156.Name = "tableGroup9";
            this.table28.ColumnGroups.Add(tableGroup155);
            this.table28.ColumnGroups.Add(tableGroup156);
            this.table28.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.checkBox47,
            this.checkBox48});
            this.table28.Name = "table28";
            tableGroup157.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup157.Name = "detailTableGroup3";
            this.table28.RowGroups.Add(tableGroup157);
            this.table28.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(1.167D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
            this.table28.Style.BorderColor.Default = System.Drawing.Color.Silver;
            this.table28.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.table28.Style.BorderWidth.Default = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table28.Style.Color = System.Drawing.Color.MidnightBlue;
            this.table28.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(1D);
            this.table28.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.table28.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            // 
            // checkBox47
            // 
            this.checkBox47.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox47.CheckedImage = ((object)(resources.GetObject("checkBox47.CheckedImage")));
            this.checkBox47.IndeterminateImage = ((object)(resources.GetObject("checkBox47.IndeterminateImage")));
            this.checkBox47.Name = "checkBox47";
            this.checkBox47.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.566D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
            this.checkBox47.Style.BorderStyle.Default = Telerik.Reporting.Drawing.BorderType.None;
            this.checkBox47.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox47.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox47.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.checkBox47.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox47.StyleName = "";
            this.checkBox47.Text = " Yes";
            this.checkBox47.UncheckedImage = ((object)(resources.GetObject("checkBox47.UncheckedImage")));
            this.checkBox47.Value = "";
            // 
            // checkBox48
            // 
            this.checkBox48.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBox48.CheckedImage = ((object)(resources.GetObject("checkBox48.CheckedImage")));
            this.checkBox48.IndeterminateImage = ((object)(resources.GetObject("checkBox48.IndeterminateImage")));
            this.checkBox48.Name = "checkBox48";
            this.checkBox48.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(0.518D), Telerik.Reporting.Drawing.Unit.Inch(0.18D));
            this.checkBox48.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.checkBox48.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(5D);
            this.checkBox48.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.checkBox48.StyleName = "";
            this.checkBox48.Text = " No";
            this.checkBox48.UncheckedImage = ((object)(resources.GetObject("checkBox48.UncheckedImage")));
            this.checkBox48.Value = "";
            // 
            // table29
            // 
            this.table29.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.622D)));
            this.table29.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(3.23D)));
            this.table29.Body.Columns.Add(new Telerik.Reporting.TableBodyColumn(Telerik.Reporting.Drawing.Unit.Inch(2.004D)));
            this.table29.Body.Rows.Add(new Telerik.Reporting.TableBodyRow(Telerik.Reporting.Drawing.Unit.Inch(0.179D)));
            this.table29.Body.SetCellContent(0, 2, this.textBox116);
            this.table29.Body.SetCellContent(0, 0, this.htmlTextBox4, 1, 2);
            tableGroup174.Name = "tableGroup37";
            tableGroup175.Name = "tableGroup38";
            tableGroup176.Name = "tableGroup39";
            this.table29.ColumnGroups.Add(tableGroup174);
            this.table29.ColumnGroups.Add(tableGroup175);
            this.table29.ColumnGroups.Add(tableGroup176);
            this.table29.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.htmlTextBox4,
            this.textBox116});
            this.table29.Name = "table29";
            tableGroup177.Groupings.Add(new Telerik.Reporting.Grouping(null));
            tableGroup177.Name = "detailTableGroup13";
            this.table29.RowGroups.Add(tableGroup177);
            this.table29.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.856D), Telerik.Reporting.Drawing.Unit.Inch(0.179D));
            this.table29.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.table29.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table29.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.table29.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table29.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.table29.Style.Font.Bold = true;
            this.table29.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.table29.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.table29.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.table29.StyleName = "";
            // 
            // textBox116
            // 
            this.textBox116.Name = "textBox116";
            this.textBox116.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(2.004D), Telerik.Reporting.Drawing.Unit.Inch(0.179D));
            // 
            // htmlTextBox4
            // 
            this.htmlTextBox4.Name = "htmlTextBox4";
            this.htmlTextBox4.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(5.852D), Telerik.Reporting.Drawing.Unit.Inch(0.179D));
            this.htmlTextBox4.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.htmlTextBox4.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox4.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.htmlTextBox4.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.htmlTextBox4.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.htmlTextBox4.Style.Font.Bold = false;
            this.htmlTextBox4.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(8.5D);
            this.htmlTextBox4.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.htmlTextBox4.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.htmlTextBox4.StyleName = "";
            this.htmlTextBox4.Value = "If SA Power Networks has done prior work to supply a connection point, please pro" +
    "vide job number";
            // 
            // textBox109
            // 
            this.textBox109.Name = "textBox109";
            this.textBox109.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.856D), Telerik.Reporting.Drawing.Unit.Inch(0.158D));
            this.textBox109.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox109.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox109.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox109.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox109.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox109.Style.Font.Bold = true;
            this.textBox109.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox109.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Left;
            this.textBox109.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox109.StyleName = "";
            this.textBox109.Value = "Comments:";
            // 
            // textBox110
            // 
            this.textBox110.Name = "textBox110";
            this.textBox110.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.856D), Telerik.Reporting.Drawing.Unit.Inch(0.408D));
            this.textBox110.Style.BackgroundColor = System.Drawing.Color.Transparent;
            this.textBox110.Style.BorderStyle.Bottom = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox110.Style.BorderStyle.Top = Telerik.Reporting.Drawing.BorderType.Solid;
            this.textBox110.Style.BorderWidth.Bottom = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox110.Style.BorderWidth.Top = Telerik.Reporting.Drawing.Unit.Point(0.3D);
            this.textBox110.Style.Font.Bold = true;
            this.textBox110.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(9D);
            this.textBox110.Style.TextAlign = Telerik.Reporting.Drawing.HorizontalAlign.Center;
            this.textBox110.Style.VerticalAlign = Telerik.Reporting.Drawing.VerticalAlign.Middle;
            this.textBox110.StyleName = "";
            // 
            // htmlTextBox5
            // 
            this.htmlTextBox5.Location = new Telerik.Reporting.Drawing.PointU(Telerik.Reporting.Drawing.Unit.Inch(0D), Telerik.Reporting.Drawing.Unit.Inch(10.8D));
            this.htmlTextBox5.Name = "htmlTextBox5";
            this.htmlTextBox5.Size = new Telerik.Reporting.Drawing.SizeU(Telerik.Reporting.Drawing.Unit.Inch(7.826D), Telerik.Reporting.Drawing.Unit.Inch(0.37D));
            this.htmlTextBox5.Style.Font.Size = Telerik.Reporting.Drawing.Unit.Point(6.5D);
            this.htmlTextBox5.Value = resources.GetString("htmlTextBox5.Value");
            // 
            // SAPowerNetworks
            // 
            this.Items.AddRange(new Telerik.Reporting.ReportItemBase[] {
            this.detail});
            this.Name = "SAPowerNetworks";
            this.PageSettings.ContinuousPaper = false;
            this.PageSettings.Landscape = false;
            this.PageSettings.Margins = new Telerik.Reporting.Drawing.MarginsU(Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0.5D), Telerik.Reporting.Drawing.Unit.Cm(0D));
            this.PageSettings.PaperKind = System.Drawing.Printing.PaperKind.A4;
            styleRule1.Selectors.AddRange(new Telerik.Reporting.Drawing.ISelector[] {
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.TextItemBase)),
            new Telerik.Reporting.Drawing.TypeSelector(typeof(Telerik.Reporting.HtmlTextBox))});
            styleRule1.Style.Padding.Left = Telerik.Reporting.Drawing.Unit.Point(2D);
            styleRule1.Style.Padding.Right = Telerik.Reporting.Drawing.Unit.Point(2D);
            this.StyleSheet.AddRange(new Telerik.Reporting.Drawing.StyleRule[] {
            styleRule1});
            this.Width = Telerik.Reporting.Drawing.Unit.Inch(7.892D);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion
		private Telerik.Reporting.DetailSection detail;
        private Telerik.Reporting.Panel panel1;
        private Telerik.Reporting.TextBox textBox1;
        private Telerik.Reporting.TextBox textBox2;
        private Telerik.Reporting.TextBox textBox3;
        private Telerik.Reporting.TextBox textBox4;
        private Telerik.Reporting.CheckBox checkBox13;
        private Telerik.Reporting.Table table1;
        private Telerik.Reporting.TextBox textBox13;
        private Telerik.Reporting.CheckBox checkBox43;
        private Telerik.Reporting.CheckBox checkBox1;
        private Telerik.Reporting.CheckBox checkBox2;
        private Telerik.Reporting.CheckBox checkBox3;
        private Telerik.Reporting.CheckBox checkBox4;
        private Telerik.Reporting.Table table2;
        private Telerik.Reporting.TextBox textBox5;
        private Telerik.Reporting.Table table3;
        private Telerik.Reporting.TextBox textBox7;
        private Telerik.Reporting.TextBox textBox10;
        private Telerik.Reporting.TextBox textBox12;
        private Telerik.Reporting.TextBox textBox14;
        private Telerik.Reporting.TextBox textBox20;
        private Telerik.Reporting.TextBox textBox22;
        private Telerik.Reporting.TextBox textBox23;
        private Telerik.Reporting.TextBox textBox24;
        private Telerik.Reporting.TextBox textBox25;
        private Telerik.Reporting.TextBox textBox26;
        private Telerik.Reporting.TextBox textBox28;
        private Telerik.Reporting.TextBox textBox30;
        private Telerik.Reporting.TextBox textBox31;
        private Telerik.Reporting.TextBox textBox34;
        private Telerik.Reporting.TextBox textBox35;
        private Telerik.Reporting.TextBox textBox36;
        private Telerik.Reporting.TextBox textBox37;
        private Telerik.Reporting.TextBox textBox39;
        private Telerik.Reporting.TextBox textBox42;
        private Telerik.Reporting.TextBox textBox44;
        private Telerik.Reporting.TextBox textBox47;
        private Telerik.Reporting.TextBox textBox50;
        private Telerik.Reporting.TextBox textBox52;
        private Telerik.Reporting.TextBox textBox58;
        private Telerik.Reporting.TextBox textBox59;
        private Telerik.Reporting.TextBox textBox60;
        private Telerik.Reporting.TextBox textBox61;
        private Telerik.Reporting.TextBox textBox6;
        private Telerik.Reporting.Table table4;
        private Telerik.Reporting.CheckBox checkBox76;
        private Telerik.Reporting.CheckBox checkBox5;
        private Telerik.Reporting.CheckBox checkBox6;
        private Telerik.Reporting.CheckBox checkBox7;
        private Telerik.Reporting.CheckBox checkBox8;
        private Telerik.Reporting.PictureBox pictureBox1;
        private Telerik.Reporting.TextBox textBox11;
        private Telerik.Reporting.Table table5;
        private Telerik.Reporting.TextBox textBox18;
        private Telerik.Reporting.TextBox textBox43;
        private Telerik.Reporting.TextBox textBox45;
        private Telerik.Reporting.TextBox textBox46;
        private Telerik.Reporting.TextBox textBox48;
        private Telerik.Reporting.TextBox textBox49;
        private Telerik.Reporting.TextBox textBox51;
        private Telerik.Reporting.Table table6;
        private Telerik.Reporting.TextBox textBox17;
        private Telerik.Reporting.TextBox textBox53;
        private Telerik.Reporting.TextBox textBox54;
        private Telerik.Reporting.TextBox textBox56;
        private Telerik.Reporting.TextBox textBox15;
        private Telerik.Reporting.TextBox textBox16;
        private Telerik.Reporting.TextBox textBox19;
        private Telerik.Reporting.TextBox textBox55;
        private Telerik.Reporting.HtmlTextBox htmlTextBox1;
        private Telerik.Reporting.TextBox textBox8;
        private Telerik.Reporting.Table table7;
        private Telerik.Reporting.TextBox textBox21;
        private Telerik.Reporting.TextBox textBox32;
        private Telerik.Reporting.TextBox textBox38;
        private Telerik.Reporting.TextBox textBox40;
        private Telerik.Reporting.TextBox textBox57;
        private Telerik.Reporting.TextBox textBox9;
        private Telerik.Reporting.TextBox textBox29;
        private Telerik.Reporting.TextBox textBox27;
        private Telerik.Reporting.TextBox textBox41;
        private Telerik.Reporting.TextBox textBox33;
        private Telerik.Reporting.TextBox textBox64;
        private Telerik.Reporting.Table table8;
        private Telerik.Reporting.TextBox textBox65;
        private Telerik.Reporting.TextBox textBox67;
        private Telerik.Reporting.HtmlTextBox htmlTextBox2;
        private Telerik.Reporting.TextBox textBox62;
        private Telerik.Reporting.TextBox textBox66;
        private Telerik.Reporting.HtmlTextBox htmlTextBox3;
        private Telerik.Reporting.TextBox textBox69;
        private Telerik.Reporting.Table table9;
        private Telerik.Reporting.TextBox textBox70;
        private Telerik.Reporting.TextBox textBox72;
        private Telerik.Reporting.TextBox textBox74;
        private Telerik.Reporting.TextBox textBox63;
        private Telerik.Reporting.TextBox textBox75;
        private Telerik.Reporting.TextBox textBox76;
        private Telerik.Reporting.TextBox textBox77;
        private Telerik.Reporting.TextBox textBox78;
        private Telerik.Reporting.TextBox textBox79;
        private Telerik.Reporting.TextBox textBox68;
        private Telerik.Reporting.TextBox textBox71;
        private Telerik.Reporting.TextBox textBox73;
        private Telerik.Reporting.TextBox textBox82;
        private Telerik.Reporting.Table table10;
        private Telerik.Reporting.TextBox textBox83;
        private Telerik.Reporting.TextBox textBox85;
        private Telerik.Reporting.TextBox textBox87;
        private Telerik.Reporting.TextBox textBox90;
        private Telerik.Reporting.TextBox textBox92;
        private Telerik.Reporting.TextBox textBox80;
        private Telerik.Reporting.TextBox textBox81;
        private Telerik.Reporting.TextBox textBox94;
        private Telerik.Reporting.TextBox textBox95;
        private Telerik.Reporting.Table table11;
        private Telerik.Reporting.CheckBox checkBox10;
        private Telerik.Reporting.CheckBox checkBox11;
        private Telerik.Reporting.CheckBox checkBox12;
        private Telerik.Reporting.CheckBox checkBox14;
        private Telerik.Reporting.TextBox textBox86;
        private Telerik.Reporting.TextBox textBox88;
        private Telerik.Reporting.TextBox textBox91;
        private Telerik.Reporting.Table table12;
        private Telerik.Reporting.TextBox textBox93;
        private Telerik.Reporting.TextBox textBox96;
        private Telerik.Reporting.Table table13;
        private Telerik.Reporting.CheckBox checkBox22;
        private Telerik.Reporting.CheckBox checkBox23;
        private Telerik.Reporting.Table table14;
        private Telerik.Reporting.CheckBox checkBox9;
        private Telerik.Reporting.CheckBox checkBox15;
        private Telerik.Reporting.Table table15;
        private Telerik.Reporting.TextBox textBox84;
        private Telerik.Reporting.CheckBox checkBox16;
        private Telerik.Reporting.CheckBox checkBox18;
        private Telerik.Reporting.CheckBox checkBox19;
        private Telerik.Reporting.Table table16;
        private Telerik.Reporting.CheckBox checkBox17;
        private Telerik.Reporting.CheckBox checkBox20;
        private Telerik.Reporting.CheckBox checkBox21;
        private Telerik.Reporting.CheckBox checkBox24;
        private Telerik.Reporting.TextBox textBox97;
        private Telerik.Reporting.CheckBox checkBox25;
        private Telerik.Reporting.CheckBox checkBox26;
        private Telerik.Reporting.TextBox textBox99;
        private Telerik.Reporting.Table table17;
        private Telerik.Reporting.TextBox textBox89;
        private Telerik.Reporting.TextBox textBox100;
        private Telerik.Reporting.Table table18;
        private Telerik.Reporting.CheckBox checkBox27;
        private Telerik.Reporting.CheckBox checkBox28;
        private Telerik.Reporting.Table table19;
        private Telerik.Reporting.CheckBox checkBox29;
        private Telerik.Reporting.CheckBox checkBox30;
        private Telerik.Reporting.Table table20;
        private Telerik.Reporting.CheckBox checkBox31;
        private Telerik.Reporting.CheckBox checkBox32;
        private Telerik.Reporting.CheckBox checkBox33;
        private Telerik.Reporting.TextBox textBox101;
        private Telerik.Reporting.Table table21;
        private Telerik.Reporting.CheckBox checkBox34;
        private Telerik.Reporting.CheckBox checkBox35;
        private Telerik.Reporting.CheckBox checkBox36;
        private Telerik.Reporting.CheckBox checkBox37;
        private Telerik.Reporting.TextBox textBox102;
        private Telerik.Reporting.Table table22;
        private Telerik.Reporting.CheckBox checkBox38;
        private Telerik.Reporting.CheckBox checkBox39;
        private Telerik.Reporting.CheckBox checkBox40;
        private Telerik.Reporting.CheckBox checkBox41;
        private Telerik.Reporting.TextBox textBox103;
        private Telerik.Reporting.Table table23;
        private Telerik.Reporting.TextBox textBox98;
        private Telerik.Reporting.TextBox textBox104;
        private Telerik.Reporting.Table table24;
        private Telerik.Reporting.TextBox textBox106;
        private Telerik.Reporting.TextBox textBox111;
        private Telerik.Reporting.TextBox textBox105;
        private Telerik.Reporting.TextBox textBox107;
        private Telerik.Reporting.Table table25;
        private Telerik.Reporting.CheckBox checkBox42;
        private Telerik.Reporting.CheckBox checkBox44;
        private Telerik.Reporting.Table table26;
        private Telerik.Reporting.TextBox textBox108;
        private Telerik.Reporting.TextBox textBox113;
        private Telerik.Reporting.Table table27;
        private Telerik.Reporting.CheckBox checkBox45;
        private Telerik.Reporting.CheckBox checkBox46;
        private Telerik.Reporting.Table table28;
        private Telerik.Reporting.CheckBox checkBox47;
        private Telerik.Reporting.CheckBox checkBox48;
        private Telerik.Reporting.Table table29;
        private Telerik.Reporting.TextBox textBox116;
        private Telerik.Reporting.HtmlTextBox htmlTextBox4;
        private Telerik.Reporting.TextBox textBox109;
        private Telerik.Reporting.TextBox textBox110;
        private Telerik.Reporting.HtmlTextBox htmlTextBox5;
    }
}